__author__ = 'Ivan Truskov'

from datetime import datetime
from sys import stderr
from exceptions import StopIteration
from re import sub
from string import rstrip, lstrip, strip
from os import system


class Article(object):
    def __init__(self):
        self.author = ''
        self.content = ''
        self.tag_date = datetime.min
        self.commit_date = datetime.max
        self.commit_id = ''
        self.tag_name = ''


articles_feed = []


def fetch_feed(filename):
    try:
        filestream = open(filename, 'r')
    except Exception:
        stderr.write("Failed to open {} to read tags\n".format(filename))
    lineiter = iter(filestream)
    try:
        while True:
            line = lineiter.next()
            if not len(line.split()) or line.split()[0] == 'tag':
                if not len(line.split()):
                    continue
                article = Article()
                article.tag_name = line.split()[1]
                line = lineiter.next()
                article.author = rstrip(line.replace('Tagger: ', ''))
                line = lineiter.next()
                line = sub('Date:[ ]+', '', line)
                line = rstrip(sub(' [+-][0123456789]{4}', '', line))
                # %z format directive is not accepted, sadly
                article.tag_date = datetime.strptime(line, '%a %b %d %H:%M:%S %Y')
                line = lineiter.next()
                while not len(line.split()) or line.split()[0] != 'commit':
                    if len(line):
                        if lstrip(line).startswith("- "):
                            article.content += "<ul><li>{}</li></ul>".format(strip(line[2:]))
                        else:
                            article.content += "<p>{}</p>".format(strip(line))
                    line = lineiter.next()
                article.content = sub("</ul><ul>", '', article.content)
                article.commit_id = line.split()[1]
                while line.split(':')[0] != 'Date':
                    line = lineiter.next()
                line = sub('Date:[ ]+', '', line)
                line = rstrip(sub(' [+-][0123456789]{4}', '', line))
                # %z format directive is not accepted, sadly
                article.tag_date = datetime.strptime(line, '%a %b %d %H:%M:%S %Y')
                articles_feed.append(article)
    except StopIteration:
        pass
    filestream.close()


def regenerate_feed(project_path):
    '''
    Get tags from git
    '''
    system('cd {} && git tag -l tag?? | xargs git show | sed s/trus19@gmail.com/kurvivor19@yandex.ru/ > {}/tags'.format(project_path, project_path))
