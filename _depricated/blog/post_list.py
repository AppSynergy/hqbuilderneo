__author__ = 'dante'

from _depricated.blog.consts import post_per_page
from blog.text import get_read_more_head
from db import *


def post_list(page=0, tag=None, author=None, reader=None):

    select = {}
    if tag:
        select['tags'] = tag
    if author:
        author = users.find_one({'name': author})
        select['author'] = author['_id']

    cursor = posts.find(select, {'comments': 0}).sort('time', -1).limit(post_per_page).skip(post_per_page * page)

    post_list = [{
        'id': str(data['_id']),
        'author': data['author'],
        'title': data['title'],
        'text': get_read_more_head(data['text']),
        'time': data['time'],
        'comments': data['comments_count'],
        'tags': data['tags'],
        'rate': int(data['rate']['value']),
    } for data in cursor]

    authors = {}
    for post in post_list:
        authors[str(post['author'])] = post['author']
    for auth in users.find({'_id': {'$in': [authors[a] for a in authors]}}):
        authors[str(auth['_id'])] = {
            'id': str(auth['_id']),
            'name': auth['name'],
            'avatar': auth['personal_info']['avatar'],
            'rate': auth['rate']
        }
    for post in post_list:
        post['author'] = authors[str(post['author'])]

    #     authors = users.find({'$or': [{'_id': authors[a]} for a in authors]})
    #     for auth in authors:
    #         for post in post_list:
    #             if auth['_id'] == post.author.id:
    #                 post.author.load(auth)
    # elif author:
    #     for post in post_list:
    #         post.author.load(author)

    # query = Q()
    # if tag is not None:
    #     tag = Tag.objects.get(title = tag)
    #     query = query & Q(tags__id = tag.id)
    # elif author is None and reader is not None:
    #     profile = reader.get_profile()
    #     if profile.filter_tags == UserProfile.filter_include:
    #         for filter_tag in reader.filtertag_set.all():
    #             query = query | Q(tags__id = filter_tag.filtered_tag.id)
    #     elif profile.filter_tags == UserProfile.filter_exclude:
    #         for filter_tag in reader.filtertag_set.all():
    #             query = query & ~Q(tags__id = filter_tag.filtered_tag.id)
    #
    # if author is not None:
    #     query = query & Q(author__username = author)
    # elif reader is not None:
    #     profile = reader.get_profile()
    #     if profile.filter_users == UserProfile.filter_include:
    #         for filter_user in reader.stream_owner.all():
    #             query = query | Q(author = filter_user.filtered_user)
    #     elif profile.filter_users == UserProfile.filter_exclude:
    #         for filter_user in reader.stream_owner.all():
    #             query = query & ~Q(author = filter_user.filtered_user)
    #
    # hit_query = Q()
    # for post_entity in data['posts']:
    #     post_entity.text, post_entity.read_more = get_read_more_head(post_entity.text)
    #     hit_query = hit_query | Q(post=post_entity)
    #Asdf12
    # if reader is not None:
    #     hits = PostHit.objects.filter(hit_query, user=reader)
    #     for post_entity in data['posts']:
    #         for hit in hits:
    #             if post_entity.id == hit.post_id:
    #                 post_entity.new_count = post_entity.comments - hit.hit_comment_count

    if author:
        tags = author['blog']['tags']
    else:
        tags = blog.find_one()['tags']
    tags = map(lambda (v, k): v, sorted(tags.iteritems(), key=lambda (k, v): (v, k), reverse=True))

    data = {
        'posts': post_list,
        'author': author,
        'page': page,
        'tag': tag,
        'tags': tags,
        'prev': (page - 1) if page > 0 else None,
        'next': (page + 1) if page < ((cursor.count() - 1) / post_per_page) else None,
    }

    # if page > 0:
    #     data.update({'prev': page - 1})
    #
    # if page < ((cursor.count() - 1) / post_per_page):
    #     data.update({'next': page + 1})

    return data

# def clear_bookmarks(user):
#     from datetime import datetime, timedelta
#     date_delta = datetime.now() - timedelta(days=user.get_profile().keep_bookmarks)
#     Bookmark.objects.filter(hit_time__lte=date_delta, pinned=False, user=user).delete()
#
# def get_bookmarked_posts_headers(user):
#     clear_bookmarks(user)
#     return [{ 'id': bm.post.id,
#               'title': bm.post.title,
#               'comments': bm.post.comments,
#               'new_comments': bm.post.comments - bm.hit_comment_count }
#             for bm in Bookmark.objects.filter(user=user).order_by('-hit_time')[:bookmarks_limit]]
#
# def get_bookmarked_posts_list(user):
#     """Get data for display list of posts"""
#     clear_bookmarks(user)
#     data = {'posts': []}
#
#     for bm in Bookmark.objects.filter(user=user).order_by('-hit_time'):
#         bm.post.new_count = bm.post.comments - bm.hit_comment_count
#         bm.post.text, bm.post.read_more = get_read_more_head(bm.post.text)
#         data['posts'].append(bm.post)
#
#     return data
