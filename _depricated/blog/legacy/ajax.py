from django.utils import simplejson
from django.template.loader import render_to_string
from dajaxice.decorators import dajaxice_register
from waaagh.multiblog.models import Comment, Post, Bookmark
from waaagh.multiblog.voting import process_vote
from _depricated.blog.legacy.views import get_stable_data, login_required

@login_required
@dajaxice_register
def comment(request, post, parent, text):
    from waaagh.multiblog.controllers.comment import new_comment
    return simplejson.dumps({
        'result': 'true',
        'parent': parent,
        'html_data': render_to_string('comment.html', {
            'comment': new_comment(
                author=request.user,
                post=Post.objects.get(id=int(post)),
                parent=Comment.objects.get(id=int(parent)) if parent != 'null' else None,
                text=text
            ),
            'stable': get_stable_data(request)})
    })

@login_required
@dajaxice_register
def vote(request, post_id, comment_id, val):
    try:
        return process_vote(request, post_id, comment_id, val)
    except Exception:
        return simplejson.dumps({'result': 'false'})

@dajaxice_register
def preview_post(request, post):
    from waaagh.multiblog.controllers.post import render_post_preview
    return simplejson.dumps({'post': render_post_preview(post)})

@login_required
@dajaxice_register
def send_post(request, post):
    from waaagh.multiblog.controllers.post import modify_post
    return simplejson.dumps({'post': modify_post(request.user, post)})

@login_required
@dajaxice_register
def vote_poll(request, post, ids):
    from waaagh.multiblog.controllers.post import vote_poll
    return simplejson.dumps({'result' : vote_poll(request.user, post, ids)})

@dajaxice_register
def preview_comment(request, text, proc_ref, proc_images, proc_lines):
    from waaagh.multiblog.controllers.comment import get_comment_text
    return simplejson.dumps({'preview_text':
                                 render_to_string('comment_text.html',
                                         {'comment': {'text': get_comment_text(text, proc_ref, proc_images, proc_lines)}})})

@dajaxice_register
def preview_mail(request, subject, text):
    return simplejson.dumps({'preview_text': render_to_string('mail_text.html',
            {'mail': {'zone' :{'message': {'text': text, 'subject': subject}}}})})

@login_required
@dajaxice_register
def bookmark(request, post):
    post = Post.objects.get(id=int(post))
    try:
        bookmark_ent = Bookmark.objects.get(post=post, user=request.user)
        if bookmark_ent.pinned:
            bookmark_ent.delete()
            return simplejson.dumps({'bookmarked': False})
        bookmark_ent.pinned = True
        bookmark_ent.save()
    except Exception:
        Bookmark(post=post, user=request.user, hit_comment_count=post.comments, pinned=True).save()
    return simplejson.dumps({'bookmarked': True})


# User settings callbacks
from waaagh.multiblog.controllers import user

@login_required
@dajaxice_register
def update_secure(request, password, new_password, email):
    return simplejson.dumps({'result': user.update_user_secure(request.user, password, new_password, email)})

@login_required
@dajaxice_register
def update_profile(request, country, city, date, about):
    return simplejson.dumps({'result': user.update_user_profile(request.user, country, city, date, about)})

@login_required
@dajaxice_register
def update_settings(request, blog_title, blog_desc, message_notify, bookmarked_notify, post_notify, keep):
    return simplejson.dumps({'result': user.update_user_settings(request.user, blog_title, blog_desc, message_notify,
        bookmarked_notify, post_notify, keep)})

@login_required
@dajaxice_register
def update_stream(request, tag_filter, tags, user_filter, users):
    return simplejson.dumps({'result': user.update_stream(request.user, tag_filter, tags, user_filter, users)})


#HQ
@login_required
@dajaxice_register
def hq_setup_roster(request, guid, name, limit):
    from waaagh.multiblog.hq.roster import setup_roster
    return simplejson.dumps({'result': setup_roster(request.user, guid, name, limit)})

@login_required
@dajaxice_register
def hq_save_unit(request, unit):
    from waaagh.multiblog.hq.roster import update_unit
    return simplejson.dumps({'result': update_unit(request.user, unit)})

@login_required
@dajaxice_register
def hq_delete_unit(request, guid):
    from waaagh.multiblog.hq.roster import delete_unit
    return simplejson.dumps({'result': delete_unit(request.user, guid)})
