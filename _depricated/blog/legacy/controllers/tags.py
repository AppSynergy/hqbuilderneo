# -*- coding: utf-8 -*-

__author__ = 'dante'
from waaagh.multiblog.models import Post, Tag, UserTag

def get_tags_list():
    return Tag.objects.exclude(post_count=0).order_by('-post_count')

#TODO: temporary conversion code
def fix_tags():
    rubrics = [
        {'ref': 'news', 'title': u'Новости', 'post_to': u'Новости'},
        {'ref': 'articles', 'title': u'Статьи', 'post_to': u'Статьи'},
        {'ref': 'qa', 'title': u'Вопросы', 'post_to': u'Вопросы'},
        {'ref': 'flame', 'title': u'Дискуссии', 'post_to': u'Дискуссии'},
        {'ref': 'offtopic', 'title': u'Offtopic', 'post_to': u'Offtopic'},
        {'ref': 'fight', 'title': u'Дуэли', 'post_to': u'Дуэли'},
        ]
    for rubric in rubrics:
        tag = Tag(name=rubric['ref'], title=rubric['title'])
        tag.save()
        rubric['tag'] = tag
        print "tag %s added" % tag.name
    for post in Post.objects.all():
        for rubric in rubrics:
            if post.rubric == rubric['ref']:
                post.tags.add(rubric['tag'])
                post.save()
                rubric['tag'].post_count += 1
                print "post %d tag %s added" % (post.id, rubric['tag'].name)
    for rubric in rubrics:
        rubric['tag'].save()


def fix_user_tags():
    for post in Post.objects.all():
        for tag in post.tags.all():
            try:
                user_tag = UserTag.objects.get(title = tag.title, user = post.author)
                user_tag.post_count = Post.objects.filter(tags__id = tag.id, author = post.author).count()
            except UserTag.DoesNotExist:
                user_tag = UserTag(title = tag.title, user = post.author, post_count = 1)
            user_tag.save()
            print "post %d tag %s added" % (post.id, tag.title)
#    # Update changed tags post counts
#    for tag in fix_tags.keys():
#        if fix_tags[tag] is not None:
#            fix_tags[tag].post_count = Post.objects.filter(tags__id = fix_tags[tag].id).count()
#            fix_tags[tag].save()
#            try:
#                user_tag = UserTag.objects.get(title = fix_tags[tag].title, user = post.author)
#                user_tag.post_count = Post.objects.filter(tags__id = fix_tags[tag].id, author = post.author).count()
#            except UserTag.DoesNotExist:
#                user_tag = UserTag(title = fix_tags[tag].title, user = post.author, post_count = 1)
#            user_tag.save()

