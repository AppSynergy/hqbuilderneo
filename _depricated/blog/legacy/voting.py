__author__ = 'dromanov'

from datetime import datetime, timedelta

from django.db.models import Sum
from django.utils import simplejson
from waaagh.multiblog.models import Comment, Vote, Post


def get_vote_val(val):
    if val == 'up':
        return Vote.up
    elif val == 'down':
        return Vote.down
    raise Exception

def update_rate(obj, rate):
    if rate is None:
        obj.rate = 0
    else:
        obj.rate = rate
    obj.save()
    return obj.rate

def get_user_vote_limit(user):
    from _depricated.blog.consts import user_vote_base, user_vote_step
    return max(user_vote_base + user.get_profile().rate / user_vote_step, 0)

def is_over_voted(user):
    date_delta = datetime.now() - timedelta(days=1)
    vote_count = Vote.objects.filter(voting_user=user, time__gte=date_delta).count()
    return vote_count >= get_user_vote_limit(user)

def process_vote(request, post_id, comment_id, val):
    """
    Process vote for comment of post
    raise simple Exception on any error
    """
    if not request.user.is_authenticated():
        raise Exception

    # Objects selection for comment or post voting
    post_data = Post.objects.get(id=int(post_id))
    if comment_id != 'null':
        comment_data = Comment.objects.get(id=int(comment_id))
        target_user = comment_data.author
    else:
        comment_data = None
        target_user = post_data.author

    # Vote processing
    if val == 'cancel_up' or val == 'cancel_down':
        Vote.objects.get(voting_user=request.user, post=post_data, comment=comment_data).delete()
    elif val == 'up' or val == 'down':
        if is_over_voted(request.user):
            return simplejson.dumps({'result': 'overvoted'})

        try:
            vote = Vote.objects.get(voting_user=request.user, post=post_data, comment=comment_data)
        except Exception:
            vote = Vote(voting_user=request.user, target_user=target_user, post=post_data, comment=comment_data)
        vote.value = get_vote_val(val)
        vote.save()
    else:
        raise Exception

    # Updating model objects for new rate value
    # Comment or post
    rate = Vote.objects.filter(comment = comment_data, post=post_data).aggregate(Sum('value'))['value__sum']
    if comment_data is None:
        rate = update_rate(post_data, rate)
    else:
        rate = update_rate(comment_data, rate)
    # Rated user
    user_rate = Vote.objects.filter(target_user=target_user).aggregate(Sum('value'))['value__sum']
    update_rate(target_user.get_profile(), user_rate)

    return simplejson.dumps({
        'result': 'true',
        'post_id': post_id,
        'comment_id': comment_id,
        'val': val,
        'rate': rate,
        })

