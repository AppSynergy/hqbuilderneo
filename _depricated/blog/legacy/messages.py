# -*- coding: utf-8 -*-

__author__ = 'dante'

error_required = u'Обязательное поле'
error_invalid = u'Введите корректное значение'
error_invalid_email = u'Введите корректное значение e-mail адресa'
error_passwords_not_match = u'Пароли не совпадают'
error_username_exist = u'Пользователь с таким именем уже существует'
error_email_exist = u'E-mail уже используется'
error_email_not_found = u'E-mail не найден'
error_user_not_found = u'Пользователь не найден'
error_self_mail = u'Вы не можете послать письмо самому себе'
error_access_denied = u'Неверный пароль'
error_max_value = u'Проверьте что данное значение меньше либо равно %(limit_value)s'
error_min_value = u'Проверьте что данное значение больше либо равно %(limit_value)s'
error_date = u'Неверная дата'
error_max_avatar_size = u'Превышен максимальный размер аватара (%d килобайт)'

months = {
    1: u'январь',
    2: u'февраль',
    3: u'март',
    4: u'апрель',
    5: u'май',
    6: u'июнь',
    7: u'июль',
    8: u'август',
    9: u'сентябрь',
    10: u'октябрь',
    11: u'ноябрь',
    12: u'декабрь'}

restore_password_subj = u'Восстановление пароля'
restore_password_msg = u'Здравствуйте\r\nВашему пользователю www.DaBunker.com: %(name)s\r\nбыл установлен новый пароль: %(password)s'

new_comment_subj = u'Новый комментарий'
new_comment_msg = u'В теме "%(title)s" за которой Вы следите www.DaBunker.com/post/%(post_id)d пользователь %(author)s оставил комментарий следующего содержания:\r\n%(text)s'

new_private_massage_subj = u'Новое личное сообщение'
new_private_massage_msg = u'Вашему пользователю: %(username)s, пришло новое личное сообщения от пользователя: %(author)s, следующего содержания:\r\n %(text)s'
