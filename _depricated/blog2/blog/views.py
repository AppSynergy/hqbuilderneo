from blog.tools import process_post, get_data, get_posts

__author__ = 'Denis Romanov'

from flask import Blueprint, render_template, jsonify, request, url_for
from flask.ext.login import current_user
import json

module = Blueprint('blog', __name__, url_prefix='/social')


@module.route('/_update_post', methods=['POST'])
def update_post():
    data = json.loads(request.form.get('data'))
    res = get_data(data)
    if res:
        return jsonify(res)
    if not current_user.is_authenticated():
        return jsonify(redirect=url_for('auth.signin_page'))
    return jsonify(process_post(data, current_user))


@module.route('/')
@module.route('/user/<user>')
@module.route('/tag/<tag>')
@module.route('/post/<post>')
def main(user=None, tag=None, post=None):
    return render_template(
        "blog/main.html",
        posts=get_posts(user=user, tag=tag, post=post),
        post=post, tag=tag, user=user
    )
