__author__ = 'dromanow'

import unittest

from _depricated.core2 import Gear, Observable, Node, Option


class GearTest(unittest.TestCase):
    def test_creating(self):
        gear = Gear(name='Lightning claw')
        self.assertDictEqual({'name': 'Lightning claw', 'count': 1, 'type': None}, gear.get())

        gear = Gear(name='Lightning claw', count=2)
        self.assertDictEqual({'name': 'Lightning claw', 'count': 2, 'type': None}, gear.get())

        try:
            Gear(name='', count=2, gear_type='Wargear')
            self.assert_(False, Gear.err_empty_name)
        except ValueError as e:
            self.assertEqual(e.message, Gear.err_empty_name)

        try:
            Gear(name=None, count=2, gear_type='Wargear')
            self.assert_(False, Gear.err_empty_name)
        except ValueError as e:
            self.assertEqual(e.message, Gear.err_empty_name)

    def test_update(self):
        gear = Gear(name='Lightning claw', count=2, gear_type='Wargear')
        gear.set_count(1)
        self.assertDictEqual({'name': 'Lightning claw', 'count': 1, 'type': 'Wargear'}, gear.get())

    def test_clone(self):
        gear = Gear(name='Lightning claw', count=3, gear_type='Wargear')
        gear1 = gear.clone()
        self.assertDictEqual(gear.get(), gear1.get())

    def test_get_count(self):
        gear = Gear(name='Lightning claw', count=3, gear_type='Wargear')
        self.assertEquals(gear.get_count(), 3)
        gear.set_count(5)
        self.assertEquals(gear.get_count(), 5)

    def test_restore(self):
        data = {'name': 'Lightning claw', 'count': 1, 'type': 'Wargear'}
        gear = Gear.restore(data)
        self.assertDictEqual(data, gear.get())


class ObservableTest(unittest.TestCase):
    def test_creating(self):
        obs = Observable('test', 10)
        self.assertDictEqual({}, obs.get_delta())
        self.assertDictEqual({'test': 10}, obs.dump())
        self.assertEqual(10, obs.get())

    def test_update(self):
        obs = Observable('test', 10)
        obs.set(20)
        self.assertDictEqual({'test': 20}, obs.get_delta())
        self.assertEqual(20, obs.get())
        self.assertDictEqual({'test': 20}, obs.dump())
        obs.set(30)
        self.assertDictEqual({'test': 30}, obs.get_delta())
        self.assertEqual(30, obs.get())
        self.assertDictEqual({'test': 30}, obs.dump())
        obs.flush_delta()
        self.assertDictEqual({}, obs.get_delta())
        self.assertEqual(30, obs.get())
        self.assertDictEqual({'test': 30}, obs.dump())
        self.assertEquals(obs.get_id(), 'test')

    def test_old_value_check(self):
        obs = Observable('test', 10)
        obs.set(20)
        self.assertDictEqual({'test': 20}, obs.get_delta())
        self.assertEqual(20, obs.get())
        obs.set(20)
        self.assertDictEqual({}, obs.get_delta())


class NodeTest(unittest.TestCase):
    class Tester(Node):
        def __init__(self, node_id=None):
            Node.__init__(self, node_id)
            self._obs1 = Observable('obs1', True)
            self._obs2 = Observable('obs2', 10)
            self._obs3 = Observable('obs3', 'some string')

    def setUp(self):
        self.tester = NodeTest.Tester()

    def test_create(self):
        self.assertDictEqual(self.tester.dump(), {
            'id': None,
            'obs1': True,
            'obs2': 10,
            'obs3': 'some string'
        })
        self.assertDictEqual(self.tester.get_delta(), {})

    def test_update(self):
        self.tester._obs1.set(False)
        self.tester._obs3.set('some other string')
        self.assertDictEqual(self.tester.dump(), {
            'id': None,
            'obs1': False,
            'obs2': 10,
            'obs3': 'some other string'
        })
        self.assertDictEqual(self.tester.get_delta(), {
            'obs1': False,
            'obs3': 'some other string'
        })

    def test_create_id(self):
        self.tester = NodeTest.Tester('node_id')
        self.assertDictEqual(self.tester.dump(), {
            'id': 'node_id',
            'obs1': True,
            'obs2': 10,
            'obs3': 'some string'
        })

    def test_auto_flush(self):
        self.tester._obs1.set(False)
        self.tester._obs3.set('some other string')
        self.assertDictEqual(self.tester.get_delta(), {
            'obs1': False,
            'obs3': 'some other string'
        })
        self.assertDictEqual(self.tester.get_delta(), {})

    def test_flush_delta(self):
        self.tester._obs1.set(False)
        self.tester._obs3.set('some other string')
        self.tester.flush_delta()
        self.assertDictEqual(self.tester.get_delta(), {})

    def test_load(self):
        tester = NodeTest.Tester('node_base_id')
        data = {
            'id': 'node_id',
            'obs1': False,
            'obs2': 20,
            'obs3': 'some other string'
        }
        tester.load(data)
        self.assertDictEqual(tester.dump(), data)

    def test_load_id_check(self):
        data = {
            'id': 'node_id',
            'obs1': False,
            'obs2': 20,
            'obs3': 'some other string'
        }
        tester = NodeTest.Tester('node_base_id')

        tester.load(data, skip_id=True)
        self.assertDictEqual(tester.dump(), {
            'id': 'node_base_id',
            'obs1': False,
            'obs2': 20,
            'obs3': 'some other string'
        })

        try:
            tester.load(data, check_id=True)
            self.assert_(False, Node.err_id_not_match)
        except ValueError as e:
            self.assertEquals(e.message, Node.err_id_not_match)

    def test_gen_id(self):
        self.assertEqual(Node.build_id('This is some id'), 'thisissomeid')


class OptionTest(unittest.TestCase):
    def test_create(self):
        o = Option(opt_id='some_id')
        self.assertDictEqual(o.dump(), {'enabled': True, 'id': 'some_id', 'visible': True})
        self.assertDictEqual(o.get_delta(), {})

        o = Option(opt_id='some_id', enabled=False)
        self.assertDictEqual(o.dump(), {'enabled': False, 'id': 'some_id', 'visible': True})
        self.assertDictEqual(o.get_delta(), {})

        o = Option(opt_id='some_id', visible=False)
        self.assertDictEqual(o.dump(), {'enabled': True, 'id': 'some_id', 'visible': False})
        self.assertDictEqual(o.get_delta(), {})

    def test_update(self):
        o = Option(opt_id='some_id')
        o.set_enabled(False)
        self.assertDictEqual(o.dump(), {'enabled': False, 'id': 'some_id', 'visible': True})
        self.assertEqual(o.get_enabled(), False)
        o.set_visible(False)
        self.assertDictEqual(o.dump(), {'enabled': False, 'id': 'some_id', 'visible': False})
        self.assertEqual(o.get_visible(), False)

    def test_delta(self):
        o = Option(opt_id='some_id')
        o.set_enabled(False)
        self.assertDictEqual(o.get_delta(), {'enabled': False})
        o.set_visible(False)
        self.assertDictEqual(o.get_delta(), {'visible': False})
        self.assertDictEqual(o.get_delta(), {})

    def test_flush(self):
        o = Option(opt_id='some_id')
        o.set_enabled(False)
        o.set_visible(False)
        o.flush_delta()
        self.assertDictEqual(o.get_delta(), {})

    def test_used(self):
        o = Option(opt_id='some_id')
        self.assert_(o.is_used())

        o.set_enabled(False)
        self.assert_(not o.is_used())

        o = Option(opt_id='some_id')
        o.set_visible(False)
        self.assert_(not o.is_used())

        o = Option(opt_id='some_id')
        o.set_visible(False)
        o.set_enabled(False)
        self.assert_(not o.is_used())


class VariantTest(unittest.TestCase):
    def test_create(self):
        pass


#
#     def __init__(self):


# class VariantTest(NodeTest):
#     def test_creating(self):
#         NodeTest.test_creating(self)
#         try:
#             Variant(None)
#             self.assert_(False, Variant.err_empty_name)
#         except ValueError as e:
#             self.assertEqual(e.message, Variant.err_empty_name)
#
#         var = Variant('Pair of Lightning claw')
#         res = {'enabled': True, 'name': 'Pair of Lightning claw', 'pt': None, 'visible': True}
#         self.assertDictContainsSubset(res, var.dump())
#
#         var.set_visible(False)
#         res['visible'] = False
#         self.assertDictContainsSubset(res, var.dump())
#
#         var.set_enable(False)
#         res['enabled'] = False
#         self.assertDictContainsSubset(res, var.dump())
#
#         var.set_name('Pair of Lightning claws')
#         res['name'] = 'Pair of Lightning claws'
#         self.assertDictContainsSubset(res, var.dump())
#
#         var.set_pt(30)
#         res['pt'] = 30
#         self.assertDictContainsSubset(res, var.dump())
#
#         var = Variant(name='Pair of Lightning claw')
#         self.assertEquals(var.get_points(), 0)
#
#         var = Variant(name='Pair of Lightning claw', pt=10)
#         self.assertEquals(var.get_points(), 10)
#
#         try:
#             var.set_name('')
#             self.assert_(False, Variant.err_empty_name)
#         except ValueError as e:
#             self.assertEqual(e.message, Variant.err_empty_name)
#
#         try:
#             var.set_name(None)
#             self.assert_(False, Variant.err_empty_name)
#         except ValueError as e:
#             self.assertEqual(e.message, Variant.err_empty_name)
#
#     def test_gear(self):
#         var = Variant('Pair of Lightning claw', pt=30, gear=Gear(name='Lightning claw', count=2))
#         res = {'visible': True, 'gear': {'count': 2, 'type': None, 'name': 'Lightning claw'}, 'pt': 30,
#                'enabled': True, 'id': 'pairoflightningclaw', 'name': 'Pair of Lightning claw'}
#         self.assertDictEqual(res, var.dump())
#
#         var.set_gear(None)
#         res['gear'] = None
#         self.assertDictEqual(res, var.dump())
#         self.assertDictEqual(var.get_gear().get(), {'count': 1, 'type': None, 'name': 'Pair of Lightning claw'})
#
#         gear = Gear(name='Power fist', count=2)
#         var.set_gear(gear)
#         res['gear'] = gear.get()
#         self.assertDictEqual(res, var.dump())
#         self.assertDictEqual(gear.get(), var.get_gear().get())
#
#     def test_load(self):
#         var = Variant('test var')
#         res = {'visible': False, 'gear': {'count': 2, 'type': 'Wargear', 'name': 'Lightning claw'}, 'pt': 30,
#                'enabled': False, 'id': 'pairoflightningclaw', 'name': 'Pair of Lightning claw'}
#         var.load(res)
#         self.assertDictEqual(res, var.dump())
#
#         var = Variant('test var')
#         res = {'visible': False, 'gear': {'count': 2, 'type': 'Wargear', 'name': 'Lightning claw'}, 'pt': 30,
#                'enabled': False, 'id': 'pairoflightningclaw', 'name': 'Pair of Lightning claw'}
#         old_id = var.dump()['id']
#         var.load(res, skip_id=True)
#         res['id'] = old_id
#         self.assertDictEqual(res, var.dump())


# class CountTest(NodeTest):
#     def test_count(self):
#         NodeTest.test_creating(self)
#         count = Count('Veterans', 1, 10, 100)
#         self.assertDictEqual({'cur': 1, 'pt': 100, 'min': 1, 'max': 10, 'enabled': True, 'visible': True,
#                               'gear': None, 'type': 'count', 'id': 'veterans', 'name': 'Veterans'},
#                              count.dump())
#
#         count.set(5)
#         self.assertDictEqual({'cur': 5, 'pt': 100, 'min': 1, 'max': 10, 'enabled': True, 'visible': True,
#                               'gear': None, 'type': 'count', 'id': 'veterans', 'name': 'Veterans'},
#                              count.dump())
#         self.assertEquals(count.get_min(), 1)
#         self.assertEquals(count.get_max(), 10)
#         self.assertEquals(count.get(), 5)
#         self.assertEquals(count.get_points(), 500)
#         self.assertDictEqual(count.get_delta(), {'cur': 5})
#
#         count.set_min(6)
#         self.assertDictEqual({'cur': 6, 'pt': 100, 'min': 6, 'max': 10, 'enabled': True, 'visible': True,
#                               'gear': None, 'type': 'count', 'id': 'veterans', 'name': 'Veterans'},
#                              count.dump())
#         self.assertEquals(count.get(), 6)
#         self.assertEquals(count.get_points(), 600)
#         self.assertDictEqual(count.get_delta(), {'cur': 6, 'min': 6})
#
#         count.set_min(1)
#         count.set_max(5)
#         self.assertDictEqual({'cur': 5, 'pt': 100, 'min': 1, 'max': 5, 'enabled': True, 'visible': True,
#                               'gear': None, 'type': 'count', 'id': 'veterans', 'name': 'Veterans'},
#                              count.dump())
#         self.assertEquals(count.get(), 5)
#         self.assertEquals(count.get_points(), 500)
#         self.assertDictEqual(count.get_delta(), {'max': 5, 'cur': 5, 'min': 1})
#
#         count.set_min(1)
#         count.set_max(10)
#         count.flush_delta()
#         self.assertDictEqual({'cur': 5, 'pt': 100, 'min': 1, 'max': 10, 'enabled': True, 'visible': True,
#                               'gear': None, 'type': 'count', 'id': 'veterans', 'name': 'Veterans'},
#                              count.dump())
#         self.assertDictEqual(count.get_delta(), {})
#         count.set_range(2, 4)
#         self.assertDictEqual({'cur': 4, 'pt': 100, 'min': 2, 'max': 4, 'enabled': True, 'visible': True,
#                               'gear': None, 'type': 'count', 'id': 'veterans', 'name': 'Veterans'},
#                              count.dump())
#         self.assertDictEqual(count.get_delta(), {'max': 4, 'cur': 4, 'min': 2})
#         count.set_range(5, 10)
#         self.assertDictEqual({'cur': 5, 'pt': 100, 'min': 5, 'max': 10, 'enabled': True, 'visible': True,
#                               'gear': None, 'type': 'count', 'id': 'veterans', 'name': 'Veterans'},
#                              count.dump())
#         self.assertDictEqual(count.get_delta(), {'cur': 5, 'min': 5, 'max': 10})
#         self.assertDictEqual(count.get_delta(), {})
#
#         count.on_update({'value': 7})
#         self.assertEquals(count.get(), 7)
#
#         count.on_update({'value': 1})
#         self.assertEquals(count.get(), 5)
#
#         count.on_update({'value': 12})
#         self.assertEquals(count.get(), 10)
#
#     def test_gear(self):
#         count = Count('Veterans', 1, 10, 100)
#         self.assertDictEqual({'count': 1, 'type': None, 'name': 'Veterans'}, count.get_gear().get())
#         count.set(5)
#         self.assertDictEqual({'count': 5, 'type': None, 'name': 'Veterans'}, count.get_gear().get())
#
#         count = Count('Pair of Lightning claw', count_min=1, count_max=10, pt=100,
#                       gear=Gear(name='Lightning claw', count=2, gear_type='Wargear'))
#         self.assertDictEqual({'count': 2, 'name': 'Lightning claw', 'type': 'Wargear'},
#                              count.get_gear().get())
#         count.set(5)
#         self.assertDictEqual({'count': 10, 'name': 'Lightning claw', 'type': 'Wargear'},
#                              count.get_gear().get())
#
#     def test_unused(self):
#         count = Count('Veterans', 1, 10, 100)
#         count.set(5)
#         self.assertEquals(count.get_points(), 500)
#         count.set_enable(False)
#         self.assertEquals(count.get_points(), 0)
#         count.set_enable(True)
#         self.assertEquals(count.get_points(), 500)
#         count.set_visible(False)
#         self.assertEquals(count.get_points(), 0)
#         count.set_visible(True)
#         self.assertEquals(count.get_points(), 500)
#
#     def test_load(self):
#         count = Count('Veteran', count_min=1, count_max=10, pt=100)
#         res = {'visible': True, 'pt': 10, 'type': 'count', 'cur': 5, 'name': 'Veteran', 'min': 3, 'max': 7,
#                'enabled': True, 'id': 'veteran', 'gear': {'count': 10, 'name': 'Lightning claw', 'type': 'Wargear'}}
#         count.load(res)
#         self.assertDictEqual(res, count.dump())
#
#         count = Count('Veteran', count_min=1, count_max=10, pt=100)
#         old_id = res['id']
#         res['id'] = 'some_id'
#         count.load(res, skip_id=True)
#         res['id'] = old_id
#         self.assertDictEqual(res, count.dump())

# gear_suite = unittest.TestSuite.(GearTest)
# gear_suite.addTest(GearTest)

if __name__ == "__main__":
    # gear_suite.run()
    unittest.main()
