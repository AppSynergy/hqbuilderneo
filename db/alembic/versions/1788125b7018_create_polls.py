"""create polls

Revision ID: 1788125b7018
Revises: 2824fef8e347
Create Date: 2015-10-03 23:16:06.438154

"""

# revision identifiers, used by Alembic.
revision = '1788125b7018'
down_revision = '2824fef8e347'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'polls',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('post', sa.Integer(), nullable=False),
        sa.Column('limit', sa.Integer(), nullable=False),
        sa.Column('deadline', sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(['post'], ['posts.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index('ix_polls_post', 'polls', ['post'], unique=False)
    op.create_table(
        'pollvariants',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('poll', sa.Integer(), nullable=False),
        sa.Column('text', sa.String(128), nullable=False),
        sa.Column('hits', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['poll'], ['polls.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index('ix_pollvariants_poll', 'pollvariants', ['poll'], unique=False)
    op.create_table(
        'pollvotes',
        sa.Column('poll', sa.Integer(), nullable=False),
        sa.Column('variant', sa.Integer(), nullable=True),
        sa.Column('user', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['poll'], ['polls.id'], ),
        sa.ForeignKeyConstraint(['variant'], ['pollvariants.id'], ),
        sa.ForeignKeyConstraint(['user'], ['users.id'], ),
        sa.PrimaryKeyConstraint('poll', 'user', name='pollvotes_pk')
    )
    op.create_index('ix_pollvotes_poll', 'pollvotes', ['poll'], unique=False)
    op.create_index('ix_pollvotes_variant', 'pollvotes', ['variant'], unique=False)


def downgrade():
    # op.drop_index('ix_pollvotes_variant', 'pollvotes')
    # op.drop_index('ix_pollvotes_poll', 'pollvotes')
    # op.drop_table('pollvotes')
    op.drop_index('ix_pollvariants_poll', 'pollvariants')
    op.drop_table('pollvariants')
    op.drop_index('ix_polls_post', 'polls')
    op.drop_table('polls')
