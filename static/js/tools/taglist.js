/**
 * Created by dromanow on 5/22/14.
 */

var TagObj = function(name, listCallback) {
    var self = this;
    self.selected = ko.observable(false);
    self.name = name;
    self.count = 1;
    self.click = function() {
        listCallback(self);
    }
};

var TagListControl = function(taggedObjects, changedCallback) {
    this.tags = ko.observableArray([]);
    this.selectedTag = ko.observable(null);
    this.changedCallback = changedCallback;
    this.reset(taggedObjects);
};

TagListControl.prototype.reset = function(taggedObjects) {
    var self = this;
    this.tagsMap = {};
    taggedObjects.forEach(function(o) {
        o.tagsObj = ko.observable(o.tags().map(function(tag) {
            return self.getTag(tag);
        }));
        o.visible = ko.observable(true);
    });
    this.taggedObjects = taggedObjects;
    this.selectedTag(null);
    this.postProcess(taggedObjects.length);
};

TagListControl.prototype.isAnySelected = function() {
    return this.selectedTag() != null;
};

TagListControl.prototype.tagSelected = function(tag) {
    var self = this;
    if (this.selectedTag() && this.selectedTag().name == tag.name) {
        this.selectedTag().selected(false);
        this.selectedTag(null);
    }
    else if (this.selectedTag()) {
        this.selectedTag().selected(false);
        this.selectedTag(tag);
        tag.selected(true);
    }
    else {
        this.selectedTag(tag);
        tag.selected(true);
    }
    $.each(this.taggedObjects, function(n, r) {
        r.visible(!self.isAnySelected() || r.tagsObj().some(function(tag) { return tag.selected(); }));
    });
    if (this.changedCallback)
        this.changedCallback();
};

TagListControl.prototype.getTag = function(tagName) {
    var self = this;
    if (this.tagsMap[tagName] === undefined)
        this.tagsMap[tagName] = new TagObj(tagName, function(t) { self.tagSelected(t); });
    else
        this.tagsMap[tagName].count++;
    return this.tagsMap[tagName];
};

TagListControl.prototype.postProcess = function(rosterCount) {
    var tags = [];
    $.each(this.tagsMap, function(tag, tagObj) {
        if (tagObj.count < rosterCount)
            tags.push(tagObj)
    });
    tags.sort(function(a, b) { return  b.name > a.name ? -1 : b.name < a.name ? 1 : 0; });
    this.tags(tags);
};
