/**
 * Created by dromanow on 5/16/14.
 */

if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (obj, fromIndex) {
    if (fromIndex == null) {
        fromIndex = 0;
    } else if (fromIndex < 0) {
        fromIndex = Math.max(0, this.length + fromIndex);
    }
    for (var i = fromIndex, j = this.length; i < j; i++) {
        if (this[i] === obj)
            return i;
    }
    return -1;
  };
}


if (!Array.prototype.include) {
    Array.prototype.include = function (obj) {
        return (this.indexOf(obj) != -1);
    };
}
