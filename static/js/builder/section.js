/**
 * Created by dromanow on 4/29/14.
 */

// UnitType

var UnitType = function(parent, data) {
    this.setupNode(parent, data);
    this.name = data.name;
    this.tag = data.tag;
};

UnitType.prototype = new Node();


// TypeSelector

var TypeSelector = function(section, data) {
    var self = this;
    this.visible = ko.observable(true);
    this.section = section;
    this.options = ko.observableArray(data.types.map(function(type) {
        return new UnitType(self, type);
    }));
    this.cur = ko.observable(this.getFirst());
    this.updater = function(){};
};

TypeSelector.prototype.getFirst = function() {
    for(var i = 0; i < this.options().length; i++)
        if (this.options()[i].visible() && this.options()[i].active())
        {
            this.visible(true);
            return this.options()[i];
        }
    this.visible(false);
    return this.options()[0];
};

TypeSelector.prototype.pushChanges = function(types) {
    var self = this;
    $.each(types, function(n, type_data) {
        $.each(self.options(), function(n, type) {
            if (type.id == type_data.id)
                type.genericPush(type_data);
        });
    });
    if (!this.visible() || !this.cur().visible() || !this.cur().active())
        this.cur(this.getFirst());
    this.updater();
};

TypeSelector.prototype.setOptionDisable = function(option, item) {
    ko.applyBindingsToNode(option, {enable: item.active, visible: item.visible}, item);
    if(item.tag)
    {
        var grp =$(option).parent("select").find("[label=\""+item.tag+"\"]");
        if(grp.length==0)
        {
            grp = $("<optGroup></optGroup>");
            grp.attr("label",item.tag);
            $(option).parent("select").append(grp);
        }
        $(option).attr("data-subtext", item.tag);
        grp.append($(option));
    }
    else
    {
        var grp =$(option).parent("select").find("[label]:first");
        if(grp.length !== 0)
        {
            $(option).detach().insertBefore(grp.get(0));
        }
    }
};

TypeSelector.prototype.add = function() {
    this.section.addUnit({id: this.cur().id});
};

ko.bindingHandlers.pickerinit = {
    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        // This will be called when the binding is first applied to an element
        // Set up any initial state, event handlers, etc. here
        $(element).selectpicker('refresh');
        bindingContext.$data.updater = function() {
            $(element).selectpicker('refresh');
        };
    }
};

//SectionLimit

var SectionLimit = function(section) {
    this.active = ko.observable(false);
    this.section = section;
    this.min = ko.observable(0);
    this.min.key = digitFilter;
    this.max = ko.observable(0);
    this.max.key = digitFilter;
};

SectionLimit.prototype.ok = function() {
    this.section.updateLimits(parseInt(this.min()), parseInt(this.max()));
    this.active(false);
};

SectionLimit.prototype.cancel = function() {
    this.active(false);
};

SectionLimit.prototype.show = function(min, max) {
    if (!this.active()) {
        this.active(true);
        this.min(min);
        this.max(max);
    }
};


// Section

var Section = function(section) {
    var self = this;
    self.setupNode(section.parent, section.data);
    this.name = ko.observable(section.data.name);
    this.min = ko.observable(section.data.min);
    this.max = ko.observable(section.data.max);
    this.range = new SectionLimit(this);
    this.types = new TypeSelector(this, section.data);
    this.units = ko.observableArray(section.data.units.map(function(unit) {
        return new Unit({parent: self, data: unit});
    }));
};

Section.prototype = new Node();

Section.prototype.updateUnit = function(obj) {
    this.parent.updateSection({id: this.id, units: [obj]})
};

Section.prototype.deleteUnit = function(obj) {
    this.parent.updateSection({id: this.id, deleted_units: [obj]})
};

Section.prototype.addUnit = function(obj) {
    this.parent.updateSection({id: this.id, added_units: [obj]})
};

Section.prototype.cloneUnit = function(obj) {
    this.parent.updateSection({id: this.id, clone_unit: obj})
};

Section.prototype.clear = function() {
    var self = this;
    document.modalInfo.warning("Clear " + this.name() + " section? Are you sure?", function() {
        self.parent.updateSection({id: self.id, clear: true})
    });
};

Section.prototype.setupClick = function() {
    this.range.show(this.min(), this.max());
};


Section.prototype.updateLimits = function(min, max) {
    this.parent.updateSection({
        id: this.id,
        min: min,
        max: max
    });
};

Section.prototype.pushChanges = function(data) {
    var self = this;
    self.genericPush(data);
    if (data.added_units)
        $.each(data.added_units, function(n, unit) {
            if (typeof unit.pos == "number") {
                self.units.splice(unit.pos, 0, new Unit({parent: self, data: unit}));
            }
            else
                self.units.push(new Unit({parent: self, data: unit}));
        });
    if (data.deleted_units)
        self.units.remove(function(unit) {
            return data.deleted_units.reduce(function (val, unit_data) {
                return val || unit.id == unit_data.id;
            }, false);
        });
    if (data.units)
        $.each(data.units, function(n, unit_data) {
            $.each(self.units(), function(n, unit) {
                if (unit.id == unit_data.id)
                    unit.pushChanges(unit_data);
            });
        });
    if (data.types)
        self.types.pushChanges(data.types);
    if ('min' in data)
        self.min(data.min);
    if ('max' in data)
        self.max(data.max);
};

// Formation

var Formation = function(formation) {
    var self = this;
    self.setupNode(formation.parent, formation.data);
    this.name = ko.observable(formation.data.roster.name);
    this.type = 'formation'
    this.settings = new RosterSettings(new OptionsBlock(self, formation.data.roster.options));
    this.options_description = ko.observable(formation.data.roster.options_description);
    this.types = new TypeSelector(this, formation.data.roster);
    this.errors = ko.observableArray([]);
    this.errorsCount = ko.computed(function() {return self.errors().length; });
    this.units = ko.observableArray(formation.data.roster.units.map(function(unit) {
        return new Unit({parent: self, data: unit});
    }));
    this.updateErrors(formation.data.roster);
};

Formation.prototype = new Node();

Formation.prototype.update = function(obj) {
    this.parent.update({id: this.id, roster: {options: [obj]}})
};

Formation.prototype.updateErrors = function(data) {
    this.errors(data.errors ? data.errors : []);
};

Formation.prototype.updateUnit = function(obj) {
    this.parent.update({id: this.id, roster: {units: [obj]}})
};

Formation.prototype.deleteUnit = function(obj) {
    this.parent.update({id: this.id, roster: {deleted_units: [obj]}})
};

Formation.prototype.addUnit = function(obj) {
    this.parent.update({id: this.id, roster: {added_units: [obj]}})
};

Formation.prototype.cloneUnit = function(obj) {
    this.parent.update({id: this.id, roster: {clone_unit: obj}})
};

Formation.prototype.clear = function() {
    var self = this;
    document.modalInfo.warning("Clear " + this.name() + " formation? Are you sure?", function() {
        self.parent.updateSection({id: self.id, clear: true})
    });
};

Formation.prototype.pushChanges = function(data) {
    var self = this;
    self.genericPush(data);
    if (data.roster.added_units)
        $.each(data.roster.added_units, function(n, unit) {
            if (typeof unit.pos == "number") {
                self.units.splice(unit.pos, 0, new Unit({parent: self, data: unit}));
            }
            else
                self.units.push(new Unit({parent: self, data: unit}));
        });
    if (data.roster.deleted_units)
        self.units.remove(function(unit) {
            return data.roster.deleted_units.reduce(function (val, unit_data) {
                return val || unit.id == unit_data.id;
            }, false);
        });
    if (data.roster.units)
        $.each(data.roster.units, function(n, unit_data) {
            $.each(self.units(), function(n, unit) {
                if (unit.id == unit_data.id)
                    unit.pushChanges(unit_data);
            });
        });
    if (data.roster.types)
        self.types.pushChanges(data.roster.types);
    if (data.roster.options)
    {
        self.settings.block.pushChanges(data.roster.options);
        if (data.roster.options_description)
        {
            self.options_description(data.roster.options_description);
        }
    }
};

