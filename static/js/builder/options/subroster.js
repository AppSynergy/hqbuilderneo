/**
 * Created by dromanow on 4/25/14.
 */


var SubRoster = function(opt) {
    this.type = 'sub_roster';
    this.setupNode(opt.parent, opt.data);
    this.name = opt.data.name;
    this.roster = new RosterData(this, opt.data.roster);
};

SubRoster.prototype = new Node();

SubRoster.prototype.update = function(obj) {
    this.parent.update({id: this.id, roster: obj})
};

SubRoster.prototype.pushChanges = function(data) {
    this.genericPush(data);
    if (data.roster && data.roster.id == this.roster.id)
        this.roster.pushChanges(data.roster);
};
