/**
 * Created by dromanow on 4/25/14.
 */


var GenericCount = function(opt) {
    var self = this;
    this.setupNode(opt.parent, opt.data);
    this.type = 'generic_count';

    this.section = opt.data.name;
    this.min = ko.observable(opt.data.min);
    this.max = ko.observable(opt.data.max);
    this.options = ko.observableArray([]);
    this.cur = ko.observable(opt.data.cur);
    this.help = ko.observable(opt.data.help);
    this.disabled = ko.observable(false);

    this._updateRange();

    this.cur.subscribe(function(o) {
        self.parent.update({id: self.id, value: self.cur()});
    });
};

GenericCount.prototype = new Node();

GenericCount.prototype._genArray = function(f, l) {
    var res = [];
    for (var i = f; i <= l; i++)
        res.push(i);
    return res;
};

GenericCount.prototype._updateRange = function () {
    if (this.min() > this.cur())
        this.cur(this.min());
    if (this.max() < this.cur())
        this.cur(this.max());
    this.options(this._genArray(this.min(), this.max()));
    this.disabled(this.min() == this.max());
};

GenericCount.prototype.pushChanges = function(data) {
    var self = this;
    this.genericPush(data);
    $.each(data, function(option_id, val) {
        if($.inArray(option_id, ['min', 'max', 'cur']) != -1) {
            self[option_id](val);
            self._updateRange();
        }
    });
};

var SkipCount = function(opt) {
    GenericCount.call(this, opt);
    // for now, count is kept the same as a regular count
};

SkipCount.prototype = new Node();

SkipCount.prototype._genArray = function(f, l) {
    var res = [];
    for (var i = f; i <= l; i=i+f)
        res.push(i);
    return res;
};

SkipCount.prototype._updateRange = GenericCount.prototype._updateRange;

SkipCount.prototype.pushChanges = GenericCount.prototype.pushChanges;
