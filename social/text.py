# -*- coding: utf-8 -*-
# from jinja2 import Markup
# from markdown import markdown

__author__ = 'dante'


read_more_code = '[read more]'


def get_read_more_head(text):
    pos = text.find(read_more_code)
    if pos == -1:
        return text, False
    return text[0:pos], True


def process_read_more_marker(text):
    return text.replace(read_more_code, '\n\n')


def normalize_line_breaks(text):
    new_text = []
    for line in text.splitlines(False):
        if line and not line.isspace():
            if line.startswith('---') or line.startswith('==='):
                new_text[-1] = new_text[-1] + '\r\n' + line
            else:
                new_text.append(line)
    return '\r\n\r\n'.join(new_text)
#
#
# def render_text(text):
#     return Markup(markdown(auto_mark_links(normalize_line_breaks(text))))
