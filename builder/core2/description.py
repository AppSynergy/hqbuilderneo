__author__ = 'dante'


class Gear(object):
    def __init__(self, name, subtype=None, count=1):
        super(Gear, self).__init__()
        self.name = name
        self.subtype = subtype
        self.count = count

    def dump(self):
        return {
            'name': self.name,
            'subtype': self.subtype,
            'count': self.count
        }

    def clone(self):
        return Gear(name=self.name, subtype=self.subtype, count=self.count)


class UnitDescription():
    def __init__(self, name='', points=0, count=1, options=None):
        self.name = name
        self.points = points
        self.count = count
        self.options = []
        self.sub_units = []
        if options:
            self.add(options)

    def dump(self):
        return {
            'name': self.name,
            'points': self.points,
            'count': self.count,
            'options': [o.dump() for o in self.options],
            'sub_units': [o.dump() for o in self.sub_units]
        }

    def are_same(self, other):
        """
        Compare 2 UnitDescriptions
        it is presumed they both only have Gear in their options
        Gear instances must have the same order and count (and name, of course)
        """
        if self.name == other.name:
            if self.points == other.points:
                if len(self.options) == len(other.options):
                    for ga, gb in zip(self.options, other.options):
                        if not (ga.name == gb.name and ga.count == gb.count):
                            return False
                    return True
        return False

    def add_dup(self, option):
        """
        Add possibly duplicate UnitDescription
        """
        if isinstance(option, list):
            for o in option:
                self.add_dup(o)
            return
        if not isinstance(option, UnitDescription):
            self.add(option)
        try:
            ud = next(opt for opt in self.sub_units
                        if isinstance(opt, UnitDescription) and opt.are_same(option))
            ud.count = ud.count + option.count
        except StopIteration:
            self.add(option)

    def add(self, options):
        if not options:
            return self
        if not isinstance(options, (list, tuple)):
            options = [options]
        for val in options:
            if not val:
                continue
            if isinstance(val, Gear):
                if val.count == 0:
                    continue
                try:
                    gear = next(opt for opt in self.options if opt.name == val.name)
                    gear.count += val.count
                except StopIteration:
                    self.options.append(val.clone())
            elif isinstance(val, UnitDescription):
                if val.count == 0:
                    continue
                self.sub_units.append(val.clone())
            else:
                raise TypeError('Unknown unit description type')
        return self

    def clone(self):
        return UnitDescription(name=self.name, points=self.points, count=self.count,
                               options=self.options + self.sub_units)

    def add_points(self, pts):
        self.points += pts
        return self

    def set_count(self, count):
        self.count = count
        return self
