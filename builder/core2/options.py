__author__ = 'Denis Romanov'

from description import Gear
from node import Node, Observable, IdGenerator
from section import Formation


class Option(Node):
    def __init__(self, parent, node_id=None, active=True, visible=True, used=True, order=0):
        super(Option, self).__init__(parent=parent, node_id=node_id, active=active, visible=visible)
        self._used = Observable(used)
        self.order = order

    @property
    def used(self):
        return self._used.value

    @used.setter
    def used(self, value):
        self._used.value = value

    @property
    def points(self):
        return 0

    @property
    def description(self):
        return None

    @property
    def errors(self):
        return []

    def dump(self):
        res = super(Option, self).dump()
        res.update({
            'used': self.used,
        })
        return res

    def load(self, data):
        super(Option, self).load(data)
        self.used = data.get('used', self.used)


class Variant(Node):
    def __init__(self, parent, name, points=None, node_id=None, per_model=False, active=True, visible=True, gear=None,
                 subtype=None):
        super(Variant, self).__init__(parent=parent, node_id=node_id, active=active, visible=visible)
        self.title = name
        self.per_model = per_model
        self._points = Observable(points)
        self._name = Observable(self.build_name(self.title, points, per_model))

        self._gear = gear if gear is not None else Gear(name, subtype)
        self.parent.append_variant(self)

    @property
    def gear(self):
        return self._gear

    @gear.setter
    def gear(self, val):
        self._gear = val    

    @property
    def points(self):
        return self._points.value

    @points.setter
    def points(self, val):
        self._points.value = val
        self.name = Variant.build_name(self.title, self.points, self.per_model)

    @property
    def name(self):
        return self._name.value

    @name.setter
    def name(self, val):
        self._name.value = val

    @staticmethod
    def build_points_string(points, per_model=False):
        if points is None:
            return ''
        elif points is 0:
            return '(free)'
        elif per_model:
            return '(+{0}pt. per model)'.format(points)
        else:
            return '(+{0}pt.)'.format(points)

    @staticmethod
    def build_name(name, points, per_model=False):
        return '{0} {1}'.format(name, Variant.build_points_string(points, per_model))

    def dump(self):
        res = super(Variant, self).dump()
        res.update({
            'name': self.name,
            'points': self.points,
        })
        return res


class OneOf(Option):

    class Variant(Variant):
        @property
        def active(self):
            return Variant.active.fget(self)

        @active.setter
        def active(self, value):
            Variant.active.fset(self, value)
            self.parent.drop_inactive()

        @property
        def visible(self):
            return Variant.visible.fget(self)

        @visible.setter
        def visible(self, value):
            Variant.visible.fset(self, value)
            self.parent.drop_inactive()

    def __init__(self, parent, name, node_id=None, active=True, visible=True, used=True, order=0):
        super(OneOf, self).__init__(parent=parent, node_id=node_id, active=active, visible=visible, used=used,
                                    order=order)

        self._name = Observable(name)
        self._options = Observable([])
        self._selected = Observable(None)

        self._cur = None
        self.id_gen = IdGenerator()

        self.parent.append_option(self)

    def variant(self, name, points=None, node_id=None, per_model=False, active=True, visible=True, gear=None,
                subtype=None):
        return self.Variant(self, name, points, node_id, per_model, active, visible, gear, subtype)

    @property
    def cur(self):
        return self._cur

    @cur.setter
    def cur(self, value):
        self._cur = value
        self._selected.value = self.cur.id

    @property
    def options(self):
        return self._options.value

    @property
    def name(self):
        return self._name.value

    @name.setter
    def name(self, value):
        self._name.value = value

    def append_variant(self, option):
        if option.id is None:
            option.id = self.id_gen.next()
        self.options.append(option)
        if not self.cur:
            self.cur = self.options[0]

    @property
    def points(self):
        return self.cur.points if self.cur.points is not None else 0

    @property
    def description(self):
        gear = self.cur.gear
        return gear if isinstance(gear, (list, tuple)) else [gear]

    def drop_inactive(self):
        if not self.cur.visible or not self.cur.active:
            self.cur = next((o for o in self.options if o.visible and o.active), self.cur)

    #def set_active_options(self, ids, val):
    #    for id in ids:
    #        next(o for o in self._options.get() if o.id == id).set_active(val)
    #    if not self.cur.is_active():
    #        self.set(next(o for o in self._options.get() if o.is_active()).id)
    #
    #def set_active_all(self, val):
    #    for o in self._options.get():
    #        o.set_active(val)

    def dump(self):
        res = super(OneOf, self).dump()
        res.update({
            'type': 'one_of',
            'name': self.name,
            'selected': self.cur and self.cur.id,
            'options': [v.dump() for v in self.options]
        })
        return res

    def update(self, data):
        idx = data['selected']
        self.cur = next(o for o in self.options if o.id == idx)

    def save(self):
        res = super(OneOf, self).save()
        res.update({
            'selected': self.cur and self.cur.id,
        })
        return res

    def load(self, data):
        super(OneOf, self).load(data)
        self.name = data.get('name', self.name)
        self.cur = next((o for o in self.options if o.id == data.get('selected', None)), self.cur)

    def check_rules(self):
        pass

    def check_rules_chain(self):
        self.check_rules()


class OptionsList(Option):

    class Variant(Variant, Option):
        def __init__(self, parent, name, points=None, node_id=None, per_model=False, active=True, visible=True,
                     gear=None, subtype=None, combined=False, parent_option=None):
            super(OptionsList.Variant, self).__init__(
                parent=parent, name=name, points=points, node_id=node_id, per_model=per_model, active=active,
                visible=visible, gear=gear, subtype=subtype
            )
            self._value = Observable(False)
            self.combined = combined
            self.parent_option = parent_option

        @property
        def value(self):
            return self._value.value

        @value.setter
        def value(self, val):
            self._value.value = val

        @property
        def gear(self):
            if self.parent_option:
                return []
            if not self.combined or isinstance(self._gear, (list, tuple)):
                return self._gear
            subordinate = [opt for opt in self.parent.options if opt.parent_option == self and opt.value and opt.used]
            if len(subordinate):
                gname = '{} with {}'.format(self._gear.name, ', '.join(opt.title for opt in subordinate))
                return Gear(gname, self._gear.subtype, self._gear.count)
            return self._gear

        def check_rules_chain(self):
            super(OptionsList.Variant, self).check_rules_chain()
            if self.parent_option:
                self.used = self.visible = self.parent_option.used and self.parent_option.value

        def dump(self):
            res = super(OptionsList.Variant, self).dump()
            res.update({'value': self.value})
            res.update({'low_level': self.parent_option is not None})
            return res

        def save(self):
            res = super(OptionsList.Variant, self).save()
            res.update({'value': self.value})
            return res

        def load(self, data):
            super(OptionsList.Variant, self).load(data)
            self.value = data.get('value', self.value)

        def update(self, data):
            self.value = data['value']

    def __init__(self, parent, name, node_id=None, active=True, visible=True, used=True, limit=None, points_limit=None,
                 order=0):
        super(OptionsList, self).__init__(parent=parent, node_id=node_id, active=active, visible=visible, used=used,
                                          order=order)

        self._name = Observable(name)
        self._options = Observable([])

        self._points_limit = points_limit
        self._limit = limit
        self.id_gen = IdGenerator()

        parent.append_option(self)

    def variant(self, name, points=None, node_id=None, per_model=False, active=True, visible=True, gear=None,
                subtype=None):
        return self.Variant(self, name, points, node_id, per_model, active, visible, gear, subtype)

    def variants(self, name, points=None, node_id=None, per_model=False, active=True, visible=True, gear=None,
                 subtype=None, var_dicts=[]):
        main_variant = self.Variant(self, name, points, node_id, per_model, active, visible,
                                    gear, subtype, combined=True)
        for _dict in var_dicts:
            self.Variant(self, parent_option=main_variant, **_dict)
        return main_variant

    def append_variant(self, option):
        if option.id is None:
            option.id = self.id_gen.next()
        self.options.append(option)

    @property
    def limit(self):
        return self._limit

    @limit.setter
    def limit(self, val):
        self._limit = val
        self.check_rules()

    @property
    def points_limit(self):
        return self._points_limit

    @points_limit.setter
    def points_limit(self, val):
        self._points_limit = val
        self.check_rules()

    @property
    def options(self):
        return self._options.value

    @property
    def count(self):
        return sum(1 for o in self.options if o.value)

    @property
    def name(self):
        return self._name.value

    @name.setter
    def name(self, value):
        self._name.value = value

    @property
    def points(self):
        return sum(o.points for o in self.options if o.value and o.used and o.points is not None)

    @property
    def description(self):
        desc = []
        for o in self.options:
            if o.value and o.used:
                if isinstance(o.gear, (list, tuple)):
                    desc.extend(o.gear)
                else:
                    desc.append(o.gear)
        return desc

    def dump(self):
        res = super(OptionsList, self).dump()
        res.update({
            'type': 'options_list',
            'name': self.name,
            'options': [v.dump() for v in self.options]
        })
        return res

    def update(self, data):
        for option in self.options:
            for option_data in data.get('options', []):
                if option.id == option_data['id']:
                    option.update(option_data)

    def save(self):
        res = super(OptionsList, self).save()
        res.update({
            'options': [v.save() for v in self.options]
        })
        return res

    def load(self, data):
        super(OptionsList, self).load(data)
        for option_data in data.get('options', []):
            option = next((o for o in self.options if o.id == option_data['id']), None)
            if option:
                option.load(option_data)

    @staticmethod
    def process_limit(options, limit):
        selected = sum(1 for v in options if v.value)
        if selected > limit:
            for v in options:
                v.value = False
            return
        for v in options:
            v.active = v.value or selected < limit

    @staticmethod
    def process_points_limit(options, limit):
        rest = limit - sum(v.points for v in options)
        for v in options:
            v.set_active(v.value or v.points <= rest)

    @staticmethod
    def process_list_points_limit(options, limit):
        limit = limit - sum(o.points for o in options)
        for o in options:
            o.points_limit = limit

    @staticmethod
    def same_options(lists):
        '''
        Returns True if same options are selected in all supplied options lists
        '''
        optchecks = []
        if len(lists) < 2:
            return True
        for oplist in lists:
            for idx, optval in enumerate(opt.value for opt in oplist.options):
                if idx < len(optchecks):
                    optchecks[idx] += optval
                else:
                    optchecks.append(optval)
        return all(v == 0 or v == len(lists) for v in optchecks)

    def process_full_limit(self, options, limit, points_limit):
        selected = sum(1 for v in options if v.value)
        if selected > limit:
            for v in options:
                v.value = False
            return
        rest = points_limit - self.points
        for v in options:
            v.active = v.value or (selected < limit and v.points <= rest)

    @property
    def any(self):
        return any(v.value for v in self.options if v.used)

    @property
    def count(self):
        return sum(v.value for v in self.options if v.used)

    def check_rules(self):
        if self.limit is not None and self.points_limit is None:
            self.process_limit(self.options, self.limit)
        elif self.limit is None and self.points_limit is not None:
            self.process_points_limit(self.options, self.points_limit)
        elif self.limit is not None and self.points_limit is not None:
            self.process_full_limit(self.options, self.limit, self.points_limit)

    def check_rules_chain(self):
        super(OptionsList, self).check_rules_chain()
        self.check_rules()


class Count(Option):

    def __init__(self, parent, name, min_limit, max_limit, points=None, per_model=False, node_id=None, active=True,
                 visible=True, used=True, gear=None, subtype=None, order=0):
        super(Count, self).__init__(parent=parent, node_id=node_id, active=active, visible=visible, used=used,
                                    order=order)
        self.name = name
        self.option_points = points
        self.per_model = per_model
        self.gear = Gear(name, subtype) if gear is None else gear

        self._min = Observable(min_limit)
        self._max = Observable(max_limit)
        self._cur = Observable(min_limit)
        self._help = Observable(Variant.build_points_string(points, per_model))

        parent.append_option(self)

    @property
    def min(self):
        return self._min.value

    @min.setter
    def min(self, value):
        self._min.value = value
        if self.cur < value:
            self.cur = value

    @property
    def max(self):
        return self._max.value

    @max.setter
    def max(self, value):
        self._max.value = value
        if self.cur > value:
            self.cur = value

    @property
    def cur(self):
        return self._cur.value

    @cur.setter
    def cur(self, value):
        if value > self.max:
            self._cur.value = self.max
        elif value < self.min:
            self._cur.value = self.min
        else:
            self._cur.value = value

    @property
    def help(self):
        return self._help.value

    @help.setter
    def help(self, val):
        self._help.value = val

    @property
    def points(self):
        if not self.option_points:
            return 0
        return self.cur * self.option_points

    @points.setter
    def points(self, val):
        self.option_points = val
        self.help = Variant.build_points_string(self.option_points, self.per_model)

    def update(self, data):
        self.cur = data.get('value', self.cur)

    @property
    def description(self):
        if not self.gear:
            return []
        gear = self.gear.clone()
        gear.count *= self.cur
        return [gear]

    def dump(self):
        res = super(Count, self).dump()
        res.update({
            'type': 'count',
            'name': self.name,
            'min': self.min,
            'max': self.max,
            'cur': self.cur,
            'help': self.help,
        })
        return res

    def save(self):
        res = super(Count, self).save()
        res.update({
            'cur': self.cur,
        })
        return res

    def load(self, data):
        super(Count, self).load(data)
        self.min = data.get('min', self.min)
        self.max = data.get('max', self.max)
        cur = data.get('cur', self.cur)
        if cur > self.max:
            self.max = cur
        if cur < self.min:
            self.min = cur
        self.cur = cur

    @staticmethod
    def norm_counts(total_min, total_max, counts, single_min=0):
        if len(counts) == 1:
            counts[0].max, counts[0].min = total_max, total_min
            return
        total = sum((c.cur for c in counts))
        if total < total_min:
            diff = total_min - total
            for c in counts:
                if diff > 0:
                    count_diff = c.max - c.cur
                    rise = min(count_diff, diff)
                    c.cur += rise
                    c.min = c.cur
                    diff -= rise
                if c.min < single_min:
                    c.min = single_min
        elif total > total_max:
            diff = total - total_max
            for c in counts:
                if diff > 0:
                    count_diff = c.cur - c.min
                    rise = min(count_diff, diff)
                    c.cur -= rise
                    c.max = c.cur
                    diff -= rise
                if c.min < single_min:
                    c.min = single_min
        else:
            to_min = total - total_min
            to_max = total_max - total
            for c in counts:
                c.min, c.max = max(c.cur - to_min, single_min), c.cur + to_max

    @staticmethod
    def norm_points(total_points_max, counts):
        cur_total = sum(cnt.points for cnt in counts)
        diff = total_points_max - cur_total
        for cnt in counts:
            room = int(diff / cnt.option_points)
            cnt.max = max(0, cnt.cur + room)


class UnitList(Option):
    def __init__(self, parent, unit_class, min_limit, max_limit, start_value=None, node_id=None, active=True,
                 visible=True, used=True, order=0):
        super(UnitList, self).__init__(parent=parent, node_id=node_id, active=active, visible=visible, used=used,
                                       order=order)
        self.unit_class = unit_class
        self.min = min_limit
        self.max = max_limit
        self._can_add = Observable(True)
        self._can_delete = Observable(False)
        self._can_split = Observable(None)
        self._deleted_unit = Observable('')
        self._new_unit = Observable({})
        self._units = Observable([self._build_unit(start_value)])
        self.parent.append_option(self)

    def _build_unit(self, start_value=None):
        unit = self.unit_class(parent=self)
        #unit.roster = self.parent.roster
        unit.models.max = self.max
        if start_value:
            unit.models.cur = start_value
        return unit

    @property
    def unit(self):
        return self.unit_class    

    @property
    def unit_name(self):
        return self.units[0].name

    @property
    def base_points(self):
        return self.units[0].base_points * self.min

    @property
    def can_add(self):
        return self._can_add.value

    @can_add.setter
    def can_add(self, val):
        self._can_add.value = val

    @property
    def can_delete(self):
        return self._can_delete.value

    @can_delete.setter
    def can_delete(self, val):
        self._can_delete.value = val

    @property
    def can_split(self):
        return self._can_split.value

    @can_split.setter
    def can_split(self, val):
        self._can_split.value = val

    @property
    def deleted_unit(self):
        return self._deleted_unit.value

    @deleted_unit.setter
    def deleted_unit(self, val):
        self._deleted_unit.value = val

    @property
    def new_unit(self):
        return self._new_unit.value

    @new_unit.setter
    def new_unit(self, val):
        self._new_unit.value = val

    @property
    def units(self):
        return self._units.value

    @units.setter
    def units(self, val):
        self._units.value = val

    def dump(self):
        res = super(UnitList, self).dump()
        res.update({
            'type': 'unit_list',
            'id': self.id,
            'can_add': self.can_add,
            'can_delete': self.can_delete,
            'can_split': self.can_split,
            'units': [unit.dump() for unit in self.units],
        })
        return res

    def save(self):
        res = super(UnitList, self).save()
        res.update({
            'units': [unit.save() for unit in self.units],
        })
        return res

    def load(self, data):
        super(UnitList, self).load(data)
        self.units = []
        for unit_data in data.get('units', []):
            unit = self._build_unit()
            unit.load(unit_data)
            self.units.append(unit)

    def add_unit(self):
        if self.count >= self.max:
            return
        unit = self._build_unit()
        unit.check_rules_chain()
        self.units.append(unit)
        self.new_unit = unit.dump()

    def delete_unit(self, unit_id):
        for u in self.units:
            if u.id != unit_id:
                continue
            self.units.remove(u)
            self.deleted_unit = unit_id
        self.update_range(self.min, self.max)

    def split_unit(self, unit_id):
        actual_unit = None
        next_unit = None
        uiter = iter(self.units)
        while True:
            try:
                cur = uiter.next()
                if cur.id == unit_id:
                    actual_unit = cur
                    if actual_unit.models.cur <= 1:
                        raise RuntimeError('Error: can\'t split unit')
                    next_unit = uiter.next()
                    next_unit.models.max += 1
                    next_unit.models.cur += 1
                    next_unit.check_rules_chain()
                    break
            except StopIteration:
                if actual_unit is not None:
                    new_unit = actual_unit.clone()
                    new_unit.models.min = 1
                    new_unit.models.cur = 1
                    new_unit.check_rules_chain()
                    self.units.append(new_unit)
                    self.new_unit = new_unit.dump()
                    break
        if actual_unit is not None:
            actual_unit.models.min -= 1
            actual_unit.models.cur -= 1
            actual_unit.check_rules_chain()
        # for u in self.units:
        #     if u.id != unit_id:
        #         continue
        #     if u.models.cur <= 1:
        #         raise RuntimeError(message='Error: can\'t split unit')
        #     new_unit = u.clone()
        #     new_unit.models.min = 1
        #     new_unit.models.cur = 1
        #     u.models.min -= 1
        #     u.models.cur -= 1
        #     u.check_rules_chain()
        #     new_unit.check_rules_chain()
        #     self.units.append(new_unit)
        #     self.new_unit = new_unit.dump()

    def update(self, data):
        if 'add' in data.keys():
            self.add_unit()
        if 'delete' in data.keys():
            self.delete_unit(data['delete'])
        if 'split' in data.keys():
            self.split_unit(data['split'])
        if 'units' in data.keys():
            for unit_data in data['units']:
                for unit in self.units:
                    if unit.id == unit_data['id']:
                        unit.update(unit_data)

    def update_range(self, min_limit=None, max_limit=None):
        if max_limit and max_limit < len(self.units):
            return
        if min_limit:
            self.min = min_limit
        if max_limit:
            self.max = max_limit
        self.update_actions_status()
        self.check_rules_chain()

    def update_actions_status(self):
        Count.norm_counts(self.min, self.max, [m.models for m in self.units], 1)
        self.can_delete = len(self.units) > 1
        self.can_add = self.count < self.max
        self.can_split = [unit.id for unit in self.units if unit.get_count() > 1]

    def check_rules_chain(self):
        for u in self.units:
            u.errors = []
            u.check_rules_chain()
        self.update_actions_status()

    @property
    def description(self):
        return [u.build_description() for u in self.units]

    @property
    def points(self):
        return sum(u.points * u.get_count() for u in self.units)

    @property
    def errors(self):
        return sum((unit.errors for unit in self.units), [])

    @property
    def count(self):
        return sum(u.get_count() for u in self.units)


class SubUnit(Option):
    def __init__(self, parent, unit, node_id=None, active=True, visible=True, used=True, order=0):
        super(SubUnit, self).__init__(parent=parent, node_id=node_id, active=active, visible=visible, used=used,
                                      order=order)
        self._unit = Observable(unit)
        self.unit.parent = self
        #self.unit.roster = self.parent.roster
        self.parent.append_option(self)

    @property
    def unit_name(self):
        return self.unit.name

    @property
    def base_points(self):
        return self.unit.base_points
    
    @property
    def unit(self):
        return self._unit.value

    @unit.setter
    def unit(self, val):
        self._unit.value = val

    def dump(self):
        res = super(SubUnit, self).dump()
        res.update({
            'type': 'sub_unit',
            'unit': self.unit.dump(),
        })
        return res

    def save(self):
        res = super(SubUnit, self).save()
        res.update({
            'unit': self.unit.save(),
        })
        return res

    def load(self, data):
        super(SubUnit, self).load(data)
        self.unit.load(data.get('unit', self.unit.save()))

    def update(self, data):
        self.unit.update(data['unit'])

    @property
    def description(self):
        return [self.unit.build_description()]

    @property
    def points(self):
        return self.unit.points

    @property
    def errors(self):
        return self.unit.errors

    @property
    def count(self):
        return self.unit.get_count()

    def check_rules_chain(self):
        self.unit.check_rules_chain()


class OptionalSubUnit(Option):

    class OptionsList(OptionsList):
        def __init__(self, parent, name, units, limit, order=0):
            super(OptionalSubUnit.OptionsList, self).__init__(parent=parent, name=name, limit=limit, order=order)
            self.opts = {self.Variant(self, sub_unit.unit_name, sub_unit.base_points): sub_unit
                         for sub_unit in units}

        def check_rules(self):
            super(OptionalSubUnit.OptionsList, self).check_rules()
            for unit in self.opts.itervalues():
                unit.check_rules_chain()
            for opt, unit in self.opts.iteritems():
                unit.visible = unit.used = opt.value and opt.visible

        def set_visible(self, sub_units, value):
            if not isinstance(sub_units, (list, tuple)):
                sub_units = [sub_units]
            for opt, unit in self.opts.iteritems():
                if unit in sub_units:
                    opt.visible = value
                    if not value:
                        opt.value = False
            self.check_rules()

    def get_options_list(self):
        return self.OptionsList

    def __init__(self, parent, name, limit=None, node_id=None, active=True, visible=True, used=True, order=0):
        super(OptionalSubUnit, self).__init__(parent=parent, node_id=node_id, active=active, visible=visible, used=used,
                                              order=order)
        self.name = name
        self.limit = limit
        self.id_generator = IdGenerator()
        self._units = Observable([])
        self._options = Observable(None)

        parent.append_option(self)

    def append_option(self, option):
        if option.id is None:
            option.id = self.id_generator.next()
        self.units.append(option)
        return option

    @property
    def units(self):
        return self._units.value

    @units.setter
    def units(self, val):
        self._units.value = val

    class Appender(object):
        def __init__(self, parent):
            super(OptionalSubUnit.Appender, self).__init__()
            self.parent = parent

        def append_option(self, option):
            option.parent = self.parent
            option.id = 'options'

    @property
    def options(self):
        if not self._options.value:
            self.options = self.get_options_list()(self.Appender(self), self.name, self.units, self.limit,
                                                       order=self.order)
        return self._options.value

    @options.setter
    def options(self, val):
        self._options.value = val

    def dump(self):
        res = super(OptionalSubUnit, self).dump()
        res.update({
            'type': 'optional_sub_unit',
            'options': self.options.dump(),
            'units': [unit.dump() for unit in self.units]
        })
        return res

    def save(self):
        res = super(OptionalSubUnit, self).save()
        res.update({
            'options': self.options.dump(),
            'units': [unit.dump() for unit in self.units]
        })
        return res

    def load(self, data):
        super(OptionalSubUnit, self).load(data)
        self.options.load(data.get('options', self.options.save()))
        for unit_data in data.get('units', []):
            try:
                next(o for o in self.units if o.id == unit_data['id']).load(unit_data)
            except StopIteration:
                print 'StopIteration for OptionalSubUnit.load'

    def update(self, data):
        self.options.update(data.get('options', {}))
        for unit_data in data.get('units', []):
            try:
                next(o for o in self.units if o.id == unit_data['id']).update(unit_data)
            except StopIteration:
                print 'StopIteration for OptionalSubUnit.update'

    @property
    def description(self):
        return sum((unit.description for unit in self.units if unit.used), [])

    @property
    def points(self):
        return sum(unit.points for unit in self.units if unit.used)

    @property
    def errors(self):
        return sum((unit.errors for unit in self.units if unit.used), [])

    @property
    def count(self):
        return sum(unit.count for unit in self.units if unit.used)

    def check_rules_chain(self):
        self.options.check_rules_chain()
        self.check_rules()

    def check_rules(self):
        pass

    @property
    def any(self):
        return self.options.any


class SubRoster(Option):

    def __init__(self, parent, roster, active=True, visible=True, used=True, order=0):
        super(SubRoster, self).__init__(parent=parent, node_id=roster.army_id, active=active, visible=visible,
                                        used=used, order=order)
        self._roster = Observable(roster)
        self.parent.append_option(self)

    @property
    def roster(self):
        return self._roster.value

    @roster.setter
    def roster(self, value):
        self._roster.value = value

    def dump(self):
        res = super(SubRoster, self).dump()
        # default value
        # import pdb; pdb.set_trace()
        contained = 'formation' if issubclass(type(self.roster), Formation) else 'sub_roster'
        res.update({
            'type': contained,
            'roster': self.roster.dump(),
        })
        return res

    def save(self):
        res = super(SubRoster, self).save()
        res.update({
            'roster': self.roster.save(),
        })
        return res

    def load(self, data):
        # import pdb; pdb.set_trace()
        super(SubRoster, self).load(data)
        self.roster.load(data['roster'])

    def update(self, data):
        # import pdb; pdb.set_trace()
        if 'roster' in data.keys():
            self.roster.update(data['roster'])

    @property
    def points(self):
        return self.roster.points

    @property
    def errors(self):
        return self.roster.errors
