from builder.core2.node import IdGenerator

__author__ = 'dromanow'

import unittest


class IdGeneratorTest(unittest.TestCase):
    def test_generator(self):
        gen = IdGenerator()
        self.assert_(gen.next() == '_id_1')
        self.assert_(gen.next() == '_id_2')
        self.assert_(gen.next() == '_id_3')
