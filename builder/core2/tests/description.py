__author__ = 'dante'

import unittest
from builder.core2.description import Gear, UnitDescription


class GearTest(unittest.TestCase):
    def test_create(self):
        gear = Gear(name='Some gear', subtype='Subtype', count=10)
        self.assert_(gear.name == 'Some gear')
        self.assert_(gear.subtype == 'Subtype')
        self.assert_(gear.count == 10)

    def test_build(self):
        gear = Gear(name='Some gear', subtype='Subtype', count=10)
        self.assertDictEqual(gear.dump(), {
            'name': 'Some gear',
            'subtype': 'Subtype',
            'count': 10
        })

    def test_clone(self):
        gear1 = Gear(name='Some gear', subtype='Subtype', count=10)
        gear2 = gear1.clone()
        gear1.name = 'New name'
        self.assertDictEqual(gear1.dump(), {
            'name': 'New name',
            'subtype': 'Subtype',
            'count': 10
        })
        self.assertDictEqual(gear2.dump(), {
            'name': 'Some gear',
            'subtype': 'Subtype',
            'count': 10
        })


class DescriptionTest(unittest.TestCase):
    def test_create(self):
        desc = UnitDescription(name='Some name', points=10, count=2)
        self.assert_(desc.name == 'Some name' and desc.points == 10 and desc.count == 2)

        g1 = Gear('Some gear', count=10)
        g2 = Gear('Some gear 2', count=12)
        desc = UnitDescription(options=g1)
        self.assertDictEqual(desc.options[0].dump(), g1.dump())

        desc = UnitDescription(options=[g1, g2])
        self.assertDictEqual(desc.options[0].dump(), g1.dump())
        self.assertDictEqual(desc.options[1].dump(), g2.dump())

        g1.name = 'New name'
        self.assertDictEqual(desc.options[0].dump(), {
            'name': 'Some gear',
            'subtype': None,
            'count': 10
        })

    def test_add(self):
        g1 = Gear('Some gear', count=10)
        g2 = Gear('Some gear 2', count=12)
        desc = UnitDescription(name='Some name', points=10, count=2, options=g1)

        desc.add(g1)
        self.assertDictEqual(desc.options[0].dump(), {
            'name': 'Some gear',
            'subtype': None,
            'count': 20
        })

        desc = UnitDescription(name='Some name', points=10, count=2, options=g1)

        desc.add([g1, g2])
        self.assertDictEqual(desc.options[0].dump(), {
            'name': 'Some gear',
            'subtype': None,
            'count': 20
        })
        self.assertDictEqual(desc.options[1].dump(), g2.dump())

        desc.add(UnitDescription(name='Some sub name', points=20, count=3, options=g1))
        self.assertDictEqual(desc.sub_units[0].dump(), {
            'name': 'Some sub name',
            'points': 20,
            'count': 3,
            'sub_units': [],
            'options': [{'count': 10, 'name': 'Some gear', 'subtype': None}],
        })

        desc.add([
            UnitDescription(name='Some sub name 2', points=10, count=1, options=g2),
            UnitDescription(name='Some sub name 3', points=50, count=5, options=g2),
        ])
        self.assertDictEqual(desc.sub_units[0].dump(), {
            'name': 'Some sub name',
            'points': 20,
            'count': 3,
            'sub_units': [],
            'options': [{'count': 10, 'name': 'Some gear', 'subtype': None}],
        })
        self.assertDictEqual(desc.sub_units[1].dump(), {
            'name': 'Some sub name 2',
            'points': 10,
            'count': 1,
            'sub_units': [],
            'options': [{'count': 12, 'name': 'Some gear 2', 'subtype': None}],
        })
        self.assertDictEqual(desc.sub_units[2].dump(), {
            'name': 'Some sub name 3',
            'points': 50,
            'count': 5,
            'sub_units': [],
            'options': [{'count': 12, 'name': 'Some gear 2', 'subtype': None}],
        })

        try:
            desc.add(10)
        except TypeError as e:
            self.assert_(e.message == 'Unknown unit description type')

    def test_dump(self):
        g1 = Gear('Some gear', count=10)
        g2 = Gear('Some gear 2', count=12)
        desc = UnitDescription(name='Some name', points=10, count=2, options=g2)
        desc.add(g1)
        self.assertDictEqual(desc.dump(), {
            'count': 2,
            'name': 'Some name',
            'options': [{'count': 12, 'name': 'Some gear 2', 'subtype': None},
                        {'count': 10, 'name': 'Some gear', 'subtype': None}],
            'points': 10,
            'sub_units': []
        })

        desc.add([
            UnitDescription(name='Some sub name 2', points=10, count=1, options=g2),
            UnitDescription(name='Some sub name 3', points=50, count=5, options=g2),
        ])
        orig = {
            'count': 2,
            'name': 'Some name',
            'options': [{'count': 12, 'name': 'Some gear 2', 'subtype': None},
                        {'count': 10, 'name': 'Some gear', 'subtype': None}],
            'points': 10,
            'sub_units': [{
                'name': 'Some sub name 2',
                'points': 10,
                'count': 1,
                'sub_units': [],
                'options': [{'count': 12, 'name': 'Some gear 2', 'subtype': None}],
            }, {
                'name': 'Some sub name 3',
                'points': 50,
                'count': 5,
                'sub_units': [],
                'options': [{'count': 12, 'name': 'Some gear 2', 'subtype': None}],
            }]
        }

        self.assertDictEqual(desc.dump(), orig)

        clone = desc.clone()
        clone.name = 'Cloned name'
        clone.options[0].name = 'Cloned option'
        clone.sub_units[0].name = 'Cloned sub'

        cloned = {
            'count': 2,
            'name': 'Cloned name',
            'options': [{'count': 12, 'name': 'Cloned option', 'subtype': None},
                        {'count': 10, 'name': 'Some gear', 'subtype': None}],
            'points': 10,
            'sub_units': [{
                'name': 'Cloned sub',
                'points': 10,
                'count': 1,
                'sub_units': [],
                'options': [{'count': 12, 'name': 'Some gear 2', 'subtype': None}],
            }, {
                'name': 'Some sub name 3',
                'points': 50,
                'count': 5,
                'sub_units': [],
                'options': [{'count': 12, 'name': 'Some gear 2', 'subtype': None}],
            }]
        }
        self.assertDictEqual(desc.dump(), orig)
        self.assertDictEqual(clone.dump(), cloned)

        desc.add(None)
        desc.add([g1, None, g2])
