from itertools import groupby
from builder.core2 import *
from builder.core2 import OptionsList, Gear, Unit
from builder.core2.roster import Roster
from builder.core2.section import Section

__author__ = 'dante'


class FCGUnit(Unit):

    class FCG(OptionsList):
        def __init__(self, parent, musician_price, musician_name, standard_price, standard_name,
                     champion_price, champion_name):
            super(FCGUnit.FCG, self).__init__(parent, 'Command group')
            self.musician = musician_price is not None and self.variant(name=musician_name, points=musician_price,
                                                                        gear=[])
            self.standard = standard_price is not None and self.variant(name=standard_name, points=standard_price,
                                                                        gear=[])
            self.champion = champion_price is not None and self.variant(name=champion_name, points=champion_price,
                                                                        gear=[])

        @staticmethod
        def _has_model(model_opt):
            return model_opt and model_opt.value

        def has_champion(self):
            return self._has_model(self.champion)

        def has_musician(self):
            return self._has_model(self.musician)

        def has_standard(self):
            return self._has_model(self.standard)

    def __init__(
        self, parent, model_name, model_price, min_models=1, max_models=100,
        gear=None, rank_gear=None,
        musician_price=None, musician_name='Musician',
        standard_price=None, standard_name='Standard bearer',
        # magic_banners=None,
        champion_price=None, champion_name='Champion', champion_gear=None,
        # champion_options=None,
        # options=None, unit_options=None
    ):
        super(FCGUnit, self).__init__(parent)
        self.models = Count(self, model_name, min_models, max_models, points=model_price, per_model=True, gear=[])
        self.fcg = self.FCG(
            self,
            musician_price, musician_name,
            standard_price, standard_name,
            champion_price, champion_name
        )
        self.model_price = model_price
        self.champion_gear = champion_gear or gear or []
        self.rank_gear = rank_gear or gear or []
        self.model_options = []
        self.unit_options = []
        self.magic_banners = []
        self.champion_options = []
        self.champion_magic_options = []

    def build_points(self):
        return super(FCGUnit, self).build_points() + (self.get_count() - 1) * sum(o.points for o in self.model_options)

    def check_rules(self):
        for o in self.champion_options:
            o.visible = o.used = self.fcg.has_champion()
        for o in self.champion_magic_options:
            o.visible = o.used = self.fcg.has_champion()
        for o in self.magic_banners:
            o.visible = o.used = self.fcg.has_standard()

    def build_description(self):
        description = UnitDescription(name=self.name, points=self.points, count=self.count)
        # description = super(FCGUnit, self).build_description()
        model_gear = sum((o.description for o in self.model_options if o.used), [])
        model_price = sum((o.points for o in self.model_options if o.used))
        if self.fcg.has_champion():
            description.add(UnitDescription(
                name=self.fcg.champion.title,
                points=self.fcg.champion.points + self.model_price + model_price +
                sum(o.points for o in self.champion_options + self.champion_magic_options),
                options=self.champion_gear + model_gear +
                sum((o.description for o in self.champion_options + self.champion_magic_options), [])
            ))
        if self.fcg.has_musician():
            description.add(UnitDescription(
                name=self.fcg.musician.title,
                points=self.fcg.musician.points + self.model_price + model_price,
                options=self.rank_gear + model_gear,
            ))
        if self.fcg.has_standard():
            description.add(UnitDescription(
                name=self.fcg.standard.title,
                points=self.fcg.standard.points + self.model_price + model_price + sum(o.points for o in
                                                                                       self.magic_banners),
                options=self.rank_gear + model_gear + sum((o.description for o in self.magic_banners), []),
            ))
        description.add(UnitDescription(
            name=self.models.name,
            points=self.model_price + model_price,
            count=self.models.cur - self.fcg.count,
            options=self.rank_gear + model_gear,
        ))
        for o in self.unit_options:
            if o.used:
                description.add(o.description)
        return description

    def get_count(self):
        return self.models.cur

    def get_unique_gear(self):
        return sum((m.description for m in self.magic_banners + self.champion_magic_options if m.used), [])


class PercentSection(Section):

    def check_limits(self):
        pass

    def get_limits_error(self):
        if self.min is None or self.max is None or not self.roster.limit:
            return
        current_percent = 100.0 * float(self.points) / self.roster.limit
        if self.max < current_percent:
            return 'Section {0} is more then {1}% of total points'.format(self.name, self.max)
        elif self.min > current_percent:
            return 'Section {0} is less then {1}% of total points'.format(self.name, self.min)


class LordsSection(PercentSection):
    def __init__(self, parent, min_limit=0, max_limit=25):
        super(LordsSection, self).__init__(parent, 'lords', 'Lords', min_limit, max_limit)


class HeroesSection(PercentSection):
    def __init__(self, parent, min_limit=0, max_limit=25):
        super(HeroesSection, self).__init__(parent, 'heroes', 'Heroes', min_limit, max_limit)


class CoreSection(PercentSection):
    def __init__(self, parent, min_limit=25, max_limit=100):
        super(CoreSection, self).__init__(parent, 'core', 'Core units', min_limit, max_limit)


class SpecialSection(PercentSection):
    def __init__(self, parent, min_limit=0, max_limit=50):
        super(SpecialSection, self).__init__(parent, 'special', 'Special units', min_limit, max_limit)


class RareSection(PercentSection):
    def __init__(self, parent, min_limit=0, max_limit=25):
        super(RareSection, self).__init__(parent, 'rare', 'Rare units', min_limit, max_limit)


class FantasyRoster(Roster):
    game_name = 'Warhammer Fantasy Battle'
    base_tags = ['Warhammer Fantasy']

    def __init__(self, lords, heroes, core, special, rare):
        self.lords = lords
        self.heroes = heroes
        self.core = core
        self.special = special
        self.rare = rare
        super(FantasyRoster, self).__init__([lords, heroes, core, special, rare], limit=2500)

    def check_limit(self, name, group, limit, mul):
        limit = limit * mul
        if sum((1 for _ in group)) > limit:
            self.error("Army cannot include more then {1} of {0}.".format(name, limit))

    def check_special_limit(self, name, group, limit, mul):
        return self.check_limit(name, group, limit, mul)

    def check_rare_limit(self, name, group, limit, mul):
        return self.check_limit(name, group, limit, mul)

    def check_rules(self):
        if len(self.lords.units) + len(self.heroes.units) < 1:
            self.error("Army must include at least one lord or hero as commander.")
        elif sum((len(lst.units) for lst in [self.lords, self.heroes, self.core, self.special, self.rare])) < 4:
            self.error("Army must include at least 3 units besides commander.")

        mul = 1 if self._limit < 3000 else 2
        for key, group in groupby(self.special.units, lambda unit: unit.type_name):
            self.check_special_limit(key, group, 3, mul)
        for key, group in groupby(self.rare.units, lambda unit: unit.type_name):
            self.check_rare_limit(key, group, 2, mul)


class BSB(OptionsList):
    def __init__(self, parent, flags_cls):
        super(BSB, self).__init__(parent, 'Battle standard')
        self.bsb = self.variant('Battle standard bearer', points=25)
        self.flags = flags_cls(parent)

    def check_rules(self):
        self.flags.visible = self.flags.used = self.bsb.value

    def is_bsb(self):
        return self.bsb.value

    def has_magic_flags(self):
        return self.flags.any

    def get_unique_gear(self):
        if self.is_bsb():
            return self.flags.description + [Gear('Battle standard bearer')]
        return []


class Character(Unit):
    def __init__(self, *args, **kwargs):
        self.magic_limit = kwargs.pop('magic_limit')
        super(Character, self).__init__(*args, **kwargs)
        self.magic = []
        self.bsb = None

    def check_rules(self):
        OptionsList.process_list_points_limit(self.magic, self.magic_limit)
        if self.bsb:
            for m in self.magic:
                m.visible = m.used = not self.bsb.is_bsb() or not self.bsb.has_magic_flags()

    def get_unique_gear(self):
        items = sum((m.description for m in self.magic if m.used), [])
        if self.bsb:
            items += self.bsb.get_unique_gear()
        return items


class Lore(OptionsList):
    def __init__(self, parent):
        super(Lore, self).__init__(parent, 'Lores')
        self.variant('The Lore of Fire'),
        self.variant('The Lore of Beasts'),
        self.variant('The Lore of Metal'),
        self.variant('The Lore of Light'),
        self.variant('The Lore of Life'),
        self.variant('The Lore of Heavens'),
        self.variant('The Lore of Shadow'),
        self.variant('The Lore of Death'),
