__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.legacy_armory import magic_standards, build_magic_levels
from builder.core.options import norm_points
from special import SeekerChariot
from rare import BurningChariot, ExaltedSeekerChariot
from mounts import *


class Skulltaker(Unit):
    name = "Skulltaker"
    base_points = 200
    gear = ['Cloak of Skulls', 'The Slayer Sword', 'Lesser Locus of Abjuration']

    def __init__(self):
        Unit.__init__(self)
        self.ride = self.opt_optional_sub_unit('Mount', [Juggernaut(), BloodThrone(points=120)])

    def get_unique(self):
        return self.name


class Karanak(StaticUnit):
    name = "Karanak"
    base_points = 195
    gear = ['Greater Locus of Fury', 'Brass Collar of Bloody Vengeance']


class Changeling(StaticUnit):
    name = "The Changeling"
    base_points = 170
    gear = ['Hand weapon', "Lesser Locus of Transmogrification", 'Level 1 Wizard', 'The LOre of Tzeentch']


class BlueScribes(StaticUnit):
    name = "The Blue Scribes"
    base_points = 81
    gear = ['Scrolls of Sorcery']


class Epidemius(StaticUnit):
    name = "Epidemius"
    base_points = 200
    gear = ['Lesser Locus of Virulence', 'Plaguesword']


class Masque(StaticUnit):
    name = "The Masque of Slaanesh"
    base_points = 140


class ExFlamer(StaticUnit):
    unique = False
    name = "Exalted Flamer of Tzeentch"
    base_points = 90


class Herald(Unit):
    def __init__(self):
        Unit.__init__(self)
        self.les = self.opt_count("Lesser Gifts", 0, 2, 25)
        self.grt = self.opt_count("Greater Gifts", 0, 1, 50)
        self.gift = [self.les, self.grt]
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', magic_standards, 1)

    def check_rules(self):
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.gift:
            opt.set_active(not self.magban.is_any_selected())
        norm_points(50, self.gift)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return self.magban.get_selected()


class HeraldOfKhorne(Herald):
    name = "Herald of Khorne"
    base_points = 100
    gear = ['Hellblade']

    def __init__(self):
        Herald.__init__(self)
        self.ride = self.opt_optional_sub_unit('Mount', [Juggernaut(), BloodThrone(points=160)])
        self.opt = self.opt_options_list('Options', [
            ['Lesser Locus of Abjuration', 30, 'abj'],
            ['Greater Locus of Fury', 60, 'fury'],
            ['Exalted Locus of Wrath', 75, 'wrath']
        ], 1)


class HeraldOfTzeentch(Herald):
    name = "Herald of Tzeentch"
    base_points = 90
    gear = ['Hand weapon']

    def __init__(self):
        Herald.__init__(self)
        self.ride = self.opt_optional_sub_unit("Mount", [DiskOfTzeentch(), BurningChariot(boss=True, points=70)])
        self.opt = self.opt_options_list('Options', [
            ['Lesser Locus of Transmogrification', 25, 'trmg'],
            ['Greater Locus of Change', 35, 'chng'],
            ['Exalted Locus of Conjuration', 50, 'conj']
        ], 1)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))

        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Metal', 0, 'metal'],
            ['The Lore of Tzeentch', 0, 'tzeentch'],
        ], limit=1)


class HeraldOfNurgle(Herald):
    name = "Herald of Nurgle"
    base_points = 90
    gear = ['Plaguesword']

    def __init__(self):
        Herald.__init__(self)
        self.ride = self.opt_optional_sub_unit("Mount", [PalanquinOfNurgle()])
        self.opt = self.opt_options_list('Options', [
            ['Lesser Locus of Virulence', 40, 'vir'],
            ['Greater Locus of Fecundity', 45, 'fec'],
            ['Exalted Locus of Contagion', 50, 'con']
        ], 1)
        self.mage = self.opt_options_list('Magic level', [['Level 1 Wizard', 35]])
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Death', 0, 'death'],
            ['The Lore of Nurgle', 0, 'nurgle'],
        ], limit=1)

    def check_rules(self):
        self.lores.set_active_options(self.lores.get_all_ids(), self.mage.is_any_selected())
        Herald.check_rules(self)


class HeraldOfSlaanesh(Herald):
    name = "Herald of Slaanesh"
    base_points = 90
    gear = ['Hand weapon']

    def __init__(self):
        Herald.__init__(self)
        self.ride = self.opt_optional_sub_unit("Mount", [SteedOfSlaanesh(), SeekerChariot(boss=True, points=85),
                                                         ExaltedSeekerChariot(boss=True, points=190)])
        self.opt = self.opt_options_list('Options', [
            ['Lesser Locus of Grace', 5, 'grc'],
            ['Greater Locus of Swiftness', 50, 'swf'],
            ['Exalted Locus of Beguilement', 60, 'bgl']
        ], 1)
        self.mage = self.opt_options_list('Magic level', [['Level 1 Wizard', 35]])
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Slaanesh', 0, 'slaanesh'],
        ], limit=1)

    def check_rules(self):
        self.lores.set_active_options(self.lores.get_all_ids(), self.mage.is_any_selected())
        Herald.check_rules(self)
