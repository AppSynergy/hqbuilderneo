__author__ = 'Denis Romanov'

from builder.core.unit import Unit, OptionsList
from builder.games.whfb.unit import FCGUnit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.legacy_armory import magic_standards, filter_items

unit_standards = filter_items(magic_standards, 25)


class SkullCannon(Unit):
    name = 'Skull Cannon of Khorne'
    base_points = 135
    static = True

    def __init__(self):
        Unit.__init__(self)

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Skull Cannon of Khorne', count=1, gear=['Scythes']).build())
        self.description.add(ModelDescriptor(name='Bloodletter', count=2, gear=['Hellblade']).build())


class ExaltedSeekerChariot(Unit):
    name = 'Exalted Seeker Chariot of Slaanesh'
    base_points = 220
    gear = ['Scythes']
    static = True

    def __init__(self, boss=False, points=220):
        Unit.__init__(self)
        self.boss = boss
        self.base_points = points

    def check_rules(self):
        Unit.check_rules(self)
        if not self.boss:
            self.description.add(ModelDescriptor(name='Exalted Alluress', count=1).build())
        self.description.add(ModelDescriptor(name='Daemonette Crew', count=3).build())
        self.description.add(ModelDescriptor(name='Steeds of Slaanesh', count=4).build())


class Hellflayer(Unit):
    name = 'Hellflayer of Slaanesh'
    base_points = 130
    gear = ['Scythes']
    static = True

    def __init__(self, boss=False):
        Unit.__init__(self)
        self.boss = boss

    def check_rules(self):
        Unit.check_rules(self)
        if not self.boss:
            self.description.add(ModelDescriptor(name='Exalted Alluress', count=1).build())
        self.description.add(ModelDescriptor(name='Daemonette Crew', count=2).build())
        self.description.add(ModelDescriptor(name='Steeds of Slaanesh', count=2).build())


class BurningChariot(Unit):
    name = 'Burning Chariot of Tzeentch'
    base_points = 150
    gear = ['Scythes']

    def __init__(self, boss=False, points=150):
        Unit.__init__(self)
        self.boss = boss
        self.base_points = points
        self.opt_options_list('Options', [['Blue Horror Crew', 20, 'bhc']])

    def check_rules(self):
        Unit.check_rules(self)
        if not self.boss:
            self.description.add(ModelDescriptor(name='Exalted Flamer', count=1).build())
        self.description.add(ModelDescriptor(name='Screamer', count=2).build())


class SoulGrinder(Unit):
    name = "Soul Grinder"
    base_points = 250
    gear = ['Hand weapon', 'Harvester cannon']

    def __init__(self):
        Unit.__init__(self)
        self.marks = self.opt_one_of('', [
            ['Daemon of Khorne', 5, 'kh'],
            ['Daemon of Tzeench', 10, 'tz'],
            ['Daemon of Nurgle', 5, 'ng'],
            ['Daemon of Slaanesh', 0, 'sl']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Daemonbone Claw', 10, 'dc'],
        ])
        self.opt1 = self.opt_options_list('', [
            ['Baleful Torrent', 50, 'bf'],
            ['Phlegm Bombardment', 50, 'pb'],
            ['Warp Gaze', 55, 'wg'],
        ], limit=1)


class PlagueDrones(FCGUnit):
    name = 'Plague Drones of Nurgle'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Plaguebearer', model_price=55, min_models=3,
            musician_price=10, standard_price=10, champion_name='Plaguebringer', champion_price=10,
            magic_banners=unit_standards,
            base_gear=['Plaguesword'],
            options=[
                OptionsList('opt1', 'Options', [
                    ['Death\'s Heads', 10, 'dh'],
                ]),
                OptionsList('opt2', '', [
                    ['Plague Proboscis', 5, 'pp'],
                    ['Venom Sting', 10, 'ws'],
                ], limit=1)
            ]
        )
