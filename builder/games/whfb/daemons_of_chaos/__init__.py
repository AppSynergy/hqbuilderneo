__author__ = 'Denis Romanov'
from builder.games.whfb.legacy_roster import LegacyFantasy
from builder.games.whfb.daemons_of_chaos.heroes import *
from builder.games.whfb.daemons_of_chaos.lords import *
from builder.games.whfb.daemons_of_chaos.core import *
from builder.games.whfb.daemons_of_chaos.special import *
from builder.games.whfb.daemons_of_chaos.rare import *


class DaemonsOfChaos(LegacyFantasy):
    army_name = 'Daemons of Chaos'
    army_id = 'c560b10e21f64537885445d03453b118'

    def __init__(self):
        LegacyFantasy.__init__(
            self,
            lords=[Scarbrand, Fateweaver, Kugath, Bloodthirster, LordOfChange, Unclean, Keeper, DaemonPrince],
            heroes=[Skulltaker, Karanak, BlueScribes, Changeling, Epidemius, Masque, HeraldOfKhorne, HeraldOfTzeentch,
                    HeraldOfNurgle, HeraldOfSlaanesh, ExFlamer],
            core=[Bloodletters, Horrors, Plaguebearers, Daemonettes],
            special=[Bloodcrushers, FlashHounds, Flamers, Screamers, Nurglings, BeastsOfNurgle, Seekers,
                     FiendsOfSlaanesh, SeekerChariot, Furies],
            rare=[SkullCannon, BurningChariot, SoulGrinder, ExaltedSeekerChariot, Hellflayer, PlagueDrones]
        )
