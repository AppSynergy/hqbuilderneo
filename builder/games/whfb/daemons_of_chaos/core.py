__author__ = 'Denis Romanov'

from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.legacy_armory import magic_standards, filter_items

unit_standards = filter_items(magic_standards, 25)


class Bloodletters(FCGUnit):
    name = 'Bloodletters of Khorne'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Bloodletter', model_price=14, min_models=10,
            musician_price=10, standard_price=10, champion_name='Bloodreaper', champion_price=10,
            magic_banners=unit_standards,
            base_gear=['Hellblade'],
        )


class Plaguebearers(FCGUnit):
    name = 'Plaguebearers of Nurgle'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Plaguebearer', model_price=13, min_models=10,
            musician_price=10, standard_price=10, champion_name='Plagueridden', champion_price=10,
            magic_banners=unit_standards,
            base_gear=['Plaguesword'],
        )


class Daemonettes(FCGUnit):
    name = 'Daemonettes of Slaanesh'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Daemonette', model_price=11, min_models=10,
            musician_price=10, standard_price=10, champion_name='Alluress', champion_price=10,
            magic_banners=unit_standards,
        )


class Horrors(FCGUnit):
    name = 'Pink Horrors of Tzeentch'
    gear = ['Level 1 Wizard', 'The Lore of Tzeentch']

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Pink Horror', model_price=13, min_models=10,
            musician_price=10, standard_price=10, champion_name='Iridescent Horror', champion_price=10,
            magic_banners=unit_standards,
        )
