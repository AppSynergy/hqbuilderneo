__author__ = 'Ivan Truskov'


from builder.games.whfb.legacy_armory import *


vampire_powers = [
    ['Master of the Black Arts', 75, 'moba'],
    ['Curse of the Renevant', 55, 'cor'],
    ['Red Fury', 50, 'rf'],
    ['Flying Horror', 30, 'fh'],
    ['Quickblood', 30, 'qb'],
    ['Aura of Dark Majesty', 25, 'adm'],
    ['Dark Acolyte', 25, 'da'],
    ['Forbidden Lore', 25, 'fl'],
    ['Supernatural Horror', 25, 'sh'],
    ['Fear Incarnate', 20, 'fi'],
    ['Beguile', 15, 'bg'],
    ['Master strike', 15, 'ms'],
    ['Dread knight', 10, 'dk'],
    ['Summon Creatures of the Night', 10, 'scr']
]

vampire_magic_weapons = [
    ['Scabscrath', 75, 'scabs']
] + magic_weapons

necromancer_armours = [
    ['Nightshroud', 40, 'nshr']
]

vampire_armours = necromancer_armours + magic_armours

vampire_arcane_items = [
    ['Black Periart', 55, 'blper'],
    ['Staff of Damnation', 40, 'std'],
    ['The Cursed Book', 35, 'cbk'],
    ['Book of Arkhan', 25, 'barkh']
] + arcane_items

vampire_enchanted_items = [
    ['Rod of Flaming Death', 40, 'rofd']
] + enchanted_items

vampire_magic_standards = [
    ['Banner of the Barrows', 50, 'bbrw'],
    ['The Screaming Banner', 25, 'scrb']
] + magic_standards
