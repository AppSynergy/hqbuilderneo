__author__ = 'Ivan Truskov'
from builder.games.whfb.legacy_roster import LegacyFantasy
from builder.games.whfb.vampire_counts.heroes import *
from builder.games.whfb.vampire_counts.lords import *
from builder.games.whfb.vampire_counts.core import *
from builder.games.whfb.vampire_counts.special import *
from builder.games.whfb.vampire_counts.rare import *


class VampireCounts(LegacyFantasy):
    army_name = 'Vampire Counts'
    army_id = 'afb565a3190640a1b79ab9ded87a3b42'

    def __init__(self):
        LegacyFantasy.__init__(
            self,
            lords=[Vlad, MannfredC, Kemmler, VampireLord, MasterNecromancer, StrigoiKing],
            heroes=[MannfredA, Krell, Konrad, Izabella, Necromancer, Vampire, WightKing, CairnWraith, TombBanshee],
            core=[Zombies, SkeletonWarriors, Ghouls, Wolves],
            special=[CorpseCart, GraveGuard, BlackKnights, CryptHorrors, FellBats, BatSwarms, SpiritHost, Hexwraiths,
                     Vargheists],
            rare=[Terrorgheist, Varghulf, BloodKnights, CairnWraiths, BlackCoach, MortisEngine]
        )

    def check_rules(self):
        LegacyFantasy.check_rules(self)
        master = 1 if self.lords.count_unit(Kemmler) > 0 else 0
        for necro in self.lords.get_units([MasterNecromancer]) + self.heroes.get_units([Necromancer]):
            master = master + 1 if necro.is_master() else 0
        if master > 1:
            self.error("Only one Master of the Dead can be in the army")
        scelcnt = sum((1 for squad in self.core.get_units([SkeletonWarriors]) if squad.get_unique_gear()))
        if scelcnt > 1:
            self.error("Only one unit of Skeleton Warriors in the army may carry a magic banner.")
        bcnt = sum([1 if unt.is_bearer() else 0 for unt in self.heroes.get_units([Vampire, WightKing])])
        if bcnt > 1:
            self.error("Only one Vampire or Wight King in the army can carry army banner.")
