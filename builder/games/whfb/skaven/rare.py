__author__ = 'Denis Romanov'

from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor


class Doomwheel(Unit):
    name = 'Doomwheel'
    base_points = 150
    static = True

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Crew (Warlock and Rats)').build(1))


class WarpCannon(Unit):
    name = 'Warp Lightning Cannon'
    base_points = 90
    static = True

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Engineer and Crew').build(1))


class Catapult(Unit):
    name = 'Plague Claw Catapult'
    base_points = 100
    static = True

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Plague Monk Crew').build(1))


class Abomination(Unit):
    name = 'Hell Pit Abomination'
    base_points = 235

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Warpstone spikes', 15, 'sw'],
        ])
