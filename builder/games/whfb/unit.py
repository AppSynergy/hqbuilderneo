__author__ = 'dromanow'

from builder.core.model_descriptor import ModelDescriptor
from builder.core.unit import Unit


class FCGUnit(Unit):
    def __init__(self, model_name, model_price, min_models=1, max_model=100, base_gear=None, rank_gear=None,
                 musician_price=None, musician_name='Musician',
                 standard_price=None, standard_name='Standard bearer', magic_banners=None, magic_banner_check=None,
                 champion_price=None, champion_name='Champion', champion_gear=None, champion_options=None,
                 champion_units=None, options=None, unit_options=None):
        Unit.__init__(self)
        self.model_name = model_name
        self.model_price = model_price
        self.base_gear = base_gear if base_gear else []
        self.magic_banner_check = magic_banner_check
        self.rank_gear = rank_gear if rank_gear else []

        self.musician_price = musician_price
        self.musician_name = musician_name
        self.standard_price = standard_price
        self.standard_name = standard_name
        self.champion_price = champion_price
        self.champion_name = champion_name

        self.min_models = min_models
        self.max_models = max_model

        self.count = self.opt_count(self.model_name, min_models, max_model, self.model_price)
        fcg = []
        if champion_price is not None:
            fcg.append([champion_name, champion_price, 'chmp'])
        if standard_price is not None:
            fcg.append([standard_name, standard_price, 'sb'])
        if musician_price is not None:
            fcg.append([musician_name, musician_price, 'mus'])
        self.champion_units = self.append_opt(champion_units) if champion_units else None
        self.command_group = self.opt_options_list('Command group', fcg) if fcg else None
        self.champ = self.opt_options_list('Champion equipment', champion_gear, 1) if champion_gear else None
        self.champ_options = [self.append_opt(o) for o in champion_options] if champion_options else []
        self.ban = self.opt_options_list('Magic Standard', magic_banners, 1) if magic_banners else None
        self.options = [self.append_opt(o) for o in options] if options else []
        self.unit_options = [self.append_opt(o) for o in unit_options] if unit_options else []

    def get_unique_gear(self):
        if self.ban:
            return self.ban.get_selected()

    def check_rules(self):
        if self.champion_units:
            self.count.update_range(self.min_models - self.champion_units.get_count(), self.max_models)

        if self.command_group and len(self.command_group.get_all()) > self.count.get():
            self.count.set(len(self.command_group.get_all()))

        if self.ban:
            if not self.magic_banner_check:
                self.ban.set_visible(self.command_group.get('sb'))
            else:
                self.ban.set_visible(self.command_group.get('sb') and self.magic_banner_check())

        if self.champ:
            self.champ.set_visible(self.command_group.get('chmp'))

        if self.command_group:
            for o in self.champ_options:
                o.set_visible(self.command_group.get('chmp'))

        self.points.set(self.build_points(count=1, exclude=[o.id for o in self.options]) +
                        sum((o.points() for o in self.options)) * self.count.get())
        self.build_description(options=self.unit_options, count=1)
        if self.champion_units:
            self.description.add(self.champion_units.get_selected())

        d = ModelDescriptor(self.model_name, gear=self.base_gear, points=self.model_price)
        for o in self.options:
            d.add_gear_opt(o)

        if self.command_group and self.command_group.get('chmp'):
            ch = d.clone().set_name(self.champion_name).add_points(self.champion_price).add_gear_opt(self.champ)
            for o in self.champ_options:
                ch.add_gear_opt(o)
            self.description.add(ch.build(1))

        d.add_gear_list(self.rank_gear)

        if self.command_group:
            if self.command_group.get('sb'):
                self.description.add(d.clone().add_gear(self.standard_name,
                                                        self.standard_price).add_gear_opt(self.ban).build(1))
            if self.command_group.get('mus'):
                self.description.add(d.clone().add_gear(self.musician_name, self.musician_price).build(1))
            command_count = len(self.command_group.get_all())
        else:
            command_count = 0
        self.description.add(d.build(self.count.get() - command_count))

    def has_flag(self):
        return self.command_group.get('sb')

    def has_magic_flag(self):
        return self.ban.is_any_selected() if self.ban else False

    def get_count(self):
        return Unit.get_count(self) + (self.champion_units.get_count() if self.champion_units else 0)