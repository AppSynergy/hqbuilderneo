__author__ = 'Ivan Truskov'

from vampire_counts import VampireCounts
from orcs_and_goblins import Orcs
from obsolete.lizardmen import Lizardmen
from dwarfs import Dwarfs
from high_elves import HighElves
from builder.games.whfb.obsolete.wood_elves import WoodElves
from empire import Empire
from tomb_kings import TombKings
from warriors_of_chaos import WarriorsOfChaos
from daemons_of_chaos import DaemonsOfChaos
from ogre_kingdoms import OgreKingdoms
from beastmen import Beastmen
from builder.games.whfb.obsolete.dark_elves import DarkElves
from skaven import Skaven
from lizardmen_v2 import LizardmenV2
from dark_elves_v2 import DarkElvesV2
from wood_elves_v2 import WoodElvesV2

armies = [
    VampireCounts,
    Orcs,
    Lizardmen,
    Dwarfs,
    HighElves,
    Empire,
    WoodElves,
    TombKings,
    WarriorsOfChaos,
    DaemonsOfChaos,
    OgreKingdoms,
    Beastmen,
    DarkElves,
    Skaven,
    LizardmenV2,
    DarkElvesV2,
    WoodElvesV2,
]
