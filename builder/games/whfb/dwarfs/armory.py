from builder.games.whfb.legacy_armory import multiple_id

__author__ = 'Denis Romanov'


from builder.games.whfb.legacy_armory import *


def multiple_runes(name, points, item_id, count=3):
    return [[name, points, multiple_id(item_id, i)] for i in range(count)]


def process_base_runes(rune_list, rune_id, count=3):
    selected = sum((1 for i in range(count) if rune_list.get(multiple_id(rune_id, i))))
    for i in range(count):
        full_id = multiple_id(rune_id, i)
        if rune_list.get(full_id) or selected >= 0:
            rune_list.set_visible_options([multiple_id(rune_id, i)], True)
        else:
            rune_list.set_visible_options([multiple_id(rune_id, i)], False)
        selected -= 1


def process_base_rune_list(rune_list, id_list):
    for rune_id in id_list:
        process_base_runes(rune_list, rune_id)


def get_master_runes_ids(runes):
    return {v[2]: v[0] for v in runes}


master_standard_runes = [
    ['Master Rune of Valaya', 100, 'dw_valaya'],
    ['Master Rune of Stromni Redbeard', 100, 'dw_Stromni'],
    ['Master Rune of Fear', 75, 'dw_fear'],
    ['Master Rune of Grungni', 50, 'dw_Grungni'],
]

base_standard_runes = [
    ['Strolazz\'s Rune', 55, 'dw_strolazz'],
    ['Rune of Slowness', 50, 'dw_rosl'],
    ['Rune of Courage', 30, 'dw_roc'],
    ['Rune of Guarding', 30, 'dw_rogrd'],
    ['Rune of Battle', 25, 'dw_rob'],
    ['Rune of Stoicism', 25, 'dw_stoicism'],
    ['Rune of Determination', 20, 'dw_det'],
    ['Rune of Sanctuary', 15, 'dw_ros'],
    ['Ancestor Rune', 10, 'dw_ancestor']
]

standard_runes = master_standard_runes + sum((multiple_runes(v[0], v[1], v[2]) for v in base_standard_runes), [])
master_standard_runes_id = get_master_runes_ids(master_standard_runes)
base_standard_runes_id = get_ids(base_standard_runes)

lesser_standard_runes = filter_items(standard_runes, 50)
lesser_runic_standards = sum((multiple_runes(v[0], v[1], v[2]) for v in lesser_standard_runes), [])
lesser_runic_standards_ids = get_ids(lesser_standard_runes)


master_eng_runes = [
    ['Master Rune of Defence', 40, 'dw_mrod'],
    ['Master Rune of Disguise', 30, 'dw_mrodis'],
    ['Master Rune of Immolation', 30, 'dw_mroim']
]

base_eng_runes = [
    ['Rune of Fortune', 25, 'dw_rofort'],
    ['Rune of Penetrating', 25, 'dw_ropen'],
    ['Valiant Rune', 20, 'dw_val'],
    ['Stalwart Rune', 20, 'dw_str'],
    ['Rune of Burning', 5, 'dw_rbur'],
]

base_cannon_runes = base_eng_runes + [
    ['Rune of Forging', 35, 'dw_rof'],
    ['Rune of Reloading', 30, 'dw_ror']
]

base_stone_thrower_runes = base_eng_runes + [
    ['Rune of Accuracy', 25, 'dw_roa']
]

base_bolt_thrower_runes = base_eng_runes + [
    ['Flakkson\'s Rune of Seeking', 15, 'dw_fros']
]

master_bolt_thrower_runes = master_eng_runes + [
    ['Master Rune of Skewering', 30, 'dw_mrosk']
]

master_cannon_runes = master_eng_runes
master_stone_thrower_runes = master_eng_runes

cannon_runes = master_cannon_runes + sum((multiple_runes(v[0], v[1], v[2]) for v in base_cannon_runes), [])
master_cannon_runes_id = get_master_runes_ids(master_cannon_runes)
base_cannon_runes_id = get_ids(base_cannon_runes)


bolt_thrower_runes = master_bolt_thrower_runes + sum((multiple_runes(v[0], v[1], v[2])
                                                      for v in base_bolt_thrower_runes), [])
master_bolt_thrower_runes_id = get_master_runes_ids(master_bolt_thrower_runes)
base_bolt_thrower_runes_id = get_ids(base_bolt_thrower_runes)


stone_thrower_runes = master_stone_thrower_runes + sum((multiple_runes(v[0], v[1], v[2])
                                                        for v in base_stone_thrower_runes), [])
master_stone_thrower_runes_id = get_master_runes_ids(master_stone_thrower_runes)
base_stone_thrower_runes_id = get_ids(base_stone_thrower_runes)

master_weapon_runes = [
    ['Master Rune of Skalf Blackhammer', 75, 'dw_skalf'],
    ['Master Rune of Smiting', 70, 'dw_smiting'],
    ['Master Rune of Alaric the Mad', 50, 'dw_alaric'],
    ['Master Rune of Breaking', 45, 'dw_breaking'],
    ['Master Rune of Flight', 50, 'dw_flight'],
    ['Master Rune of Swiftness', 25, 'dw_swiftness'],
    ['Master Rune of Snorri Spangelhelm', 25, 'dw_snorri'],
    ['Master Rune of Krag the Grim', 20, 'dw_krag'],
]

base_weapon_runes = [
    ['Rune of Might', 25, 'dw_might'],
    ['Rune of Fury', 25, 'dw_fury'],
    ['Rune of Cleaving', 20, 'dw_cleaving'],
    ['Grudge Rune', 15, 'dw_grudge'],
    ['Rune of Striking', 10, 'dw_striking'],
    ['Rune of Speed', 5, 'dw_speed'],
    ['Rune of Fire', 5, 'dw_fire'],
]

weapon_runes = master_weapon_runes + sum((multiple_runes(v[0], v[1], v[2]) for v in base_weapon_runes), [])
master_weapon_runes_id = get_master_runes_ids(master_weapon_runes)
base_weapon_runes_id = get_ids(base_weapon_runes)


master_armour_runes = [
    ['Master Rune of Steel', 50, 'dw_steel'],
    ['Master Rune of Adamant', 45, 'dw_adamant'],
    ['Master Rune of Gromril', 25, 'dw_gromril'],
]

base_armour_runes = [
    ['Rune of Fortitude', 50, 'dw_fortitude'],
    ['Rune of Shielding', 25, 'dw_shielding'],
    ['Rune of Resistance', 25, 'dw_resistance'],
    ['Rune of Iron', 15, 'dw_iron'],
    ['Rune of Preservation', 15, 'dw_pres'],
    ['Rune of Stone', 5, 'dw_stone'],

]

armour_runes = master_armour_runes + sum((multiple_runes(v[0], v[1], v[2]) for v in base_armour_runes), [])
master_armour_runes_id = get_master_runes_ids(master_armour_runes)
base_armour_runes_id = get_ids(base_armour_runes)

master_talisman_runes = [
    ['Master Rune of Kingship', 100, 'dw_kingship'],
    ['Master Rune of Spite', 45, 'dw_spite'],
    ['Master Rune of Challenge', 25, 'dw_challenge'],
    ['Master Rune of Dismay', 25, 'dw_dismay'],
]

runesmith_master_talisman_runes = [
    ['Master Rune of Balance', 50, 'dw_balance'],  # Runesmith only
    ['Master Rune of Spellbinding', 50, 'dw_spellbinding'],  # Runesmith only
]

base_talisman_runes = [
    ['Spelleater Rune', 50, 'dw_spelleater'],
    ['Rune of Fate', 35, 'dw_fate'],
    ['Rune of Spellbreaking', 25, 'dw_spellbreaking'],
    ['Rune of Brotherhood', 20, 'dw_brotherhood'],
    ['Rune of Luck', 15, 'dw_luck'],
    ['Rune of Warding', 15, 'dw_warding'],
    ['Rune of the Furnace', 5, 'dw_furnace'],
]

talisman_runes = master_talisman_runes + sum((multiple_runes(v[0], v[1], v[2]) for v in base_talisman_runes), [])
master_talisman_runes_id = get_master_runes_ids(master_talisman_runes)
base_talisman_runes_id = get_ids(base_talisman_runes)

runesmith_talisman_runes = master_talisman_runes + runesmith_master_talisman_runes + \
                           sum((multiple_runes(v[0], v[1], v[2]) for v in base_talisman_runes), [])
runesmith_master_talisman_runes_id = get_master_runes_ids(runesmith_master_talisman_runes)
runesmith_base_talisman_runes_id = get_ids(base_talisman_runes)

