__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit
from mounts import *
from armory import *
from rare import AncientStegadon
from special import Stegadon
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor


class Chakax(StaticUnit):
    base_points = 335
    name = 'Chakax'
    gear = ['The Star-Stone Mace', 'Helm of the Prime Guardian', 'Key of the Eternity Chamber']


class GorRok(StaticUnit):
    name = 'Gor-Rok'
    base_points = 215
    gear = ['The Shield of Aeons', 'The Mace of Ulamak']


class TettoEko(StaticUnit):
    name = 'Tetto\'eko'
    base_points = 255
    gear = ['Level 2 Wizard', 'The Lore of Heavens', 'Eye of the Old Ones', 'Stellar Staff',
            'The Palanquin of Connstelations']


class Oxiotl(StaticUnit):
    name = 'Oxiotl'
    base_points = 160
    gear = ['Hand weapon', 'Blowpipe']


class TictaqTo(StaticUnit):
    name = 'Tictaq\'to'
    base_points = 315
    gear = ['Mask of Heavens', 'Blade of the Ancient Skies']

    def __init__(self):
        StaticUnit.__init__(self)
        self.description.add(ModelDescriptor(name='Zwup', count=1).build())


class Scar(Unit):
    name = 'Saurus Scar-Veteran'
    gear = ['Hand Weapon']
    base_points = 85

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Spear', 4, 'sp'],
            ['Great weapon', 6, 'gw'],
            ['Halberd', 4, 'hb'],
            ['Hand weapon', 4, 'hw'],
        ], limit=1)
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 3, 'sh'],
            ['Light armour', 5, 'la'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [ColdOne(price=20)])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_liz_magic_weapons, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_saurus_magic_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_liz_talismans, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_saurus_enchanted_items, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', liz_magic_standards, 1)

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(liz_magic_shields_ids))
        self.eq.set_active_options(['la'], not self.magic_arm.is_selected(liz_magic_armour_suits_ids))
        self.magic_enc.set_visible_options(['liz_cjw'], self.steed.get_count() == 0)
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])

    def is_bearer(self):
        return self.banner.get('bs')


class Chief(Unit):
    name = 'Skink Chief'
    gear = ['Hand Weapon']
    base_points = 55

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Spear', 4, 'sp'],
            ['Blowpipe', 8, 'bp'],
            ['Javelin', 8, 'jav'],
            ['Hand weapon', 4, 'hw'],
        ], limit=1)
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 2, 'sh'],
            ['Light armour', 2, 'la'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [Stegadon(boss=True), AncientStegadon(boss=True), Terradon()])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_skink_magic_weapons, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_skink_magic_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_liz_talismans, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_skink_enchanted_items, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', skink_magic_standards, 1)

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(liz_magic_shields_ids))
        self.eq.set_active_options(['la'], not self.magic_arm.is_selected(liz_magic_armour_suits_ids))
        self.magic_enc.set_visible_options(['liz_cjw', 'liz_cof'], self.steed.get_count() == 0)
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        self.magic_wep.set_visible_options(['liz_steg'],
                                           self.steed.get(Stegadon.name) or self.steed.get(AncientStegadon.name))
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])

    def is_bearer(self):
        return self.banner.get('bs')


class Priest(Unit):
    name = 'Skink Priest'
    gear = ['Hand Weapon', 'The Lore of Heavens']
    base_points = 65

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [AncientStegadon(boss=True, priest=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_skink_magic_weapons, 1)
        self.magic_arc = self.opt_options_list('Arcane item', heroes_liz_arcane_items, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_liz_talismans, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_skink_enchanted_items, 1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.magic_enc.set_visible_options(['liz_cjw', 'liz_cof'], self.steed.get_count() == 0)
        self.magic_wep.set_visible_options(['liz_steg'], self.steed.get(AncientStegadon.name))
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic if m.get_selected()), [])
