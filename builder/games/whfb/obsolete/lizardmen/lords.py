__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit
from mounts import *
from armory import *
from rare import AncientStegadon
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor


class Kroak(StaticUnit):
    name = 'Lord Kroak'
    base_points = 600
    gear = ['Golden Death Mask']


class Mazdamundi(StaticUnit):
    name = 'Lord Mazdamundi'
    base_points = 620
    gear = ['Cobra Mace of Mazdamundi', 'Sun Standard of Hexoatl']

    def __init__(self):
        StaticUnit.__init__(self)
        self.description.add(ModelDescriptor(name='Zlaaq', count=1).build())


class KroqGar(StaticUnit):
    name = 'Kroq-Gar'
    base_points = 635
    gear = ['Light armour', 'Hand of Gods', 'Revered Spear of Tlanxia']

    def __init__(self):
        StaticUnit.__init__(self)
        self.description.add(ModelDescriptor(name='Grymloq', count=1).build())


class Tehenhauin(Unit):
    name = 'Tehenhauin'
    gear = ['Level 2 Wizard', 'The Lore of Beasts', 'Blade of the Serpent\'s Tongue', 'Plaque of Sotek']
    base_points = 250

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_optional_sub_unit('Steed', [AncientStegadon(boss=True, priest=True)])


class Slann(Unit):
    name = 'Slann Mage-Priest'
    base_gear = ['Level 4 Wizard']
    base_points = 275

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Upgrade', [
            ['One Discipline of the Ancient', 0, 'one'],
            ['Two Disciplines of the Ancient', 50, 'two'],
            ['Three Disciplines of the Ancient', 100, 'three'],
            ['Four Disciplines of the Ancient', 150, 'four'],
        ])
        self.magic_wep = self.opt_options_list('Magic weapon', liz_magic_weapons, 1)
        self.magic_tal = self.opt_options_list('Talisman', liz_talismans, 1)
        self.magic_arc = self.opt_options_list('Arcane item', liz_arcane_items, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', slann_enchanted_items, 1)
        self.magic = [self.magic_wep, self.magic_tal, self.magic_arc, self.magic_enc]
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', liz_magic_standards, 1)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Fire', 0, 'fire'],
            ['The Lore of Beasts', 0, 'beast'],
            ['The Lore of Metal', 0, 'metal'],
            ['The Lore of Light', 0, 'light'],
            ['The Lore of Life', 0, 'life'],
            ['The Lore of Heavens', 0, 'heavens'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
        ], limit=1)

    def check_rules(self):
        self.magban.set_active(self.banner.get('bsb'))
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])

    def is_bearer(self):
        return self.banner.get('bs')


class Oldblood(Unit):
    name = 'Saurus Oldblood'
    gear = ['Hand Weapon']
    base_points = 145

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Spear', 8, 'sp'],
            ['Great weapon', 12, 'gw'],
            ['Halberd', 8, 'hb'],
            ['Hand weapon', 8, 'hw'],
        ], limit=1)
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 6, 'sh'],
            ['Light armour', 10, 'la'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [ColdOne(), Carnosaur()])
        self.magic_wep = self.opt_options_list('Magic weapon', liz_magic_weapons, 1)
        self.magic_arm = self.opt_options_list('Magic armour', saurus_magic_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', liz_talismans, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', saurus_enchanted_items, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal,  self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(liz_magic_shields_ids))
        self.eq.set_active_options(['la'], not self.magic_arm.is_selected(liz_magic_armour_suits_ids))
        self.magic_enc.set_visible_options(['liz_cjw'], self.steed.get_count() == 0)
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])
