__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit


class ElvenSteed(Unit):
    name = 'Elven Steed'
    base_points = 18
    static = True

    def __init__(self, points=18):
        self.base_points = points
        Unit.__init__(self)


class GreatEagle(StaticUnit):
    name = 'Great Eagle'
    base_points = 50


class Unicorn(StaticUnit):
    name = 'Unicorn'
    base_points = 65


class GreatStag(StaticUnit):
    name = 'Great Stag'
    base_points = 50


class ForestDragon(StaticUnit):
    name = 'Forest Dragon'
    base_points = 320

