__author__ = 'Denis Romanov'


from builder.games.whfb.armory import *


class Weapon(BaseWeapon):
    def __init__(self, parent, name='Magic weapon', limit=None):
        super(Weapon, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.variant('The Spirit Sword', 85)
        self.variant('Daith\'s Reaper', 50)
        self.variant('The Bow of Loren', 20)
        self.add_base()


class Standards(BaseStandards):
    def __init__(self, parent, name='Magic standards', limit=None):
        super(Standards, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.variant('The Banner of the Eternal Queen', 100)
        self.variant('The Banner of the Hinter King', 75)
        self.add_base()


class Armour(BaseArmour):
    def __init__(self, parent, name='Magic armour', suits=True, shields=True, limit=None):
        super(Armour, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.variant('The Helm of Hunt', 20)
        self.add_base(suits=suits, shields=shields)


class Arcane(BaseArcane):
    def __init__(self, parent, name='Arcane items', limit=None):
        super(Arcane, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.variant('Galaingor\'s Stave', 20)
        self.add_base()


class Enchanted(BaseEnchanted):
    def __init__(self, parent, name='Enchanted items', limit=None):
        super(Enchanted, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.variant('Acorns of the Ages', 100)
        self.variant('Moonstone of the Hidden Ways', 40)
        self.variant('Hail of Doom Arrow', 30)
        self.add_base()


class Talismans(BaseTalismans):
    def __init__(self, parent, name='Talismans', limit=None):
        super(Talismans, self).__init__(parent, name=name, limit=1, filter_limit=limit)
        self.add_base()


class Arrows(OptionsList):
    def __init__(self, parent, per_model=True):
        super(Arrows, self).__init__(parent, 'Enchanted arrows', limit=1)
        self.variant('Hagbane tips', points=3, per_model=per_model)
        self.variant('Trueflight arrows', points=3, per_model=per_model)
        self.variant('Moonfire shot', points=4, per_model=per_model)
        self.variant('Starfire shafts', points=4, per_model=per_model)
        self.variant('Swiftsilver shards', points=4, per_model=per_model)
        self.variant('Arcane bodkins', points=5, per_model=per_model)
