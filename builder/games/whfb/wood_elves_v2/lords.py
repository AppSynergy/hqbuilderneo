from armory import *
from builder.games.whfb.roster import Character, Lore

__author__ = 'Denis Romanov'


class Orion(Unit):
    type_name = 'Orion'
    type_id = 'orion_v1'

    def __init__(self, parent):
        super(Orion, self).__init__(parent, points=600, unique=True, gear=[
            Gear('Hawk\'s Talon'), Gear('Spear of Kurnous'), Gear('Cloak of Isha'), Gear('Horn of the Wild Hunt')
        ])
        Count(self, 'Hound of Orion', 0, 2, 20, gear=UnitDescription('Hound of Orion', points=20))


class Durthu(Unit):
    type_name = 'Durthu'
    type_id = 'durthu_v1'

    def __init__(self, parent):
        super(Durthu, self).__init__(parent, points=385, unique=True, gear=[
            Gear('Hand weapon'), Gear('Level 1 Wizard'), Gear('Lore of Beast')
        ])


class Araloth(Unit):
    type_name = 'Araloth'
    type_id = 'araloth_v1'

    def __init__(self, parent):
        super(Araloth, self).__init__(parent, points=260, unique=True, gear=[
            Gear('Hand weapon'), Gear('Shield'), Gear('Asrai spear')
        ])


class GladeLord(Character):
    type_name = 'Glade Lord'
    type_id = 'gladelord_v1'

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(GladeLord.Weapon, self).__init__(parent, name='Weapon', limit=1)
            self.spear = self.variant('Asrai spear', 3)
            self.hw = self.variant('Additional hand weapon', 3, gear=Gear('Hand weapon'))
            self.gw = self.variant('Great weapon', 6)

        def check_rules(self):
            has_mount = self.parent.mount.has_mount()
            if has_mount and self.hw.value:
                self.hw.value = False
            self.process_limit([self.spear, self.hw, self.gw], 1)
            if has_mount:
                self.hw.active = self.hw.used = False

    class Armour(OptionsList):
        def __init__(self, parent):
            super(GladeLord.Armour, self).__init__(parent, name='')
            self.shield = self.variant('Shield', 3)

        def check_rules(self):
            super(GladeLord.Armour, self).check_rules()
            self.shield.used = self.shield.active = not self.parent.magic_armour.has_shield()

    class Mount(OptionsList):
        def __init__(self, parent):
            super(GladeLord.Mount, self).__init__(parent, 'Mount', limit=1)
            for i in [
                ('Elven Steed', 20),
                ('Great Eagle', 50),
                ('Great Stag', 65),
                ('Forest Dragon', 300),
            ]:
                self.variant(i[0], i[1], gear=[UnitDescription(i[0], points=i[1])])

        def has_mount(self):
            return self.any

    def __init__(self, parent):
        super(GladeLord, self).__init__(
            parent, points=145,
            gear=[Gear('Hand weapon'), Gear('Asrai longbow')],
            magic_limit=100
        )
        self.Weapon(self)
        Arrows(self, per_model=False)
        self.Armour(self)
        self.mount = self.Mount(self)

        self.magic_weapon = Weapon(self)
        self.magic_armour = Armour(self)
        self.magic = [
            self.magic_weapon,
            self.magic_armour,
            Enchanted(self),
            Talismans(self),
        ]

    def build_description(self):
        desc = super(GladeLord, self).build_description()
        if not self.magic_armour.has_suit():
            desc.add(Gear('Light armour'))
        return desc


class SpellWeaver(Character):
    type_name = 'Spellweaver'
    type_id = 'spellweaver_v1'

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(SpellWeaver.Weapon, self).__init__(parent, name='Weapon')
            self.spear = self.variant('Asrai longbow', 5)

    class Mount(OptionsList):
        def __init__(self, parent):
            super(SpellWeaver.Mount, self).__init__(parent, 'Mount', limit=1)
            for i in [
                ('Elven Steed', 20),
                ('Great Eagle', 50),
                ('Unicorn', 60),
            ]:
                self.variant(i[0], i[1], gear=[UnitDescription(i[0], points=i[1])])

    class MagicLevel(OneOf):
        def __init__(self, parent):
            super(SpellWeaver.MagicLevel, self).__init__(parent, 'Wizard')
            self.variant('Level 3 Wizard')
            self.variant('Level 4 Wizard', 35)

    class Lore(Lore):
        def __init__(self, parent):
            super(SpellWeaver.Lore, self).__init__(parent)
            self.variant('The Lore of High Magic')
            self.variant('The Lore of Dark Magic')

    def __init__(self, parent):
        super(SpellWeaver, self).__init__(parent, points=185, gear=[Gear('Hand weapon')], magic_limit=100)
        self.Weapon(self)
        self.Mount(self)
        self.MagicLevel(self)
        self.Lore(self)

        self.magic = [
            Weapon(self),
            Enchanted(self),
            Arcane(self),
            Talismans(self),
        ]


class TreemanAncient(Unit):
    type_id = 'ancienttreeman_v1'
    type_name = 'Treeman Ancient'

    class Opt(OptionsList):
        def __init__(self, parent):
            super(TreemanAncient.Opt, self).__init__(parent, 'Options')
            self.variant('Strangleroots', 20)

    class MagicLevel(OneOf):
        def __init__(self, parent):
            super(TreemanAncient.MagicLevel, self).__init__(parent, 'Wizard')
            self.variant('Level 2 Wizard')
            self.variant('Level 3 Wizard', 35)
            self.variant('Level 4 Wizard', 70)

    def __init__(self, parent):
        super(TreemanAncient, self).__init__(parent, points=290, gear=[Gear('Lore of Life')])
        self.MagicLevel(self)
        self.Opt(self)
