__author__ = 'Denis Romanov'


from builder.core2 import *
from builder.games.whfb.lizardmen_v2.special import Stegadon


class AncientStegadon(Unit):
    type_id = 'ancient_stegadon_v1'
    type_name = 'Ancient Stegadon'

    class Options(OneOf):
        def __init__(self, parent):
            super(AncientStegadon.Options, self).__init__(parent, '')
            self.variant('Giant Blowpipes', points=0)
            self.variant('Engine of the Gods', points=50)

    def __init__(self, parent):
        super(AncientStegadon, self).__init__(parent, points=230, gear=[Stegadon.crew.clone().set_count(5)])
        Stegadon.Options(self)
        self.Options(self)


class Triglodon(Unit):
    type_id = 'triglodon_v1'
    type_name = 'Troglodon'

    class Options(OptionsList):
        def __init__(self, parent):
            super(Triglodon.Options, self).__init__(parent, 'Options')
            self.rod = self.variant('Divining Rod', points=15, gear=[])

    def __init__(self, parent):
        super(Triglodon, self).__init__(parent, points=200)
        self.opt = self.Options(self)

    def build_description(self):
        desc = super(Triglodon, self).build_description()
        skink = UnitDescription('Skink Oracle Raider', options=[Gear('Hand weapon'), Gear('Lustrian javelin')])
        if self.opt.rod.value:
            skink.add(Gear('Divining Rod')).add_points(self.opt.points)
        desc.add(skink)
        return desc


class SalamanderPack(Unit):
    type_name = 'Salamander Hunting Pack'
    type_id = 'salamander_pack_v1'

    def __init__(self, parent, beast_name='Salamander', pack_points=80):
        super(SalamanderPack, self).__init__(parent)
        self.models = Count(self, 'Hunting Pack', 1, 100, pack_points, gear=[])
        self.skinks = Count(self, 'Skink Handler', 0, 1, 4, gear=[])
        self.beast = beast_name

    def check_rules(self):
        self.skinks.max = self.models.cur

    def build_description(self):
        desc = super(SalamanderPack, self).build_description()
        desc.add(UnitDescription(self.beast, count=self.models.cur))
        desc.add(UnitDescription('Skink Handler', points=4, count=self.models.cur * 3 + self.skinks.cur,
                                 options=[Gear('Hand weapon')]))
        return desc

    def get_count(self):
        return self.models.cur * 4 + self.skinks.cur


class RazordonPack(SalamanderPack):
    type_name = 'Razordon Hunting Pack'
    type_id = 'barbed_razordon_pack_v1'

    def __init__(self, parent):
        super(RazordonPack, self).__init__(parent, beast_name='Razordon', pack_points=65)
