from builder.core2 import *
from builder.games.whfb.roster import FCGUnit
from armory import Standards, Weapon
from core import Skinks

__author__ = 'Denis Romanov'


class TempleGuard(FCGUnit):
    type_name = 'Temple Guard'
    type_id = 'temple_guard_v1'

    def __init__(self, parent):
        super(TempleGuard, self).__init__(
            parent, model_name='Temple Guard', model_price=14, min_models=10,
            musician_price=10,
            standard_price=10,
            champion_name='Revered Guardian', champion_price=10,
            gear=[Gear('Halberd'), Gear('Shield'), Gear('Light armour')]
        )
        self.magic_banners = [Standards(self, limit=50)]
        self.champion_magic_options = [Weapon(self, name='Revered Guardian\'s weapon', limit=25)]


class Swarms(Unit):
    type_name = 'Jungle Swarms'
    type_id = 'jungle_swarms_v1'

    def __init__(self, parent):
        super(Swarms, self).__init__(parent)
        self.models = Count(self, 'Jungle Swarm', 2, 100, points=35, per_model=True,
                            gear=UnitDescription('Jungle Swarm', points=35))

    def get_count(self):
        return self.models.cur


class ChameleonSkins(FCGUnit):
    type_name = 'Chameleon Skinks'
    type_id = 'chameleon_skinks_v1'

    def __init__(self, parent):
        super(ChameleonSkins, self).__init__(
            parent, model_name='Chameleon Skink', model_price=13, min_models=5,
            champion_name='Stalker', champion_price=10,
            gear=[Gear('Hand weapon'), Gear('Blowpipe')]
        )


class Kroxigor(FCGUnit):
    type_name = 'Kroxigor'
    type_id = 'kroxigor_v1'

    def __init__(self, parent):
        super(Kroxigor, self).__init__(
            parent, model_name='Kroxigor', model_price=50, min_models=3,
            champion_name='Kroxigor Ancient', champion_price=10,
            gear=[Gear('Great weapon')]
        )


class ColdOneRaiders(FCGUnit):
    type_name = 'Cold One Riders'
    type_id = 'cold_one_raiders_v1'

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(ColdOneRaiders.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Spear', points=4)

    def __init__(self, parent):
        super(ColdOneRaiders, self).__init__(
            parent, model_name='Cold One Raider', model_price=30, min_models=5,
            musician_price=10,
            standard_price=10,
            champion_name='Pack Leader', champion_price=10,
            gear=[Gear('Shield'), Gear('Hand weapon')]
        )
        self.model_options = [self.Weapon(self)]


class TerradonRaiders(FCGUnit):
    type_name = 'Terradon Riders'
    type_id = 'terradon_raiders_v1'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(TerradonRaiders.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Lustrian javelin', points=0)
            self.variant('Fireleech bolas', points=1)

    class Options(OptionsList):
        def __init__(self, parent):
            super(TerradonRaiders.Options, self).__init__(parent, 'Tiktaq\'to\'s upgrade')
            self.amb = self.variant('Ambushers', points=5)

        def check_rules(self):
            super(TerradonRaiders.Options, self).check_rules()
            self.amb.active = self.amb.used = self.roster.has_tik

    def __init__(self, parent):
        super(TerradonRaiders, self).__init__(
            parent, model_name='Terradon Raider', model_price=35, min_models=3,
            champion_name='Sky Leader', champion_price=10,
            gear=[Gear('Hand weapon')]
        )
        self.model_options = [self.Weapon(self)]
        self.amb = self.Options(self)
        self.model_options.extend([self.amb, Skinks.Options(self)])

    def has_amb(self):
        return self.amb.amb.value and self.amb.amb.used


class RipperdactylRaiders(FCGUnit):
    type_name = 'Ripperdactyl Riders'
    type_id = 'ripperdactyl_raiders_v1'

    def __init__(self, parent):
        super(RipperdactylRaiders, self).__init__(
            parent, model_name='Ripperdactyl Raider', model_price=40, min_models=3,
            champion_name='Ripperdactyl Brave', champion_price=10,
            gear=[Gear('Hand weapon'), Gear('Shield')]
        )
        self.model_options = [Skinks.Options(self)]


class Stegadon(Unit):
    type_id = 'stegadon_v1'
    type_name = 'Stegadon'

    crew = UnitDescription('Skink crew', options=[Gear('Hand weapon'), Gear('Lustrian javelin')])

    class Options(OptionsList):
        def __init__(self, parent):
            super(Stegadon.Options, self).__init__(parent, 'Options')
            self.variant('Unstoppable Stampede', points=10)
            self.variant('Sharpened Horns', points=20)

    def __init__(self, parent):
        super(Stegadon, self).__init__(parent, points=215, gear=[self.crew.clone().set_count(5), Gear('Giant bow')])
        self.Options(self)


class Bastiladon(Unit):
    type_id = 'bastiladon_v1'
    type_name = 'Bastiladon'

    class Options(OneOf):
        def __init__(self, parent):
            super(Bastiladon.Options, self).__init__(parent, 'Options')
            skink = UnitDescription('Skink crew', options=[Gear('Hand weapon'), Gear('Lustrian javelin')])
            self.variant('Arc of Sotek', points=0, gear=[Gear('Arc of Sotek'), skink.clone().set_count(4)])
            self.variant('Solar Engine', points=0, gear=[Gear('Solar Engine'), skink.clone().set_count(3)])

    def __init__(self, parent):
        super(Bastiladon, self).__init__(parent, points=150)
        self.Options(self)
