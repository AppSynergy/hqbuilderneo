from builder.core2 import *
from builder.games.whfb.roster import FCGUnit

__author__ = 'Denis Romanov'


class SaurusWarriors(FCGUnit):
    type_name = 'Saurus Warriors'
    type_id = 'saurus_warriors_v1'

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(SaurusWarriors.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Spear', points=0)

    def __init__(self, parent):
        super(SaurusWarriors, self).__init__(
            parent, model_name='Saurus Warrior', model_price=11, min_models=10,
            musician_price=10,
            standard_price=10,
            champion_name='Spawn Leader', champion_price=10,
            gear=[Gear('Shield'), Gear('Hand weapon')]
        )
        self.model_options = [self.Weapon(self)]


class Skinks(FCGUnit):
    type_name = 'Skink Cohort'
    type_id = 'skinks_v1'

    class Poison(OptionsList):
        def __init__(self, parent):
            super(Skinks.Poison, self).__init__(parent, 'Options')
            self.variant('Poisoned Attacks', points=2, per_model=True)

        @property
        def points(self):
            return super(Skinks.Poison, self).points * self.parent.models.cur

    class Options(OptionsList):
        def __init__(self, parent):
            super(Skinks.Options, self).__init__(parent, 'Tehenhauin\'s upgrade')
            self.hate = self.variant('Hatred (Skaven)', points=1)

        def check_rules(self):
            super(Skinks.Options, self).check_rules()
            self.hate.active = self.hate.used = self.roster.has_teh

    def __init__(self, parent):
        super(Skinks, self).__init__(
            parent, model_name='Skink', model_price=5, min_models=10,
            musician_price=10, standard_price=10, champion_name='Skink Brave', champion_price=10,
            gear=[Gear('Shield'), Gear('Hand weapon'), Gear('Lustrian javelin')]
        )
        self.poison = self.Poison(self)
        self.krox = Count(self, 'Kroxigor', 0, 1, 50, gear=UnitDescription('Kroxigor', points=50,
                                                                           options=[Gear('Great weapon')]))
        self.unit_options = [self.poison, self.krox]
        self.model_options = [self.Options(self)]

    def check_rules(self):
        self.krox.max = int(self.models.cur / 8)


class SkinksSkirmishers(FCGUnit):
    type_name = 'Skink Skirmishers'
    type_id = 'skinks_skirmishers_v1'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(SkinksSkirmishers.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Blowpipe', points=0)
            self.variant('Lustrian javelin and shield', points=0, gear=[Gear('Lustrian javelin'), Gear('Shield')])

    def __init__(self, parent):
        super(SkinksSkirmishers, self).__init__(
            parent, model_name='Skinks Skirmisher', model_price=7, min_models=10,
            champion_name='Patrol Leader', champion_price=10,
            gear=[Gear('Hand weapon')]
        )
        self.model_options = [self.Weapon(self), Skinks.Options(self)]
