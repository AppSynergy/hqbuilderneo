__author__ = 'Denis Romanov'
from builder.games.whfb.legacy_roster import LegacyFantasy
from builder.games.whfb.bretonnia.heroes import *
from builder.games.whfb.bretonnia.lords import *
from builder.games.whfb.bretonnia.core import *
from builder.games.whfb.bretonnia.special import *
from builder.games.whfb.bretonnia.rare import *


class Bretonnia(LegacyFantasy):
    name = 'Bretonnia'
    army_id = 'bd8ec0b2da15417f9203b20ae1b47019'

    def __init__(self):
        LegacyFantasy.__init__(
            self,
            lords=[Tyrion, Teclis, Eltharion, Alith, Alarielle, Prince, Archmage, Anointed, HoethLoremaster],
            heroes=[Korhil, Caradryan, Noble, Mage, DragonMage, SeaHelm, Handmaiden],
            core=[Spearmen, Archers, SeaGuard, SilverHelm, Reavers],
            special=[LionChariot, WhiteLions, Swordmasters, Shadow, PhoenixGuard, DragonPrinces, Skycutter,
                     TiranocChariot],
            rare=[BoltThrower, GreatEagles, FrostPhoenix, FlamePhoenix, Sisters]
        )

    def check_rare_limit(self, name, group, limit, mul):
        if name == BoltThrower.name:
            limit = 4
        return LegacyFantasy.check_rare_limit(self, name, group, limit, mul)

    def has_alarielle(self):
        return self.lords.count_unit(Alarielle) > 0
