__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.bretonnia.mounts import *
from builder.games.whfb.bretonnia.armory import *
from builder.games.whfb.bretonnia.rare import FlamePhoenix, FrostPhoenix
from builder.games.whfb.bretonnia.special import TiranocChariot
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor


class Tyrion(StaticUnit):
    name = 'Tyrion'
    base_points = 410
    gear = ['Sunfang', 'Dragon Armour of Aenarion', 'Heart of Avelorn']

    def __init__(self):
        StaticUnit.__init__(self)
        self.description.add(ModelDescriptor(name='Malhandhir', count=1).build())


class Teclis(StaticUnit):
    name = 'Teclis'
    base_points = 450
    gear = ['Level 4 Wizard', 'Sword of Teclis', 'Moon Staff of Lileath', 'Scroll of Hoeth', 'War Crown of Saphery']


class Eltharion(StaticUnit):
    name = 'Eltharion the Grim'
    base_points = 295
    gear = ['Long bow', 'Heavy armour', 'Fangsword of Eltharion', 'Helm of Yvresse', 'Talisman of Hoeth']

    def __init__(self):
        StaticUnit.__init__(self)
        self.description.add(ModelDescriptor(name='Stormwing', count=1).build())


class Alith(StaticUnit):
    name = 'Alith Anar'
    base_points = 250
    gear = ['Hand weapon', 'Light armour', 'The Moonbow', 'The Shadow Crown', 'Stone of Midnight']


class Alarielle(StaticUnit):
    name = 'Alarielle the Radiant'
    base_points = 350
    gear = ['Level 4 Wizard', 'Hand weapon', 'The Shieldstone of Isha', 'Star of Avelorn', 'Stave of Avelorn']


class Prince(Unit):
    name = 'Prince'
    gear = ['Hand Weapon']
    base_points = 140

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Halberd', 3, 'hb'],
            ['Spear', 3, 'sp'],
            ['Great weapon', 6, 'gw'],
            ['Hand weapon', 3, 'hw'],
            ['Lance', 7, 'ln'],
        ], limit=1)
        self.wep_1 = self.opt_options_list('', [
            ['Long Bow', 5, 'lb'],
        ])
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 3, 'sh'],
            ['Lion cloak', 6, 'lc'],
        ])
        self.arm = self.opt_one_of('', [
            ['Light armour', 0, 'la'],
            ['Heavy armour', 6, 'ha'],
            ['Dragon armour', 20, 'da'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [ElvenSteed(), GreatEagle(), Griffon(),
                                                          TiranocChariot(boss=True), SunDragon(), MoonDragon(),
                                                          StarDragon()])
        self.magic_wep = self.opt_options_list('Magic weapon', bret_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', bret_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', bret_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', bret_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(bret_shields_ids))
        self.arm.set_visible(not self.magic_arm.is_selected(bret_armour_suits_ids))
        self.wep.set_visible_options(['hw'], self.steed.get_count() == 0)
        self.wep.set_visible_options(['ln'], self.steed.get_count() > 0)
        self.magic_wep.set_visible_options(['bret_star'], self.steed.get_count() > 0)
        self.magic_enc.set_visible_options(['bret_mor'], self.steed.get_count() == 0)
        self.eq.set_active_options(['lc'], self.arm.get_cur() != 'da')
        self.arm.set_active_options(['da'], not self.eq.get('lc'))
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Archmage(Unit):
    name = 'Archmage'
    gear = ['Hand Weapon']
    base_points = 185

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(3, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [ElvenSteed(), GreatEagle(), TiranocChariot(boss=True),
                                                          SunDragon(), MoonDragon(), StarDragon()])
        self.magic_wep = self.opt_options_list('Magic weapon', bret_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', bret_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', bret_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', bret_arcane, 1)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of High', 0, 'high'],
            ['The Lore of Fire', 0, 'fire'],
            ['The Lore of Beasts', 0, 'beast'],
            ['The Lore of Metal', 0, 'metal'],
            ['The Lore of Light', 0, 'light'],
            ['The Lore of Life', 0, 'life'],
            ['The Lore of Heavens', 0, 'heavens'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
        ], limit=1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.magic_wep.set_visible_options(['bret_star'], self.steed.get_count() > 0)
        self.magic_enc.set_visible_options(['bret_mor'], self.steed.get_count() == 0)
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Anointed(Unit):
    name = 'Anointed of Asuryan'
    base_gear = ['Halberd']
    base_points = 210

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_optional_sub_unit('Steed', [FlamePhoenix(), FrostPhoenix()])
        self.magic_wep = self.opt_options_list('Magic weapon', bret_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', bret_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', bret_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', bret_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.gear = self.base_gear + (['Heavy Armour'] if not self.magic_arm.is_selected(bret_armour_suits_ids) else [])
        self.magic_wep.set_visible_options(['bret_star'], self.steed.get_count() > 0)
        self.magic_enc.set_visible_options(['bret_mor'], self.steed.get_count() == 0)
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class HoethLoremaster(Unit):
    name = 'Loremaster of Hoeth'
    base_gear = ['Level 2 Wizard', 'Great weapon']
    base_points = 230

    def __init__(self):
        Unit.__init__(self)
        self.magic_wep = self.opt_options_list('Magic weapon', bret_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', bret_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', bret_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', bret_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.gear = self.base_gear + (['Heavy Armour'] if not self.magic_arm.is_selected(bret_armour_suits_ids) else [])
        self.magic_wep.set_visible_options(['bret_star'], False)
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


