__author__ = 'Denis Romanov'


from builder.games.whfb.legacy_armory import *


tk_weapon = [
    ['Blade of Antarhak', 50, 'tk_boa'],
] + magic_weapons

king_weapon = [
    ['Destroyer of Eternities', 80, 'tk_doe'],  # tob king on foot only
] + tk_weapon

tk_armour = magic_armours

tk_armour_suits_ids = magic_armour_suits_ids
tk_shields_ids = magic_shields_ids

tk_talisman = talismans

tk_enchanted = [
    ['Golden Death Mask of Kharnut', 60, 'tk_mask'],
    ['Cloak of Dunes', 50, 'tk_cloak'],  # on foot only
] + enchanted_items

tk_arcane = [
    ['Neferra\'s Scrolls of Mighty Incantations', 50, 'tk_nef'],
    ['Enkhil\'s Kanopi', 25, 'tk_kan']
] + arcane_items

tk_standard = [
    ['Standard of the Undying Legion', 50, 'tk_leg'],
    ['Banner of the Hidden Dead', 90, 'tk_hid']
] + magic_standards
