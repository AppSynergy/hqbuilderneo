__author__ = 'Denis Romanov'
from builder.core.unit import Unit, OneOf, OptionsList
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.orcs_and_goblins.armory import orcs_magic_standards, filter_items


class BlackOrcs(FCGUnit):
    name = 'Black Orcs'

    def __init__(self):
        self.up = OptionsList('up', 'Grimgors\'s upgrade', [['Da Immortulz', 0, 'imm']])
        FCGUnit.__init__(
            self, model_name='Black Orc', model_price=12, min_models=10,
            musician_price=10, standard_price=10, champion_name='Black Orc Boss', champion_price=15,
            base_gear=['Heavy armour', 'Armed to da Teef'],
            magic_banners=filter_items(orcs_magic_standards, 50),
            options=[OptionsList('arm', '', [['Shield', 1, 'sh']])],
            unit_options=[self.up]

        )

    def check_rules(self):
        self.up.set_active(self.get_roster().has_grimgor())
        FCGUnit.check_rules(self)

    def is_immortulz(self):
        return self.up.get('imm')


class BoarBoyz(FCGUnit):
    name = 'Orc Boar Boyz'

    def __init__(self):
        self.big = OptionsList('opt', '', [['Big \'Un', 4, 'bu']])
        FCGUnit.__init__(
            self, model_name='Orc Boar Boy', model_price=16, min_models=5,
            musician_price=10, standard_price=10, champion_name='Orc Boar Boy Boss', champion_price=15,
            base_gear=['Light armour', 'Hand weapon'],
            magic_banners=filter_items(orcs_magic_standards, 50),
            options=[OptionsList('wep', 'Weapon', [
                ['Spear', 2, 'sp'],
                ['Shield', 2, 'sh'],
            ]), self.big]
        )

    def is_big_uns(self):
        return self.big.get('bu')


class SavageBoarBoyz(FCGUnit):
    name = 'Savage Orc Boar Boyz'

    def __init__(self):
        self.big = OptionsList('opt', '', [['Big \'Un', 4, 'bu']])
        self.wep = OptionsList('wep', 'Weapon', [
            ['Spear', 2, 'sp'],
            ['Hand weapon', 2, 'hw'],
            ['Shield', 2, 'sh']
        ])
        FCGUnit.__init__(
            self, model_name='Savage Orc Boar Boy', model_price=18, min_models=5,
            musician_price=10, standard_price=10, champion_name='Savage Orc Boss', champion_price=15,
            base_gear=['Hand weapon'],
            magic_banners=filter_items(orcs_magic_standards, 50),
            options=[self.wep, self.big]
        )

    def check_rules(self):
        self.wep.set_active_options(['hw'], not self.wep.get('sh') and not self.wep.get('sp'))
        self.wep.set_active_options(['sh', 'sp'], not self.wep.get('hw'))
        FCGUnit.check_rules(self)

    def is_big_uns(self):
        return self.big.get('bu')


class BoarChariot(Unit):
    name = 'Orc Boar Chariot'
    base_points = 85

    def __init__(self, boss=False):
        Unit.__init__(self)
        self.boss = boss
        self.add = self.opt_options_list('Options', [['Extra Orc Crew', 5, 'oc']])

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        crew = 2 - (1 if self.boss else 0) + (1 if self.add.get('oc') else 0)
        self.description.add(ModelDescriptor(name='Orc Crew', count=crew, gear=['Hand weapon', 'Spear']).build())
        self.description.add(ModelDescriptor(name='War Boar', count=2).build())


class WolfChariot(Unit):
    name = 'Goblin Wolf Chariot'
    base_points = 50

    def __init__(self, boss=False):
        Unit.__init__(self)
        if not boss:
            self.count = self.opt_count('Goblin Wolf Chariot', 1, 3, self.base_points)
        self.boss = boss
        self.add = self.opt_options_list('Options', [
            ['Extra Goblin Crew', 5, 'cw'],
            ['Extra Giant Wolf', 5, 'gw']
        ])

    def check_rules(self):
        exclude = [self.count.id] if not self.boss else []
        self.set_points(self.build_points(exclude=exclude))
        self.build_description(options=[])
        crew = 2 - (1 if self.boss else 0) + (1 if self.add.get('cw') else 0)
        wolfs = 2 + (1 if self.add.get('gw') else 0)
        self.description.add(ModelDescriptor(name='Goblin Crew', count=crew,
                                             gear=['Hand weapon', 'Spear', 'Short bow']).build())
        self.description.add(ModelDescriptor(name='Giant Wolf', count=wolfs).build())


class SpearChukka(Unit):
    name = 'Goblin Spear Chukka'
    base_points = 35

    def __init__(self):
        Unit.__init__(self)
        self.add = self.opt_options_list('Options', [
            ['Orc Bully', 10, 'orc'],
        ])

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Spear Chukka', count=1).build())
        self.description.add(ModelDescriptor(name='Goblin Crew', count=3, gear=['Hand weapon']).build())
        if self.add.get('orc'):
            self.description.add(ModelDescriptor(name='Orc Bully', count=1,
                                                 gear=['Hand weapon', 'Light armour']).build())


class SquigHerds(Unit):
    name = 'Night Goblin Squig Herd'
    max_models = 100

    def __init__(self):
        Unit.__init__(self)
        self.sq = self.opt_count('Cave Squig', 0, self.max_models, 8)
        self.sq.set(8)
        self.herd = self.opt_count('Night Goblin Herder', 2, self.max_models, 3)

    def get_count(self):
        return self.sq.get() + self.herd.get()

    def check_rules(self):
        if self.get_count() < 10:
            self.error('Night Goblin Squig Herd unit must have 10+ models (taken: {})'.format(self.get_count()))
        self.herd.update_range(int(self.sq.get() / 3), self.max_models)
        self.set_points(self.build_points(count=1))
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Cave Squig', count=self.sq.get()).build())
        self.description.add(ModelDescriptor(name='Night Goblin Herder', count=self.herd.get(),
                                             gear=['Hand weapon']).build())


class SquigHopper(FCGUnit):
    name = 'Night Goblin Squig Hoppers'

    def __init__(self):
        FCGUnit.__init__(self, model_name='Squig Hoppers', model_price=12, min_models=5, base_gear=['Hand weapon'])


class Snotling(FCGUnit):
    name = 'Snotlings'

    def __init__(self):
        FCGUnit.__init__(self, model_name='Snotling', model_price=30, min_models=2,
                         base_gear=['Hand weapon', 'Explodin\' Spores'])


class Trolls(FCGUnit):
    name = 'Trolls'

    def __init__(self):
        FCGUnit.__init__(self, model_name='Troll', model_price=35, min_models=1,
                         base_gear=['Bone, club or bit of tree'])
