__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.orcs_and_goblins.mounts import *
from builder.games.whfb.orcs_and_goblins.armory import orcs_magic_weapons_heroes, orcs_armours_heroes, \
    orcs_arcane_items_heroes, orcs_enchanted_items_heroes, talismans_heroes, magic_shields_ids, orcs_armour_suits_ids, \
    magic_shields_heroes, build_magic_levels, orcs_magic_standards
from builder.games.whfb.orcs_and_goblins.special import BoarChariot, WolfChariot
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor


class Gitilla(StaticUnit):
    name = 'Gitilla da Hunter'
    base_points = 110
    gear = ['Spear', 'Bone Bow', 'Stinky Pelt']

    def __init__(self):
        StaticUnit.__init__(self)
        self.description.add(ModelDescriptor(name='Ulda the Great Wolf', count=1).build())


class Snagla(StaticUnit):
    name = 'Snagla Grobspit'
    base_points = 115
    gear = ['Sting of Snagla', 'Fangspike', 'Shield']

    def __init__(self):
        StaticUnit.__init__(self)
        self.description.add(ModelDescriptor(name='Giant Spider', count=1).build())


class OrcBigBoss(Unit):
    name = 'Orc Big Boss'
    base_gear = ['Hand weapon']
    base_points = 55

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 4, 'gw'],
            ['Hand weapon', 2, 'hw'],
            ['Spear', 2, 'sp']
        ], limit=1)
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 2, 'sh'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [WarBoar(price=16), BoarChariot(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons_heroes, 1)
        self.magic_arm = self.opt_options_list('Magic armour', orcs_armours_heroes, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans_heroes, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items_heroes, 1)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', orcs_magic_standards, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal,  self.magic_enc]

    def check_rules(self):
        self.wep.set_visible_options(['hw'], self.steed.get_count() == 0)
        self.wep.set_visible_options(['sp'], self.steed.get_count() > 0)
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(magic_shields_ids))
        self.gear = self.base_gear + (['Light Armour'] if not self.magic_arm.is_selected(orcs_armour_suits_ids) else [])
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])

    def is_bearer(self):
        return self.banner.get('bs')


class BlackOrcBigBoss(Unit):
    name = 'Black Orc Big Boss'
    base_gear = ['Armed to da Teef']
    base_points = 90

    def __init__(self):
        Unit.__init__(self)
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 2, 'sh'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [WarBoar(price=16), BoarChariot(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons_heroes, 1)
        self.magic_arm = self.opt_options_list('Magic armour', orcs_armours_heroes, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans_heroes, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items_heroes, 1)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', orcs_magic_standards, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal,  self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(magic_shields_ids))
        self.gear = self.base_gear + (['Heavy Armour'] if not self.magic_arm.is_selected(orcs_armour_suits_ids) else [])
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])

    def is_bearer(self):
        return self.banner.get('bs')


class SavageOrcBigBoss(Unit):
    name = 'Savage Orc Big Boss'
    gear = ['Hand weapon']
    base_points = 75

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 4, 'gw'],
            ['Hand weapon', 2, 'hw'],
            ['Spear', 2, 'sp']
        ], limit=1)
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 2, 'sh'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [WarBoar(price=16)])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons_heroes, 1)
        self.magic_arm = self.opt_options_list('Magic armour', magic_shields_heroes, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans_heroes, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items_heroes, 1)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', orcs_magic_standards, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal,  self.magic_enc]

    def check_rules(self):
        self.wep.set_visible_options(['sp'], self.steed.get_count() > 0)
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(magic_shields_ids))
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])

    def is_bearer(self):
        return self.banner.get('bs')


class GoblinBigBoss(Unit):
    name = 'Goblin Big Boss'
    gear = ['Hand weapon']
    base_points = 35

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 4, 'gw'],
            ['Hand weapon', 2, 'hw'],
            ['Spear', 2, 'sp']
        ], limit=1)
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 2, 'sh'],
            ['Light armour', 2, 'la'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [GiantWolf(price=12), GiantSpider(price=15),
                                                          GiganticSpider(price=40), WolfChariot(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons_heroes, 1)
        self.magic_arm = self.opt_options_list('Magic armour', orcs_armours_heroes, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans_heroes, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items_heroes, 1)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', orcs_magic_standards, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.wep.set_visible_options(['hw'], self.steed.get_count() == 0)
        self.wep.set_visible_options(['sp'], self.steed.get_count() > 0)
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(magic_shields_ids))
        self.eq.set_active_options(['la'], not self.magic_arm.is_selected(orcs_armour_suits_ids))
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])

    def is_bearer(self):
        return self.banner.get('bs')


class NightGoblinBigBoss(Unit):
    name = 'Night Goblin Big Boss'
    gear = ['Hand weapon']
    base_points = 30

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 4, 'gw'],
            ['Hand weapon', 2, 'hw'],
            ['Spear', 2, 'sp'],
            ['Short bow', 2, 'bow']
        ], limit=1)
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 2, 'sh'],
            ['Light armour', 2, 'la'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [GreatCaveSquig(price=50)])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons_heroes, 1)
        self.magic_arm = self.opt_options_list('Magic armour', orcs_armours_heroes, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans_heroes, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items_heroes, 1)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', orcs_magic_standards, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal,  self.magic_enc]

    def check_rules(self):
        self.wep.set_visible_options(['hw'], self.steed.get_count() == 0)
        self.wep.set_visible_options(['sp'], self.steed.get_count() > 0)
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(magic_shields_ids))
        self.eq.set_active_options(['la'], not self.magic_arm.is_selected(orcs_armour_suits_ids))
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])

    def is_bearer(self):
        return self.banner.get('bs')


class OrcShaman(Unit):
    name = 'Orc Shaman'
    base_gear = ['Hand weapon', 'Spells of do Big Waaagh!']
    base_points = 65

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [WarBoar(price=16)])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons_heroes, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans_heroes, 1)
        self.magic_arc = self.opt_options_list('Arcane item', orcs_arcane_items_heroes, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items_heroes, 1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class SavageOrcShaman(Unit):
    name = 'Savage Orc Shaman'
    base_gear = ['Hand weapon', 'Spells of do Big Waaagh!']
    base_points = 70

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [WarBoar(price=16)])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons_heroes, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans_heroes, 1)
        self.magic_arc = self.opt_options_list('Arcane item', orcs_arcane_items_heroes, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items_heroes, 1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class GoblinShaman(Unit):
    name = 'Goblin Shaman'
    base_gear = ['Hand weapon', 'Spells of do Little Waaagh!']
    base_points = 55

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [GiantWolf(), WolfChariot()])
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons_heroes, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans_heroes, 1)
        self.magic_arc = self.opt_options_list('Arcane item', orcs_arcane_items_heroes, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items_heroes, 1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class NightGoblinShaman(Unit):
    name = 'Night Goblin Shaman'
    base_gear = ['Hand weapon', 'Magic Mushrooms', 'Spells of do Little Waaagh!']
    base_points = 50

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.magic_wep = self.opt_options_list('Magic weapon', orcs_magic_weapons_heroes, 1)
        self.magic_tal = self.opt_options_list('Talisman', talismans_heroes, 1)
        self.magic_arc = self.opt_options_list('Arcane item', orcs_arcane_items_heroes, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', orcs_enchanted_items_heroes, 1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])
