__author__ = 'Denis Romanov'


from builder.games.whfb.legacy_armory import *

orcs_magic_weapons = [
    ['Battle Axe of the Last Waaagh!', 100, 'orcs_balw'],
    ['Basha\'s Axe of Stunty Smashin\'', 50, 'orcs_bass']
] + magic_weapons

orcs_armours = [
    ['Armour of Gork', 100, 'orcs_ag'],
] + magic_armours

orcs_armour_suits_ids = ['orcs_ag'] + magic_armour_suits_ids

orcs_arcane_items = [
    ['Lucky Srhunken Head', 50, 'orcs_lsh'],
] + arcane_items


orcs_enchanted_items = [
    ['Skull Wand of Kaloth', 75, 'orcs_swk']
] + enchanted_items

orcs_magic_standards = [
    ['Mork\'s War Banner', 100, 'orcs_mork'],
    ['Spider Banner', 85, 'orcs_sp'],
    ['Bad Moon Banner', 50, 'orcs_bmb'],
] + magic_standards


orcs_armours_heroes = filter_items(orcs_armours, 50)
orcs_magic_weapons_heroes = filter_items(orcs_magic_weapons, 50)
magic_shields_heroes = filter_items(magic_shields, 50)
talismans_heroes = filter_items(talismans, 50)
orcs_arcane_items_heroes = filter_items(orcs_arcane_items, 50)
orcs_enchanted_items_heroes = filter_items(orcs_enchanted_items, 50)