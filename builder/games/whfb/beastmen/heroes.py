__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.beastmen.armory import *
from builder.core.options import norm_point_limits
from builder.games.whfb.beastmen.special import RazorgorChariot
from builder.games.whfb.beastmen.core import TuskgorChariot
from builder.core.model_descriptor import ModelDescriptor


heroes_weapon = filter_items(beast_weapon, 50)
heroes_armour = filter_items(beast_armour, 50)
heroes_talisman = filter_items(beast_talisman, 50)
heroes_arcane = filter_items(beast_arcane, 50)
heroes_enchanted = filter_items(beast_enchanted, 50)
heroes_gifts = filter_items(gifts, 50)


class Morghul(StaticUnit):
    name = 'Morghul, Master of Skulls'
    base_points = 280
    gear = ['Hand weapon']


class Slugtongue(Unit):
    name = 'Slugtongue'
    base_points = 190
    gear = ['Level 2 Wizard', 'Braystaff']

    def __init__(self):
        Unit.__init__(self)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Wild', 0, 'wild'],
            ['The Lore of Death', 0, 'death'],
        ], limit=1)


class Moonclaw(Unit):
    name = 'Moonclaw, Son of Morrislieb'
    base_points = 200
    gear = ['Level 1 Wizard', 'Braystaff']

    def __init__(self):
        Unit.__init__(self)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Wild', 0, 'wild'],
            ['The Lore of Shadow', 0, 'shadow'],
        ], limit=1)
        self.eq = self.opt_options_list('Steed', [
            ['Umbralok', 35, 'umb'],
        ])

    def check_rules(self):
        self.points.set(self.build_points())
        self.build_description(exclude=[self.eq.id])
        if self.eq.get('umb'):
            self.description.add(ModelDescriptor(name='Umbralok', count=1, points=35).build())


class Wargor(Unit):
    name = 'Wargor'
    gear = ['Hand Weapon']
    base_points = 85

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 4, 'gw'],
            ['Hand weapon', 4, 'hw'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 2, 'sh'],
        ])
        self.arm = self.opt_options_list('', [
            ['Light armour', 2, 'la'],
            ['Heavy armour', 4, 'ha'],
        ], limit=1)
        self.steed = self.opt_optional_sub_unit('Steed', [TuskgorChariot(boss=True), RazorgorChariot(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic_gifts = self.opt_options_list('Gifts of Chaos', heroes_gifts, points_limit=50)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', beast_standard, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(beast_shields_ids))
        self.arm.set_visible(not self.magic_arm.is_selected(beast_armour_suits_ids))
        self.magic_enc.set_visible_options(['beast_skin'], False)
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic + [self.magic_gifts])
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])


class Gorebull(Unit):
    name = 'Doombull'
    gear = ['Hand Weapon']
    base_points = 160

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 10, 'gw'],
            ['Hand weapon', 5, 'hw'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 4, 'sh'],
        ])
        self.arm = self.opt_options_list('', [
            ['Light armour', 4, 'la'],
            ['Heavy armour', 8, 'ha'],
        ], limit=1)
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic_gifts = self.opt_options_list('Gifts of Chaos', heroes_gifts, points_limit=50)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', beast_standard, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(beast_shields_ids))
        self.arm.set_visible(not self.magic_arm.is_selected(beast_armour_suits_ids))
        self.magic_enc.set_visible_options(['beast_skin', 'beast_hunt', 'beast_first'], False)
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic + [self.magic_gifts])
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])


class BrayShaman(Unit):
    name = 'Bray-Shaman'
    gear = ['Braystaff']
    base_points = 75

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 2, 'hw'],
        ], limit=1)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [TuskgorChariot(boss=True), RazorgorChariot(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', heroes_arcane, 1)
        self.magic_gifts = self.opt_options_list('Gifts of Chaos', heroes_gifts)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Wild', 0, 'wild'],
            ['The Lore of Beasts', 0, 'beast'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
        ], limit=1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.magic_enc.set_visible_options(['beast_hunt', 'beast_first'], False)
        norm_point_limits(50, self.magic + [self.magic_gifts])
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])
