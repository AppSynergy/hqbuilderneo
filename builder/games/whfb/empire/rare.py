__author__ = 'Denis Romanov'

from builder.games.whfb.empire.special import Cannon
from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor


class Helblaster(Cannon):
    name = 'Helblaster Volley Gun'
    base_points = 120


class Helstorm(Cannon):
    name = 'Helstorm Rocket Battery'
    base_points = 120


class Tank(Unit):
    name = 'Steam Tank'
    base_points = 250

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('', [
            ['Hochland long rifle', 20, 'hlr'],
        ])

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name=self.name, count=1,
                                             gear=['Steam Cannon', 'Steam Gun', 'Steam Engine']).build())
        self.description.add(ModelDescriptor(name='Engineer Commander', count=1,
                                             gear=['Hand weapon', 'Repeater pistol']).add_gear_opt(self.opt).build())


class Hurricanum(Unit):
    name = 'Celestial Hurricanum'
    base_points = 130
    static = True

    def __init__(self, boss=False):
        self.boss = boss
        Unit.__init__(self)

    def check_rules(self):
        Unit.check_rules(self)
        crew = 1 if self.boss else 2
        self.description.add(ModelDescriptor(name='Acolyte', count=crew, gear=['Hand weapon']).build())
        self.description.add(ModelDescriptor(name='Warhorse', count=2).build())


class Luminark(Hurricanum):
    name = 'Luminark of Hysh'
    base_points = 120
