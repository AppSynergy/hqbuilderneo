__author__ = 'Denis Romanov'
from builder.core.unit import Unit, OptionsList, OneOf
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.empire.armory import *
from builder.games.whfb.empire.core import EmpireUnit


class Greatswords(EmpireUnit):
    name = 'Greatswords'
    base_gear = ['Great weapon', 'Full plate armour']
    model_price = 11

    def __init__(self):
        EmpireUnit.__init__(
            self, model_name='Greatsword', model_price=self.model_price, min_models=10,
            musician_price=10, standard_price=10, champion_name='Count\'s champion', champion_price=10,
            base_gear=self.base_gear, magic_banners=filter_items(empire_standard, 50),
            regiment=True
        )


class Demigryph(FCGUnit):
    name = 'Demigryph Knights'
    base_gear = ['Hand weapon', 'Full plate armour', 'Barding', 'Shield']
    model_price = 58

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Inner circle knight', model_price=self.model_price, min_models=3,
            musician_price=10, standard_price=10, champion_name='Inner circle preceptor', champion_price=10,
            base_gear=self.base_gear, magic_banners=filter_items(empire_standard, 50),
            options=[
                OneOf('arm', 'Weapon', [
                    ['Lance', 0, 'ln'],
                    ['Halberd', 0, 'hb'],
                ])
            ],
        )


class Reiksguard(FCGUnit):
    name = 'Reiksguard Knights'
    base_gear = ['Hand weapon', 'Full plate armour', 'Barding', 'Shield', 'Lance']
    model_price = 27

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Reiksguard knight', model_price=self.model_price, min_models=5,
            musician_price=10, standard_price=10, champion_name='Reikscaptain', champion_price=10,
            base_gear=self.base_gear, magic_banners=filter_items(empire_standard, 50),
        )


class Huntsmen(FCGUnit):
    name = 'Huntsmen'
    base_gear = ['Hand weapon', 'Bow']
    model_price = 8

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Huntsman', model_price=self.model_price, min_models=10,
            musician_price=10, standard_price=10, champion_name='Tracker', champion_price=10,
            base_gear=self.base_gear, magic_banners=filter_items(empire_standard, 50),
        )


class Pistoliers(FCGUnit):
    name = 'Pistoliers'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Pistolier', model_price=18, min_models=5,
            musician_price=10, champion_name='Outrider', champion_price=10,
            base_gear=['Light armour'], rank_gear=['Brace of Pistols'],
            champion_options=[OneOf('arm', 'Outrider\'s weapon', [
                ['Brace of Pistols', 0, 'bp'],
                ['Brace of Pistols including repeater pistol', 10, 'bpr'],
                ['Repeater handgun', 10, 'rh'],
            ])]
        )


class Outriders(FCGUnit):
    name = 'Outriders'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Outrider', model_price=21, min_models=5,
            musician_price=10, champion_name='Sharpshooter', champion_price=10,
            base_gear=['Hand weapon', 'Light armour'], rank_gear=['Repeater handgun'],
            champion_options=[OneOf('wep', 'Sharpshooter\'s weapon', [
                ['Repeater handgun', 0, 'bp'],
                ['Brace of Pistols including repeater pistol', 10, 'bpr'],
                ['Grenade launching blunderbuss', 10, 'rh'],
            ])],
            options=[
                OptionsList('arm', 'Options', [['Barding', 1, 'br']])
            ]
        )


class Cannon(Unit):
    name = 'Great Cannon'
    base_points = 120
    static = True

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name=self.name, count=1).build())
        self.description.add(ModelDescriptor(name='Crewman', count=3, gear=['Hand weapon']).build())


class Mortar(Cannon):
    name = 'Mortar'
    base_points = 100


class Flagellants(FCGUnit):
    name = 'Flagellants'
    base_gear = ['Flail']
    model_price = 12

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Flagellant', model_price=self.model_price, min_models=10,
            champion_name='Prophet of Doom', champion_price=10,
            base_gear=self.base_gear
        )
