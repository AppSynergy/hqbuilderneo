__author__ = 'Denis Romanov'

from builder.games.whfb.unit import FCGUnit
from builder.core.unit import OptionsList, SubUnit, OneOf
from armory import *


class EmpireUnit(FCGUnit):

    def process_detachments(self, item_id, count=2):
        selected = sum((1 for i in range(count) if self.detachments.get(multiple_id(item_id, i))))
        for i in range(count):
            full_id = multiple_id(item_id, i)
            if self.detachments.get(full_id) or selected >= 0:
                self.detachments.set_visible_options([multiple_id(item_id, i)], True)
            else:
                self.detachments.set_visible_options([multiple_id(item_id, i)], False)
            selected -= 1

    class Detach(object):
        def __init__(self, unit_class, index):
            self.index = index
            self.unit = unit_class(True)
            self.base_id = unit_class.__name__.lower()
            self.id = multiple_id(self.base_id, index)
            self.sub_unit = SubUnit(self.id, self.unit)

        @property
        def selector(self):
            return [self.unit.name, 0, self.id]

    def __init__(self, *args, **kwargs):
        self.detachment = kwargs.get('detachment', False)
        self.regiment = kwargs.get('regiment', False)
        if self.detachment:
            kwargs['musician_price'] = None
            kwargs['standard_price'] = None
            kwargs['champion_name'] = None
            kwargs['champion_price'] = None
            kwargs['champion_options'] = None
            kwargs['min_models'] = 5
        if 'detachment' in kwargs.keys():
            del kwargs['detachment']
        if 'regiment' in kwargs.keys():
            del kwargs['regiment']
        FCGUnit.__init__(self, *args, **kwargs)
        if self.regiment:
            self.detach_units = [self.Detach(unit, i) for unit in detach_class for i in range(2)]
            self.base_ids = set((u.base_id for u in self.detach_units))
            self.detachments = self.opt_options_list('Detachments', [u.selector for u in self.detach_units], limit=2)
            for detach in self.detach_units:
                self.append_opt(detach.sub_unit)

    def check_rules(self):
        if self.regiment:
            for detach in self.detach_units:
                detach.sub_unit.set_active(self.detachments.get(detach.id))
                if detach.unit.get_count() * 2 > self.get_count():
                    detach.unit.error('Detachment\'s size must be less then half the number of models in Regimental '
                                      'Unit')
            for unit_id in self.base_ids:
                self.process_detachments(unit_id)
        FCGUnit.check_rules(self)
        if self.regiment:
            for detach in self.detach_units:
                self.description.add(detach.sub_unit.get_selected())


class Halberdiers(EmpireUnit):
    name = 'Halberdiers'
    base_gear = ['Halberd', 'Light armour']
    model_price = 6

    def __init__(self, detachment=False):
        EmpireUnit.__init__(
            self, model_name='Empire soldier', model_price=self.model_price, min_models=10,
            musician_price=10, standard_price=10, champion_name='Sergeant', champion_price=10,
            base_gear=self.base_gear,
            options=[
                OptionsList('arm', '', [['Shield', 1, 'sh']])
            ],
            detachment=detachment, regiment=not detachment
        )


class Spearmen(Halberdiers):
    name = 'Spearmen'
    base_gear = ['Spear', 'Light armour']
    model_price = 5


class Swordsmen(EmpireUnit):
    name = 'Swordsmen'
    base_gear = ['Sword', 'Shield', 'Light armour']
    model_price = 7
    sergeant = 'Duellist'

    def __init__(self, detachment=False):
        EmpireUnit.__init__(
            self, model_name='Empire soldier', model_price=self.model_price, min_models=10,
            musician_price=10, standard_price=10, champion_name=self.sergeant, champion_price=10,
            base_gear=self.base_gear, detachment=detachment, regiment=not detachment
        )


class Crossbowmen(Swordsmen):
    name = 'Crossbowmen'
    base_gear = ['Hand weapon', 'Crossbow']
    model_price = 9
    sergeant = 'Marksman'


class Archers(Crossbowmen):
    name = 'Archers'
    base_gear = ['Hand weapon', 'Bow']
    model_price = 7


class Handgunners(EmpireUnit):
    name = 'Handgunners'

    def __init__(self, detachment=False):
        EmpireUnit.__init__(
            self, model_name='Empire soldier', model_price=9, min_models=10,
            musician_price=10, standard_price=10, champion_name='Marksman', champion_price=10,
            base_gear=['Hand weapon'], rank_gear=['Handgun'], detachment=detachment,
            champion_options=[OneOf('arm', 'Marksman\'s weapon', [
                ['Handgun', 0, 'hg'],
                ['Brace of Pistols', 5, 'bp'],
                ['Hochland long rifle', 20, 'hlr'],
                ['Repeater handgun', 10, 'rh'],
            ])]
        )


class Militia(EmpireUnit):
    name = 'Free Company Militia'
    base_gear = ['Hand weapon' for _ in range(2)]
    model_price = 6

    def __init__(self, detachment=False):
        EmpireUnit.__init__(
            self, model_name='Militia Fighter', model_price=self.model_price, min_models=10,
            musician_price=10, standard_price=10, champion_name='Militia Leader', champion_price=10,
            base_gear=self.base_gear, detachment=detachment, regiment=False
        )


class Knights(FCGUnit):
    name = 'Knightly Orders'

    def __init__(self):
        self.big = OptionsList('opt', '', [['Inner Circle', 3, 'ic']])
        FCGUnit.__init__(
            self, model_name='Empire knight', model_price=22, min_models=5,
            musician_price=10, standard_price=10, champion_name='Preceptor', champion_price=10,
            base_gear=['Full plate armour', 'Hand weapon', 'Barding'],
            magic_banners=filter_items(empire_standard, 50), magic_banner_check=lambda: self.big.get('ic'),
            options=[OneOf('wep', 'Weapon', [
                ['Lance and Shield', 0, 'sp'],
                ['Great weapon', 0, 'gw'],
            ]), self.big]
        )

    def get_unique(self):
        if self.big.get('ic'):
            return ['Inner Circle Knights']

detach_class = [Halberdiers, Spearmen, Swordsmen, Crossbowmen, Handgunners, Archers, Militia]

