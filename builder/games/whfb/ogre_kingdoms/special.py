__author__ = 'Denis Romanov'
from builder.core.unit import Unit, OptionsList, OneOf, ListSubUnit
from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.ogre_kingdoms.armory import ok_standard, filter_items

unit_standards = filter_items(ok_standard, 50)


class Leadbelchers(FCGUnit):
    name = 'Leadbelchers'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Leadbelcher', model_price=43, min_models=3,
            musician_price=10, musician_name='Bellower', champion_name='Thunderfist', champion_price=10,
            base_gear=['Hand weapon', 'Leadbelcher gun', 'Light armour'],
        )


class Sabretusk(FCGUnit):
    name = 'Sabretusk Pack'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Sabretusk', model_price=21, min_models=1, max_model=10,
        )


class Yhetees(FCGUnit):
    name = 'Yhetees'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Yhetee', model_price=44, min_models=3,
            champion_name='Greyback', champion_price=10
        )


class Gorger(Unit):
    name = 'Gorger'
    base_points = 90

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count(self.name, 1, 1, self.base_points)

    def check_rules(self):
        self.count.update_range(new_min=1, new_max=2 if self.get_roster().has_skrag() else 1)
        self.points.set(self.build_points(exclude=[self.count.id]))
        self.build_description(exclude=[self.count.id])


class Maneaters(Unit):
    name = 'Maneaters'

    class Maneater(ListSubUnit):
        name = 'Maneater'
        base_points = 50
        gear = ['Hand weapon']

        def __init__(self):
            ListSubUnit.__init__(self, min_models=3, max_models=100)
            self.wep = self.opt_options_list('Weapon', [
                ['Hand weapon', 2],
                ['Great weapon', 11],
                ['Ogre pistol', 7],
                ['Brace of Ogre pistols', 12],
            ], limit=1)
            fcg = [
                ['Maneater Captain', 10, 'chmp'],
                ['Standard bearer', 10, 'sb'],
                ['Bellower', 10, 'mus']
            ]
            self.command_group = self.opt_options_list('Command group', fcg, limit=1)
            self.ban = self.opt_options_list('Magic Standard', unit_standards, limit=1)

        def captain(self):
            return self.get_count() if self.command_group.get('chmp') else 0

        def bellower(self):
            return self.get_count() if self.command_group.get('mus') else 0

        def standard(self):
            return self.get_count() if self.command_group.get('sb') else 0

        def update_options(self, arm):
            self.ban.set_visible(self.command_group.get('sb'))
            exclude = [self.count.id]
            single_points = self.build_points(exclude=exclude, count=1) + arm.points()
            self.points.set(single_points * self.get_count())
            name = self.name
            if self.command_group.get('chmp'):
                exclude.append(self.command_group.id)
                name = 'Maneater Captain'
            self.build_description(points=single_points, count=1, exclude=exclude, add=[arm.get_selected()], name=name)

        def check_rules(self):
            pass

        def get_unique_gear(self):
            return self.ban.get_selected() if self.ban.is_any_selected() else []


    def __init__(self):
        Unit.__init__(self)
        self.models = self.opt_units_list(self.Maneater.name, self.Maneater, 3, 100)
        self.arm = self.opt_one_of('Armour', [
            ['Light armor', 0, 'la'],
            ['Heavy armor', 4, 'ha'],
        ])
        self.gnoblar = self.opt_options_list('', [
            ['Look-out Gnoblar', 5, 'log'],
        ])
        self.golgfag = self.opt_options_list('Golgfag\'s upgrade', [
            ['Golgfag\'s Maneaters', 0, 'gl']
        ])

    def is_golgfag_up(self):
        return self.golgfag.get('gl')

    def check_rules(self):
        self.models.update_range()
        self.golgfag.set_active_all(self.get_roster().has_goldfag())
        for unit in self.models.get_units():
            unit.update_options(self.arm)
        if sum((unit.captain() for unit in self.models.get_units())) > 1:
            self.error('Only one Maneater may be upgraded to Maneater Captain')
        if sum((unit.bellower() for unit in self.models.get_units())) > 1:
            self.error('Only one Maneater may be upgraded to Bellower')
        flag = sum((unit.standard() for unit in self.models.get_units()))
        if flag > 1:
            self.error('Only one Maneater may be upgraded to Standard bearer')
        self.gnoblar.set_visible(flag > 0)

        self.points.set(self.build_points(exclude=[self.arm.id]))
        self.build_description(exclude=[self.arm.id])

    def get_unique_gear(self):
        return sum((unit.get_unique_gear() for unit in self.models.get_units()), [])

class Mournfang(FCGUnit):
    name = 'Mournfang Cavalry'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Ogre', model_price=60, min_models=2,
            musician_price=10, musician_name='Bellower', standard_price=10, champion_name='Crusher', champion_price=10,
            magic_banners=unit_standards,
            base_gear=['Hand weapon'],
            options=[
                OneOf('arm', 'Armour', [
                    ['Light armor', 0, 'la'],
                    ['Heavy armor', 5, 'ha'],
                ]),
                OptionsList('wep', 'Weapon', [
                    ['Great weapon', 8, 'gw'],
                    ['Ironfist', 5, 'if'],
                ], limit=1),
            ],
            champion_options=[
                OptionsList('ch_wep', 'Crusher\'s weapon', [
                    ['Brace of Ogre pistols', 6, 'pis'],
                ]),
            ]
        )
