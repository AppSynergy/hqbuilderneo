__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.warriors_of_chaos.mounts import *
from builder.games.whfb.warriors_of_chaos.armory import *
from builder.games.whfb.warriors_of_chaos.special import GorebeastChariot, Warshine
from builder.games.whfb.warriors_of_chaos.core import ChaosChariot
from builder.core.options import norm_point_limits


class Archaon(Unit):
    name = 'Archaon'
    base_points = 580
    unique = True
    gear = ['Level 2 Wizard', 'The Slayer of Kings', 'Armour of Morkar', 'Crown of Domination', 'Eye of Sheerian',
            'Shield']

    class Dorghar(StaticUnit):
        name = 'Dorghar'
        base_points = 70

    def __init__(self):
        Unit.__init__(self)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Fire', 0, 'fire'],
            ['The Lore of Metal', 0, 'metal'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
            ['The Lore of Tzeentch', 0, 'tzeentch'],
        ], limit=1)
        self.steed = self.opt_optional_sub_unit('Steed', [self.Dorghar()])


class Galrauch(StaticUnit):
    name = 'Galrauch'
    base_points = 510
    gear = ['Level 4 Wizard', 'The Lore of Tzeentch']


class Kholek(StaticUnit):
    name = 'Kholek Suneater'
    base_points = 545
    gear = ['Starcrusher']


class Sigvald(StaticUnit):
    name = 'Sigvald the Magnificent'
    base_points = 375
    gear = ['Silverslash', 'Auric Armour', 'Shield']


class Valkia(StaticUnit):
    name = 'Valkia the Bloody'
    base_points = 405
    gear = ['The Spear of Slaupnir', 'The Scarlet Armour', 'Daemonshield', 'Mark of Khorne']


class Vilitch(StaticUnit):
    name = 'Vilitch the Curseling'
    base_points = 380
    gear = ['Hand weapon', 'Hand weapon', 'Vessel of Chaos', 'Level 4 Wizard', 'The Lore of Tzeentch',
            'Mark of Tzeench']


class ChaosLord(Unit):
    name = 'Chaos Lord'
    gear = ['Hand weapon']
    base_points = 210

    def __init__(self):
        Unit.__init__(self)
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Khorne', 10, 'kh'],
            ['Mark of Tzeentch', 10, 'tz'],
            ['Mark of Nurgle', 10, 'ng'],
            ['Mark of Slaanesh', 5, 'sl'],
        ], limit=1)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 8, 'gw'],
            ['Flail', 6, 'fl'],
            ['Halberd', 8, 'hl'],
            ['Lance', 8, 'ln'],
            ['Hand weapon', 6, 'hw'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 5, 'sh'],
        ])
        self.rewards = self.opt_options_list('Rewards', rewards, points_limit=50)
        self.steed = self.opt_optional_sub_unit('Steed', [ChaosSteed(), SteedOfSlaanesh(), DiskOfTzeentch(),
                                                          DemonicMount(), PalanquinOfNurgle(), JuggernautOfKhorne(),
                                                          ChaosChariot(boss=self), GorebeastChariot(boss=self),
                                                          Warshine(boss=self), Manticore(), Dragon()])
        self.magic_wep = self.opt_options_list('Magic weapon', woc_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', woc_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', woc_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', woc_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    @property
    def mark(self):
        if not self.marks.is_any_selected():
            return
        return self.marks.get_all()[0]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(woc_shields_ids))
        self.gear = ['Hand weapon'] + (['Chaos armour'] if not self.magic_arm.is_selected(woc_armour_suits_ids) else [])

        self.magic_wep.set_active_options(['woc_fm'], self.marks.get('ng'))
        self.magic_enc.set_active_options(['woc_sl'], self.marks.get('sl'))
        self.rewards.set_active_options(['collar'], self.marks.get('kh') and not self.magic_tal.is_any_selected())
        self.rewards.set_active_options(['all'], self.marks.get('sl'))
        self.rewards.set_active_options(['eye'], self.marks.get('tz'))
        self.rewards.set_active_options(['rot'], self.marks.get('ng'))
        self.rewards.set_active_options(['blade'], not self.magic_wep.is_any_selected())
        self.rewards.set_visible_options(['cf'], False)  # Arcane item - mage only
        self.magic_wep.set_active(not self.rewards.get('blade'))
        self.magic_tal.set_active(not self.rewards.get('collar'))

        self.steed.set_active_options([SteedOfSlaanesh.name], self.marks.get('sl'))
        self.steed.set_active_options([DiskOfTzeentch.name], self.marks.get('tz'))
        self.steed.set_active_options([PalanquinOfNurgle.name], self.marks.get('nl'))
        self.steed.set_active_options([JuggernautOfKhorne.name], self.marks.get('kh'))

        self.wep.set_active_options(['hw'], self.steed.get_count() == 0)
        self.wep.set_active_options(['ln'], self.steed.get_count() > 0)
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic if m.is_used()), []) + self.rewards.get_selected()


class ChaosSorcererLord(Unit):
    name = 'Chaos Sorcerer Lord'
    gear = ['Hand weapon', 'Chaos armour']
    base_points = 235

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(3, 2, 35))
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Tzeentch', 15, 'tz'],
            ['Mark of Nurgle', 10, 'ng'],
            ['Mark of Slaanesh', 5, 'sl'],
        ], limit=1)
        self.rewards = self.opt_options_list('Rewards', rewards, points_limit=50)
        self.steed = self.opt_optional_sub_unit('Steed', [ChaosSteed(), SteedOfSlaanesh(), DiskOfTzeentch(),
                                                          DemonicMount(), PalanquinOfNurgle(),
                                                          ChaosChariot(boss=self), GorebeastChariot(boss=self),
                                                          Warshine(boss=self), Manticore(), Dragon()])
        self.magic_wep = self.opt_options_list('Magic weapon', woc_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', woc_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', woc_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', woc_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', woc_arcane, 1)
        self.magic = [self.magic_wep, self.magic_tal, self.magic_enc, self.magic_arc, self.magic_arm]
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Fire', 0, 'fire'],
            ['The Lore of Metal', 0, 'metal'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
            ['The Lore of Tzeentch', 0, 'tzeentch'],
            ['The Lore of Nurgle', 0, 'nurgle'],
            ['The Lore of Slaanesh', 0, 'slaanesh'],
        ], limit=1)

    @property
    def mark(self):
        if not self.marks.is_any_selected():
            return
        return self.marks.get_all()[0]

    def check_rules(self):
        self.magic_wep.set_active_options(['woc_fm'], self.marks.get('ng'))
        self.magic_enc.set_active_options(['woc_sl'], self.marks.get('sl'))
        self.rewards.set_visible_options(['collar'], False)
        self.rewards.set_active_options(['all'], self.marks.get('sl'))
        self.rewards.set_active_options(['eye'], self.marks.get('tz'))
        self.rewards.set_active_options(['rot'], self.marks.get('ng'))
        self.rewards.set_active_options(['blade'], not self.magic_wep.is_any_selected())
        self.rewards.set_active_options(['cf'], not self.magic_arc.is_any_selected())
        self.magic_wep.set_active(not self.rewards.get('blade'))
        self.magic_arc.set_active(not self.rewards.get('cf'))

        self.steed.set_active_options([SteedOfSlaanesh.name], self.marks.get('sl'))
        self.steed.set_active_options([DiskOfTzeentch.name], self.marks.get('tz'))
        self.steed.set_active_options([PalanquinOfNurgle.name], self.marks.get('nl'))

        self.gear = ['Hand weapon'] + (['Chaos armour'] if not self.magic_arm.is_selected(woc_armour_suits_ids) else [])

        self.lores.set_active_options(['fire'], not self.marks.is_any_selected())
        self.lores.set_active_options(['metal'], not self.marks.is_any_selected() or self.marks.get('tz'))
        self.lores.set_active_options(['shadow'], not self.marks.is_any_selected() or self.marks.get('sl'))
        self.lores.set_active_options(['death'], not self.marks.is_any_selected() or self.marks.get('ng'))
        self.lores.set_active_options(['tzeentch'], self.marks.get('tz'))
        self.lores.set_active_options(['nurgle'], self.marks.get('ng'))
        self.lores.set_active_options(['slaanesh'], self.marks.get('sl'))

        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic if m.is_used()), []) + self.rewards.get_selected()


class DaemonPrince(Unit):
    name = 'Daemon Prince'
    gear = ['Hand weapon']
    base_points = 235

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_options_list('Magic level', build_magic_levels(1, 4, 35, price_offset=35), limit=1)
        self.marks = self.opt_one_of('Marks', [
            ['Mark of Khorne', 10, 'kh'],
            ['Mark of Tzeentch', 15, 'tz'],
            ['Mark of Nurgle', 10, 'ng'],
            ['Mark of Slaanesh', 5, 'sl'],
        ])
        self.eq = self.opt_options_list('Options', [
            ['Chaos armour', 20, 'ca'],
            ['Daemonic Flight', 40, 'df'],
        ])
        self.rewards = self.opt_options_list('Rewards', rewards, points_limit=100)
        self.magic_wep = self.opt_options_list('Magic weapon', filter_items(woc_weapon, 25), 1)
        self.magic_arm = self.opt_options_list('Magic armour', filter_items(woc_armour, 25), 1)
        self.magic_tal = self.opt_options_list('Talisman', filter_items(woc_talisman, 25), 1)
        self.magic_enc = self.opt_options_list('Enchanted item', filter_items(woc_enchanted, 25), 1)
        self.magic_arc = self.opt_options_list('Arcane item', filter_items(woc_arcane, 25), 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc, self.magic_arc]
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Metal', 0, 'metal'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
            ['The Lore of Tzeentch', 0, 'tzeentch'],
            ['The Lore of Nurgle', 0, 'nurgle'],
            ['The Lore of Slaanesh', 0, 'slaanesh'],
        ], limit=1)

    @property
    def mark(self):
        if not self.marks.is_any_selected():
            return
        return self.marks.get_all()[0]

    def check_rules(self):
        self.mage.set_active(not self.marks.get_cur() == 'kh')
        self.eq.set_active_options(['ca'], not self.magic_arm.is_selected(woc_armour_suits_ids))

        self.magic_arc.set_active_options(self.magic_arc.get_all_ids(), self.mage.is_any_selected())
        self.magic_arm.set_active_options(self.magic_arm.get_all_ids(), not self.mage.is_any_selected())

        self.rewards.set_active_options(['collar'],
                                        self.marks.get_cur() == 'kh' and not self.magic_tal.is_any_selected())
        self.rewards.set_active_options(['all'], self.marks.get_cur() == 'sl')
        self.rewards.set_active_options(['eye'], self.marks.get_cur() == 'tz')
        self.rewards.set_active_options(['rot'], self.marks.get_cur() == 'ng')
        self.rewards.set_active_options(['blade'], not self.magic_wep.is_any_selected())
        self.rewards.set_active_options(['cf'], self.mage.is_any_selected() and not self.magic_arc.is_any_selected())
        self.magic_wep.set_active(not self.rewards.get('blade'))
        self.magic_arc.set_active(not self.rewards.get('cf'))

        self.lores.set_active_options(['metal', 'tzeentch'], self.marks.get_cur() == 'tz')
        self.lores.set_active_options(['shadow', 'slaanesh'], self.marks.get_cur() == 'sl')
        self.lores.set_active_options(['death', 'nurgle'], self.marks.get_cur() == 'ng')

        norm_point_limits(25, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic if m.is_used()), []) + self.rewards.get_selected()
