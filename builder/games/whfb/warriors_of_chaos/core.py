__author__ = 'Denis Romanov'

from builder.games.whfb.unit import FCGUnit
from builder.core.unit import OptionsList, Unit
from builder.core.model_descriptor import ModelDescriptor
from armory import woc_standard, filter_items

unit_standards = filter_items(woc_standard, 25)


class Warriors(FCGUnit):
    name = 'Chaos Warriors'

    def __init__(self):
        self.marks = OptionsList('opt', 'Options', [
            ['Mark of Khorne', 2, 'kh'],
            ['Mark of Tzeentch', 2, 'tz'],
            ['Mark of Nurgle', 2, 'ng'],
            ['Mark of Slaanesh', 1, 'sl'],
        ], limit=1)
        FCGUnit.__init__(
            self, model_name='Chaos Warrior', model_price=14, min_models=10,
            musician_price=10, standard_price=10, champion_name='Aspiring Champion', champion_price=10,
            magic_banners=unit_standards,
            base_gear=['Hand weapon', 'Chaos armour'],
            options=[
                OptionsList('arm', 'Armour', [
                    ['Shield', 1, 'sh'],
                ]),
                OptionsList('wep', 'Weapon', [
                    ['Hand weapon', 2, 'hw'],
                    ['Great weapon', 3, 'gw'],
                    ['Halberd', 3, 'hb'],
                ], limit=1),
                self.marks
            ]
        )

    def check_rules(self):
        self.ban.set_visible_options(['woc_blas'], self.marks.get('tz'))
        self.ban.set_visible_options(['woc_rage'], self.marks.get('kh'))
        FCGUnit.check_rules(self)


class Marauders(FCGUnit):
    name = 'Chaos Marauders'

    def __init__(self):
        self.marks = OptionsList('opt', 'Options', [
            ['Mark of Khorne', 2, 'kh'],
            ['Mark of Tzeentch', 2, 'tz'],
            ['Mark of Nurgle', 2, 'ng'],
            ['Mark of Slaanesh', 1, 'sl'],
        ], limit=1)
        FCGUnit.__init__(
            self, model_name='Chaos Marauder', model_price=6, min_models=10,
            musician_price=10, standard_price=10, champion_name='Marauder Chieftain', champion_price=10,
            base_gear=['Hand weapon'],
            options=[
                OptionsList('arm', 'Armour', [
                    ['Shield', 1, 'sh'],
                    ['Light armour', 1, 'la'],
                ]),
                OptionsList('wep', 'Weapon', [
                    ['Great weapon', 3, 'gw'],
                    ['Flail', 2, 'fl'],
                ], limit=1),
                self.marks
            ]
        )


class MaraudersHorsemen(FCGUnit):
    name = 'Marauder Horsemen'

    def __init__(self):
        self.marks = OptionsList('opt', 'Options', [
            ['Mark of Khorne', 2, 'kh'],
            ['Mark of Tzeentch', 2, 'tz'],
            ['Mark of Nurgle', 2, 'ng'],
            ['Mark of Slaanesh', 1, 'sl'],
        ], limit=1)
        FCGUnit.__init__(
            self, model_name='Marauder Horseman', model_price=14, min_models=5,
            musician_price=10, standard_price=10, champion_name='Marauder Horsemaster', champion_price=10,
            base_gear=['Hand weapon'],
            options=[
                OptionsList('arm', 'Armour', [
                    ['Shield', 1, 'sh'],
                    ['Light armour', 1, 'la'],
                ]),
                OptionsList('wep', 'Weapon', [
                    ['Throwing axes', 2, 'ta'],
                    ['Javelin', 1, 'jv'],
                ], limit=1),
                OptionsList('wep1', '', [
                    ['Spear', 1, 'sp'],
                    ['Flail', 2, 'fl'],
                ], limit=1),
                self.marks
            ]
        )


class Forsaken(FCGUnit):
    name = 'Forsaken'

    def __init__(self):
        self.marks = OptionsList('opt', 'Options', [
            ['Mark of Khorne', 2, 'kh'],
            ['Mark of Tzeentch', 1, 'tz'],
            ['Mark of Nurgle', 2, 'ng'],
            ['Mark of Slaanesh', 2, 'sl'],
        ], limit=1)
        FCGUnit.__init__(
            self, model_name='Forsaken', model_price=19, min_models=5,
            base_gear=['Hand weapon', 'Chaos armour'],
            options=[self.marks]
        )


class Warhound(FCGUnit):
    name = 'Chaos Warhound'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Chaos Warhound', model_price=6, min_models=5,
            options=[
                OptionsList('opt', 'Options', [
                    ['Vanguard', 2, 'vn'],
                ]),
                OptionsList('opt1', '', [
                    ['Scaly hide', 1, 'sh'],
                    ['Mutant Poisons', 1, 'mp'],
                ])
            ]
        )


class ChaosChariot(Unit):
    name = 'Chaos Chariot'
    base_points = 110
    gear = ['Scythes']

    def __init__(self, boss=None):
        Unit.__init__(self)
        self.boss = boss
        self.marks = self.opt_options_list('Options', [
            ['Mark of Khorne', 10, 'kh'],
            ['Mark of Tzeentch', 10, 'tz'],
            ['Mark of Nurgle', 15, 'ng'],
            ['Mark of Slaanesh', 5, 'sl'],
        ], limit=1)

    def check_rules(self):
        if self.boss:
            boss_mark = self.boss.mark
            for mark_id in self.marks.get_all_ids():
                self.marks.set_active_options([mark_id], mark_id == boss_mark)
        Unit.check_rules(self)
        self.description.add(ModelDescriptor(name='Chaos Charioteer', count=1 if self.boss else 2,
                                             gear=['Halberd']).build())
        self.description.add(ModelDescriptor(name='Chaos Steed', count=2).build())
