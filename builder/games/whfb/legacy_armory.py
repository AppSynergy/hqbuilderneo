__author__ = 'Ivan Truskov'

from builder.core import filter_items, get_ids


def build_magic_levels(start, count, price, price_offset=0):
    return [['Level {0} Wizard'.format(i + start), price * i + price_offset, 'ml{0}'.format(i)] for i in range(count)]


def multiple_id(item_id, n):
    return '{0}_{1}'.format(item_id, n)


magic_weapons = [
    ['Giant Blade', 60, 'gbl'],
    ['Sword of Bloodshed', 60, 'sobl'],
    ['Obsidian Blade', 50, 'obb'],
    ['Ogre blade', 40, 'ogb'],
    ['Sword of Strife', 40, 'sos'],
    ['Fencer\'s blades', 35, 'fb'],
    ['Sword of Anti-Heroes', 30, 'soah'],
    ['Spellthieving sword', 25, 'sths'],
    ['Sword of Swift Slaying', 25, 'soss'],
    ['Sword of Battle', 20, 'sobt'],
    ['Berserker sword', 20, 'bs'],
    ['Sword of Might', 20, 'som'],
    ['Gold Sigil sword', 15, 'gss'],
    ['Sword of Striking', 15, 'sostr'],
    ['Biting Blade', 10, 'bitb'],
    ['Relic sword', 10, 'rels'],
    ['Shrieking Blade', 10, 'shb'],
    ['Tormentor sword', 5, 'tsw'],
    ['Warrior Bane', 5, 'wb']
]

magic_armour_suits = [
    ['Armour of Destiny', 50, 'aod'],
    ['Armour of Silvered Steel', 45, 'aoss'],
    ['Armour of Fortune', 35, 'aof'],
    ['Glittering Scales', 25, 'gls'],
    ['Gambler\'s Armour', 20, 'ga']
]

magic_helms = [
    ['Trickster\'s Helm', 50, 'th'],
    ['Helm of Discord', 30, 'hod'],
    ['Dragonhelm', 10, 'dh']
]

magic_shields = [
    ['Spellshield', 20, 'ssh'],
    ['Enchanted shield', 5, 'esh'],
    ['Charmed shield', 5, 'csh']
]

magic_armours = magic_armour_suits + magic_helms + magic_shields

magic_shields_ids = get_ids(magic_shields)
magic_armour_suits_ids = get_ids(magic_armour_suits)

talismans = [
    ['Talisman of Preservations', 45, 'toprs'],
    ['Obsidian Lodestone', 45, 'olds'],
    ['Talisman of Endurance', 30, 'toe'],
    ['Obsidian Amulet', 30, 'obsidian'],
    ['Dawnstone', 25, 'dst'],
    ['Opal Amulet', 15, 'opalamulet'],
    ['Obsidian Trinket', 15, 'otr'],
    ['Talisman of Protection', 15, 'toprt'],
    ['Seed of Rebirth', 10, 'sor'],
    ['Dragonbane Gem', 5, 'dbg'],
    ['Pigeon Plucker pendant', 5, 'ppp'],
    ['Luckstone', 5, 'lst']
]

arcane_items = [
    ['Book of Ashur', 70, 'boa'],
    ['Feedback scroll', 50, 'fsc'],
    ['Scroll of Leeching', 50, 'scl'],
    ['Sivejir\'s Hex scroll', 50, 'shsc'],
    ['Power scroll', 35, 'psc'],
    ['Wand of Jet', 35, 'wndj'],
    ['Forbidden rod', 35, 'frod'],
    ['Trickster\'s shard', 25, 'tsh'],
    ['Earthing Rod', 25, 'erod'],
    ['Dispel scroll', 25, 'dsc'],
    ['Power stone', 20, 'pst'],
    ['Sceptre of stability', 15, 'sst'],
    ['Channeling staff', 15, 'cst'],
    ['Scroll of Shielding', 15, 'scsh']
]

enchanted_items = [
    ['Wizarding Hat', 100, 'wh'],
    ['Fozzrik\'s Folding Fortress', 100, 'fozz'],
    ['Arabyan carpet', 50, 'acrp'],
    ['Crown of Command', 35, 'crocmd'],
    ['Healing potion', 35, 'hp'],
    ['Featherfoe torc', 35, 'ft'],
    ['Ruby Ring of Ruin', 25, 'rror'],
    ['The Terrifying Mask of EEE!', 25, 'eee'],
    ['Potion of Strength', 20, 'postr'],
    ['Potion of toughness', 20, 'potgh'],
    ['The other Trickster\'s Shard', 15, 'totsh'],
    ['Ironcurse icon', 5, 'ici'],
    ['Potion of Foolhardiness', 5, 'pofhd'],
    ['Potion of Speed', 5, 'pospd']
]

magic_standards = [
    ['Rampager\'s Standard', 55, 'rmps'],
    ['Wailing Banner', 50, 'wlb'],
    ['Ranger\'s Standard', 50, 'rngs'],
    ['Razor standard', 45, 'rzs'],
    ['War Banner', 35, 'wrb'],
    ['Banner of Swiftness', 15, 'bsw'],
    ['Lichebone Pennant', 15, 'lbp'],
    ['Standard of Discipline', 15, 'sdsc'],
    ['Banner of Eternal Flame', 10, 'bef'],
    ['Gleaming Pennant', 5, 'gp'],
    ['Scarecrow banner', 5, 'scb']
]
