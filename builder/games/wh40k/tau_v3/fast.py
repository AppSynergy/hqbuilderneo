__author__ = 'maria'

from builder.core2 import Gear, OneOf, Count, ListSubUnit,\
    UnitList, OptionalSubUnit, SubUnit, UnitDescription,\
    OptionsList
from builder.games.wh40k.unit import Unit, Squadron
from armory import *


class Devilfish(Unit):
    type_name = u'Devilfish'
    type_id = 'devilfish_v3'

    def __init__(self, parent=None):
        super(Devilfish, self).__init__(parent=parent, points=80,
                                        name='TV7 Devilfish',
                                        gear=[Gear('Burst cannon')])
        self.Weapon(self)
        Vehicle(self)
        Count(self, 'Seeker missile', min_limit=0, max_limit=2, points=8)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Devilfish.Weapon, self).__init__(parent=parent, name='Weapon')
            self.twogundrones = self.variant('Two Gun Drones', 0, gear=[Gear('Gun Drone', count=2)])
            self.twinlinkedsmartmissilesystem = self.variant('Twin-linked smart missile system', 10)

    def build_statistics(self):
        res = super(Devilfish, self).build_statistics()
        res['Models'] += 2
        return res


class Transport(OptionalSubUnit):
    def __init__(self, parent):
        super(Transport, self).__init__(parent=parent, name='Transport', limit=1)
        self.devilfish = SubUnit(self, Devilfish())


class GunDrones(Unit):
    type_name = u'Drones'
    type_id = 'dronesquadron_v3'

    def __init__(self, parent):
        super(GunDrones, self).__init__(parent=parent)
        self.drones = [
            Count(self, 'MV1 Gun Drone', min_limit=4, max_limit=12, points=14),
            Count(self, 'MV7 Marker Drone', min_limit=0, max_limit=12, points=14),
            Count(self, 'MV4 Shield Drone', min_limit=0, max_limit=12, points=14)
        ]

    def check_rules(self):
        Count.norm_counts(4, 12, self.drones)

    def get_count(self):
        return sum(o.cur for o in self.drones)


class PiranhaTeam(Unit):
    type_name = u'TX4 Piranhas'
    type_id = 'piranha_team_v3'

    class Piranha(ListSubUnit):

        def __init__(self, parent):
            super(PiranhaTeam.Piranha, self).__init__(parent, name='TX4 Piranha', points=40,
                                                      gear=[Gear('MV1 Gun Drone', count=2)])
            self.Weapon(self)
            Count(self, 'Seeker missile', min_limit=0, max_limit=2, points=8)
            Vehicle(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(PiranhaTeam.Piranha.Weapon, self).__init__(parent=parent, name='Weapon')
                self.variant('Burst cannon', 0)
                self.variant('Fusion blaster', 10)

    def __init__(self, parent):
        super(PiranhaTeam, self).__init__(parent=parent)
        self.models = UnitList(parent=self, unit_class=self.Piranha, min_limit=1, max_limit=5)

    def get_count(self):
        return self.models.count

    def build_statistics(self):
        res = super(PiranhaTeam, self).build_statistics()
        res['Models'] *= 3
        return res


class TirelessHunter(Unit):
    type_name = u'Tireless Hunter'
    type_id = 'tireless_hunter_v3'

    def __init__(self, parent):
        super(TirelessHunter, self).__init__(parent, points=55, unique=True, static=True, gear=[
            Gear('Fusion blaster'), Gear('Gun Drone', count=2)
        ])

    def build_statistics(self):
        res = super(TirelessHunter, self).build_statistics()
        res['Models'] += 2
        return res


class Pathfinders(Unit):
    type_name = u'Pathfinder Team'
    type_id = 'pathfinderteam_v3'

    model_points = 11
    model_gear = [Gear('Photon grenades')]
    model_min = 4
    model_max = 10
    model_name = 'Pathfinder'
    model = UnitDescription(model_name, points=model_points, options=model_gear)

    class Shasui(Unit):
        type_name = u'Pathfinder Shas\'ui'
        type_id = 'shasui_v1'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Pathfinders.Shasui.Weapon, self).__init__(parent=parent, name='Weapon')
                self.pulsecarbine = self.variant('Pulse carbine', 0, gear=[Gear('Pulse carbine'), Gear('Markerlight')])
                self.ion = self.variant('Ion rifle', 10)
                self.rail = self.variant('Rail rifle', 15)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Pathfinders.Shasui.Options, self).__init__(parent=parent, name='Options', limit=None)
                self.variant('Blacksun filter', 1)

        def __init__(self, parent=None):
            super(Pathfinders.Shasui, self).__init__(parent=parent, points=Pathfinders.model_points + 10,
                                                     name=self.type_name, gear=Pathfinders.model_gear)
            self.weapon = self.Weapon(self)
            self.opt = self.Options(self)
            self.drones = Drones(self)

        def check_rules(self):
            self.drones.check_rules()

        def has_rifles(self):
            return self.weapon.cur != self.weapon.pulsecarbine

    class Options(Ritual):
        def __init__(self, parent):
            super(Pathfinders.Options, self).__init__(parent=parent)
            self.empgrenades = self.variant('EMP grenades', 2)

    class ModelCount(Count):
        @property
        def description(self):
            return [Pathfinders.model.clone().add([Gear('Pulse carbine'), Gear('Markerlight')]).
                    set_count(self.cur - self.parent.ionrifle.cur - self.parent.railrifle.cur)]

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(Pathfinders.Leader, self).__init__(parent=parent, name='')
            self.shasui = SubUnit(self, Pathfinders.Shasui())

    class Drones(OptionsList):
        def __init__(self, parent):
            super(Pathfinders.Drones, self).__init__(parent=parent, name='Drones', limit=None)
            self.recondrone = self.variant('MB3 Recon Drone', 28)
            self.gravinhibitordrone = self.variant('MV33 Grav-inhibitor Drone', 15)
            self.pulseacceleratordrone = self.variant('MV31 Pulse accelerator Drone', 15)

    def __init__(self, parent):
        super(Pathfinders, self).__init__(parent=parent, points=0, gear=[])
        self.leader = self.Leader(self)
        self.pathfinder = self.ModelCount(self, self.model_name, min_limit=self.model_min, max_limit=self.model_max,
                                          points=self.model_points)
        self.ionrifle = Count(self, 'Ion rifle', min_limit=0, max_limit=3, points=10,
                              gear=self.model.clone().add(Gear('Ion rifle')).add_points(10))
        self.railrifle = Count(self, 'Rail rifle', min_limit=0, max_limit=3, points=15,
                               gear=self.model.clone().add(Gear('Rail rifle')).add_points(15))
        self.Options(self)
        self.drones = self.Drones(self)
        self.transport = Transport(self)

    def check_rules(self):
        super(Pathfinders, self).check_rules()
        self.pathfinder.min = self.model_min - self.leader.count
        self.pathfinder.max = self.model_max - self.leader.count
        rifle_limit = 3 - (1 if self.leader.shasui.unit.has_rifles() and self.leader.count else 0)
        Count.norm_counts(0, rifle_limit, [self.ionrifle, self.railrifle])

    def get_count(self):
        return self.pathfinder.cur + self.leader.count

    def build_statistics(self):
        res = self.note_transport(super(Pathfinders, self).build_statistics())
        res['Models'] += sum(op.value for op in self.drones.options)
        return res


class TeamAurora(Unit):
    type_name = u'Pathfinder Team Aurora'
    type_id = 'aurora_v3'

    def __init__(self, parent):
        super(TeamAurora, self).__init__(parent, points=240, static=True, unique=True, gear=[
            UnitDescription("Shas'ui Starshroud", options=[
                Gear('Photon grenades'), Gear('Pulse carbine'), Gear('Markerlight')]),
            UnitDescription("Pathfinder", count=6, options=[
                Gear('Photon grenades'), Gear('Pulse carbine'), Gear('Markerlight')]),
            UnitDescription("Pathfinder", count=1, options=[
                Gear('Photon grenades'), Gear('Ion rifle')]),
            UnitDescription("Pathfinder", count=2, options=[
                Gear('Photon grenades'), Gear('Rail rifle')]),
            Gear('Grav-inhibitor Drone'), Gear('Pulse Accelerator Drone'), Gear('Recon Drone')
        ])

    def build_statistics(self):
        return {'Models': 13, 'Units': 1}


class SunShark(Unit):
    type_name = u'AX39 Sun Shark Bomber'
    type_id = 'sunsharkbomber_v3'

    def __init__(self, parent):
        super(SunShark, self).__init__(
            parent=parent, points=160,
            gear=[Gear('Pulse bomb generator'), Gear('Networked markerlight'), Gear('Seeker missile', count=2),
                  UnitDescription(name='Interceptor Drone', options=[Gear('Twin-linked ion rifle')], count=2)]
        )
        self.Weapon(self)
        Vehicle(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(SunShark.Weapon, self).__init__(parent=parent, name='Weapon')
            self.missilepod = self.variant('Missile pod', 0)
            self.twinlinkedmissilepod = self.variant('Twin-linked missile pod', 5)

    def build_statistics(self):
        res = super(SunShark, self).build_statistics()
        res['Models'] += 2
        return res


class Sunsharks(Squadron):
    type_name = u'AX39 Sun Shark Bombers'
    type_id = 'sunsharkbombers_v3'
    unit_class = SunShark
    unit_max = 4


class RazorShark(Unit):
    type_name = u'AX3 Razorshark Strike Fighter'
    type_id = 'razorsharkstrikefighter_v3'

    def __init__(self, parent):
        super(RazorShark, self).__init__(
            parent=parent, points=145,
            gear=[Gear('Quad ion turret'), Gear('Seeker missile'), Gear('Seeker missile')]
        )
        self.Weapon(self)
        Vehicle(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(RazorShark.Weapon, self).__init__(parent=parent, name='Weapon')
            self.burstcannon = self.variant('Burst cannon', 0)
            self.missilepod = self.variant('Missile pod', 5)


class Razorsharks(Squadron):
    type_name = u'AX3 Razorshark Strike Fighters'
    type_id = 'razorsharkstrikefighter_v3'
    unit_class = RazorShark
    unit_max = 4


class Vespids(Unit):
    type_name = u'Vespid Stingwings'
    type_id = 'vespidstingwings_v3'
    model_points = 18
    model_gear = [Gear('Neutron blaster')]

    class Leader(OptionsList):
        def __init__(self, parent):
            super(Vespids.Leader, self).__init__(parent=parent, name='')
            self.vespidstainleader = self.variant(
                'Vespid Stain Leader', Vespids.model_points + 10,
                gear=UnitDescription(name='Vespid Stain Leader', points=Vespids.model_points + 10,
                                     options=Vespids.model_gear)
            )

    def __init__(self, parent):
        super(Vespids, self).__init__(parent=parent)
        self.leader = self.Leader(self)
        self.vespidstingwing = Count(
            self, 'Vespid Stingwing', min_limit=4, max_limit=12, points=self.model_points,
            gear=UnitDescription(name='Vespid Stingwing', points=Vespids.model_points, options=Vespids.model_gear)
        )

    def check_rules(self):
        self.vespidstingwing.min = 4 - self.leader.any
        self.vespidstingwing.max = 12 - self.leader.any

    def get_count(self):
        return self.vespidstingwing.cur + self.leader.vespidstainleader.value
