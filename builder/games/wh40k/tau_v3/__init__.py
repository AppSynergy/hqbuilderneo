__author__ = 'Ivan Truskov'
__summary__ = ['Codex Tau 7th edition', 'Kayon Supplement', 'Montka Supplement']

from dataslates import TauHeavySupport, TauFastAttack, TauLordsSection
from builder.games.wh40k.escalation.tau import LordsOfWar as EscLordsOfWar
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kImperial, Wh40kKillTeam,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, UnitType, Detachment, Formation, FlyerSection
from builder.games.wh40k.fortifications import Fort

from hq import *
from elites import *
from troops import *
from fast import *
from heavy import *
from lords import *
from fortifications import TidewallFortifications, Shieldline, Droneport,\
    Gunrig


class HQ(HQSection):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        self.farsight = UnitType(self, Farsight)
        self.shadowsun = UnitType(self, Shadowsun)
        self.aunva = UnitType(self, Aunva)
        self.aunshi = UnitType(self, Aunshi)
        self.darkstider = UnitType(self, Darkstider)
        self.ethereal = UnitType(self, Ethereal)
        self.cadre = UnitType(self, Cadre)
        self.commander = UnitType(self, Commander)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, StrikeTeam)
        UnitType(self, BreacherTeam)
        UnitType(self, KrootSquad)
        self.crisis_team = UnitType(self, CrisisTeam)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        self.stealth_team = UnitType(self, StealthTeam)
        self.crisis_team = UnitType(self, CrisisTeam)
        self.bodyguards = UnitType(self, Bodyguards)
        UnitType(self, Riptides)
        self.ghost = UnitType(self, Ghostkeels)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        self.fish = UnitType(self, Devilfish)
        self.pathfinders = UnitType(self, Pathfinders)
        self.vespids = UnitType(self, Vespids)
        self.gun_drones = UnitType(self, GunDrones)
        self.piranha = UnitType(self, PiranhaTeam)
        sun = UnitType(self, SunShark)
        sun.visible = False
        UnitType(self, Sunsharks)
        razor = UnitType(self, RazorShark)
        razor.visible = False
        UnitType(self, Razorsharks)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        self.broadside_team = UnitType(self, BroadsideTeam)
        self.hammerhead = UnitType(self, Hammerheads)
        self.sky_ray = UnitType(self, SkyRays)
        self.sniper_drone_team = UnitType(self, SniperDroneTeam)


class BaseLordsSection(EscLordsOfWar):
    def __init__(self, parent):
        super(BaseLordsSection, self).__init__(parent)
        self.stormsurge = UnitType(self, Stormsurges)


class FortificationSection(Fort, TidewallFortifications):
    pass


class HeavySupport(TauHeavySupport, BaseHeavySupport):
    pass


class FastAttack(TauFastAttack, BaseFastAttack):
    pass


class LordsSection(TauLordsSection, BaseLordsSection):
    pass


class Fliers(FlyerSection):
    def __init__(self, parent):
        super(Fliers, self).__init__(parent, [Razorsharks, Sunsharks])


class SupplementOptions(OneOf):
    def __init__(self, parent):
        super(SupplementOptions, self).__init__(parent=parent, name='Codex')
        self.base = self.variant(name=TauV3Base.army_name)
        self.enclaves = self.variant(name='Farsight Enclaves')


class TauFormation(Formation):
    def __init__(self, *args, **kwargs):
        super(TauFormation, self).__init__(*args, **kwargs)

        self.codex = SupplementOptions(self)

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_enclaves(self):
        return self.codex.cur == self.codex.enclaves


class HunterCadre(TauFormation):
    army_id = 'tau_v3_cadre'
    army_name = 'Hunter Cadre'

    def __init__(self):
        super(HunterCadre, self).__init__()
        UnitType(self, Commander, min_limit=1, max_limit=1)
        UnitType(self, Cadre, max_limit=1)
        infantry = [
            UnitType(self, StrikeTeam),
            UnitType(self, BreacherTeam),
            UnitType(self, KrootSquad)
        ]
        self.add_type_restriction(infantry, 3, 6)
        suits = [
            UnitType(self, StealthTeam),
            UnitType(self, CrisisTeam),
            UnitType(self, Ghostkeels),
            UnitType(self, Riptides)
        ]
        self.add_type_restriction(suits, 1, 3)
        fast = [
            UnitType(self, Pathfinders),
            UnitType(self, PiranhaTeam),
            UnitType(self, Vespids),
            UnitType(self, GunDrones)
        ]
        self.add_type_restriction(fast, 1, 3)
        support = [
            UnitType(self, BroadsideTeam),
            UnitType(self, Hammerheads),
            UnitType(self, Stormsurges),
            UnitType(self, SniperDroneTeam)
        ]
        self.add_type_restriction(support, 1, 3)


class RetaliationCadre(TauFormation):
    army_id = 'tau_v3_retaliation'
    army_name = 'Retaliation Cadre'

    def __init__(self):
        super(RetaliationCadre, self).__init__()
        UnitType(self, Commander, min_limit=1, max_limit=1)
        UnitType(self, CrisisTeam, min_limit=3, max_limit=3)
        UnitType(self, BroadsideTeam, min_limit=1, max_limit=1)
        UnitType(self, Riptides, min_limit=1, max_limit=1)


class HeavyRetributionCadre(TauFormation):
    army_id = 'tau_v3_heavyretribution'
    army_name = 'Heavy Retribution Cadre'

    def __init__(self):
        super(HeavyRetributionCadre, self).__init__()
        UnitType(self, Ghostkeels, min_limit=1, max_limit=1)
        UnitType(self, Stormsurges, min_limit=2, max_limit=2)


class InfiltrationCadre(TauFormation):
    army_id = 'tau_v3_infiltration'
    army_name = 'Infiltration Cadre'

    def __init__(self):
        super(InfiltrationCadre, self).__init__()
        UnitType(self, Pathfinders, min_limit=3, max_limit=3)
        UnitType(self, StealthTeam, min_limit=2, max_limit=2)
        UnitType(self, PiranhaTeam, min_limit=1, max_limit=1)


class OptimisedStealthCadre(TauFormation):
    army_id = 'tau_v3_stealth'
    army_name = 'Optimized Stealth Cadre'

    def __init__(self):
        super(OptimisedStealthCadre, self).__init__()
        UnitType(self, Ghostkeels, min_limit=1, max_limit=1)
        UnitType(self, StealthTeam, min_limit=2, max_limit=2)


class Firebase(TauFormation):
    army_id = 'tau_v3_firebase'
    army_name = 'Firebase Support Cadre'

    def __init__(self):
        super(Firebase, self).__init__()
        UnitType(self, Riptides, min_limit=1, max_limit=1)
        UnitType(self, BroadsideTeam, min_limit=2, max_limit=2)


class Interdiction(TauFormation):
    army_id = 'tau_v3_interdiction'
    army_name = 'Armoured Interdiction Cadre'

    def __init__(self):
        super(Interdiction, self).__init__()
        UnitType(self, SkyRays, min_limit=1, max_limit=1)
        UnitType(self, Hammerheads, min_limit=3, max_limit=3)


class AirCaste(TauFormation):
    army_id = 'tau_v3_air'
    army_name = 'Air Caste Support Cadre'

    def __init__(self):
        super(AirCaste, self).__init__()
        UnitType(self, SunShark, min_limit=1, max_limit=2)
        UnitType(self, RazorShark, min_limit=2, max_limit=2)


class AlliedAdvance(TauFormation):
    army_id = 'tau_v3_allies'
    army_name = 'Allied Advance Cadre'

    def __init__(self):
        super(AlliedAdvance, self).__init__()
        UnitType(self, KrootSquad, min_limit=4, max_limit=4)
        UnitType(self, Vespids, min_limit=2, max_limit=2)


class HunterContingent(Wh40k7ed):
    army_id = 'tau_v3_contingent'
    army_name = 'Hunter Contingent'

    class HunterSubFormation(TauFormation):
        def __init__(self, *args, **kwargs):
            super(HunterContingent.HunterSubFormation, self).__init__(*args, **kwargs)

            self.codex.used = self.codex.visible = False

        @property
        def is_base(self):
            return self.parent.roster.is_base

        @property
        def is_enclaves(self):
            return not self.parent.roster.is_base

    @staticmethod
    def make_hunter(formation):
        class BaseSubformation(HunterContingent.HunterSubFormation, formation):
            pass
        return BaseSubformation

    class Headquarters(HunterSubFormation):
        army_id = 'tau_v3_headquarters'
        army_name = 'Contingent Headquarters'

        def __init__(self):
            super(HunterContingent.Headquarters, self).__init__()
            commanders = [
                UnitType(self, Commander),
                UnitType(self, Shadowsun)
            ]
            self.add_type_restriction(commanders, 1, 1)
            ethereals = [
                UnitType(self, Ethereal),
                UnitType(self, Aunshi),
                UnitType(self, Aunva)
            ]
            self.add_type_restriction(ethereals, 0, 1)
            UnitType(self, Bodyguards, min_limit=1, max_limit=2)

    class AirAsset(HunterSubFormation):
        army_id = 'tau_v3_air_asset'
        army_name = 'Assigned Air Caste Asset'

        def __init__(self):
            super(HunterContingent.AirAsset, self).__init__()
            planes = [UnitType(self, SunShark), UnitType(self, RazorShark)]
            self.add_type_restriction(planes, 1, 1)

    class Network(HunterSubFormation):
        army_id = 'tau_v3_drones'
        army_name = 'Drone network'

        def __init__(self):
            super(HunterContingent.Network, self).__init__()
            UnitType(self, GunDrones, min_limit=1, max_limit=1)

    class Core(Detachment):
        def __init__(self, parent):
            super(HunterContingent.Core, self).__init__(
                parent, 'core', 'Core',
                [HunterContingent.make_hunter(HunterCadre)], 1, None)

    class Command(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(HunterContingent.Command, self).__init__(
                parent, 'command', 'Command',
                [HunterContingent.Headquarters], 0, 1)

        def check_limits(self):
            self.min = 0
            self.max = max([1, len(self.core.units)])
            return super(HunterContingent.Command, self).check_limits()

    class Auxilary(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(HunterContingent.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary', [
                    HunterContingent.make_hunter(f) for f in [
                        OptimisedStealthCadre,
                        RetaliationCadre,
                        AlliedAdvance,
                        Firebase,
                        AirCaste,
                        InfiltrationCadre,
                        HeavyRetributionCadre,
                        Interdiction
                    ]
                ] + [HunterContingent.AirAsset, HunterContingent.Network],
                1, 10)

        def check_limits(self):
            self.min = max([1, len(self.core.units)])
            self.max = max([1, len(self.core.units)]) * 10
            return super(HunterContingent.Auxilary, self).check_limits()

    def __init__(self):
        core = self.Core(self)
        command = self.Command(self, core)
        aux = self.Auxilary(self, core)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

        self.codex = SupplementOptions(self)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_enclaves(self):
        return self.codex.cur == self.codex.enclaves


class Counterstrike(TauFormation):
    army_id = 'tau_v3_counterstrike'
    army_name = 'Counterstrike Cadre'

    class EmbarkedUnit(Unit):
        def check_rules(self):
            super(Counterstrike.EmbarkedUnit, self).check_rules()
            if not self.transport.devilfish.visible:
                self.error('Unit of {} must be embarked on Devilfish'.
                           format(self.type_name))

    def __init__(self):
        super(Counterstrike, self).__init__()

        class EmbarkedPathfinders(Counterstrike.EmbarkedUnit, Pathfinders):
            pass

        class EmbarkedBreachers(Counterstrike.EmbarkedUnit, BreacherTeam):
            pass

        class EmbarkedStrikers(Counterstrike.EmbarkedUnit, StrikeTeam):
            pass

        UnitType(self, EmbarkedPathfinders, min_limit=1, max_limit=1)
        self.fire = [
            UnitType(self, EmbarkedBreachers),
            UnitType(self, EmbarkedStrikers)
        ]
        self.add_type_restriction(self.fire, 3, 3)


class RapidInsertion(TauFormation):
    army_id = 'tau_v3_rapid'
    army_name = 'Rapid Insertion Force'

    def __init__(self):
        super(RapidInsertion, self).__init__()
        UnitType(self, StealthTeam, min_limit=1, max_limit=1)
        UnitType(self, CrisisTeam, min_limit=3, max_limit=3)
        UnitType(self, Riptides, min_limit=1, max_limit=1)


class RangedSupport(TauFormation):
    army_id = 'tau_v3_ranged'
    army_name = 'Ranged Support Cadre'

    def __init__(self):
        super(RangedSupport, self).__init__()
        UnitType(self, Pathfinders, min_limit=3, max_limit=3)
        UnitType(self, BroadsideTeam, min_limit=3, max_limit=3)


class Firestream(TauFormation):
    army_id = 'tau_v3_firestream'
    army_name = 'Pirahna Firestream Wing'

    def __init__(self):
        super(Firestream, self).__init__()
        self.pirahnas = UnitType(self, PiranhaTeam, min_limit=4, max_limit=4)

    def check_rules(self):
        super(Firestream, self).check_rules()
        if all(u.count > 1 for u in self.pirahnas.units):
            self.error('Formation must include a Target Acquisition Team from a single Piranha')


class GhostkeelWing(TauFormation):
    army_id = 'tau_v3_ghostkeels'
    army_name = 'Ghostkeel Wing'

    def __init__(self):
        super(GhostkeelWing, self).__init__()
        UnitType(self, Ghostkeels, min_limit=3, max_limit=3)


class SkysweepWing(TauFormation):
    army_id = 'tau_v3_skysweep'
    army_name = 'Skysweep Missile Defence'

    def __init__(self):
        super(SkysweepWing, self).__init__()
        UnitType(self, Devilfish, min_limit=1, max_limit=1)
        UnitType(self, SkyRays, min_limit=3, max_limit=3)


class Ethereals(TauFormation):
    army_id = 'tau_v3_ethereals'
    army_name = 'Ethereal Council'

    def __init__(self):
        super(Ethereals, self).__init__()
        council = [
            UnitType(self, Ethereal),
            UnitType(self, Aunva),
            UnitType(self, Aunshi)
        ]
        self.add_type_restriction(council, 3, 7)


class DroneNet(TauFormation):
    army_id = 'tau_v3_dronenet'
    army_name = 'Drone-Net VX1-0'

    def __init__(self):
        super(DroneNet, self).__init__()
        UnitType(self, GunDrones, min_limit=4, max_limit=-1)


class RiptideWing(TauFormation):
    army_id = 'tau_v3_riptides'
    army_name = 'Riptide Wing'

    def __init__(self):
        super(RiptideWing, self).__init__()
        UnitType(self, Riptides, min_limit=3, max_limit=3)


class AirSuperiority(TauFormation):
    army_id = 'tau_v3_airsuperiority'
    army_name = 'Air Superiority Cadre'

    def __init__(self):
        super(AirSuperiority, self).__init__()
        UnitType(self, RazorShark, min_limit=3, max_limit=3)


class Eight(Formation):
    army_id = 'tau_v3_eight'
    army_name = 'The Eight'

    @property
    def is_base(self):
        return False

    @property
    def is_enclaves(self):
        return True

    class Bravestorm(Unit):
        type_name = u'Bravestorm'
        type_id = 'bravestorm_v2'

        def __init__(self, parent):
            super(Eight.Bravestorm, self).__init__(parent, gear=[  	# 85
                Gear('XV8-02 Crisis \'Iridium\' Battlesuit'),		# 25
                Gear('Plasma rifle'),					# 15
                Gear('Flamer'),						# 5
                Gear('Stimulant injector'),				# 15
                Gear('Shield generator'),				# 25
                Gear('Gun Drone', count=2),				# 24
                Gear('Onager gauntlet')					# 5
            ], points=199, static=True, unique=True)

        def get_unique_gear(self):
            return [
                'XV8-02 Crisis \'Iridium\' Battlesuit',
                'Onager gauntlet'
            ]

    class Brightsword(Unit):
        type_name = u'Brightsword'
        type_id = 'brightsword_v2'

        def __init__(self, parent):
            super(Eight.Brightsword, self).__init__(parent, gear=[  	# 85
                Gear('Crisis battlesuit'),
                Gear('Twin-linked fusion blaster'),			# 20
                Gear('Advanced targeting system'),			# 10
                Gear('Stimulant injector'),				# 15
                Gear('Shield drone'),					# 12
                Gear('Fusion blades'),					# 20
                Gear('Warscaper Drone'),				# 35
            ], points=197, static=True, unique=True)

        def get_unique_gear(self):
            return [
                'Fusion blades',
                'Warscaper Drone'
            ]

    class ShaVastos(Unit):
        type_name = u'Sha\'vastos'
        type_id = 'shavastos_v2'

        def __init__(self, parent):
            super(Eight.ShaVastos, self).__init__(parent, gear=[  	# 85
                Gear('Crisis battlesuit'),
                Gear('Plasma rifle'),					# 15
                Gear('Flamer'),						# 5
                Gear('Shield generator'),				# 25
                Gear('Vectored retro-thrusters'),			# 5
                Gear('Gun Drone', count=2),				# 24
                Gear('Puretide Engram Neurochip')			# 15
            ], points=174, static=True, unique=True)

        def get_unique_gear(self):
            return [
                'Puretide Engram Neurochip'
            ]

    class OVesa(Unit):
        type_name = u'O\'Vesa'
        type_id = 'ovesa_v2'

        def __init__(self, parent):
            super(Eight.OVesa, self).__init__(parent, gear=[
                Gear('Riptide battlesuit'),				# 180
                Gear('Nova reactor'),
                Gear('Riptide shield generator'),
                Gear('Twin-linked fusion blaster'),
                Gear('Ion accelerator'),
                Gear('Early warning override'),				# 5
                Gear('Stimulant injector'),				# 35
                Gear('Shielded Missile Drone', count=2),		# 50
                Gear('Earth Caste Pilot Array')				# 30
            ], points=300, static=True, unique=True)

        def get_unique_gear(self):
            return [
                'Earth Caste Pilot Array'
            ]

    class Oblotai(Unit):
        type_name = u'Ob\'lotai 9-0'
        type_id = 'oblotai_v2'

        def __init__(self, parent):
            super(Eight.Oblotai, self).__init__(parent, gear=[		# 10
                Gear('Broadside battlesuit'), 				# 65
                Gear('Twin-linked high-yield missile pod'),
                Gear('Twin-linked smart missile system'),
                Gear('Velocity tracker'),				# 20
                Gear('Seeker missile'),					# 8
                Gear('Missile Drone', count=2),				# 24
                Gear('Bonding knife ritual')				# 1
            ], points=128, static=True, unique=True)

    class Arrakon(Unit):
        type_name = u'Shas\'o Arra\'kon'
        type_id = 'arrakon_v2'

        def __init__(self, parent):
            super(Eight.Arrakon, self).__init__(parent, gear=[  	# 85
                Gear('Crisis battlesuit'),
                Gear('Plasma rifle'),					# 15
                Gear('Cyclic ion blaster'),				# 15
                Gear('Airbursting fragmentation projector'),		# 15
                Gear('Counterfire defence system'),			# 5
                Gear('Gun Drone', count=2),				# 24
                Gear('Repulsor impact field')				# 10
            ], points=169, static=True, unique=True)

        def get_unique_gear(self):
            return [
                'Repulsor impact field'
            ]

    class Torchstar(Unit):
        type_name = u'Torchstar'
        type_id = 'torchstart_v2'

        def __init__(self, parent):
            super(Eight.Torchstar, self).__init__(parent, gear=[  	# 85
                Gear('Crisis battlesuit'),
                Gear('Flamer', count=2),				# 10
                Gear('Target lock'),					# 5
                Gear('Drone controller'),				# 8
                Gear('Marker Drone', count=2),				# 24
                Gear('Multi-spectrum Sensor Suit'),			# 20
                Gear('Neuroweb System Jammer')				# 2
            ], points=154, static=True, unique=True)

        def get_unique_gear(self):
            return [
                'Multi-spectrum Sensor Suit',
                'Neuroweb System Jammer'
            ]

    def __init__(self):
        super(Eight, self).__init__()
        UnitType(self, Farsight, min_limit=1, max_limit=1)
        UnitType(self, self.Bravestorm, min_limit=1, max_limit=1)
        UnitType(self, self.Brightsword, min_limit=1, max_limit=1)
        UnitType(self, self.ShaVastos, min_limit=1, max_limit=1)
        UnitType(self, self.OVesa, min_limit=1, max_limit=1)
        UnitType(self, self.Oblotai, min_limit=1, max_limit=1)
        UnitType(self, self.Arrakon, min_limit=1, max_limit=1)
        UnitType(self, self.Torchstar, min_limit=1, max_limit=1)


class DawnBladeContingent(Wh40k7ed):
    army_id = 'tau_v3_dawn_blade'
    army_name = 'Dawn Blade Contingent'

    class DawnSubFormation(TauFormation):
        def __init__(self, *args, **kwargs):
            super(DawnBladeContingent.DawnSubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        @property
        def is_base(self):
            return False

        @property
        def is_enclaves(self):
            return True

    @staticmethod
    def make_dawn(formation):
        class BaseSubformation(DawnBladeContingent.DawnSubFormation, formation):
            pass
        return BaseSubformation

    class Headquarters(DawnSubFormation):
        army_id = 'tau_v3_headquarters'
        army_name = 'Contingent Headquarters'

        def __init__(self):
            super(DawnBladeContingent.Headquarters, self).__init__()
            commanders = [
                UnitType(self, Commander),
                UnitType(self, Farsight)
            ]
            self.add_type_restriction(commanders, 1, 1)
            UnitType(self, Bodyguards, min_limit=1, max_limit=1)

    class Core(Detachment):
        def __init__(self, parent):
            super(DawnBladeContingent.Core, self).__init__(
                parent, 'core', 'Core',
                [
                    DawnBladeContingent.make_dawn(HunterCadre),
                    DawnBladeContingent.make_dawn(RetaliationCadre)
                ], 1, None)

    class Command(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(DawnBladeContingent.Command, self).__init__(
                parent, 'command', 'Command',
                [DawnBladeContingent.Headquarters, Eight], 0, 1)

        def check_limits(self):
            self.min = 0
            self.max = max([1, len(self.core.units)])
            return super(DawnBladeContingent.Command, self).check_limits()

    class Auxilary(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(DawnBladeContingent.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary', [
                    DawnBladeContingent.make_dawn(f) for f in [
                        Counterstrike,
                        RapidInsertion,
                        AlliedAdvance,
                        RangedSupport,
                        Firestream,
                        Firebase,
                        SkysweepWing,
                        AirSuperiority,
                        DroneNet,
                    ]
                ] + [HunterContingent.AirAsset],
                1, 10)

        def check_limits(self):
            self.min = max([1, len(self.core.units)])
            self.max = max([1, len(self.core.units)]) * 10
            return super(DawnBladeContingent.Auxilary, self).check_limits()

    def __init__(self):
        core = self.Core(self)
        command = self.Command(self, core)
        aux = self.Auxilary(self, core)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()

    @property
    def is_base(self):
        return False

    @property
    def is_enclaves(self):
        return True


class TidewallCounterstrike(TauFormation):
    army_id = 'tau_v3_tidewall'
    army_name = 'Tidewall Counterstrike Cadre'

    class FootBreachers(BreacherTeam):
        def __init__(self, *args, **kwargs):
            super(TidewallCounterstrike.FootBreachers, self).__init__(*args, **kwargs)
            self.transport.used = self.transport.visible = False

    class FootStrikers(StrikeTeam):
        def __init__(self, *args, **kwargs):
            super(TidewallCounterstrike.FootStrikers, self).__init__(*args, **kwargs)
            self.transport.used = self.transport.visible = False

    class FootPathfinders(Pathfinders):
        def __init__(self, *args, **kwargs):
            super(TidewallCounterstrike.FootPathfinders, self).__init__(*args, **kwargs)
            self.transport.used = self.transport.visible = False

    def __init__(self):
        super(TidewallCounterstrike, self).__init__()
        self.infantry = [
            UnitType(self, self.FootPathfinders, min_limit=1, max_limit=1),
            UnitType(self, self.FootBreachers, min_limit=2, max_limit=2),
            UnitType(self, self.FootStrikers, min_limit=1, max_limit=1)
        ]
        UnitType(self, Gunrig, min_limit=2, max_limit=2)
        UnitType(self, Shieldline, min_limit=2, max_limit=2)
        UnitType(self, Droneport, min_limit=2, max_limit=2)


class EtherealHonorGuard(TauFormation):
    army_id = 'tau_v3_ethereal_honor_guard'
    army_name = 'Ethereal Honor Guard'

    def __init__(self):
        super(EtherealHonorGuard, self).__init__()
        self.ethereal = UnitType(self, Ethereal, min_limit=1, max_limit=1)
        UnitType(self, StrikeTeam, min_limit=1, max_limit=1)
        UnitType(self, CrisisTeam, min_limit=1, max_limit=1)

    def check_rules(self):
        super(EtherealHonorGuard, self).check_rules()
        for unit in self.ethereal.units:
            if not unit.opt.hover.value:
                self.error('Etherial must take a hover drone')


class BurningDawn(Formation):
    army_id = 'tau_v3_burning_dawn'
    army_name = 'Infiltration Cadre Burning Dawn'

    def __init__(self):
        super(BurningDawn, self).__init__()
        UnitType(self, Aundo, min_limit=1, max_limit=1)
        UnitType(self, TeamDarktide, min_limit=1, max_limit=1)
        UnitType(self, TeamAurora, min_limit=1, max_limit=1)
        UnitType(self, TirelessHunter, min_limit=1, max_limit=1)


class TargetedReconnaissanceCadre(TauFormation):
    army_id = 'tau_v3_targeted_reconnaissance_cadre'
    army_name = 'Targeted Reconnaissance Cadre'
    # wikilink = "https://sites.google.com/site/wh40000rules/codices/tau/structure#TOC-Armoured-Interdiction-Cadre"  # AUTO-INSERTED/i

    def __init__(self):
        super(TargetedReconnaissanceCadre, self).__init__()
        UnitType(self, Commander, min_limit=1, max_limit=1)
        UnitType(self, Ghostkeels, min_limit=1, max_limit=1)
        UnitType(self, StealthTeam, min_limit=1, max_limit=1)
        UnitType(self, Pathfinders, min_limit=1, max_limit=1)
        UnitType(self, Devilfish, min_limit=1, max_limit=1)
        UnitType(self, BroadsideTeam, min_limit=1, max_limit=1)


class TauV3Base(Wh40kBase):
    army_id = 'tau_v3_base'
    army_name = 'Tau Empire'

    def __init__(self):

        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        super(TauV3Base, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops,
            fast=self.fast, heavy=self.heavy,
            fort=FortificationSection(parent=self),
            lords=LordsSection(parent=self)
        )

        self.codex = SupplementOptions(self)

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_enclaves(self):
        return self.codex.cur == self.codex.enclaves

    def check_rules(self):
        super(TauV3Base, self).check_rules()

        self.troops.crisis_team.visible = self.is_enclaves
        self.elites.crisis_team.visible = self.is_base
        if self.is_enclaves:
            self.troops.move_units(self.elites.crisis_team.units)
        else:
            self.elites.move_units(self.troops.crisis_team.units)


class TauV3CAD(TauV3Base, CombinedArmsDetachment):
    army_id = 'tau_v3_cad'
    army_name = 'Tau Empire (Combined arms detachment)'


class TauV3AD(TauV3Base, AlliedDetachment):
    army_id = 'tau_v3_ad'
    army_name = 'Tau Empire (Allied detachment)'


class TauV3PA(TauV3Base, PlanetstrikeAttacker):
    army_id = 'tau_v3_pa'
    army_name = 'Tau Empire (Planetstrike attacker detachment)'


class TauV3PD(TauV3Base, PlanetstrikeDefender):
    army_id = 'tau_v3_pd'
    army_name = 'Tau Empire (Planetstrike defender detachment)'


class TauV3SA(TauV3Base, SiegeAttacker):
    army_id = 'tau_v3_sa'
    army_name = 'Tau Empire (Siege War attacker detachment)'


class TauV3SD(TauV3Base, SiegeDefender):
    army_id = 'tau_v3_sd'
    army_name = 'Tau Empire (Siege War defender detachment)'


class FlierDetachment(Wh40kBase):
    army_id = 'tau_v3_asd'
    army_name = 'Tau Empire (Air superiority detachment)'

    def __init__(self):
        super(FlierDetachment, self).__init__(wings=Fliers(self))


class TauV3KillTeam(Wh40kKillTeam):
    army_id = 'tau_v3_kt'
    army_name = 'Tau Empire'

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(TauV3KillTeam.KTTroops, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [
                StrikeTeam, BreacherTeam])
            UnitType(self, KrootSquad)
            self.crisis_team = UnitType(self, CrisisTeam)

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(TauV3KillTeam.KTElites, self).__init__(parent)
            self.stealth_team = UnitType(self, StealthTeam)
            self.crisis_team = UnitType(self, CrisisTeam)

    class KTFastAttack(FastSection):
        def __init__(self, parent):
            super(TauV3KillTeam.KTFastAttack, self).__init__(parent)
            Wh40kKillTeam.process_vehicle_units(self, [
                Devilfish, PiranhaTeam
            ])
            Wh40kKillTeam.process_transported_units(self, [Pathfinders])
            self.vespids = UnitType(self, Vespids)
            self.gun_drones = UnitType(self, GunDrones)

    def __init__(self):
        self.troops = self.KTTroops(self)
        self.elites = self.KTElites(self)
        super(TauV3KillTeam, self).__init__(
            self.troops, self.elites, self.KTFastAttack(self)
        )
        self.codex = SupplementOptions(self)

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_enclaves(self):
        return self.codex.cur == self.codex.enclaves

    def check_rules(self):
        super(TauV3KillTeam, self).check_rules()

        self.troops.crisis_team.visible = self.is_enclaves
        self.elites.crisis_team.visible = self.is_base
        if self.is_enclaves:
            self.troops.move_units(self.elites.crisis_team.units)
        else:
            self.elites.move_units(self.troops.crisis_team.units)


faction = 'Tau Empire'


class TauV3(Wh40k7ed, Wh40kImperial):
    army_id = 'tau_v3'
    army_name = 'Tau Empire'
    faction = faction

    def __init__(self):
        super(TauV3, self).__init__([
            TauV3CAD, FlierDetachment, HunterContingent, DawnBladeContingent,
            HunterCadre, RetaliationCadre, HeavyRetributionCadre,
            InfiltrationCadre, OptimisedStealthCadre, Firebase,
            Interdiction, Counterstrike, RapidInsertion,
            RangedSupport, Firestream, GhostkeelWing, RiptideWing,
            SkysweepWing, Ethereals, DroneNet, AirSuperiority,
            Eight, TidewallCounterstrike, EtherealHonorGuard,
            BurningDawn, TargetedReconnaissanceCadre
        ], [TauV3AD])

    def check_rules(self):
        super(TauV3, self).check_rules()
        # enclaves may ally with empire
        # but no allying between same factions is allowed
        if any([isinstance(m.sub_roster.roster, TauV3CAD)
               for m in self.primary.units]):
            enclave_flag = self.primary.units[0].sub_roster.roster.is_enclaves
            same_allies = [m.sub_roster.roster.is_enclaves
                           for m in self.secondary.units
                           if isinstance(m.sub_roster.roster, TauV3AD)]
            if sum([f == enclave_flag for f in same_allies]) > 0:
                self.error('Alliyng between same faction is prohibited')


class TauV3Missions(Wh40k7edMissions, Wh40kImperial):
    army_id = 'tau_v3_mis'
    army_name = 'Tau Empire'
    faction = faction

    def __init__(self):
        super(TauV3Missions, self).__init__([
            TauV3CAD, TauV3PA, TauV3PD, TauV3SA, TauV3SD,
            FlierDetachment, HunterContingent, DawnBladeContingent,
            HunterCadre, RetaliationCadre, HeavyRetributionCadre,
            InfiltrationCadre, OptimisedStealthCadre, Firebase,
            Interdiction, Counterstrike, RapidInsertion,
            RangedSupport, Firestream, GhostkeelWing, RiptideWing,
            SkysweepWing, Ethereals, DroneNet, AirSuperiority,
            Eight, TidewallCounterstrike, EtherealHonorGuard,
            BurningDawn, TargetedReconnaissanceCadre
        ], [TauV3AD])

    def check_rules(self):
        super(TauV3Missions, self).check_rules()
        # enclaves may ally with empire
        # but no allying between same factions is allowed
        if any([isinstance(m.sub_roster.roster, TauV3CAD)
               for m in self.primary.units]):
            enclave_flag = self.primary.units[0].sub_roster.roster.is_enclaves
            same_allies = [m.sub_roster.roster.is_enclaves
                           for m in self.secondary.units
                           if isinstance(m.sub_roster.roster, TauV3AD)]
            if sum([f == enclave_flag for f in same_allies]) > 0:
                self.error('Alliyng between same faction is prohibited')


detachments = [
    TauV3CAD,
    TauV3AD,
    TauV3PA,
    TauV3PD,
    TauV3SA,
    TauV3SD,
    FlierDetachment,
    HunterContingent,
    DawnBladeContingent,
    HunterCadre,
    RetaliationCadre,
    HeavyRetributionCadre,
    InfiltrationCadre,
    OptimisedStealthCadre,
    Firebase,
    Interdiction,
    Counterstrike,
    RapidInsertion,
    RangedSupport,
    Firestream,
    GhostkeelWing,
    RiptideWing,
    SkysweepWing,
    Ethereals,
    DroneNet,
    AirSuperiority,
    Eight,
    TidewallCounterstrike,
    EtherealHonorGuard,
    BurningDawn,
    TargetedReconnaissanceCadre
]


unit_types = [
    Farsight, Shadowsun, Aunva, Aunshi, Darkstider,
    Ethereal, Cadre, Commander, StrikeTeam, BreacherTeam, KrootSquad,
    CrisisTeam, StealthTeam, Bodyguards, Riptides, Ghostkeels,
    Devilfish, Pathfinders, Vespids, GunDrones, PiranhaTeam,
    Sunsharks, Razorsharks, BroadsideTeam, Hammerheads, SkyRays,
    SniperDroneTeam, Stormsurges
]
