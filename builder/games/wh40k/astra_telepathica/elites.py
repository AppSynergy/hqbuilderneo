from builder.core2 import *
from builder.games.wh40k.roster import Unit


class WyrdvanePsykers(Unit):
    type_name = u'Wyrdvane Psykers'
    type_id = 'astra_telepathica_wyrdvane_psykers_v1'
    # wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Wyrdvane-Psykers')

    def __init__(self, parent):
        super(WyrdvanePsykers, self).__init__(parent=parent)
        self.psykers = Count(
            self, 'Wyrdvane Psyker', min_limit=5, max_limit=10, points=12,
            gear=UnitDescription('Wyrdvane Psyker', points=12, options=[
                Gear('Laspistol'), Gear('Close combat weapon')
            ]))

    def get_count(self):
        return self.psykers.cur

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(super(WyrdvanePsykers, self).build_statistics())
