from builder.core2 import *
from builder.games.wh40k.roster import Unit

__author__ = 'Denis Romanov'


class Vehicle(OptionsList):
    def __init__(self, parent):
        super(Vehicle, self).__init__(parent, 'Options')
        self.variant('Searchlight', 1)
        self.variant('Relic plating', 3)
        self.variant('Dozer blade', 5)
        self.bolter = self.variant('Storm bolter', 5)
        self.stubber = self.variant('Heavy stubber', 5)
        self.variant('Recovery gear', 5)
        self.variant('Smoke launchers', 10)
        self.variant('Extra armour', 10)
        self.variant('Fire barrels', 10)
        self.variant('Hunter-killer missile', 10)
        self.variant('Camo netting', 15)
        self.variant('Augur array', 25)

    def check_rules(self):
        super(Vehicle, self).check_rules()
        self.process_limit([self.bolter, self.stubber], 1)


class TauroxPrime(Unit):
    type_name = 'Taurox Prime'
    type_id = 'taurox_v1'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(TauroxPrime.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Taurox battle cannon', 0)
            self.variant('Twin-linked Taurox gatling cannon', 10)
            self.variant('Taurox missile launcher', 20)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(TauroxPrime.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Twin-linked hot-shot volley gun', 0)
            self.variant('Twin-linked autocannon', 0)

    def __init__(self, parent):
        super(TauroxPrime, self) .__init__(parent=parent, points=80)
        self.Weapon1(self)
        self.Weapon2(self)
        Vehicle(self)


class Transport(OptionalSubUnit):
    def __init__(self, parent):
        super(Transport, self).__init__(parent=parent, name='Transport', limit=1)
        SubUnit(self, TauroxPrime(parent=None))


class Valkyrie(Unit):
    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Valkyrie.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Multi-laser', 0)
            self.variant('Lascannon', 10)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Valkyrie.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Hellstrike missiles', 0, gear=[Gear('Hellstrike missile', count=2)])
            self.variant('Multiple rocket pods', 10, gear=[Gear('Multiple rocket pod', count=2)])

    class Weapon3(OptionsList):
        def __init__(self, parent):
            super(Valkyrie.Weapon3, self).__init__(parent=parent, name='Sponsons')
            self.variant('Heavy bolters', 10, gear=[Gear('Heavy bolter', count=2)])

    def __init__(self, parent):
        super(Valkyrie, self).__init__(parent=parent, points=125, name="Valkyrie")
        self.Weapon1(self)
        self.Weapon2(self)
        self.Weapon3(self)


class ValkyrieSquadron(Unit):
    type_name = "Valkyrie Squadron"
    type_id = "valkyrie_squadron_v1"

    class Valkyrie(Valkyrie, ListSubUnit):
        pass

    def __init__(self, parent):
        super(ValkyrieSquadron, self).__init__(parent=parent)
        self.units = UnitList(parent=self, unit_class=self.Valkyrie, min_limit=1, max_limit=3)

    def get_count(self):
        return self.units.count
