from builder.core2 import *
from fast import Transport
from hq import CCW, Armour
from builder.games.wh40k.roster import Unit

__author__ = 'Denis Romanov'


class Tempestor(Unit):

    class Pistol(OneOf):
        def __init__(self, parent, name):
            super(Tempestor.Pistol, self).__init__(parent, name=name)
            self.variant('Hot-shot laspistol', 0)
            self.variant('Bolt pistol', 0)
            self.variant('Plasma pistol', 15)

    def __init__(self, parent):
        super(Tempestor, self).__init__(parent, name='Tempestor', gear=Armour.carapace, points=70 - 4 * 12)
        CCW(self, 'Weapon')
        self.Pistol(self, '')


class Scions(Unit):
    type_name = 'Militarum Tempestus Scions'
    type_id = 'scions_v1'

    scion = UnitDescription('Tempestus Scion', options=Armour.carapace, points=12)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Scions.Options, self).__init__(parent, name='Options')
            spec = Scions.scion
            self.caster = self.variant('Vox-caster', 5, gear=[])
            self.caster_weapon = OneOf(parent, 'Vox operator weapon', visible=False, used=False)
            self.caster_weapon.variant('Hot-shot lasgun', gear=[spec.clone().add(
                [Gear('Hot-shot lasgun'), Gear('Vox-caster')])])
            self.caster_weapon.variant('Hot-shot laspistol', gear=[spec.clone().add(
                [Gear('Hot-shot laspistol'), Gear('Vox-caster')])])

        def check_rules(self):
            super(Scions.Options, self).check_rules()
            self.caster_weapon.used = self.caster_weapon.visible = self.caster.value

    class Scion(Count):
        @property
        def description(self):
            return Scions.scion.clone().add(Gear('Hot-shot lasgun')).set_count(self.cur - self.parent.opt.count -
                                                                               sum(o.cur for o in self.parent.spec))

    def __init__(self, parent):
        super(Scions, self).__init__(parent)
        SubUnit(self, Tempestor(None))
        self.models = self.Scion(self, 'Tempestus Scion', 4, 9, points=12)
        self.opt = self.Options(self)
        self.spec = [
            Count(self, name=g['name'], min_limit=0, max_limit=2, points=g['points'],
                  gear=Scions.scion.clone().add(Gear(g['name'])).add_points(g['points']))
            for g in [
                dict(name='Flamer', points=5),
                dict(name='Greanade launcher', points=5),
                dict(name='Hot-shot volley gun', points=10),
                dict(name='Meltagun', points=10),
                dict(name='Plasma gun', points=15),
            ]
        ]
        self.transport = Transport(self)

    def check_rules(self):
        super(Scions, self).check_rules()
        Count.norm_counts(0, 2, self.spec)

    def get_count(self):
        return self.models.cur + 1

    def build_statistics(self):
        return self.note_transport(super(Scions, self).build_statistics())
