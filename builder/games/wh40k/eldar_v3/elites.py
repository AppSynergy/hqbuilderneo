__author__ = 'dvarh'
from builder.core2 import Gear, Count, UnitDescription, OneOf, SubUnit, OptionalSubUnit
from builder.games.wh40k.roster import Unit
from fast import Transport, VyperSquadron
from heavy import WarWalkers


class Banshees(Unit):
    type_name = u'Howling Banshees'
    type_id = 'banshees_v3'
    default_min = 5
    default_max = 10
    model_points = 13
    model_gear = [Gear('Aspect armor'), Gear('Shuriken pistol'), Gear('Power sword'), Gear('Banshee mask')]

    class Exarch(Unit):
        class Weapons(OneOf):
            def __init__(self, parent):
                super(Banshees.Exarch.Weapons, self).__init__(parent, 'Weapons')
                self.variant('Power sword', 0)
                self.variant('Triskele', 5)
                self.variant('Executioner', 10)

        def __init__(self, parent):
            super(Banshees.Exarch, self).__init__(parent, name='Howling Banshee Exarch', points=Banshees.model_points + 10,
                                               gear=[Gear('Aspect Armor'), Gear('Shuriken pistol'), Gear('Banshee mask')])
            self.Weapons(self)

    class BansheeExarch(OptionalSubUnit):
        def __init__(self, parent):
            super(Banshees.BansheeExarch, self).__init__(parent=parent, name='Exarch', limit=1)
            SubUnit(self, Banshees.Exarch(parent=None))

    def __init__(self, parent):
        super(Banshees, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Banshee', self.default_min, self.default_max, self.model_points, gear=UnitDescription('Banshee', self.model_points, options=self.model_gear), per_model=True)
        self.exarch = self.BansheeExarch(self)
        self.transport = Transport(parent=self)

    def get_count(self):
        return self.models.cur + self.exarch.count

    def check_rules(self):
        self.models.min = self.default_min - self.exarch.count
        self.models.max = self.default_max - self.exarch.count

    def build_statistics(self):
        return self.note_transport(super(Banshees, self).build_statistics())


class Scorpions(Unit):
    type_name = u'Striking Scorpions'
    type_id = 'scorpions_v3'
    default_min = 5
    default_max = 10
    model_points = 17
    model_gear = [Gear('Heavy Aspect armor'), Gear('Shuriken pistol'), Gear('Scorpion chainsword'), Gear('Plasma grenades'), Gear('Mandiblasters')]

    class Exarch(Unit):
        class RightWeapons(OneOf):
            def __init__(self, parent):
                super(Scorpions.Exarch.RightWeapons, self).__init__(parent, 'Weapons')
                self.variant('Chainsword', 0)
                self.variant('Bithing blade', 5)
                self.chainsabres = self.variant('Two Chainsabres', 10)

            def is_chainsabres(self):
                return self.cur == self.chainsabres

        class LeftWeapons(OneOf):
            def __init__(self, parent):
                super(Scorpions.Exarch.LeftWeapons, self).__init__(parent, 'Weapons')
                self.variant('Shuriken pistol', 0)
                self.variant('Scorpion\'s claw', 30)

        def __init__(self, parent):
            super(Scorpions.Exarch, self).__init__(parent, name='Striking Scorpion Exarch', points=Scorpions.model_points + 10,
                                               gear=[Gear('Heavy Aspect armor'), Gear('Plasma grenades'), Gear('Mandiblasters')])
            self.left_wep = self.LeftWeapons(self)
            self.right_wep = self.RightWeapons(self)

        def check_rules(self):
            super(Scorpions.Exarch, self).check_rules()
            self.left_wep.visible = not self.right_wep.is_chainsabres()

    class ScorpionExarch(OptionalSubUnit):
        def __init__(self, parent):
            super(Scorpions.ScorpionExarch, self).__init__(parent=parent, name='Exarch', limit=1)
            SubUnit(self, Scorpions.Exarch(parent=None))

    def __init__(self, parent):
        super(Scorpions, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Scorpion', self.default_min, self.default_max, self.model_points, gear=UnitDescription('Scorpion', self.model_points, options=self.model_gear), per_model=True)
        self.exarch = self.ScorpionExarch(self)
        self.transport = Transport(parent=self)

    def get_count(self):
        return self.models.cur + self.exarch.count

    def check_rules(self):
        self.models.min = self.default_min - self.exarch.count
        self.models.max = self.default_max - self.exarch.count

    def build_statistics(self):
        return self.note_transport(super(Scorpions, self).build_statistics())


class Dragons(Unit):
    type_name = u'Fire Dragons'
    type_id = 'dragons_v3'
    default_min = 5
    default_max = 10
    model_points = 22
    model_gear = [Gear('Heavy Aspect armor'), Gear('Fusion gun'), Gear('Melta bombs')]

    class Exarch(Unit):
        class Weapons(OneOf):
            def __init__(self, parent):
                super(Dragons.Exarch.Weapons, self).__init__(parent, 'Weapons')
                self.variant('Fusion gun', 0)
                self.variant('Dragon\'s breath flamer', 0)
                self.variant('Firepike', 15)

        def __init__(self, parent):
            super(Dragons.Exarch, self).__init__(parent, name='Fire Dragon Exarch', points=Dragons.model_points + 10,
                                               gear=[Gear('Heavy Aspect armor'), Gear('Melta bombs')])
            self.Weapons(self)

    class DragonExarch(OptionalSubUnit):
        def __init__(self, parent):
            super(Dragons.DragonExarch, self).__init__(parent=parent, name='Exarch', limit=1)
            SubUnit(self, Dragons.Exarch(parent=None))

    def __init__(self, parent):
        super(Dragons, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Dragon', self.default_min, self.default_max, self.model_points, gear=UnitDescription('Dragon', self.model_points, options=self.model_gear), per_model=True)
        self.exarch = self.DragonExarch(self)
        self.transport = Transport(parent=self)

    def get_count(self):
        return self.models.cur + self.exarch.count

    def check_rules(self):
        self.models.min = self.default_min - self.exarch.count
        self.models.max = self.default_max - self.exarch.count

    def build_statistics(self):
        return self.note_transport(super(Dragons, self).build_statistics())


class Wraithguards(Unit):
    type_name = u'Wraithguards'
    type_id = 'wraithguards_v3'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Wraithguards.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Wraithcannon', 0)
            self.variant('D-scythes', 10)

    def __init__(self, parent):
        super(Wraithguards, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Wraithguard', 5, 10, 32, True)
        self.wep = self.Weapon(self)
        self.transport = Transport(self)

    def get_count(self):
        return self.models.cur

    def build_points(self):
        return self.models.points + self.transport.points +\
            self.models.cur * self.wep.points

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.count)
        model = UnitDescription('Wraithguard', 32 + self.wep.points,
                                self.models.cur, self.wep.description)
        desc.add(model)
        desc.add(self.transport.description)
        return desc

    def build_statistics(self):
        return self.note_transport(super(Wraithguards, self).build_statistics())


class Wraithblades(Unit):
    type_name = u'Wraithblades'
    type_id = 'wraithblades_v3'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Wraithblades.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Two ghostsword', 0)
            self.variant('Ghost axe and forceshield', 0, gear=[
                Gear('Ghost axe'), Gear('Forceshield')])

    def __init__(self, parent):
        super(Wraithblades, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Wraithblade', 5, 10, 30, True)
        self.wep = self.Weapon(self)
        self.transport = Transport(self)

    def get_count(self):
        return self.models.cur

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.count)
        model = UnitDescription('Wraithblade', 30,
                                self.models.cur, self.wep.description)
        desc.add(model)
        desc.add(self.transport.description)
        return desc

    def build_statistics(self):
        return self.note_transport(super(Wraithblades, self).build_statistics())


class BlackGuardians(Unit):
    type_name = u'Black Guardians'
    type_id = 'blackguardians_v3'
    default_min = 10
    default_max = 20
    model_points = 11
    model_gear = [Gear('Plasma grenades')]

    class CommonGear(OneOf):
        def __init__(self, parent):
            super(BlackGuardians.CommonGear, self).__init__(parent, 'Guardian weapons')
            self.cat = self.variant('Shuriken catapult')
            self.variant('Shuriken pistol and close combat weapon', gear=[
                Gear('Shuriken pistol'), Gear('Close combat weapon')
            ])

    def __init__(self, parent):
        super(BlackGuardians, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Black Guardians', self.default_min,
                            self.default_max, self.model_points, per_model=True)
        self.common = self.CommonGear(self)
        self.flame = Count(self, 'Flamer', 0, 2, 5)
        self.fg = Count(self, 'Fusion gun', 0, 2, 10)
        self.ps = Count(self, 'Power sword', 0, 2, 15)
        from troops import GuardianHeavyWeaponTeam
        self.hwt1 = GuardianHeavyWeaponTeam(parent=self, discount=False)
        self.hwt2 = GuardianHeavyWeaponTeam(parent=self, discount=False)

    def check_rules(self):
        super(BlackGuardians, self).check_rules()
        self.hwt2.visible = self.hwt2.used = self.models.cur == 20
        Count.norm_counts(0, 2, [self.flame, self.fg])
        for wep in [self.flame, self.fg, self.ps]:
            wep.used = wep.visible = not self.common.cur == self.common.cat
        self.root.extra_checks['racist_ulthwe'] = BlackGuardians.chaos_check

    @staticmethod
    def chaos_check(roster):
        from builder.games.wh40k.chaos_marines_v2 import faction as csm_fac
        from builder.games.wh40k.khorne_daemonkin import faction as khd_fac
        from builder.games.wh40k.chaos_daemons_v3 import faction as cd_fac
        from builder.games.wh40k.dataslates.renegade_knight import faction as rk_fac
        for sec in roster.sections:
            for unit in sec.units:
                # for now, we simply check faction as there are no mixes
                if hasattr(unit, 'faction') and\
                   unit.faction in [csm_fac, khd_fac, cd_fac, rk_fac]:
                    roster.error('Ulthwe black guardian units cannot be taken in army that includes Chaos')
                    return

    def get_count(self):
        return self.models.cur

    def build_description(self):
        result = UnitDescription(self.name, self.points, self.get_count())
        base_model = UnitDescription('Black Guardian', self.model_points,
                                     options=self.model_gear)
        if self.common.cur == self.common.cat:
            result.add(base_model.add(self.common.description).set_count(self.models.cur))
        else:
            uncommon = 0
            for wep, gear in [(self.flame, [Gear('Flamer')]),
                              (self.fg, [Gear('Fusion gun')]),
                              (self.ps, [Gear('Shuriken pistol'), Gear('Power sword')])]:
                if wep.cur > 0:
                    result.add(base_model.clone().add(gear).add_points(wep.option_points)
                               .set_count(wep.cur))
                uncommon += wep.cur
            result.add(base_model.add(self.common.description).set_count(self.models.cur - uncommon))
        result.add(self.hwt1.description)
        if self.hwt2.used:
            result.add(self.hwt2.description)
        return result


class BlackWindriders(Unit):
    type_name = u'Black Guardian Windriders'
    type_id = 'blackwindriders_v1'
    model = UnitDescription('Black Guardian Windrider', 20)

    class Rider(Count):
        @property
        def description(self):
            return BlackWindriders.model.clone().add(Gear('Twin-linked shuriken catapult'))\
                                                .set_count(self.cur - self.parent.las.cur - self.parent.can.cur)

    def __init__(self, parent):
        super(BlackWindriders, self).__init__(parent)
        self.models = self.Rider(self, 'Black Guardian Windriders', 3, 10, 20, per_model=True)
        self.las = Count(self, 'Scatter laser', 0, 3, 10,
                         gear=self.model.clone().add_points(10).add(Gear('Scatter laser')))
        self.can = Count(self, 'Shuriken cannon', 0, 3, 10,
                         gear=self.model.clone().add_points(10).add(Gear('Shuriken cannon')))

    def check_rules(self):
        super(BlackWindriders, self).check_rules()
        Count.norm_counts(0, self.models.cur, [self.las, self.can])
        self.root.extra_checks['racist_ulthwe'] = BlackGuardians.chaos_check

    def get_count(self):
        return self.models.cur


class BlackVypers(VyperSquadron):
    type_id = 'blackvypers_v3'
    type_name = u'Black Guardian Vypers squadron'

    def __init__(self, parent):
        self.Vyper.name = 'Black Guardian Vyper'
        self.Vyper.base_points = 45
        super(BlackVypers, self).__init__(parent)

    def check_rules(self):
        super(BlackVypers, self).check_rules()
        self.root.extra_checks['racist_ulthwe'] = BlackGuardians.chaos_check


class BlackWarWalkers(WarWalkers):
    type_id = 'blackwarwalkers_v3'
    type_name = u'Black Guardian War Walkers'

    def __init__(self, parent):
        self.WarWalker.type_name = u'Black Guardian War Walker'
        self.WarWalker.base_points = 65
        super(BlackWarWalkers, self).__init__(parent)

    def check_rules(self):
        super(BlackWarWalkers, self).check_rules()
        self.root.extra_checks['racist_ulthwe'] = BlackGuardians.chaos_check
