__author__ = 'dvarh'
from builder.core2 import Gear, Count, UnitDescription, OneOf, SubUnit, OptionalSubUnit
from builder.games.wh40k.roster import Unit
from fast import Transport


class Warlock(Unit):
    class Weapons(OneOf):
        def __init__(self, parent):
            super(Warlock.Weapons, self).__init__(parent, 'Weapons')
            self.variant('Whitchblade', 0)
            self.variant('Singing spear', 5)

    def __init__(self, parent):
        super(Warlock, self).__init__(parent, name='Warlock Leader', points=35,
                                           gear=[Gear('Rune Armor'), Gear('Shuriken pistol')])
        self.Weapons(self)


class WarlockLeader(OptionalSubUnit):
    def __init__(self, parent):
        super(WarlockLeader, self).__init__(parent=parent, name='Warlock Leader', limit=1, order=20)
        SubUnit(self, Warlock(parent=None))


class HeavyWeaponTeam(Unit):
    model_points = 0

    class Weapons(OneOf):
        def __init__(self, parent):
            super(HeavyWeaponTeam.Weapons, self).__init__(parent, 'Weapons')
            self.variant('Shuriken cannon', 15)
            self.variant('Scatter laser', 15)
            self.variant('Bright lance', 20)
            self.variant('Starcannon', 20)
            self.variant('Eldar missile launcher', 30)

    def __init__(self, parent, discount=False):
        super(HeavyWeaponTeam, self).__init__(parent, name='Heavy Weapon Team', points=0)
        self.wep = self.Weapons(self)
        self.discount = discount

    def build_points(self):
        if self.discount:
            return 0
        else:
            return self.wep.points


class GuardianHeavyWeaponTeam(OptionalSubUnit):
    def __init__(self, parent, discount=False):
        super(GuardianHeavyWeaponTeam, self).__init__(parent=parent, name='Heavy Weapon Team', limit=1)
        self.hwt = SubUnit(self, HeavyWeaponTeam(parent=None, discount=discount))


class GuardianDefenders(Unit):
    type_name = u'Guardian Defenders'
    type_id = 'guardiandefenders_v3'
    default_min = 10
    default_max = 20
    model_points = 9
    model_gear = [Gear('Mesh armor'), Gear('Shuriken catapult'), Gear('Plasma grenades')]
    discount = False

    def __init__(self, parent):
        super(GuardianDefenders, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Guardian Defenders', self.default_min, self.default_max, self.model_points, gear=UnitDescription('Guardian Defenders', self.model_points, options=self.model_gear), per_model=True)
        self.warlock = WarlockLeader(self)
        self.hwt1 = GuardianHeavyWeaponTeam(parent=self, discount=self.discount)
        self.hwt2 = GuardianHeavyWeaponTeam(parent=self, discount=self.discount)
        self.transport = Transport(parent=self)

    def check_rules(self):
        super(GuardianDefenders, self).check_rules()
        self.hwt2.visible = self.hwt2.used = self.models.cur == 20

    def get_count(self):
        return self.models.cur + self.warlock.count + self.hwt1.count + self.hwt2.count

    def count_charges(self):
        return self.warlock.count

    def build_statistics(self):
        return self.note_charges(self.note_transport(super(GuardianDefenders, self).build_statistics()))


class StormGuardians(Unit):
    type_name = u'Storm Guardians'
    type_id = 'stormguardians_v3'
    default_min = 8
    default_max = 12
    model_points = 9
    model_gear = [Gear('Mesh armor'), Gear('Plasma grenades')]
    discount = False

    def __init__(self, parent):
        super(StormGuardians, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Storm Guardian', self.default_min, self.default_max, self.model_points, gear=UnitDescription('Storm Guardian', self.model_points, options=self.model_gear), per_model=True)
        self.flame = Count(self, 'Flamer', 0, 2, 0 if self.discount is True else 5)
        self.fg = Count(self, 'Fusion gun', 0, 2, 0 if self.discount is True else 10)
        self.ps = Count(self, 'Power sword', 0, 2, 0 if self.discount is True else 15)
        self.warlock = WarlockLeader(self)
        self.transport = Transport(parent=self)

    def check_rules(self):
        super(StormGuardians, self).check_rules()
        Count.norm_counts(0, 2, [self.flame, self.fg])
        Count.norm_counts(0, 2, [self.ps])

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        guardian = UnitDescription(
            name='Storm Guardian',
            options=StormGuardians.model_gear,
            points=StormGuardians.model_points
        )
        count = self.models.cur
        for o in [self.fg, self.flame]:
            if o.cur:
                desc.add(guardian.clone().add_points(o.option_points).add(Gear(o.name)).set_count(o.cur))
                count -= o.cur
        guardian.add(Gear('Shuriken pistol'))
        if self.ps.cur:
            desc.add(guardian.clone().
                     add(Gear('Power sword')).
                     add_points(self.ps.option_points).set_count(self.ps.cur))
            count -= self.ps.cur

        desc.add(guardian.add(Gear('Close combat weapon')).set_count(count))
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.models.cur + self.warlock.count

    def count_charges(self):
        return self.warlock.count

    def build_statistics(self):
        return self.note_charges(self.note_transport(super(StormGuardians, self).build_statistics()))


class Windriders(Unit):
    type_name = u'Windriders'
    type_id = 'windriders_v3'
    default_min = 3
    default_max = 10
    model_points = 17
    model_gear = [Gear('Mesh armor'), Gear('Eldar jetbike')]

    class WarlockWindrider(Unit):
        class Weapons(OneOf):
            def __init__(self, parent):
                super(Windriders.WarlockWindrider.Weapons, self).__init__(parent, 'Weapons')
                self.variant('Whitchblade', 0)
                self.variant('Singing spear', 5)

        def __init__(self, parent):
            super(Windriders.WarlockWindrider, self).__init__(parent, name='Windrider Warlock', points=50,
                                               gear=[Gear('Rune Armor'), Gear('Eldar jetbike')])
            self.Weapons(self)

    class WarlockLeader(OptionalSubUnit):
        def __init__(self, parent):
            super(Windriders.WarlockLeader, self).__init__(parent=parent, name='Windrider Warlock', limit=1)
            SubUnit(self, Windriders.WarlockWindrider(parent=None))

    def __init__(self, parent):
        super(Windriders, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Windrider', self.default_min, self.default_max, self.model_points, per_model=True)
        self.warlock = self.WarlockLeader(self)
        self.scatter = Count(self, 'Scatter laser', 0, 3, points=10, per_model=True)
        self.cannon = Count(self, 'Shuriken cannon', 0, 3, points=10, per_model=True)

    def check_rules(self):
        super(Windriders, self).check_rules()
        max_wep = self.models.cur
        Count.norm_counts(0, max_wep, [self.scatter, self.cannon])

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.get_count())
        windrider = UnitDescription('Windrider', self.model_points, options=self.model_gear)
        count = self.models.cur
        for o in [self.scatter, self.cannon]:
            if o.cur:
                desc.add(windrider.clone().add(Gear(o.name)).add_points(o.option_points).set_count(o.cur))
                count -= o.cur
        if count:
            desc.add(windrider.add(Gear('Twin-linked shuriken catapult')).set_count(count))
        if self.warlock.used:
            desc.add(self.warlock.description)
        return desc

    def get_count(self):
        return self.models.cur + self.warlock.count

    def count_charges(self):
        return self.warlock.count

    def build_statistics(self):
        return self.note_charges(super(Windriders, self).build_statistics())


class Rangers(Unit):
    type_name = u'Rangers'
    type_id = 'rangers_v3'
    default_min = 5
    default_max = 10
    model_points = 12
    model_gear = [Gear('Mesh armor'), Gear('Ranger long rifle'), Gear('Shuriken pistol')]

    def __init__(self, parent):
        super(Rangers, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Ranger', self.default_min, self.default_max, self.model_points, gear=UnitDescription('Ranger', self.model_points, options=self.model_gear), per_model=True)

    def get_count(self):
        return self.models.cur


class Avengers(Unit):
    type_name = u'Dire Avengers'
    type_id = 'avengers_v3'
    default_min = 5
    default_max = 10
    model_points = 13
    model_gear = [Gear('Aspect armor'), Gear('Avenger shuriken catapult'), Gear('Plasma grenades')]

    class Exarch(Unit):
        class Weapons(OneOf):
            def __init__(self, parent):
                super(Avengers.Exarch.Weapons, self).__init__(parent, 'Weapons')
                self.variant('Avenger shuriken catapult', 0)
                self.variant('Twin-linked avenger shuriken catapult', 5)
                self.variant('Power weapon and shuriken pistol', 15)
                self.variant('Diresword and shuriken pistol', 20)
                self.variant('Power weapon and shimmershield', 20)

        def __init__(self, parent):
            super(Avengers.Exarch, self).__init__(parent, name='Dire Avenger Exarch', points=Avengers.model_points + 10,
                                               gear=[Gear('Aspect Armor'), Gear('Plasma grenades')])
            self.Weapons(self)

    class AvengerExarch(OptionalSubUnit):
        def __init__(self, parent):
            super(Avengers.AvengerExarch, self).__init__(parent=parent, name='Exarch', limit=1)
            SubUnit(self, Avengers.Exarch(parent=None))

    def __init__(self, parent):
        super(Avengers, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Avenger', self.default_min, self.default_max, self.model_points, gear=UnitDescription('Avenger', self.model_points, options=self.model_gear), per_model=True)
        self.exarch = self.AvengerExarch(self)
        self.transport = Transport(parent=self)

    def get_count(self):
        return self.models.cur + self.exarch.count

    def check_rules(self):
        self.models.min = self.default_min - self.exarch.count
        self.models.max = self.default_max - self.exarch.count

    def build_statistics(self):
        return self.note_transport(super(Avengers, self).build_statistics())
