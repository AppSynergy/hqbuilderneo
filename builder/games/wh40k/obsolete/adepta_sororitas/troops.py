__author__ = 'Ivan Truskov'

from builder.core.unit import Unit
from builder.games.wh40k.obsolete.adepta_sororitas.armory import *
from builder.core.model_descriptor import ModelDescriptor


class Rhino(Unit):
    name = "Rhino"
    base_points = 40
    gear = ['Storm bolter', 'Smoke launchers', 'Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', vehicles)


class Immolator(Unit):
    name = "Immolator"
    base_points = 60
    gear = ['Smoke launchers', 'Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Twin-linked heavy flamer', 0, 'tlhflame'],
            ['Twin-linked heavy bolter', 0, 'tlhbgun'],
            ['Twin-linked multi-melta', 0, 'tlmmelta']
        ])
        self.opt = self.opt_options_list('Options', vehicles)


class Sisters(Unit):
    name = 'Battle Sister Squad'
    common_gear = ['Power armour', 'Frag grenades', 'Krak grenades']
    model_points = 12

    class Superior(Unit):
        name = 'Sister Superior'

        def __init__(self):
            self.gear = Sisters.common_gear
            self.base_points = Sisters.model_points
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [['Boltgun', 0, 'bgun']] + melee + ranged)
            self.wep2 = self.opt_one_of('', [['Bolt pistol', 0, 'bpist']] + melee + ranged)
            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
            ])
            self.vet = self.opt_options_list('', [
                ['Veteran', 10, 'vet']
            ])

        def check_rules(self):
            self.set_points(self.build_points())
            if self.vet.get('vet'):
                self.build_description(name='Veteran Sister Superior', exclude=[self.vet.id])
            else:
                self.build_description()

    def __init__(self):
        Unit.__init__(self)
        self.sup = self.opt_sub_unit(self.Superior())
        self.squad = self.opt_count('Battle Sister', 4, 19, self.model_points)
        self.opt = self.opt_options_list('Options', [
            ['Simulacrum imperialis', 10, 'simp'],
        ], limit=1)
        self.wep1 = self.opt_options_list('Weapon', special)
        self.wep2 = self.opt_options_list('', special + heavy)
        self.transport = self.opt_optional_sub_unit('Transport', [Rhino(), Immolator()], id='trans')

    def check_rules(self):
        self.points.set(self.build_points(count=1))
        self.build_description(options=[self.sup])
        des = ModelDescriptor('Battle Sister', points=self.model_points, gear=self.common_gear + ['Bolt Pistol'])
        count = self.squad.get()
        if self.opt.is_any_selected():
            self.description.add(des.clone().add_gear('Boltgun').add_gear_opt(self.opt).build(1))
            count -= 1
        for wep in [self.wep1, self.wep2]:
            if wep.is_any_selected():
                self.description.add(des.clone().add_gear_opt(wep).build(1))
                count -= 1
        self.description.add(des.clone().add_gear('Boltgun').build(count))
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.squad.get() + 1
