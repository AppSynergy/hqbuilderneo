from builder.games.wh40k.obsolete.eldar import WaveSerpent

__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit
from hq import Warlock

class Scorpions(Unit):
    name = 'Striking Scorpions'
    gear = ['Shuriken pistol','Scorpion chainsword','Mandiblaster','Plasma grenades']
    min = 5
    max = 10

    class Exarch(Unit):
        name = 'Exarch'
        base_points = 16 + 12
        gear = ['Mandiblaster','Plasma grenades']
        def __init__(self):
            Unit.__init__(self)
            self.side = self.opt_one_of('Sidearm',[
                ['Shuriken pistol',0,'shp'],
                ['Scorpion\'s claw',15,'scc']
            ])
            self.wep = self.opt_one_of('Weapon',[
                ['Scorpion chainsword',0,'scsw'],
                ['Biting blade',5,'btb']
            ])
            self.pair = self.opt_options_list('',[
                ['Chainsabres', 5, 'chsbr' ]
            ])
            self.opt = self.opt_options_list('Warrior powers',[
                ['Stalker', 5, 'stlk'],
                ['Shadowstrike', 20, 'shstr']
            ])

        def check_rules(self):
            self.wep.set_visible(not self.pair.get('chsbr'))
            self.side.set_visible(not self.pair.get('chsbr'))
            Unit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Striking Scorpion', self.min, self.max,16)
        self.leader = self.opt_optional_sub_unit(self.Exarch.name, self.Exarch())
        self.transport = self.opt_optional_sub_unit('Transport', [WaveSerpent()])

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(self.min - ldr, self.max - ldr)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id])

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()
        
class Dragons(Unit):
    name = 'Fire Dragons'
    gear = ['Fusion gun','Melta bombs']
    min = 5
    max = 10

    class Exarch(Unit):
        name = 'Exarch'
        base_points = 16 + 12
        gear = ['Melta bombs']
        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon',[
                ['Fusion gun',0,'fg'],
                ['Dragon\s breath flamer', 0, 'ddbflame'],
                ['Firepike',8,'pike']
            ])

            self.opt = self.opt_options_list('Warrior powers',[
                ['Crack shot', 5, 'coca'],
                ['Tank Hunters', 15, 'thnt']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Fire Dragon', self.min, self.max,16)
        self.leader = self.opt_optional_sub_unit(self.Exarch.name, self.Exarch())
        self.transport = self.opt_optional_sub_unit('Transport', [WaveSerpent()])

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(self.min - ldr, self.max - ldr)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id])

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()
        
class Wraithguard(Unit):
    name = 'Wraithguard'
    gear = ['Wraithcannon']
    min = 3
    max = 10

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Wraithguard', self.min, self.max,35)
        self.leader = self.opt_optional_sub_unit(Warlock.name, Warlock())
        self.transport = self.opt_optional_sub_unit('Transport', [WaveSerpent()])

    def check_rules(self):
        self.transport.set_active(self.warriors.get() <= 5)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id])

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()
        
class Banshees(Unit):
    name = 'Howling Banshees'
    gear = ['Banshee mask','Shuriken pistol','Power weapon']
    min = 5
    max = 10

    class Exarch(Unit):
        name = 'Exarch'
        base_points = 16 + 12

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon',[
                ['Power weapon',0,'pwep'],
                ['Triskele', 5, 'trskl'],
                ['Executioner',10,'exec'],
                ['Mirrorswords',10,'msw']
            ])

            self.opt = self.opt_options_list('Warrior powers',[
                ['War shout', 5, 'wsh'],
                ['Acrobatic', 15, 'acrb']
            ])

        def check_rules(self):
            self.gear = ['Banshee mask']
            if self.wep.get_cur() != 'msw':
                self.gear += ['Shuriken pistol']
            Unit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Howling Banshee', self.min, self.max,16)
        self.leader = self.opt_optional_sub_unit(self.Exarch.name, self.Exarch())
        self.transport = self.opt_optional_sub_unit('Transport', [WaveSerpent()])

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(self.min - ldr, self.max - ldr)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id])

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()
        
class Harlequins(Unit):
    name = 'Harlequins'

    class Harlequin(ListSubUnit):
        name = 'Harlequin'
        base_points = 18
        max = 10
        gear = ['Flip belt','Holo-suit']
        def __init__(self):
            ListSubUnit.__init__(self)
            self.ccw = self.opt_one_of('Weapon',[
                ['Close combat weapon',0,'ccw'],
                ['Harlequin\'s kiss',4,'hkiss']
                    ])
            self.rng = self.opt_one_of('',[
                ['Shuriken pistol',0,'spist'],
                ['Fusion pistol',10,'fpist']
                ])

        def has_fusion(self):
            return self.count.get() if self.rng.get_cur() == 'fpist' else 0


    class DeathJester(Unit):
        name = 'Death Jester'
        base_points = 28
        gear = ['Flip belt','Holo-suit', 'Shrieker cannon']

        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_one_of('Weapon',[
                ['Close combat weapon',0,'ccw'],
                ['Harlequin\'s kiss',4,'hkiss']
            ])


    class Shadowseer(Unit):
        gear = ['Flip belt','Holo-suit', 'Hallucinogen grenades']
        name = 'Shadowseer'
        base_points = 48

        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_one_of('Weapon',[
                ['Close combat weapon',0,'ccw'],
                ['Harlequin\'s kiss',4,'hkiss']
            ])
            self.rng = self.opt_one_of('',[
                ['Shuriken pistol',0,'spist'],
                ['Fusion pistol',10,'fpist']
            ])

        def has_fusion(self):
            return 1 if self.rng.get_cur() == 'fpist' else 0


    class TroupeMaster(Unit):
        name = 'Troupe Master'
        base_points = 38
        gear = ['Flip belt','Holo-suit']
        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_one_of('Weapon',[
                ['Close combat weapon',0,'ccw'],
                ['Power weapon',0,'psw'],
                ['Harlequin\'s kiss',0,'hkiss']
            ])
            self.rng = self.opt_one_of('',[
                ['Shuriken pistol',0,'spist'],
                ['Fusion pistol',10,'fpist']
            ])

        def has_fusion(self):
            return 1 if self.rng.get_cur() == 'fpist' else 0

    def __init__(self):
        Unit.__init__(self)
        self.troupe = self.opt_units_list(self.Harlequin.name,self.Harlequin,5,10)
        self.dj = self.opt_optional_sub_unit(self.DeathJester.name, self.DeathJester())
        self.shs = self.opt_optional_sub_unit(self.Shadowseer.name, self.Shadowseer())
        self.tm = self.opt_optional_sub_unit(self.TroupeMaster.name, self.TroupeMaster())

    def get_count(self):
        return self.troupe.get_count() + self.dj.get_count() + self.shs.get_count() + self.tm.get_count()

    def check_rules(self):
        ldrs = self.dj.get_count() + self.shs.get_count() + self.tm.get_count()
        self.troupe.update_range(5 - ldrs, 10 - ldrs)
        def get_unit(u):
            u = u.get_unit()
            return [u.get_unit()] if u else []
        list = self.troupe.get_units() + get_unit(self.shs) + get_unit(self.tm)
        fusions = reduce(lambda val, u: val + u.has_fusion() if u else val, list, 0)
        if fusions > 2:
            self.error("You can't take more then 2 fusion pistols")
        self.set_points(self.build_points(count=1))
        self.build_description(count=1)
