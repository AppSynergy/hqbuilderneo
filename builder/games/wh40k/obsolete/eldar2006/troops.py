
__author__ = 'Ivan Truskov'

from transport import WaveSerpent
from builder.core.unit import Unit
from hq import Warlock

class Avengers(Unit):
    name = 'Dire Avengers'
    gear = ['Avenger shuriken catapult']
    min = 5
    max = 10

    class Exarch(Unit):
        name = 'Exarch'
        base_points = 12 + 12
        def __init__(self):
            Unit.__init__(self)

            self.wep = self.opt_one_of('Weapon',[
                ['Avenger shuriken catapult',0,'asc'],
                ['Two Avenger shuriken catapults',5,'2asc'],
                ['Diresword and shuriken pistol',10,'pair1'],
                ['Power weapon and shimmershield',15,'pair2']
            ])

            self.opt = self.opt_options_list('Warrior powers',[
                ['Defend', 15, 'dfd'],
                ['Bladestorm', 15, 'bst']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Dire Avenger', self.min, self.max,12)
        self.leader = self.opt_optional_sub_unit(self.Exarch.name, self.Exarch())
        self.transport = self.opt_optional_sub_unit('Transport', [WaveSerpent()])

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(self.min - ldr, self.max - ldr)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id])

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()
        
class Rangers(Unit):
    name = 'Rangers'
    gear = ['Ranger long rifle', 'Shuriken pistol']
    base_points = 19
    min = 5
    max = 10
    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Ranger', self.min, self.max,19)
        self.path = self.opt_options_list('Promotion',[
                        ['Upgrade to Pathfinders',5,'path']
                            ])
    def get_count(self):
        return self.warriors.get()
        
    def check_rules(self):
        if self.path.get('path'):
            name = 'Pathfinders'
        else:
            name = 'Rangers'

        self.set_points(self.build_points(exclude=[self.warriors.id]))
        self.build_description(name=name, exclude=[self.warriors.id,self.path.id])
        
class Guardians(Unit):
    name = 'Guardians'
    min = 10
    max = 20
    gear = ['Shuriken catapult']

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Guardian', self.min, self.max,8)
        self.heavy = self.opt_one_of('Heavy weapon platform',[
                            ['Shuriken cannon',5,'shcan'],
                            ['Scatter laser',15,'scat'],
                            ['Eldar missile launcher',20,'eml'],
                            ['Starcannon',25,'scan'],
                            ['Bright lance',30,'blance']
                                ])
        self.leader = self.opt_optional_sub_unit(Warlock.name, Warlock())
        self.transport = self.opt_optional_sub_unit('Transport', [WaveSerpent()])

    def check_rules(self):
        self.transport.set_active(self.get_count() <= 12)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id, self.heavy.id])
        self.description.add(self.heavy.get_selected())

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()
        
class BikerGuardians(Unit):
    name = 'Guardian Jetbike Squadrron'
    min = 3
    max = 12
    gear = ['Eldar jetbike']

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Guardian', self.min, self.max, 22)
        self.hvy = self.opt_count('Shuriken cannon', 0, 0, 10)
        self.leader = self.opt_optional_sub_unit(Warlock.name, Warlock(biker=True))

    def check_rules(self):
        self.hvy.update_range(0, int(self.warriors.get() / 3))
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id, self.hvy.id])
        self.description.add('Twin-linked shuriken catapults', self.warriors.get() - self.hvy.get())
        self.description.add(self.hvy.get_selected())

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()
        
class TWraithguard(Unit):
    name = 'Wraithguard'
    gear = ['Wraithcannon']
    min = 10
    max = 10

    class Spiritseer(Unit):
        gear = ['Rune armour','Shuriken pistol']
        name = "Spiritseer"
        base_points = 31        
        def __init__(self):
            Unit.__init__(self)
            self.powers = self.opt_options_list('Powers',[
                            ['Conceal',15,'con'],
                            ['Destructor',10,'dtr'],
                            ['Embolden',5,'emb'],
                            ['Enhance',15,'enh']
                                ],1)
            self.wep = self.opt_one_of('Weapon',[
                            ['Witchblade',0,'wsw'],
                            ['Singing spear',3,'ssp']
                                ])

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Wraithguard', self.min, self.max,35)
        self.leader = self.opt_sub_unit(self.Spiritseer())
        self.transport = self.opt_optional_sub_unit('Transport', [WaveSerpent()])

    def check_rules(self):
        self.transport.set_active(self.warriors.get() <= 5)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id])

    def get_count(self):
        return self.warriors.get() + 1