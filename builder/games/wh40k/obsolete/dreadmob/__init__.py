__author__ = 'Ivan'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.old_roster import Ally
from hq import *
from elites import *
from troops import *
from fast import *
from heavy import *
from builder.games.wh40k.obsolete.orks.orks_ex.hq import Buzzgob


class Dreadmob(LegacyWh40k):
    army_id = '819c684e9a3c481f8641447053ba0620'
    army_name = 'Dreadmob'
    imperial_armour = True
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(
            self,
            hq=[Buzzgob, BigMek, PainBoss, KustomMekaDread],
            elites=[Junka, BurnaBoyz, SlashaMob],
            troops=[Boyz, Gretchins, DeffDreadMob],
            fast=[Deffkoptas, Warkoptas, Kans, GrotTanks],
            heavy=[MegaDread, Lootas, LootedWagon, Lifta, BigTrakk],
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.ORCS)
        )

    def check_rules(self):
        if self.troops.count_unit(Boyz) == 0:
            self.error('Dreadmob must include at last one Spanna Boyz unit.')
        pain_bosses = self.hq.count_units([PainBoss])
        cyb_sp = reduce(lambda acc, cur: acc + 1 if cur.are_cyb() else 0, self.troops.get_units(Boyz), 0)
        if pain_bosses < cyb_sp:
            self.error("Only one unit of Spanna Boyz per Pain Boss may be upgraded to have cybork bodies.")
