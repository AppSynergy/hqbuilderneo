__author__ = 'Ivan Truskov'

from builder.core.unit import ListUnit, ListSubUnit


class Deffkoptas(ListUnit):
    name = 'Deffkoptas'

    class Deffkopta(ListSubUnit):
        name = 'Deffkopta'
        base_points = 35
        gear = ['Choppa']

        def __init__(self):
            ListSubUnit.__init__(self, min_models=1, max_models=5)
            self.wep = self.opt_one_of('Weapon', [
                ["Twin-linked Big Shoota", 0], 
                ["Twin-linked Rokkit Launcha", 10], 
                ["Kustom Mega-Blasta", 5], 
            ])
            self.opt = self.opt_options_list('Options', [
                ["Bigbomm", 15], 
                ["Buzzsaw", 10], 
            ])

    def __init__(self):
        ListUnit.__init__(self, self.Deffkopta, 1, 5)


class Warkoptas(ListUnit):
    name = 'Warkoptas'

    class Warkopta(ListSubUnit):
        name = 'Warkopta'
        base_points = 65

        def __init__(self):
            ListSubUnit.__init__(self, min_models=1, max_models=3)
            self.wep1 = self.opt_one_of('Weapon', [
                ["Big Shoota", 0], 
                ["Twin-linked Rokkit Launcha", 10], 
                ["Kustom Mega-Blasta", 15], 
            ])
            self.wep2 = self.opt_one_of('', [
                ["Twin-linked Deffgun", 0], 
                ["Twin-linked Rattler Cannon", 10], 
            ])
            self.opt = self.opt_options_list('Options', [
                ['Red Paint Job', 5], 
                ['Stikkbomb Chukkas', 5]
            ])
            self.bmb = self.opt_count('Bigbombs', 0, 2, 15)

    def __init__(self):
        ListUnit.__init__(self, self.Warkopta, 1, 3)


class Kans(ListUnit):
    name = "Killa Kan attack mob"

    class Kan(ListSubUnit):
        name = 'Killa Kan'
        base_points = 40
        gear = ['Dreadnought Close Combat weapon']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ["Big Shoota", 0], 
                ["Scorcha", 0], 
                ["Grotzooka", 5], 
                ["Rokkit Launcha", 10], 
                ["Kustom mega-blasta", 15]

            ])
            self.opt = self.opt_options_list('Options', [
                ["Grot riggers", 5], 
                ["Armor plates", 10], 
            ])

    def __init__(self):
        ListUnit.__init__(self, self.Kan, 3, 5)


class GrotTanks(ListUnit):
    name = "Grot Tank battle mob"

    class GrotTank(ListSubUnit):
        name = 'Grot Tank'
        base_points = 30

        def set_paint(self, paint):
            self.wep2.set_visible(self.up.get('up'))
            options = [self.wep1, self.wep2, self.opt, self.up, paint]
            single_points = self.build_points(options=options, count=1)
            self.points.set(single_points * self.get_count())
            self.build_description(points=single_points, count=1, options=options)

        def __init__(self):
            ListSubUnit.__init__(self, min_models=1, max_models=6)
            self.wep1 = self.opt_one_of('Weapon', [
                ["Big Shoota", 5], 
                ["Scorcha", 5], 
                ["Grotzooka", 10], 
                ["Rokkit Launcha", 15], 
                ["Kustom mega-blasta", 20]
            ])
            self.wep2 = self.opt_one_of('', [
                ["Big Shoota", 5], 
                ["Scorcha", 5], 
                ["Grotzooka", 10], 
                ["Rokkit Launcha", 15], 
                ["Kustom mega-blasta", 20]
            ])
            self.opt = self.opt_options_list('Options', [
                ["Pintle-mounted shoota", 5]
            ])
            self.up = self.opt_options_list('', [
                ['Kommanda', 15, 'up']
            ])

        def is_kommanda(self):
            return self.get_count() if self.up.get('up') else 0

        def check_rules(self):
            pass

    def __init__(self):
        ListUnit.__init__(self, self.GrotTank, min_num=3, max_num=6)
        self.opt = self.opt_options_list('Options', [
            ['Red Paint Job', 5, 'rpj']
        ])

    def check_rules(self):
        self.units.update_range()
        for tnk in self.units.get_units():
            tnk.set_paint(self.opt)
        kom = sum((u.is_kommanda() for u in self.units.get_units()))
        if kom > 1:
            self.error('Only one Kommanda may be in the mob! (taken: {})'.format(kom))
        self.set_points(self.build_points(count=1, exclude=[self.opt.id]))
        self.build_description(count=1, exclude=[self.opt.id])
