__author__ = 'Ivan Truskov'

from builder.games.wh40k.section import Formation, UnitType
from builder.games.wh40k.roster import Wh40k7ed, Wh40kImperial
from elites import SilentSistersSquad


class NullMaidens(Formation):
    army_name = 'Null Maiden Task Force'
    army_id = 'sisters_silent_v1_task'

    def __init__(self):
        super(NullMaidens, self).__init__()
        UnitType(self, SilentSistersSquad, min_limit=1, max_limit=3)


faction = 'Sisters of Silence'


class SistersOfSilence(Wh40kImperial, Wh40k7ed):
    army_id = 'sisters_silent_v1'
    army_name = 'Sisters of Silence'
    faction = faction
    obsolete = True

    def __init__(self):
        super(SistersOfSilence, self).__init__([NullMaidens])

detachments = [NullMaidens]
