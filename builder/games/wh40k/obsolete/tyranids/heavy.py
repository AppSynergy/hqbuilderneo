__author__ = 'dante'

from builder.core.unit import Unit, StaticUnit
from builder.games.wh40k.obsolete.tyranids.elites import MyceticSpore


class Carnifex(Unit):
    name = 'Carnifex Brood'
    base_points = 160
    gear = ['Bonded Exoskeleton']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Carnifex', 1, 3, self.base_points)
        self.wep1 = self.opt_one_of('Weapon',
            [
                ['Scything talons', 0],
                ['Twin-linked deathspitter', 15],
                ['Twin-linked devourers with brainleech worms', 15],
                ['Stranglethorn cannon', 20, 'sc'],
                ['Heavy venom cannon', 25, 'hv'],
                ], 'wep1')
        self.wep2 = self.opt_one_of('Weapon',
            [
                ['Scything talons', 0, 'st'],
                ['Twin-linked deathspitter', 15],
                ['Twin-linked devourers with brainleech worms', 15],
                ['Crushing claws', 25, 'sc'],
                ], 'wep2')

        self.opt1 = self.opt_options_list('Options', [
            ["Frag spines",5],
            ["Adrenal glands", 10],
            ["Toxin sacs", 10],
            ["Bio plasma", 20],
            ["Regeneration", 20],
            ], id='opt1')
        self.spore = self.opt_options_list('Drop', [["Mycetic Spore", 40, 'ms']])
        self.spore_unit = self.opt_sub_unit(MyceticSpore())

    def check_rules(self):
        self.spore.set_active_options(['ms'], self.count.get() == 1)
        self.spore_unit.set_active(self.spore.get('ms') and self.count.get() == 1)
        self.points.set(self.count.get() * (self.base_points + self.opt1.points() + self.wep1.points() +
                                            self.wep2.points()) + self.spore_unit.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.count.get())
        self.description.add(self.wep1.get_selected(), self.count.get())
        self.description.add(self.wep2.get_selected(), self.count.get())
        self.description.add(self.opt1.get_selected(), self.count.get())
        self.description.add(self.spore_unit.get_selected())
        self.set_description(self.description)


class TheOld(StaticUnit):
    name = 'Old One Eye'
    base_points = 260
    gear = ['Scything talons', 'Crushing claws', 'Bonded Exoskeleton']


class Biovore(Unit):
    name = 'Biovore Brood'
    base_points = 45
    gear = ['Claws and teeth', 'Hardened carapace', 'Spore mine launcher']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Biovore', 1, 3, self.base_points)

    def check_rules(self):
        self.points.set(self.count.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.count.get())
        self.set_description(self.description)


class Trygon(Unit):
    name = 'Trygon'
    base_points = 200
    gear = ['Bonded Exoskeleton', 'Bio-electric pulse', 'Scything talons', 'Scything talons']

    def __init__(self):
        Unit.__init__(self)
        self.prime = self.opt_options_list('Trygon Prime', [['Upgrade', 40, 'prime']])
        self.opt1 = self.opt_options_list('Options', [
            ["Adrenal glands", 10],
            ["Toxin sacs", 10],
            ["Regeneration", 25],
            ])

    def check_rules(self):
        Unit.check_rules(self)
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear)
        if self.prime.get('prime'):
            self.description.add(['Trygon Prime', 'Containment spines'])
        self.description.add(self.opt1.get_selected())
        self.set_description(self.description)


class Mawloc(Unit):
    name = 'Mawloc'
    base_points = 170
    gear = ['Claws and teeth', 'Bonded Exoskeleton']

    def __init__(self):
        Unit.__init__(self)
        self.opt1 = self.opt_options_list('Options', [
            ["Adrenal glands", 10],
            ["Toxin sacs", 10],
            ["Regeneration", 25],
            ])


class Tyrannofex(Unit):
    name = 'Tyrannofex'
    base_points = 250
    gear = ['Claws and teeth', 'Armoured shell']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon',
            [
                ['Stinger salvo', 0],
                ['Cluster spines', 0],
                ], 'wep1')
        self.wep2 = self.opt_one_of('Weapon',
        [
            ['Acid spray', 0],
            ['Fleshborer hive', 10],
            ['Rupture cannon', 15],
            ], 'wep2')
        self.thorax = self.opt_one_of('Thorax swarm',
            [
                ['Electroshock grubs', 0],
                ['Desiccator larvae', 0],
                ['Shreddershard beetles', 0],
                ])
        self.opt1 = self.opt_options_list('Options', [
            ["Adrenal glands", 10],
            ["Toxin sacs", 10],
            ["Regeneration", 30],
            ])
