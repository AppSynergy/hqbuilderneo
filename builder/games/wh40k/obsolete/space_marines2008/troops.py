__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit, StaticUnit
from builder.core.options import norm_counts


class Rhino(Unit):
    name = "Rhino"
    base_points = 35
    gear = ['Storm bolter','Smoke launchers','Searchlight']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options',
            [
                ["Storm bolter", 10, 'sbgun'],
                ["Hunter-killer missile", 10, 'hkm'],
                ["Dozer blade", 5, 'dblade'],
                ["Extra armour", 15, 'exarm']
                ])


class Razorback(Unit):
    name = "Razorback"
    base_points = 40
    gear = ['Smoke launchers','Searchlight']
    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon',
            [
                ["Twin-linked heavy bolter", 0, 'tlhbgun'],
                ["Twin-linked heavy flamer", 25, 'tlhflame'],
                ["Twin-linked assault cannon", 35, 'tlasscan'],
                ["Twin-linked lascannon", 35, 'tllcannon'],
                ["Lascannon and twin-linked plasma gun", 35, 'lascplasgun']
                ])
        self.opt = self.opt_options_list('Options',
            [
                ["Storm bolter", 10, 'sbgun'],
                ["Hunter-killer missile", 10, 'hkm'],
                ["Dozer blade", 5, 'dblade'],
                ["Extra armour", 15, 'exarm']
                ])


class DropPod(Unit):
    name = "Drop pod"
    base_points = 35
    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon',
        [
            ["Storm bolter", 0, 'sbgun'],
            ["Deathwind missile launcher", 20, 'dwind']
            ])
        self.opt = self.opt_options_list('Options',
            [
                ["Locator beacon", 10, 'lbcon']
                ])


class TacticalSquad(Unit):
    name = 'Tactical Squad'
    gear = ['Power armor', 'Bolt pistol', 'Frag grenades', 'Krak grenades']

    class SpaceMarineSergeant(Unit):
        name = 'Space Marine Sergeant'
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']
        base_points = 90 - 16 * 4
        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon',
                [
                    ['Boltgun', 0, 'bgun' ],
                    ['Chainsword', 0, 'chain' ],
                    ['Combi-melta', 10, 'cmelta' ],
                    ['Combi-flamer', 10, 'cflame' ],
                    ['Combi-plasma', 10, 'cplasma' ],
                    ['Stormbolter', 10, 'sbgun' ],
                    ['Plasma pistol', 15, 'ppist' ],
                    ['Power weapon', 15, 'pw' ],
                    ['Power fist', 25, 'pf' ]
                    ],    id='sgt_w1')
            self.wep2 = self.opt_one_of('Weapon',
                [
                    ['Bolt pistol', 0, 'bpist' ],
                    ['Chainsword', 0, 'chain' ],
                    ['Combi-melta', 10, 'cmelta' ],
                    ['Combi-flamer', 10, 'cflame' ],
                    ['Combi-plasma', 10, 'cplasma' ],
                    ['Stormbolter', 10, 'sbgun' ],
                    ['Plasma pistol', 15, 'ppist' ],
                    ['Power weapon', 15, 'pw' ],
                    ['Power fist', 25, 'pf' ]
                    ],    id='sgt_w2')
            self.opt = self.opt_options_list('Options',
            [
                ["Melta bombs", 5, 'mbomb'],
                ["Teleport homer", 15, 'thomer'],
                ])

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_count('Space Marine',4,9,16)
        self.hvy1 = self.opt_one_of('Heavy weapon',
            [
                ['Boltgun', 0, 'bgun' ],
                ['Flamer', 0, 'flame' ],
                ['Meltagun', 5, 'mgun' ],
                ['Plasma gun',10,'pgun']
                ])
        self.hvy2 = self.opt_one_of('Heavy weapon',
            [
                ['Boltgun', 0, 'bgun' ],
                ['Heavy bolter', 0, 'hbgun' ],
                ['Multi-melta', 0, 'mulmgun' ],
                ['Missile launcher', 0, 'mlaunch' ],
                ['Plasma cannon', 5, 'pcannon' ],
                ['Lascannon', 10, 'lcannon']
                ])

        self.sergeant = self.opt_sub_unit(self.SpaceMarineSergeant())
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])

    def check_rules(self):
        heavy = self.marines.get() + 1 == 10
        self.hvy1.set_visible(heavy)
        self.hvy2.set_visible(heavy)
        self.set_points(self.build_points(count=1))
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.marines.get())
        if heavy:
            self.description.add(self.hvy1.get_selected())
            self.description.add(self.hvy2.get_selected())
            self.description.add('Boltgun', self.marines.get() - 2)
        else:
            self.description.add('Boltgun', self.marines.get())
        self.description.add(self.sergeant.get_selected())
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.marines.get() + 1

class ScoutSquad(Unit):
    name = 'Scout Squad'

    class Telion(StaticUnit):
        name = 'Sergeant Telion'
        base_points = 50 + 75 - 13 * 4
        gear = ['Scout armour','Bolt pistol','Stalker Pattern Boltgun','Frag grenades','Krak grenades','Camo cloak']


    class ScoutSergeant(Unit):
        name = 'Scout Sergeant'
        gear = ['Scout armour','Frag grenades','Krak grenades']
        base_points = 75 - 13 * 4

        def __init__(self):
            Unit.__init__(self)
            wep = [
                ['Combi-melta', 10, 'cmelta' ],
                ['Combi-flamer', 10, 'cflame' ],
                ['Combi-plasma', 10, 'cplasma' ],
                ['Plasma pistol', 15, 'ppist' ],
                ['Power weapon', 15, 'pw' ],
                ['Power fist', 25, 'pf' ]
            ]
            self.wep1 = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bgun' ],
                ['Sniper rifle', 0],
                ['Shotgun', 0],
                ['Combat blade', 0],
            ] + wep)
            self.wep2 = self.opt_one_of('', [['Bolt pistol', 0, 'bpist' ]] + wep)
            self.opt = self.opt_options_list('Options', [
                ["Melta bombs", 5, 'mbomb'],
                ["Teleport homer", 15, 'thomer'],
            ])

    class Scout(ListSubUnit):
        base_points = 13
        name = 'Scout'
        min = 1
        max = 9
        gear = ['Scout armour','Frag grenades','Krak grenades', 'Bolt pistol']

        def __init__(self):
            ListSubUnit.__init__(self)

            self.wep = self.opt_one_of('Weapon', [
                ['Boltgun', 0],
                ['Sniper rifle', 0],
                ['Shotgun', 0],
                ['Combat blade', 0],
                ['Heavy Bolter', 10, 'hb'],
                ['Missile Launcher', 10, 'ml'],
            ])

        def has_special(self):
            return (1 if self.wep.get_cur() in ['hb', 'ml'] else 0) * self.count.get()

    def __init__(self):
        Unit.__init__(self)
        self.scouts = self.opt_units_list(self.Scout.name, self.Scout, 4, 9)
        self.telion = self.opt_optional_sub_unit('', [self.Telion()])
        self.sergeant = self.opt_sub_unit(self.ScoutSergeant())
        self.opt = self.opt_options_list('Options',[["Camo cloaks", 3, 'cmcl']])

    def get_count(self):
        return self.scouts.get_count() + 1
        
    def check_rules(self):
        self.scouts.update_range()
        self.sergeant.set_active(not self.telion.get(self.Telion.name))
        self.points.set(self.build_points(exclude=[self.opt.id], count=1) +
                        (self.scouts.get_count() + self.sergeant.get_count()) * self.opt.points())
        spec_limit = 1
        spec_total = reduce(lambda val, u: u.has_special() + val, self.scouts.get_units(), 0)
        if spec_limit < spec_total:
            self.error('Special weapon (Heavy Bolter, Missile Launcher) is over limit ({0})'.format(spec_limit))
        self.build_description(exclude=[self.opt.id])
        self.description.add(self.opt.get_selected(), self.scouts.get_count() + self.sergeant.get_count())

    def get_unique(self):
        if self.telion.get_count():
            return self.Telion.name