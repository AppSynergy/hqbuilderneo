from builder.core2 import *
from builder.games.wh40k.obsolete.orks import hq, heavy, fast, elites, troops
from builder.games.wh40k.old_roster import Wh40k
from builder.games.wh40k.roster import HQSection, ElitesSection, TroopsSection, FastSection, HeavySection, Fort
from builder.games.wh40k.old_roster import Ally
from builder.games.wh40k.escalation.orks import LordsOfWar as OrksLordsOfWar


__author__ = 'Denis Romanov'


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.ghazghkullthraka = UnitType(self, Unit.norm_core1_unit(hq.GhazghkullThraka))
        self.maddokgrotsnik = UnitType(self, Unit.norm_core1_unit(hq.MadDokGrotsnik))
        self.wazdakkagutsmek = UnitType(self, Unit.norm_core1_unit(hq.WazdakkaGutsmek))
        self.oldzogwort = UnitType(self, Unit.norm_core1_unit(hq.OldZogwort))
        self.warboss = UnitType(self, Unit.norm_core1_unit(hq.Warboss))
        self.bigmek = UnitType(self, Unit.norm_core1_unit(hq.BigMek))
        self.weirdboy = UnitType(self, Unit.norm_core1_unit(hq.Weirdboy))


class HQ(BaseHQ):
    pass


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.meganobz = UnitType(self, Unit.norm_core1_unit(elites.Meganobz))
        self.nobz = UnitType(self, Unit.norm_core1_unit(elites.Nobz))
        self.burnaboyz = UnitType(self, Unit.norm_core1_unit(elites.BurnaBoyz))
        self.tankbustas = UnitType(self, Unit.norm_core1_unit(elites.Tankbustas))
        self.lootas = UnitType(self, Unit.norm_core1_unit(elites.Lootas))
        self.kommandos = UnitType(self, Unit.norm_core1_unit(elites.Kommandos))


class Elites(BaseElites):
    pass


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.orkboyz = UnitType(self, Unit.norm_core1_unit(troops.OrkBoyz))
        self.gretchins = UnitType(self, Unit.norm_core1_unit(troops.Gretchins))
        self.meganobz = UnitType(self, Unit.norm_core1_unit(elites.Meganobz))
        self.nobz = UnitType(self, Unit.norm_core1_unit(elites.Nobz))
        self.warbikers = UnitType(self, Unit.norm_core1_unit(fast.Warbikers))
        self.deffdread = UnitType(self, Unit.norm_core1_unit(heavy.DeffDread))


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        self.stormboyz = UnitType(self, Unit.norm_core1_unit(fast.Stormboyz))
        self.warbikers = UnitType(self, Unit.norm_core1_unit(fast.Warbikers))
        self.warbuggies = UnitType(self, Unit.norm_core1_unit(fast.Warbuggies))
        self.deffkoptas = UnitType(self, Unit.norm_core1_unit(fast.Deffkoptas))
        self.dakkajet = UnitType(self, Unit.norm_core1_unit(fast.Dakkajet))
        self.burnabommer = UnitType(self, Unit.norm_core1_unit(fast.BurnaBommer))
        self.blitzabommer = UnitType(self, Unit.norm_core1_unit(fast.BlitzaBommer))


class FastAttack(BaseFastAttack):
    pass


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        self.battlewagon = UnitType(self, Unit.norm_core1_unit(heavy.Battlewagon))
        self.deffdread = UnitType(self, Unit.norm_core1_unit(heavy.DeffDread))
        self.lootedwagon = UnitType(self, Unit.norm_core1_unit(heavy.LootedWagon))
        self.kans = UnitType(self, Unit.norm_core1_unit(heavy.Kans))
        self.gunz = UnitType(self, Unit.norm_core1_unit(heavy.Gunz))
        self.flashgitz = UnitType(self, Unit.norm_core1_unit(heavy.FlashGitz))


class HeavySupport(BaseHeavySupport):
    pass


class LordsOfWar(OrksLordsOfWar):
    pass


class OrksV2(Wh40k):
    army_id = 'orks_v2'
    army_name = 'Orks'
    obsolete = True

    def __init__(self, secondary=False):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)

        super(OrksV2, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self),
            ally=Ally(parent=self, ally_type=Ally.ORCS),
            secondary=secondary
        )

    def has_grotsnik(self):
        return self.hq.maddokgrotsnik.count > 0

    def check_rules(self):
        super(OrksV2, self).check_rules()
        big_bosses = self.hq.warboss.count + self.hq.ghazghkullthraka.count
        self.troops.nobz.active = self.troops.meganobz.active = big_bosses > 0
        if big_bosses < self.troops.nobz.count + self.troops.meganobz.count:
            self.error("You can't have more Nobz or Meganobz in Troops then Warbosses (include Ghazghkull Thraka) "
                       "in HQ.")

        bike_boss = self.hq.wazdakkagutsmek.count
        self.troops.warbikers.active = bike_boss
        if not bike_boss and self.troops.warbikers.count > 0:
            self.error("You can't have Warbikers in Troops without Wazdakka Gutsmek in HQ.")

        self.check_unit_limit(
            count=self.hq.bigmek.count, units=self.troops.deffdread,
            message="You can't have more Deff Dreads in Troops then Big Meks in HQ."
        )
