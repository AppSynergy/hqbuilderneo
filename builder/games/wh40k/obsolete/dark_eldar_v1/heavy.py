__author__ = 'Ivan Truskov'

from builder.core.unit import Unit
from builder.core.options import norm_counts

class Ravager(Unit):
    name = 'Ravager'
    base_points = 105
    def __init__(self):
        Unit.__init__(self)
        self.dcan = self.opt_count('Disintegrator cannon', 0, 3, 0)

        self.opt = self.opt_options_list('Options',[
            ['Shock prow', 5, 'sprow'],
            ['Torment grenade launchers', 5, 'tgl'],
            ['Enchanted aethersails', 5, 'eaes'],
            ['Retrofire jets', 5, 'rfjet'],
            ['Chain-snares', 5, 'chsn'],
            ['Grisly trophies', 5, 'gristr'],
            ['Envenomed blades', 5, 'envblld'],
            ['Night shields', 10, 'nsh'],
            ['Flickerfield', 10, 'flkf']
        ])
    def check_rules(self):
        self.gear = ['Dark Lance' for _ in range(3 - self.dcan.get())]
        Unit.check_rules(self)


class Talos(Unit):
    name = 'Talos Pain Engine'
    base_points = 100
    gear = ['Armoured caparace']
    def __init__(self):
        Unit.__init__(self)
        self.ccw = self.opt_one_of('Weapon',[
            ['Close combat weapon', 0, 'ccw'],
            ['Twin-linked liquefier gun', 5, 'tlliqgun'],
            ['Ichor injector', 5, 'iinj'],
            ['Chain flails', 10, 'chfl']
        ])
        self.rng = self.opt_one_of('',[
            ['Twin-linked splinter cannon', 0, 'tlsplcan'],
            ['Stinger pod', 5, 'stgpod'],
            ['Twin-linked haywire blaster', 5, 'tlhwrblast'],
            ['Twin-linked heat lance', 10, 'tlhlance']
        ])
        self.opt = self.opt_options_list('',[
            ['Close combat weapon', 15, 'ccw']
        ])


class Chronos(Unit):
    name = 'Chronos Parasite Engine'
    base_points = 80
    gear = ['Armoured caparace','Close combat weapon','Spirit syphon']
    def __init__(self):
        Unit.__init__(self)

        self.opt = self.opt_options_list('Options',[
            ['Spirit probe', 10, 'sprprobe'],
            ['Spirit vortex', 20, 'sprvort']
        ])


class Razorwing(Unit):
    name = 'Razorwing Jetfighter'
    base_points = 145
    def __init__(self):
        Unit.__init__(self)
        self.nose_mnt = self.opt_one_of('Weapon',[
            ['Twin-linked splinter rifles', 0, 'tlsplgun'],
            ['Splinter cannon', 10, 'splcan']
        ])
        self.wing_mnt = self.opt_one_of('Wing mount',[
            ['Dark lance', 0, 'dlance'],
            ['Disintegrator cannon', 0, 'discan']
        ])
        self.sm = self.opt_count('Shatterfield missile',0, 4, 5)
        self.nm = self.opt_count('Necrotoxin missile',0, 4, 5)
        self.opt = self.opt_options_list('Options',[
            ['Night shields', 10, 'nsh'],
            ['Flickerfield', 10, 'flkf']
        ])

    def check_rules(self):
        norm_counts(0, 4, [self.sm, self.nm], 0)
        self.gear = ['Monoscythe missile' for _ in range(4 - self.sm.get() - self.nm.get())]
        self.points.set(self.build_points())
        self.build_description(exclude=[self.wing_mnt.id])
        self.description.add(self.wing_mnt.get_selected(), 2)


class Voidraven(Unit):
    name = 'Voidraven bomber'
    base_points = 145
    gear = ['Void lance','Void lance','Void mine']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options',[
            ['Night shields', 10, 'nsh'],
            ['Flickerfield', 10, 'flkf']
        ])
        self.mis1 = self.opt_count('Monoscythe missile',0,4,10)
        self.mis2 = self.opt_count('Shatterfield missile',0,4,10)
        self.mis3 = self.opt_count('Necrotoxin missile',0,4,10)
        self.mis4 = self.opt_count('Implosion missile',0,4,30)
    def check_rules(self):
        norm_counts(0, 4, [self.mis1, self.mis2, self.mis3, self.mis4], 0)
        Unit.check_rules(self)
