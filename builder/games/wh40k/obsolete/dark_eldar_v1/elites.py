__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit
from builder.core.options import norm_counts
from troops import Raider,Venom

class Wracks(Unit):
    name = 'Wracks'
    gear = ['Gnarlskin', 'Poisoned weapon']

    class Acothyst(Unit):
        name = 'Acothyst'
        gear = ['Gnarlskin', 'Poisoned weapon','Poisoned weapon']
        base_points = 10 + 10
        def __init__(self):
            Unit.__init__(self)
            self.opt = self.opt_options_list('Options',[
                ['Stinger pistol',5,'stpist'],
                ['Venom blade',5,'vsw'],
                ['Mindphase gauntlet', 10, 'mphgnt' ],
                ['Hexrifle', 15, 'hexgun'],
                ['Scissorhand', 15, 'schnd'],
                ['Flesh gauntlet', 20, 'flshgnt' ],
                ['Agoniser', 20, 'agon' ],
                ['Electrocorrosive whip', 20, 'ewhip' ]
                    ], 1)

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Wrack',3,10,10)
        self.rng = self.opt_count('Liquefier gun', 0, 0, 10)
        self.transport = self.opt_optional_sub_unit('Transport', [Raider(), Venom()])
        self.leader = self.opt_optional_sub_unit(self.Acothyst.name, self.Acothyst())

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(3-ldr,10-ldr)
        self.rng.update_range(0, int(self.get_count() / 5))
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id, self.rng.id])
        self.description.add('Poisoned weapon', self.warriors.get() - self.rng.get())
        self.description.add(self.rng.get_selected())

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()


class Mandrakes(Unit):
    name = 'Mandrakes'
    gear = ['Evil-looking blade']

    class Nightfiend(Unit):
        name = 'Nightfiend'
        base_points = 15 + 10
        gear = ['Evil-looking blade']
        static = True

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Mandrake',3,10,15)
        self.leader = self.opt_optional_sub_unit(self.Nightfiend.name, [self.Nightfiend()])

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(3-ldr,10-ldr)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id])

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()


class HekatrixBloodbrides(Unit):
    name = 'Hekatrix Bloodbrides'
    gear = ['Wychsuit', 'Combat drugs','Plasma grenades']

    class Syren(Unit):
        base_points = 13 + 10
        name = 'Syren'
        gear = ['Wychsuit', 'Combat drugs','Plasma grenades']

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon',[
                ['Splinter pistol', 0, 'splpist' ],
                ['Blast pistol', 15, 'blpist' ]
            ])
            self.wep2 = self.opt_one_of('', [
                ['Close combat weapon', 0, 'ccw' ],
                ['Venom blade', 5, 'vblade' ],
                ['Power weapon', 10, 'pwep' ],
                ['Agoniser', 20, 'agon' ]
            ])
            self.opt = self.opt_options_list('',[
                ['Phantasm grenade launcher', 10, 'phgrl']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Hekatrix Bloodbride',3,10,13)

        ccwlist = [
            ['Razorflails', 10, 'rzf' ],
            ['Hydra gauntlets', 10, 'hydra' ],
            ['Shardnet and impaler', 10, 'net' ]
        ]
        self.wep = [self.opt_count(o[0], 0, 1, o[1]) for o in ccwlist]
        self.leader = self.opt_optional_sub_unit(self.Syren.name, self.Syren())
        self.spec = self.opt_options_list('Option',[
            ['Haywire grenades', 2, 'hwrgrnd' ]
        ])
        self.transport = self.opt_optional_sub_unit('Transport', [Raider(), Venom()], id='trans')

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(3-ldr, 10-ldr)
        norm_counts(0, int(self.get_count() / 3), self.wep)
        self.set_points(self.build_points(count=1, exclude=[self.spec.id]) + self.spec.points() * self.get_count())
        self.build_description(count=self.warriors.get(), exclude=[self.spec.id, self.warriors.id] + [o.id for o in self.wep])
        self.description.add(['Splinter pistol','Close combat weapon'], self.warriors.get() - reduce(lambda val, i: val + i.get(), self.wep, 0))
        self.description.add([o.get_selected() for o in self.wep])
        self.description.add(self.spec.get_selected(), self.get_count())

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()


class Incubi(Unit):
    name = 'Incubi'
    gear = ['Klaive', 'Incubus warsuit']
    class Klaivex(Unit):
        base_points = 22 + 15
        name = 'Klaivex'
        gear = ['Incubus warsuit']
        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_one_of('Weapon',[
                ['Klaive',0,'klv'],
                ['Demiklaives',20,'dklv']
                    ])
            self.opt = self.opt_options_list('Options',[
                ['Bloodstone',15,'bldstone'],
                ['Murderous Assault',10,'murdass'],
                ['Onslaught', 15, 'onsl' ]
                    ], )

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Incubus',3,10,22)
        self.leader = self.opt_optional_sub_unit(self.Klaivex.name, [self.Klaivex()])
        self.transport = self.opt_optional_sub_unit('Transport', [Raider(), Venom()], id='trans')

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(3-ldr,10-ldr)
        self.points.set(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id])

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()


class Grotesques(Unit):
    name = 'Grotesques'
    gear = ['Gnarlskin']

    class Aberration(Unit):
        base_points = 35 + 10
        name = 'Aberration'
        gear = ['Gnarlskin', 'Close combat weapon']
        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_options_list('Weapon',[
                ['Venom blade',5,'vsw'],
                ['Mindphase gauntlet', 10, 'mphgnt' ],
                ['Scissorhand', 15, 'schnd'],
                ['Flesh gauntlet', 20, 'flshgnt' ]
                ], limit=1)

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Grotesque',3,10,35)
        self.rng = self.opt_options_list('', [['Liquefier gun',10,'liqgun']])
        self.hulk = self.opt_options_list('',[
            ['Raise strength',5,'rak_up']
        ])
        self.leader = self.opt_optional_sub_unit(self.Aberration.name, [self.Aberration()])
        self.transport = self.opt_optional_sub_unit('Transport', [Raider()], id='trans')

    def set_hulk(self,val):
        self.hulk.set_visible(val)

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(3-ldr, 10-ldr)
        self.set_points(self.build_points(count=1, exclude=[self.rng.id, self.hulk.id]) + self.hulk.points() * self.get_count() + self.rng.points())
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id, self.rng.id, self.hulk.id])
        self.description.add('Close combat weapon', self.warriors.get() - (1 if self.rng.get('liqgun') else 0))
        self.description.add(self.rng.get_selected())
        self.description.add(self.hulk.get_selected(), self.get_count())

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()


class Harlequins(Unit):
    name = 'Harlequins'

    class Harlequin(ListSubUnit):
        name = 'Harlequin'
        base_points = 18
        max = 10
        gear = ['Flip belt','Holo-suit']
        def __init__(self):
            ListSubUnit.__init__(self)
            self.ccw = self.opt_one_of('Weapon',[
                ['Close combat weapon',0,'ccw'],
                ['Harlequin\'s kiss',4,'hkiss']
                    ])
            self.rng = self.opt_one_of('',[
                ['Shuriken pistol',0,'spist'],
                ['Fusion pistol',10,'fpist']
                ])

        def has_fusion(self):
            return self.count.get() if self.rng.get_cur() == 'fpist' else 0


    class DeathJester(Unit):
        name = 'Death Jester'
        base_points = 28
        gear = ['Flip belt','Holo-suit', 'Shrieker cannon']

        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_one_of('Weapon',[
                ['Close combat weapon',0,'ccw'],
                ['Harlequin\'s kiss',4,'hkiss']
            ])


    class Shadowseer(Unit):
        gear = ['Flip belt','Holo-suit', 'Hallucinogen grenades']
        name = 'Shadowseer'
        base_points = 48

        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_one_of('Weapon',[
                ['Close combat weapon',0,'ccw'],
                ['Harlequin\'s kiss',4,'hkiss']
            ])
            self.rng = self.opt_one_of('',[
                ['Shuriken pistol',0,'spist'],
                ['Fusion pistol',10,'fpist']
            ])

        def has_fusion(self):
            return 1 if self.rng.get_cur() == 'fpist' else 0


    class TroupeMaster(Unit):
        name = 'Troupe Master'
        base_points = 38
        gear = ['Flip belt','Holo-suit']
        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_one_of('Weapon',[
                ['Close combat weapon',0,'ccw'],
                ['Power weapon',0,'psw'],
                ['Harlequin\'s kiss',0,'hkiss']
            ])
            self.rng = self.opt_one_of('',[
                ['Shuriken pistol',0,'spist'],
                ['Fusion pistol',10,'fpist']
            ])

        def has_fusion(self):
            return 1 if self.rng.get_cur() == 'fpist' else 0

    def __init__(self):
        Unit.__init__(self)
        self.troupe = self.opt_units_list(self.Harlequin.name,self.Harlequin,5,10)
        self.dj = self.opt_optional_sub_unit(self.DeathJester.name, self.DeathJester())
        self.shs = self.opt_optional_sub_unit(self.Shadowseer.name, self.Shadowseer())
        self.tm = self.opt_optional_sub_unit(self.TroupeMaster.name, self.TroupeMaster())

    def get_count(self):
        return self.troupe.get_count() + self.dj.get_count() + self.shs.get_count() + self.tm.get_count()

    def check_rules(self):
        ldrs = self.dj.get_count() + self.shs.get_count() + self.tm.get_count()
        self.troupe.update_range(5 - ldrs, 10 - ldrs)
        def get_unit(u):
            u = u.get_unit()
            return [u.get_unit()] if u else []
        list = self.troupe.get_units() + get_unit(self.shs) + get_unit(self.tm)
        fusions = reduce(lambda val, u: val + u.has_fusion() if u else val, list, 0)
        if fusions > 2:
            self.error("You can't take more then 2 fusion pistols")
        self.set_points(self.build_points(count=1))
        self.build_description(count=1)


class Trueborns(Unit):
    name = 'Kabalite Trueborn'

    class Trueborn(ListSubUnit):
        name = 'Kabalite Trueborn'
        max = 10
        base_points = 12
        gear = ['Kabalite armour']
        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon',[
                ['Splinter rifle',0,'splgun'],
                ['Splinter pistol and close ombat weapon',0,'pair'],
                ['Shardcarbine',5,'shrdcarb'],
                ['Shredder',5,'shred'],
                ['Blaster',15,'blast'],
                ['Splinter cannon',10,'splcan'],
                ['Dark lance',25,'dlance']
                    ])
            self.hw1 = ['shred','blast']
            self.hw2 = ['splcan','dlance']

        def has_heavy1(self):
            return self.count.get() if self.wep.get_cur() in self.hw1 else 0

        def has_heavy2(self):
            return self.count.get() if self.wep.get_cur() in self.hw2 else 0

    class Dracon(Unit):
        name = 'Dracon'
        base_points = 17
        def __init__(self):
            Unit.__init__(self)
            self.armr = self.opt_one_of('Armour',[
                ['Kabalite armour',0,'kabarmr'],
                ['Ghostplate armour',10,'ghostarmr']
            ])
            self.bas = self.opt_options_list('Weapon',[
                ["Splinter rifle", 0, 'splgun'],
                ['Shardcarbine',5,'shrdcarb']
            ],1)
            self.bas.set('splgun', True)
            self.ccw = self.opt_one_of('',[
                ['Close combat weapon',0,'csw'],
                ['Venom blade',5,'vsw'],
                ['Power weapon', 15, 'pwep' ],
                ['Agoniser', 20, 'agon' ]
            ])
            self.rng = self.opt_one_of('',[
                ['Splinter pistol',0,'splpist'],
                ['Blast pistol',15,'blpist']
            ])
            self.opt = self.opt_options_list('Options',[
                ["Phantasm grenade launcher", 25, 'phgrlnch']
            ])
        def check_rules(self):
            self.ccw.set_visible(not (self.bas.get('splgun') or self.bas.get('shrdcarb')))
            self.rng.set_visible(not (self.bas.get('splgun') or self.bas.get('shrdcarb')))
            Unit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_units_list(self.Trueborn.name,self.Trueborn,3,10)
        self.leader = self.opt_optional_sub_unit(self.Dracon.name, self.Dracon())
        self.opt = self.opt_options_list('Options',[
            ['Plasma grenades', 1, 'plgrnd' ],
            ['Haywire grenades', 2, 'hwrgrnd' ]
        ])
        self.transport = self.opt_optional_sub_unit('Transport', [Raider(), Venom()], id='trans')

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(3-ldr,10-ldr)
        heavy1 = reduce(lambda val, u: val + u.has_heavy1(), self.warriors.get_units(), 0)
        heavy2 = reduce(lambda val, u: val + u.has_heavy2(), self.warriors.get_units(), 0)
        if heavy1 > 4:
            self.error("You cannot have more then 4 Shredder or Blaster")
        if heavy2 > 2:
            self.error("You cannot have more then 2 Splinter cannon or Dark lance")
        self.set_points(self.build_points(count=1, exclude=[self.opt.id]) + self.opt.points() * (ldr + self.warriors.get_count()))
        self.build_description(exclude=[self.opt.id])
        if self.opt.get('plgrnd'):
            self.description.add('Plasma grenades', self.warriors.get_count() + ldr)
        if self.opt.get('hwrgrnd'):
            self.description.add('Haywire grenades', self.warriors.get_count() + ldr)

    def get_count(self):
        return self.warriors.get_count() + self.leader.get_count()
