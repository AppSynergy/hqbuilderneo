# __author__ = 'Ivan Truskov'
from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.obsolete.tau2005.troops import *
from builder.games.wh40k.obsolete.tau2005.hq import *
from builder.games.wh40k.obsolete.tau2005.elites import *
from builder.games.wh40k.obsolete.tau2005.fast import *
from builder.games.wh40k.obsolete.tau2005.heavy import *
from builder.games.wh40k.obsolete.tau2005.armory import BWeaponSpec, BWargearSpec, BSupportSpec


class Tau2005(LegacyWh40k):
    army_id = '85ffef963f0a42468d9fa6df6945c3bd'
    army_name = 'Tau (2005)'
    obsolete = True

    def __init__(self):
        LegacyWh40k.__init__(
            self,
            hq=[CommanderShasO, CommanderShasEl, Farsight, Shadowsun, Ethereal, Aunva],
            elites=[CrisisTeam, StealthTeam],
            troops=[FireWarriors, KrootSquad],
            fast=[GunDrones, Pathfinders, PiranhaTeam, Vespids],
            heavy=[BroadsideTeam, SniperDroneTeam, Hammerhead, SkyRay]
        )
        def check_heavy_limit():
            count = len(self.heavy.get_units()) - self.heavy.count_unit(SniperDroneTeam) + int((2 + self.heavy.count_unit(SniperDroneTeam))/3)
            return self.heavy.min <= count <= self.heavy.max
        self.heavy.check_limits = check_heavy_limit
    def check_rules(self):
        if self.troops.count_unit(FireWarriors) < 1:
            self.error("Fire Warrior team is mandatory.")
        if self.hq.count_units([CommanderShasO, CommanderShasEl, Farsight, Shadowsun]) < 1:
            self.error("Tau Commander is mandatory")
        if (int((2 + self.heavy.count_unit(SniperDroneTeam))/3)) > 1:
            self.error("No more then 3 Sniper Drone teams can be taken")
        farsight = self.hq.count_unit(Farsight) > 0
        if farsight:
            if self.elites.count_unit(CrisisTeam) < 1:
                self.error("Crisis suit team is mandatory in army with Commander Farsight")
            if self.elites.count_unit(StealthTeam) > 1:
                self.error("Stealth suit team cannot be taken more then once in army including Commander Farsight")
            if self.fast.count_unit(Pathfinders) > 1:
                self.error("Pathfinder team cannot be taken more then once in army including Commander Farsight")
            if self.heavy.count_unit(BroadsideTeam) > 1:
                self.error("Broadside battlesuit team cannot be taken more then once in army including Commander Farsight")
            if self.heavy.count_unit(Hammerhead) > 1:
                self.error("Hammerhead gunship cannot be taken more then once in army including Commander Farsight")
            if self.heavy.count_unit(SkyRay) > 1:
                self.error("Sky Ray missile ship cannot be taken more then once in army including Commander Farsight")
            if self.fast.count_unit(PiranhaTeam) > 1:
                self.error("Pyrahna squadrons cannot be taken more then once in army including Commander Farsight")
            if self.troops.count_unit(KrootSquad) > 0:
                self.error("Kroot Carnivore squads cannot be taken more then once in army including Commander Farsight")
            if self.fast.count_unit(Vespids) > 0:
                self.error("Vespid squads cannot be taken more then once in army including Commander Farsight")
            if self.hq.count_units([Ethereal, Aunva, Shadowsun]):
                self.error("No Ethereals or ideological opponents can be in army containing Commander Farsight")
        if self.get_points() < 1500:
            if farsight:
                self.error("Commander Farsight can be HQ of Tau Empire army of 1500 points ane more")
            if self.hq.count_unit(Aunva) > 0:
                self.error("Aun\'Va can be HQ of Tau Empire army of 1500 points ane more")
            if self.hq.count_unit(Shadowsun) > 0:
                self.error("Commander Shadowsun can be HQ of Tau Empire army of 1500 points ane more")

        self.hq.set_active_types([Ethereal, Aunva, Shadowsun],not farsight)
        self.fast.set_active_types([Vespids],not farsight)
        self.troops.set_active_types([KrootSquad],not farsight)

        speclist = BWeaponSpec + BWargearSpec + BSupportSpec
        specusers = self.hq.get_units([CommanderShasO, CommanderShasEl, Farsight]) + self.elites.get_units([CrisisTeam])
        for sp in speclist:
            countspec = reduce(lambda val,tr: val + tr.has_special(sp[2]),specusers,0)
            if countspec > 1:
                self.error('No more then 1 ' + sp[0] + ' can be taken per army')
