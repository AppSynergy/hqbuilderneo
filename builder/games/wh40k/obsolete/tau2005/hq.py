__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, StaticUnit
from builder.games.wh40k.obsolete.tau2005.armory import Infantry, Drones, Crisis
from builder.games.wh40k.obsolete.tau2005.troops import FireWarriors
from builder.core.options import norm_counts


class BodyguardShasVre(Crisis):
    name = 'Bodyguard Shas\'vre'
    base_points = 35

    def __init__(self, can_select_ass=True, max_count=1):
        Crisis.__init__(self, alone=False, can_select_ass=can_select_ass, max_count=max_count)


class Commander(Crisis):
    def __init__(self):
        Crisis.__init__(self)
        self.guards = [self.opt_optional_sub_unit('', BodyguardShasVre()) for _ in range(2)]

    def get_guards(self):
        res = []
        for guard in self.guards:
            if guard.get_unit() and guard.get_unit().get_unit():
                res.append(guard.get_unit().get_unit())
        return res

    def check_rules(self):
        team = self.get_guards()
        if len(team):
            has_ass = self.has_advanced_ss()
            if has_ass and not all(map(lambda unit: unit.has_advanced_ss(), team)):
                self.error("Advanced stabilisation system must be taken on either all of the members of the unit or none of them.")
            if self.count_es():
                self.error("Ejection systems may be taken for single-model units only (discounting drones).")
            if self.count_vrt():
                self.error("Vectored retro-thrusters may be taken for single-model units only (discounting drones).")
        Crisis.check_rules(self)

    def has_special(self, spid):
        return Crisis.has_special(self, spid) + reduce(lambda v, unit: v + unit.has_special(spid), self.get_guards(), 0)


class CommanderShasEl(Commander):
    name = 'Commander Shas\'el'
    base_points = 50


class CommanderShasO(Commander):
    name = 'Commander Shas\'o'
    base_points = 75


class Farsight(Unit):
    name = 'Commander Farsight'
    base_points = 170
    gear = ['XV8 Crisis battlesuit', 'Hard-wired target lock', 'Shield generator', 'Bonding knife', 'Plasma rifle',
            'Dawn blade']

    class FarsightBodyguard(BodyguardShasVre):
        def __init__(self):
            BodyguardShasVre.__init__(self, can_select_ass=False, max_count=7)

    def __init__(self):
        Unit.__init__(self)
        self.guards_enabled = self.opt_options_list('', [[self.FarsightBodyguard.name,
                                                          self.FarsightBodyguard.base_points,
                                                          'on']])
        self.guards = self.opt_units_list(self.FarsightBodyguard.name, self.FarsightBodyguard, 1, 7)

    def check_rules(self):
        self.guards.set_active(self.guards_enabled.get('on'))
        self.guards.update_range()
        self.points.set(self.build_points(exclude=[self.guards_enabled.id], count=1))
        self.build_description(exclude=[self.guards_enabled.id], count=1)

    def get_guards(self):
        if not self.guards_enabled.get('on'):
            return []
        return self.guards.get_units()

    def get_unique(self):
        return self.name

    def has_special(self, spid):
        return reduce(lambda v, unit: v + unit.has_special(spid), self.get_guards(), 0)

    def get_count(self):
        return 1 + self.guards.get_count()


class Shadowsun(StaticUnit):
    name = 'Commander Shadowsun'
    base_points = 175
    gear = ['XV22 Battlesuit', 'Multi tracker', 'Advanced target lock', 'Drone controller', 'Command link drone',
            'Shield drone', 'Shield drone', 'Fusion blaster', 'Fusion blaster', 'Bonding knife']


class Ethereal(Unit):
    name = 'Ethereal'
    base_points = 50

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon',[
            ['Symbols of office',0,'ccw'],
            ['Honour blade',10,'hsw']
        ])
        self.opt = self.opt_options_list('Options', Infantry)
        self.drones = [self.opt_count(tp[0], 0, 2, tp[1]) for tp in Drones]
        self.guards = self.opt_optional_sub_unit('', [FireWarriors('Honour Guard')])

    def check_rules(self):
        has_dc = self.opt.get('dc')
        dr_cnt = 0
        for ctr in self.drones:
            ctr.set_active(has_dc)
            dr_cnt += ctr.get() if has_dc else 0
        if has_dc:
            norm_counts(0, 2, self.drones)
            if dr_cnt != 2:
                self.error('2 drones must be taken along with controller')
        if self.guards.get_unit():
            self.guards.get_unit().get_unit().enable_transport(self.get_count() <= 12)
            self.guards.get_unit().get_unit().check_rules()
        self.points.set(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return 1 + (reduce(lambda val, n: val + n.get(), self.drones, 0) if self.opt.get('dc') else 0) \
               + self.guards.get_count()


class Aunva(Unit):
    name = 'Aun\'va'
    base_points = 205
    gear = ['Paragon of Duality']
    static = True

    class HonourGuard(StaticUnit):
        name = 'Honour Guard'
        gear = ['Honour blade']

    def __init__(self):
        Unit.__init__(self)
        self.guard = self.HonourGuard()

    def check_rules(self):
        self.points.set(self.build_points())
        self.build_description()
        self.description.add(self.guard.get_description())
        self.description.add(self.guard.get_description())

    def get_unique(self):
        return self.name


