__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit, ListUnit
from builder.games.wh40k.obsolete.tau2005.troops import Devilfish
from builder.games.wh40k.obsolete.tau2005.armory import Vehicle, Infantry, Drones
from builder.core.options import norm_counts


class GunDrones(Unit):
    name = 'Gun Drone squadron'
    gear = ['Twin-linked pulse carbine']

    def __init__(self):
        Unit.__init__(self)
        self.team = self.opt_count('Gun Drone',4,8,12)

    def get_count(self):
        return self.team.get()

    def check_rules(self):
        self.set_points(self.team.points())
        self.build_description(exclude = [self.team.id])


class Pathfinders(Unit):
    name = 'Pathfinder team'

    class Shasui(Unit):
        name = 'Shas\'ui'
        base_points = 20
        gear = ['Pulse carbine with Markerlight target designator']

        def __init__(self):
            Unit.__init__(self)
            self.opt = self.opt_options_list('Options',Infantry)
            self.drones  = [self.opt_count(tp[0],0,2,tp[1]) for tp in Drones]

        def check_rules(self):
            has_dc = self.opt.get('dc')
            dr_cnt = 0
            for ctr in self.drones:
                ctr.set_active(has_dc)
                dr_cnt += ctr.get() if has_dc else 0
            if has_dc:
                norm_counts(0,2,self.drones)
                if dr_cnt != 2:
                    self.error('2 drones must be taken along with controller')
            Unit.check_rules(self)

        def get_count(self):
            return 1 + (reduce(lambda val, n: val + n.get(), self.drones, 0) if self.opt.get('dc') else 0)

    def __init__(self):
        Unit.__init__(self)
        self.team = self.opt_count('Shas\'la',4,8,12)
        self.rifle = self.opt_count('Rail rifle and target lock',0,3,10)
        self.gren = self.opt_options_list('Grenades',[
            ['Photon grenades',1,'ph'],
            ['EMP grenades',3,'rmp']
        ])
        self.leader = self.opt_optional_sub_unit('Team leader',[self.Shasui()])
        self.transport = self.opt_sub_unit(Devilfish())

    def check_rules(self):
        leader = 1 if self.leader.get_count() else 0
        self.team.update_range(4 - leader,8 - leader)
        self.set_points(self.leader.points()+self.transport.points() + self.team.get()*(12 + self.gren.points()) + self.leader.get_count()*self.gren.points() + self.rifle.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        if self.team.get() - self.rifle.get() > 0:
            self.description.add('Pulse carbine with Markerlight target designator',self.team.get() - self.rifle.get())
        if self.rifle.get() > 0:
            self.description.add('Rail rifle and target lock',self.rifle.get())
        if self.gren.get('ph'):
            self.description.add('Photon grenades',self.leader.get_count() + self.team.get())
        if self.gren.get('emp'):
            self.description.add('EMP grenades',self.leader.get_count() + self.team.get())
        self.description.add(self.leader.get_selected())

        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.team.get() + self.leader.get_count()


class PiranhaTeam(ListUnit):
    name = 'Piranha light skimmer team'

    class Piranha(ListSubUnit):
        min = 1
        max = 5
        name = 'Pirahna'
        base_points = 60
        gear = ['Two Gun Drones']
        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon',[
                ['Hull-mounted burst cannon',0,'bcan'],
                ['Fusion blaster',5,'fblast']
            ])
            self.opt = self.opt_options_list('Options',Vehicle)
            self.opt.set_active_options(['mtr','tl'],False)

    unit_class = Piranha


class Vespids(Unit):
    name = 'Vespid Stingwings'
    gear = ['Neutron blaster']

    class Leader(Unit):
        name = 'Strain leader'
        base_points = 22
        gear = ['Neutron blaster']
        static = True

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.opt_sub_unit(self.Leader())
        self.team = self.opt_count('Stingwing',3,10,16)

    def check_rules(self):
        self.points.set(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return self.team.get() + 1
