__author__ = 'Denis Romanov'

tl_weapon = [
    ['Twin-linked flamer', 10, 'tlflame'],
    ['Twin-linked burst cannon', 15, 'tlbc'],
    ['Twin-linked fusion blaster', 20, 'tlfblast'],
    ['Twin-linked missile pod', 20, 'tlmpod'],
    ['Twin-linked plasma rifle', 20, 'tlpgun'],
]
tl_ids = map(lambda u: u[2], tl_weapon)

unique_weapon = [
    ['Airbursting fragmentation projector', 15, 'afp'],
    ['Cyclic ion blaster', 15, 'cib']
]

base_weapon = [
    ['Flamer', 5, 'flame'],
    ['Burst cannon', 10, 'bc'],
    ['Fusion blaster', 15, 'fblast'],
    ['Missile pod', 15, 'mpod'],
    ['Plasma rifle', 15, 'pgun'],
]

weapon = base_weapon + tl_weapon + unique_weapon

support = [
    ['Advanced targeting system', 3, 'ass'],
    ['Counter fire system', 5, 'cfs'],
    ['Early warning override', 5, 'ewo'],
    ['Positional relay', 5, 'pr'],
    ['Target lock', 5, 'tl'],
    ['Drone controller', 8, 'dc'],
    ['Velocity tracker', 20, 'mtr'],
]

riptide_support = support + [
    ['Stimulant injector', 35, 'sinj'],
]

broadside_support = support + [
    ['Stimulant injector', 15, 'sinj'],
    ['Shield generator', 25, 'shgen'],
]

crisis_support = broadside_support + [
    ['Vectored retro-thrusters', 5, 'vrt'],
]

signature = [
    ['Neuroweb System Jammer', 2, 'nws'],
    ['Onager gauntlet', 5, 'ong'],
    ['Failsafe detonator', 10, 'fs'],
    ['Repulsor impact field', 10, 'rif'],
    ['Command and control node', 15, 'ccn'],
    ['Puretide Engram Neurochip', 15, 'ern'],
    ['Multi-spectrum Sensor Suit', 20, 'esss'],
    ['XV8-02 Crisis \'Iridium\' Battlesuit', 25, 'iridium'],
]

drones = [
    ['Gun drone', 12],
    ['Marker drone', 12],
    ['Shield drone', 12]
]

broadside_drones = drones + [
    ['Missile drone', 12]
]

vehicle = [
    ['Blacksun filter', 1, 'bs'],
    ['Decoy launchers', 3, 'dl'],
    ['Automated repair system', 5, 'ars'],
    ['Sensor spines', 5, 'ssp'],
    ['Flechette discharger', 10, 'fd'],
    ['Point defence targeting relay', 10, 'tarr'],
    ['Advanced targeting system', 10, 'tl'],
    ['Disruption pod', 15, 'dp'],
]

enclaves_signature = [
    ['The Mirrorcodex', 50, 'codex'],
    ['Seismic Fibrillator Node', 45, 'seis'],
    ['Warscaper Drone', 35, 'wsd'],
    ['Earth Caste Pilot Array', 30, 'earth'],
    ['The Talisman of Arthas Moloch', 25, 'tal'],
]
commander_signature = enclaves_signature + [
    ['Fusion Blades', 30, 'fblades'],
]
