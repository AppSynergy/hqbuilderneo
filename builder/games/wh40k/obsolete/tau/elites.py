__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit
from builder.core.options import norm_counts
from builder.games.wh40k.obsolete.tau.armory import *


class CrisisTeam_v2(Unit):
    name = 'XV8 Crisis team'

    class CrisisSuit(ListSubUnit):
        base_points = 22
        name = 'Shas\'ui'
        gear = ['Crisis battlesuit', 'Multi-tracker', 'Blacksun filter']

        def __init__(self, enclaves=False):
            ListSubUnit.__init__(self, max_models=3)
            self.base_wep = [self.opt_count(w[0], 0, 3, w[1]) for w in base_weapon]
            self.tl_wep = self.opt_options_list('', tl_weapon)
            self.unique_wep = self.opt_options_list('', unique_weapon)
            self.support = self.opt_options_list('Support system', crisis_support)
            self.drones = [self.opt_count(v[0], 0, 0, v[1]) for v in drones]
            self.leader = self.opt_options_list('', [
                ['Shas\'vre', 10, 'up']
            ])
            self.signature = self.opt_options_list('Signature system', signature)
            self.enclaves = enclaves
            if self.enclaves:
                self.enclaves_signature = self.opt_options_list('Signature system of the Farsight Enclaves',
                                                                enclaves_signature)

        def check_rules(self):
            self.leader.set_active_options(['up'], self.get_count() <= 1)
            norm_counts(0, 2, self.drones)

            if self.leader.get('up'):
                if self.enclaves:
                    self.enclaves_signature.set_visible(True)
                    self.signature.set_visible(False)
                else:
                    self.signature.set_visible(True)
            else:
                if self.enclaves:
                    self.enclaves_signature.set_visible(False)
                self.signature.set_visible(False)

            self.name = 'Shas\'ui' if not self.leader.get('up') else 'Shas\'vre'
            nodes = len(self.unique_wep.get_all()) + len(self.tl_wep.get_all()) * 2 + len(self.support.get_all()) + \
                sum((c.get() for c in self.base_wep))
            if nodes > 3:
                self.error(self.name + ' may take up to 3 items from Ranged weapon and Support systems lists. '
                                       '(taken {0})'.format(nodes))
            ListSubUnit.check_rules(self)

        def count_leader(self):
            return self.get_count() if self.leader.get('up') else 0

        def get_unique_gear(self):
            return (self.unique_wep.get_selected() +
                    (self.signature.get_selected() if self.signature.is_used() else []) +
                    (self.enclaves_signature.get_selected() if self.enclaves and self.enclaves_signature.is_used()
                     else [])) * self.get_count()

    class EnclaveCrisisSuit(CrisisSuit):
        def __init__(self):
            CrisisTeam_v2.CrisisSuit.__init__(self, enclaves=True)

    def __init__(self, enclaves=False):
        Unit.__init__(self)
        self.enclaves = enclaves
        if self.enclaves:
            self.team = self.opt_units_list('Shas\'ui', self.EnclaveCrisisSuit, 1, 3)
        else:
            self.team = self.opt_units_list('Shas\'ui', self.CrisisSuit, 1, 3)
            self.ritual = self.opt_options_list('Options', [
                ['Bonding knife ritual', 1, 'up']
            ])

    def check_rules(self):
        self.team.update_range()
        leader = sum((m.count_leader() for m in self.team.get_units()))
        if leader > 1:
            self.error('Only one Shas\'ui can be upgraded to Shas\'vre (upgraded: {0}).'.format(leader))
        if self.enclaves:
            self.set_points(self.build_points(count=1) + 1 * self.get_count())
        else:
            self.set_points(self.build_points(count=1, exclude=[self.ritual.id]) +
                            self.ritual.points() * self.get_count())
        self.build_description(count=1)
        if self.enclaves:
            self.description.add('Bonding knife ritual')

    def get_count(self):
        return self.team.get_count()

    def get_unique_gear(self):
        return sum((u.get_unique_gear() for u in self.team.get_units()), [])


class EnclavesCrisisTeam(CrisisTeam_v2):
    def __init__(self):
        CrisisTeam_v2.__init__(self, enclaves=True)


class StealthTeam(Unit):
    name = 'Stealth Team'

    class Suit(ListSubUnit):
        name = 'Shas\'ui'
        base_points = 30
        gear = ['XV25 Stealth battlesuit', 'Multi-tracker', 'Blacksun filter']

        def __init__(self):
            ListSubUnit.__init__(self, max_models=6)
            self.weapon = self.opt_one_of('Weapon', [
                ['Burst cannon', 0, 'bcan'],
                ['Fusion blaster', 5, 'fb']
            ])
            self.sup = self.opt_options_list('Support systems', crisis_support, limit=1)
            self.leader = self.opt_options_list('', [
                ['Shas\'vre', 10, 'up']
            ])
            self.opt = self.opt_options_list('Options', [
                ['Homing beacon', 10],
                ['Market light and target lock', 5],
            ])
            self.drones = [self.opt_count(v[0], 0, 0, v[1]) for v in drones]

        def count_leader(self):
            return self.get_count() if self.leader.get('up') else 0

        def count_blaster(self):
            return self.get_count() if self.weapon.get_cur() == 'fb' else 0

        def check_rules(self):
            self.name = 'Shas\'ui' if not self.leader.get('up') else 'Shas\'vre'
            self.opt.set_visible(self.leader.get('up'))
            for o in self.drones:
                o.set_active(self.leader.get('up'))
            norm_counts(0, 2, self.drones)
            ListSubUnit.check_rules(self)

    def __init__(self, enclaves=False):
        Unit.__init__(self)
        self.team = self.opt_units_list(self.Suit.name, self.Suit, 3, 6)
        self.enclaves = enclaves
        if not self.enclaves:
            self.ritual = self.opt_options_list('Options', [
                ['Bonding knife ritual', 1, 'up']
            ])

    def check_rules(self):
        self.team.update_range()

        blaster = sum((m.count_blaster() for m in self.team.get_units()))
        blaster_lim = int(self.get_count() / 3)
        if blaster > blaster_lim:
            self.error('Only one blaster may be taken per 3 models in unit.')

        leader = sum((m.count_leader() for m in self.team.get_units()))
        if leader > 1:
            self.error('Only one Shas\'ui can be upgraded to Shas\'vre (upgraded: {0}).'.format(leader))

        if self.enclaves:
            self.set_points(self.build_points(count=1) + 1 * self.get_count())
        else:
            self.set_points(self.build_points(count=1, exclude=[self.ritual.id]) +
                            self.ritual.points() * self.get_count())
        self.build_description(count=1)
        if self.enclaves:
            self.description.add('Bonding knife ritual')

    def get_count(self):
        return self.team.get_count()


class EnclavesStealthTeam(StealthTeam):
    def __init__(self):
        StealthTeam.__init__(self, enclaves=True)


class Riptide(Unit):
    name = "XV104 Riptide"
    base_points = 180
    gear = ['Riptide battlesuit', 'Multi-tracker', 'Blacksun filter', 'Riptide shield generator']

    def __init__(self, enclaves=False):
        Unit.__init__(self)
        self.opt_one_of('Weapon', [
            ['Heavy burst cannon', 0],
            ['Ion accelerator', 5],
        ])
        self.opt_one_of('', [
            ['Twin-linked smart missile system', 0],
            ['Twin-linked plasma rifle', 0],
            ['Twin-linked fusion blaster', 0],
        ])
        self.opt_options_list('Support systems', riptide_support, limit=2)
        self.opt_count('Shielded missile drone', 0, 2, 25)
        self.enclaves = enclaves
        if self.enclaves:
            self.enclaves_signature = self.opt_options_list('Signature system of the Farsight Enclaves',
                                                            enclaves_signature)

    def get_unique_gear(self):
        if self.enclaves:
            return self.enclaves_signature.get_selected()


class EnclavesRiptide(Riptide):
    def __init__(self):
        Riptide.__init__(self, enclaves=True)
