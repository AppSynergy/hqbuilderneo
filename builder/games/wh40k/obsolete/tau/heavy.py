__author__ = 'Denis Romanov'
from builder.core.unit import Unit, ListSubUnit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.tau.armory import *
from builder.core.options import norm_counts


class BroadsideTeam(Unit):
    name = 'Broadside Battlesuit team'

    class BroadsideSuit(ListSubUnit):
        name = 'Shas\'ui'
        base_points = 65
        gear = ['Broadside battlesuit', 'Multi-tracker', 'Blacksun filter']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.opt_one_of('Weapon', [
                ['Twin-linked heavy rail rifle', 0],
                ['Twin-linked high-yield missile pod', 0]
            ])
            self.opt_one_of('', [
                ['Twin-linked smart missile system', 0],
                ['Twin-linked plasma rifles', 0]
            ])
            self.opt_options_list('', [['Seeker missile', 8]])
            self.opt_options_list('Support system', broadside_support, limit=1)
            self.drones = [self.opt_count(v[0], 0, 0, v[1]) for v in broadside_drones]
            self.leader = self.opt_options_list('', [
                ['Shas\'vre', 10, 'up']
            ])

        def check_rules(self):
            self.leader.set_active_options(['up'], self.get_count() <= 1)
            norm_counts(0, 2, self.drones)
            self.name = 'Shas\'ui' if not self.leader.get('up') else 'Shas\'vre'
            ListSubUnit.check_rules(self)

        def count_leader(self):
            return self.get_count() if self.leader.get('up') else 0

    def __init__(self, enclaves=False):
        Unit.__init__(self)
        self.team = self.opt_units_list('Shas\'ui', self.BroadsideSuit, 1, 3)
        self.enclaves = enclaves
        if not self.enclaves:
            self.ritual = self.opt_options_list('Options', [
                ['Bonding knife ritual', 1, 'up']
            ])

    def check_rules(self):
        self.team.update_range()
        leader = sum((m.count_leader() for m in self.team.get_units()))
        if leader > 1:
            self.error('Only one Shas\'ui can be upgraded to Shas\'vre (upgraded: {0}).'.format(leader))
        if self.enclaves:
            self.set_points(self.build_points(count=1) + 1 * self.get_count())
        else:
            self.set_points(self.build_points(count=1, exclude=[self.ritual.id]) +
                            self.ritual.points() * self.get_count())
        self.build_description(count=1)
        if self.enclaves:
            self.description.add('Bonding knife ritual')

    def get_count(self):
        return self.team.get_count()


class EnclavesBroadsideTeam(BroadsideTeam):
    def __init__(self):
        BroadsideTeam.__init__(self, enclaves=True)


class SniperDroneTeam(Unit):
    name = 'Sniper drone team'

    def __init__(self):
        Unit.__init__(self)
        self.sp = self.opt_count('Firesight Marksman', 1, 3, 13)
        self.dr = self.opt_count('Sniper Drone', 3, 9, 15)

    def check_rules(self):
        self.set_points(self.build_points(count=1))
        self.build_description(exclude=[self.sp.id, self.dr.id], count=1)
        self.description.add(ModelDescriptor(
            name='Firesight Marksman',
            points=13,
            gear=['Combat armour', 'Pulse pistol', 'Drone controller', 'Markerlight'],
            count=self.sp.get()
        ).build())
        self.description.add(ModelDescriptor(
            name='Sniper Drone',
            points=15,
            gear=['Long pulse rifle'],
            count=self.dr.get()
        ).build())

    def get_count(self):
        return self.dr.get() + self.sp.get()


class Hammerhead(Unit):
    name = 'Hammerhead'
    base_points = 125

    def __init__(self):
        Unit.__init__(self)
        self.commander = self.opt_options_list('Commander', [
            ['Longstrike', 45, 'ls']
        ])
        self.wep1 = self.opt_one_of('Weapon', [
            ['Railgun with solid shot', 0, 'rail'],
            ['Ion cannon', 0]
        ])
        self.up = self.opt_options_list('', [
            ['Submunition rounds', 5]
        ])
        self.wep2 = self.opt_one_of('', [
            ['Two Gun Drones', 0, 'gd'],
            ['Twin-linked Burst cannon', 0, 'bcan'],
            ['Twin-linked Smart missile system', 0, 'sms']
        ])
        self.mis = self.opt_count('Seeker missile', 0, 2, 8)
        self.opt = self.opt_options_list('Options', vehicle)

    def check_rules(self):
        self.up.set_visible(self.wep1.get_cur() == 'rail')
        self.points.set(self.build_points())
        self.build_description(exclude=([self.wep2.id] if self.wep2.get_cur() == 'gd' else []))
        if self.wep2.get_cur() == 'gd':
            self.description.add('Gun Drone', 2)

    def get_unique(self):
        if self.commander.get('ls'):
            return 'Commander Longstrike'


class SkyRay(Unit):
    name = 'Sky Ray'
    base_points = 115
    gear = ['Seeker missile' for _ in range(6)] + ['Networked markerlight' for _ in range(2)] + ['Velocity tracker']

    def __init__(self):
        Unit.__init__(self)
        self.wep2 = self.opt_one_of('Weapon', [
            ['Two Gun Drones', 0, 'gd'],
            ['Twin-linked Burst cannon', 0, 'bcan'],
            ['Twin-linked Smart missile system', 0, 'sms']
        ])
        self.opt = self.opt_options_list('Options', vehicle)

    def check_rules(self):
        self.points.set(self.build_points())
        self.build_description(exclude=([self.wep2.id] if self.wep2.get_cur() == 'gd' else []))
        if self.wep2.get_cur() == 'gd':
            self.description.add('Gun Drone', 2)
