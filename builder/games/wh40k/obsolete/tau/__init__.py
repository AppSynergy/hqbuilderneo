__author__ = 'Denis Romanov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.old_roster import Ally
from builder.games.wh40k.obsolete.tau.depricated import *
from builder.games.wh40k.obsolete.tau.troops import *
from builder.games.wh40k.obsolete.tau.hq import *
from builder.games.wh40k.obsolete.tau.elites import *
from builder.games.wh40k.obsolete.tau.fast import *
from builder.games.wh40k.obsolete.tau.heavy import *
from builder.games.wh40k.obsolete.tau.armory import *


class Tau(LegacyWh40k):
    army_id = '459a88b4390b4652abc91a76315ebcb8'
    army_name = 'Tau'
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(
            self,
            hq=[
                dict(unit=Farsight_v2, type_id='farsight_v2'),
                dict(unit=Shadowsun_v2, type_id='shadowsun_v2'),
                Aunva, Aunshi, Darkstider, Ethereal, Cadre,
                dict(unit=Commander_v2, type_id='commander_v2'),
                dict(unit=Bodyguards_v2, active=False),
                dict(unit=FarsightBodyguards, active=False),
                dict(unit=Farsight, deprecated=True),
                dict(unit=Shadowsun, deprecated=True),
                dict(unit=Commander, deprecated=True),
            ],
            elites=[
                dict(unit=CrisisTeam_v2, type_id='crisis_v2'),
                StealthTeam,
                Riptide,
                dict(unit=CrisisTeam, deprecated=True),
            ],
            troops=[FireWarriors, KrootSquad],
            fast=[Pathfinders, Vespids, GunDrones, PiranhaTeam, SunShark, RazorShark],
            heavy=[BroadsideTeam, Hammerhead, SkyRay, SniperDroneTeam],
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.TAU)
        )

        def check_hq_limit():
            count = len(self.hq.get_units()) - self.hq.count_units([Bodyguards_v2, FarsightBodyguards])
            return self.hq.min <= count <= self.hq.max
        self.hq.check_limits = check_hq_limit

    def check_rules(self):
        fs = self.hq.count_unit(Farsight_v2)
        self.hq.set_active_types([FarsightBodyguards], fs > 0)
        if fs == 0 and self.hq.count_unit(FarsightBodyguards) > 0:
            self.error("You can't have Farsight\'s bodyguard team without Commander Farsight.")

        com = self.hq.count_units([Commander_v2, Farsight_v2, Shadowsun_v2])
        self.hq.set_active_types([Bodyguards_v2], com > 0)
        if com == 0 and self.hq.count_units([FarsightBodyguards, Bodyguards_v2]) > 0:
            self.error("You can't have more bodyguard teams then commanders (include Commander Farsight and "
                       "Commander Shadowsun)")


class FarsightEnclaves(LegacyWh40k):
    army_id = '96c24b32166f4a99b39f560270239d32'
    army_name = 'Tau: Farsight Enclaves'
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(
            self,
            hq=[
                Farsight_v2, Aunshi, Darkstider, Ethereal, Cadre, EnclavesCommander,
                dict(unit=EnclavesBodyguards, active=False),
                dict(unit=EnclavesFarsightBodyguards, active=False),
                dict(unit=FarsightCommanders, active=False),
            ],
            elites=[EnclavesStealthTeam, EnclavesRiptide],
            troops=[EnclavesCrisisTeam, EnclavesFireWarriors, KrootSquad],
            fast=[EnclavesPathfinders, Vespids, GunDrones, PiranhaTeam, SunShark, RazorShark],
            heavy=[EnclavesBroadsideTeam, Hammerhead, SkyRay, SniperDroneTeam],
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.TAU)

        )

        def check_hq_limit():
            count = len(self.hq.get_units()) - self.hq.count_units([EnclavesBodyguards, EnclavesFarsightBodyguards])
            return self.hq.min <= count <= self.hq.max
        self.hq.check_limits = check_hq_limit

    def check_rules(self):
        fs = self.hq.count_unit(Farsight_v2)
        self.hq.set_active_types([EnclavesFarsightBodyguards, FarsightCommanders], fs > 0)
        if fs == 0 and self.hq.count_unit(EnclavesFarsightBodyguards) > 0:
            self.error("You can't have Farsight\'s bodyguard team without Commander Farsight.")
        if fs == 0 and self.hq.count_unit(FarsightCommanders) > 0:
            self.error("You can't have Farsight\'s commander team without Commander Farsight.")
        if self.hq.count_unit(EnclavesFarsightBodyguards) > 0 and self.hq.count_unit(FarsightCommanders) > 0:
            self.error("You can't have Farsight\'s commander team and Farsight\'s bodyguard team together.")

        com = self.hq.count_units([EnclavesCommander, Farsight_v2])
        self.hq.set_active_types([EnclavesBodyguards], com > 0)
        if com == 0 and self.hq.count_units([EnclavesFarsightBodyguards, EnclavesBodyguards, FarsightCommanders]) > 0:
            self.error("You can't have more bodyguard or command teams then commanders (include Commander Farsight)")

        crisis = self.troops.get_units(EnclavesCrisisTeam)
        if len(crisis) == 0 or not any(map(lambda u: u.get_count() >= 3, crisis)):
            self.error("You must include at least one XV8 Crisis Team consists of three models.")
