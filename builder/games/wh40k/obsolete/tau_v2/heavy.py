__author__ = 'maria'

from builder.core2 import *
from armory import Vehicle, SupportSystem, Drones, Ritual


class BroadsideTeam(Unit):
    type_name = 'Broadside Battlesuit Team'
    type_id = 'broadsidebattlesuitteam_v1'

    def __init__(self, parent):
        super(BroadsideTeam, self).__init__(parent=parent, points=0, gear=[])
        self.team = UnitList(self, self.BroadsideSuit, min_limit=1, max_limit=3)
        self.opt = Ritual(self)

    def get_count(self):
        return self.team.count

    def check_rules(self):
        shasvre = sum(u.count_shasvre() for u in self.team.units)
        if shasvre > 1:
            self.error('Only one Shas\'ui can be upgraded to Shas\'vre (upgraded: {0}).'.format(shasvre))

    class BroadsideSuit(ListSubUnit):
        type_name = 'Shas\'ui'
        type_id = 'shasui_v1'

        def __init__(self, parent):
            super(BroadsideTeam.BroadsideSuit, self).__init__(parent=parent, points=65, name=self.type_name, gear=[
                Gear('Broadside battlesuit'),
                Gear('Multi-tracker'),
                Gear('Blacksun filter'),
            ])
            self.weapon1 = self.Weapon1(self)
            self.weapon2 = self.Weapon2(self)
            self.weapon3 = self.Weapon3(self)
            self.shasvre = self.ShasVre(self)
            self.support = SupportSystem(self, broadside=True, slots=1)
            self.drones = Drones(self, broadside=True)

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(BroadsideTeam.BroadsideSuit.Weapon1, self).__init__(parent=parent, name='Weapon')

                self.twinlinkedheavyrailrifle = self.variant('Twin-linked heavy rail rifle', 0)
                self.twinlinkedhighyieldmissilepod = self.variant('Twin-linked high-yield missile pod', 0)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(BroadsideTeam.BroadsideSuit.Weapon2, self).__init__(parent=parent, name='')

                self.twinlinkedsmartmissilesystem = self.variant('Twin-linked smart missile system', 0)
                self.twinlinkedplasmarifles = self.variant('Twin-linked plasma rifles', 5)

        class Weapon3(OptionsList):
            def __init__(self, parent):
                super(BroadsideTeam.BroadsideSuit.Weapon3, self).__init__(parent=parent, name='', limit=None)

                self.seekermissile = self.variant('Seeker missile', 8)

        class ShasVre(OptionsList):
            def __init__(self, parent):
                super(BroadsideTeam.BroadsideSuit.ShasVre, self).__init__(parent=parent, name='Leader', limit=None)

                self.shasvre = self.variant('Shas\'vre', 10, gear=[])

        def check_rules(self):
            self.drones.check_rules()
            self.shasvre.shasvre.active = self.shasvre.shasvre.used = self.get_count() == 1
            if self.get_count() > 1:
                self.shasvre.shasvre.value = False

        def build_description(self):
            desc = super(BroadsideTeam.BroadsideSuit, self).build_description()
            if self.shasvre.shasvre.value:
                desc.name = self.shasvre.shasvre.title
            return desc

        @ListSubUnit.count_gear
        def count_shasvre(self):
            return self.shasvre.shasvre.value


class SniperDroneTeam(Unit):
    type_name = 'Sniper Drone Team'
    type_id = 'sniperdroneteam_v1'

    marksman_points = 13
    drone_points = 15

    def __init__(self, parent):
        super(SniperDroneTeam, self).__init__(parent=parent, points=0, gear=[])

        self.firesightmarksman = Count(
            self, 'Firesight Marksman', min_limit=1, max_limit=3, points=self.marksman_points,
            gear=UnitDescription(
                name='Firesight Marksman',
                points=self.marksman_points,
                options=[Gear('Combat armour'), Gear('Pulse pistol'), Gear('Drone controller'), Gear('Markerlight')],
            )
        )
        self.sniperdrone = Count(
            self, 'Sniper Drone', min_limit=3, max_limit=9, points=self.drone_points,
            gear=UnitDescription(
                name='Sniper Drone',
                points=self.drone_points,
                options=[Gear('Long pulse rifle')],
            )
        )


class Hammerhead(Unit):
    type_name = 'Hammerhead'
    type_id = 'hammerhead_v1'

    def __init__(self, parent):
        super(Hammerhead, self).__init__(parent=parent, points=125, gear=[])

        self.commander = self.Commander(self)
        self.weapon = self.Weapon(self)
        self.up = self.Up(self, self.weapon)
        self.support_weapon = self.SupportWeapon(self)
        self.seekermissile = Count(self, 'Seeker missile', min_limit=0, max_limit=2, points=8)
        self.opt = Vehicle(self)

    class Commander(OptionsList):
        commander_name = 'Commander Longstrike'
        commander_points = 45

        def __init__(self, parent):
            super(Hammerhead.Commander, self).__init__(parent=parent, name='Options', limit=None)
            self.longstrike = self.variant(
                name=self.commander_name, points=self.commander_points,
                gear=UnitDescription(name=self.commander_name, points=self.commander_points,
                                     options=[Gear('XV02 Pilot Battlesuit')])
            )

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Hammerhead.Weapon, self).__init__(parent=parent, name='Weapon')

            self.railgunwithsolidshot = self.variant('Railgun with solid shot', 0)
            self.ioncannon = self.variant('Ion cannon', 0)

    class Up(OptionsList):
        def __init__(self, parent, weapon):
            super(Hammerhead.Up, self).__init__(parent=parent, name='', limit=None)
            self.weapon = weapon
            self.submunitionrounds = self.variant('Submunition rounds', 5)

        def check_rules(self):
            self.visible = self.used = self.weapon.cur == self.weapon.railgunwithsolidshot

    class SupportWeapon(OneOf):
        def __init__(self, parent):
            super(Hammerhead.SupportWeapon, self).__init__(parent=parent, name='')

            self.twogundrones = self.variant('Two Gun Drones', 0, gear=Gear('Gun Drone', count=2))
            self.twinlinkedburstcannon = self.variant('Twin-linked Burst cannon', 0)
            self.twinlinkedsmartmissilesystem = self.variant('Twin-linked Smart missile system', 0)

    def get_unique(self):
        if self.commander.longstrike.value:
            return [self.commander.commander_name]
        return []


class SkyRay(Unit):
    type_name = 'Sky Ray'
    type_id = 'skyray_v1'

    def __init__(self, parent):
        super(SkyRay, self).__init__(parent=parent, points=115, gear=[
            Gear('Seeker missile', count=6),
            Gear('Networked markerlight', count=2),
            Gear('Velocity tracker'),
        ])

        self.weapon = Hammerhead.SupportWeapon(self)
        self.opt = Vehicle(self)
