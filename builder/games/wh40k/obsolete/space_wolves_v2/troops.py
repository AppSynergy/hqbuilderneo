__author__ = 'Denis Romanov'

from builder.core2 import *
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle
from builder.games.wh40k.imperial_armour.volume2.transport import InfernumRazorback, LuciusDropPod, IATransport
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedUnit


class DropPod(SpaceMarinesBaseVehicle):
    type_name = 'Drop Pod'
    type_id = 'droppod_v1'

    def __init__(self, parent):
        super(DropPod, self).__init__(parent=parent, points=35, deep_strike=True, transport=True)
        self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DropPod.Weapon, self).__init__(parent=parent, name='Weapon')
            self.stormbolter = self.variant('Storm Bolter', 0)
            self.deathwindmissilelauncher = self.variant('Deathwind Missile Launcher', 20)


class Rhino(SpaceMarinesBaseVehicle):
    type_name = 'Rhino'
    type_id = 'rhino_v1'

    def __init__(self, parent):
        super(Rhino, self).__init__(
            parent=parent, points=35, tank=True, transport=True,
            gear=[Gear('Storm Bolter'), Gear('Smoke Launchers'), Gear('Searchlight')]
        )
        self.Options(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Rhino.Options, self).__init__(parent=parent, name='Options')
            self.dozenblade = self.variant('Dozen Blade', 5)
            self.stormbolter = self.variant('Storm Bolter', 10)
            self.hinterkillermissile = self.variant('Hinter-killer Missile', 10)
            self.extraarmour = self.variant('Extra Armour', 15)


class Razorback(SpaceMarinesBaseVehicle):
    type_name = 'Razorback'
    type_id = 'razorback_v1'

    def __init__(self, parent):
        super(Razorback, self).__init__(
            parent=parent, points=40, tank=True, transport=True,
            gear=[Gear('Smoke Launchers'), Gear('Searchlight')]
        )
        self.Weapon(self)
        self.Options(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Razorback.Weapon, self).__init__(parent=parent, name='Weapon')
            self.twinlinkedheavybolter = self.variant('Twin-linked Heavy Bolter', 0)
            self.twinlinkedheavyflamer = self.variant('Twin-linked Heavy Flamer', 25)
            self.twinlinkedassaultcannon = self.variant('Twin-linked Assault Cannon', 35)
            self.twinlinkedlascannon = self.variant('Twin-linked Lascannon', 35)
            self.lascannonandtwinlinkedplasmagun = self.variant('Lascannon and Twin-linked Plasma Gun', 35)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Razorback.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.dozenblade = self.variant('Dozen Blade', 5)
            self.stormbolter = self.variant('Storm Bolter', 10)
            self.hinterkillermissile = self.variant('Hinter-killer Missile', 10)
            self.extraarmour = self.variant('Extra Armour', 15)


class Transport(IATransport):
    def __init__(self, parent):
        super(Transport, self).__init__(parent=parent, name='Transport')
        self.models.extend([
            SubUnit(self, Rhino(parent=None)),
            SubUnit(self, Razorback(parent=None)),
            SubUnit(self, DropPod(parent=None)),
        ])
        self.ia_models.append(SubUnit(self, InfernumRazorback(parent=None)))


class DreadnoughtTransport(IATransport):
    def __init__(self, parent):
        super(DreadnoughtTransport, self).__init__(parent=parent, name='Transport')
        self.models.append(SubUnit(self, DropPod(parent=None)))
        self.ia_models.append(SubUnit(self, LuciusDropPod(parent=None)))


class GreyHunters(IATransportedUnit):
    type_name = "Grey Hunters Pack"
    type_id = "grey_hunters_pack_v1"

    class Marine(ListSubUnit):

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(GreyHunters.Marine.Weapon1, self).__init__(parent=parent, name='Weapon')
                self.bolter = self.variant('Bolter', 0)
                self.flamer = self.variant('Flamer', 0)
                self.meltagun = self.variant('Meltagun', 5)
                self.plasmagun = self.variant('Plasma Gun', 10)

        class FreeWeapon1(OneOf):
            def __init__(self, parent):
                super(GreyHunters.Marine.FreeWeapon1, self).__init__(parent=parent, name='Weapon')
                self.bolter = self.variant('Bolter', 0)
                self.flamer = self.variant('Flamer', 0)
                self.meltagun = self.variant('Meltagun', 0)
                self.plasmagun = self.variant('Plasma Gun', 0)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(GreyHunters.Marine.Weapon2, self).__init__(parent=parent, name='')
                self.boltpistol = self.variant('Bolt Pistol', 0)
                self.plasmapistol = self.variant('Plasma Pistol', 15)

        class Weapon3(OneOf):
            def __init__(self, parent):
                super(GreyHunters.Marine.Weapon3, self).__init__(parent=parent, name='')
                self.closecombatweapon = self.variant('Close Combat Weapon', 0)
                self.powerweapon = self.variant('Power Weapon', 15)
                self.powerfist = self.variant('Power Fist', 25)

        class Options(OptionsList):
            def __init__(self, parent):
                super(GreyHunters.Marine.Options, self).__init__(parent=parent, name='Options')
                self.wolfstandard = self.variant('Wolf Standard', 10)
                self.markofthewulfen = self.variant('Mark of the Wulfen', 15)
                self.free = self.variant('Free special weapon', gear=[])

        def __init__(self, parent):
            super(GreyHunters.Marine, self).__init__(
                parent=parent, name='Grey Hunter', points=15,
                gear=[Gear('Power Armour'), Gear('Frag Grenades'), Gear('Krak Grenades')]
            )
            self.wep1 = self.Weapon1(self)
            self.freewep1 = self.FreeWeapon1(self)
            self.wep2 = self.Weapon2(self)
            self.wep3 = self.Weapon3(self)
            self.opt = self.Options(self)

        def check_rules(self):
            super(GreyHunters.Marine, self).check_rules()
            self.wep1.visible = self.wep1.used = not self.opt.free.value
            self.freewep1.visible = self.freewep1.used = self.opt.free.value

        @ListSubUnit.count_gear
        def has_free(self):
            return self.opt.free.value and self.has_spec()

        @ListSubUnit.count_gear
        def has_spec(self):
            return (self.wep1.cur != self.wep1.bolter and self.wep1.used) or \
                   (self.freewep1.cur != self.freewep1.bolter and self.freewep1.used)

        @ListSubUnit.count_gear
        def has_pistol(self):
            return self.wep2.cur != self.wep2.boltpistol

        @ListSubUnit.count_gear
        def has_ccw(self):
            return self.wep3.cur != self.wep3.closecombatweapon

        @ListSubUnit.count_gear
        def has_mark(self):
            return self.opt.markofthewulfen.value

        @ListSubUnit.count_gear
        def has_banner(self):
            return self.opt.wolfstandard.value

    def __init__(self, parent):
        super(GreyHunters, self).__init__(parent)
        self.marines = UnitList(self, self.Marine, 5, 10)
        self.transport = Transport(self)

    def check_rules(self):
        super(GreyHunters, self).check_rules()
        if sum(u.has_banner() for u in self.marines.units) > 1:
            self.error('Only one Grey Hunters may have Wolf Standard')
        if sum(u.has_mark() for u in self.marines.units) > 1:
            self.error('Only one Grey Hunters may have Mark of the Wulfen')
        if sum(u.has_pistol() for u in self.marines.units) > 1:
            self.error('Only one Grey Hunters may upgrade his Bolt Pistol')
        if sum(u.has_ccw() for u in self.marines.units) > 1:
            self.error('Only one Grey Hunters may upgrade his Close Combat Weapon')
        spec_limit = 1 if self.get_count() < 10 else 2
        if sum(u.has_free() for u in self.marines.units) > 1:
            self.error('Only one Grey Hunters in squad number 10 models may have free special weapon')
        if sum(u.has_spec() for u in self.marines.units) > spec_limit:
            self.error('Only {} Grey Hunters may have special weapon'.format(spec_limit))

    def get_count(self):
        return self.marines.count


class BloodClaws(IATransportedUnit):
    type_name = "Blood Claws Pack"
    type_id = "blood_claws_pack_v1"

    class Options(OptionsList):
        def __init__(self, parent):
            super(BloodClaws.Options, self).__init__(parent=parent, name='Options')
            self.lukas = self.variant('Lukas the Trickster', points=155, gear=[UnitDescription(
                name='Lukas the Trickster', points=155,
                options=[Gear('Power Armour'), Gear('Wolftooth Necklace'), Gear('Wolf Tail Talisman'),
                         Gear('Wolf Claw'), Gear('Plasma Pistol'), Gear('Pelt of the Doppegangrel')]
            )])

    class Marine(ListSubUnit):

        class Options(OptionsList):
            def __init__(self, parent):
                super(BloodClaws.Marine.Options, self).__init__(parent=parent, name='Options')
                self.free = self.variant('Free special weapon', gear=[])

        def __init__(self, parent):
            super(BloodClaws.Marine, self).__init__(
                parent=parent, name='Blood Claws', points=15,
                gear=[Gear('Power Armour'), Gear('Frag Grenades'), Gear('Krak Grenades')]
            )
            self.wep1 = GreyHunters.Marine.Weapon1(self)
            self.freewep1 = GreyHunters.Marine.FreeWeapon1(self)
            self.wep2 = GreyHunters.Marine.Weapon2(self)
            self.wep3 = GreyHunters.Marine.Weapon3(self)
            self.opt = self.Options(self)

        def check_rules(self):
            super(BloodClaws.Marine, self).check_rules()
            self.wep1.visible = self.wep1.used = not self.opt.free.value
            self.freewep1.visible = self.freewep1.used = self.opt.free.value

        @ListSubUnit.count_gear
        def has_free(self):
            return self.opt.free.value and self.has_spec()

        @ListSubUnit.count_gear
        def has_spec(self):
            return (self.wep1.cur != self.wep1.bolter and self.wep1.used) or \
                   (self.freewep1.cur != self.freewep1.bolter and self.freewep1.used)

        @ListSubUnit.count_gear
        def has_pistol(self):
            return self.wep2.cur != self.wep2.boltpistol

        @ListSubUnit.count_gear
        def has_ccw(self):
            return self.wep3.cur != self.wep3.closecombatweapon

    def __init__(self, parent):
        super(BloodClaws, self).__init__(parent)
        self.marines = UnitList(self, self.Marine, 5, 15)
        self.opt = self.Options(self)
        self.transport = Transport(self)

    def check_rules(self):
        super(BloodClaws, self).check_rules()
        self.marines.update_range(5 - int(self.opt.lukas.value), 15 - int(self.opt.lukas.value))
        if sum(u.has_pistol() for u in self.marines.units) > 1:
            self.error('Only one Blood Claws may upgrade his Bolt Pistol')
        if sum(u.has_ccw() for u in self.marines.units) > 1:
            self.error('Only one Blood Claws may upgrade his Close Combat Weapon')
        spec_limit = 1 if self.get_count() < 15 else 2
        if sum(u.has_free() for u in self.marines.units) > 1:
            self.error('Only one Blood Claws in squad number 10 models may have free special weapon')
        if sum(u.has_spec() for u in self.marines.units) > spec_limit:
            self.error('Only {} Blood Claws may have special weapon'.format(spec_limit))

    def get_count(self):
        return self.marines.count + int(self.opt.lukas.value)

    def get_unique_gear(self):
        return super(BloodClaws, self).get_unique_gear() + self.opt.description
