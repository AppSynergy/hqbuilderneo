__author__ = 'Denis Romanov'

from builder.core2 import *
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedUnit
from troops import Transport


class LongFangs(IATransportedUnit):
    type_name = "Long Fangs Pack"
    type_id = "long_fangs_pack"

    model_gear = [Gear('Power Armour'), Gear('Frag Grenades'), Gear('Krak Grenades')]
    model_points = 15
    model = UnitDescription('Long Fang', points=model_points, options=model_gear + [Gear('Close Combat Weapon')])

    class LongFangLeader(Unit):
        type_name = 'Squad Leader'
        type_id = 'squadleader_v1'

        def __init__(self, parent):
            super(LongFangs.LongFangLeader, self).__init__(parent=parent, points=LongFangs.model_points,
                                                           gear=LongFangs.model_gear)
            self.Weapon1(self, name='Weapon')
            self.Weapon2(self, name='')
            self.Options(self)

        class CCW(OneOf):
            def __init__(self, parent, name):
                super(LongFangs.LongFangLeader.CCW, self).__init__(parent=parent, name=name)
                self.variant('Close Combat Weapon', 0)

        class Pistol(OneOf):
            def __init__(self, parent, name):
                super(LongFangs.LongFangLeader.Pistol, self).__init__(parent=parent, name=name)
                self.variant('Bolt Pistol', 0)

        class Weapon(OneOf):
            def __init__(self, parent, name):
                super(LongFangs.LongFangLeader.Weapon, self).__init__(parent=parent, name=name)
                self.flamer = self.variant('Flamer', 5)
                self.meltagun = self.variant('Meltagun', 10)
                self.plasmagun = self.variant('Plasma Gun', 15)
                self.plasmapistol = self.variant('Plasma Pistol', 15)
                self.powerweapon = self.variant('Power Weapon', 15)
                self.powerfist = self.variant('Power Fist', 25)

        class Options(OptionsList):
            def __init__(self, parent):
                super(LongFangs.LongFangLeader.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta Bombs', 5)

        class Weapon1(Weapon, CCW):
            pass

        class Weapon2(Weapon, Pistol):
            pass

    class MarineCount(Count):
        @property
        def description(self):
            return [LongFangs.model.clone().add(Gear('Bolt pistol')).set_count(self.cur - self.parent.count_heavy())]

    def __init__(self, parent):
        super(LongFangs, self).__init__(parent)
        self.leader = SubUnit(self, self.LongFangLeader(None))
        self.marines = self.MarineCount(self, 'Long Fang', 1, 5, self.model_points)
        self.spec = [
            Count(self, name=g['name'], min_limit=0, max_limit=1, points=g['points'],
                  gear=LongFangs.model.clone().add(Gear(g['name'])).add_points(g['points']))
            for g in [
                dict(name='Heavy Bolter', points=5),
                dict(name='Missile Launcher', points=10),
                dict(name='Multi-melta', points=10),
                dict(name='Plasma Cannon', points=20),
                dict(name='Lascannon', points=25),
            ]
        ]
        self.transport = Transport(self)

    def count_heavy(self):
        return sum(o.cur for o in self.spec)

    def check_rules(self):
        super(LongFangs, self).check_rules()
        Count.norm_counts(0, self.marines.cur, self.spec)
        if self.count_heavy() != self.marines.cur:
            self.error('Each Long Fang must select heavy weapon')

    def get_count(self):
        return self.marines.cur + 1


class Predator(SpaceMarinesBaseVehicle):
    type_name = 'Predator'
    type_id = 'predator_v1'

    def __init__(self, parent):
        super(Predator, self).__init__(parent=parent, points=60, gear=[Gear('Smoke Launchers'), Gear('Searchlight')],
                                       tank=True)
        self.Weapon(self)
        self.Sponsons(self)
        self.Options(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Predator.Weapon, self).__init__(parent=parent, name='Weapon')
            self.autocannon = self.variant('Autocannon', 0)
            self.twinlinkedlascannon = self.variant('Twin-linked Lascannon', 45)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(Predator.Sponsons, self).__init__(parent=parent, name='Side Sponsons', limit=1)
            self.heavybolters = self.variant('Heavy Bolters', 25, gear=[Gear('Heavy Bolter', count=2)])
            self.lascannons = self.variant('Lascannons', 60, gear=[Gear('Lascannon', count=2)])

    class Options(OptionsList):
        def __init__(self, parent):
            super(Predator.Options, self).__init__(parent=parent, name='Options')
            self.dozenblade = self.variant('Dozen Blade', 5)
            self.stormbolter = self.variant('Storm Bolter', 10)
            self.hinterkillermissile = self.variant('Hinter-killer Missile', 10)
            self.extraarmour = self.variant('Extra Armour', 15)


class Whirlwind(SpaceMarinesBaseVehicle):
    type_name = 'Whirlwind'
    type_id = 'whirlwind_v1'

    def __init__(self, parent):
        super(Whirlwind, self).__init__(
            parent=parent, points=85, tank=True,
            gear=[Gear('Whirlwind Multiple Missile Launcher'), Gear('Smoke Launchers'), Gear('Searchlight')]
        )
        self.Options(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Whirlwind.Options, self).__init__(parent=parent, name='Options')
            self.dozenblade = self.variant('Dozen Blade', 5)
            self.stormbolter = self.variant('Storm Bolter', 10)
            self.hinterkillermissile = self.variant('Hinter-killer Missile', 10)
            self.extraarmour = self.variant('Extra Armour', 15)


class Vindicator(SpaceMarinesBaseVehicle):
    type_name = 'Vindicator'
    type_id = 'vindicator_v1'

    def __init__(self, parent):
        super(Vindicator, self).__init__(
            parent=parent, points=115, tank=True,
            gear=[Gear('Demolisher Cannon'), Gear('Storm Bolter'), Gear('Smoke Launchers'), Gear('Searchlight')]
        )
        self.Options(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Vindicator.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.dozenblade = self.variant('Dozen Blade', 5)
            self.stormbolter = self.variant('Storm Bolter', 10)
            self.hinterkillermissile = self.variant('Hinter-killer Missile', 10)
            self.siegeshield = self.variant('Siege Shield', 10)
            self.extraarmour = self.variant('Extra Armour', 15)


class LandRaider(SpaceMarinesBaseVehicle):
    type_name = 'Land Raider'
    type_id = 'landraider_v1'

    def __init__(self, parent):
        super(LandRaider, self).__init__(
            parent=parent, points=250, tank=True, transport=True,
            gear=[Gear('Twin-linked Heavy Bolter'), Gear('Twin-linked Lascannon', count=2),
                  Gear('Smoke Launchers'), Gear('Searchlight')]
        )
        self.Options(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(LandRaider.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.stormbolter = self.variant('Storm Bolter', 10)
            self.hinterkillermissile = self.variant('Hinter-killer Missile', 10)
            self.multimelta = self.variant('Multi-melta', 10)
            self.extraarmour = self.variant('Extra Armour', 15)


class LandRaiderCrusader(SpaceMarinesBaseVehicle):
    type_name = 'Land Raider Crusader'
    type_id = 'landraidercrusader_v1'

    def __init__(self, parent):
        super(LandRaiderCrusader, self).__init__(
            parent=parent, points=250, tank=True, transport=True,
            gear=[Gear('Twin-linked Assault Cannon'), Gear('Hurricane Bolter', count=2), Gear('Smoke Launchers'),
                  Gear('Frag Assault Launcher'), Gear('Searchlight')]
        )
        LandRaider.Options(self)


class LandRaiderRedeemer(SpaceMarinesBaseVehicle):
    type_name = 'Land Raider Redeemer'
    type_id = 'landraiderredeemer_v1'

    def __init__(self, parent):
        super(LandRaiderRedeemer, self).__init__(
            parent=parent, points=240, tank=True, transport=True,
            gear=[Gear('Twin-linked Assault Cannon'), Gear('Flamestorm Cannon', count=2), Gear('Frag Assault Launcher'),
                  Gear('Smoke Launchers'), Gear('Searchlight')]
        )
        LandRaider.Options(self)
