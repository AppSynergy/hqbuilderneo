from builder.games.wh40k.obsolete.space_wolves import elites

__author__ = 'Denis Romanov'

from itertools import chain
from builder.core2 import *
from troops import Transport, DreadnoughtTransport
from heavy import LandRaider, LandRaiderRedeemer, LandRaiderCrusader
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedDreadnought, IATransportedUnit

LoneWolf = Unit.norm_core1_unit(elites.LoneWolf)
IronPriest = Unit.norm_core1_unit(elites.IronPriest)
WolfScouts = Unit.norm_core1_unit(elites.WolfScouts)


class WolfGuards(IATransportedUnit):
    type_name = "Wolf Guard Pack"

    class Transport(Transport):
        def __init__(self, parent):
            super(WolfGuards.Transport, self).__init__(parent=parent)
            self.lr = [
                SubUnit(self, LandRaider(parent=None)),
                SubUnit(self, LandRaiderRedeemer(parent=None)),
                SubUnit(self, LandRaiderCrusader(parent=None)),
            ]
            self.models.extend(self.lr)

    class WolfGuard(ListSubUnit):
        type_name = 'Wolf Guard'
        type_id = 'wolfguard_v1'

        def __init__(self, parent):
            super(WolfGuards.WolfGuard, self).__init__(parent=parent, points=18, gear=[])
            self.armour = self.Armour(self)
            self.wep1 = self.Weapon1(self, name='Weapon')
            self.wep2 = self.Weapon2(self, name='')
            self.tdawep1 = self.TdaWeapon1(self)
            self.tdawep2 = self.TdaWeapon2(self)
            self.opt = self.Options(self)

        def check_rules(self):
            super(WolfGuards.WolfGuard, self).check_rules()
            self.tdawep2.active_heavy(not self.opt.is_heavy())
            self.opt.active_heavy(not self.tdawep2.is_heavy())

        class Armour(OneOf):
            def __init__(self, parent):
                super(WolfGuards.WolfGuard.Armour, self).__init__(parent=parent, name='Armour')
                self.powerarmour = self.variant(
                    'Power Armour', 0, gear=[Gear('Power Armour'), Gear('Frag Grenades'), Gear('Krak Grenades')]
                )
                self.tda = self.variant('Terminator Armour', 15)

            def is_tda(self):
                return self.cur == self.tda

        @ListSubUnit.count_gear
        def count_heavy(self):
            return (self.tdawep2.is_heavy() or self.opt.is_heavy()) and self.armour.is_tda()

        @ListSubUnit.count_gear
        def count_mark(self):
            return self.opt.markofthewulfen.value

        class Pistol(OneOf):
            def __init__(self, parent, name):
                super(WolfGuards.WolfGuard.Pistol, self).__init__(parent=parent, name=name)
                self.variant('Bolt Pistol', 0)

        class CCW(OneOf):
            def __init__(self, parent, name):
                super(WolfGuards.WolfGuard.CCW, self).__init__(parent=parent, name=name)
                self.variant('Close Combat Weapon', 0)

        class Weapon(OneOf):
            def __init__(self, parent, name):
                super(WolfGuards.WolfGuard.Weapon, self).__init__(parent=parent, name=name)
                self.boltgun = self.variant('Boltgun', 0)
                self.stormbolter = self.variant('Storm Bolter', 3)
                self.combiflamer = self.variant('Combi-flamer', 5)
                self.combimelta = self.variant('Combi-melta', 5)
                self.combiplasma = self.variant('Combi-plasma', 5)
                self.powerweapon = self.variant('Power Weapon', 10)
                self.plasmapistol = self.variant('Plasma Pistol', 10)
                self.wolfclaw = self.variant('Wolf Claw', 15)
                self.powerfist = self.variant('Power Fist', 20)
                self.frostblade = self.variant('Frost Blade', 20)
                self.frostaxe = self.variant('Frost Axe', 20)
                self.thunderhummer = self.variant('Thunder Hummer', 25)
                self.stormshield = self.variant('Storm Shield', 25)

            def check_rules(self):
                super(WolfGuards.WolfGuard.Weapon, self).check_rules()
                self.visible = self.used = not self.parent.armour.is_tda()

        class Weapon1(Weapon, Pistol):
            pass

        class Weapon2(Weapon, CCW):
            pass

        class TdaWeapon1(OneOf):
            def __init__(self, parent):
                super(WolfGuards.WolfGuard.TdaWeapon1, self).__init__(parent=parent, name='Weapon')
                self.powerweapon = self.variant('Power Weapon', 0)
                self.wolfclaw = self.variant('Wolf Claw', 5)
                self.powerfist = self.variant('Power Fist', 10)
                self.frostblade = self.variant('Frost Blade', 10)
                self.frostaxe = self.variant('Frost Axe', 10)
                self.thunderhummer = self.variant('Thunder Hummer', 15)
                self.stormshield = self.variant('Storm Shield', 15)
                self.chainfist = self.variant('Chain Fist', 15)

            def check_rules(self):
                super(WolfGuards.WolfGuard.TdaWeapon1, self).check_rules()
                self.visible = self.used = self.parent.armour.is_tda()

        class TdaWeapon2(OneOf):
            def __init__(self, parent):
                super(WolfGuards.WolfGuard.TdaWeapon2, self).__init__(parent=parent, name='')
                self.stormbolter = self.variant('Storm Bolter', 0)
                self.combiflamer = self.variant('Combi-flamer', 5)
                self.combimelta = self.variant('Combi-melta', 5)
                self.combiplasma = self.variant('Combi-plasma', 5)
                self.wolfclaw = self.variant('Wolf Claw', 10)
                self.powerfist = self.variant('Power Fist', 10)
                self.thunderhummer = self.variant('Thunder Hummer', 15)
                self.stormshield = self.variant('Storm Shield', 15)
                self.chainfist = self.variant('Chain Fist', 15)
                self.heavyflamer = self.variant('Heavy Flamer', 5)
                self.assaultcannon = self.variant('Assault Cannon', 30)

            def check_rules(self):
                super(WolfGuards.WolfGuard.TdaWeapon2, self).check_rules()
                self.visible = self.used = self.parent.armour.is_tda()

            def is_heavy(self):
                return self.cur in [self.heavyflamer, self.assaultcannon]

            def active_heavy(self, val):
                for o in [self.heavyflamer, self.assaultcannon]:
                    o.active = val

        class Options(OptionsList):
            def __init__(self, parent):
                super(WolfGuards.WolfGuard.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta Bombs', 5)
                self.markofthewulfen = self.variant('Mark of the Wulfen', 15)
                self.mount = [
                    self.variant('Jump Pack', 25),
                    self.variant('Space Marine Bike', 35)
                ]
                self.cyclonemissilelauncher = self.variant('Cyclone Missile Launcher', 30)

            def is_heavy(self):
                return self.cyclonemissilelauncher.value

            def active_heavy(self, val):
                self.cyclonemissilelauncher.active = self.cyclonemissilelauncher.used = val

            def check_rules(self):
                super(WolfGuards.WolfGuard.Options, self).check_rules()
                for o in self.mount:
                    o.visible = o.used = not self.parent.armour.is_tda()
                self.process_limit(self.mount, 1)
                self.cyclonemissilelauncher.visible = self.cyclonemissilelauncher.used = self.parent.armour.is_tda()

    def __init__(self, parent):
        super(WolfGuards, self).__init__(parent)
        self.models = UnitList(self, self.WolfGuard, 3, 10)
        self.opt = self.Options(self)
        self.transport = self.Transport(self)

    def check_rules(self):
        super(WolfGuards, self).check_rules()
        self.models.update_range(3 - int(self.opt.arjac.value), 10 - int(self.opt.arjac.value))
        if sum(u.count_mark() for u in self.models.units) > 1:
            self.error('Only one Wolf Guard may have Mark of the Wulfen')
        spec_limit = int(self.get_count() / 5)
        heavy = sum(u.count_heavy() for u in self.models.units)
        if heavy > spec_limit:
            self.error('Wolf Guard may take only {} heavy weapon. (taken: {})'.format(spec_limit, heavy))

    def get_count(self):
        return self.models.count + int(self.opt.arjac.value)

    class Options(OptionsList):
        def __init__(self, parent):
            super(WolfGuards.Options, self).__init__(parent=parent, name='Options')
            self.arjac = self.variant('Arjac Rockfist, The Anvil of Fernis', points=170+18, gear=[UnitDescription(
                name='Arjac Rockfist', points=170+18,
                options=[Gear('Terminator Armour'), Gear('Foehammer'), Gear('Anvil Shield'), Gear('Wolftooth Necklace'),
                         Gear('Saga of Bear')]
            )])

    def get_unique_gear(self):
        return super(WolfGuards, self).get_unique_gear() + self.opt.description

    def has_landraider(self):
        return any(u.used for u in self.transport.lr)


class Dreadnought(IATransportedDreadnought):
    type_name = 'Dreadnought'
    type_id = 'dreadnought_v1'

    def __init__(self, parent):
        super(Dreadnought, self).__init__(
            parent=parent, points=105, walker=True,
            gear=[Gear('Smoke Launchers'), Gear('Searchlight')]
        )
        self.wep1 = self.Weapon1(self)
        self.buildin = self.BuildIn(self)
        self.Weapon2(self)
        self.Options(self)
        self.transport = DreadnoughtTransport(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Dreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.dreadnoughtccw = self.variant('Dreadnought Close Combat Weapon', 0)
            self.twinlinkedautocannon = self.variant('Twin-linked Autocannon', 10)
            self.missilelauncher = self.variant('Missile Launcher ', 10)

        def check_rules(self):
            super(Dreadnought.Weapon1, self).check_rules()
            self.parent.buildin.visible = self.parent.buildin.user = self.cur == self.dreadnoughtccw

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(Dreadnought.BuildIn, self).__init__(parent=parent, name='Weapon')
            self.variant('Storm Bolter', 0)
            self.variant('Heavy Flamer', 10)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Dreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.assaultcannon = self.variant('Assault Cannon', 0)
            self.twinlinkedheavyflamer = self.variant('Twin-linked Heavy Flamer', 0)
            self.multimelta = self.variant('Multi-melta', 0)
            self.twinlinkedheavybolter = self.variant('Twin-linked Heavy Bolter', 5)
            self.twinlinkedautocannon = self.variant('Twin-linked Autocannon', 10)
            self.plasmacannon = self.variant('Plasma Cannon', 10)
            self.twinlinkedlascannon = self.variant('Twin-linked Lascannon', 30)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Dreadnought.Options, self).__init__(parent=parent, name='Options', limit=None)

            self.extraarmour = self.variant('Extra Armour', 15)
            self.wolftoothnecklace = self.variant('Wolftooth Necklace', 10)
            self.wolftailtalisman = self.variant('Wolf Tail Talisman', 5)


class Venerable(IATransportedDreadnought):
    type_name = 'Venerable Dreadnought'
    type_id = 'venerabledreadnought_v1'

    def __init__(self, parent):
        super(Venerable, self).__init__(parent=parent, points=165, gear=[Gear('Smoke Launchers'), Gear('Searchlight')],
                                        walker=True)
        self.wep1 = Dreadnought.Weapon1(self)
        self.buildin = Dreadnought.BuildIn(self)
        Dreadnought.Weapon2(self)
        self.Options(self)
        self.transport = DreadnoughtTransport(self)

    class Options(Dreadnought.Options):
        def __init__(self, parent):
            super(Venerable.Options, self).__init__(parent=parent)
            self.sagaofmajesty = self.variant('Saga of Majesty', 15)
