from builder.games.wh40k.obsolete.space_wolves import hq

__author__ = 'Denis Romanov'

from builder.core2 import Unit

Logan = Unit.norm_core1_unit(hq.Logan)
Njal = Unit.norm_core1_unit(hq.Njal)
Ragnar = Unit.norm_core1_unit(hq.Ragnar)
Ulric = Unit.norm_core1_unit(hq.Ulric)
Canis = Unit.norm_core1_unit(hq.Canis)
Bjorn = Unit.norm_core1_unit(hq.Bjorn)
WolfLord = Unit.norm_core1_unit(hq.WolfLord)
RunePriest = Unit.norm_core1_unit(hq.RunePriest)
WolfPriest = Unit.norm_core1_unit(hq.WolfPriest)
BattleLeader = Unit.norm_core1_unit(hq.BattleLeader)
