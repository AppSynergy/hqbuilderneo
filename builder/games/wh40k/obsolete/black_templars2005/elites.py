__author__ = 'Denis Romanov'
from builder.core.unit import Unit, ListSubUnit
from builder.games.wh40k.obsolete.black_templars2005.troops import Rhino, Razorback, DropPod
from builder.games.wh40k.obsolete.black_templars2005.heavy import LandRaiderCrusader
from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor


class TerminatorAssaultSquad(Unit):
    name = 'Sword Brethren Terminator Assault Squad'

    def __init__(self):
        Unit.__init__(self)
        self.terms = self.opt_count('Terminators', 5, 10, 40)
        self.wep = self.opt_count('Thunder hummer and storm shield', 0, 5, 0)
        self.cs = self.opt_options_list('Options', [
            ['Crusader seals', 2, 'cs'],
        ])
        self.opt = self.opt_options_list('', [
            ['Furious charge', 3, 'fc'],
            ['Tank hunter', 3, 'th'],
        ], limit=1)

        self.transport = self.opt_optional_sub_unit('Transport', LandRaiderCrusader())

    def check_rules(self):
        self.wep.update_range(0, self.terms.get())
        self.transport.set_active(self.get_count() <= 8)
        self.set_points(self.build_points(exclude=[self.terms.id, self.transport.id]) + self.terms.points() +
                        self.transport.points())
        self.build_description(options=[])
        desc = ModelDescriptor(name='Terminator', points=40, gear=['Terminator armour'])\
            .add_gear_opt(self.cs)\
            .add_gear_opt(self.opt)
        self.description.add(desc.clone().add_gear('Thunder hummer').add_gear('Storm shield').build(self.wep.get()))
        self.description.add(desc.add_gear('Lightning claw', count=2).build(self.terms.get() - self.wep.get()))
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.terms.get()


class TerminatorSquad(Unit):
    name = 'Sword Brethren Terminator Squad'

    class Terminator(ListSubUnit):
        name = 'Terminator'
        base_points = 40
        gear = ['Terminator armour']

        def __init__(self):
            ListSubUnit.__init__(self, max_models=10)
            self.ccw = self.opt_one_of('Close combat weapon', [
                ['Power fist', 0, 'pfist'],
                ['Chainfist', 5, 'chfist']
            ])
            self.rng = self.opt_one_of('Ranged weapon', [
                ['Storm bolter', 0, 'sbgun'],
                ['Heavy flamer', 10, 'hflame'],
                ['Assault cannon', 20, 'asscan']
            ])
            self.cym = self.opt_options_list('Take missile launcher', [["Cyclone missile launcher", 25, 'cmlnch']])
            self.opt = None
            self.cs = None

        def has_special(self):
            return (1 if self.cym.get('cmlnch') or self.rng.get_cur() != 'sbgun' else 0) * self.count.get()

        def add_opt(self, cs, opt):
            self.cym.set_active(self.rng.get_cur() == 'sbgun')
            self.rng.set_active_options(['hflame', 'asscan'], not self.cym.get('cmlnch'))
            single_points = self.build_points(exclude=[self.count.id], count=1) + cs.points() + opt.points()
            self.set_points(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id])
            self.description.add(cs.get_selected())
            self.description.add(opt.get_selected())

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)
        self.terms = self.opt_units_list(self.Terminator.name, self.Terminator, 5, 10)
        self.cs = self.opt_options_list('Options', [
            ['Crusader seals', 2, 'cs'],
        ])
        self.opt = self.opt_options_list('', [
            ['Furious charge', 3, 'fc'],
            ['Tank hunter', 3, 'th'],
        ], limit=1)
        self.transport = self.opt_optional_sub_unit('Transport', LandRaiderCrusader())

    def check_rules(self):
        for u in self.terms.get_units():
            u.add_opt(self.cs, self.opt)
        self.terms.update_range()
        self.transport.set_active(self.get_count() <= 8)
        spec_total = sum((u.has_special() for u in self.terms.get_units()))
        if spec_total > 2:
            self.error('Special weapon (Cyclone missile launcher, Heavy flamer, Assault cannon) is over limit 2 '
                       '(taken {0}).'.format(spec_total))
        self.set_points(self.build_points(count=1, exclude=[self.cs.id, self.opt.id]))
        self.build_description(options=[self.terms, self.transport], count=1)

    def get_count(self):
        return self.terms.get_count()


class Dreadnought(Unit):
    name = 'Dreadnought'
    base_points = 105

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Assault cannon', 0, 'asscan'],
            ['Twin-linked lascannon', 20, 'tllcan'],
            ['Multi-melta', 10, 'mmelta'],
        ])
        self.wep2 = self.opt_one_of('', [
            ['Power fist', 0, 'dccw'],
            ['Missile launcher', 10, 'mlnch'],
        ])
        self.bin = self.opt_one_of('', [
            ['Built-in Storm bolter', 0, 'sbgun'],
            ['Built-in Heavy flamer', 10, 'hflame']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Extra armour', 5, 'ea'],
            ['Searchlight', 1, 'scl'],
            ['Smoke launchers', 3, 'sls'],
        ])
        self.ven = self.opt_options_list('', [
            ["Venerable Dreadnought", 20, 'ven']
        ])
        self.ven_up = self.opt_options_list('', [
            ['Furious charge', 10, 'fc'],
            ['Tank hunter', 10, 'th'],
        ], limit=1)
        self.transport = self.opt_optional_sub_unit('Transport', DropPod())

    def check_rules(self):
        self.bin.set_visible(self.wep2.get_cur() == 'dccw')
        self.ven_up.set_visible(self.ven.get('ven'))
        self.set_points(self.build_points())
        if self.ven.get('ven'):
            self.build_description(name='Venerable Dreadnought', exclude=[self.ven.id])
        else:
            self.build_description()


class SwordBrethrenSquad(Unit):
    name = 'Sword Brethren Squad'

    class Brethren(ListSubUnit):
        name = "Sword brethren"
        base_points = 19
        gear = ['Power armor']

        def __init__(self):
            ListSubUnit.__init__(self, max_models=10)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bg'],
                ['Bolt pistol and close combat weapon', 0, 'ccw'],
            ])

            self.wep2 = self.opt_options_list('', [
                ['Heavy bolter', 5, 'hb'],
                ['Missile launcher', 10, 'ml'],
                ['Multi-melta', 10, 'mm'],
                ['Lascannon', 15, 'lc'],
                ['Plasma cannon', 20, 'pc'],
                ['Lightning claws', 25, 'lcs'],
                ['Power fist', 15, 'pf'],
                ['Flamer', 6, 'fl'],
                ['Meltagun', 10, 'mg'],
                ['Plasma gun', 10, 'pg'],
                ['Power weapon', 15, 'pw'],
            ], limit=1)
            self.heavy = ['lcs', 'pf', 'hb', 'ml', 'mm', 'lc', 'pc']
            self.spec = ['fl', 'mg', 'pg', 'pw']
            self.honour = self.opt_options_list('Options', [
                ['Terminator honours', 10]
            ])
            self.th_wep = self.opt_options_list('', [
                ['Storm shield and bolt pistol', 10, 'ssbp'],
                ['Storm shield and close combat weapon', 10, 'ssccw'],
            ], limit=1)
            self.th_combat = self.opt_options_list('', [
                ['Combat shield', 5, 'cs'],
            ])

        def set_opt(self, bomb, cs, skill):
            self.th_combat.set_visible(self.honour.get_all() != [])
            self.th_wep.set_visible(self.honour.get_all() != [])
            self.wep2.set_active(self.get_count() == 1)
            single_points = self.build_points(exclude=[self.count.id], count=1) + bomb.points() + cs.points()
            self.set_points(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id, self.wep1.id,
                                                                           self.th_wep.id, self.wep2.id])
            if self.wep2.get('lcs'):
                self.description.add('Lightning claw', 2)
            else:
                self.description.add(self.wep2.get_selected())
            if self.th_wep.get_all():
                self.description.add('Storm shield')
            if self.wep1.get_cur() == 'bg':
                self.description.add(self.wep1.get_selected())
                if self.th_wep.get('ssbp'):
                    self.description.add(['Bolt pistol'])
                elif self.th_wep.get('ssccw'):
                    self.description.add(['Close combat weapon'])
            else:
                self.description.add(['Bolt pistol', 'Close combat weapon'])
            self.description.add(bomb.get_selected())
            self.description.add(cs.get_selected())
            self.description.add(skill.get_selected())

        def has_heavy(self):
            for ids in self.wep2.get_all():
                if ids in self.heavy:
                    return self.get_count()
            return 0

        def has_spec(self):
            for ids in self.wep2.get_all():
                if ids in self.spec:
                    return self.get_count()
            return 0

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_units_list(self.Brethren.name, self.Brethren, 5, 10)
        self.opt = self.opt_options_list('Options', [
            ['Frag grenades', 1, 'frag'],
            ['Krak grenades', 2, 'krak'],
        ])
        self.cs = self.opt_options_list('', [
            ['Crusader seals', 2, 'cs'],
        ])
        self.skill = self.opt_one_of('', [
            ['Furious charge', 0, 'fc'],
            ['Counter-attack', 0, 'ca'],
            ['Infiltrate', 0, 'inf'],
        ])
        self.transport = self.opt_options_list('Transport', [
            [Rhino.name, Rhino.base_points, 'rh'],
            [DropPod.name, DropPod.base_points, 'dp'],
            [Razorback.name, Razorback.base_points, 'rz'],
            [LandRaiderCrusader.name, LandRaiderCrusader.base_points, 'lr']
        ], limit=1)
        self.rh = self.opt_sub_unit(Rhino())
        self.rz = self.opt_sub_unit(Razorback())
        self.dp = self.opt_sub_unit(DropPod())
        self.lr = self.opt_sub_unit(LandRaiderCrusader())

    def check_rules(self):
        self.skill.set_active_options(['inf'], not self.transport.get_all())
        spec = sum((u.has_spec() for u in self.marines.get_units()))
        if spec > 1:
            self.error('Sword brethren can take only 1 special weapon (taken: {0})'.format(spec))
        heavy = sum((u.has_heavy() for u in self.marines.get_units()))
        if heavy > 1:
            self.error('Sword brethren can take only 1 heavy or power weapon (taken: {0})'.format(heavy))

        self.marines.update_range()
        for unit in self.marines.get_units():
            unit.set_opt(self.opt, self.cs, self.skill)
        self.dp.set_active(self.get_count() <= 10 and self.transport.get('dp'))
        self.rh.set_active(self.get_count() <= 10 and self.transport.get('rh'))
        self.rz.set_active(self.get_count() <= 6 and self.transport.get('rz'))
        self.lr.set_active(self.get_count() <= 15 and self.transport.get('lr'))
        self.set_points(self.build_points(count=1, exclude=[self.opt.id, self.cs.id, self.transport.id]))
        self.build_description(options=[self.marines])
        self.description.add(self.lr.get_selected())
        self.description.add(self.dp.get_selected())
        self.description.add(self.rz.get_selected())
        self.description.add(self.rh.get_selected())

    def get_count(self):
        return self.marines.get_count()


class Techmarine(Unit):
    name = 'Techmarine'
    base_points = 70
    base_gear = ['Power armour', 'Bolter', 'Power weapon', 'Signum', 'Auspex', 'Crusader seals', 'Servo-arm']

    def __init__(self):
        Unit.__init__(self)
        self.harness = self.opt_options_list('Equipment', [['Servo-harness', 35, 'sharn']])
        self.ts = self.opt_count('Tech servitor', 0, 4, 10)
        self.cs = self.opt_count('Combat servitor', 0, 4, 25)
        self.gs = self.opt_count('Gun servitor', 0, 4, 10)
        self.hb = self.opt_count('Heavy bolter', 0, 0, 15)
        self.mm = self.opt_count('Multi-melta', 0, 0, 25)
        self.pc = self.opt_count('Plasma cannon', 0, 1, 35)
        self.transport = self.opt_optional_sub_unit('Transport', [Rhino(), Razorback(), LandRaiderCrusader()])

    def check_rules(self):
        norm_counts(0, 4, [self.ts, self.gs, self.cs])
        self.hb.set_active(self.gs.get() > 0)
        self.mm.set_active(self.gs.get() > 0)
        self.pc.set_active(self.gs.get() > 0)
        self.hb.update_range(self.gs.get() - self.pc.get() - self.mm.get(),
                             self.gs.get() - self.pc.get() - self.mm.get())
        self.mm.update_range(0, self.gs.get() - self.pc.get())

        self.set_points(self.build_points(count=1))
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name=self.name, points=70, count=1,
                                               gear=self.base_gear).add_gear_opt(self.harness).build())
        self.description.add(ModelDescriptor('Tech servitor', points=10, count=self.ts.get(),
                                             gear=['Close combat weapon']).build())
        self.description.add(ModelDescriptor('Combat servitor', points=25, count=self.cs.get(),
                                             gear=['Close combat weapon', 'Power fist']).build())
        gs = ModelDescriptor('Gun servitor', points=10)
        self.description.add(gs.clone().add_gear('Heavy bolter', points=15).build(self.hb.get()))
        self.description.add(gs.clone().add_gear('Milti-melta', points=25).build(self.mm.get()))
        self.description.add(gs.clone().add_gear('Plasma cannon', points=35).build(self.pc.get()))
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return 1 + self.gs.get() + self.ts.get() + self.cs.get()
