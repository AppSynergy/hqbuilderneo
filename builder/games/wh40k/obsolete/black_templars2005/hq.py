__author__ = 'maria'

from builder.core.unit import Unit, ListSubUnit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.black_templars2005.armory import *
from builder.games.wh40k.obsolete.black_templars2005.elites import TerminatorSquad
from builder.games.wh40k.obsolete.black_templars2005.heavy import LandRaiderCrusader
from builder.games.wh40k.obsolete.black_templars2005.troops import Rhino, Razorback, DropPod


class TerminatorCommand(Unit):
    name = 'Sword Brethren Terminator Command Squad'

    class Sergeant(Unit):
        name = 'Terminator Sergeant'
        base_points = 40
        gear = ['Terminator armour', 'Storm bolter', 'Power weapon']

        def __init__(self):
            Unit.__init__(self)
            self.sh_tda_wep = self.opt_options_list('Weapon', sh_tda_weapon_1w, limit=2)
            self.th_tda_wep = self.opt_options_list('', th_tda_weapon_1w, limit=1)
            self.plc = self.opt_options_list('', plc, limit=1)
            self.tda_wargear = self.opt_options_list('Wargear', tda_wargear, points_limit=100)

        def add_opt(self, cs, opt):
            one_handed = len(self.sh_tda_wep.get_all())
            two_handed = len(self.th_tda_wep.get_all())
            plc = self.plc.get('plc')
            self.plc.set_active(one_handed + two_handed == 0)
            self.sh_tda_wep.set_limit(limit=(1 if two_handed == 1 else 2))
            self.sh_tda_wep.set_active(not plc)
            self.th_tda_wep.set_active(one_handed < 2 and not plc)
            self.set_points(self.build_points() + opt.points() + cs.points())
            self.build_description(exclude=[self.plc.id])
            if plc:
                self.description.add('Lighting claw', 2)
            self.description.add(cs.get_selected())
            self.description.add(opt.get_selected())

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)
        self.base_count = 1
        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.terms = self.opt_units_list(TerminatorSquad.Terminator.name, TerminatorSquad.Terminator, 3, 9)
        self.cs = self.opt_options_list('Options', [
            ['Crusader seals', 2, 'cs'],
        ])
        self.opt = self.opt_options_list('', [
            ['Furious charge', 3, 'fc'],
            ['Tank hunter', 3, 'th'],
        ], limit=1)
        self.transport = self.opt_optional_sub_unit('Transport', LandRaiderCrusader())

    def check_rules(self):
        for u in self.terms.get_units() + [self.sergeant.get_unit()]:
            u.add_opt(self.cs, self.opt)
        self.terms.update_range()
        self.transport.set_active(self.get_count() <= (8 - self.base_count))
        spec_total = sum((u.has_special() for u in self.terms.get_units()))
        if spec_total > 2:
            self.error('Special weapon (Cyclone missile launcher, Heavy flamer, Assault cannon) is over limit 2 '
                       '(taken {0}).'.format(spec_total))
        self.set_points(self.build_points(count=1, exclude=[self.cs.id, self.opt.id]))
        self.build_description(options=[self.sergeant, self.terms, self.transport], count=1)

    def get_skills(self):
        return self.opt

    def get_count(self):
        return 1 + self.terms.get_count()


class CommandSquad(Unit):
    name = 'Sword Brethren Command Squad'

    class Brethren(ListSubUnit):
        name = "Sword brethren"
        base_points = 16
        gear = ['Power armor']

        def __init__(self):
            ListSubUnit.__init__(self, max_models=10)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bg'],
                ['Bolt pistol and close combat weapon', 0, 'ccw'],
            ])

            self.wep2 = self.opt_options_list('', [
                ['Heavy bolter', 15, 'hb'],
                ['Missile launcher', 20, 'ml'],
                ['Multi-melta', 20, 'mm'],
                ['Lascannon', 35, 'lc'],
                ['Plasma cannon', 35, 'pc'],
                ['Flamer', 6, 'fl'],
                ['Meltagun', 10, 'mg'],
                ['Plasma gun', 10, 'pg'],
            ], limit=1)

            self.honour = self.opt_options_list('Options', [
                ['Terminator honours', 10]
            ])

        def get_honour(self):
            return len(self.honour.get_all()) > 0

        def set_opt(self, bomb, cs, skill):
            self.wep2.set_active(self.get_count() <= 2)
            single_points = self.build_points(exclude=[self.count.id], count=1) + \
                bomb.points() + cs.points() + skill.points()
            self.set_points(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id, self.wep1.id])
            self.description.add(self.wep2.get_selected())
            if self.wep1.get_cur() == 'bg':
                self.description.add(self.wep1.get_selected())
            else:
                self.description.add(['Bolt pistol', 'Close combat weapon'])
            self.description.add(bomb.get_selected())
            self.description.add(cs.get_selected())
            self.description.add(skill.get_selected())

        def has_spec(self):
            if len(self.wep2.get_all()):
                return self.get_count()
            return 0

        def check_rules(self):
            pass

    class Specialist(Unit):
        def __init__(self, th_points=10):
            Unit.__init__(self)
            self.armour = self.opt_one_of('Armour', [
                ['Power armour', 0, 'pwr'],
                ['Artificer armour', 20, 'art'],
            ])
            self.wep1 = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bg'],
                ['Bolt pistol and close combat weapon', 0, 'ccw'],
            ])
            self.honour = self.opt_options_list('Options', [
                ['Terminator honours', th_points]
            ])
            self.sh_wep = self.opt_options_list('Weapon', sh_weapon_1w, limit=2)
            self.th_wep = self.opt_options_list('', th_weapon_1w, limit=1)
            self.plc = self.opt_options_list('', plc, limit=1)
            self.wargear = self.opt_options_list('Wargear', wargear_w1, points_limit=100)

        def get_honour(self):
            return len(self.honour.get_all()) > 0

        def set_opt(self, bomb, cs, skill):
            honour = len(self.honour.get_all())
            plc = self.plc.get('plc')
            if honour:
                one_handed = len(self.sh_wep.get_all())
                two_handed = len(self.th_wep.get_all())
                self.plc.set_active(one_handed + two_handed == 0)
                self.sh_wep.set_limit(limit=(1 if two_handed == 1 else 2))
                self.sh_wep.set_active(not plc)
                self.th_wep.set_active(one_handed < 2 and not plc)
            self.plc.set_visible(honour)
            self.sh_wep.set_visible(honour)
            self.th_wep.set_visible(honour)
            self.armour.set_active_options(['art'], honour)
            self.wargear.set_visible(honour)

            self.set_points(self.build_points(count=1) + bomb.points() + cs.points() + skill.points())
            self.build_description(exclude=[self.plc.id, self.wep1.id], count=1)
            if self.wep1.get_cur() == 'bg':
                self.description.add(self.wep1.get_selected())
            else:
                self.description.add(['Bolt pistol', 'Close combat weapon'])
            if plc:
                self.description.add('Lighting claw', 2)
            self.description.add(bomb.get_selected())
            self.description.add(cs.get_selected())
            self.description.add(skill.get_selected())

        def check_rules(self):
            pass

    class Sergeant(Specialist):
        name = 'Sergeant'
        base_points = 16

        def __init__(self):
            CommandSquad.Specialist.__init__(self, th_points=15)

    class Medic(Specialist):
        name = 'Apothecary'
        base_points = 16 + 15
        gear = ['Narthecium']

    class Champion(Specialist):
        name = 'Fighting Company Champion'
        base_points = 16 + 20
        gear = ['Power weapon', 'Combat shield', 'Bolt pistol']

    class Flag(Specialist):
        name = 'Fighting Company Standard Barrier'
        base_points = 16 + 25
        gear = ['Fighting Company Standard ']

        def __init__(self, is_holy):
            CommandSquad.Specialist.__init__(self)
            self.flags = self.opt_options_list('Banners', [
                ['Chapter banner', 25, 'cb'],
                ['Sacred standard', 20, 'ss'],
            ] + ([['Holy relic', 30, 'hr']] if is_holy else []), limit=1)

        def set_opt(self, bomb, cs, skill):
            CommandSquad.Specialist.set_opt(self, bomb, cs, skill)
            self.flags.set_visible(len(self.honour.get_all()) > 0)

    def __init__(self, commander, forced_charge=False, hide_skills=False):
        Unit.__init__(self)
        self.commander = commander
        self.fc = forced_charge
        self.hide_skill = hide_skills
        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.marines = self.opt_units_list(self.Brethren.name, self.Brethren, 4, 9)
        self.spec = self.opt_options_list('Specialist', [
            [self.Medic.name, self.Medic.base_points, 'medic'],
            [self.Champion.name, self.Champion.base_points, 'champ'],
            [self.Flag.name, self.Flag.base_points, 'flag'],
        ])
        self.medic = self.opt_sub_unit(self.Medic())
        self.champ = self.opt_sub_unit(self.Champion())
        self.flag = self.opt_sub_unit(self.Flag(commander.is_holy()))
        self.opt = self.opt_options_list('Options', [
            ['Frag grenades', 1, 'frag'],
            ['Krak grenades', 2, 'krak'],
        ])
        self.cs = self.opt_options_list('', [
            ['Crusader seals', 2, 'cs'],
        ])
        self.skill = self.opt_options_list('', [
            ['Furious charge', 3, 'fc'],
            ['Counter-attack', 3, 'ca'],
            ['Infiltrate', 3, 'inf'],
        ], limit=1)
        self.transport = self.opt_options_list('Transport', [
            [Rhino.name, Rhino.base_points, 'rh'],
            [DropPod.name, DropPod.base_points, 'dp'],
            [Razorback.name, Razorback.base_points, 'rz'],
            [LandRaiderCrusader.name, LandRaiderCrusader.base_points, 'lr']
        ], limit=1)
        self.rh = self.opt_sub_unit(Rhino())
        self.rz = self.opt_sub_unit(Razorback())
        self.dp = self.opt_sub_unit(DropPod())
        self.lr = self.opt_sub_unit(LandRaiderCrusader())

    def get_skills(self):
        return self.skill

    def check_rules(self):
        if self.fc:
            self.skill.set('fc', True)
        if self.hide_skill:
            self.skill.set_visible(False)
        spec_count = len(self.spec.get_all())
        self.marines.update_range(4 - spec_count, 9 - spec_count)
        self.medic.set_active(self.spec.get('medic'))
        self.champ.set_active(self.spec.get('champ'))
        self.flag.set_active(self.spec.get('flag'))
        self.skill.set_active_options(['inf'], not self.transport.get_all())
        spec = sum((u.has_spec() for u in self.marines.get_units()))
        if spec > 2:
            self.error('Sword brethren command squad can take only 2 special weapons (taken: {0})'.format(spec))

        if not self.sergeant.get_unit().get_honour():
            if any((u.get_honour() for u in self.marines.get_units() + [self.medic.get_unit(),
                                                                        self.champ.get_unit(), self.flag.get_unit()])):
                self.error('You can\'t take Terminator honours for Brethren or Specialists before Sergeant.')

        self.marines.update_range()
        for unit in self.marines.get_units() + [self.sergeant.get_unit(), self.medic.get_unit(),
                                                self.champ.get_unit(), self.flag.get_unit()]:
            if unit:
                unit.set_opt(self.opt, self.cs, self.skill)
        disabled = []
        full_count = self.get_count() + self.commander.get_base_count()
        if full_count > 15:
            disabled += ['lr']
        if full_count > 10:
            disabled += ['dp', 'rh']
        if full_count > 6:
            disabled += ['rz']
        self.transport.set_active_options(self.transport.get_all_ids(), True)
        self.transport.set_active_options(disabled, False)
        self.dp.set_active(self.transport.get('dp'))
        self.rh.set_active(self.transport.get('rh'))
        self.rz.set_active(self.transport.get('rz'))
        self.lr.set_active(self.transport.get('lr'))
        self.set_points(self.build_points(count=1, exclude=[self.opt.id, self.cs.id, self.skill.id, self.transport.id]))
        self.build_description(options=[self.sergeant, self.marines, self.medic, self.champ, self.flag])
        self.description.add(self.lr.get_selected())
        self.description.add(self.dp.get_selected())
        self.description.add(self.rz.get_selected())
        self.description.add(self.rh.get_selected())

    def get_count(self):
        return self.marines.get_count() + 1 + len(self.spec.get_all())


class Commander(Unit):
    gear = ['Crusader seals']

    def __init__(self):
        Unit.__init__(self)
        self.armour = self.opt_one_of('Armour', [
            ['Power armour', 0, 'pwr'],
            ['Artificer armour', 20, 'art'],
            ['Terminator armour', 25, 'tda']
        ])
        self.sh_wep = self.opt_options_list('Weapon', sh_weapon, limit=2)
        self.th_wep = self.opt_options_list('', th_weapon, limit=1)
        self.sh_tda_wep = self.opt_options_list('Weapon', sh_tda_weapon, limit=2)
        self.th_tda_wep = self.opt_options_list('', th_tda_weapon, limit=1)
        self.plc = self.opt_options_list('', plc, limit=1)
        self.wargear = self.opt_options_list('Wargear', wargear, points_limit=100)
        self.tda_wargear = self.opt_options_list('Wargear', tda_wargear, points_limit=100)
        self.squads = self.opt_options_list('', [
            [TerminatorCommand.name, 0, 'tc'],
            [CommandSquad.name, 0, 'cs'],
        ])
        self.tc = self.opt_sub_unit(TerminatorCommand())
        self.cs = self.opt_sub_unit(CommandSquad(self))

    def get_base_count(self):
        return 1

    def is_holy(self):
        return False

    def check_rules(self):
        tda = (self.armour.get_cur() == 'tda')
        self.squads.set_active_options(['tc'], tda)
        self.squads.set_active_options(['cs'], not tda)
        self.tc.set_active(self.squads.get('tc'))
        self.cs.set_active(self.squads.get('cs'))
        self.sh_tda_wep.set_visible(tda)
        self.th_tda_wep.set_visible(tda)
        self.tda_wargear.set_visible(tda)
        self.th_wep.set_visible(not tda)
        self.sh_wep.set_visible(not tda)
        self.wargear.set_visible(not tda)
        if tda:
            sh_wep = self.sh_tda_wep
            th_wep = self.th_tda_wep
        else:
            sh_wep = self.sh_wep
            th_wep = self.th_wep
        one_handed = len(sh_wep.get_all())
        two_handed = len(th_wep.get_all())
        plc = self.plc.get('plc')
        self.plc.set_active(one_handed + two_handed == 0)
        sh_wep.set_limit(limit=(1 if two_handed == 1 else 2))
        sh_wep.set_active(not plc)
        th_wep.set_active(one_handed < 2 and not plc)
        skill_points = 0
        skill_desc = None
        squad_unit = self.tc.get_unit() or self.cs.get_unit()
        if squad_unit:
            opt = squad_unit.get_skills()
            skill_points = opt.points()
            skill_desc = opt.get_selected()
        self.set_points(self.build_points(count=1) + skill_points)
        self.build_description(exclude=[self.plc.id, self.squads.id], count=1)
        if plc:
            self.description.add('Lighting claw', 2)
        self.description.add(skill_desc)

    def get_count(self):
        return 1 + self.tc.get_count() + self.cs.get_count()


class Marshall(Commander):
    name = 'Marshall'
    base_points = 80


class Castellan(Commander):
    name = 'Castellan'
    base_points = 65


class Chaplain(Commander):
    gear = Commander.gear + ['Crozius arcanum', 'Rosarius']

    def __init__(self):
        Commander.__init__(self)
        self.opt = self.opt_count('Cenobyte Servitor', 0, 3, 10)

    def check_rules(self):
        self.opt.set_active(not self.squads.get('tc') and
                            not (self.squads.get('cs') and len(self.cs.get_unit().skill.get_all())))
        Commander.check_rules(self)

    def get_count(self):
        return self.opt.get() + Commander.get_count(self)

    def get_base_count(self):
        return 1 + self.opt.get()

    def is_holy(self):
        return True


class Master(Chaplain):
    name = 'Master of Sanctuary'
    base_points = 110


class Reclusiarch(Chaplain):
    name = 'Reclusiarch'
    base_points = 95


class Champion(Unit):
    name = 'Emperors\'s Champion'
    base_points = 90
    gear = ['Black Sword', 'The Armour of Faith', 'Terminator Honours', 'Crusaders seals', 'Frag grenades',
            'Bolt pistol']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_one_of('Vows', [
            ['Accept any challenge, no matter the odds', 50],
            ['Uphold the Honour of the Emperor', 10],
            ['Shutter not the Unclean to Live', 35],
            ['Abhor the witch, Destroy the Witch', 20],
        ])


class HighMarshall(Unit):
    unique = True
    name = 'High Marshall Helbrecht'
    base_points = 175
    gear = ['Artificer armour', 'Sword of High Marshals', 'Combi-melta', 'Bionics', 'Crusaders seals',
            'Terminator honours', 'Iron halo', 'Frag grenades', 'Krak grenades']

    def __init__(self):
        Unit.__init__(self)
        self.cs = self.opt_optional_sub_unit('', CommandSquad(self, forced_charge=True))
        self.neo = self.opt_count('Neophyte', 0, 5, 10)
        self.shotgun = self.opt_count('Shotguns', 0, 0, 0)
        self.flag = self.opt_options_list('', [
            ['Fighting Company Standard', 15, 'flag']
        ])

    def get_base_count(self):
        return 1 + self.neo.get()

    def is_holy(self):
        return False

    def check_rules(self):
        skill_points = 0
        skill_desc = None
        if self.cs.get_unit():
            opt = self.cs.get_unit().get_unit().get_skills()
            skill_points = opt.points()
            skill_desc = opt.get_selected()
        self.neo.set_active(self.cs.get_unit() is not None)
        self.shotgun.set_active(self.cs.get_unit() is not None)
        self.flag.set_visible(self.cs.get_unit() is not None)
        self.flag.set_active_options(['flag'], self.neo.get() > 0)
        self.shotgun.update_range(0, self.neo.get())

        self.set_points(self.build_points(count=1) + skill_points)
        self.build_description(count=1, exclude=[self.neo.id, self.flag.id, self.shotgun])
        self.description.add(skill_desc)

        desc = ModelDescriptor('Neophyte', gear=['Scout armor'], points=10)
        bp = self.neo.get() - self.shotgun.get()
        sg = self.shotgun.get()
        if self.flag.get_all():
            if bp != 0:
                bp -= 1
                self.description.add(desc.clone().add_gear_opt(self.flag).add_gear('Bolt pistol')
                    .add_gear('Close combat weapon').build(1))
            else:
                sg -= 1
                self.description.add(desc.clone().add_gear_opt(self.flag).add_gear('Shotgun').build(1))
        self.description.add(desc.clone().add_gear('Shotgun').build(sg))
        self.description.add(desc.add_gear('Bolt pistol').add_gear('Close combat weapon').build(bp))

    def get_count(self):
            return self.get_base_count() + self.cs.get_count()


class Grimaldus(Unit):
    unique = True
    name = 'Chaplain Grimaldus, Hero of Helsearch'
    gear = ['Crozius arcanum', 'Rosarius', 'Power armour', 'Master-crafter plasma pistol', 'Crusaders seals',
            'Terminator honours', 'Cenobyte Servitor', 'Cenobyte Servitor', 'Cenobyte Servitor']
    base_points = 195

    def __init__(self):
        Unit.__init__(self)
        self.cs = self.opt_optional_sub_unit('', CommandSquad(self, hide_skills=True))

    def check_rules(self):
        self.set_points(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return 4 + self.cs.get_count()

    def get_base_count(self):
        return 4

    def is_holy(self):
        return True
