__author__ = 'maria'

plc = [
    ['Pair of lighting claws', 30, 'plc'],  # TODO: count as two weapons
]

# single-handed
sh_weapon = [
    ['Bolt pistol', 1, 'bp'],  # TODO: not with Terminator Armour
    ['Close combat weapon', 1, 'ccw'],  # TODO: not with Terminator Armour
    ['Lighting claw', 25, 'slc'],
    ['Plasma pistol', 15, 'pp'],  # TODO: not with Terminator Armour
    ['Power fist', 25, 'pf'],
    ['Power weapon', 15, 'pw'],
    ['Storm shield', 10, 'ssh'],
    ['Thunder hummer', 30, 'th'],
]

sh_weapon_1w = [
    ['Bolt pistol', 1, 'bp'],  # TODO: not with Terminator Armour
    ['Close combat weapon', 1, 'ccw'],  # TODO: not with Terminator Armour
    ['Lighting claw', 15, 'slc'],
    ['Plasma pistol', 10, 'pp'],  # TODO: not with Terminator Armour
    ['Power fist', 15, 'pf'],
    ['Power weapon', 10, 'pw'],
    ['Storm shield', 10, 'ssh'],
    ['Thunder hummer', 30, 'th'],
]

sh_tda_weapon = [
    ['Chainfist', 30, 'chf'],  # TODO: with Terminator Armour only
    ['Lighting claw', 25, 'slc'],
    ['Power fist', 25, 'pf'],
    ['Power weapon', 15, 'pw'],
    ['Storm shield', 10, 'ssh'],
    ['Thunder hummer', 30, 'th'],
]

sh_tda_weapon_1w = [
    ['Chainfist', 30, 'chf'],  # TODO: with Terminator Armour only
    ['Lighting claw', 15, 'slc'],
    ['Power fist', 15, 'pf'],
    ['Power weapon', 10, 'pw'],
    ['Storm shield', 10, 'ssh'],
    ['Thunder hummer', 30, 'th'],
]

# two-handed
th_weapon = [
    ['Bolter', 1, 'blt'],  # TODO: not with Terminator Armour
    ['Combi-meltagun', 15, 'cmg'],
    ['Combi-plasma gun', 15, 'cpg'],
    ['Combi-flamer', 10, 'cf'],
    ['Storm bolter', 5, 'sb'],
]

th_weapon_1w = [
    ['Bolter', 1, 'blt'],  # TODO: not with Terminator Armour
    ['Combi-meltagun', 10, 'cmg'],
    ['Combi-plasma gun', 10, 'cpg'],
    ['Combi-flamer', 5, 'cf'],
    ['Storm bolter', 5, 'sb'],
]

th_tda_weapon = [
    ['Combi-meltagun', 15, 'cmg'],
    ['Combi-plasma gun', 15, 'cpg'],
    ['Combi-flamer', 10, 'cf'],
    ['Storm bolter', 5, 'sb'],
]

th_tda_weapon_1w = [
    ['Combi-meltagun', 10, 'cmg'],
    ['Combi-plasma gun', 10, 'cpg'],
    ['Combi-flamer', 5, 'cf'],
    ['Storm bolter', 5, 'sb'],
]

# wargear
wargear = [
    ['Auspex', 2, 'aus'],
    ['Bionics', 5, 'bio'],
    ['Combat shield', 10, 'csh'],  # TODO: not with Terminator Armour
    ['Frag grenades', 1, 'fg'],  # TODO: not with Terminator Armour
    ['Holy orb of Antioch', 10, 'hoa'],  # TODO: not with Terminator Armour
    ['Jump pack', 20, 'jp'],  # TODO: not with Terminator Armour
    ['Krak grenades', 2, 'kg'],  # TODO: not with Terminator Armour
    ['Master-crafted weapon', 15, 'mcw'],
    ['Melta bombs', 5, 'mb'],  # TODO: not with Terminator Armour
    ['Space Marine bike', 30, 'smb'],  # TODO: not with Terminator Armour
    ['Teleport homer', 5, 'tlh'],
    ['Terminator honours', 15, 'ths'],  # TODO: not with Terminator Armour
    ['Iron Halo', 25, 'ih'],  # TODO: not with Terminator Armour
    ['Adamantine mantle', 35, 'am'],
]

wargear_w1 = [
    ['Auspex', 2, 'aus'],
    ['Bionics', 5, 'bio'],
    ['Combat shield', 5, 'csh'],  # TODO: not with Terminator Armour
    ['Frag grenades', 1, 'fg'],  # TODO: not with Terminator Armour
    ['Krak grenades', 2, 'kg'],  # TODO: not with Terminator Armour
    ['Master-crafted weapon', 15, 'mcw'],
    ['Melta bombs', 5, 'mb'],  # TODO: not with Terminator Armour
    ['Teleport homer', 5, 'tlh'],
    ['Iron Halo', 25, 'ih'],  # TODO: not with Terminator Armour
    ['Adamantine mantle', 35, 'am'],
]

# wargear
tda_wargear = [
    ['Auspex', 2, 'aus'],
    ['Bionics', 5, 'bio'],
    ['Master-crafted weapon', 15, 'mcw'],
    ['Teleport homer', 5, 'tlh'],
    ['Adamantine mantle', 35, 'am'],
]

# #relics & artefacts
# relics = [  # TODO: single item for army only
#     ['Iron Halo', 25, 'ih'],  # TODO: not with Terminator Armour
#     ['Adamantine mantle', 35, 'am'],
# ]
#
# #relics & artefacts
# relics = [  # TODO: single item for army only
#     ['Chapter Banner', 25, 'cb'],  # TODO: Standard Bearer only
#     ['Holy relic', 30, 'hr'],  # TODO: Reclusiam Standard Bearer only
#     ['Iron Halo', 25, 'ih'],  # TODO: not with Terminator Armour
#     ['Sacred standard', 25, 'sst'],  # TODO: Standard Bearer only
#     ['Adamantine mantle', 35, 'am'],
# ]

#vehicle upgrades
vehicle = [
    ['Dozer blade', 5, 'db'],
    ['Extra armour', 5, 'ea'],
    ['Hunter-killer missile', 15, 'hkm'],
    ['Pintle-mounted storm bolter', 10, 'psb'],
    ['Power of the Machine Spirit', 30, 'pms'],
    ['Searchlight', 1, 'scl'],
    ['Smoke launchers', 3, 'sls'],
]
