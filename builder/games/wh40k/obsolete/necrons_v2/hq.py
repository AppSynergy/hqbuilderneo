__author__ = 'dante'

from builder.core2 import *


class CommandBarge(Unit):
    type_name = 'Catacomb Command Barge'
    type_id = 'catacombcommandbarge_v1'

    def __init__(self, parent):
        super(CommandBarge, self).__init__(parent=parent, points=80, gear=[Gear('Quantum shielding')])
        self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(CommandBarge.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Tesla Cannon', 0)
            self.variant('Gauss Cannon', 0)


class OverlordTransport(OptionalSubUnit):
    def __init__(self, parent):
        super(OverlordTransport, self).__init__(parent, 'Transport', order=1001)
        SubUnit(self, CommandBarge(parent=None))


class Imotekh(Unit):
    type_name = 'Imotekh the Stormlord'
    type_id = 'imotekhthestormlord_v1'

    def __init__(self, parent):
        super(Imotekh, self).__init__(parent=parent, points=225, unique=True, gear=[
            Gear('Bloodswarm Nanoscarabs'), Gear('Gauntlet of Fire'), Gear('Phase Shifter'), Gear('Phylactery'),
            Gear('Sempiternal Weave'), Gear('Staff of the Destroyer')
        ])
        OverlordTransport(self)


class Nemesor(Unit):
    type_name = 'Nemesor Zahndrekh'
    type_id = 'nemesorzahndrekh_v1'

    def __init__(self, parent):
        super(Nemesor, self).__init__(parent=parent, points=185, unique=True, gear=[
            Gear('Phase Shifter'), Gear('Resurrection Orb'), Gear('Sempiternal Weave'), Gear('Staff of Light')
        ])
        OverlordTransport(self)


class Vargard(Unit):
    type_name = 'Vargard Obyron'
    type_id = 'vargardobyron_v1'

    def __init__(self, parent):
        super(Vargard, self).__init__(parent=parent, points=160, unique=True, gear=[
            Gear('Ghostwalk mantle'), Gear('Sempiternal Weave'), Gear('Warscythe')
        ])


class Illuminor(Unit):
    type_name = 'Illuminor Szeras'
    type_id = 'illuminorszeras_v1'

    def __init__(self, parent):
        super(Illuminor, self).__init__(parent=parent, points=100, unique=True, gear=[Gear('Eldritche Lance'),
                                                                                      Gear('Gaze of Flame')])


class Orikan(Unit):
    type_name = 'Orikan the Diviner'
    type_id = 'orikanthediviner_v1'

    def __init__(self, parent):
        super(Orikan, self).__init__(parent=parent, points=165, unique=True, gear=[
            Gear('Phase Shifter'), Gear('Staff of Tomorrow'), Gear('Transdimensional Beamer')
        ])


class Anrakyr(Unit):
    type_name = 'Anrakyr the Traveller'
    type_id = 'anrakyrthetraveller_v1'

    def __init__(self, parent):
        super(Anrakyr, self).__init__(parent=parent, points=165, unique=True,
                                      gear=[Gear('Tachyon Arrow'), Gear('Warscythe')])
        OverlordTransport(self)


class Trazin(Unit):
    type_name = 'Trazin the Infinite'
    type_id = 'trazintheinfinite_v1'

    def __init__(self, parent):
        super(Trazin, self).__init__(parent=parent, points=175, unique=True,
                                     gear=[Gear('Empathic Obliterator'), Gear('Mindshackle scarabs')])
        OverlordTransport(self)


class Overlord(Unit):
    type_name = 'Necron Overlord'
    type_id = 'necronoverlord_v1'

    def __init__(self, parent):
        super(Overlord, self).__init__(parent=parent, points=90)
        self.Weapon(self)
        self.Options(self)
        OverlordTransport(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Overlord.Weapon, self).__init__(parent=parent, name='Weapon')
            self.staffoflight = self.variant('Staff of Light', 0)
            self.hyperphasesword = self.variant('Hyperphase Sword', 0)
            self.gauntletoffire = self.variant('Gauntlet of Fire', 5)
            self.voidblade = self.variant('Voidblade', 10)
            self.warscythe = self.variant('Warscythe', 10)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Overlord.Options, self).__init__(parent=parent, name='Options')
            self.phaeron = self.variant('Phaeron', 20)
            self.phylactery = self.variant('Phylactery', 15)
            self.mindshacklescarabs = self.variant('Mindshackle scarabs', 15)
            self.sempiternalweave = self.variant('Sempiternal Weave', 15)
            self.tesseractlabyrinth = self.variant('Tesseract labyrinth', 20)
            self.tachyonarrow = self.variant('Tachyon Arrow', 30)
            self.resurrectionorb = self.variant('Resurrection Orb', 30)
            self.phaseshifter = self.variant('Phase Shifter', 45)


class DestroyerLord(Unit):
    type_name = 'Destroyer Lord'
    type_id = 'destroyerlord_v1'

    def __init__(self, parent):
        super(DestroyerLord, self).__init__(parent=parent, points=125, gear=[])
        self.Weapon(self)
        self.Options(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DestroyerLord.Weapon, self).__init__(parent=parent, name='Weapon')
            self.warscythe = self.variant('Warscythe', 0)
            self.staffoflight = self.variant('Staff of Light', 0)
            self.gauntletoffire = self.variant('Gauntlet of Fire', 0)
            self.voidblade = self.variant('Voidblade', 5)

    class Options(OptionsList):
        def __init__(self, parent):
            super(DestroyerLord.Options, self).__init__(parent=parent, name='Options')
            self.sempiternalweave = self.variant('Sempiternal Weave', 15)
            self.mindshacklescarabs = self.variant('Mindshackle scarabs', 20)
            self.tachyonarrow = self.variant('Tachyon Arrow', 30)
            self.resurrectionorb = self.variant('Resurrection Orb', 30)


class Lord(ListSubUnit):
    type_name = 'Necron Lord'
    type_id = 'necronlord_v1'

    def __init__(self, parent):
        super(Lord, self).__init__(parent=parent, points=35)
        self.Weapon(self)
        self.Options(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Lord.Weapon, self).__init__(parent=parent, name='Weapon')
            self.staffoflight = self.variant('Staff of Light', 0)
            self.hyperphasesword = self.variant('Hyperphase Sword', 0)
            self.gauntletoffire = self.variant('Gauntlet of Fire', 5)
            self.voidblade = self.variant('Voidblade', 10)
            self.warscythe = self.variant('Warscythe', 10)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Lord.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.sempiternalweave = self.variant('Sempiternal Weave', 15)
            self.mindshacklescarabs = self.variant('Mindshackle scarabs', 15)
            self.tesseractlabyrinth = self.variant('Tesseract labyrinth', 20)
            self.resurrectionorb = self.variant('Resurrection Orb', 30)
            self.phaseshifter = self.variant('Phase Shifter', 45)


class Cryptek(ListSubUnit):
    type_name = 'Cryptek'
    type_id = 'cryptek_v1'

    def __init__(self, parent):
        super(Cryptek, self).__init__(parent=parent, points=25)
        self.weapon = self.Weapon(self)
        self.opt = self.Options(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Cryptek.Weapon, self).__init__(parent=parent, name='Weapon')
            self.staffoflight = self.variant('Staff of Light', 0)
            self.despair = self.variant('Abyssal Staff (Harbinger of Despair)', 5,
                                        gear=[Gear('Harbinger of Despair'), Gear('Abyssal Staff')])
            self.destruction = self.variant('Eldritch Lance (Harbinger of Destruction)', 10,
                                            gear=[Gear('Harbinger of Destruction'), Gear('Eldritch Lance')])
            self.eternity = self.variant('Aeonstave (Harbinger of Eternity)', 0,
                                         gear=[Gear('Harbinger of Eternity'), Gear('Aeonstave')])
            self.storm = self.variant('Voltaric Staff (Harbinger of the Storm)', 0,
                                      gear=[Gear('Harbinger of the Storm'), Gear('Voltaric Staff')])
            self.transmogrification = self.variant('Thermostave (Harbinger of Transmogrification)', 5,
                                                   gear=[Gear('Harbinger of Transmogrification'), Gear('Thermostave')])

    class Options(OptionsList):
        def __init__(self, parent):
            super(Cryptek.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.nightmareshroud = self.variant('Nightmare Shroud', 10)
            self.veilofdarkness = self.variant('Veil of Darkness', 30)
            self.gaseofflame = self.variant('Gase of Flame', 10)
            self.solarpulse = self.variant('Solar Pulse', 20)
            self.chronometron = self.variant('Chronometron', 15)
            self.timesplintercloak = self.variant('Timesplinter Cloak', 30)
            self.lightningshield = self.variant('Lightning Shield', 10)
            self.ethercrystal = self.variant('Ether Crystal', 20)
            self.seismiccrucible = self.variant('Seismic Crucible', 10)
            self.harpofdissonance = self.variant('Harp of Dissonance', 20)

        @staticmethod
        def _activate(opt, val):
            for o in opt:
                o.used = o.visible = val

        def activate_despair(self, val):
            self._activate([self.nightmareshroud, self.veilofdarkness], val)

        def activate_destruction(self, val):
            self._activate([self.gaseofflame, self.solarpulse], val)

        def activate_eternity(self, val):
            self._activate([self.chronometron, self.timesplintercloak], val)

        def activate_storm(self, val):
            self._activate([self.lightningshield, self.ethercrystal], val)

        def activate_transmogrification(self, val):
            self._activate([self.seismiccrucible, self.harpofdissonance], val)

    def check_rules(self):
        super(Cryptek, self).check_rules()
        self.opt.used = self.opt.visible = not self.weapon.staffoflight == self.weapon.cur
        self.opt.activate_despair(self.weapon.despair == self.weapon.cur)
        self.opt.activate_destruction(self.weapon.destruction == self.weapon.cur)
        self.opt.activate_eternity(self.weapon.eternity == self.weapon.cur)
        self.opt.activate_storm(self.weapon.storm == self.weapon.cur)
        self.opt.activate_transmogrification(self.weapon.transmogrification == self.weapon.cur)


class RoyalCourt(Unit):
    type_name = 'Royal Court'
    type_id = 'royal_court_v1'

    class Options(OptionsList):
        def __init__(self, parent):
            super(RoyalCourt.Options, self).__init__(parent=parent, name='', used=False)

            self.lords = self.variant(Lord.type_name)
            self.cryptek = self.variant(Cryptek.type_name)

    def __init__(self, parent):
        super(RoyalCourt, self).__init__(parent)
        self.opt = self.Options(self)
        self.lords = UnitList(self, Lord, 1, 5)
        self.cryptek = UnitList(self, Cryptek, 1, 5)

    def check_rules(self):
        super(RoyalCourt, self).check_rules()
        self.lords.visible = self.lords.used = self.opt.lords.value
        self.cryptek.visible = self.cryptek.used = self.opt.cryptek.value
        names = set()
        for u in self.cryptek.units:
            desc = u.opt.description
            if u.count > 1 and len(desc) > 0:
                for gear in desc:
                    self.error('{} must be unique in Royal Court'.format(gear.name))
            else:
                for gear in desc:
                    if gear.name in names:
                        self.error('{} must be unique in Royal Court'.format(gear.name))
                    names.add(gear.name)

    def get_count(self):
        return int(self.lords.used and self.lords.count) + int(self.cryptek.used and self.cryptek.count)
