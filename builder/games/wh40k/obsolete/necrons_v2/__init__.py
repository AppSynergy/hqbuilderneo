from builder.games.wh40k.obsolete.necrons import troops, heavy, fast, elites

__author__ = 'Denis Romanov'

from builder.games.wh40k.old_roster import Wh40k
from builder.games.wh40k.roster import Wh40kBase, CombinedArmsDetachment, AlliedDetachment, Wh40k7ed, HQSection, ElitesSection, TroopsSection, FastSection, HeavySection, Fort
from builder.games.wh40k.escalation.necron import LordsOfWar as EscLordsOfWar
from hq import *


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.imotekh = UnitType(self, Imotekh)
        self.nemesor = UnitType(self, Nemesor)
        self.vargard = UnitType(self, Vargard)
        UnitType(self, Illuminor)
        UnitType(self, Orikan)
        self.anrakyr = UnitType(self, Anrakyr)
        self.trazin = UnitType(self, Trazin)
        self.overlord = UnitType(self, Overlord)
        UnitType(self, DestroyerLord)
        self.court = UnitType(self, RoyalCourt, slot=0)
        self.overlords = [self.overlord, self.imotekh, self.nemesor, self.anrakyr, self.trazin]

    def check_limits(self):
        self.vargard.slot = 0.0 if self.nemesor.count > 0 else 1.0
        return super(BaseHQ, self).check_limits()


class HQ(BaseHQ):
    pass


Deathmarks = Unit.norm_core1_unit(elites.Deathmarks)
Lichguards = Unit.norm_core1_unit(elites.Lichguards)
Praetorians = Unit.norm_core1_unit(elites.Praetorians)
FlayedOne = Unit.norm_core1_unit(elites.FlayedOne)
Stalker = Unit.norm_core1_unit(elites.Stalker)
CTan = Unit.norm_core1_unit(elites.CTan)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.deathmarks = UnitType(self, Deathmarks)
        self.lichguards = UnitType(self, Lichguards)
        self.praetorians = UnitType(self, Praetorians)
        self.flayedone = UnitType(self, FlayedOne)
        self.stalker = UnitType(self, Stalker)
        self.ctan = UnitType(self, CTan)


class Elites(BaseElites):
    pass


Warriors = Unit.norm_core1_unit(troops.Warriors)
Immortals = Unit.norm_core1_unit(troops.Immortals)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.warriors = UnitType(self, Warriors)
        self.immortals = UnitType(self, Immortals)


Wraiths = Unit.norm_core1_unit(fast.Wraiths)
Scarabs = Unit.norm_core1_unit(fast.Scarabs)
TombBlades = Unit.norm_core1_unit(fast.TombBlades)
Destroyers = Unit.norm_core1_unit(fast.Destroyers)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        self.wraiths = UnitType(self, Wraiths)
        self.scarabs = UnitType(self, Scarabs)
        self.tombblades = UnitType(self, TombBlades)
        self.destroyers = UnitType(self, Destroyers)


class FastAttack(BaseFastAttack):
    pass


Ark = Unit.norm_core1_unit(heavy.Ark)
Barge = Unit.norm_core1_unit(heavy.Barge)
Monolith = Unit.norm_core1_unit(heavy.Monolith)
Scythe = Unit.norm_core1_unit(heavy.Scythe)
Spyders = Unit.norm_core1_unit(heavy.Spyders)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        self.ark = UnitType(self, Ark)
        self.barge = UnitType(self, Barge)
        self.monolith = UnitType(self, Monolith)
        self.scythe = UnitType(self, Scythe)
        self.spyders = UnitType(self, Spyders)


class HeavySupport(BaseHeavySupport):
    pass


class LordsOfWar(EscLordsOfWar):
    pass


#class NecronV2(Wh40k):
#    army_id = 'necron_v2'
#    army_name = 'Necron'
#
#    def __init__(self, secondary=False):
#        self.hq = HQ(parent=self)
#        self.elites = Elites(parent=self)
#        self.troops = Troops(parent=self)
#        self.fast = FastAttack(parent=self)
#        self.heavy = HeavySupport(parent=self)
#
#        super(necronv2, self).__init__(
#            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
#            fort=Fort(parent=self),
#            lords=LordsOfWar(parent=self),
#            ally=Ally(parent=self, ally_type=Ally.NEC),
#            secondary=secondary
#        )
#
#    def check_rules(self):
#        super(NecronV2, self).check_rules()
#        self.check_unit_limit(
#            count=sum(u.count for u in self.hq.overlords), units=self.hq.court,
#            message='You can\'t have more Royal Courts then Overlords (include unique ones)'
#        )
#

class NecronV2Base(Wh40kBase):
    army_id = 'necron_v2_base'
    army_name = 'Necron'

    def __init__(self):
        self.hq = HQ(parent=self)

        super(NecronV2Base, self).__init__(
            hq=self.hq, elites=Elites(parent=self), troops=Troops(parent=self), fast=FastAttack(parent=self), heavy=HeavySupport(parent=self),
            fort=Fort(parent=self),
            lords=LordsOfWar(self)
        )

    def check_rules(self):
        super(NecronV2Base, self).check_rules()
        self.check_unit_limit(
            count=sum(u.count for u in self.hq.overlords), units=self.hq.court,
            message='You can\'t have more Royal Courts then Overlords (include unique ones)'
        )

class NecronV2CAD(NecronV2Base, CombinedArmsDetachment):
    army_id = 'necron_c2_cad'
    army_name = 'Necron (Combined arms detachment)'

class NecronV2AD(NecronV2Base, AlliedDetachment):
    army_id = 'necron_v2_ad'
    army_name = 'Necron (Allied detachment)'

    
faction = 'Necron'

class NecronV2(Wh40k7ed):
    army_id = 'necron_v2'
    army_name = 'Necron'
    obsolete = True
    faction=faction

    def __init__(self):
        super(NecronV2, self).__init__([NecronV2CAD])

detachments = [NecronV2CAD, NecronV2AD]
