__author__ = 'Denis Romanov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.obsolete.necrons.hq import *
from builder.games.wh40k.obsolete.necrons.elites import *
from builder.games.wh40k.obsolete.necrons.troops import *
from builder.games.wh40k.obsolete.necrons.fast import *
from builder.games.wh40k.obsolete.necrons.heavy import *


class Necrons(LegacyWh40k):
    army_id = '0d12f7f90ef6412ebcab37c397fd3b6c'
    army_name = 'Necrons'
    obsolete = True

    def __init__(self, secondary=False):
        from builder.games.wh40k.old_roster import Ally
        LegacyWh40k.__init__(
            self,
            hq=[Imotekh, Nemesor, Vargard, Illuminor, Orikan, Anrakyr, Trazin,
                Overlord,
                # dict(unit=Overlord, deprecated=True),
                # dict(unit=Overlord_v2, type_id='overlordv2'),
                DestroyerLord],
            elites=[Deathmarks, Lichguards, Praetorians, FlayedOne, Stalker, CTan],
            troops=[Warriors, Immortals],
            fast=[Wraiths, Scarabs, TombBlades, Destroyers],
            heavy=[Ark, Barge, Monolith, Scythe, Spyders],
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.NEC)
        )

        def check_hq_limit():
            count = len(self.hq.get_units())
            if self.hq.count_unit(Nemesor) == 1 and self.hq.count_unit(Vargard) == 1:
                count -= 1
            return self.hq.min <= count <= self.hq.max
        self.hq.check_limits = check_hq_limit

    def check_rules(self):
        pass

