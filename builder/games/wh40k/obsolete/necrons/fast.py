__author__ = 'Denis Romanov'

from builder.core.unit import Unit, ListUnit, ListSubUnit
from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor


class Wraiths(ListUnit):
    name = "Canoptek Wraiths"

    class Wraith(ListSubUnit):
        name = "Canoptek Wraith"
        base_points = 35
        gear = ['Phase shifter']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.opt = self.opt_options_list('Options', [
                ['Particle caster', 5],
                ['Whip coils', 10],
                ['Transdimensional beamer', 15],
            ])

    def __init__(self):
        ListUnit.__init__(self, self.Wraith, 1, 6)


class Scarabs(Unit):
    name = "Canoptek Scarabs"
    base_points = 15

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Scarabs', 3, 10, self.base_points)

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]))
        self.build_description(exclude=[self.count.id])


class TombBlades(Unit):
    name = "Tomb Blades"
    base_points = 20

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Blades', 1, 5, self.base_points)
        self.wep = self.opt_one_of('Weapon', [
            ['Twin-linked Tesla Carbine', 0],
            ['Twin-linked Gauss Blaster', 0],
            ['Particle Beamer', 10],
        ])
        self.opt = self.opt_options_list('Options', [
            ["Nebuloscope", 5],
            ["Shadowloom", 10],
            ["Shield Vane", 10],
        ])

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]))
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Tomb Blade', self.count.get(), 20).add_gear_opt(self.wep)
            .add_gear_opt(self.opt).build(self.count.get()))


class Destroyers(Unit):
    name = "Necron Destroyers"
    base_points = 40

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Destroyers', 1, 5, self.base_points)
        self.wep = self.opt_count('Heavy Gauss Cannon', 0, 3, 20)

    def check_rules(self):
        norm_counts(0, min(self.count.get(), 3), [self.wep])
        self.points.set(self.count.points() + self.wep.points())
        self.build_description(options=[])
        desc = ModelDescriptor('Necron Destroyer', points=40)
        self.description.add(desc.clone().add_gear('Heavy Gauss Cannon').build(self.wep.get()))
        self.description.add(desc.clone().add_gear('Gauss Cannon', points=20).build(self.count.get() - self.wep.get()))
