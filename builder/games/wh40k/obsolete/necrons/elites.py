__author__ = 'Denis Romanov'

from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.necrons.troops import NightScythe


class Deathmarks(Unit):
    name = "Deathmarks"
    base_points = 19
    base_gear = ['Synaptic disintegrator']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Deathmarks', 5, 10, self.base_points)
        self.trans = self.opt_options_list('Transport', [
            [NightScythe.name, NightScythe.base_points, 'ns']
        ], limit=1)
        self.ns_unit = self.opt_sub_unit(NightScythe())

    def check_rules(self):
        self.ns_unit.set_active(self.trans.get('ns'))
        self.set_points(self.build_points(exclude=[self.trans.id], count=1, base_points=0))
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Deathmark', points=self.base_points,
                                             gear=self.base_gear).build(self.get_count()))
        self.description.add(self.ns_unit.get_selected())


class Lichguards(Unit):
    name = "Lichguards"
    base_points = 40

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Lichguards', 5, 10, self.base_points)
        self.wep = self.opt_one_of('Weapon', [
            ['Warscythe', 0],
            ['Hyperphase sword and dispersion shield', 5],
        ])
        self.trans = self.opt_options_list('Transport', [
            [NightScythe.name, NightScythe.base_points, 'ns']
        ], limit=1)
        self.ns_unit = self.opt_sub_unit(NightScythe())

    def check_rules(self):
        self.ns_unit.set_active(self.trans.get('ns'))

        self.set_points((self.base_points + self.wep.points()) * self.count.get() + self.ns_unit.points())
        self.build_description(options=[])
        desc = ModelDescriptor('Lichguard', points=self.base_points)
        if self.wep.get_selected() == 'Warscythe':
            desc.add_gear('Warscythe')
        else:
            desc.add_gear('Hyperphase sword', points=5).add_gear('Dispersion shield')
        self.description.add(desc.build(self.get_count()))
        self.description.add(self.ns_unit.get_selected())


class Praetorians(Unit):
    name = "Triarch Praetorians"
    base_points = 40

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Praetorians', 5, 10, self.base_points)
        self.wep = self.opt_one_of('Weapon', [
            ['Rod of covenant', 0],
            ['Voidblade and particle caster', 0],
        ])

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]))
        self.build_description(options=[])
        desc = ModelDescriptor('Triarch Praetorian', points=self.base_points)
        if self.wep.get_selected() == 'Rod of covenant':
            desc.add_gear('Rod of covenant')
        else:
            desc.add_gear('Voidblade').add_gear('Particle caster')
        self.description.add(desc.build(self.get_count()))


class CTan(Unit):
    name = "C'Tan"
    base_points = 185
    gear = ['Necrodermis']

    def __init__(self):
        Unit.__init__(self)
        powers = [
            ['Entropic Touch', 10],
            ['Lord of Fire', 10],
            ['Pyreshards', 15],
            ['Swarm of Spirit Dust', 20],
            ['Moulder of Worlds', 25],
            ['Sentient Singularity', 30],
            ['Writhing Worldscape', 35],
            ['Grand Illusion', 40],
            ["Time's Arrow", 40],
            ['Transdimesional Thunderbolt', 45],
            ['Gaze of Death', 50],
        ]
        self.pow1 = self.opt_one_of('Manifestations of Power', powers, id='pow1')
        self.pow2 = self.opt_one_of('', powers, id='pow2')

    def check_rules(self):
        self.pow1.set_active_all(True)
        self.pow2.set_active_all(True)
        self.pow2.set_active_options([self.pow1.get_cur()], False)
        self.pow1.set_active_options([self.pow2.get_cur()], False)
        self.points.set(self.base_points + self.pow1.points() + self.pow2.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear)
        self.description.add(self.pow1.get_selected())
        self.description.add(self.pow2.get_selected())
        self.set_description(self.description)

    def get_unique_gear(self):
        return [self.pow1.get_selected(), self.pow2.get_selected()]


class FlayedOne(Unit):
    name = "Flayed One Pack"
    base_points = 13

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Flayed One', 5, 20, self.base_points)

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]))
        self.build_description(options=[])
        desc = ModelDescriptor('Flayed One', points=self.base_points)
        self.description.add(desc.build(self.get_count()))


class Stalker(Unit):
    name = "Triarch Stalker"
    base_points = 150
    gear = ['Quantum shielding']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Heat Ray', 0],
            ['Particle Shredder', 5],
            ['Twin-linked Gauss Cannon', 15],
        ])
