from builder.games.wh40k.obsolete.eldar import fast, troops, elites, heavy
from builder.games.wh40k.roster import  Wh40kBase, CombinedArmsDetachment, AlliedDetachment, Wh40k7ed, HQSection, ElitesSection, TroopsSection, FastSection, HeavySection, Fort

from imperial_armour.volume11.eldar import IA11HQSection, IA11FastSection, IA11HeavySection,\
    IA11LordsOfWar
from builder.games.wh40k.escalation.eldar import LordsOfWar as EscLordsOfWar
from builder.games.wh40k.obsolete.eldar_v2.hq import *


__author__ = 'Denis Romanov'


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.eldrad = UnitType(self, Eldrad)
        self.uriel = UnitType(self, Uriel)
        self.illic = UnitType(self, Illic)
        self.asurmen = UnitType(self, Asurmen)
        self.jainzar = UnitType(self, JainZar)
        self.karandras = UnitType(self, Karandras)
        self.fuegan = UnitType(self, Fuegan)
        self.baharroth = UnitType(self, Baharroth)
        self.maugan = UnitType(self, Maugan)
        self.avatar = UnitType(self, Avatar)
        self.autarch = UnitType(self, Autarch)
        self.farseer = UnitType(self, Farseer)
        self.spiritseer = UnitType(self, Spiritseer)
        self.warlocks = UnitType(self, Warlocks, slot=0)

    def check_limits(self):
        self.spiritseer.slot = 0.2 if self.roster.is_iyanden else 1.0
        return super(BaseHQ, self).check_limits()


class HQ(IA11HQSection, BaseHQ):
    pass


Dragons = Unit.norm_core1_unit(elites.Dragons)
Scorpions = Unit.norm_core1_unit(elites.Scorpions)
Banshees = Unit.norm_core1_unit(elites.Banshees)
Wraithguard = Unit.norm_core1_unit(elites.Wraithguard)
Wraithblades = Unit.norm_core1_unit(elites.Wraithblades)
Harlequins = Unit.norm_core1_unit(elites.Harlequins)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.dragons = UnitType(self, Dragons)
        self.scorpions = UnitType(self, Scorpions)
        self.banshees = UnitType(self, Banshees)
        self.wraithguard = UnitType(self, Wraithguard)
        self.wraithblades = UnitType(self, Wraithblades)
        self.harlequins = UnitType(self, Harlequins)


class Elites(BaseElites):
    pass


Guardians = Unit.norm_core1_unit(troops.Guardians)
StormGuardians = Unit.norm_core1_unit(troops.StormGuardians)
Avengers = Unit.norm_core1_unit(troops.Avengers)
BikerGuardians = Unit.norm_core1_unit(troops.BikerGuardians)
Rangers = Unit.norm_core1_unit(troops.Rangers)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.guardians = UnitType(self, Guardians)
        self.stormguardians = UnitType(self, StormGuardians)
        self.avengers = UnitType(self, Avengers)
        self.bikerguardians = UnitType(self, BikerGuardians)
        self.rangers = UnitType(self, Rangers)
        self.wraithguard = UnitType(self, Wraithguard, active=False)
        self.wraithblades = UnitType(self, Wraithblades, active=False)


CrimsonHunter = Unit.norm_core1_unit(fast.CrimsonHunter)
VyperSquadron = Unit.norm_core1_unit(fast.VyperSquadron)
Hemlock = Unit.norm_core1_unit(fast.Hemlock)
Spiders = Unit.norm_core1_unit(fast.Spiders)
Spears = Unit.norm_core1_unit(fast.Spears)
Hawks = Unit.norm_core1_unit(fast.Hawks)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        self.crimsonhunter = UnitType(self, CrimsonHunter)
        self.vypersquadron = UnitType(self, VyperSquadron)
        self.hemlock = UnitType(self, Hemlock)
        self.spiders = UnitType(self, Spiders)
        self.spears = UnitType(self, Spears)
        self.hawks = UnitType(self, Hawks)


class FastAttack(IA11FastSection, BaseFastAttack):
    pass


Falcon = Unit.norm_core1_unit(heavy.Falcon)
WalkerSquadron = Unit.norm_core1_unit(heavy.WalkerSquadron)
Battery = Unit.norm_core1_unit(heavy.Battery)
FirePrism = Unit.norm_core1_unit(heavy.FirePrism)
NightSpinner = Unit.norm_core1_unit(heavy.NightSpinner)
Wraithlord = Unit.norm_core1_unit(heavy.Wraithlord)
Wraithknight = Unit.norm_core1_unit(heavy.Wraithknight)
DarkReapers = Unit.norm_core1_unit(heavy.DarkReapers)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        self.falcon = UnitType(self, Falcon)
        self.walkersquadron = UnitType(self, WalkerSquadron)
        self.battery = UnitType(self, Battery)
        self.fireprism = UnitType(self, FirePrism)
        self.nightspinner = UnitType(self, NightSpinner)
        self.wraithlord = UnitType(self, Wraithlord)
        self.wraithknight = UnitType(self, Wraithknight)
        self.darkreapers = UnitType(self, DarkReapers)


class HeavySupport(IA11HeavySection, BaseHeavySupport):
    pass


class LordsOfWar(IA11LordsOfWar, EscLordsOfWar):
    pass


#class EldarV2(Wh40k):
#    army_id = 'eldar_v2'
#    army_name = 'Eldar'
#
#    class SupplementOptions(OneOf):
#        def __init__(self, parent):
#            super(EldarV2.SupplementOptions, self).__init__(parent=parent, name='Codex')
#            self.base = self.variant(name=EldarV2.army_name)
#            self.iyanden = self.variant(name='Iyanden')
#
#    def __init__(self, secondary=False):
#        self.hq = HQ(parent=self)
#        self.elites = Elites(parent=self)
#        self.troops = Troops(parent=self)
#        self.fast = FastAttack(parent=self)
#        self.heavy = HeavySupport(parent=self)
#
#        super(EldarV2, self).__init__(
#            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
#            fort=Fort(parent=self),
#            lords=LordsOfWar(parent=self),
#            ally=Ally(parent=self,ally_type=Ally.ELD),
#            secondary=secondary
#        )
#
#        self.codex = self.SupplementOptions(self)
#
#    @property
#    def is_base(self):
#        return self.codex.cur == self.codex.base
#
#    @property
#    def is_iyanden(self):
#        return self.codex.cur == self.codex.iyanden
#
#    def check_rules(self):
#        super(EldarV2, self).check_rules()
#        self.move_units(self.hq.spiritseer.count > 0, self.elites.wraithblades, self.troops.wraithblades)
#        self.move_units(self.hq.spiritseer.count > 0, self.elites.wraithguard, self.troops.wraithguard)
#
#        if self.fast.spectres.count == 0 and any(aut.spectre for aut in self.hq.autarch.units):
#            self.error('You can\'t select Shadow Spectre gear for Autarch without Shadow Spectre unit')
#
#        if self.hq.belannath.count > 0 and self.hq.farseer.count + self.hq.eldrad.count > 0:
#            self.error("If Farseer Bel-Annath is part of the army, no other Farseers may be included.")
#
#        if self.hq.wraithseer.count > 0:
#            if self.elites.wraithguard.count + self.troops.wraithguard.count < 1:
#                self.error("If Wraithseer is part of the army, it must also incluide at least one unit of "
#                           "wraithguard.")
#            if len(self.hq.units) - (self.hq.warlocks.count + self.hq.wraithseer.count) <= 0:
#                    self.error("You must always include another non-Wraithseer unit in HQ")
#
#    def has_illic(self):
#        return self.hq.illic.count > 0

class EldarV2Base(Wh40kBase):
    army_id = 'eldar_v2_base'
    army_name = 'Eldar'

    class SupplementOptions(OneOf):
        def __init__(self, parent):
            super(EldarV2Base.SupplementOptions, self).__init__(parent=parent, name='Codex')
            self.base = self.variant(name=EldarV2Base.army_name)
            self.iyanden = self.variant(name='Iyanden')

    def __init__(self):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)

        super(EldarV2Base, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self)
        )

        self.codex = self.SupplementOptions(self)

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_iyanden(self):
        return self.codex.cur == self.codex.iyanden

    def check_rules(self):
        super(EldarV2Base, self).check_rules()
        self.move_units(self.hq.spiritseer.count > 0, self.elites.wraithblades, self.troops.wraithblades)
        self.move_units(self.hq.spiritseer.count > 0, self.elites.wraithguard, self.troops.wraithguard)

        if self.fast.spectres.count == 0 and any(aut.spectre for aut in self.hq.autarch.units):
            self.error('You can\'t select Shadow Spectre gear for Autarch without Shadow Spectre unit')

        if self.hq.belannath.count > 0 and self.hq.farseer.count + self.hq.eldrad.count > 0:
            self.error("If Farseer Bel-Annath is part of the army, no other Farseers may be included.")

        if self.hq.wraithseer.count > 0:
            if self.elites.wraithguard.count + self.troops.wraithguard.count < 1:
                self.error("If Wraithseer is part of the army, it must also incluide at least one unit of "
                           "wraithguard.")
            if len(self.hq.units) - (self.hq.warlocks.count + self.hq.wraithseer.count) <= 0:
                    self.error("You must always include another non-Wraithseer unit in HQ")

    def has_illic(self):
        return self.hq.illic.count > 0

class EldarV2CAD(EldarV2Base, CombinedArmsDetachment):
    army_id = 'eldar_v2_cad'
    army_name = 'Eldar (Combined arms detachment)'

class EldarV2AD(EldarV2Base, AlliedDetachment):
    army_id = 'eldar_v2_ad'
    army_name = 'Eldar (Allied detachment)'

faction = 'Eldar'

class EldarV2(Wh40k7ed):
    army_id = 'eldar_v2'
    army_name = 'Eldar'
    faction = faction
    obsolete = True

    def __init__(self):
        super(EldarV2, self).__init__([EldarV2CAD])

detachments = [EldarV2CAD, EldarV2AD]
