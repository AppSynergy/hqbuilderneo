__author__ = 'Denis Romanov'

from builder.core2 import *
from builder.games.wh40k.obsolete.eldar import hq

Eldrad = Unit.norm_core1_unit(hq.Eldrad)
Uriel = Unit.norm_core1_unit(hq.Uriel)
Illic = Unit.norm_core1_unit(hq.Illic)
Asurmen = Unit.norm_core1_unit(hq.Asurmen)
JainZar = Unit.norm_core1_unit(hq.JainZar)
Karandras = Unit.norm_core1_unit(hq.Karandras)
Fuegan = Unit.norm_core1_unit(hq.Fuegan)
Baharroth = Unit.norm_core1_unit(hq.Baharroth)
Maugan = Unit.norm_core1_unit(hq.Maugan)
Avatar = Unit.norm_core1_unit(hq.Avatar)
Warlocks = Unit.norm_core1_unit(hq.Warlocks)


class ShurikenPistol(OneOf):
    def __init__(self, parent, name):
        super(ShurikenPistol, self).__init__(parent=parent, name=name)
        self.shurikenpistol = self.variant('Shuriken pistol', 0)


class Witchblade(OneOf):
    def __init__(self, parent, name):
        super(Witchblade, self).__init__(parent=parent, name=name)
        self.witchblade = self.variant('Witchblade', 0)
        self.singingspear = self.variant('Singing spear', 5)


class Witchstaff(OneOf):
    def __init__(self, parent, name):
        super(Witchstaff, self).__init__(parent=parent, name=name)
        self.witchstaff = self.variant('Witch staff', 0)


class RelicWeapon(OneOf):
    def __init__(self, *args, **kwargs):
        psyker = kwargs.pop('psyker', False)
        super(RelicWeapon, self).__init__(*args, **kwargs)
        self.remnants = [
            self.variant('Uldanorethi Long Rifle', 25),
            self.variant('Faolchu\'s Wing', 30),
            self.variant('Firesabre', 30),
            self.variant('Shard of Anaris', 40),
        ]

        self.gifts = [
            self.variant('The Celestial Lance', 35),
            self.variant('Soulshrive', 30)
        ]
        if psyker:
            self.gifts.append(self.variant('Spear of Teuthlas', 15))

    @property
    def has_gifts(self):
        return self.cur in self.gifts

    @property
    def has_remnants(self):
        return self.cur in self.remnants

    def activate_gifts(self, val):
        for o in self.gifts:
            o.active = val

    def activate_remnants(self, val):
        for o in self.remnants:
            o.active = val

    def show_gifts(self, val):
        for o in self.gifts:
            o.visible = val
            o.used = val and o.active


class Relic(OptionsList):
    def __init__(self, parent, seer=False):
        super(Relic, self).__init__(parent=parent, name='', limit=None)

        self.remnants = [
            self.variant('The Phoenix Gem', 25),
            self.variant('Mantle of the Laughing God', 40),
        ]
        if seer:
            self.remnants.append(self.variant('The Spirit Stone of Anath\'lan', 15))
        self.gifts = [
            self.variant('The Wraithforge Stone', 30),
            self.variant('Guardian Helm of Xellethon', 15)
        ]

    def activate_gifts(self, val):
        for o in self.gifts:
            o.active = val
            o.used = val and o.visible

    def activate_remnants(self, val):
        for o in self.remnants:
            o.active = val
            o.used = val and o.visible

    def show_gifts(self, val):
        for o in self.gifts:
            o.visible = val
            o.used = val and o.active

    @property
    def has_gifts(self):
        return any(o.value for o in self.gifts if o.used)

    @property
    def has_remnants(self):
        return any(o.value for o in self.remnants)


class Gifted(Unit):
    def __init__(self, *args, **kwargs):
        super(Gifted, self).__init__(*args, **kwargs)
        self.relics = []

    @property
    def has_gifts(self):
        return any(relic.has_gifts for relic in self.relics)

    @property
    def has_remnants(self):
        return any(relic.has_remnants for relic in self.relics)

    def check_rules(self):
        super(Gifted, self).check_rules()
        for o in self.relics:
            o.show_gifts(self.roster.is_iyanden)
            o.activate_remnants(not self.has_gifts or not self.roster.is_iyanden)
            o.activate_gifts(not self.has_remnants)


class Autarch(Gifted):
    type_name = 'Autarch'
    type_id = 'autarch_v1'

    class RelicWeapon(RelicWeapon, ShurikenPistol):
        pass

    def __init__(self, parent):
        super(Autarch, self).__init__(parent=parent, points=70, gear=[Gear('Plasma grenades'), Gear('Haywire grenades'),
                                                                      Gear('Forcefield'), Gear('Heavy Aspect armour')])

        self.opt = self.Options(self)
        self.wep1 = self.Weapon(self)
        self.wep2 = self.RelicWeapon(self, name='')
        self.relics = [self.wep2, Relic(self)]

    class Options(OptionsList):
        def __init__(self, parent):
            super(Autarch.Options, self).__init__(parent=parent, name='Options')

            self.swoopinghawkwings = self.variant('Swooping Hawk wings', 15)
            self.warpjumpgenerator = self.variant('Warp jump generator', 15)
            self.eldarjetbike = self.variant('Eldar jetbike', 15)
            self.shadowspect = self.variant('Shadow Spectre jet pack and Spectre holo-field', 20,
                                            gear=[Gear('Jet pack'), Gear('Spectre holo-field')])  # IA 11

            self.bansheemask = self.variant('Banshee mask', 5)
            self.mandiblasters = self.variant('Mandiblasters', 10)

        def check_rules(self):
            super(Autarch.Options, self).check_rules()
            self.process_limit([self.bansheemask, self.mandiblasters], 1)
            self.process_limit([self.shadowspect, self.swoopinghawkwings, self.eldarjetbike, self.warpjumpgenerator], 1)
            self.shadowspect.visible = self.shadowspect.used = self.roster.ia_enabled

        @property
        def has_bike(self):
            return self.eldarjetbike.value

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Autarch.Weapon, self).__init__(parent=parent, name='Weapon', limit=2)

            self.scorpionchainsword = self.variant('Scorpion chainsword', 3)
            self.avengershurikencatapult = self.variant('Avenger shuriken catapult', 5)
            self.lasblaster = self.variant('Lasblaster', 5)
            self.deathspinner = self.variant('Deathspinner', 10)
            self.fusiongun = self.variant('Fusion gun', 10)
            self.laserlance = self.variant('Laser lance', 10)
            self.powerweapon = self.variant('Power weapon', 15)
            self.reaperlauncher = self.variant('Reaper launcher with starswarm missiles', 25)
            self.prismrifle = self.variant('Prism rifle', 10)  # IA 11

        def check_rules(self):
            super(Autarch.Weapon, self).check_rules()
            self.laserlance.active = self.laserlance.used = self.parent.opt.has_bike
            self.prismrifle.visible = self.prismrifle.used = self.roster.ia_enabled

    @property
    def spectre(self):
        return (self.opt.shadowspect.used and self.opt.shadowspect.value) or \
               (self.wep1.prismrifle.used and self.wep1.prismrifle.value)


class Farseer(Gifted):
    type_name = 'Farseer'
    type_id = 'farseer_v1'

    class RelicWeapon1(RelicWeapon, Witchblade):
        pass

    class RelicWeapon2(RelicWeapon, ShurikenPistol):
        pass

    def __init__(self, parent):
        super(Farseer, self).__init__(parent=parent, points=100, gear=[Gear('Ghosthelm'), Gear('Rune armour')])
        self.wep1 = self.RelicWeapon1(self, 'Weapon', psyker=True)
        self.wep2 = self.RelicWeapon2(self, '', psyker=True)
        self.opt = self.Options(self)
        self.relic = Relic(self, seer=True)

        self.relics = [self.wep1, self.wep2, self.relic]

    class Options(OptionsList):
        def __init__(self, parent):
            super(Farseer.Options, self).__init__(parent=parent, name='Options')
            self.variant('Runes of warding', 10)
            self.variant('Runes of witnessing', 15)
            self.variant('Eldar jetbike', 15)

    def check_rules(self):
        super(Farseer, self).check_rules()
        for r1, r2 in zip(self.wep1.gifts + self.wep1.remnants, self.wep2.gifts + self.wep2.remnants):
            if self.wep1.cur == r1 and self.wep2.cur == r2:
                self.error('{} is unique gear'.format(r1.title))


class Spiritseer(Gifted):
    type_name = 'Spiritseer'
    type_id = 'spiritseer_v1'

    class RelicWeapon1(RelicWeapon, Witchstaff):
        pass

    class RelicWeapon2(RelicWeapon, ShurikenPistol):
        pass

    def __init__(self, parent):
        super(Spiritseer, self).__init__(parent=parent, points=70, gear=[Gear('Rune armour')])
        self.wep1 = self.RelicWeapon1(self, 'Weapon', psyker=True)
        self.wep2 = self.RelicWeapon2(self, '', psyker=True)
        self.relic = Relic(self, seer=True)
        self.relics = [self.wep1, self.wep2, self.relic]

    def check_rules(self):
        super(Spiritseer, self).check_rules()
        for r1, r2 in zip(self.wep1.gifts + self.wep1.remnants, self.wep2.gifts + self.wep2.remnants):
            if self.wep1.cur == r1 and self.wep2.cur == r2:
                self.error('{} is unique gear'.format(r1.title))
