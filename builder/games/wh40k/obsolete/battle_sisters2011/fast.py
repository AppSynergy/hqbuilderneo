__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit
from builder.games.wh40k.obsolete.battle_sisters2011.troops import Rhino,Immolator
class Seraphims(Unit):
    name = 'Seraphim Squad'
    gear = ['Power armour','Frag grenades','Krak grenades','Jump pack']
    class Superior(Unit):
        name = 'Seraphim Superior'
        gear = ['Power armour','Frag grenades','Krak grenades','Jump pack']
        base_points = 80 - 15 * 4
        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('First weapon', [
                ['Bolt pistol',0,'bpist'],
                ['Chainsword',0,'csw'],
                ['Power sword',10,'psw'],
                ['Evisecrator',25,'evsc']
            ])
            self.wep2 = self.opt_one_of('Second weapon',[
                ['Bolt pistol',0,'bpist'],
                ['Plasma pistol',15,'ppist']
            ])
            self.opt = self.opt_options_list('Options',[
                ['Melta bombs',5,'mbomb']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.squad = self.opt_count('Seraphim',4,9,15)
        self.sup = self.opt_sub_unit(self.Superior())
        self.spec = [
            self.opt_one_of('Special weapon',[
                ['Two bolt pistols',0,'bpistp'],
                ['Two hand flamers',20,'flamep'],
                ['Two inferno pistols',30,'ipistp']
            ]) for i in range(0,2)]

    def check_rules(self):
        self.set_points(self.build_points(count=1))
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.squad.get())
        scount = 0
        for i in range(0,2):
            if self.spec[i].get_cur() != 'bpistp':
                scount = scount + 1
                self.description.add(self.spec[i].get_selected())
        self.description.add('Two bolt pistols',self.squad.get() - scount)
        self.description.add(self.sup.get_selected())

    def get_count(self):
        return self.squad.get() + 1


class Dominions(Unit):
    name = 'Dominion Squad'
    class Dominion(ListSubUnit):
        max = 9
        name = 'Dominion'
        base_points = 13
        gear = ['Power armour','Frag grenades','Krak grenades','Bolt pistol']
        def __init__(self):
            ListSubUnit.__init__(self)
            self.hlist = [
                ['Storm Bolter',3,'sbgun'],
                ['Flamer', 5, 'flame' ],
                ['Meltagun', 10, 'mgun' ]
            ]

            self.wep = self.opt_one_of('Weapon', [['Boltgun',0,'bgun']] + self.hlist)
            self.flag = self.opt_options_list('Options',[['Simulacrum imperialis',20,'simp']])
            self.have_flag = False
            self.have_hvy = False

        def has_hvy(self):
            return self.wep.get_cur() != 'bgun'

        def check_rules(self):
            if self.wep.get_cur() == 'bgun':
                self.wep.set_active_options([w[2] for w in self.hlist], not self.have_hvy)
            if not self.flag.get('simp'):
                self.flag.set_active_options(self.flag.get_all_ids(), not self.have_flag)
            ListSubUnit.check_rules(self)
    class Superior(Unit):
        name = 'Dominion Superior'
        gear = ['Power armour','Frag grenades','Krak grenades']
        base_points = 80 - 15 * 4
        def __init__(self):
            Unit.__init__(self)
            weaponlist = [
                ['Chainsword',0,'chsw'],
                ['Storm Bolter',3,'sbgun'],
                ['Power sword',10,'psw'],
                ['Combi-melta', 10, 'cmelta' ],
                ['Combi-flamer', 10, 'cflame' ],
                ['Combi-plasma', 10, 'cplasma' ],
                ['Condemnor boltgun',15,'cbgun'],
                ['Plasma pistol',15,'ppist']
            ]
            self.wep1 = self.opt_one_of('Weapon', [['Boltgun',0,'bgun']] + weaponlist)
            self.wep2 = self.opt_one_of('Weapon',[['Bolt pistol',0,'bpist']] + weaponlist)
            self.opt = self.opt_options_list('Options',[
                ['Melta bombs',5,'mbomb']
            ])
    def __init__(self):
        Unit.__init__(self)
        self.squad = self.opt_units_list(self.Dominion.name,self.Dominion,4,9)
        self.sup = self.opt_sub_unit(self.Superior())
        self.transport = self.opt_optional_sub_unit('Transport', [Rhino(), Immolator()], id='trans')
    def check_rules(self):
        self.squad.update_range()
        flag = reduce(lambda val, c: val + (1 if c.flag.get('simp') else 0), self.squad.get_units(), 0)
        if flag > 1:
            self.error('Only one Simulacrum imperialis can be carried by squad of Seraphims.')
        hlim = 2 * int((1 + self.squad.get_count())/5)
        hvy = reduce(lambda val, c: val + (1 if c.has_hvy() else 0), self.squad.get_units(), 0)
        if hvy > hlim:
            self.error('No more then 2 for every 5 models in squad can carry special weapons.')

        for c in self.squad.get_units():
            c.have_flag = (flag > 0)
            self.have_hvy = hvy >= hlim
            c.check_rules()
        self.points.set(self.build_points(count=1))
        self.build_description(count=1)
    def get_count(self):
        return self.squad.get_count() + 1
