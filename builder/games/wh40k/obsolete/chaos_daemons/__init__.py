__author__ = 'Ivan Truskov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.old_roster import Ally
from builder.games.wh40k.obsolete.chaos_daemons.hq import *
from builder.games.wh40k.obsolete.chaos_daemons.elites import *
from builder.games.wh40k.obsolete.chaos_daemons.troops import *
from builder.games.wh40k.obsolete.chaos_daemons.fast import *
from builder.games.wh40k.obsolete.chaos_daemons.heavy import *


class ChaosDaemons(LegacyWh40k):
    army_id = 'c1ccef758684433da1d929d587676ca2'
    army_name = 'Chaos Daemons'
    obsolete = True
    heralds = [Karanak, Epidemius, BlueScribes, Changeling, Skulltaker, HKhorne, HTzeentch, HNurgle, HSlaanesh]
    godList = {'kh':'Khorne', 'tz':'Tzeentch', 'ng':'Nurgle', 'sl':'Slaanesh'}

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(self,
            hq = [Kugath, Fateweaver, Scarbrand, Keeper, Unclean, Bloodthirster, LordOfChange, DaemonPrince, Masque,
                  Karanak, Epidemius, BlueScribes, Changeling, Skulltaker, HKhorne, HTzeentch, HNurgle, HSlaanesh],
            elites = [Fiends, Flamers, Crushers, Beasts],
            troops = [Letters, Daemonettes, Bearers, Horrors, Nurglings],
            fast = [Hounds, Seekers, Drones, Screamers, Fury, Hellflayer],
            heavy = [SoulGrinder, {'unit':DaemonPrince, 'active':False}, SkullCannon, BurningChariot, SeekerCavalcade],
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.CD)
        )

        def check_hq_limit():
            count = len(self.hq.get_units())

            h_count = self.hq.count_units(self.heralds)
            return self.hq.min <= count - h_count + int((3 + h_count)/4) <= self.hq.max
        self.hq.check_limits = check_hq_limit

    def check_rules(self):
        if self.hq.count_units(self.heralds) > 4:
            self.error("More then 4 Heralds cannot be taken in the primary detachment")
        aglist = []
        if self.hq.count_units([Scarbrand, Bloodthirster]) > 0:
            aglist.append('kh')
        if self.hq.count_units([Fateweaver, LordOfChange]) > 0:
            aglist.append('tz')
        if self.hq.count_units([Kugath, Unclean]) > 0:
            aglist.append('ng')
        if self.hq.count_unit(Keeper) > 0:
            aglist.append('sl')
        self.heavy.set_active_types([DaemonPrince], len(aglist) > 0)
        if len(aglist) == 0:
            if self.heavy.count_unit(DaemonPrince):
                self.error("Daemon princes can only be taken for Heavy Support if Greater Daemon of the corresponding "
                           "Chaos god is in HQ.")
        else:
            for gid in aglist:
                if any(prince.god.get_cur() == gid for prince in self.hq.get_units(DaemonPrince)):
                    self.error("Daemon princes of " + self.godList[gid] + " cannot be taken as HQ.")
            for gid in [item for item in self.godList.keys() if item not in aglist]:
                if any(prince.god.get_cur() == gid for prince in self.heavy.get_units(DaemonPrince)):
                    self.error("Daemon princes of " + self.godList[gid] + " cannot be taken as Heavy Support.")
