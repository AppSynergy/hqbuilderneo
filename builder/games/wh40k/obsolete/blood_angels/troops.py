__author__ = 'Denis Romanov'

from builder.core import build_id
from builder.core.model_descriptor import ModelDescriptor
from builder.core.unit import Unit, StaticUnit, ListSubUnit
from builder.games.wh40k.obsolete.blood_angels.transport import Rhino, DropPod, Razorback, LandRaider, LandRaiderCrusader, LandRaiderRedeemer


class TacticalSquad(Unit):
    name = 'Tactical Squad'
    gear = ['Power armor', 'Bolt pistol', 'Frag grenades', 'Krak grenades']

    class SpaceMarineSergeant(Unit):
        name = 'Space Marine Sergeant'
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']
        base_points = 90 - 16 * 4
        def __init__(self):
            Unit.__init__(self)
            wep = [
                ['Chainsword', 0, 'chain' ],
                ['Combi-melta', 10, 'cmelta' ],
                ['Combi-flamer', 10, 'cflame' ],
                ['Combi-plasma', 10, 'cplasma' ],
                ['Stormbolter', 10, 'sbgun' ],
                ['Plasma pistol', 15, 'ppist' ],
                ['Power weapon', 15, 'pw' ],
                ['Power fist', 25, 'pf' ]
            ]

            self.wep1 = self.opt_one_of('Weapon',[['Boltgun', 0, 'bgun' ]] + wep)
            self.wep2 = self.opt_one_of('',[['Bolt pistol', 0, 'bpist' ]] + wep)
            self.opt = self.opt_options_list('Options',[
                ["Melta bombs", 5, 'mbomb'],
                ["Teleport homer", 15, 'thomer'],
                ])

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_count('Space Marine',4,9,16)
        self.hvy1 = self.opt_one_of('Special weapon',
            [
                ['Boltgun', 0, 'bgun' ],
                ['Flamer', 0, 'flame' ],
                ['Meltagun', 5, 'mgun' ],
                ['Plasma gun',10,'pgun']
            ])
        self.hvy2 = self.opt_one_of('Heavy weapon',
            [
                ['Boltgun', 0, 'bgun' ],
                ['Heavy bolter', 0, 'hbgun' ],
                ['Multi-melta', 0, 'mulmgun' ],
                ['Missile launcher', 0, 'mlaunch' ],
                ['Plasma cannon', 5, 'pcannon' ],
                ['Lascannon', 10, 'lcannon']
            ])

        self.sergeant = self.opt_sub_unit(self.SpaceMarineSergeant())
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback(), LandRaider(),
                                                                  LandRaiderCrusader(), LandRaiderRedeemer()])

    def check_rules(self):
        heavy = self.marines.get() + 1 == 10
        self.hvy1.set_visible(heavy)
        self.hvy2.set_visible(heavy)
        self.set_points(self.build_points(count=1))
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.marines.get())
        if heavy:
            self.description.add(self.hvy1.get_selected())
            self.description.add(self.hvy2.get_selected())
            self.description.add('Boltgun', self.marines.get() - 2)
        else:
            self.description.add('Boltgun', self.marines.get())
        self.description.add(self.sergeant.get_selected())
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.marines.get() + 1

class DeathCompany(Unit):
    name = 'Death Company'
    class DeathCompanyMarine(ListSubUnit):
        name = 'Death Company'
        base_points = 20
        max = 30
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            ListSubUnit.__init__(self)
            wep = [
                ['Power weapon', 15, 'pw' ],
                ['Power fist', 25, 'pf' ],
                ['Thunder hammer', 30, 'th' ]
            ]
            spec = [
                ['Hand flamer', 10, 'hf' ],
                ['Infernus pistol', 15, 'ip'],
                ['Plasma pistol', 15, 'pp']
            ]
            self.spec_ids = [o[2] for o in spec]

            self.wep1 = self.opt_one_of('Weapon',[['Boltgun', 0, 'bgun' ], ['Bolt pistol', 0, 'bpist' ]] + wep + spec)
            self.wep2 = self.opt_one_of('',[['Chainsword', 0, 'chain' ]] + wep)

        def has_special(self):
            return self.count.get() if self.wep1.get_cur() in self.spec_ids else 0

    class Lemartes(StaticUnit):
        name = 'Lemartes, Guardian of the Lost'
        base_points = 150
        gear = ['Power armour', 'Bolt pistol', 'The Blood Crozius', 'Frag grenades', 'Krak grenades', "Jump Pack", 'Rosarius']

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_units_list(self.DeathCompanyMarine.name, self.DeathCompanyMarine, 3, 30)
        self.lem = self.opt_optional_sub_unit('', [self.Lemartes()])
        self.mount = self.opt_options_list("", [["Jump Packs", 15, "jump"]])
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback(), LandRaider(),
                                                                  LandRaiderCrusader(), LandRaiderRedeemer()])

    def check_rules(self):
        self.marines.update_range()
        spec_limit = int(self.marines.get_count() / 5)
        spec_total = reduce(lambda val, u: u.has_special() + val, self.marines.get_units(), 0)
        if spec_limit < spec_total:
            self.error('Special weapon (Hand flamer, Infernus pistol, Plasma pistol) is over limit ({0})'.format(spec_limit))
        self.transport.set_active(not self.mount.get('jump'))
        self.set_points(self.build_points(count=1, exclude=[self.mount.id]) + self.mount.points() * self.marines.get_count())
        self.build_description(count=1)
        #self.description.add(self.mount.get_selected())

    def get_count(self):
        return self.marines.get_count() + self.lem.get_count()

    def get_unique(self):
        if self.lem.get_count():
            return self.Lemartes.name

class DeathCompanyDreadnought(Unit):
    name = 'Death Company Dreadnought'
    base_points = 125
    gear = ['Smoke launchers', 'Built-in Meltagun']

    def __init__(self):
        Unit.__init__(self)

        self.bin1 = self.opt_one_of('',[
            ['Built-in Storm bolter',0,'sbgun'],
            ['Built-in Heavy flamer',10,'hflame']
        ])

        self.talons = self.opt_options_list('',[
            ["Blood talons", 0, 'bt'],
            ])

        self.opt = self.opt_options_list('Options',[
            ["Magna-grapple", 15, 'mg'],
            ["Searchlight", 1, 'sl'],
            ])

        self.transport = self.opt_optional_sub_unit('Transport', [DropPod()])

    def check_rules(self):
        self.points.set(self.build_points())
        self.build_description(exclude=[self.talons.id])
        if self.talons.get('bt'):
            self.description.add('Blood Talon', 2)
        else:
            self.description.add('Blood fists', 2)

class ScoutSquad(Unit):
    name = 'Scout Squad'

    class ScoutSergeant(Unit):
        name = 'Scout Sergeant'
        gear = ['Scout armour','Frag grenades','Krak grenades']
        base_points = 75 - 13 * 4

        def __init__(self):
            Unit.__init__(self)
            wep = [
                ['Combi-melta', 10, 'cmelta' ],
                ['Combi-flamer', 10, 'cflame' ],
                ['Combi-plasma', 10, 'cplasma' ],
                ['Plasma pistol', 15, 'ppist' ],
                ['Power weapon', 15, 'pw' ],
                ['Power fist', 25, 'pf' ]
            ]
            self.wep1 = self.opt_one_of('Weapon', [
                ['Shotgun', 0, 'bgun'],
                ['Boltgun', 0],
                ['Sniper rifle', 0],
                ['Combat blade', 0],
            ] + wep)
            self.wep2 = self.opt_one_of('', [['Bolt pistol', 0, 'bpist' ]] + wep)
            self.opt = self.opt_options_list('Options', [
                ["Melta bombs", 5, 'mbomb'],
                ["Locator beacon", 25, 'loc'],
                ])

    class Scout(ListSubUnit):
        base_points = 13
        name = 'Scout'
        min = 1
        max = 9
        gear = ['Scout armour','Frag grenades','Krak grenades', 'Bolt pistol']

        def __init__(self):
            ListSubUnit.__init__(self)

            self.wep = self.opt_one_of('Weapon', [
                ['Shotgun', 0],
                ['Boltgun', 0],
                ['Sniper rifle', 0],
                ['Combat blade', 0],
                ['Heavy Bolter', 10, 'hb'],
                ['Missile Launcher', 10, 'ml'],
                ])

        def has_special(self):
            return (1 if self.wep.get_cur() in ['hb', 'ml'] else 0) * self.count.get()

    def __init__(self):
        Unit.__init__(self)
        self.scouts = self.opt_units_list(self.Scout.name, self.Scout, 4, 9)
        self.sergeant = self.opt_sub_unit(self.ScoutSergeant())
        self.opt = self.opt_options_list('Options',[["Camo cloaks", 3, 'cmcl']])

    def get_count(self):
        return self.scouts.get_count() + 1

    def check_rules(self):
        self.scouts.update_range()
        self.points.set(self.build_points(exclude=[self.opt.id], count=1) +
                        (self.scouts.get_count() + self.sergeant.get_count()) * self.opt.points())
        spec_limit = 1
        spec_total = reduce(lambda val, u: u.has_special() + val, self.scouts.get_units(), 0)
        if spec_limit < spec_total:
            self.error('Special weapon (Heavy Bolter, Missile Launcher) is over limit ({0})'.format(spec_limit))
        self.build_description(exclude=[self.opt.id])
        self.description.add(self.opt.get_selected())


class AssaultSquad(Unit):
    name = 'Assault squad'
    base_gear = ['Power armor', 'Frag grenades', 'Krak grenades', 'Chainsword']

    class SpaceMarineSergeant(Unit):
        name = 'Space Marine Sergeant'
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']
        base_points = 100 - 18 * 4

        def __init__(self):
            Unit.__init__(self)
            weaponlist = [
                ['Hand flamer',10,'hf'],
                ['Plasma pistol',15,'ppist'],
                ['Infernus pistol',15,'ip'],
                ['Power weapon',15,'psw'],
                ['Lightning claw',15,'lclaw'],
                ['Storm shield',20,'ssh'],
                ['Power fist',25,'pfist'],
                ['Thunder hammer',30,'ham']
            ]
            self.wep1 = self.opt_one_of('Weapon',
                [['Bolt pistol', 0, 'bpist' ]] + weaponlist)
            self.wep2 = self.opt_one_of('',
                [['Chainsword',0,'chsw']] + weaponlist)
            self.opt = self.opt_options_list('Options', [
                ["Melta bombs", 5, 'mbomb'],
                ['Combat shield', 5,'csh']
            ])

    def get_count(self):
        return self.marines.get() + 1

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_count('Space Marine',4, 9, 18)
        spec = [
            ['Bolt pistol', 0],
            ['Flamer', 5],
            ['Hand flamer', 10],
            ['Meltagun', 10],
            ['Plasma pistol', 15],
            ['Plasma gun', 15],
            ['Infernus pistol', 15],
            ]
        self.spec1 = self.opt_one_of('Special weapon', spec)
        self.spec2 = self.opt_one_of('', spec)

        self.sergeant = self.opt_sub_unit(self.SpaceMarineSergeant())

        self.tr_units = [DropPod(), Rhino(), Razorback(), LandRaider(), LandRaiderCrusader(), LandRaiderRedeemer()]
        for t in self.tr_units:
            t.base_points -= 35

        self.transport = self.opt_one_of('Options', [['Jump Pack', 0, 'jp']] + [[t.name, t.base_points, build_id(t.name)] for t in self.tr_units])
        self.tr_units = [self.opt_sub_unit(t) for t in self.tr_units]

    def check_rules(self):
        spec_limit = int(self.get_count() / 5)
        self.spec2.set_visible(spec_limit > 1)

        for t in self.tr_units:
            t.set_active(self.transport.get_cur() == build_id(t.get_unit().name))
        self.set_points(self.build_points(count=1, exclude=[self.transport.id]))
        self.build_description(count=self.marines.get(), exclude=[self.marines.id, self.transport.id, self.spec1.id, self.spec2.id])
        desc = ModelDescriptor('Space marine', points=18, gear=self.base_gear)
        self.description.add(desc.clone().add_gear('Bolt pistol').build(self.marines.get() - spec_limit))
        self.description.add(desc.clone().add_gear_opt(self.spec1).build(1))
        self.description.add(desc.clone().add_gear_opt(self.spec2).build(1))
        if self.transport.get_cur() == 'jp':
            self.description.add(self.transport.get_selected(), self.get_count())



