__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit


class Warboss(Unit):
    name = 'Warboss'
    base_points = 60

    def __init__(self):
        Unit.__init__(self)
        self.ccw = self.opt_one_of('Close combat weapon', [
            ['Choppa', 0, 'choppa'],
            ['Big choppa', 5, 'bc'],
            ['Power klaw', 25, 'klaw'],
        ])
        self.rng = self.opt_one_of('Ranged weapon', [
            ["Slugga", 0, 'slugga'],
            ["Shoota", 0, 'shoota'],
            ["Shoota/Rokkit", 5, 'sr'],
            ["Shoota/Skorcha", 5, 'ss'],
            ["Twin-linked Shoota", 5, 'tls'],
        ])
        self.opt = self.opt_options_list('Options', [
            ["Mega armour", 40, 'mega'],
            ["Warbike", 40, 'bike'],
            ["Ammo runt", 3, 'runt'],
            ["Attack squig", 15, 'squig'],
            ["Cyborg body", 10, 'cyborg'],
            ["'eavy armour", 5, 'armour'],
            ["Bosspole", 5, 'pole'],
        ])
        self.cyborg = self.opt_options_list('Grotsnik\'s upgrade', [
            ["Cybork body", 5, 'cyborg'],
        ])

    def check_rules(self):
        self.cyborg.set_active_options(['cyborg'], self.get_roster().has_grotsnik())
        self.opt.set_active_options(['armour', 'bike'], not self.opt.get('mega'))
        self.ccw.set_visible(not self.opt.get('mega'))
        self.rng.set_visible(not self.opt.get('mega'))
        self.opt.set_active_options(['mega'], not (self.opt.get('armour') or self.opt.get('bike')))
        if self.opt.get('mega'):
            self.gear = ['Power klaw', 'Twin-linked Shoota']
        Unit.check_rules(self)


class BigMek(Unit):
    name = 'Big Mek'
    base_points = 35

    def __init__(self):
        Unit.__init__(self)
        self.ccw = self.opt_one_of('Close combat weapon', [
            ['Choppa', 0, 'choppa'],
            ['Burna', 20, 'br'],
            ['Power klaw', 25, 'klaw'],
        ])
        self.rng = self.opt_one_of('Ranged weapon', [
            ["Slugga", 0, 'slugga'],
            ["Shoota", 0, 'shoota'],
            ["Shoota/Rokkit", 5, 'sr'],
            ["Shoota/Skorcha", 5, 'ss'],
            ["Kustom mega-blasta", 15, 'kmb'],
            ["Mega armour", 40, 'mega'],
            ["Warbike", 40, 'bike'],
            ["Shokk attack gun", 60, 'sag'],
            ["Kustom force field", 50, 'kkf'],
        ])
        self.opt = self.opt_options_list('Options', [
            ["Ammo runt", 3, 'runt'],
            ["Attack squig", 15, 'squig'],
            ["Cyborg body", 10, 'cyborg'],
            ["'eavy armour", 5, 'armour'],
            ["Bosspole", 5, 'pole'],
        ])
        self.oilers = self.opt_count("Grot oilers", 0, 3, 5)
        self.cyborg = self.opt_options_list('Grotsnik\'s upgrade', [
            ["Cybork body", 5, 'cyborg'],
        ])

    def check_rules(self):
        self.cyborg.set_active_options(['cyborg'], self.get_roster().has_grotsnik())
        self.gear = ["Mek's tools"]
        if self.rng.get_cur() == 'mega':
            self.gear += ['Power klaw', 'Twin-linked Shoota']
        Unit.check_rules(self)


class GhazghkullThraka(Unit):
    name = 'Ghazghkull Thraka'
    base_points = 225
    unique = True
    gear = ["Cyborg body", "Bosspole", "Mega armor", "Power klaw", "Big Shoota", "Stikkbombs", "Adamantium skull"]

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ["Ammo runt", 3, 'runt'],
        ])


class MadDokGrotsnik(StaticUnit):
    name = 'Mad Dok Grotsnik'
    base_points = 160
    gear = ["Power klaw", "Slugga", "'Urty syringe", "Dok's tools", "Cybork body"]


class WazdakkaGutsmek(StaticUnit):
    name = 'Wazdakka Gutsmek'
    base_points = 180
    gear = ["Bike of Aporkalypse", "Kustom mega-blasta", "Power klaw", "Slugga", "Mek's tools", "Stikkbombs, Bosspole"]


class OldZogwort(StaticUnit):
    name = 'Old Zogwort'
    base_points = 145
    gear = ["Nest of Vipers"]


class Weirdboy(Unit):
    name = 'Weirdboy'
    base_points = 55

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ["Warphead", 30, 'wh'],
        ])
        self.cyborg = self.opt_options_list('Grotsnik\'s upgrade', [
            ["Cybork body", 5, 'cyborg'],
        ])

    def check_rules(self):
        self.cyborg.set_active_options(['cyborg'], self.get_roster().has_grotsnik())
        Unit.check_rules(self)
