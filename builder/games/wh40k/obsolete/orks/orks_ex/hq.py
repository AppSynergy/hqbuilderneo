__author__ = 'Ivan Truskov'

from builder.core.unit import Unit,StaticUnit

class Zhadsnark(StaticUnit):
    name = 'Zhadsnark'
    base_points = 150
    gear = ['\'Da Beast\'','\'Da Rippa\'','Slugga','Stikkbombz']

class Buzzgob(StaticUnit):
    name = 'Buzzgob'
    base_points = 100
    gear = ['Slugga','Mek arms','Big choppa','\'Eavy Armour','Bosspole','Grot Oiler','Grot Oiler']