__author__ = 'Ivan Truskov'
from builder.core.unit import Unit, StaticUnit
class Kugath(StaticUnit):
    name = 'Ku\'gath, The Plaguefather'
    base_points = 300
    gear = ['Cloud of Flies','Noxious Touch','Breath of Chaos','Aura of Decay']
class Fateweaver(StaticUnit):
    name = "Fateweaver, Oracle of Tzeentch"
    base_points = 333
    gear = ['Soul Devourer','Daemonic Flight','Master of Sorcery','We are Legion','Breath of Chaos','Daemonic Gaze','Boon of Mutation','Bolt of Tzeentch']
class Scarbrand(StaticUnit):
    name = 'Scarbrand, The Exiled One'
    base_points = 300
    gear = ['Iron Hide']
class Keeper(Unit):
    name = 'Keeper of Secrets'
    base_points = 200
    gear = ['Aura of Acquiescence']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options',[
                            ['Transfixing Gaze',10,'tgaze'],
                            ['Soporific Musk',20,'smusk'],
                            ['Pavane of Slaanesh',25,'pav'],
                            ['Daemonic gaze',15,'dgaze'],
                            ['Unholy Might',15,'umght'],
                            ['Instrument of Chaos',5,'ich']
                                ])
class Unclean(Unit):
    name = 'Great Unclean One'
    base_points = 160
    gear = ['Noxious Touch']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options',[
                            ['Cloud of Flies',5,'cof'],
                            ['Aura of Decay',20,'aodec'],
                            ['Breath of Chaos',30,'boc'],
                            ['Unholy Might',15,'umght'],
                            ['Instrument of Chaos',5,'ich']
                                ])
                                
class Bloodthirster(Unit):
    name = 'Bloodthirster'
    base_points = 250
    gear = ['Iron Hide','Daemonic Flight']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options',[
                            ['Death Strike',20,'dstr'],
                            ['Blessing of the Blood God',5,'bless'],
                            ['Unholy Might',20,'umght'],
                            ['Instrument of Chaos',5,'ich']
                                ])
                                
class LordOfChange(Unit):
    name = 'Lord of Change'
    base_points = 250
    gear = ['Soul Devourer','Daemonic Flight','Daemonic Gaze','Bolt of Tzeentch']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options',[
                            ['We are Legion',40,'wal'],
                            ['Master of Sorcery',10,'mos'],
                            ['Breath of Chaos',30,'boc'],
                            ['Boon of Mutation',30,'bom'],
                            ['Instrument of Chaos',5,'ich']
                                ])
                                
class Masque(StaticUnit):
    name = "The Masque"
    base_points = 100
    gear = ['Aura of Acquiescence','Soporific Musk','Rending Claws','Instrument of Chaos','Pavane of Slaanesh','We are Legion']
    
class Epidemius(StaticUnit):
    name = "Epidemius"
    base_points = 110
    gear = ['Cloud of Flies','Plaguesword','Aura of Decay']
    
class BlueScribes(StaticUnit):
    name = "The Blue Scribes"
    base_points = 130
    gear = ['Master of Sorcery','We are Legion','Warpfire','Bolt of Tzeentch','Boon of Mutation','Daemonic gaze','Breath of Chaos','Pavane of Slaanesh','Aura of Decay']
    
class Skulltaker(StaticUnit):
    name = "Skulltaker"
    base_points = 140
    gear = ['Hellblade','Iron Hide','Fury of Khorne','Blessing of the Blood God']
    
class HKhorne(Unit):
    name = "Herald of Khorne"
    base_points = 70
    gear = ['Hellblade']
    def __init__(self):
        Unit.__init__(self)
        self.ride = self.opt_options_list('Ride',[
                            ['Juggernaut',35,'jug'],
                            ['Chariot of Khorne',15,'char']
                                ],1)
        self.opt = self.opt_options_list('Options',[
                            ['Fury of Khorne',10,'fkh'],
                            ['Death Strike',15,'dstr'],
                            ['Blessing of the Blood God',5,'bless'],
                            ['Iron Hide',15,'ihd'],
                            ['Unholy Might',15,'umght'],
                            ['Chaos Icon',25,'chic']
                                ],3)
class HTzeentch(Unit):
    name = "Herald of Tzeentch"
    base_points = 50
    gear = ['Daemonic gaze']
    def __init__(self):
        Unit.__init__(self)
        self.ride = self.opt_options_list('Ride',[
                            ['Disc of Tzeentch',15,'disc'],
                            ['Chariot of Tzeentch',15,'char']
                                ],1)
        self.opt = self.opt_options_list('Options',[
                            ['We are Legion',10,'wal'],
                            ['Master of Sorcery',5,'mos'],
                            ['Soul Devourer',20,'sdev'],
                            ['Bolt of Tzeentch',30,'bot'],
                            ['Breath of Chaos',30,'boc'],
                            ['Chaos Icon',25,'chic'],
                            ['Boon of Mutation',30,'bom']
                                ],3)
class HNurgle(Unit):
    name = "Herald of Nurgle"
    base_points = 50
    gear = ['Plaguesword']
    def __init__(self):
        Unit.__init__(self)
        self.ride = self.opt_options_list('Ride',[
                            ['Palanquin of Nurgle',15,'char']
                                ],1)
        self.opt = self.opt_options_list('Options',[
                            ['Noxious Touch',10,'ntch'],
                            ['Cloud of Flies',5,'cof'],
                            ['Aura of Decay',15,'aodec'],
                            ['Breath of Chaos',30,'boc'],
                            ['Chaos Icon',25,'chic'],
                            ['Unholy Might',10,'umght']
                                ],3)
class HSlaanesh(Unit):
    name = "Herald of Slaanesh"
    base_points = 50
    gear = ['Aura of Acquiescence','Rending Claws']
    def __init__(self):
        Unit.__init__(self)
        self.ride = self.opt_options_list('Ride',[
                            ['Mount of Slaanesh',15,'mnt'],
                            ['Chariot of Slaanesh',15,'char']
                                ],1)
        self.opt = self.opt_options_list('Options',[
                            ['Transfixing Gaze',5,'tgaze'],
                            ['Soporific Musk',15,'smusk'],
                            ['Pavane of Slaanesh',20,'pav'],
                            ['Daemonic gaze',15,'dgaze'],
                            ['Chaos Icon',25,'chic'],
                            ['Unholy Might',10,'umght'],
                                ],3)