#
# __author__ = 'Ivan Truskov'
#
from builder.games.wh40k.obsolete.eldar.transport import WaveSerpent, vehicle_options
from builder.core import get_ids
from builder.core.unit import Unit, ListSubUnit, ListUnit
from builder.core.model_descriptor import ModelDescriptor


class Battery(Unit):
    name = 'Vaul\'s Wraith Support Battery'
    base_points = 30

    def __init__(self):
        Unit.__init__(self)
        self.guns = self.opt_count('Support weapon', 1, 3, self.base_points)
        self.type = self.opt_one_of('Cannon type', [
            ['Shadow weaver', 0, 'shw'],
            ['Vibro-cannon', 0, 'vcan'],
            ['D-cannon', 25, 'dcan'],
        ])

    def check_rules(self):
        self.set_points(self.guns.points() + self.guns.get() * self.type.points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Support weapon',
                                             points=30).add_gear_opt(self.type).build(self.guns.get()))
        self.description.add(ModelDescriptor('Guardian crew',
                                             gear=[
                                                 'Shuriken catapult',
                                                 'Mesh armour',
                                                 'Plasma grenades'
                                             ]).build(self.guns.get() * 2))


class DarkReapers(Unit):
    name = 'Dark Reapers'
    base_gear = ['Reaper launcher', 'Reaper rangefinder', 'Heavy Aspect armour']
    min_models = 3
    max_models = 10
    model_points = 30
    model_name = 'Dark Reaper'

    class Exarch(Unit):
        name = 'Dark Reaper Exarch'
        base_points = 30 + 10
        gear = ['Reaper rangefinder', 'Heavy Aspect armour']

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Reaper launcher', 0, 'rl'],
                ['Shuriken cannon', 10, 'shcan'],
                ['Eldar missile launcher', 10, 'eml'],
                ['Tempest launcher', 20, 'tl'],
            ])
            self.flakk = self.opt_options_list('', [['Flakk missiles', 10, 'flakk']])
            self.sm = self.opt_options_list('', [['Starshot missiles', 10, 'sm']])

            self.opt = self.opt_options_list('Exarch powers', [
                ['Night Vision', 5, 'nv'],
                ['Fast Shot', 10, 'fs'],
                ['Marksman\'s Eye', 10, 'me'],
            ], limit=2)

        def check_rules(self):
            self.flakk.set_visible(self.wep.get_cur() == 'eml')
            self.sm.set_visible(self.wep.get_cur() == 'rl')
            Unit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count(self.model_name, self.min_models, self.max_models, self.model_points)
        self.opt = self.opt_options_list('', [['Starshot missiles', 8, 'sm']])
        self.leader = self.opt_optional_sub_unit('Exarch', self.Exarch())
        self.transport = self.opt_optional_sub_unit('Transport', [WaveSerpent()])

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(self.min_models - ldr, self.max_models - ldr)
        self.set_points(self.build_points(count=1, exclude=[self.opt.id]) + self.opt.points() * self.warriors.get())
        self.build_description(options=[self.leader])
        self.description.add(
            ModelDescriptor(name=self.model_name, gear=self.base_gear,
                            points=self.model_points).add_gear_opt(self.opt).build(self.warriors.get())
        )
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()


class Wraithlord(Unit):
    name = 'Wraithlord'
    base_points = 120

    wep = [
        ['Shuriken cannon', 15, 'shcan'],
        ['Bright lance', 20, 'blance'],
        ['Scatter laser', 20, 'scat'],
        ['Starcannon', 20, 'stcan'],
        ['Eldar missile launcher', 30, 'eml'],
    ]

    @staticmethod
    def multiple_id(item_id, n):
        if not n:
            return item_id
        return '{0}_{1}'.format(item_id, n)

    def process_sec_wep(self, item_id, count=2):
        selected = sum((1 for i in range(count) if self.sec.get(self.multiple_id(item_id, i))))
        for i in range(count):
            full_id = self.multiple_id(item_id, i)
            if self.sec.get(full_id) or selected >= 0:
                self.sec.set_visible_options([self.multiple_id(item_id, i)], True)
            else:
                self.sec.set_visible_options([self.multiple_id(item_id, i)], False)
            selected -= 1

    def __init__(self):
        Unit.__init__(self)
        free = [
            ['Shuriken catapult', 0, 'sc'],
            ['Flamer', 0, 'flame'],
        ]
        self.free1 = self.opt_one_of('Weapon', free)
        self.free2 = self.opt_one_of('', free)
        self.costy = self.opt_options_list('', [['Ghostglave', 5, 'wsw']])
        wep_opt = []
        for o in self.wep:
            wep_opt.append([o[0], o[1], self.multiple_id(o[2], 0)])
            wep_opt.append([o[0], o[1], self.multiple_id(o[2], 1)])
        self.sec = self.opt_options_list('', wep_opt, limit=2)

    def check_rules(self):
        for unit_id in get_ids(self.wep):
            self.process_sec_wep(unit_id)
        Unit.check_rules(self)


class Wraithknight(Wraithlord):
    name = 'Wraithknight'
    base_points = 240
    base_gear = {
        'hw': ['Heavy wraith cannon' for _ in range(2)],
        'gs': ['Ghostglave', 'Scattershield'],
        'ss': ['Suncannon', 'Scattershield'],
    }
    wep_opt = [
        ['Shuriken cannon', 15, 'shcan'],
        ['Scatter laser', 20, 'scat'],
        ['Starcannon', 20, 'stcan'],
    ]

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Two heavy wraith cannons', 0, 'hw'],
            ['Ghostglave and scattershield', 10, 'gs'],
            ['Suncannon and scattershield', 40, 'ss'],
        ])
        wep_opt = []
        for o in self.wep_opt:
            wep_opt.append([o[0], o[1], self.multiple_id(o[2], 0)])
            wep_opt.append([o[0], o[1], self.multiple_id(o[2], 1)])
        self.sec = self.opt_options_list('', wep_opt, limit=2)

    def check_rules(self):
        for unit_id in get_ids(self.wep_opt):
            self.process_sec_wep(unit_id)
        self.gear = self.base_gear[self.wep.get_cur()]
        self.points.set(self.build_points())
        self.build_description(exclude=[self.wep.id])


class WalkerSquadron(ListUnit):
    name = 'War Walker Squadron'

    class WarWalker(ListSubUnit):
        name = 'War Walker'
        base_points = 60

        def __init__(self):
            ListSubUnit.__init__(self)
            weapon = [
                ['Shuriken cannon', 0, 'shcan'],
                ['Bright lance', 5, 'blance'],
                ['Scatter laser', 5, 'sclas'],
                ['Starcannon', 5, 'scan'],
                ['Eldar missile launcher', 15, 'eml'],
            ]
            self.left = self.opt_one_of('Weapon', weapon)
            self.left_flakk = self.opt_options_list('', [['Flakk missiles', 10, 'flakk']])
            self.right = self.opt_one_of('', weapon)
            self.right_flakk = self.opt_options_list('', [['Flakk missiles', 10, 'flakk']])
            self.opt = self.opt_options_list('Options', vehicle_options)

        def check_rules(self):
            self.left_flakk.set_visible(self.left.get_cur() == 'eml')
            self.right_flakk.set_visible(self.right.get_cur() == 'eml')
            ListSubUnit.check_rules(self)

    def __init__(self):
        ListUnit.__init__(self, unit_class=self.WarWalker, min_num=1, max_num=3)

    def check_rules(self):
        mtx = sum((1 for u in self.units.get_units() if u.opt.get('gwm')))
        if mtx and mtx != len(self.units.get_units()):
            self.error('Ghostwalk Matrix must be taken for all vehicles in squadron')
        ListUnit.check_rules(self)


class Falcon(Unit):
    name = 'Falcon'
    base_points = 125
    gear = ['Pulse laser']

    def __init__(self):
        Unit.__init__(self)
        self.costy = self.opt_one_of('Weapon', [
            ['Shuriken cannon', 0, 'shcan'], 
            ['Starcannon', 5, 'scan'], 
            ['Bright lances', 10, 'blance'], 
            ['Scatter laser', 10, 'sclas'], 
            ['Eldar missile launcher', 15, 'eml'], 
        ])
        self.free = self.opt_one_of('', [
            ['Twin-linked shuriken catapults', 0, 'tlshc'],
            ['Shuriken cannon', 10, 'shcan']
        ])
        self.opt = self.opt_options_list('Options', vehicle_options)


class FirePrism(Unit):
    name = 'Fire Prism'
    base_points = 125
    gear = ['Prism cannon']

    def __init__(self):
        Unit.__init__(self)
        self.free = self.opt_one_of('Weapon', [
            ['Twin-linked shuriken catapults', 0, 'tlshc'],
            ['Shuriken cannon', 10, 'shcan']
        ])
        self.opt = self.opt_options_list('Options', vehicle_options)


class NightSpinner(Unit):
    name = 'Night Spinner'
    base_points = 115
    gear = ['Doomweaver']

    def __init__(self):
        Unit.__init__(self)
        self.free = self.opt_one_of('Weapon', [
            ['Twin-linked shuriken catapults', 0, 'tlshc'],
            ['Shuriken cannon', 10, 'shcan']
        ])
        self.opt = self.opt_options_list('Options', vehicle_options)