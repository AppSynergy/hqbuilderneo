__author__ = 'Ivan Truskov'

from builder.games.wh40k.obsolete.eldar.transport import WaveSerpent
from builder.core.unit import Unit, ListSubUnit
from builder.core.model_descriptor import ModelDescriptor


class Dragons(Unit):
    name = 'Fire Dragons'
    base_gear = ['Fusion gun', 'Melta bombs', 'Heavy Aspect armour']
    min_models = 5
    max_models = 10
    model_points = 22
    model_name = 'Fire Dragon'

    class Exarch(Unit):
        name = 'Fire Dragon Exarch'
        base_points = 22 + 10
        gear = ['Melta bombs', 'Heavy Aspect armour']

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Fusion gun', 0, 'fg'], 
                ['Dragon\s breath flamer', 0, 'ddbflame'], 
                ['Firepike', 15, 'pike']
            ])

            self.opt = self.opt_options_list('Exarch powers', [
                ['Iron Resolve', 5, 'is'], 
                ['Crushing Blow', 10, 'cb'], 
                ['Fast Shot', 10, 'fs'], 
            ], limit=2)

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count(self.model_name, self.min_models, self.max_models, self.model_points)
        self.leader = self.opt_optional_sub_unit('Exarch', self.Exarch())
        self.transport = self.opt_optional_sub_unit('Transport', [WaveSerpent()])

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(self.min_models - ldr, self.max_models - ldr)
        self.set_points(self.build_points(count=1))
        self.build_description(options=[self.leader])
        self.description.add(ModelDescriptor(name=self.model_name, gear=self.base_gear,
                                             points=self.model_points).build(self.warriors.get()))
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()


class Scorpions(Dragons):
    name = 'Striking Scorpions'
    base_gear = ['Shuriken pistol', 'Scorpion chainsword', 'Mandiblaster', 'Plasma grenades', 'Heavy Aspect armour']
    min_models = 5
    max_models = 10
    model_points = 17
    model_name = 'Striking Scorpion'

    class Exarch(Unit):
        name = 'Striking Scorpion Exarch'
        base_points = 17 + 10
        gear = ['Mandiblaster', 'Plasma grenades', 'Heavy Aspect armour']

        def __init__(self):
            Unit.__init__(self)
            self.side = self.opt_one_of('Weapon', [
                ['Shuriken pistol', 0, 'shp'],
                ['Scorpion\'s claw', 30, 'scc']
            ])
            self.wep = self.opt_one_of('', [
                ['Scorpion chainsword', 0, 'scsw'],
                ['Biting blade', 5, 'btb']
            ])
            self.pair = self.opt_options_list('', [
                ['Chainsabres', 10, 'chsbr']
            ])
            self.opt = self.opt_options_list('Exarch powers', [
                ['Monster Hunter', 5, 'is'],
                ['Crushing Blow', 10, 'cb'],
                ['Stalker', 10, 'stlk'],
            ], limit=2)

        def check_rules(self):
            self.wep.set_visible(not self.pair.get('chsbr'))
            self.side.set_visible(not self.pair.get('chsbr'))
            Unit.check_rules(self)


class Wraithguard(Unit):
    name = 'Wraithguards'
    model_points = 32

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Wraithguard', 5, 10, self.model_points)
        self.wep = self.opt_one_of('Weapon', [
            ['Wraithcannon', 0, 'wc'],
            ['D-scythe', 10, 'ds']
        ])
        self.transport = self.opt_optional_sub_unit('Transport', [WaveSerpent()])

    def check_rules(self):
        self.set_points(self.build_points(count=1, exclude=[self.wep.id]) + self.wep.points() * self.get_count())
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Wraithguard',
                                             points=self.model_points).add_gear_opt(self.wep).build(self.get_count()))
        self.description.add(self.transport.get_selected())


class Wraithblades(Unit):
    name = 'Wraithblades'
    model_points = 32

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Wraithblade', 5, 10, self.model_points)
        self.wep = self.opt_one_of('Weapon', [
            ['Ghostswords', 0, 'sw'],
            ['Ghost axe and forceshield', 0, 'axe']
        ])
        self.transport = self.opt_optional_sub_unit('Transport', [WaveSerpent()])

    def check_rules(self):
        self.set_points(self.build_points(count=1))
        self.build_description(options=[])
        d = ModelDescriptor('Wraithguard', points=self.model_points, count=self.get_count())
        if self.wep.get_cur() == 'sw':
            d.add_gear('Ghostsword', count=2)
        else:
            d.add_gear_list(['Ghost axe', 'Forceshield'])
        self.description.add(d.build())
        self.description.add(self.transport.get_selected())


class Banshees(Dragons):
    name = 'Howling Banshees'
    base_gear = ['Banshee mask', 'Shuriken pistol', 'Power sword', 'Aspect armour']
    min_models = 5
    max_models = 10
    model_points = 15
    model_name = 'Howling Banshee'

    class Exarch(Unit):
        name = 'Howling Banshee Exarch'
        base_points = 15 + 10
        base_gear = ['Banshee mask', 'Heavy Aspect armour']

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Power sword', 0, 'pwep'],
                ['Triskele', 5, 'trskl'],
                ['Executioner', 10, 'exec'],
            ])
            self.pair = self.opt_options_list('', [
                ['Mirrorswords', 10, 'chsbr']
            ])
            self.opt = self.opt_options_list('Exarch powers', [
                ['Fear', 5, 'is'],
                ['Disarm Strike', 10, 'cb'],
                ['Shield of Grace', 10, 'stlk'],
            ], limit=2)

        def check_rules(self):
            self.wep.set_visible(not self.pair.get('chsbr'))
            self.gear = self.base_gear + (['Shuriken pistol'] if not self.pair.get('chsbr') else [])
            Unit.check_rules(self)


class Harlequins(Unit):
    name = 'Harlequins'

    class Harlequin(ListSubUnit):
        name = 'Harlequin'
        base_points = 18
        gear = ['Flip belt', 'Holo-suit']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.ccw = self.opt_one_of('Weapon', [
                ['Close combat weapon', 0, 'ccw'],
                ['Harlequin\'s kiss', 4, 'hkiss']
            ])
            self.rng = self.opt_one_of('', [
                ['Shuriken pistol', 0, 'spist'],
                ['Fusion pistol', 10, 'fpist']
            ])

        def has_fusion(self):
            return self.count.get() if self.rng.get_cur() == 'fpist' else 0

    class DeathJester(Unit):
        name = 'Death Jester'
        base_points = 28
        gear = ['Flip belt', 'Holo-suit', 'Shrieker cannon']
        static = True

    class Shadowseer(Unit):
        gear = ['Flip belt', 'Holo-suit', 'Hallucinogen grenades', 'Close combat weapon', 'Shuriken pistol']
        name = 'Shadowseer'
        base_points = 48
        static = True

    class TroupeMaster(Unit):
        name = 'Troupe Master'
        base_points = 38
        gear = ['Flip belt', 'Holo-suit', 'Fusion pistol']

        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_one_of('Weapon', [
                ['Harlequin\'s kiss', 0, 'hkiss'],
                ['Power sword', 0, 'psw'],
            ])

    def __init__(self):
        Unit.__init__(self)
        self.troupe = self.opt_units_list(self.Harlequin.name, self.Harlequin, 5, 10)
        self.dj = self.opt_optional_sub_unit(self.DeathJester.name, self.DeathJester())
        self.shs = self.opt_optional_sub_unit(self.Shadowseer.name, self.Shadowseer())
        self.tm = self.opt_optional_sub_unit(self.TroupeMaster.name, self.TroupeMaster())

    def get_count(self):
        return self.troupe.get_count() + self.dj.get_count() + self.shs.get_count() + self.tm.get_count()

    def check_rules(self):
        leaders = self.dj.get_count() + self.shs.get_count() + self.tm.get_count()
        self.troupe.update_range(5 - leaders, 10 - leaders)
        fusions = sum((u.has_fusion() for u in self.troupe.get_units()))
        if fusions > 2:
            self.error("Harlequins can't have more then 2 fusion pistols")
        self.set_points(self.build_points(count=1))
        self.build_description(count=1)
