__author__ = 'Denis Romanov'

from builder.core.unit import Unit, ListSubUnit, ListUnit


class Cavalry(Unit):
    name = "Thunderwolf Cavalry"
    base_points = 50

    class Raider(Unit):
        name = 'Thunderwolf Raider'
        base_points = 50
        gear = ["Power Armour", "Frag and Krak grenades"]

        def __init__(self):
            Unit.__init__(self)
            self.count = self.opt_count(self.name, 1, 5, self.base_points)
            self.wep1 = self.opt_one_of("Weapon", [
                ["Bolt Pistol", 0, "pistol"],
                ["Boltgun", 0, "gun"],
                ["Plasma Pistol", 15, "plasma"],
                ["Storm Shield", 30, "shield"],
            ], id='w1')
            self.wep2 = self.opt_one_of("Weapon", [
                ["Close Combat Weapon", 0, "ccw"],
                ["Power Weapon", 10, "pw"],
                ["Wolf Claw", 15, "claw"],
                ["Power Fist", 20, "fist"],
                ["Frost Blade", 20, "blade"],
                ["Frost Axe", 20, "axe"],
                ["Thunder Hummer", 25, "hummer"],
                ["Storm Shield", 25, "shield"],
            ], id='w2')
            self.opt = self.opt_options_list("Options", [
                ["Melta Bombs", 5, "power"],
                ["Mark of the Wulfen", 15, "mark"],
            ])

        def check_rules(self):
            self.points.set((self.base_points + self.wep1.points() + self.wep2.points() + self.opt.points()) * self.count.get() )
            self.description.reset()
            self.description.set_header(self.name, self.base_points + self.wep1.points() + self.wep2.points() + self.opt.points())
            self.description.add(self.gear)
            self.description.add(self.wep1.get_selected())
            self.description.add(self.wep2.get_selected())
            self.description.add(self.opt.get_selected())
            self.set_description(self.description)

        def have_unique_wep(self):
            return self.wep2.get_cur() != 'ccw' and self.count.get() == 1

        def have_mark(self):
            return self.opt.get('mark')

        def disable_unique_wep(self, val):
            opt = ["pw", "claw", "fist", "blade", "axe",  "hummer", "shield"]
            if self.count.get() != 1:
                self.wep2.set_active_options(opt, False)
            elif not self.have_unique_wep():
                self.wep2.set_active_options(opt, val)

        def disable_mark(self, val):
            if self.count.get() != 1:
                self.opt.set_active_options(['mark'], False)
            elif not self.have_mark():
                self.opt.set_active_options(['mark'], val)

    def __init__(self):
        Unit.__init__(self)
        self.raider = self.opt_units_list(self.Raider.name, self.Raider, 1, 5)

    def check_rules(self):
        self.raider.update_range(1, 5)
        have_mark = reduce(lambda val, u: u.have_mark() or val, self.raider.get_units(), False)
        for u in self.raider.get_units():
            u.disable_mark(not have_mark)

        have_wep = reduce(lambda val, u: u.have_unique_wep() or val, self.raider.get_units(), False)
        for u in self.raider.get_units():
            u.disable_unique_wep(not have_wep)

        self.points.set(self.raider.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.raider.get_selected())
        self.set_description(self.description)

    def get_count(self):
        return self.raider.get_count()


class Biker(Unit):
    name = "Swiftclaw Biker Pack"
    base_points = 25
    gear = ["Space Marine Bike", "Power Armour", "Frag and Krak grenades"]

    class AttackBike(Unit):
        name = "Swiftclaw Attack Bike"
        base_points = 30
        gear = ["Power Armour", "Frag and Krak grenades", "Close Combat Weapon", "Bolt Pistol"]

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of("Weapon", [
                ["Heavy Bolter", 0, "bolt"],
                ["Multy-melta", 10, "melta"],
                ])
            self.bombs = False

        def check_rules(self):
            self.points.set(self.base_points + self.wep.points() + (5 if self.bombs else 0))
            self.description.reset()
            self.description.set_header(self.name, self.points.get())
            self.description.add(self.gear)
            self.description.add(self.wep.get_selected())
            if self.bombs:
                self.description.add('Melta Bombs')
            self.set_description(self.description)

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Bikes', 3, 10, self.base_points)
        self.wep1 = self.opt_one_of("Weapon", [
            ["Bolt Pistol", 0, "bolt"],
            ["Flamer", 5, "flame"],
            ["Meltagun", 10, "melta"],
            ["Plasma Gun", 15, "plasmagun"],
            ["Plasma Pistol", 15, "plasma"],
        ], id='w1')
        self.wep2 = self.opt_one_of("Weapon", [
            ["Close Combat Weapon", 0, "ccw"],
            ["Power Weapon", 15, "pw"],
            ["Power Fist", 25, "fist"],
        ], id='w2')
        self.opt = self.opt_options_list("Options", [
            ["Melta Bombs", 5, "bombs"],
        ])
        self.ab = self.opt_options_list('Attack Bike', [
            [self.AttackBike.name, self.AttackBike.base_points, 'ab'],
        ])
        self.ab_unit = self.opt_sub_unit(self.AttackBike())

    def check_rules(self):
        self.ab_unit.set_active(self.ab.get('ab'))
        self.ab_unit.get_unit().bombs = self.opt.get('bombs')
        self.ab_unit.get_unit().check_rules()

        self.points.set((self.base_points + self.opt.points())* self.count.get() + self.wep1.points() + self.wep2.points() + self.ab_unit.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.count.get())
        self.description.add(["Close Combat Weapon", "Bolt Pistol"], self.count.get() - 1)
        self.description.add(self.wep1.get_selected())
        self.description.add(self.wep2.get_selected())
        self.description.add(self.opt.get_selected(), self.count.get())
        self.description.add(self.ab_unit.get_selected())
        self.set_description(self.description)

    def get_count(self):
        return self.count.get() + self.ab_unit.get_count()


class Assault(Unit):
    name = "Skyclaw Assault Pack"
    base_points = 18
    gear = ["Jump Pack", "Power Armour", "Frag and Krak grenades"]

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Skyclaw', 5, 10, self.base_points)
        self.wep1 = self.opt_one_of("Weapon", [
            ["Bolt Pistol", 0, "bolt"],
            ["Flamer", 5, "flame"],
            ["Meltagun", 10, "melta"],
            ["Plasma Gun", 15, "plasmagun"],
            ["Plasma Pistol", 15, "plasma"],
            ], id ='w1')
        self.wep2 = self.opt_one_of("Weapon", [
            ["Close Combat Weapon", 0, "ccw"],
            ["Power Weapon", 15, "pw"],
            ["Power Fist", 25, "fist"],
            ], id ='w2')
        self.opt = self.opt_options_list("Options", [
            ["Mark of the Wulfen", 15, "mark"],
            ])

    def check_rules(self):
        self.points.set(self.base_points * self.count.get() + self.wep1.points() + self.wep2.points() + self.opt.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.count.get())
        self.description.add(["Close Combat Weapon", "Bolt Pistol"], self.count.get() - 1)
        self.description.add(self.wep1.get_selected())
        self.description.add(self.wep2.get_selected())
        self.description.add(self.opt.get_selected())
        self.set_description(self.description)


class LandSpeederSquadron(ListUnit):
    name = "Land Speeder Squadron"

    class LandSpeeder(ListSubUnit):
        name = "Land Speeder"
        base_points = 50
        max = 3

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ["Heavy Bolter", 0, "bolt"],
                ["Heavy Flamer", 0, "flame"],
                ["Multi-melta", 10, "melta"],
            ])
            self.up = self.opt_options_list('Upgrade', [
                ["Heavy Bolter", 10, "bolt"],
                ["Heavy Flamer", 10, "flame"],
                ["Multi-melta", 20, "melta"],
                ["Assault Cannon", 40, "assault"],
                ["Typhoon Missile Launcher", 40, "tml"],
            ], limit=1)

        def check_rules(self):
            self.name = "Land Speeder"
            if self.up.get('tml'):
                self.name += ' Typhoon'
            elif self.up.get("bolt") or self.up.get("flame") or self.up.get("melta") or self.up.get("assault"):
                self.name += ' Tornado'
            ListSubUnit.check_rules(self)

    unit_class = LandSpeeder


class WolfPack(Unit):
    name = "Fenrisian Wolf Pack"
    base_points = 8
    gear = ['Claws and fangs']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Fenrisian Wolf', 5, 15, self.base_points)
        self.up = self.opt_options_list('Upgrade', [
            ["Cyberwolf", 8, "cyber"],
        ])

    def check_rules(self):
        self.points.set(self.count.points() + self.up.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.count.get())
        self.description.add(self.up.get_selected())
        self.set_description(self.description)
