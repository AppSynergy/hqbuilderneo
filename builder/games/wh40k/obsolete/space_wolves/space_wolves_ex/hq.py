__author__ = 'Ivan Truskov'
from builder.core.unit import StaticUnit

class Redmaw(StaticUnit):
    name = "Bran Redmaw"
    base_points = 210
    gear = ['Rune Armour', 'The Axe Langnvast', 'Wolftooth Necklace', 'Bolt Pistol', 'The Belt of Russ', 'Saga of the Hunter', 'Frag grenades', 'Krak grenades']
