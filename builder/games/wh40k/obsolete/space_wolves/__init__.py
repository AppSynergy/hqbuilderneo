__author__ = 'Denis Romanov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.old_roster import Ally
from builder.games.wh40k.obsolete.space_wolves.hq import *
from builder.games.wh40k.obsolete.space_wolves.elites import *
from builder.games.wh40k.obsolete.space_wolves.troops import *
from builder.games.wh40k.obsolete.space_wolves.fast import *
from builder.games.wh40k.obsolete.space_wolves.heavy import *
from builder.games.wh40k.obsolete.space_wolves.space_wolves_ex import ia_hq

class SpaceWolves(LegacyWh40k):
    army_id = '79bc29d35d914e73b1fd72f934ad5b6f'
    army_name = 'Space Wolves'
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(self,
            hq = [Logan, Njal, Ragnar, Ulric, Canis, Bjorn, WolfLord, RunePriest, WolfPriest, BattleLeader] + ia_hq,
            elites = [WolfGuards, Dreadnought, Venerable, WolfScouts, IronPriest, LoneWolf],
            troops = [GreyHunters, BloodClaws,
                      {'unit': WolfGuards, 'active': False},
                      {'unit': WolfPack, 'active': False},
                      ],
            fast = [Cavalry, Biker, Assault, LandSpeederSquadron, WolfPack],
            heavy = [LongFangs, Predator, Whirlwind, Vindicator, LandRaider, LandRaiderCrusader, LandRaiderRedeemer],
            secondary=secondary, ally=Ally(parent=self, ally_type=Ally.SW)
        )
        self.opt.visible = True

        def check_hq_limit():
            return self.hq.min <= len(self.hq.get_units()) <= self.hq.max*2
        self.hq.check_limits = check_hq_limit

    def check_rules(self):
        self.hq.set_active_types(ia_hq, self.opt.ia.value)

        logan = self.hq.count_unit(Logan) > 0
        self.troops.set_active_types([WolfGuards], logan)
        self.elites.set_active_types([WolfGuards], not logan)
        if not logan and self.troops.count_unit(WolfGuards) > 0:
            self.error("You can't have Wolf Guards in Troops without Logan Grimnar in HQ.")

        canis = self.hq.count_unit(Canis) > 0
        self.troops.set_active_types([WolfPack], canis)
        self.fast.set_active_types([WolfPack], not canis)
        if not canis and self.troops.count_unit(WolfPack) > 0:
            self.error("You can't have Fenrisian Wolf Packs in Troops without Canis Wolfborn in HQ.")

        wg = self.elites.get_units(WolfGuards) + self.troops.get_units(WolfGuards)
        lr = reduce(lambda val, u: (val + 1) if u.have_land_raider() else val, wg, 0)
        if lr > 1:
            self.error("You can't have more then one Land Raider of any type as Wolf Guards dedicated transport.")
