__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit

class Logan(StaticUnit):
    name = "Logan Grimnar, The Great Wolf"
    base_points = 275
    gear = ['Terminator Armour', 'The Axe Morkai', 'Wolftooth Necklace', 'Wolf Tail Talisman', 'Storm Bolter', 'The Belt of Russ', 'Saga of Majesty']

class Njal(Unit):
    name = "Njal Stormcaller, Lord of Tempests"
    base_points = 245
    gear = ['Wolftooth Necklace', 'Staff of the Stormcaller', 'Nightwing', 'Bolt Pistol', 'Frag and Krak Grenades', 'Saga of Majesty']
    unique = True
    def __init__(self):
        Unit.__init__(self)
        self.armor = self.opt_one_of('Armour', [
            ["Runic Armour", 0, "runic"],
            ["Terminator Armour", 25, "tda"],
        ])

class Ragnar(Unit):
    name = "Wolf Lord Ragnar Blackmane"
    base_points = 240
    gear = [ 'Power Armour', 'Bolt Pistol', 'Wolftooth Necklace', 'Wolf Tail Talisman', 'Frost Blade', 'Melta Bombs', 'Frag and Krak Grenades', 'Saga of the Warrior Born']
    unique = True
    def __init__(self):
        Unit.__init__(self)
        self.wolves = self.opt_options_list('Wolves', [
            ["Svangir", 10, "svangir"],
            ["Ulfgir", 10, "ulfgir"],
            ])

class Ulric(StaticUnit):
    name = "Ulric the Slayer"
    base_points = 180
    gear = ['Power Armour', 'Plasma Pistol', 'Frag and Krak Grenades', 'Power Weapon', 'Fang of Morkai', 'Wolftooth Necklace', 'Wolf Amulet', 'Wolf Helm of Russ']


class Canis(Unit):
    name = "Canis Wolfborn"
    base_points = 185
    gear = [ 'Power Armour', 'Wolftooth Necklace', 'Wolf Tail Talisman', 'Wolf Claw', 'Frag and Krak Grenades', 'Fangir', 'Saga of the Wolfkin']
    unique = True
    def __init__(self):
        Unit.__init__(self)
        self.wolves = self.opt_count('Fenrisian wolf', 0, 2, 10)

class Bjorn(Unit):
    name = "Bjorn the Fell-Handed"
    base_points = 270
    gear = ['Dreadnought CCW (with build-in Heavy Flamer)', 'Wolf Tail Talisman', 'Smoke Launchers', 'Saga of Majesty']
    unique = True
    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ["Assault Cannon", 0, "assault"],
            ["Plasma Cannon", 0, "plasma"],
            ["Twin-linked Lascannon", 20, "lascannon"],
            ])

class WolfLord(Unit):
    name = "Wolf Lord"
    base_points = 100
    def __init__(self):
        Unit.__init__(self)
        self.armor = self.opt_one_of('Armour', [
            ["Power Armour", 0, "power"],
            ["Runic Armour", 20, "runic"],
            ["Terminator Armour", 40, "tda"],
            ])

        wep = [
            ["Boltgun", 0, "gun"],
            ["Storm Bolter", 3, "storm"],
            ["Combi-flamer", 10, "cflame"],
            ["Combi-melta", 10, "cmelta"],
            ["Combi-plasma", 10, "cplasma"],
            ["Power Weapon", 15, "pw"],
            ["Plasma Pistol", 15, "plasma"],
            ["Wolf Claw", 20, "claw"],
            ["Power Fist", 25, "fist"],
            ["Frost Blade", 25, "blade"],
            ["Frost Axe", 25, "axe"],
            ["Thunder Hummer", 30, "hummer"],
            ["Storm Shield", 30, "shield"],
            ]

        self.wep1 = self.opt_one_of('Weapon', [["Bolt Pistol", 0, "pistol"]] + wep, id='w1')

        self.wep2 = self.opt_one_of('Weapon', [["Close Combat Weapon", 0, "ccw"]] + wep, id='w2')

        self.tda_wep1 = self.opt_one_of('Weapon', [
            ["Storm Bolter", 0, "storm"],
            ["Combi-flamer", 5, "cflame"],
            ["Combi-melta", 5, "cmelta"],
            ["Combi-plasma", 5, "cplasma"],
            ["Wolf Claw", 15, "claw"],
            ["Power Fist", 20, "fist"],
            ["Thunder Hummer", 25, "hummer"],
            ["Storm Shield", 25, "shield"],
            ["Chain Fist", 25, "chain"],
            ], id='tda_w1')

        self.tda_wep2 = self.opt_one_of('Weapon', [
            ["Power Weapon", 0, "power"],
            ["Wolf Claw", 5, "claw"],
            ["Power Fist", 10, "fist"],
            ["Frost Blade", 10, "blade"],
            ["Frost Axe", 10, "axe"],
            ["Thunder Hummer", 15, "hummer"],
            ["Storm Shield", 15, "shield"],
            ["Chain Fist", 15, "chain"],
            ], id='tda_w2')

        self.mount = self.opt_options_list("Mount", [
            ["Jump Pack", 25, "jump"],
            ["Space Marine Bike", 35, "bike"],
            ["Thunder Wolf Mount", 45, "wolf"],
        ], limit = 1)

        self.opt = self.opt_options_list("Options", [
            ["Melta Bombs", 5, "power"],
            ["Wolftooth Necklace", 10, "neck"],
            ["Wolf Tail Talisman", 5, "tail"],
            ["Fenrisian Wolf", 10, "fwolf1"],
            ["Fenrisian Wolf", 10, "fwolf2"],
            ["Mark of the Wulfen", 15, "mark"],
            ["Belt of Russ", 25, "belt"],
        ])

        self.sagas = self.opt_options_list("Sagas", [
            ["Saga of the Beastslayer", 10, "beastslayer"],
            ["Saga of Majesty", 15, "majesty"],
            ["Saga of the Wolfkin", 15, "wolfkin"],
            ["Saga of the Warrior Born", 35, "born"],
            ["Saga of the Bear", 35, "bear"],
        ], limit = 1)

    def check_rules(self):
        tda = self.armor.get_cur() == 'tda'
        self.wep1.set_visible(not tda)
        self.wep2.set_visible(not tda)
        self.mount.set_visible(not tda)
        self.tda_wep1.set_visible(tda)
        self.tda_wep2.set_visible(tda)
        self.gear = ['Frag and Krak Grenades'] if not tda else []
        Unit.check_rules(self)


class RunePriest(Unit):
    name = "Rune Priest"
    base_points = 100
    def __init__(self):
        Unit.__init__(self)
        self.armor = self.opt_one_of('Armour', [
            ["Power Armour", 0, "power"],
            ["Runic Armour", 20, "runic"],
            ["Terminator Armour", 20, "tda"],
            ])

        self.wep1 = self.opt_one_of('Weapon', [
            ["Bolt Pistol", 0, "pistol"],
            ["Boltgun", 0, "gun"],
            ["Storm Bolter", 3, "storm"],
            ["Plasma Pistol", 15, "plasma"],
            ], id='w1')


        self.tda_wep1 = self.opt_one_of('Weapon', [
            ["Storm Bolter", 0, "storm"],
            ["Combi-flamer", 5, "cflame"],
            ["Combi-melta", 5, "cmelta"],
            ["Combi-plasma", 5, "cplasma"],
            ], id='tda_w1')

        self.mount = self.opt_options_list("Mount", [
            ["Jump Pack", 25, "jump"],
            ["Space Marine Bike", 35, "bike"],
            ], limit = 1)

        self.opt = self.opt_options_list("Options", [
            ["Master of Runes", 50, "master"],
            ["Melta Bombs", 5, "power"],
            ["Wolftooth Necklace", 10, "neck"],
            ["Wolf Tail Talisman", 5, "tail"],
            ["Chooser of the Slain", 10, "chooser"],
            ])

        self.sagas = self.opt_options_list("Sagas", [
            ["Saga of the Beastslayer", 10, "beastslayer"],
            ["Saga of the Warrior Born", 35, "born"],
            ], limit = 1)

    def check_rules(self):
        tda = self.armor.get_cur() == 'tda'
        self.wep1.set_visible(not tda)
        self.mount.set_visible(not tda)
        self.tda_wep1.set_visible(tda)
        self.gear = ['Runic Weapon', 'Frag and Krak Grenades'] if not tda else ['Runic Weapon']
        Unit.check_rules(self)


class WolfPriest(Unit):
    name = "Wolf Priest"
    base_points = 100
    def __init__(self):
        Unit.__init__(self)
        self.armor = self.opt_one_of('Armour', [
            ["Power Armour", 0, "power"],
            ["Runic Armour", 20, "runic"],
            ["Terminator Armour", 20, "tda"],
            ])

        self.wep1 = self.opt_one_of('Weapon', [
            ["Bolt Pistol", 0, "pistol"],
            ["Boltgun", 0, "gun"],
            ["Storm Bolter", 3, "storm"],
            ["Plasma Pistol", 15, "plasma"],
            ["Combi-flamer", 10, "cflame"],
            ["Combi-melta", 10, "cmelta"],
            ["Combi-plasma", 10, "cplasma"],
            ], id='w1')


        self.tda_wep1 = self.opt_one_of('Weapon', [
            ["Storm Bolter", 0, "storm"],
            ["Combi-flamer", 5, "cflame"],
            ["Combi-melta", 5, "cmelta"],
            ["Combi-plasma", 5, "cplasma"],
            ], id='tda_w1')

        self.mount = self.opt_options_list("Mount", [
            ["Jump Pack", 25, "jump"],
            ["Space Marine Bike", 35, "bike"],
            ], limit = 1)

        self.opt = self.opt_options_list("Options", [
            ["Melta Bombs", 5, "power"],
            ["Wolftooth Necklace", 10, "neck"],
            ["Wolf Tail Talisman", 5, "tail"],
            ])

        self.sagas = self.opt_options_list("Sagas", [
            ["Saga of the Beastslayer", 10, "beastslayer"],
            ["Saga of Hunter", 10, "hunter"],
            ["Saga of the Wolfkin", 15, "wolfkin"],
            ["Saga of the Warrior Born", 35, "born"],
            ], limit = 1)

    def check_rules(self):
        tda = self.armor.get_cur() == 'tda'
        self.wep1.set_visible(not tda)
        self.mount.set_visible(not tda)
        self.tda_wep1.set_visible(tda)
        gear = ['Crozius Arcanum', 'Fang of Morkai', 'Wolf Amulet']
        self.gear = gear +['Frag and Krak Grenades'] if not tda else gear
        Unit.check_rules(self)


class BattleLeader(Unit):
    name = "Wolf Guard Battle Leader"
    base_points = 70
    def __init__(self):
        Unit.__init__(self)
        self.armor = self.opt_one_of('Armour', [
            ["Power Armour", 0, "power"],
            ["Runic Armour", 20, "runic"],
            ["Terminator Armour", 40, "tda"],
            ])

        wep = [
            ["Boltgun", 0, "gun"],
            ["Storm Bolter", 3, "storm"],
            ["Combi-flamer", 10, "cflame"],
            ["Combi-melta", 10, "cmelta"],
            ["Combi-plasma", 10, "cplasma"],
            ["Power Weapon", 15, "pw"],
            ["Plasma Pistol", 15, "plasma"],
            ["Wolf Claw", 20, "claw"],
            ["Power Fist", 25, "fist"],
            ["Frost Blade", 25, "blade"],
            ["Frost Axe", 25, "axe"],
            ["Thunder Hummer", 30, "hummer"],
            ["Storm Shield", 30, "shield"],
            ]

        self.wep1 = self.opt_one_of('Weapon', [["Bolt Pistol", 0, "pistol"]] + wep, id='w1')

        self.wep2 = self.opt_one_of('Weapon', [["Close Combat Weapon", 0, "ccw"]] + wep, id='w2')

        self.tda_wep1 = self.opt_one_of('Weapon', [
            ["Storm Bolter", 0, "storm"],
            ["Combi-flamer", 5, "cflame"],
            ["Combi-melta", 5, "cmelta"],
            ["Combi-plasma", 5, "cplasma"],
            ["Wolf Claw", 15, "claw"],
            ["Power Fist", 20, "fist"],
            ["Thunder Hummer", 25, "hummer"],
            ["Storm Shield", 25, "shield"],
            ["Chain Fist", 25, "chain"],
            ], id='tda_w1')

        self.tda_wep2 = self.opt_one_of('Weapon', [
            ["Power Weapon", 0, "power"],
            ["Wolf Claw", 5, "claw"],
            ["Power Fist", 10, "fist"],
            ["Frost Blade", 10, "blade"],
            ["Frost Axe", 10, "axe"],
            ["Thunder Hummer", 15, "hummer"],
            ["Storm Shield", 15, "shield"],
            ["Chain Fist", 15, "chain"],
            ], id='tda_w2')

        self.mount = self.opt_options_list("Mount", [
            ["Jump Pack", 25, "jump"],
            ["Space Marine Bike", 35, "bike"],
            ["Thunder Wolf Mount", 45, "wolf"],
            ], limit = 1)

        self.opt = self.opt_options_list("Options", [
            ["Melta Bombs", 5, "power"],
            ["Wolftooth Necklace", 10, "neck"],
            ["Wolf Tail Talisman", 5, "tail"],
            ["Fenrisian Wolf", 10, "fwolf1"],
            ["Fenrisian Wolf", 10, "fwolf2"],
            ["Mark of the Wulfen", 15, "mark"],
            ])

        self.sagas = self.opt_options_list("Sagas", [
            ["Saga of the Beastslayer", 10, "beastslayer"],
            ["Saga of Hunter", 10, "hunter"],
            ["Saga of the Wolfkin", 15, "wolfkin"],
            ["Saga of the Warrior Born", 35, "born"],
            ], limit = 1)

    def check_rules(self):
        tda = self.armor.get_cur() == 'tda'
        self.wep1.set_visible(not tda)
        self.wep2.set_visible(not tda)
        self.mount.set_visible(not tda)
        self.tda_wep1.set_visible(tda)
        self.tda_wep2.set_visible(tda)
        self.gear = ['Frag and Krak Grenades'] if not tda else []
        Unit.check_rules(self)

