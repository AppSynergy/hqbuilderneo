__author__ = 'dromanow'

from builder.core2 import Gear, UnitDescription,\
    OptionsList, Count, UnitList,\
    ListSubUnit
from armory import Boltgun, BoltPistol,\
    Melee, Ranged, Eviscerator, Shotgun, WeaponRelic,\
    Relic, InfernoPistol, Laspistol, CCW
from troops import Transport
from builder.games.wh40k.roster import Unit


class Celestine(Unit):
    type_name = 'Saint Celestine'
    type_id = 'celestine_v3'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Saint-Celestine')

    def __init__(self, parent):
        super(Celestine, self).__init__(parent, points=135, unique=True, static=True, gear=[
            Gear('Armour of Saint Katherine'), Gear('The Ardent Blade'),
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Jump pack')
        ])


class Jacobus(Unit):
    type_name = 'Uriah Jacobus, Protector of the Faith'
    type_id = 'jaccobus_v3'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Uriah-Jacobus')

    def __init__(self, parent):
        super(Jacobus, self).__init__(parent, 'Uriah Jacobus', 100, unique=True, static=True, gear=[
            Gear('Flak armour'), Gear('Bolt pistol'), Gear('The Redeemer'),
            Gear('Chainsword'), Gear('Frag grenades'), Gear('Krak grenades'),
            Gear('Banner of Sanctity'), Gear('Rosarius')
        ])


class Canoness(Unit):
    type_name = 'Canoness'
    type_id = 'canoness_v3'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Canoness')

    class Weapon1(WeaponRelic, Eviscerator, Melee,
                  Ranged, InfernoPistol, Boltgun, BoltPistol):
        pass

    class Weapon2(WeaponRelic, InfernoPistol,\
                  Eviscerator, Melee):
        pass

    class Options(OptionsList):
        def __init__(self, parent):
            super(Canoness.Options, self).__init__(parent, 'Options')
            self.variant('Melta bombs', 5)
            self.variant('Rosarius', 15)

    def __init__(self, parent):
        super(Canoness, self).__init__(parent, points=65, gear=[
            Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades')])
        
        self.wep1 = self.Weapon1(self, 'Ranged weapon')
        self.wep2 = self.Weapon2(self, 'Melee weapon')
        self.wep2.melee = self.wep2.melee[1:]
        self.opt = self.Options(self)
        self.relics = Relic(self)

    def check_rules(self):
        super(Canoness, self).check_rules()
        for wr1, wr2 in zip(self.wep1.relics, self.wep2.relics):
            wr1.active = not(self.relics.any or self.wep2.cur == wr2)
            wr2.active = not(self.relics.any or self.wep1.cur == wr1)
        self.wep1.allow_melee(not self.wep2.is_melee())
        self.wep1.allow_ranged(not self.wep2.is_ranged())
        self.wep2.allow_melee(not self.wep1.is_melee())
        self.wep2.allow_ranged(not self.wep1.is_ranged())

    def get_unique_gear(self):
        for wep in [self.wep1, self.wep2]:
            if wep.cur in wep.relics:
                return wep.description
        if self.relics.any:
            return self.relics.description
        return []


class CommandSquad(Unit):
    type_name = 'Sororitas Command Squad'
    type_id = 'sor_command_v3'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Sororitas-Command-Squad')
    faction = 'Adepta Sororotas'

    common_gear = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades')]
    model_points = 65 / 5

    class Dialogus(Unit):
        name = 'Dialogus'

        def __init__(self, parent):
            super(CommandSquad.Dialogus, self).__init__(parent, 'Dialogus', CommandSquad.model_points,
                                                        CommandSquad.common_gear + [])
            self.relics = Relic(self)

        def get_unique_gear(self):
            return self.relics.description if self.relics.any else []

    class Celestian(ListSubUnit):
        name = 'Celestian'

        class Upgrades(OptionsList):
            def __init__(self, parent):
                super(CommandSquad.Celestian.Upgrades, self).__init__(parent, 'Upgrades', limit=1)
                self.diag = self.variant('Dialogus', gear=[Gear('Bolt pistol'), Gear('Loud hailer')])
                self.hosp = self.variant('Hospitaler', gear=[Gear('Bolt pistol'), Gear('Chirurgeon\'s tools')])

        class Icons(OptionsList):
            def __init__(self, parent):
                super(CommandSquad.Celestian.Icons, self).__init__(parent, 'Icons of faith', limit=1)
                default_weapons = [Gear('Bolt pistol'), Gear('Boltgun')]
                self.simp = self.variant('Simulacrum imperialis', 10, gear=default_weapons + [Gear('Simulacrum imperialis')])
                self.flags = [self.variant('Blessed banner', 15, gear=default_weapons + [Gear('Blessed banner')]),
                              self.variant('Sacred Banner of the Order Militant', 40,
                                           gear=default_weapons + [Gear('Sacred Banner of the Order Militant')])]
        class Weapon1(Melee, Ranged, BoltPistol):
            pass

        class Weapon2(Melee, Ranged, Boltgun):
            def __init__(self, parent):
                super(CommandSquad.Celestian.Weapon2, self).__init__(parent, name='')
                self.variant('Storm bolter', 5)
                self.variant('Flamer', 5)
                self.variant('Meltagun', 10)
                self.variant('Heavy bolter', 10)
                self.variant('Multi-melta', 10)
                self.variant('Heavy flamer', 10)

        def __init__(self, parent):
            super(CommandSquad.Celestian, self).__init__(parent, 'Celestian',
                                                         CommandSquad.model_points,
                                                         CommandSquad.common_gear)
            self.up = self.Upgrades(self)
            self.icon = self.Icons(self)
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.relic = Relic(self)

        def check_rules(self):
            super(CommandSquad.Celestian, self).check_rules()
            self.icon.used = self.icon.visible = not self.up.any
            spec_gear = self.icon.any and self.icon.used
            self.wep1.used = self.wep1.visible = not(spec_gear or self.up.any)
            self.wep2.used = self.wep2.visible = not(spec_gear or self.up.any)
            self.relic.used = self.relic.visible = self.up.diag.value
            self.wep1.allow_melee(not self.wep2.is_melee())
            self.wep1.allow_ranged(not self.wep2.is_ranged())
            self.wep2.allow_melee(not self.wep1.is_melee())
            self.wep2.allow_ranged(not self.wep1.is_ranged())

        def build_description(self):
            res = super(CommandSquad.Celestian, self).build_description()
            if self.up.diag.value:
                res.name = 'Dialogus'
            if self.up.hosp.value:
                res.name = 'Hospitaler'
            return res

        @ListSubUnit.count_unique
        def get_relic(self):
            return self.relic.description if self.relic.used else []

        @ListSubUnit.count_gear
        def has_flag(self):
            return any(opt.value for opt in self.icon.flags) if self.icon.used else 0

        @ListSubUnit.count_gear
        def has_sim(self):
            return self.icon.simp.value if self.icon.used else 0

        @ListSubUnit.count_gear
        def is_dialogus(self):
            return self.up.diag.value

        @ListSubUnit.count_gear
        def is_hosp(self):
            return self.up.hosp.value

    class Options(OptionsList):
        def __init__(self, parent):
            super(CommandSquad.Options, self).__init__(parent, 'Options')
            self.variant('Melta bombs', 25)

    def __init__(self, parent):
        super(CommandSquad, self).__init__(parent)
        self.models = UnitList(self, self.Celestian, 5, 5)
        self.opt = self.Options(self)
        self.transport = Transport(self)

    def check_rules(self):
        super(CommandSquad, self).check_rules()
        flag = sum((c.has_flag() for c in self.models.units))
        if flag > 1:
            self.error('Only one Celestian can take a banner (taken: {})'.format(flag))

        sim = sum((c.has_sim() for c in self.models.units))
        if sim > 1:
            self.error('Only one Celestian can take a Simulacrum imperialis (taken: {})'.format(sim))

        diag = sum(c.is_dialogus() for c in self.models.units)
        if diag > 1:
            self.error('Only one Celestian may be upgraded to Dialogus')

        hosp = sum(c.is_hosp() for c in self.models.units)
        if hosp > 1:
            self.error('Only one Celestian may be upgraded to Hospitaler')

    def get_count(self):
        return 5

    def get_unique_gear(self):
        return sum((u.get_relic() for u in self.models.units), [])

    def build_statistics(self):
        return self.note_transport(super(CommandSquad, self).build_statistics())


class Priest(Unit):
    type_name = 'Ministorum Priest'
    type_id = 'priest_v3'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Ministorum-Priest')
    faction = 'Adepta Sororotas'

    class Weapon1(Shotgun, Melee, Laspistol):
        def __init__(self, parent):
            super(Priest.Weapon1, self).__init__(parent)
            self.variant('Autogun', 0)
            self.variant('Bolt pistol', 1)
            self.variant('Boltgun', 1)
            self.variant('Plasma gun', 15)

    class Weapon2(Shotgun, Melee, CCW):
        pass

    class Options(OptionsList):
        def __init__(self, parent):
            super(Priest.Options, self).__init__(parent, 'Options')
            self.variant('Melta bombs', 5)
    
    def __init__(self, parent):
        super(Priest, self).__init__(parent, points=25,
                                     gear=[Gear('Flak armour'), Gear('Frag grenades'),
                                           Gear('Krak grenades'), Gear('Rosarius')])
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.opt = self.Options(self)
        self.relics = Relic(self, priest=True)

    def get_unique_gear(self):
        return self.relics.description

    def check_rules(self):
        super(Priest, self).check_rules()
        self.wep1.allow_melee(not self.wep2.is_melee())
        self.wep1.allow_ranged(not self.wep2.is_ranged())
        self.wep2.allow_melee(not self.wep1.is_melee())
        self.wep2.allow_ranged(not self.wep1.is_ranged())


class Conclave(Unit):
    type_name = 'Ecclesiarchy Battle Conclave'
    type_id = 'conclave_v3'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Ecclesiarchy-Battle-Conclave')
    faction = 'Adepta Sororotas'

    def __init__(self, parent):
        super(Conclave, self).__init__(parent)
        self.arc = Count(self, 'Arco-flagellant', 0, 10, 10, True,
                         gear=UnitDescription('Arco-flagellant', 10, options=[Gear('Arco-flail', count=2)]))
        self.ass = Count(self, 'Death Cult Assassin', 0, 10, 15, True,
                         gear=UnitDescription('Death Cult Assassin', 15, options=[Gear('Power sword', count=2)]))
        self.crus = Count(self, 'Crusader', 0, 10, 15, True,
                         gear=UnitDescription('Crusader', 15, options=[Gear('Power sword'), Gear('Storm shield')]))
        self.transport = Transport(self)

    def check_rules(self):
        super(Conclave, self).check_rules()
        Count.norm_counts(3, 10, [self.ass, self.arc, self.crus])

    def get_count(self):
        return sum(c.cur for c in [self.ass, self.arc, self.crus])

    def build_statistics(self):
        return self.note_transport(super(Conclave, self).build_statistics())
