__author__ = 'Denis Romanov'

from builder.core import get_ids

#replace bolt pislol or ccw
ranged = [
    ['Storm Bolter', 5, 'sb'],
    ['Combi-melta', 10, 'cmelta'],
    ['Combi-flamer', 10, 'cflame'],
    ['Combi-plasma', 10, 'cplasma'],
    ['Combi-grav', 10, 'cgrav'],
    ['Grav-pistol', 15, 'gp'],
    ['Plasma pistol', 15, 'pp'],
]

ranged_pistol = [['Bolt pistol', 0, 'bp']] + ranged
ranged_ccw = [['Close combat weapon', 0, 'ccw']] + ranged

#replace bolt pislol or ccw
melee = [
    ['Power weapon', 15, 'pw'],
    ['Lightning claw', 15, 'claw'],
    ['Power fist', 25, 'pfist'],
    ['Thunder hammer', 30, 'ham']
]

melee_pistol = [['Bolt pistol', 0, 'bp']] + melee
melee_ccw = [['Close combat weapon', 0, 'ccw']] + melee


tda_ranged = [
    ['Storm Bolter', 0, 'sb'],
    ['Combi-melta', 6, 'cmelta'],
    ['Combi-flamer', 6, 'cflame'],
    ['Combi-plasma', 6, 'cplasma'],
    ['Lightning claw', 10, 'claw'],
    ['Thunder hammer', 25, 'ham']
]

tda_power = [
    ['Power weapon', 0, 'psw'],
    ['Lightning claw', 5, 'claw'],
    ['Storm shield', 5, 'ss'],
    ['Power fist', 10, 'pfist'],
    ['Chainfist', 15, 'cfist'],
    ['Thunder hammer', 15, 'ham']
]

spec = [
    ['Auspex', 5, 'ax'],
    ['Melta bombs', 5, 'mbomb'],
    ['Digital weapons', 10, 'digw'],
    ['Teleport homer', 10, 'tp'],
]

spec_bike = [['Space Marine Bike', 20, 'bike']]
spec_jump = [['Jump pack', 15, 'jpack']]

relic_weapon = [
    ['The Primarch\'s Wrath', 20, 'rel_pw'],
    ['Teeth of Terra', 35, 'rel_tt'],
    ['The Shield Eternal', 50, 'rel_se'],
    ['The Burning Blade', 55, 'rel_bb'],
]

relic_weapon_ids = get_ids(relic_weapon)

# Does not replace one of the character's weapons. May not be chosen by models wearing Terminator armour.
relic_gear = [
    ['The Armour Indomitus', 60, 'rel_ai'],
]
lr_vehicle = [
    ["Storm bolter", 5, 'sbgun'],
    ["Hunter-killer missile", 10, 'hkm'],
    ["Extra armour", 10, 'exarm']
]

vehicle = [
    ["Dozer blade", 5, 'dblade'],
] + lr_vehicle

special = [
    ['Flamer', 5, 'flame'],
    ['Meltagun', 10, 'mgun'],
    ['Grav-gun', 15, 'gravgun'],
    ['Plasma gun', 15, 'pgun'],
]
spec_id = get_ids(special)

heavy = [
    ['Heavy bolter', 10, 'hbgun'],
    ['Multi-melta', 10, 'mulmgun'],
    ['Missile launcher', 15, 'mlaunch'],
    ['Plasma cannon', 15, 'pcannon'],
    ['Lascannon', 20, 'lcannon']
]
heavy_id = get_ids(heavy)

leg_heavy = heavy + [['Heavy flamer', 10, 'hf']]
leg_heavy_id = get_ids(leg_heavy)

flakk = [
    ['Flakk missiles', 10]
]
