__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit, ListUnit
from builder.core.model_descriptor import ModelDescriptor
from builder.core.options import norm_counts
from builder.games.wh40k.obsolete.space_marines.transport import Rhino, DropPod
from builder.games.wh40k.obsolete.space_marines.armory import *


class AssaultSquad(Unit):
    name = 'Assault Squad'
    base_gear = ['Power armor', 'Frag grenades', 'Krak grenades', 'Chainsword']

    class SpaceMarineSergeant(Unit):
        name = 'Space Marine Sergeant'
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']
        base_points = 85 - 17 * 4

        def __init__(self):
            Unit.__init__(self)
            weapon_list = [
                ['Plasma pistol', 15, 'ppist'],
                ['Grav-pistol', 15, 'gpist'],
            ] + melee

            self.wep1 = self.opt_one_of('Weapon', [
                ['Bolt pistol', 0, 'bpist']
            ] + weapon_list)
            self.wep2 = self.opt_one_of('', [
                ['Chainsword', 0, 'chsw']
            ] + weapon_list)
            self.opt = self.opt_options_list('Options', [
                ["Melta bombs", 5, 'mbomb'],
                ['Combat shield', 5, 'csh']
            ])
            self.vet = self.opt_options_list('', [["Veteran", 10, 'vet']])

        def check_rules(self):
            self.set_points(self.build_points())
            if self.vet.get('vet'):
                self.build_description(name='Veteran Sergeant', exclude=[self.vet.id])
            else:
                self.build_description()

    def get_count(self):
        return self.marines.get() + 1

    def __init__(self):
        Unit.__init__(self)
        self.sergeant = self.opt_sub_unit(self.SpaceMarineSergeant())
        self.marines = self.opt_count('Space Marine', 4, 9, 17)
        self.flame = self.opt_count('Flamer', 0, 2, 5)
        self.pp = self.opt_count('Plasma pistol', 0, 2, 15)
        self.ride = self.opt_options_list('Options', [
            ['Jump packs', 0, 'jp']
        ])
        self.ride.set('jp', True)
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(base_points=0), Rhino(base_points=0)])

    def check_rules(self):
        self.transport.set_active(not self.ride.get('jp'))
        norm_counts(0, 2, [self.flame, self.pp])
        self.set_points(self.build_points(count=1))
        self.build_description(count=1, options=[self.sergeant, self.ride])

        desc = ModelDescriptor('Space Marine', gear=self.base_gear, points=17)
        self.description.add(desc.clone().add_gear('Flamer', points=5).build(self.flame.get()))
        self.description.add(desc.clone().add_gear('Plasma pistol', points=15).build(self.pp.get()))
        self.description.add(desc.clone().add_gear('Bolt pistol').build(self.marines.get() - self.flame.get() -
                                                                        self.pp.get()))
        self.description.add(self.transport.get_selected())


class LandSpeederSquadron(ListUnit):
    name = "Land Speeder Squadron"

    class LandSpeeder(ListSubUnit):
        name = "Land Speeder"
        base_points = 50

        def __init__(self):
            ListSubUnit.__init__(self, min_models=1, max_models=3)
            self.wep = self.opt_one_of('Weapon', [
                ["Heavy Bolter", 0, "bolt"],
                ["Heavy Flamer", 0, "flame"],
                ["Multi-melta", 10, "melta"],
            ])
            self.up = self.opt_options_list('', [
                ["Heavy Bolter", 10, "bolt"],
                ["Heavy Flamer", 10, "flame"],
                ["Multi-melta", 20, "melta"],
                ["Typhoon Missile Launcher", 25, "tml"],
                ["Assault Cannon", 30, "assault"],
            ], limit=1)

    def __init__(self):
        ListUnit.__init__(self, unit_class=self.LandSpeeder, min_num=1, max_num=3)


class Stormtalon(Unit):
    name = 'Stormtalon Gunship'
    base_points = 110
    gear = ['Ceramite plating', 'Twin-linked assault cannon']

    def __init__(self):
        Unit.__init__(self)
        self.weapon = self.opt_one_of('Weapon', [
            ['Twin-linked heavy bolter', 0, 'tlhb'],
            ['Skyhammer missile launcher', 15, 'shml'],
            ['Twin-linked lascannon', 30, 'tllcan'],
            ['Typhoon missile launcher', 35, 'tml']
        ])


class AttackBikeSquad(ListUnit):
    name = "Attack Bike Squad"

    class AttackBike(ListSubUnit):
        name = "Attack bike"
        base_points = 45
        gear = ['Power armour', 'Bolt pistol', 'Frag grenades', 'Krak grenades', 'Space Marine bike']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Heavy bolter', 0, 'hbgun'],
                ['Multi-melta', 10, 'mmelta']
            ])

    def __init__(self):
        ListUnit.__init__(self, self.AttackBike, min_num=1, max_num=3)


class SpaceMarineBikers(Unit):
    name = 'Bike Squad'
    base_gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Space Marine bike', 'Twin-linked boltgun']

    class BikerSergeant(Unit):
        name = 'Biker Sergeant'
        gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Space Marine bike', 'Twin-linked boltgun']
        base_points = 63 - 21 * 2

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', melee_pistol + ranged)
            self.opt = self.opt_options_list('Options', [
                ["Melta bombs", 5, 'mbomb']
            ])
            self.vet = self.opt_options_list('', [["Veteran", 10, 'vet']])

        def check_rules(self):
            self.set_points(self.build_points())
            if self.vet.get('vet'):
                self.build_description(name='Veteran Biker Sergeant', exclude=[self.vet.id])
            else:
                self.build_description()

    class AttackBike(Unit):
        name = "Attack bike"
        base_points = 45
        gear = ['Power armour', 'Bolt pistol', 'Frag grenades', 'Krak grenades', 'Space Marine bike']

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Heavy bolter', 0, 'hbgun'],
                ['Multi-melta', 10, 'mmelta']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.sergeant = self.opt_sub_unit(self.BikerSergeant())
        self.bikers = self.opt_count('Space Marine Biker', 2, 7, 21)

        self.flame = self.opt_count('Flamer', 0, 2, 5)
        self.melta = self.opt_count('Meltagun', 0, 2, 10)
        self.grav = self.opt_count('Grav-gun', 0, 2, 15)
        self.plasma = self.opt_count('Plasma gun', 0, 2, 15)

        self.abike = self.opt_optional_sub_unit('', [self.AttackBike()])
        self.spec = [self.flame, self.plasma, self.melta, self.grav]

    def get_count(self):
        return self.bikers.get() + self.abike.get_count() + 1

    def get_bikers(self):
        return self.bikers.get() + 1

    def check_rules(self):
        norm_counts(0, 2, self.spec)
        self.set_points(self.build_points(count=1))
        self.build_description(options=[self.sergeant, self.abike], count=1)
        desc = ModelDescriptor('Space Marine Biker', gear=self.base_gear, points=21)
        self.description.add(desc.clone().add_gear('Flamer', points=5).build(self.flame.get()))
        self.description.add(desc.clone().add_gear('Meltagun', points=10).build(self.melta.get()))
        self.description.add(desc.clone().add_gear('Grav-agun', points=15).build(self.grav.get()))
        self.description.add(desc.clone().add_gear('Plasma gun', points=15).build(self.plasma.get()))
        self.description.add(desc.add_gear('Bolt pistol').build(self.bikers.get() -
                                                                sum(map(lambda o: o.get(), self.spec))))


class ScoutBikers(Unit):
    name = 'Scout Bike squad'
    base_gear = ['Scout armour', 'Bolt pistol', 'Space Matine shotgun', 'Frag grenades', 'Krak grenades',
                 'Space Marine bike']

    class BikerSergeant(Unit):
        name = 'Scout Biker Sergeant'
        gear = ['Scout armour', 'Space Matine shotgun', 'Frag grenades', 'Krak grenades', 'Space Marine bike',
                'Twin-linked boltgun']
        base_points = 54 - 18 * 2

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', melee_pistol + ranged)
            self.opt = self.opt_options_list('Options', [
                ["Melta bombs", 5, 'mbomb'],
                ['Locator beacon', 10, 'lbcn']
            ])
            self.vet = self.opt_options_list('', [["Veteran", 10, 'vet']])

        def check_rules(self):
            self.set_points(self.build_points())
            if self.vet.get('vet'):
                self.build_description(name='Scout Biker Veteran Sergeant', exclude=[self.vet.id])
            else:
                self.build_description()

    def __init__(self):
        Unit.__init__(self)
        self.bikers = self.opt_count('Scout Biker', 2, 9, 18)
        self.grenades = self.opt_count('Astartes grenade launcher', 0, 3, 5)
        self.opt = self.opt_options_list('Options', [['Cluster mines', 20, 'cmines']])
        self.sergeant = self.opt_sub_unit(self.BikerSergeant())

    def check_rules(self):
        norm_counts(0, min(self.bikers.get(), 3), [self.grenades])
        self.set_points(self.build_points(count=1))
        self.build_description(options=[self.sergeant, self.opt], count=1)
        desc = ModelDescriptor('Scout Biker', gear=self.base_gear, points=18)
        self.description.add(desc.clone().add_gear('Astartes grenade launcher', points=5).build(self.grenades.get()))
        self.description.add(desc.add_gear('Twin-linked boltgun').build(self.bikers.get() - self.grenades.get()))

    def get_count(self):
        return self.bikers.get() + 1

