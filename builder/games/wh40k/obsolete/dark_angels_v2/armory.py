from builder.core2 import OptionsList, OneOf

__author__ = 'Denis Romanov'


class Vehicle(OptionsList):
    def __init__(self, parent, melta=False, shield=False):
        super(Vehicle, self).__init__(parent, 'Options')
        self.sbgun = self.Variant(self, 'Storm bolter', 5)
        self.dblade = self.Variant(self, 'Dozer blade', 5)
        self.hkm = self.Variant(self, 'Hunter-killer missile', 10)
        self.exarm = self.Variant(self, 'Extra armour', 10)
        if melta:
            self.melta = self.Variant(self, 'Multi melta', 10)
        if shield:
            self.shield = self.Variant(self, 'Siege shield', 10)


class Weapon(OneOf):
    def __init__(self, parent, name, armour=None, melee=False, ranged=False, tda_ranged=False, tda_melee=False,
                 relic=False, pistol=False, bolt_gun=False, force=False, crozius=False):
        super(Weapon, self).__init__(parent, name)

        self.armour = armour
        self.pwr_weapon = []
        self.tda_weapon = []
        self.relic_weapon = []

        if pistol:
            self.pistol = self.variant('Bolt pistol', 0)
            self.pwr_weapon += [self.pistol]

        if bolt_gun:
            self.boltgun = self.variant('Boltgun', 0)
            self.pwr_weapon += [self.boltgun]

        if force:
            self.force = self.variant('Force weapon', 0)

        if crozius:
            self.roz = self.variant('Crozius Arcanum', 0)

        if melee:
            self.csw = self.variant('Chainsword', 0)
            self.claw = self.variant('Lightning claw', 15)
            self.pwr = self.variant('Power weapon', 15)
            self.fist = self.variant('Power fist', 25)
            self.hummer = self.variant('Thunder hammer', 30)
            self.pwr_weapon += [self.csw, self.claw, self.pwr, self.fist, self.hummer]

        if ranged:
            self.boltgun = self.variant('Boltgun', 0)
            self.storm_bolter = self.variant('Storm Bolter', 5)
            self.combi_melta = self.variant('Combi-melta', 10)
            self.combi_flamer = self.variant('Combi-flamer', 10)
            self.combi_plasma = self.variant('Combi-plasma', 10)
            self.plasma_pistol = self.variant('Plasma pistol', 15)
            self.pwr_weapon += [self.boltgun, self.storm_bolter, self.combi_plasma, self.combi_melta, self.combi_flamer,
                                self.plasma_pistol]

        if tda_ranged:
            self.tda_storm_bolter = self.variant('Storm Bolter', 0)
            self.tda_combi_melta = self.variant('Combi-melta', 6)
            self.tda_combi_flamer = self.variant('Combi-flamer', 6)
            self.tda_combi_plasma = self.variant('Combi-plasma', 6)
            self.tda_weapon += [self.tda_storm_bolter, self.tda_combi_flamer, self.tda_combi_melta,
                                self.tda_combi_plasma]

        if tda_melee:
            self.tda_pwr = self.variant('Power sword', 0)
            self.tda_weapon += [self.tda_pwr]

        if relic:
            self.fs = self.variant('Foe-smiter', 20)
            self.lr = self.variant('Lion\'s Roar', 20)
            self.mor = self.variant('Mace or Redemption', 30)
            self.msoc = self.variant('Monster Slayer of Caliban', 45)
            self.relic_weapon = [self.fs, self.lr, self.mor, self.msoc]

    def check_rules(self):
        if self.armour:
            if self.armour.is_tda():
                visible = self.tda_weapon
                invisible = self.pwr_weapon
            else:
                visible = self.pwr_weapon
                invisible = self.tda_weapon

            for wep in visible:
                wep.visible = True
            for wep in invisible:
                wep.visible = False

    def get_unique(self):
        if self.cur in self.relic_weapon:
            return self.description
        return []
