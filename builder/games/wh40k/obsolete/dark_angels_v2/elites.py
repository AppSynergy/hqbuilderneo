__author__ = 'Denis Romanov'
from builder.core2 import *
from transport import TerminatorTransport, Transport, DropPod
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle
from builder.games.wh40k.imperial_armour.volume2.transport import LuciusDropPod


class DeathwingTerminatorSquad(Unit):
    type_name = 'Deathwing Terminator Squad'
    type_id = 'deathwing_terminator_squad_v1'

    model_points = 220 / 5

    class Veteran(ListSubUnit):
        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(DeathwingTerminatorSquad.Veteran.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.power_fist = self.variant('Power fist', 0)
                self.chain_fist = self.variant('Chainfist', 5)

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(DeathwingTerminatorSquad.Veteran.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant('Storm bolter', 0)
                self.hf = self.variant('Heavy flamer', 10)
                self.pc = self.variant('Plasma Cannon', 15)
                self.ac = self.variant('Assault Cannon', 20)

            def enable_spec(self, value):
                self.hf.active = self.pc.active = self.ac.active = value

            def has_spec(self):
                return self.cur != self.bolt

        class Weapon(OptionsList):
            def __init__(self, parent):
                super(DeathwingTerminatorSquad.Veteran.Weapon, self).__init__(parent=parent, name='')
                self.cyc = self.variant('Cyclone missile launcher', 25)

                self.lc = self.variant('Pair of lightning claws', 0, gear=Gear('Lightning claw', count=2))
                self.thss = self.variant('Thunder hammer and storm shield', 5,
                                         gear=[Gear('Thunder hammer'), Gear('Storm shield')])

                self.both = [self.lc, self.thss]

            def has_both(self):
                return any(opt.value for opt in self.both)

            def check_rules(self):
                super(DeathwingTerminatorSquad.Veteran.Weapon, self).check_rules()
                self.process_limit(self.both, 1)

        def __init__(self, parent):
            super(DeathwingTerminatorSquad.Veteran, self).__init__(
                parent=parent, name='Deathwing Terminator', points=DeathwingTerminatorSquad.model_points,
                gear=[Gear('Terminator Armour')]
            )

            self.left_weapon = self.LeftWeapon(self)
            self.right_weapon = self.RightWeapon(self)
            self.weapon = self.Weapon(self)

        def check_rules(self):
            super(DeathwingTerminatorSquad.Veteran, self).check_rules()
            for wep in [self.right_weapon, self.left_weapon]:
                wep.visible = wep.used = not self.weapon.has_both()
            self.right_weapon.enable_spec(not self.weapon.cyc.value)
            self.weapon.cyc.active = not self.right_weapon.has_spec()

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.weapon.cyc.value or self.right_weapon.has_spec()

    # FAQ: Any model can replace his storm bolter and powerfist with:
    class TerminatorSergeant(Unit):
        def __init__(self, parent):
            super(DeathwingTerminatorSquad.TerminatorSergeant, self).__init__(
                name='Deathwing Terminator Sergeant',
                parent=parent, points=DeathwingTerminatorSquad.model_points,
                gear=[Gear('Terminator Armour'), Gear('Power sword'), Gear('Storm bolter')],
            )

    def __init__(self, parent):
        super(DeathwingTerminatorSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.TerminatorSergeant(parent=None), visible=False)
        self.terms = UnitList(self, self.Veteran, min_limit=4, max_limit=9)
        self.transport = TerminatorTransport(self)

    def get_count(self):
        return self.terms.count + 1

    def check_rules(self):
        spec_limit = 1 if self.get_count() < 10 else 2
        spec = sum(c.has_spec() for c in self.terms.units)
        if spec > spec_limit:
            self.error('Deathwing Terminators may take only {0} special weapon (taken {1})'.format(spec_limit, spec))

    def get_unique_gear(self):
        return self.transport.get_unique_gear()


class DeathwingKnights(Unit):
    type_name = 'Deathwing Knights'
    type_id = 'deathwing_knights_v1'
    knights_points = 46
    master_points = 235 - 4 * knights_points

    class Relic(OptionsList):
        def __init__(self, parent):
            super(DeathwingKnights.Relic, self).__init__(parent=parent, name='Relic')
            self.relic = self.variant('Perfidious Relic of the Unforgiven', 10)

    def __init__(self, parent):
        super(DeathwingKnights, self).__init__(parent=parent, points=self.master_points)
        self.terms = Count(self, name='Deathwing Knight', min_limit=4, max_limit=9, points=self.knights_points,
                           per_model=True)
        self.relic = self.Relic(self)
        self.transport = TerminatorTransport(self)

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, count=self.get_count())
        desc.add(UnitDescription('Knight master', points=self.master_points, options=[
            Gear('Terminator Armour'), Gear('Flail of the Unforgiven'), Gear('Storm shield')
        ]))
        desc.add(UnitDescription('Deathwing Knight', points=self.knights_points, count=self.terms.cur, options=[
            Gear('Terminator Armour'), Gear('Mace of absolution'), Gear('Storm shield')
        ]))
        desc.add(self.relic.description)
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.terms.cur + 1

    def get_unique_gear(self):
        return self.transport.get_unique_gear()


class CompanyVeteransSquad(Unit):
    type_name = 'Company Veterans Squad'
    type_id = 'company_veterans_squad_v1'

    class Weapon(OneOf):
        def __init__(self, parent, special=False, heavy=False):
            super(CompanyVeteransSquad.Weapon, self).__init__(parent=parent, name='Weapon')
            self.special = special
            self.heavy = heavy

            self.bgun = self.variant('Boltgun', 0)
            self.chsw = self.variant('Chainsword', 0)
            self.sbgun = self.variant('Storm bolter', 5)
            self.cmelta = self.variant('Combi-melta', 10)
            self.cflame = self.variant('Combi-flamer', 10)
            self.cplasma = self.variant('Combi-plasma', 10)
            self.ppist = self.variant('Plasma pistol', 15)
            self.psw = self.variant('Power weapon', 15)
            self.lclaw = self.variant('Lightning claw', 15)
            self.pfist = self.variant('Power fist', 25)
            self.plclaw = self.variant('Pair of Lightning claws', 30, gear=[Gear('Lightning claw', count=2)])

            if self.special:
                self.flame = self.variant('Flamer', 5)
                self.mgun = self.variant('Meltagun', 10)
                self.pgun = self.variant('Plasmagun', 15)
                self.special = [self.flame, self.mgun, self.pgun]

            if self.heavy:
                self.hbgun = self.variant('Heavy bolter', 10)
                self.mulmgun = self.variant('Multi-melta', 10)
                self.mlaunch = self.variant('Missile launcher', 15)
                self.pcannon = self.variant('Plasma cannon', 15)
                self.lcannon = self.variant('Lascannon', 20)
                self.heavy = [self.hbgun, self.mulmgun, self.mlaunch, self.pcannon, self.lcannon]

        def has_heavy(self):
            return self.heavy and self.cur in self.heavy

        def has_special(self):
            return self.special and self.cur in self.special

        def has_missile_launcher(self):
            return self.heavy and self.cur == self.mlaunch

    class Options(OptionsList):
        def __init__(self, parent, weapon):
            super(CompanyVeteransSquad.Options, self).__init__(parent=parent, name='Options')
            self.weapon = weapon

            self.combatshield = self.variant('Combat shield', 5)
            self.meltabombs = self.variant('Melta bombs', 5)
            self.stormshield = self.variant('Storm shield', 10)
            self.flakkmissiles = self.variant('Flakk missiles', 10)

        def check_rules_chain(self):
            super(CompanyVeteransSquad.Options, self).check_rules()
            self.flakkmissiles.visible = self.flakkmissiles.used = self.weapon.has_missile_launcher()

    model_points = 18

    class Sergeant(Unit):
        def __init__(self, parent):
            super(CompanyVeteransSquad.Sergeant, self).__init__(
                parent=parent, name='Veteran Sergeant',
                points=90 - 4 * CompanyVeteransSquad.model_points, gear=[
                    Gear('Power armor'),
                    Gear('Frag grenades'),
                    Gear('Krak grenades'),
                    Gear('Bolt pistol'),
                ])
            self.wep = CompanyVeteransSquad.Weapon(parent=self)
            self.opt = CompanyVeteransSquad.Options(parent=self, weapon=self.wep)

    class Veteran(ListSubUnit):
        def __init__(self, parent):
            super(CompanyVeteransSquad.Veteran, self).__init__(
                parent=parent, name='Veteran',
                points=CompanyVeteransSquad.model_points, gear=[
                    Gear('Power armor'),
                    Gear('Frag grenades'),
                    Gear('Krak grenades'),
                    Gear('Bolt pistol'),
                ])
            self.wep = CompanyVeteransSquad.Weapon(parent=self, special=True, heavy=True)
            self.opt = CompanyVeteransSquad.Options(parent=self, weapon=self.wep)

        @ListSubUnit.count_gear
        def count_special(self):
            return self.wep.has_special()

        @ListSubUnit.count_gear
        def count_heavy(self):
            return self.wep.has_heavy()

    def __init__(self, parent):
        super(CompanyVeteransSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=CompanyVeteransSquad.Sergeant(parent=None))
        self.vets = UnitList(parent=self, unit_class=CompanyVeteransSquad.Veteran, min_limit=4, max_limit=9)
        self.transport = Transport(parent=self)

    def check_rules(self):
        spec_limit = int(self.get_count() / 5)
        spec_total = sum(u.count_special() for u in self.vets.units)
        if spec_limit < spec_total:
            self.error('Veterans may take only {0} special weapon (taken {1})'.format(spec_limit, spec_total))
        heavy_total = sum(u.count_heavy() for u in self.vets.units)
        if heavy_total > 1:
            self.error('Veterans may take only one heavy weapon (taken {0})'.format(heavy_total))

    def get_count(self):
        return self.vets.count + 1

    def get_unique_gear(self):
        return self.transport.get_unique_gear()


class Dreadnought(SpaceMarinesBaseVehicle):
    type_name = 'Dreadnought'
    type_id = 'dreadnought_v1'

    class Ranged(OneOf):
        def __init__(self, parent):
            super(Dreadnought.Ranged, self).__init__(parent=parent, name='Weapon')
            self.multimelta = self.variant('Multi-melta', 0)
            self.twinlinkedheavyflamer = self.variant('Twin-linked heavy flamer', 5)
            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 5)
            self.twinlinkedautocannon = self.variant('Twin-linked autocannon', 5)
            self.plasmacannon = self.variant('Plasma cannon', 10)
            self.assaultcannon = self.variant('Assault cannon', 20)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 25)

    class Melee(OneOf):
        def __init__(self, parent):
            super(Dreadnought.Melee, self).__init__(parent=parent, name='')
            self.build_in = None

            self.powerfist = self.variant('Power fist', 0)
            self.missilelauncher = self.variant('Missile launcher', 10)
            self.twinlinkedautocannon = self.variant('Twin-linked autocannon', 15)

        def check_rules(self):
            if self.build_in:
                self.build_in.visible = self.build_in.used = (self.cur == self.powerfist)

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(Dreadnought.BuildIn, self).__init__(parent=parent, name='')

            self.builtinstormbolter = self.variant('Built-in Storm bolter', 0, gear=Gear('Storm bolter'))
            self.builtinheavyflamer = self.variant('Built-in Heavy flamer', 10, gear=Gear('Heavy flamer'))

    class Options(OptionsList):
        def __init__(self, parent):
            super(Dreadnought.Options, self).__init__(parent=parent, name='Options')
            self.extraarmour = self.variant('Extra armour', 15)
            self.venerabledreadnought = self.variant('Venerable Dreadnought', 25,
                                                     gear=Gear('Deathwing vehicle'))

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Dreadnought.Transport, self).__init__(parent=parent, name='Transport', limit=1)
            self.drop = SubUnit(self, DropPod(parent=None))
            self.lucius = SubUnit(self, LuciusDropPod(parent=None))

        def check_rules(self):
            self.options.set_visible(self.lucius, self.roster.ia_enabled)

        def get_unique_gear(self):
            return sum((u.unit.get_unique_gear() for u in [self.drop, self.lucius] if u), [])

    def __init__(self, parent):
        super(Dreadnought, self).__init__(parent=parent, points=100, gear=[
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ])
        self.ranged = self.Ranged(self)
        self.melee = self.Melee(self)
        self.build_in = self.melee.build_in = self.BuildIn(self)
        self.opt = self.Options(self)
        self.transport = self.Transport(self)

    def build_description(self):
        desc = super(Dreadnought, self).build_description()
        if self.opt.venerabledreadnought.value:
            desc.name = 'Venerable Dreadnought'
        return desc

    def get_unique_gear(self):
        return super(Dreadnought, self).get_unique_gear() + self.transport.get_unique_gear()
