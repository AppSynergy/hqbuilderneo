__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'
__summary__ = ['Codex Dark Angels 6th edition', 'Dark Angels Librarius Conclave']

from builder.games.wh40k.roster import Wh40kBase, CombinedArmsDetachment,\
    AlliedDetachment, Wh40k7ed, HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, Fort, PrimaryDetachment, Formation
from builder.games.wh40k.escalation.space_marines import LordsOfWar
from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as Titans
from builder.games.wh40k.imperial_armour import volume2
from hq import *
from elites import *
from troops import *
from fast import *
from heavy import *


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.azrael = UnitType(self, Azrael)
        self.ezekiel = UnitType(self, Ezekiel)
        self.asmodai = UnitType(self, Asmodai)
        self.belial = UnitType(self, Belial)
        self.sammael = UnitType(self, Sammael)
        self.company_master = UnitType(self, CompanyMaster)
        self.librarian = UnitType(self, Librarian)
        self.interrogator = UnitType(self, InterrogatorChaplain)
        self.chaplain = UnitType(self, Chaplain)
        self.tech = UnitType(self, Techmarine, slot=0)
        self.command_squad = UnitType(self, CommandSquad, active=False, slot=0)
        self.deathwing_command_squad = UnitType(self, DeathwingCommandSquad, active=False, slot=0)
        self.ravenwing_command_squad = UnitType(self, RavenwingCommandSquad, active=False, slot=0)
        self.servitors = UnitType(self, Servitors, active=False, slot=0)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.company_veterans_squad = UnitType(self, CompanyVeteransSquad)
        self.deathwing_terminator_squad = UnitType(self, DeathwingTerminatorSquad)
        self.deathwing_knights = UnitType(self, DeathwingKnights)
        self.dreadnought = UnitType(self, Dreadnought)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.tactical_squad = UnitType(self, TacticalSquad)
        self.scout_squad = UnitType(self, ScoutSquad)
        self.deathwing_terminator_squad = UnitType(self, DeathwingTerminatorSquad, active=False)
        self.ravenwing_attack_squad = UnitType(self, RavenwingAttackSquadron, active=False)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        self.ravenwing_attack_squad = UnitType(self, RavenwingAttackSquadron)
        self.ravenwing_support_squad = UnitType(self, RavenwingSupportSquadron)
        self.ravenwing_black_knights = UnitType(self, RavenwingBlackKnights)
        self.ravenwing_dark_shroud = UnitType(self, RavenwingDarkShroud)
        self.assault_squad = UnitType(self, AssaultSquad)
        self.nephilim_jetfighter = UnitType(self, NephilimJetfighter)
        self.ravenwing_dark_talon = UnitType(self, RavenwingDarkTalon)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        self.devastators = UnitType(self, Devastators)
        self.predator = UnitType(self, Predator)
        self.whirlwind = UnitType(self, Whirlwind)
        self.vindicator = UnitType(self, Vindicator)
        self.land_raider = UnitType(self, LandRaider)
        self.land_raider_crusader = UnitType(self, LandRaiderCrusader)
        self.land_raider_redeemer = UnitType(self, LandRaiderRedeemer)
        self.land_speeder_vengeance = UnitType(self, LandSpeederVengeance)


class HQ(volume2.SpaceMarineHQ, BaseHQ):
    pass


class FastAttack(volume2.SpaceMarineFast, BaseFastAttack):
    pass


class Elites(volume2.DarkAngelsElites, BaseElites):
    pass


class HeavySupport(volume2.SpaceMarineHeavySupport, BaseHeavySupport):
    pass


class Lords(volume2.SpaceMarineLordsOfWar, Titans, LordsOfWar):
    pass


class DarkAngelsV2Base(Wh40kBase):
    army_id = 'dark_angels_v2_base'
    army_name = 'Dark Angels'
    # set IA enabled by default
    ia_enabled = True

    def __init__(self):
        super(DarkAngelsV2Base, self).__init__(
            hq=HQ(parent=self),
            elites=Elites(parent=self),
            troops=Troops(parent=self),
            fast=FastAttack(parent=self),
            heavy=HeavySupport(parent=self),
            fort=Fort(parent=self),
            lords=Lords(parent=self)
        )

    def check_rules(self):
        super(DarkAngelsV2Base, self).check_rules()

        tech_marines = self.hq.tech.count
        self.hq.servitors.active = tech_marines > 0
        if self.hq.servitors.count > tech_marines:
            self.error("You can only have one unit of Servitors per Techmarine.")

        if self.parent.parent is PrimaryDetachment:
            term = self.hq.azrael.count + self.hq.belial.count
            self.troops.deathwing_terminator_squad.active = term > 0
            self.elites.deathwing_terminator_squad.active = (term == 0)

            if term:
                self.troops.move_units(self.elites.deathwing_terminator_squad.units)
            else:
                self.elites.move_units(self.troops.deathwing_terminator_squad.units)

            raven = self.hq.azrael.count + self.hq.sammael.count
            self.troops.ravenwing_attack_squad.active = raven > 0
            self.fast.ravenwing_attack_squad.active = raven == 0
            if raven:
                self.troops.move_units(self.fast.ravenwing_attack_squad.units)
            else:
                self.fast.move_units(self.troops.ravenwing_attack_squad.units)
        else:
            self.troops.deathwing_terminator_squad.visible = self.troops.ravenwing_attack_squad.visible = False

        count = sum(unit_type.count for unit_type in [
            self.hq.command_squad, self.hq.deathwing_command_squad, self.hq.ravenwing_command_squad,
            self.hq.servitors, self.hq.tech])
        command = len(self.hq.units) - count

        self.hq.command_squad.active = self.hq.tech.active = command > 0
        if command < self.hq.command_squad.count:
            self.error("You can't have more Command Squads in HQ then Commanders")
        if command < self.hq.tech.count:
            self.error("You can't have more Techmarine in HQ then Commanders")

        term_units = self.hq.company_master.units + self.hq.interrogator.units + self.hq.librarian.units
        term_command = self.hq.belial.count + sum(1 for unit in term_units if unit.armour.is_tda())
        self.hq.deathwing_command_squad.active = term_command > 0
        if term_command < self.hq.deathwing_command_squad.count:
            self.error("You can't have more Deathwing Command Squads in HQ then Commanders in Terminator armour")

        bike_units = self.hq.chaplain.units + self.hq.interrogator.units + self.hq.librarian.units
        bike_command = self.hq.sammael.count + sum(1 for unit in bike_units if unit.opt.has_bike())
        self.hq.ravenwing_command_squad.active = bike_command > 0
        if bike_command < self.hq.ravenwing_command_squad.count:
            self.error("You can't have more Ravenwing Command Squads in HQ then Commanders on a Bike")


class DarkAngelsV2CAD(DarkAngelsV2Base, CombinedArmsDetachment):
    army_id = 'dark_angels_v2_cad'
    army_name = 'Dark Angels (Combined arms detachment)'


class DarkAngelsV2AD(DarkAngelsV2Base, AlliedDetachment):
    army_id = 'dark_angels_v2_ad'
    army_name = 'Dark Angels (Allied detachment)'


class Conclave(Formation):
    army_id = 'dark_angels_v2_conclave'
    army_name = 'Dark Angels Librarius Conclave'

    def __init__(self):
        super(Conclave, self).__init__()
        UnitType(self, Ezekiel, min_limit=1, max_limit=1)
        UnitType(self, Librarian, min_limit=2, max_limit=4)

faction = 'Dark_Angels'


class DarkAngelsV2(Wh40k7ed):
    army_id = 'dark_angels_v2'
    army_name = 'Dark Angels'
    faction = faction
    obsolete = True

    def __init__(self):
        super(DarkAngelsV2, self).__init__([DarkAngelsV2CAD, Conclave])

detachments = [DarkAngelsV2CAD, DarkAngelsV2AD, Conclave]
