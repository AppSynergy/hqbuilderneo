from builder.core2 import Unit, UnitDescription, SubUnit, UnitList, OneOf, OptionsList, ListSubUnit, Gear, Count
from armory import Weapon
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle

__author__ = 'Denis Romanov'


class AssaultSquad(Unit):
    type_name = 'Assault squad'
    type_id = 'assault_squad_v1'

    model_points = 17
    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades')]

    class Sergeant(Unit):
        class Options(OptionsList):
            def __init__(self, parent):
                super(AssaultSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.veteran = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent):
            super(AssaultSquad.Sergeant, self).__init__(
                name='Space Marine Sergeant',
                parent=parent, points=85 - 4 * AssaultSquad.model_points,
                gear=AssaultSquad.model_gear
            )
            self.wep1 = Weapon(self, 'Weapon', melee=True, ranged=True)
            self.wep2 = Weapon(self, '', pistol=True, melee=True, ranged=True)
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(AssaultSquad.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Veteran Sergeant'
            return desc

    class Transport(OptionsList):
        def __init__(self, parent):
            super(AssaultSquad.Transport, self).__init__(parent=parent, name='Options', limit=1)
            self.jumppack = self.variant('Jump Packs', 0)
            self.jumppack.value = True
            self.droppod = self.variant('Drop Pod', 0, gear=[])
            self.droppod_unit = None
            self.rhino = self.variant('Rhino', 0, gear=[])
            self.rhino_unit = None

        def check_rules(self):
            super(AssaultSquad.Transport, self).check_rules()
            self.rhino_unit.visible = self.rhino_unit.active = self.rhino.value
            self.droppod_unit.visible = self.droppod_unit.active = self.droppod.value

        @property
        def description(self):
            if self.jumppack.value:
                return super(AssaultSquad.Transport, self).description
            elif self.rhino.value:
                return self.rhino_unit.description
            elif self.droppod.value:
                return self.droppod_unit.description

    def __init__(self, parent):
        from transport import Rhino, DropPod
        super(AssaultSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.flame = Count(self, 'Flamer', 0, 2, 5)
        self.pp = Count(self, 'Plasma pistol', 0, 2, 15)
        self.transport = self.Transport(parent=self)
        self.transport.rhino_unit = SubUnit(self, Rhino(parent=self, points=0))
        self.transport.droppod_unit = SubUnit(self, DropPod(parent=self, points=0))

    def check_rules(self):
        super(AssaultSquad, self).check_rules()
        Count.norm_counts(0, 2, [self.flame, self.pp])

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=AssaultSquad.model_gear + [Gear('Chainsword')],
            points=AssaultSquad.model_points
        )

        count = self.marines.cur
        for o in [self.pp, self.flame]:
            if o.cur:
                spec_marine = marine.clone()
                spec_marine.points += o.option_points
                spec_marine.add(Gear(o.name))
                spec_marine.count = o.cur
                desc.add(spec_marine)
                count -= o.cur

        marine.add(Gear('Bolt pistol'))
        marine.count = count
        desc.add(marine)
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.marines.cur + 1

    def get_unique_gear(self):
        if self.transport.rhino.value:
            return self.transport.rhino_unit.unit.get_unique_gear()
        if self.transport.droppod.value:
            return self.transport.droppod_unit.unit.get_unique_gear()
        return []


class NephilimJetfighter(SpaceMarinesBaseVehicle):
    type_name = 'Nephilim Jetfighter'
    type_id = 'nephilim_jetfighter_v1'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(NephilimJetfighter.Weapon, self).__init__(parent=parent, name='Weapon')
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 0)
            self.avangermegabolter = self.variant('Avanger mega bolter', 0)

    def __init__(self, parent):
        super(NephilimJetfighter, self) .__init__(parent=parent, points=180, gear=[
            Gear('Twin-linked heavy bolter'),
            Gear('Blacksword missile', count=6)
        ])
        self.wep = self.Weapon(self)


class RavenwingDarkTalon(SpaceMarinesBaseVehicle):
    type_name = 'Ravenwing Dark Talon'
    type_id = 'ravenwing_dark_talon_v1'

    def __init__(self, parent):
        super(RavenwingDarkTalon, self) .__init__(parent=parent, points=160, gear=[
            Gear('Hurricane bolter', count=2),
            Gear('Rift cannon'),
            Gear('Stasis bomb'),
        ])


class RavenwingDarkShroud(SpaceMarinesBaseVehicle):
    type_name = 'Ravenwing Darkshroud'
    type_id = 'ravenwing_darkshroud_v1'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(RavenwingDarkShroud.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant('Heavy bolter', 0)
            self.assaultcannon = self.variant('Assault cannon', 20)

    def __init__(self, parent):
        super(RavenwingDarkShroud, self) .__init__(parent=parent, points=80)
        self.wep = self.Weapon(self)


class RavenwingSupportSquadron(Unit):
    type_name = "Ravenwing Support Squadron"
    type_id = "ravenwing_support_squadron_v1"

    class LandSpeeder(ListSubUnit, SpaceMarinesBaseVehicle):
        class BaseWeapon(OneOf):
            def __init__(self, parent):
                super(RavenwingSupportSquadron.LandSpeeder.BaseWeapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant('Heavy Bolter', 0)
                self.heavyflamer = self.variant('Heavy Flamer', 0)
                self.multimelta = self.variant('Multi-melta', 10)

        class UpWeapon(OptionsList):
            def __init__(self, parent):
                super(RavenwingSupportSquadron.LandSpeeder.UpWeapon, self).__init__(parent=parent, name='Upgrade',
                                                                                    limit=1)
                self.heavybolter = self.variant('Heavy Bolter', 10)
                self.heavyflamer = self.variant('Heavy Flamer', 10)
                self.multimelta = self.variant('Multi-melta', 20)
                self.typhoonmissilelauncher = self.variant('Typhoon Missile Launcher', 25)
                self.assaultcannon = self.variant('Assault Cannon', 30)

        def __init__(self, parent):
            super(RavenwingSupportSquadron.LandSpeeder, self).__init__(parent=parent, points=50, name="Land Speeder")
            self.wep = self.BaseWeapon(self)
            self.up = self.UpWeapon(self)

    def __init__(self, parent):
        super(RavenwingSupportSquadron, self).__init__(parent=parent)
        self.speeders = UnitList(parent=self, unit_class=self.LandSpeeder, min_limit=1, max_limit=5)

    def get_count(self):
        return self.speeders.count

    def get_unique_gear(self):
        return sum((u.get_unique_gear() for u in self.speeders.units), [])


class RavenwingBlackKnights(Unit):
    type_name = 'Ravenwing Black Knights'
    type_id = 'ravenwing_black_knights_v1'

    model_gear = [Gear('Power Armour'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Teleport Homer'),
                  Gear('Bolt pistol')]
    model_points = 42

    class Huntmaster(Unit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(RavenwingBlackKnights.Huntmaster.Weapon, self).__init__(parent=parent, name='Weapon')
                self.corvushummer = self.variant('Corvus hammer', 0)
                self.powersword = self.variant('Power sword', 12)
                self.powermaul = self.variant('Power maul', 12)

        class Options(OptionsList):
            def __init__(self, parent):
                super(RavenwingBlackKnights.Huntmaster.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)

        def __init__(self, parent):
            super(RavenwingBlackKnights.Huntmaster, self).__init__(
                name='Ravenwing Huntmaster',
                parent=parent, points=126 - RavenwingBlackKnights.model_points * 2,
                gear=RavenwingBlackKnights.model_gear + [Gear('Plasma talon')])
            self.weapon = self.Weapon(self)
            self.opt = self.Options(self)

    def __init__(self, parent):
        super(RavenwingBlackKnights, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Huntmaster(None))
        self.marines = Count(parent=self, name='Ravenwing Black Knight', min_limit=2, max_limit=9,
                             points=self.model_points, per_model=True)
        self.gl = Count(self, 'Ravenwing grenade launcher', 0, 1, 0)

    def get_count(self):
        return self.marines.cur + 1

    def check_rules(self):
        self.gl.max = int(self.get_count()/3)

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Ravenwing Black Knight',
            options=RavenwingBlackKnights.model_gear + [Gear('Corvus hammer')],
            points=RavenwingBlackKnights.model_points
        )
        spec_marine = marine.clone()
        spec_marine.add(Gear(self.gl.name))
        spec_marine.count = self.gl.cur
        desc.add(spec_marine)
        marine.add(Gear('Plasma talon'))
        marine.count = self.marines.cur - self.gl.cur
        desc.add(marine)
        return desc


class RavenwingAttackSquadron(Unit):
    type_name = 'Ravenwing Attack Squadron'
    type_id = 'ravenwing_attack_squadron_v1'

    model_gear = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Teleport homer'),
                  Gear('Twin-linked boltgun')]
    model_points = 27

    class Sergeant(Unit):
        class Options(OptionsList):
            def __init__(self, parent):
                super(RavenwingAttackSquadron.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.veteran = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent):
            super(RavenwingAttackSquadron.Sergeant, self).__init__(
                name='Ravenwing Sergeant',
                parent=parent, points=80 - 2 * RavenwingAttackSquadron.model_points,
                gear=RavenwingAttackSquadron.model_gear
            )
            self.wep = Weapon(self, '', pistol=True, melee=True, ranged=True)
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(RavenwingAttackSquadron.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Ravenwing Veteran Sergeant'
            return desc

    class AttackBike(Unit):
        model_name = 'Ravenwing Attack Bike'
        model_points = 45

        class Weapon(OneOf):
            def __init__(self, parent):
                super(RavenwingAttackSquadron.AttackBike.Weapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant('Heavy bolter', 0)
                self.multimelta = self.variant('Multi-melta', 10)

        def __init__(self, parent):
            super(RavenwingAttackSquadron.AttackBike, self).__init__(
                parent=parent, name=self.model_name, points=self.model_points,
                gear=RavenwingAttackSquadron.model_gear + [Gear('Bolt pistol')])
            self.wep = self.Weapon(self)

    class LandSpeeder(SpaceMarinesBaseVehicle):
        model_name = "Land Speeder"
        model_points = 50

        def __init__(self, parent):
            super(RavenwingAttackSquadron.LandSpeeder, self).__init__(parent=parent, points=self.model_points,
                                                                      name=self.model_name)
            self.wep = RavenwingSupportSquadron.LandSpeeder.BaseWeapon(self)
            self.up = RavenwingSupportSquadron.LandSpeeder.UpWeapon(self)

    class Biker(ListSubUnit):
        class BaseWeapon(OneOf):
            def __init__(self, parent):
                super(RavenwingAttackSquadron.Biker.BaseWeapon, self).__init__(parent=parent, name='Weapon')
                self.bolt = self.variant('Bolt pistol', 0)
                self.ccw = self.variant('Chainsword', 0)

        class UpWeapon(OptionsList):
            def __init__(self, parent):
                super(RavenwingAttackSquadron.Biker.UpWeapon, self).__init__(parent=parent, name='', limit=1)
                self.flamer = self.variant('Flamer', 5)
                self.melta = self.variant('Meltagun', 10)
                self.plasma = self.variant('Plasma gun', 15)

        def __init__(self, parent):
            super(RavenwingAttackSquadron.Biker, self).__init__(
                parent=parent, points=RavenwingAttackSquadron.model_points, name="Ravenwing Biker",
                gear=RavenwingAttackSquadron.model_gear)
            self.wep = self.BaseWeapon(self)
            self.up = self.UpWeapon(self)

        @ListSubUnit.count_gear
        def count_special(self):
            return self.up.any

    class OptUnits(OptionsList):
        def __init__(self, parent, bikers):
            self.bikers = bikers
            super(RavenwingAttackSquadron.OptUnits, self).__init__(parent=parent, name='', used=False)
            self.attack_bike = self.Variant(
                parent=self, name=RavenwingAttackSquadron.AttackBike.model_name,
                points=RavenwingAttackSquadron.AttackBike.model_points)
            self.speeder = self.Variant(
                parent=self, name=RavenwingAttackSquadron.LandSpeeder.model_name,
                points=RavenwingAttackSquadron.LandSpeeder.model_points)
            self.attack_bike_unit = SubUnit(parent=parent, unit=RavenwingAttackSquadron.AttackBike(None))
            self.speeder_unit = SubUnit(parent=parent, unit=RavenwingAttackSquadron.LandSpeeder(None))

        def check_rules(self):
            super(RavenwingAttackSquadron.OptUnits, self).check_rules()
            self.attack_bike_unit.visible = self.attack_bike_unit.used = self.attack_bike.value
            self.speeder.active = (self.bikers.count == 5)
            self.speeder_unit.used = self.speeder_unit.visible = self.speeder.active and self.speeder.value

        @property
        def count(self):
            return sum(1 for o in [self.attack_bike, self.speeder] if o.active and o.value)

    def __init__(self, parent):
        super(RavenwingAttackSquadron, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.bikers = UnitList(parent=self, unit_class=self.Biker, min_limit=2, max_limit=5)
        self.opt_units = self.OptUnits(self, self.bikers)

    def get_count(self):
        return self.opt_units.count + self.bikers.count + 1

    def check_rules(self):
        spec_total = sum(u.count_special() for u in self.bikers.units)
        if 2 < spec_total:
            self.error('Ravenwing Bikers may take only 2 special weapon (taken {0})'.format(spec_total))

    def get_unique_gear(self):
        if self.opt_units.speeder_unit.used:
            return self.opt_units.speeder_unit.unit.get_unique_gear()
        return []
