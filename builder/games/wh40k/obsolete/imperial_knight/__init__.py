__author__ = 'Denis Romanov'

from builder.core2 import Unit, Gear, UnitType
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, Section, Formation
from builder.games.wh40k.roster import Fort


class Paladin(Unit):
    type_name = 'Knight Paladin'
    type_id = 'knight_paladin_v1'

    def __init__(self, parent):
        super(Paladin, self).__init__(parent, points=375, gear=[
            Gear('Heavy stubber', count=2),
            Gear('Rapid-fire battle cannon'),
            Gear('Reaper chainsword'),
            Gear('Ion shield')
        ])


class Errant(Unit):
    type_name = 'Knight Errant'
    type_id = 'knight_errant_v1'

    def __init__(self, parent):
        super(Errant, self).__init__(parent, points=370, gear=[
            Gear('Heavy stubber'),
            Gear('Thermal cannon'),
            Gear('Reaper chainsword'),
            Gear('Ion shield')
        ])


class KnightSection(Section):
    def __init__(self, parent, secondary):
        super(KnightSection, self).__init__(
            parent, 'knight', 'Imperial Knights',
            min_limit=3 if not secondary else 1,
            max_limit=6 if not secondary else 3
        )
        UnitType(self, Paladin)
        UnitType(self, Errant)


class ALance(Formation):
    army_id = 'adamantine_lance'
    army_name = 'Adamantine Lance'

    def __init__(self, parent=None):
        super(ALance, self).__init__(parent)
        UnitType(self, Paladin)
        UnitType(self, Errant)

    def check_rules(self):
        super(ALance, self).check_rules()
        if len(self.units) != 3:
            self.errors.append('Must include 3 Knights Paladin or Knights Errant, taken:{}'.format(len(self.units)))


class ImperialKnightsPrimary(Wh40kBase):
    army_id = 'imperial_knights_prim'
    army_name = 'Imperial Knights (Army)'

    def __init__(self):
        self.knights = KnightSection(parent=self, secondary=False)
        super(ImperialKnightsPrimary, self).__init__(
            added=[self.knights],
            fort=Fort(parent=self)
        )


class ImperialKnightsAllied(Wh40kBase):
    army_id = 'imperial_knights_ally'
    army_name = 'Imperial Knights (Household detachment)'

    def __init__(self):
        self.knights = KnightSection(parent=self, secondary=True)
        super(ImperialKnightsAllied, self).__init__(
            added=[self.knights],
            fort=Fort(parent=self)
        )

faction = 'Imperial_Knights'


class ImperialKnights(Wh40k7ed):
    army_id = 'imperial_knights'
    army_name = 'Imperial Knights'
    faction = faction
    obsolete = True

    def __init__(self):
        super(ImperialKnights, self).__init__([ImperialKnightsPrimary, ALance])

detachments = [ImperialKnightsPrimary, ImperialKnightsAllied, ALance]
