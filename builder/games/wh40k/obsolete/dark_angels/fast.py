__author__ = 'Denis Romanov'

from builder.core.unit import ListSubUnit, ListUnit
from builder.games.wh40k.obsolete.dark_angels.transport import *
from builder.games.wh40k.obsolete.dark_angels.armory import *
from builder.core.options import norm_counts


class AssaultSquad(Unit):
    name = 'Assault squad'
    gear = ['Power armor', 'Frag grenades', 'Krak grenades', 'Chainsword']

    class Sergeant(Unit):
        base_points = 85 - 17 * 4
        name = 'Space Marine Sergeant'
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', melee + ranged)
            self.wep2 = self.opt_one_of('', ranged_pistol + melee)
            self.opt = self.opt_options_list('Options', [["Melta bombs", 5, 'mbomb']])
            self.vet = self.opt_options_list('', [["Veteran", 10, 'vet']])

        def check_rules(self):
            self.set_points(self.build_points())
            if self.vet.get('vet'):
                self.build_description(name='Veteran Sergeant', exclude=[self.vet.id])
            else:
                self.build_description()

    def get_count(self):
        return self.marines.get() + 1

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_count('Space Marine',4, 9, 17)
        self.flame = self.opt_count('Flamer', 0, 2, 5)
        self.pp = self.opt_count('Plasma pistol', 0, 2, 15)
        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.transport = self.opt_one_of('Options', [
            ['Jump Pack', 0, 'jp'],
            [DropPod.name, 0, 'dp'],
            [Rhino.name, 0, 'rh'],
        ])
        rh = Rhino()
        rh.base_points = 0
        self.rh = self.opt_sub_unit(rh)

        dp = DropPod()
        dp.base_points = 0
        self.dp = self.opt_sub_unit(dp)

    def check_rules(self):
        norm_counts(0, 2, [self.flame, self.pp])
        self.rh.set_active(self.transport.get_cur() == 'rh')
        self.dp.set_active(self.transport.get_cur() == 'dp')
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.marines.get(), exclude=[self.marines.id, self.transport.id])
        self.description.add('Bolt pistol', self.marines.get() - self.flame.get() - self.pp.get())
        if self.transport.get_cur() == 'jp':
            self.description.add(self.transport.get_selected(), self.get_count())


class NephilimJetfighter(Unit):
    name = 'Nephilim Jetfighter'
    base_points = 180
    gear = ['Twin-linked heavy bolter'] + ['Blacksword missile' for _ in range(6)]
    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Twin-linked lascannon', 0],
            ['Avanger mega bolter', 0],
        ])

class RavenwingDarkTalon(Unit):
    name = 'Ravenwing Dark Talon'
    base_points = 160
    gear = ['Hurricane bolter' for _ in range(2)] + ['Rift cannon', 'Stasis bomb']


class RavenwingDarkShroud(Unit):
    name = 'Ravenwing Dark Shroud'
    base_points = 80
    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Heavy bolter', 0],
            ['Assault cannon', 20],
            ])

class LandSpeeder(Unit):
    name = "Land Speeder"
    base_points = 50

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ["Heavy Bolter", 0, "bolt"],
            ["Heavy Flamer", 0, "flame"],
            ["Multi-melta", 10, "melta"],
            ])
        self.up = self.opt_options_list('Upgrade', [
            ["Heavy Bolter", 10, "bolt"],
            ["Heavy Flamer", 10, "flame"],
            ["Multi-melta", 20, "melta"],
            ["Typhoon Missile Launcher", 25, "tml"],
            ["Assault Cannon", 30, "assault"],
            ], limit = 1)


class RavenwingSupportSquadron(ListUnit):
    name = "Ravenwing Support Squadron"

    class LandSpeeder(ListSubUnit):
        name = "Land Speeder"
        base_points = 50
        max = 5

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ["Heavy Bolter", 0, "bolt"],
                ["Heavy Flamer", 0, "flame"],
                ["Multi-melta", 10, "melta"],
                ])
            self.up = self.opt_options_list('Upgrade', [
                ["Heavy Bolter", 10, "bolt"],
                ["Heavy Flamer", 10, "flame"],
                ["Multi-melta", 20, "melta"],
                ["Typhoon Missile Launcher", 25, "tml"],
                ["Assault Cannon", 30, "assault"],
                ], limit = 1)

    unit_class = LandSpeeder

class RavenwingBlackKnights(Unit):
    name = 'Ravenwing Black Knights'
    gear = ["Power Armour", 'Frag grenades', 'Krak grenades', 'Teleport Homer', 'Bolt pistol', 'Corvus hummer']

    class Huntmaster(Unit):
        name = 'Ravenwing Huntmaster'
        base_points = 126 - 42 * 2
        gear = ["Power Armour", 'Frag grenades', 'Krak grenades', 'Teleport Homer', 'Bolt pistol', 'Plasma talon']

        def __init__(self):
            Unit.__init__(self)

            self.wep1 = self.opt_one_of('Weapon', [
                ['Corvus hummer',0],
                ['Power sword', 12],
                ['Power maul', 12],
            ])
            self.opt = self.opt_options_list('Options', [["Melta bombs", 5, 'mbomb']])

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_count('Space Marine',2, 9, 42)
        self.gl = self.opt_count('Ravenwing grenade launcher', 0, 1, 0)

        self.vet = self.opt_sub_unit(self.Huntmaster())

    def get_count(self):
        return self.marines.get() + 1

    def check_rules(self):
        self.gl.update_range(0, int(self.get_count()/3))
        self.points.set(self.build_points(count=1))
        self.build_description(count=self.marines.get(), exclude=[self.marines.id, self.gl.id])
        self.description.add(self.gl.get_selected())
        self.description.add('Plasma talon', self.marines.get() - self.gl.get())


class RavenwingAttackSquadron(Unit):
    name = 'Ravenwing Attack Squadron'
    gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Teleport homer', 'Twin-linked boltgun']

    class Sergeant(Unit):
        base_points = 80 - 2 * 27
        name = 'Ravenwing Sergeant'
        gear = ['Power armor', 'Frag grenades', 'Krak grenades', 'Teleport homer', 'Twin-linked boltgun']

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', ranged_pistol + melee)
            self.opt = self.opt_options_list('Options', [["Melta bombs", 5, 'mbomb']])
            self.vet = self.opt_options_list('', [["Veteran", 10, 'vet']])

        def check_rules(self):
            self.set_points(self.build_points())
            if self.vet.get('vet'):
                self.build_description(name='Ravenwing Veteran Sergeant', exclude=[self.vet.id])
            else:
                self.build_description()

    class AttackBike(Unit):
        name = "Ravenwing Attack bike"
        base_points = 45
        gear = ['Power armour','Bolt pistol','Frag grenades','Krak grenades','Teleport homer', 'Twin-linked boltgun']
        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Heavy bolter',0,'hbgun'],
                ['Multi-melta',10,'mmelta']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.bikers = self.opt_count('Space Marine Biker',2, 5, 27)
        self.csw = self.opt_count('Chainsword', 0, 2, 0)
        self.flame = self.opt_count('Flamer', 0, 2, 5)
        self.melta = self.opt_count('Meltagun', 0, 2, 10)
        self.plasma = self.opt_count('Plasma gun', 0, 2, 15)

        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.abike = self.opt_optional_sub_unit('', [self.AttackBike()])
        self.support = self.opt_optional_sub_unit('', [LandSpeeder()])
        self.spec = [self.flame, self.plasma, self.melta]

    def get_count(self):
        return self.bikers.get() + self.abike.get_count() + self.sergeant.get_count() + self.support.get_count()

    def check_rules(self):
        norm_counts(0, 2, self.spec)
        self.csw.update_range(0, self.bikers.get())
        self.support.set_active(self.bikers.get() == 5)
        self.set_points(self.build_points(count=1))
        self.build_description(exclude=[self.bikers.id, self.csw.id], count=self.bikers.get())
        self.description.add('Bolt pistol', self.bikers.get() - self.csw.get() - reduce(lambda val, c: val + c.get(), self.spec, 0))
        self.description.add(self.csw.get_selected())

