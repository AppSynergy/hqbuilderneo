from builder.games.wh40k.obsolete.grey_knights import hq

__author__ = 'Denis Romanov'

from builder.core2 import *

Draigo = Unit.norm_core1_unit(hq.Draigo)
Mordrak = Unit.norm_core1_unit(hq.Mordrak)
Stern = Unit.norm_core1_unit(hq.Stern)
Crowe = Unit.norm_core1_unit(hq.Crowe)
GrandMaster = Unit.norm_core1_unit(hq.GrandMaster)
Captain = Unit.norm_core1_unit(hq.Captain)
Champion = Unit.norm_core1_unit(hq.Champion)
Librarian = Unit.norm_core1_unit(hq.Librarian)
Karamazov = Unit.norm_core1_unit(hq.Karamazov)
Coteaz = Unit.norm_core1_unit(hq.Coteaz)
Valeria = Unit.norm_core1_unit(hq.Valeria)
Xenos = Unit.norm_core1_unit(hq.Xenos)
Hereticus = Unit.norm_core1_unit(hq.Hereticus)
Malleus = Unit.norm_core1_unit(hq.Malleus)
