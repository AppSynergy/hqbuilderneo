__author__ = 'Ivan Truskov'

from builder.core2 import *
from armory import *
from transport import Transport, TerminatorTransport
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle


class Chronus(Unit):
    type_id = 'chronus_v1'
    type_name = 'Sergeant Chronus'

    def __init__(self, parent):
        super(Chronus, self).__init__(parent=parent, points=50, unique=True, static=True, gear=[
            Gear('Power armour'),
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Servo-arm'),
        ])


class LandRaider(SpaceMarinesBaseVehicle):
    type_id = 'land_raider_v1'
    type_name = "Land Raider"

    def __init__(self, parent):
        super(LandRaider, self).__init__(parent=parent, points=250, gear=[
            Gear('Twin-linked heavy bolter'),
            Gear('Twin-linked lascannon', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True, transport=True)
        self.opt = Vehicle(self, blade=False, melta=True)


class LandRaiderCrusader(SpaceMarinesBaseVehicle):
    type_id = 'land_raider_crusader_v1'
    type_name = "Land Raider Crusader"

    def __init__(self, parent):
        super(LandRaiderCrusader, self).__init__(parent=parent, points=250, gear=[
            Gear('Twin-linked assault cannon'),
            Gear('Hurricane bolter', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
            Gear('Frag Assault Launcher')
        ], tank=True, transport=True)
        self.opt = Vehicle(self, blade=False, melta=True)


class LandRaiderRedeemer(SpaceMarinesBaseVehicle):
    type_id = 'land_raider_redeemer_v1'
    type_name = "Land Raider Redeemer"

    def __init__(self, parent):
        super(LandRaiderRedeemer, self).__init__(parent=parent, points=240, gear=[
            Gear('Twin-linked assault cannon'),
            Gear('Flamestorm cannon', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
            Gear('Frag Assault Launcher'),
        ], tank=True, transport=True)
        self.opt = Vehicle(self, blade=False, melta=True)


class Predator(SpaceMarinesBaseVehicle):
    type_name = "Predator"
    type_id = "predator_v1"

    class Turret(OneOf):
        def __init__(self, parent):
            super(Predator.Turret, self).__init__(parent=parent, name='Turret')
            self.autocannon = self.variant('Autocannon', 0)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 25)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(Predator.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)
            self.sponsonswithheavybolters = self.variant('Sponsons with heavy bolters', 20,
                                                         gear=Gear('Heavy bolter', count=2))
            self.sponsonswithlascannons = self.variant('Sponsons with lascannons', 40,
                                                       gear=Gear('Lascannon', count=2))

    def __init__(self, parent):
        super(Predator, self).__init__(parent=parent, points=75, gear=[
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.turret = self.Turret(self)
        self.side = self.Sponsons(self)
        self.opt = Vehicle(self)


class Whirlwind(SpaceMarinesBaseVehicle):
    type_name = "Whirlwind"
    type_id = "Whirlwind_v1"

    def __init__(self, parent):
        super(Whirlwind, self).__init__(parent=parent, points=65, gear=[
            Gear('Whirlwind multiple missile launcher'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.opt = Vehicle(self)


class Vindicator(SpaceMarinesBaseVehicle):
    type_name = "Vindicator"
    type_id = "vindicator_v1"

    def __init__(self, parent):
        super(Vindicator, self).__init__(parent=parent, points=125, gear=[
            Gear('Demolisher cannon'),
            Gear('Storm bolter'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.opt = Vehicle(self, shield=True)


class Hunter(SpaceMarinesBaseVehicle):
    type_name = "Hunter"
    type_id = "hunter_v1"

    def __init__(self, parent):
        super(Hunter, self).__init__(parent=parent, points=70, gear=[
            Gear('Skyspear missile launcher'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.opt = Vehicle(self)


class Stalker(SpaceMarinesBaseVehicle):
    type_name = "Stalker"
    type_id = "stalker_v1"

    def __init__(self, parent):
        super(Stalker, self).__init__(parent=parent, points=75, gear=[
            Gear('Icarus stormcannon array'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.opt = Vehicle(self)


class Thunderfire(Unit):
    type_name = "Thunderfire cannon"

    def __init__(self, parent):
        super(Thunderfire, self).__init__(parent=parent, points=100, gear=[
            UnitDescription(name='Techmarine gunner', options=[
                Gear('Artificer armour'),
                Gear('Bolt pistol'),
                Gear('Frag grenades'),
                Gear('Krak grenades'),
                Gear('Servo-harness'),
            ])
        ])
        self.transport = Transport(self, drop_only=True)


class StormravenGunship(SpaceMarinesBaseVehicle):
    type_name = 'Stormraven Gunship'
    type_id = 'stormraven_gunship_v1'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(StormravenGunship.Weapon1, self).__init__(parent, 'Weapon')
            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 0)
            self.twinlinkedmultimelta = self.variant('Twin-linked Multi-melta', 0)
            self.typhoonmissilelauncher = self.variant('Typhoon missile launcher', 25)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(StormravenGunship.Weapon2, self).__init__(parent, '')
            self.twinlinkedassaultcannon = self.variant('Twin-linked Assault cannon', 0)
            self.twinlinkedplasmacannon = self.variant('Twin-linked Plasma cannon', 0)
            self.twinlinkedlascannon = self.variant('Twin-linked Lascannon', 0)

    class Options(OptionsList):
        def __init__(self, parent):
            super(StormravenGunship.Options, self).__init__(parent, 'Options')
            self.hurricanebolters = self.variant('Side sponsons with hurricane bolters', 30, gear=[
                Gear('Hurricane bolter', count=2)
            ])
            self.searchlight = self.variant('Searchlight', 1)
            self.extraarmour = self.variant('Extra armour', 5)
            self.locatorbeacon = self.variant('Locator beacon', 15)

    def __init__(self, parent):
        super(StormravenGunship, self).__init__(parent=parent, points=200, gear=[
            Gear('Ceramite plating'),
            Gear('Stormstrike missile', count=4),
        ], transport=True)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.opt = self.Options(self)


class Devastators(Unit):
    type_name = 'Devastator Squad'
    type_id = 'devastators_v1'

    model_points = 14
    model_gear = Armour.power_armour_set

    class Sergeant(Unit):
        class Options(OptionsList):
            def __init__(self, parent):
                super(Devastators.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.veteran = self.variant('Veteran', 10, gear=[])

        class Weapon1(Chainsword, Boltgun):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        def __init__(self, parent):
            super(Devastators.Sergeant, self).__init__(
                name='Space Marine Sergeant',
                parent=parent, points=70 - 4 * Devastators.model_points,
                gear=Devastators.model_gear + [Gear('Signum')]
            )
            self.wep1 = self.Weapon1(self, 'Weapon')
            self.wep2 = self.Weapon2(self, '')
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(Devastators.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Veteran Sergeant'
            return desc

    def __init__(self, parent):
        super(Devastators, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.hb = Count(parent=self, name='Heavy bolter', min_limit=0, max_limit=4, points=10)
        self.mm = Count(parent=self, name='Multi-melta', min_limit=0, max_limit=4, points=10)
        self.ml = Count(parent=self, name='Missile launcher', min_limit=0, max_limit=4, points=15)
        self.flakk = Count(self, name='Flakk missiles', min_limit=0, max_limit=4, points=10)
        self.pc = Count(parent=self, name='Plasma cannon', min_limit=0, max_limit=4, points=15)
        self.las = Count(parent=self, name='Lascannon', min_limit=0, max_limit=4, points=20)
        self.heavy = [self.hb, self.mm, self.ml, self.pc, self.las]
        self.transport = Transport(parent=self)

    def check_rules(self):
        Count.norm_counts(0, 4, self.heavy)
        self.flakk.max = self.ml.cur

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=Devastators.model_gear + [Gear('Bolt pistol')],
            points=Devastators.model_points
        )
        count = self.marines.cur
        for o in [self.hb, self.mm, self.pc, self.las]:
            if o.cur:
                spec_marine = marine.clone()
                spec_marine.points += o.option_points
                spec_marine.add(Gear(o.name))
                spec_marine.count = o.cur
                desc.add(spec_marine)
                count -= o.cur

        if self.ml.cur:
            ml_marine = marine.clone()
            ml_marine.points += self.ml.option_points
            ml_marine.add(Gear(self.ml.name))
            ml_marine.count = self.ml.cur - self.flakk.cur
            desc.add(ml_marine)
            count -= self.ml.cur
            if self.flakk.cur:
                ml_marine.points += self.flakk.option_points
                ml_marine.add(Gear(self.flakk.name))
                ml_marine.count = self.flakk.cur
                desc.add(ml_marine)

        marine.add(Gear('Boltgun'))
        marine.count = count
        desc.add(marine)
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.marines.cur + 1

    def get_unique_gear(self):
        return self.transport.get_unique_gear()


class CenturionDevastators(Unit):
    type_name = 'Centurion Devastator Squad'
    type_id = 'centurion_devastator_squad_v1'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CenturionDevastators.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.hurricanebolter = self.variant('Hurricane bolter', 0)
            self.missilelauncher = self.variant('Missile launcher', 10)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CenturionDevastators.Weapon2, self).__init__(parent=parent, name='')
            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 0)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 20)
            self.gravcannonandgravamp = self.variant('Grav-cannon and grav-amp', 20,
                                                     gear=[Gear('Grav-cannon'), Gear('Grav-amp')])

    class Centurion(ListSubUnit):

        def __init__(self, parent):
            super(CenturionDevastators.Centurion, self).__init__(parent=parent, name='Centurion', points=60)
            self.wep1 = CenturionDevastators.Weapon1(self)
            self.wep2 = CenturionDevastators.Weapon2(self)

    class Sergeant(Unit):

        class Options(OptionsList):
            def __init__(self, parent):
                super(CenturionDevastators.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.omniscope = self.variant('Omniscope', 10)

        def __init__(self, parent):
            super(CenturionDevastators.Sergeant, self).__init__(parent=parent, name='Centurion Sergeant', points=70)
            self.wep1 = CenturionDevastators.Weapon1(self)
            self.wep2 = CenturionDevastators.Weapon2(self)
            self.opt = self.Options(self)

    def __init__(self, parent):
        super(CenturionDevastators, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.Sergeant(parent=None))
        self.cent = UnitList(self, self.Centurion, min_limit=2, max_limit=5)
        self.transport = TerminatorTransport(self)

    def get_count(self):
        return self.cent.count + 1

    def get_unique_gear(self):
        return self.transport.get_unique_gear()
