from builder.core2 import *
from armory import *
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle

__author__ = 'Denis Romanov'


class AssaultSquad(Unit):
    type_name = 'Assault squad'
    type_id = 'assault_squad_v1'

    model_points = 17
    model_gear = Armour.power_armour_set

    class Sergeant(Unit):

        class Weapon1(PowerPistols, Melee, BoltPistol):
            pass

        class Weapon2(PowerPistols, Melee, Chainsword):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(AssaultSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.combatshiled = self.variant('Combat shield', 5)
                self.veteran = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent):
            super(AssaultSquad.Sergeant, self).__init__(
                name='Space Marine Sergeant',
                parent=parent, points=85 - 4 * AssaultSquad.model_points,
                gear=AssaultSquad.model_gear
            )
            self.wep1 = self.Weapon1(self, 'Weapon')
            self.wep2 = self.Weapon2(self, '')
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(AssaultSquad.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Veteran Sergeant'
            return desc

    class Transport(OptionsList):
        def __init__(self, parent):
            from transport import Rhino, DropPod
            super(AssaultSquad.Transport, self).__init__(parent=parent, name='Options', limit=1)
            self.jumppack = self.variant('Jump Packs', 0)
            self.jumppack.value = True
            self.rhino = self.variant('Rhino', 0, gear=[])
            self.rhino_unit = SubUnit(parent, Rhino(parent=self, points=0))
            self.droppod = self.variant('Drop Pod', 0, gear=[])
            self.droppod_unit = SubUnit(parent, DropPod(parent=self, points=0))

        def check_rules(self):
            super(AssaultSquad.Transport, self).check_rules()
            self.rhino_unit.visible = self.rhino_unit.used = self.rhino.value
            self.droppod_unit.visible = self.droppod_unit.used = self.droppod.value

        @property
        def description(self):
            if self.jumppack.value:
                return super(AssaultSquad.Transport, self).description
            elif self.rhino.value:
                return self.rhino_unit.description
            elif self.droppod.value:
                return self.droppod_unit.description

    def __init__(self, parent):
        super(AssaultSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.flame = Count(self, 'Flamer', 0, 2, 5)
        self.pp = Count(self, 'Plasma pistol', 0, 2, 15)
        self.transport = self.Transport(parent=self)

    def check_rules(self):
        super(AssaultSquad, self).check_rules()
        Count.norm_counts(0, 2, [self.flame, self.pp])

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=AssaultSquad.model_gear + [Gear('Chainsword')],
            points=AssaultSquad.model_points
        )

        count = self.marines.cur
        for o in [self.pp, self.flame]:
            if o.cur:
                desc.add(marine.clone().add_points(o.option_points).add(Gear(o.name)).set_count(o.cur))
                count -= o.cur

        desc.add(marine.add(Gear('Bolt pistol')).set_count(count))
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.marines.cur + 1

    def get_unique_gear(self):
        if self.transport.rhino_unit.used:
            return self.transport.rhino_unit.unit.get_unique_gear()
        if self.transport.droppod_unit.used:
            return self.transport.droppod_unit.unit.get_unique_gear()
        return []


class LandSpeederSquadron(Unit):
    type_name = "Land Speeder Squadron"
    type_id = "land_speeder_squadron_v1"

    class LandSpeeder(ListSubUnit, SpaceMarinesBaseVehicle):
        class BaseWeapon(OneOf):
            def __init__(self, parent):
                super(LandSpeederSquadron.LandSpeeder.BaseWeapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant('Heavy Bolter', 0)
                self.heavyflamer = self.variant('Heavy Flamer', 0)
                self.multimelta = self.variant('Multi-melta', 10)

        class UpWeapon(OptionsList):
            def __init__(self, parent):
                super(LandSpeederSquadron.LandSpeeder.UpWeapon, self).__init__(parent=parent, name='Upgrade', limit=1)
                self.heavybolter = self.variant('Heavy Bolter', 10)
                self.heavyflamer = self.variant('Heavy Flamer', 10)
                self.multimelta = self.variant('Multi-melta', 20)
                self.typhoonmissilelauncher = self.variant('Typhoon Missile Launcher', 25)
                self.assaultcannon = self.variant('Assault Cannon', 30)

        def __init__(self, parent):
            super(LandSpeederSquadron.LandSpeeder, self).__init__(parent=parent, points=50, name="Land Speeder")
            self.wep = self.BaseWeapon(self)
            self.up = self.UpWeapon(self)

        @ListSubUnit.count_unique
        def get_unique_gear(self):
            return SpaceMarinesBaseVehicle.get_unique_gear(self)

    def __init__(self, parent):
        super(LandSpeederSquadron, self).__init__(parent=parent)
        self.speeders = UnitList(parent=self, unit_class=self.LandSpeeder, min_limit=1, max_limit=3)

    def get_count(self):
        return self.speeders.count

    def get_unique_gear(self):
        return sum((u.get_unique_gear() for u in self.speeders.units), [])


class ScoutBikers(Unit):
    type_name = 'Scout Bike Squad'
    type_id = 'scout_bike_squad_v1'

    model_gear = Armour.scout_armour_set + [Gear('Space Marine bike'), Gear('Space Marine shotgun')]
    model_points = 18

    class Sergeant(Unit):
        class Options(OptionsList):
            def __init__(self, parent):
                super(ScoutBikers.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.locator = self.variant('Locator beacon', 10)
                self.veteran = self.variant('Veteran', 10, gear=[])

        class SergeantWeapon(Ranged, Melee, BoltPistol):
            pass

        def __init__(self, parent):
            super(ScoutBikers.Sergeant, self).__init__(
                name='Scout Biker Sergeant',
                parent=parent, points=54 - 2 * ScoutBikers.model_points,
                gear=ScoutBikers.model_gear + [Gear('Twin-linked boltgun')]
            )
            self.wep = self.SergeantWeapon(self, 'Weapon')
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(ScoutBikers.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Scout Biker Veteran Sergeant'
            return desc

    class Options(OptionsList):
        def __init__(self, parent):
            super(ScoutBikers.Options, self).__init__(parent=parent, name='Options')
            self.mines = self.variant('Cluster mines', 20)

    def __init__(self, parent):
        super(ScoutBikers, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))

        self.bikers = Count(self, 'Scout Biker', 2, 9, self.model_points, per_model=True, gear=[])
        self.grenade = Count(self, 'Astartes grenade launcher', 0, 3, 5, gear=[])
        self.opt = self.Options(self)

    def get_count(self):
        return self.bikers.cur + 1

    def check_rules(self):
        super(ScoutBikers, self).check_rules()
        self.grenade.max = min(self.bikers.cur, 3)

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Scout Biker',
            options=ScoutBikers.model_gear + [Gear('Bolt pistol')],
            points=ScoutBikers.model_points
        )
        count = self.bikers.cur
        if self.grenade.cur:
            desc.add(marine.clone()
                .add_points(self.grenade.option_points)
                .add(Gear(self.grenade.name))
                .set_count(self.grenade.cur))
            count -= self.grenade.cur
        if count:
            desc.add(marine.clone()
                .add(Gear('Twin-linked boltgun'))
                .set_count(count))
        desc.add(self.opt.description)
        return desc


class BikeSquad(Unit):
    type_name = 'Bike Squad'
    type_id = 'bike_squad_v1'

    model_gear = Armour.power_armour_set + [Gear('Space Marine bike'), Gear('Twin-linked boltgun')]
    model_points = 21

    class Sergeant(Unit):
        class Options(OptionsList):
            def __init__(self, parent):
                super(BikeSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.veteran = self.variant('Veteran', 10, gear=[])

        class SergeantWeapon(Ranged, Melee, BoltPistol):
            pass

        def __init__(self, parent):
            super(BikeSquad.Sergeant, self).__init__(
                name='Biker Sergeant',
                parent=parent, points=63 - 2 * BikeSquad.model_points,
                gear=BikeSquad.model_gear
            )
            self.wep = self.SergeantWeapon(self, 'Weapon')
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(BikeSquad.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Veteran Biker Sergeant'
            return desc

    class AttackBike(Unit):
        model_name = 'Attack Bike'
        model_points = 45

        class Weapon(OneOf):
            def __init__(self, parent):
                super(BikeSquad.AttackBike.Weapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant('Heavy bolter', 0)
                self.multimelta = self.variant('Multi-melta', 10)

        def __init__(self, parent):
            super(BikeSquad.AttackBike, self).__init__(
                parent=parent, name=self.model_name, points=self.model_points,
                gear=BikeSquad.model_gear + [Gear('Bolt pistol')])
            self.wep = self.Weapon(self)

    class OptUnits(OptionalSubUnit):
        def __init__(self, parent):
            super(BikeSquad.OptUnits, self).__init__(parent=parent, name='')
            self.attackbike = SubUnit(self, BikeSquad.AttackBike(parent=None))

    def __init__(self, parent, troops=False):
        super(BikeSquad, self).__init__(parent=parent)
        self.troops = troops
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.opt_units = self.OptUnits(self)

        self.bikers = Count(self, 'Space Marine Biker', 4 if self.troops else 2, 7, self.model_points, per_model=True,
                            gear=[])
        self.flame = Count(self, 'Flamer', 0, 2, 5, gear=[])
        self.melta = Count(self, 'Meltagun', 0, 2, 10, gear=[])
        self.grav = Count(self, 'Grav-gun', 0, 2, 15, gear=[])
        self.plasma = Count(self, 'Plasma gun', 0, 2, 15, gear=[])

    def check_rules(self):
        super(BikeSquad, self).check_rules()
        if self.troops:
            self.bikers.min = 4 - self.opt_units.count

    def get_count(self):
        return self.opt_units.count + self.bikers.cur + 1

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine Biker',
            options=BikeSquad.model_gear + [Gear('Bolt pistol')],
            points=BikeSquad.model_points
        )
        count = self.bikers.cur
        for o in [self.flame, self.melta, self.grav, self.plasma]:
            if o.cur:
                desc.add(marine.clone().add_points(o.option_points).add(Gear(o.name)).set_count(o.cur))
                count -= o.cur
        desc.add(marine.set_count(count))
        desc.add(self.opt_units.description)
        return desc


class TroopsBikeSquad(BikeSquad):
    def __init__(self, parent):
        super(TroopsBikeSquad, self).__init__(parent=parent, troops=True)


class Stormtalon(SpaceMarinesBaseVehicle):
    type_name = 'Stormtalon Gunship'
    type_id = 'stormtalongunship_v1'

    def __init__(self, parent):
        super(Stormtalon, self).__init__(parent=parent, points=110,
                                         gear=[Gear('Ceramite plating'), Gear('Twin-linked assault cannon')])
        self.weapon = self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Stormtalon.Weapon, self).__init__(parent=parent, name='Weapon')

            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 0)
            self.skyhammermissilelauncher = self.variant('Skyhammer missile launcher', 15)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 30)
            self.typhoonmissilelauncher = self.variant('Typhoon missile launcher', 35)


class AttackBikeSquad(Unit):
    type_name = 'Attack Bike Squad'
    type_id = 'attackbike_v1'

    model_points = 45
    model_gear = Armour.power_armour_set + [Gear('Bolt pistol'), Gear('Space Marine bike'), Gear('Twin-linked boltgun')]
    model_name = 'Attack Bike'

    def __init__(self, parent):
        super(AttackBikeSquad, self).__init__(parent)
        self.bikers = Count(self, self.model_name, 1, 3, self.model_points, per_model=True)
        self.melta = Count(self, 'Multi-melta', 0, 1, 10, per_model=True)

    def check_rules(self):
        super(AttackBikeSquad, self).check_rules()
        self.melta.max = self.bikers.cur

    def get_count(self):
        return self.bikers.cur

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        marine = UnitDescription(name=self.model_name, options=self.model_gear, points=self.model_points)
        count = self.bikers.cur
        if self.melta.cur:
            desc.add(marine.clone()
                .add_points(self.melta.option_points)
                .add(Gear(self.melta.name))
                .set_count(self.melta.cur))
            count -= self.melta.cur
        if count:
            desc.add(marine.clone()
                .add(Gear('Heavy bolter'))
                .set_count(count))
        return desc
