__author__ = 'dante'

from builder.core2 import *
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle
from builder.games.wh40k.imperial_armour.volume2.transport import InfernumRazorback, LuciusDropPod, IATransport


class Rhino(SpaceMarinesBaseVehicle):
    type_name = 'Rhino'
    type_id = 'rhino_v1'

    def __init__(self, parent, off=0):
        super(Rhino, self).__init__(parent=parent, points=50 - off, tank=True, transport=True,
                                    gear=[Gear('Storm Bolter'), Gear('Smoke Launchers')])
        self.Options(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Rhino.Options, self).__init__(parent=parent, name='Options', limit=None)

            self.dozenblade = self.variant('Dozen Blade', 5)
            self.stormbolter = self.variant('Storm Bolter', 10)
            self.hinterkillermissile = self.variant('Hinter-killer Missile', 10)
            self.extraarmour = self.variant('Extra Armour', 15)
            self.searchlight = self.variant('Searchlight', 1)


class Razorback(SpaceMarinesBaseVehicle):
    type_name = 'Razorback'
    type_id = 'razorback_v1'

    def __init__(self, parent, off=0):
        super(Razorback, self).__init__(parent=parent, points=55 - off, gear=[Gear('Smoke Launchers')],
                                        tank=True, transport=True)
        self.Weapon(self)
        Rhino.Options(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Razorback.Weapon, self).__init__(parent=parent, name='Weapon')

            self.twinlinkedheavybolter = self.variant('Twin-linked Heavy Bolter', 0)
            self.twinlinkedheavyflamer = self.variant('Twin-linked Heavy Flamer', 0)
            self.twinlinkedassaultcannon = self.variant('Twin-linked Assault Cannon', 35)
            self.twinlinkedlascannon = self.variant('Twin-linked Lascannon', 35)
            self.lascannonandtwinlinkedplasmagun = self.variant('Lascannon and Twin-linked Plasma Gun', 35)


class DropPod(SpaceMarinesBaseVehicle):
    type_name = 'Drop Pod'
    type_id = 'droppod_v1'

    def __init__(self, parent, off=0):
        super(DropPod, self).__init__(parent=parent, points=35 - off, deep_strike=True, transport=True)
        self.Weapon(self)
        self.Options(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DropPod.Weapon, self).__init__(parent=parent, name='Weapon')
            self.stormbolter = self.variant('Storm Bolter', 0)
            self.deathwindmissilelauncher = self.variant('Deathwind Missile Launcher', 20)

    class Options(OptionsList):
        def __init__(self, parent):
            super(DropPod.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.locatorbeacon = self.variant('Locator Beacon', 10)


class LandRaider(SpaceMarinesBaseVehicle):
    type_name = 'Land Raider'
    type_id = 'landraider_v1'

    def __init__(self, parent, off=0):
        super(LandRaider, self).__init__(
            parent=parent, points=250 - off, tank=True, transport=True,
            gear=[Gear('Twin-linked Heavy Bolter'), Gear('Twin-linked Lascannon', count=2), Gear('Smoke Launchers')]
        )
        self.Options(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(LandRaider.Options, self).__init__(parent=parent, name='Options', limit=None)

            self.stormbolter = self.variant('Storm Bolter', 10)
            self.hinterkillermissile = self.variant('Hinter-killer Missile', 10)
            self.multimelta = self.variant('Multi-melta', 10)
            self.extraarmour = self.variant('Extra Armour', 15)
            self.searchlight = self.variant('Searchlight', 1)


class LandRaiderCrusader(SpaceMarinesBaseVehicle):
    type_name = 'Land Raider Crusader'
    type_id = 'landraidercrusader_v1'

    def __init__(self, parent, off=0):
        super(LandRaiderCrusader, self).__init__(
            parent=parent, points=250 - off, tank=True, transport=True,
            gear=[Gear('Twin-linked Assault Cannon'), Gear('Hurricane Bolter', count=2), Gear('Smoke Launchers'),
                  Gear('Frag Assault Launcher')]
        )
        LandRaider.Options(self)


class LandRaiderRedeemer(SpaceMarinesBaseVehicle):
    type_name = 'Land Raider Redeemer'
    type_id = 'landraiderredeemer_v1'

    def __init__(self, parent, off=0):
        super(LandRaiderRedeemer, self).__init__(
            parent=parent, points=240 - off, tank=True, transport=True,
            gear=[Gear('Twin-linked Assault Cannon'), Gear('Flamestorm Cannon', count=2), Gear('Frag Assault Launcher'),
                  Gear('Smoke Launchers')])
        LandRaider.Options(self)


class Transport(IATransport):
    def __init__(self, parent, off=0):
        super(Transport, self).__init__(parent=parent, name='Transport')
        self.models.extend([
            SubUnit(self, Rhino(parent=None, off=off)),
            SubUnit(self, Razorback(parent=None, off=off)),
            SubUnit(self, DropPod(parent=None, off=off)),
            SubUnit(self, LandRaider(parent=None, off=off)),
            SubUnit(self, LandRaiderCrusader(parent=None, off=off)),
            SubUnit(self, LandRaiderRedeemer(parent=None, off=off)),
        ])
        self.ia_models.append(SubUnit(self, InfernumRazorback(parent=None)))


class TerminatorTransport(IATransport):
    def __init__(self, parent, off=0):
        super(TerminatorTransport, self).__init__(parent=parent, name='Transport')
        self.models.extend([
            SubUnit(self, LandRaider(parent=None, off=off)),
            SubUnit(self, LandRaiderCrusader(parent=None, off=off)),
            SubUnit(self, LandRaiderRedeemer(parent=None, off=off)),
        ])


class DreadnoughtTransport(IATransport):
    def __init__(self, parent):
        super(DreadnoughtTransport, self).__init__(parent=parent, name='Transport')
        self.models.append(SubUnit(self, DropPod(parent=None)))
        self.ia_models.append(SubUnit(self, LuciusDropPod(parent=None)))
