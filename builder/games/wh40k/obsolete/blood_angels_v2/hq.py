from builder.games.wh40k.obsolete.blood_angels import hq

__author__ = 'Denis Romanov'

from builder.core2 import *
from transport import Transport
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedUnit

Dante = Unit.norm_core1_unit(hq.Dante)
Gabriel = Unit.norm_core1_unit(hq.Gabriel)
Astorath = Unit.norm_core1_unit(hq.Astropath)
Sanguinor = Unit.norm_core1_unit(hq.Sanguinor)
Mephiston = Unit.norm_core1_unit(hq.Mephiston)
Tycho = Unit.norm_core1_unit(hq.Tycho)
Librarian = Unit.norm_core1_unit(hq.Librarian)
Reclusiarch = Unit.norm_core1_unit(hq.Reclusiarch)
Captain = Unit.norm_core1_unit(hq.Captian)


class HonourGuards(IATransportedUnit):
    type_name = 'Honour Guard'
    type_id = 'honourguard_v1'

    model_points = 115 / 5
    model_gear = [Gear('Power Armour'), Gear('Frag and Krak Grenades')]

    def __init__(self, parent):
        super(HonourGuards, self).__init__(parent=parent, points=0)
        SubUnit(self, self.SanguinaryNovitiate(None))
        self.guards = UnitList(self, self.HonorGuard, 1, 4, start_value=4)
        self.opt = self.Options(self)

    def get_count(self):
        return self.guards.count + 1 + self.opt.bloodchampion.value

    def check_rules(self):
        super(HonourGuards, self).check_rules()
        flag = sum(c.count_banners() for c in self.guards.units)
        if flag > 1:
            self.error('Only one Honour Guard can take a banner (taken: {})'.format(flag))
        if self.get_count() != 5:
            self.error('Honour Guard must include 5 Honour Guards (include Sanguinary Novitiate and '
                       'Blood Champion) (taken: {})'.format(self.get_count()))

    def get_unique_gear(self):
        return sum((unit.banners.get_unique() for unit in self.guards.units),
                   super(HonourGuards, self).get_unique_gear())

    class SanguinaryNovitiate(Unit):

        def __init__(self, parent):
            super(HonourGuards.SanguinaryNovitiate, self).__init__(
                parent=parent, points=HonourGuards.model_points, name='Sanguinary Novitiate',
                gear=HonourGuards.model_gear + [Gear('Chain Sword'), Gear('Blood Chalice')]
            )
            self.Weapon(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(HonourGuards.SanguinaryNovitiate.Weapon, self).__init__(parent=parent, name='Weapon')
                self.boltpistol = self.variant('Bolt Pistol', 0)
                self.boltgun = self.variant('Boltgun', 0)

    class HonorGuard(ListSubUnit):

        def __init__(self, parent):
            super(HonourGuards.HonorGuard, self).__init__(
                parent=parent, name='Honour Guard', points=HonourGuards.model_points, gear=HonourGuards.model_gear
            )
            self.Weapon(self, 'Weapon', melee=True)
            self.Weapon(self, '', ranged=True)
            self.Options(self)
            self.banners = self.Banners(self)

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.banners.any

        class Weapon(OneOf):
            def __init__(self, parent, name, melee=False, ranged=False):
                super(HonourGuards.HonorGuard.Weapon, self).__init__(parent=parent, name=name)

                if ranged:
                    self.boltpistol = self.variant('Bolt Pistol', 0)
                    self.boltgun = self.variant('Boltgun', 0)
                if melee:
                    self.chainsword = self.variant('Chain Sword', 0)

                self.stormbolter = self.variant('Storm Bolter', 3)
                self.flamer = self.variant('Flamer', 5)
                self.meltagun = self.variant('Meltagun', 10)
                self.combiflamer = self.variant('Combi-flamer', 10)
                self.combimelta = self.variant('Combi-melta', 10)
                self.combiplasma = self.variant('Combi-plasma', 10)
                self.handflamer = self.variant('Hand flamer', 10)
                self.plasmapistol = self.variant('Plasma Pistol', 15)
                self.plasmagun = self.variant('Plasma Gun', 15)
                self.infernuspistol = self.variant('Infernus Pistol', 15)
                self.powerweapon = self.variant('Power Weapon', 15)
                self.lightningclaw = self.variant('Lightning Claw', 15)
                self.stormshield = self.variant('Storm Shield', 20)
                self.powerfist = self.variant('Power Fist', 25)
                self.thunderhummer = self.variant('Thunder Hummer', 30)

        class Options(OptionsList):
            def __init__(self, parent):
                super(HonourGuards.HonorGuard.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta-bombs', 5)

        class Banners(OptionsList):
            def __init__(self, parent):
                super(HonourGuards.HonorGuard.Banners, self).__init__(parent=parent, name='', limit=1)
                self.companystandard = self.variant('Company Standard', 15)
                self.chapterbanner = self.variant('Chapter Banner', 30)

            def get_unique(self):
                if self.chapterbanner.value:
                    return self.description
                return []

    class Options(OptionsList):

        def __init__(self, parent):
            champion_points = HonourGuards.model_points + 20
            super(HonourGuards.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.bloodchampion = self.variant('Blood Champion', champion_points, gear=[
                UnitDescription('Blood Champion', points=champion_points,
                                options=HonourGuards.model_gear + [Gear('Power Weapon'), Gear('Combat Shield')])
            ])
            self.jumppacks = self.variant('Jump Packs', 50)
            self.parent.transport = Transport(parent)

        def check_rules(self):
            super(HonourGuards.Options, self).check_rules()
            self.parent.transport.visible = self.parent.transport.used = not self.jumppacks.value
            self.jumppacks.active = self.jumppacks.used = not self.parent.transport.options.any
