# -*- coding: utf-8 -*-

__author__ = 'Ivan Truskov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.old_roster import Ally
from builder.games.wh40k.obsolete.chaos_marines.hq import *
from builder.games.wh40k.obsolete.chaos_marines.elites import *
from builder.games.wh40k.obsolete.chaos_marines.troops import *
from builder.games.wh40k.obsolete.chaos_marines.fast import *
from builder.games.wh40k.obsolete.chaos_marines.heavy import *


class ChaosMarines(LegacyWh40k):
    army_id = '081ad3e444144c80bf2c0e4f152f61a7'
    army_name = 'Chaos Space Marines'
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(
            self,
            hq=[
                Abaddon, Kharn, Huron, Ahriman, Typhus, Lucius, Fabius,
                {'unit': LordV2, 'type_id': 'lord_v2'},
                {'unit': SorcV2, 'type_id': 'sorc_v2'},
                {'unit': DaemonPrinceV2, 'type_id': 'daemon_v2'},
                {'unit': WarpsmithV2, 'type_id': 'warpsmith_v2'},
                {'unit': ApostleV2, 'type_id': 'apostile_v2'},
                {'unit': Lord, 'deprecated': True},
                {'unit': Sorc, 'deprecated': True},
                {'unit': DaemonPrince, 'deprecated': True},
                {'unit': Warpsmith, 'deprecated': True},
                {'unit': Apostle, 'deprecated': True},
            ],
            elites=[Chosen, Possessed, Terminators, Hellbrute, Mutilators, Berzerks, ThousandSons, PlagueMarines,
                    NoiseMarines],
            troops=[
                ChaosSpaceMarines,
                ChaosCultists,
                {'unit': PlagueZombie, 'active': False},
                {'unit': Chosen, 'active': False},
                {'unit': PlagueMarines, 'active': False},
                {'unit': ThousandSons, 'active': False},
                {'unit': NoiseMarines, 'active': False},
                {'unit': Berzerks, 'active': False}
            ],
            fast=[Bikers, Spawn, Raptors, WarpTalons, Helldrake],
            heavy=[Havoks, Obliterators, Defiler, Forgefiend, Maulerfiend, LandRaider, Vindicator, Predator],
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.CSM)
        )

    def check_rules(self):
        #chosen as troops
        abb = self.hq.count_unit(Abaddon) > 0
        self.troops.set_active_types([Chosen], abb)
        self.elites.set_active_types([Chosen], not abb)
        if not abb and self.troops.count_unit(Chosen) > 0:
            self.error("You can't have Chosen in Troops without Abaddon in HQ.")
        if abb and self.elites.count_unit(Chosen) > 0:
            self.error("You can't have Chosen in Elites with Abaddon in HQ.")

        #plague zombies as troops
        typh = self.hq.count_unit(Typhus) > 0
        self.troops.set_active_types([PlagueZombie], typh)

        lords = self.hq.get_units(LordV2) + self.hq.get_units(Lord)

        #plague marines as troops
        plag = typh or any((u.mark.get('ng') for u in lords))
        self.troops.set_active_types([PlagueMarines], plag)
        self.elites.set_active_types([PlagueMarines], not plag)
        if not plag and self.troops.count_unit(PlagueMarines) > 0:
            self.error("You can't have Plague Marines in Troops without Chaos Lord with the Mark of Nurgle "
                       "(including Typhus) in HQ.")
        if plag and self.elites.count_unit(PlagueMarines) > 0:
            self.error("You can't have Plague Marines in Elites with Chaos Lord with the Mark of Nurgle "
                       "(including Typhus) in HQ.")

        #noise marines as troops
        noise = self.hq.count_unit(Lucius) > 0 or any((u.mark.get('sl') for u in lords))
        self.troops.set_active_types([NoiseMarines], noise)
        self.elites.set_active_types([NoiseMarines], not noise)
        if not noise and self.troops.count_unit(NoiseMarines) > 0:
            self.error("You can't have Noise Marines in Troops without Chaos Lord with the Mark of Slaanesh "
                       "(including Lucius) in HQ.")
        if noise and self.elites.count_unit(NoiseMarines) > 0:
            self.error("You can't have Noise Marines in Elites with Chaos Lord with the Mark of Slaanesh "
                       "(including Lucius) in HQ.")

        #berserkers as troops
        bers = self.hq.count_unit(Kharn) > 0 or any((u.mark.get('kh') for u in lords))
        self.troops.set_active_types([Berzerks], bers)
        self.elites.set_active_types([Berzerks], not bers)
        if not bers and self.troops.count_unit(Berzerks) > 0:
            self.error("You can't have Khorne Berzerkers in Troops without Chaos Lord with the Mark of Khorne "
                       "(including Khârn the Betrayer) in HQ.")
        if bers and self.elites.count_unit(Berzerks) > 0:
            self.error("You can't have Khorne Berzerkers in Elites with Chaos Lord with the Mark of Khorne "
                       "(including Khârn the Betrayer) in HQ.")

        sorcs = self.hq.get_units(SorcV2) + self.hq.get_units(Sorc)
        #rubruc marines as troops
        rubr = self.hq.count_unit(Ahriman) > 0 or any((u.mark.get('tz') for u in sorcs))
        self.troops.set_active_types([ThousandSons], rubr)
        self.elites.set_active_types([ThousandSons], not rubr)
        if not rubr and self.troops.count_unit(ThousandSons) > 0:
            self.error("You can't have Thousand Sons in Troops without Sorcerer with the Mark of Tzeentch "
                       "(including Ahriman) in HQ.")
        if rubr and self.elites.count_unit(ThousandSons) > 0:
            self.error("You can't have Thousand Sons in Elites with Sorcerer with the Mark of Tzeentch "
                       "(including Ahriman) in HQ.")


def bl(base_class):
    class BlackLegionUnit(base_class):
        def __init__(self):
            base_class.__init__(self, black_legion=True)
    BlackLegionUnit.__name__ = 'BlackLegion' + base_class.__name__
    return BlackLegionUnit


BlackLegionLord = bl(LordV2)
BlackLegionSorc = bl(SorcV2)
BlackLegionDaemonPrince = bl(DaemonPrinceV2)
BlackLegionWarpsmith = bl(WarpsmithV2)
BlackLegionApostle = bl(ApostleV2)
BlackLegionPossessed = bl(Possessed)
BlackLegionTerminators = bl(Terminators)
BlackLegionMutilators = bl(Mutilators)
BlackLegionBerzerks = bl(Berzerks)
BlackLegionPlagueMarines = bl(PlagueMarines)
BlackLegionNoiseMarines = bl(NoiseMarines)
BlackLegionChosen = bl(Chosen)
BlackLegionChaosSpaceMarines = bl(ChaosSpaceMarines)
BlackLegionBikers = bl(Bikers)
BlackLegionRaptors = bl(Raptors)
BlackLegionWarpTalons = bl(WarpTalons)
BlackLegionHavoks = bl(Havoks)
BlackLegionObliterators = bl(Obliterators)


class BlackLegion(LegacyWh40k):
    army_id = 'd8008ef439b14a1b933886a02a9bd1ad'
    army_name = 'Chaos Space Marines: Black Legion'
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(
            self,
            hq=[Abaddon, Kharn, Huron, Ahriman, Typhus, Lucius, Fabius, BlackLegionLord, BlackLegionSorc,
                BlackLegionDaemonPrince, BlackLegionWarpsmith, BlackLegionApostle],
            elites=[BlackLegionPossessed, BlackLegionTerminators, Hellbrute, BlackLegionMutilators, BlackLegionBerzerks,
                    ThousandSons, BlackLegionPlagueMarines, BlackLegionNoiseMarines],
            troops=[
                BlackLegionChosen,
                BlackLegionChaosSpaceMarines,
                ChaosCultists,
                {'unit': PlagueZombie, 'active': False},
                {'unit': BlackLegionPlagueMarines, 'active': False},
                {'unit': ThousandSons, 'active': False},
                {'unit': BlackLegionNoiseMarines, 'active': False},
                {'unit': BlackLegionBerzerks, 'active': False}
            ],
            fast=[BlackLegionBikers, Spawn, BlackLegionRaptors, BlackLegionWarpTalons, Helldrake],
            heavy=[BlackLegionHavoks, BlackLegionObliterators, Defiler, Forgefiend, Maulerfiend, LandRaider, Vindicator,
                   Predator],
            secondary=secondary,
            ally=Ally(parent=self, ally_type=Ally.CSM)
        )

    def has_abaddon(self):
        return self.hq.count_unit(Abaddon) > 0

    def check_rules(self):
        #plague zombies as troops
        typh = self.hq.count_unit(Typhus) > 0
        self.troops.set_active_types([PlagueZombie], typh)

        lords = self.hq.get_units(BlackLegionLord)
        #plague marines as troops
        plag = typh or any((u.mark.get('ng') for u in lords))
        self.troops.set_active_types([BlackLegionPlagueMarines], plag)
        self.elites.set_active_types([BlackLegionPlagueMarines], not plag)
        if not plag and self.troops.count_unit(BlackLegionPlagueMarines) > 0:
            self.error("You can't have Plague Marines in Troops without Chaos Lord with the Mark of Nurgle "
                       "(including Typhus) in HQ.")
        if plag and self.elites.count_unit(BlackLegionPlagueMarines) > 0:
            self.error("You can't have Plague Marines in Elites with Chaos Lord with the Mark of Nurgle "
                       "(including Typhus) in HQ.")

        #noise marines as troops
        noise = self.hq.count_unit(Lucius) > 0 or any((u.mark.get('sl') for u in lords))
        self.troops.set_active_types([BlackLegionNoiseMarines], noise)
        self.elites.set_active_types([BlackLegionNoiseMarines], not noise)
        if not noise and self.troops.count_unit(BlackLegionNoiseMarines) > 0:
            self.error("You can't have Noise Marines in Troops without Chaos Lord with the Mark of Slaanesh "
                       "(including Lucius) in HQ.")
        if noise and self.elites.count_unit(BlackLegionNoiseMarines) > 0:
            self.error("You can't have Noise Marines in Elites with Chaos Lord with the Mark of Slaanesh "
                       "(including Lucius) in HQ.")

        #berserkers as troops
        bers = self.hq.count_unit(Kharn) > 0 or any((u.mark.get('kh') for u in lords))
        self.troops.set_active_types([BlackLegionBerzerks], bers)
        self.elites.set_active_types([BlackLegionBerzerks], not bers)
        if not bers and self.troops.count_unit(BlackLegionBerzerks) > 0:
            self.error("You can't have Khorne Berzerkers in Troops without Chaos Lord with the Mark of Khorne "
                       "(including Khârn the Betrayer) in HQ.")
        if bers and self.elites.count_unit(BlackLegionBerzerks) > 0:
            self.error("You can't have Khorne Berzerkers in Elites with Chaos Lord with the Mark of Khorne "
                       "(including Khârn the Betrayer) in HQ.")

        #rubruc marines as troops
        rubr = self.hq.count_unit(Ahriman) > 0 or any((u.mark.get('tz') for u in self.hq.get_units(BlackLegionSorc)))
        self.troops.set_active_types([ThousandSons], rubr)
        self.elites.set_active_types([ThousandSons], not rubr)
        if not rubr and self.troops.count_unit(ThousandSons) > 0:
            self.error("You can't have Thousand Sons in Troops without Sorcerer with the Mark of Tzeentch "
                       "(including Ahriman) in HQ.")
        if rubr and self.elites.count_unit(ThousandSons) > 0:
            self.error("You can't have Thousand Sons in Elites with Sorcerer with the Mark of Tzeentch "
                       "(including Ahriman) in HQ.")
