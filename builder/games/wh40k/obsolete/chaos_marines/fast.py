__author__ = 'Ivan Truskov'
from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.chaos_marines.armory import melee, ranged, ranged_ids
from builder.games.wh40k.obsolete.chaos_marines.troops import IconBearer


class Spawn(Unit):
    name = "Chaos Spawns"
    base_points = 30

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Chaos Spawn', 1, 5, self.base_points)
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Khorne', 2, 'kh'],
            ['Mark of Tzeentch', 4, 'tz'],
            ['Mark of Nurgle', 6, 'ng'],
            ['Mark of Slaanesh', 3, 'sl']
        ], limit=1)

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]))
        self.build_description(options=[self.marks])
        self.description.add(
            ModelDescriptor('Chaos Spawn', self.count.get(), self.base_points).
            build()
        )


class Helldrake(Unit):
    name = "Heldrake"
    base_points = 170
    gear = ['Daemonic posession']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Hades autocannon', 0, 'hacan'],
            ['Baleflamer', 0, 'bflame']
        ])


class WarpTalons(Unit):
    name = "Warp Talons"
    model_points = 30
    base_gear = ['Jump pack', 'Lightning claw', 'Lightning claw', 'Power armour']

    class Champ(Unit):
        def __init__(self):
            Unit.__init__(self)
            self.base_points = 160 - WarpTalons.model_points * 4
            self.gear = WarpTalons.base_gear
            self.name = "Warp Talon Champion"
            self.gifts = self.opt_count('Gift of mutation', 0, 2, 10)

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion
        self.ldr = self.opt_sub_unit(self.Champ())
        self.warriors = self.opt_count('Warp Talon', 4, 9, WarpTalons.model_points)
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Khorne', 4, 'kh'],
            ['Mark of Tzeentch', 6, 'tz'],
            ['Mark of Nurgle', 4, 'ng'],
            ['Mark of Slaanesh', 3, 'sl']
        ], limit=1)
        if not self.black_legion:
            self.opt = self.opt_options_list('Options', [
                ['Veterans of the Long War', 3, 'lwvet']
            ])

    def check_rules(self):
        self.set_points(self.ldr.points() + (WarpTalons.model_points * self.warriors.get()) +
                        ((self.opt.points() if not self.black_legion else 3) + self.marks.points()) * self.get_count())
        self.build_description(exclude=[self.warriors.id], count=1)
        self.description.add(ModelDescriptor("Warp Talon", gear=WarpTalons.base_gear,
                                             points=WarpTalons.model_points).build(self.warriors.get()))
        if self.black_legion:
            self.description.add('Veterans of the Long War')

    def get_count(self):
        return self.warriors.get() + 1


class Raptors(Unit):
    name = 'Raptors'
    model_points = 17
    base_gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Jump pack']

    class Raptor(IconBearer):
        name = 'Raptor'

        def __init__(self):
            IconBearer.__init__(self, wrath=15, flame=10, despair=10, excess=30, vengeance=20)
            self.base_points = Raptors.model_points
            self.gear = Raptors.base_gear + ['Close combat weapon']
            self.count = self.opt_count(self.name, 1, 19, self.base_points)
            self.side = self.opt_one_of('Sidearm', [
                ['Bolt pistol', 0, 'bpist'],
                ['Plasma pistol', 15, 'ppist']
            ])
            self.opt = self.opt_options_list('Weapon', [
                ['Flamer', 5, 'flame'],
                ['Meltagun', 10, 'mgun'],
                ['Plasma gun', 15, 'pgun']
            ], limit=1)
            self.spec_allowed = True

        def has_spec(self):
            return self.get_count() if self.side.get_cur() == 'ppist' or len(self.opt.get_all()) > 0 else 0

        def check_rules(self):
            #either plasma pistol or special weapon can be taken
            self.side.set_active_options(['ppist'], len(self.opt.get_all()) == 0 and self.spec_allowed)
            self.opt.set_visible(self.side.get_cur() != 'ppist' and self.spec_allowed)
            single_points = self.build_points(exclude=[self.count.id], count=1)
            self.points.set(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id])

    class Champ(IconBearer):
        name = 'Raptor Champion'

        def __init__(self):
            IconBearer.__init__(self, wrath=15, flame=10, despair=10, excess=30, vengeance=20)
            self.base_points = 95 - Raptors.model_points * 4
            self.gear = Raptors.base_gear
            self.wep1 = self.opt_one_of('Weapon', [['Close combat weapon', 0, 'ccw']] + melee + ranged)
            self.wep2 = self.opt_one_of('', [['Bolt pistol', 0, 'bpist']] + melee + ranged)
            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
                ['Gift of mutation', 10, 'gift']
            ])

        def check_rules(self):
            #any weapon can be switched for ranged, but only single ranged weapon can be taken
            self.wep1.set_active_options(ranged_ids, not self.wep2.get_cur() in ranged_ids)
            self.wep2.set_active_options(ranged_ids, not self.wep1.get_cur() in ranged_ids)
            IconBearer.check_rules(self)

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion

        self.ldr = self.opt_sub_unit(self.Champ())
        self.warriors = self.opt_units_list(self.Raptor.name, self.Raptor, 4, 14)
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Khorne', 2, 'kh'],
            ['Mark of Tzeentch', 2, 'tz'],
            ['Mark of Nurgle', 3, 'ng'],
            ['Mark of Slaanesh', 2, 'sl']
        ], limit=1)
        if not self.black_legion:
            self.opt = self.opt_options_list('Options', [
                ['Veterans of the Long War', 2, 'lwvet']
            ])

    def check_rules(self):
        self.warriors.update_range()
        icon = sum(map(lambda u: u.set_icon(self.marks), [self.ldr.get_unit()] + self.warriors.get_units()))
        if icon > 1:
            self.error("Raptors can't take more then 1 icon (taken: {0}).".format(icon))

        spec = reduce(lambda val, it: val + it.has_spec(), self.warriors.get_units(), 0)
        if spec > 2:
            self.error("Raptors can't have more then 2 special weapon (taken: {0}).".format(spec))

        self.points.set(self.build_points(count=1, options=[self.ldr, self.warriors]) +
                        ((self.opt.points() if not self.black_legion else 2) + self.marks.points()) * self.get_count())
        self.build_description(count=1)
        if self.black_legion:
            self.description.add('Veterans of the Long War')

    def get_count(self):
        return self.warriors.get_count() + 1


class Bikers(Unit):
    name = 'Chaos Bikers'
    model_points = 20
    base_gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Chaos bike']

    class Biker(IconBearer):
        name = 'Chaos Biker'
        spec_wep = [
            ['Flamer', 5, 'flame'],
            ['Meltagun', 10, 'mgun'],
            ['Plasma gun', 15, 'pgun']
        ]
        spec_ids = map(lambda w: w[2], spec_wep)

        def __init__(self):
            IconBearer.__init__(self, wrath=20, flame=15, despair=10, excess=35, vengeance=25)
            self.base_points = Bikers.model_points
            self.gear = Bikers.base_gear + ['Bolt pistol']
            self.count = self.opt_count(self.name, 1, 9, self.base_points)
            self.wep = self.opt_one_of('Weapon', [['Close combat weapon', 0, 'ccw']] + self.spec_wep)
            self.bwep = self.opt_one_of('Bike weapon', [['Twin-linked boltgun', 0, 'tlbgun']] + self.spec_wep)

        def has_spec(self):
            return self.get_count() if self.wep.get_cur() != 'ccw' or self.bwep.get_cur() != 'tlbgun' else 0

        def check_rules(self):
            self.wep.set_active_options(self.spec_ids, self.bwep.get_cur() == 'tlbgun')
            self.bwep.set_active_options(self.spec_ids, self.wep.get_cur() == 'ccw')
            single_points = self.build_points(exclude=[self.count.id], count=1)
            self.points.set(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id])

    class Champ(IconBearer):
        name = 'Chaos Biker Champion'

        def __init__(self):
            IconBearer.__init__(self, wrath=20, flame=15, despair=10, excess=35, vengeance=25)
            self.base_points = 70 - Bikers.model_points * 2
            self.gear = Bikers.base_gear + ['Twin-linked boltgun']
            self.wep1 = self.opt_one_of('Weapon', [['Close combat weapon', 0, 'ccw']] + melee + ranged)
            self.wep2 = self.opt_one_of('', [['Bolt pistol', 0, 'bpist']] + melee + ranged)
            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
                ['Gift of mutation', 10, 'gift']
            ])

        def check_rules(self):
            #any weapon can be switched for ranged, but only single ranged weapon can be taken
            self.wep1.set_active_options(ranged_ids, not self.wep2.get_cur() in ranged_ids)
            self.wep2.set_active_options(ranged_ids, not self.wep1.get_cur() in ranged_ids)
            IconBearer.check_rules(self)

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion

        self.ldr = self.opt_sub_unit(self.Champ())
        self.warriors = self.opt_units_list(self.Biker.name, self.Biker, 2, 9)
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Khorne', 2, 'kh'],
            ['Mark of Tzeentch', 3, 'tz'],
            ['Mark of Nurgle', 6, 'ng'],
            ['Mark of Slaanesh', 2, 'sl']
        ], limit=1)
        if not self.black_legion:
            self.opt = self.opt_options_list('Options', [
                ['Veterans of the Long War', 1, 'lwvet']
            ])

    def check_rules(self):
        self.warriors.update_range()
        icon = sum(map(lambda u: u.set_icon(self.marks), [self.ldr.get_unit()] + self.warriors.get_units()))
        if icon > 1:
            self.error("Chaos Bikers can't take more then 1 icon (taken: {0}).".format(icon))

        spec = reduce(lambda val, it: val + it.has_spec(), self.warriors.get_units(), 0)
        if spec > 2:
            self.error("Chaos Bikers can't have more then 2 special weapon (taken: {0}).".format(spec))

        self.points.set(self.build_points(count=1, options=[self.ldr, self.warriors]) +
                        ((self.opt.points() if not self.black_legion else 2) + self.marks.points()) * self.get_count())
        self.build_description(count=1)
        if self.black_legion:
            self.description.add('Veterans of the Long War')

    def get_count(self):
        return self.warriors.get_count() + 1
