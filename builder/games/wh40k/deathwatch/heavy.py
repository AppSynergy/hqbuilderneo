__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from armory import Vehicle
from builder.games.wh40k.unit import Unit


class LandRaider(Unit):
    type_id = 'land_raider_v1'
    type_name = "Land Raider"

    class Options(Vehicle):
        def __init__(self, parent):
            super(LandRaider.Options, self).__init__(parent, blade=False)
            self.variant('Multi-melta', 10)

    def __init__(self, parent):
        super(LandRaider, self).__init__(parent=parent, points=250, gear=[
            Gear('Twin-linked heavy bolter'),
            Gear('Twin-linked lascannon', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ])
        self.Options(self)


class LandRaiderRedeemer(Unit):
    type_id = 'land_raider_redeemer_v1'
    type_name = "Land Raider Redeemer"

    def __init__(self, parent):
        super(LandRaiderRedeemer, self).__init__(parent=parent, points=240, gear=[
            Gear('Twin-linked assault cannon'),
            Gear('Flamestorm cannon', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
            Gear('Frag Assault Launcher'),
        ])
        LandRaider.Options(self)


class LandRaiderCrusader(Unit):
    type_id = 'land_raider_crusader_v1'
    type_name = "Land Raider Crusader"

    def __init__(self, parent):
        super(LandRaiderCrusader, self).__init__(parent=parent, points=250, gear=[
            Gear('Twin-linked assault cannon'),
            Gear('Hurricane bolter', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
            Gear('Frag Assault Launcher')
        ])
        LandRaider.Options(self)
