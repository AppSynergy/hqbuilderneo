__author__ = 'Ivan Truskov'

from builder.core2 import Gear, UnitDescription
from builder.games.wh40k.roster import Unit


class Cassius(Unit):
    type_name = u'Orthan Cassius, The True Believer'
    type_id = 'dw_cassius_v1'

    def __init__(self, parent):
        super(Cassius, self).__init__(parent, 'Orthan Cassius', 95, gear=[
            Gear('Bolt pistol'), Gear('Crozius Arcanum'), Gear('Frag grenades'),
            Gear('Krak grenades'), Gear('Rosarius'), Gear('Special issue ammunition')
        ], static=True, unique=True)

    def get_unique(self):
        return ['Chaplain Cassius']


class Jensus(Unit):
    type_name = u'Jensus Natorian, The Vengeful Son'
    type_id = 'dw_jensus_v1'

    def __init__(self, parent):
        super(Jensus, self).__init__(parent, 'Jensus Natorian', 95, gear=[
            Gear('Bolt pistol'), Gear('Force sword'), Gear('Frag grenades'),
            Gear('Krak grenades'), Gear('Psychic hood'), Gear('Special issue ammunition')
        ], static=True, unique=True)

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(Jensus, self).build_statistics())


class SquadDonatus(Unit):
    type_name = u'Squad Donatus'
    type_id = 'dw_donatus_v1'
    chapter = 'raven'

    def __init__(self, parent):
        grenades = [Gear('Frag grenades'), Gear('Krak grenades')]
        super(SquadDonatus, self).__init__(parent, points=175, gear=[
            UnitDescription('Vael Donatus', options=grenades + [
                Gear('Boltgun'), Gear('Special Issue ammunition')]),
            UnitDescription('Drenn Redblade', options=grenades + [
                Gear('Boltgun'), Gear('Special Issue ammunition'), Gear('Close combat weapon', count=2)]),
            UnitDescription('Rodricus Grytt', options=grenades + [
                Gear('Deathwatch frag cannon')]),
            UnitDescription('Ennox Sorrlock', options=grenades + [
                Gear('Combi-melta'), Gear('Special Issue ammunition')]),
            UnitDescription('Zameon Gydrael', options=grenades + [
                Gear('Plasma pistol'), Gear('Power sword')]),
        ], unique=True, static=True)

    def build_statistics(self):
        return {'Models': 5, 'Units': 1}


class Setorax(Unit):
    type_name = u'Edryc Setorax, The Silent Killer'
    type_id = 'dw_setorax_v1'

    def __init__(self, parent):
        super(Setorax, self).__init__(parent, 'Edryc Setorax', 35, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'),
            Gear('Lightning claw', count=2)
        ], static=True, unique=True)


class Delassio(Unit):
    type_name = u'Antor Delassio, The Cursed Young Prince'
    type_id = 'dw_delassio_v1'

    def __init__(self, parent):
        super(Delassio, self).__init__(parent, 'Antor Delassio', 35, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'),
            Gear('Hand flamer'), Gear('Chainsword')
        ], static=True, unique=True)


class Garran(Unit):
    type_name = u'Garran Branatar, The Walker in the fire'
    type_id = 'dw_garran_v1'

    def __init__(self, parent):
        super(Garran, self).__init__(parent, 'Garran Branatar', 60, gear=[
            Gear('Terminator armour'), Gear('Heavy flamer'),
            Gear('Master-crafted meltagun'), Gear('Master-crafted power fist')
        ], static=True, unique=True)


class Suberei(Unit):
    type_name = u'Jetek Suberei, The Living Hurricane'
    type_id = 'dw_jetek_v1'

    def __init__(self, parent):
        super(Suberei, self).__init__(parent, 'Jetek Suberei', 40, gear=[
            Gear('Twin-linked boltgun'), Gear('Power sword'),
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Special Issue ammunition'),
            Gear('Teleport homer')
        ], static=True, unique=True)
