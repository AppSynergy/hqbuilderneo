__author__ = 'Ivan Truskov'

from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as BaseLords
from builder.games.wh40k.imperial_armour.dataslates.imperial_knights import CerastusSection
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, PrimaryDetachment,\
    AlliedDetachment, CombinedArmsDetachment, Wh40kKillTeam, Wh40kImperial,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, UnitType, Detachment, Formation, HeavySection
from hq import WatchMaster, WatchCaptain,\
    Chaplain, Librarian
from fast import Rhino, Razorback, DropPod, Corvus, Bikers
from elites import Terminators, Dreadnought, VenDreadnought,\
    Vanguard
from troops import Veterans
from heavy import LandRaider, LandRaiderRedeemer, LandRaiderCrusader
from overkill_units import SquadDonatus, Setorax,\
    Delassio, Garran, Suberei, Cassius, Jensus
from death_masque_units import Artemis, SquadCrull,\
    SquadGalatael, Nihilus
from builder.games.wh40k.fortifications import Fort
from formations import Aquila, Furor, Venator,\
    Dominatus, Malleus, Purgatus, Strategium,\
    Company, Dropships, TeamCassius, WatchForceArtemis,\
    WatchbladeTaskForce, KillTeam


class BaseHq(HQSection):
    def __init__(self, parent):
        super(BaseHq, self).__init__(parent)
        UnitType(self, WatchMaster)
        UnitType(self, WatchCaptain)
        UnitType(self, Chaplain)
        UnitType(self, Librarian)
        UnitType(self, Cassius)
        UnitType(self, Jensus)
        UnitType(self, Artemis)


class Hq(BaseHq):
    def __init__(self, parent):
        super(Hq, self).__init__(parent)
        from builder.games.wh40k.dataslates.cypher import Cypher
        self.cypher = UnitType(self, Cypher, slot=0)
        from builder.games.wh40k.adepta_sororitas_v4 import Celestine
        UnitType(self, Celestine)
        from builder.games.wh40k.inquisition_v2 import Greyfax
        UnitType(self, Greyfax)
        from builder.games.wh40k.cult_mechanicus import Cawl
        UnitType(self, Cawl)
        from builder.games.wh40k.grey_knights_v3 import Voldus
        UnitType(self, Voldus)

    def check_rules(self):
        super(Hq, self).check_rules()
        self.cypher.visible = isinstance(self.parent.parent.parent, PrimaryDetachment)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, Veterans)
        UnitType(self, SquadDonatus)
        UnitType(self, SquadCrull)


class Fast(FastSection):
    def __init__(self, parent):
        super(Fast, self).__init__(parent)
        UnitType(self, Bikers)
        UnitType(self, Rhino)
        UnitType(self, Razorback)
        UnitType(self, DropPod)
        UnitType(self, Corvus)
        UnitType(self, Suberei)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        UnitType(self, Terminators)
        UnitType(self, Dreadnought)
        UnitType(self, VenDreadnought)
        UnitType(self, Vanguard)
        UnitType(self, Setorax)
        UnitType(self, Delassio)
        UnitType(self, Garran)
        UnitType(self, SquadGalatael)
        UnitType(self, Nihilus)


class Heavy(HeavySection):
    def __init__(self, parent):
        super(Heavy, self).__init__(parent)
        UnitType(self, LandRaider)
        UnitType(self, LandRaiderRedeemer)
        UnitType(self, LandRaiderCrusader)


class LordsOfWar(CerastusSection, BaseLords):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent)
        from builder.games.wh40k.space_marines_v3 import Guilliman
        UnitType(self, Guilliman)


class BlackSpear(Wh40k7ed):
    army_name = 'Black Spear strike force'
    army_id = 'dw_v1_black_spear'

    class Commander(Formation):
        army_id = 'dw_v1_commander'
        army_name = 'Watch Commander'

        def __init__(self):
            super(BlackSpear.Commander, self).__init__()
            types = [UnitType(self, ut) for ut in
                     [WatchMaster, WatchCaptain, Chaplain, Librarian,
                      Dreadnought, VenDreadnought, Artemis, Nihilus]]
            self.add_type_restriction(types, 1, 1)

    class CommandSection(Detachment):
        def __init__(self, parent):
            super(BlackSpear.CommandSection, self).__init__(
                parent, 'command', 'Command', [BlackSpear.Commander], 1, 3)

    class CoreSection(Detachment):
        def __init__(self, parent):
            class PlainKillTeam(KillTeam):
                army_name = 'Kill Team'
                army_id = 'dw_v1_kill_team'

                def __init__(self):
                    super(PlainKillTeam, self).__init__([])

            super(BlackSpear.CoreSection, self).__init__(
                parent, 'core', 'Core', [PlainKillTeam, Aquila, Furor,
                                         Venator, Malleus, Dominatus,
                                         Purgatus, Strategium, Company],
                1, None)

    class Dropship(Formation):
        army_name = 'Dropship'
        army_id = 'dq_v1_dropship'

        def __init__(self):
            super(BlackSpear.Dropship, self).__init__()
            UnitType(self, Corvus, min_limit=1, max_limit=1)

    class Ancient(Formation):
        army_name = 'Ancient'
        army_id = 'dw_v1_ancient'

        def __init__(self):
            super(BlackSpear.Ancient, self).__init__()
            types = [UnitType(self, Dreadnought),
                     UnitType(self, VenDreadnought),
                     UnitType(self, Nihilus)]
            self.add_type_restriction(types, 1, 1)

    class Armour(Formation):
        army_name = 'Armour'
        army_id = 'dw_v1_armour'

        def __init__(self):
            super(BlackSpear.Armour, self).__init__()
            types = [UnitType(self, LandRaider),
                     UnitType(self, LandRaiderRedeemer),
                     UnitType(self, LandRaiderCrusader)]
            self.add_type_restriction(types, 1, 1)

    class AuxilarySection(Detachment):
        def __init__(self, parent):
            super(BlackSpear.AuxilarySection, self).__init__(
                parent, 'aux', 'Auxilary', [BlackSpear.Dropship,
                                            BlackSpear.Ancient,
                                            BlackSpear.Armour,
                                            Dropships],
                1, None)

    def __init__(self):
        super(Wh40k7ed, self).__init__([self.CommandSection(self),
                                        self.CoreSection(self),
                                        self.AuxilarySection(self)])

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class DeathwatchBase(Wh40kBase):
    def __init__(self):
        super(DeathwatchBase, self).__init__(
            hq=Hq(self), troops=Troops(self),
            elites=Elites(self), fast=Fast(self),
            heavy=Heavy(self),
            fort=Fort(parent=self),
            lords=LordsOfWar(self)
        )


class CAD(DeathwatchBase, CombinedArmsDetachment):
    army_name = 'Deathwatch (Combined arms detachment)'
    army_id = 'dw_v1_cad'


class Allies(DeathwatchBase, AlliedDetachment):
    army_name = 'Deathwatch (Allied detachment)'
    army_id = 'dw_v1_ad'


class DeathwatchPA(DeathwatchBase, PlanetstrikeAttacker):
    army_id = 'dw_v1_pa'
    army_name = 'Deathwatch (Planetstrike attacker detachment)'


class DeathwatchPD(DeathwatchBase, PlanetstrikeDefender):
    army_id = 'dw_v1_pd'
    army_name = 'Deathwatch (Planetstrike defender detachment)'


class DeathwatchSA(DeathwatchBase, SiegeAttacker):
    army_id = 'dw_v1_sa'
    army_name = 'Deathwatch (Siege War attacker detachment)'


class DeathwatchSD(DeathwatchBase, SiegeDefender):
    army_id = 'dw_v1_sd'
    army_name = 'Deathwatch (Siege War defender detachment)'


class DeathwatchKillTeam(Wh40kKillTeam):
    army_id = 'dw_v1_kt'
    army_name = 'Deathwatch'

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(DeathwatchKillTeam.KTTroops, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [Veterans],
                                                    lambda u: [u.transport.pod, u.transport.corvus])
            UnitType(self, SquadDonatus)
            UnitType(self, SquadCrull)

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(DeathwatchKillTeam.KTElites, self).__init__(parent)
            UnitType(self, Vanguard)
            UnitType(self, Setorax)
            UnitType(self, Delassio)
            UnitType(self, SquadGalatael)

    class KTFast(FastSection):
        def __init__(self, parent):
            super(DeathwatchKillTeam.KTFast, self).__init__(parent)
            UnitType(self, Bikers)
            UnitType(self, Suberei)
            Wh40kKillTeam.process_vehicle_units(self, [Rhino, Razorback])

    def __init__(self):
        super(DeathwatchKillTeam, self).__init__(
            self.KTTroops(self), self.KTElites(self), self.KTFast(self)
        )


class DeathwatchKillTeamFormations(Wh40kBase):
    army_id = 'dw_v1_dkt'
    army_name = 'Deathwatch Kill Team'

    def __init__(self):
        self.troops = Troops(parent=self)
        self.troops.min, self.troops.max = (1, 1)
        super(DeathwatchKillTeamFormations, self).__init__(
            added=[self.troops]
        )


faction = 'Deathwatch'


class Deathwatch(Wh40k7ed, Wh40kImperial):
    army_id = 'dw_v1'
    army_name = 'Deathwatch'
    faction = faction

    def __init__(self):
        super(Deathwatch, self).__init__(
            [CAD, BlackSpear, DeathwatchKillTeamFormations,
             Aquila, Furor, Venator,
             Dominatus, Malleus, Purgatus, Strategium,
             Company, Dropships, TeamCassius, WatchForceArtemis,
             WatchbladeTaskForce], [])


class DeathwatchMissions(Wh40k7edMissions, Wh40kImperial):
    army_id = 'dw_v1_mis'
    army_name = 'Deathwatch'
    faction = faction

    def __init__(self):
        super(DeathwatchMissions, self).__init__(
            [CAD, DeathwatchPA, DeathwatchPD, DeathwatchSA, DeathwatchSD,
             BlackSpear, DeathwatchKillTeamFormations,
             Aquila, Furor, Venator,
             Dominatus, Malleus, Purgatus, Strategium,
             Company, Dropships, TeamCassius, WatchForceArtemis,
             WatchbladeTaskForce], [])


detachments = [CAD, BlackSpear, DeathwatchKillTeamFormations, Allies,
               DeathwatchPA, DeathwatchPD, DeathwatchSA, DeathwatchSD,
               Aquila, Furor, Venator,
               Dominatus, Malleus, Purgatus, Strategium,
               Company, Dropships, TeamCassius, WatchForceArtemis,
               WatchbladeTaskForce]


unit_types = [
    WatchMaster, WatchCaptain, Chaplain, Librarian, Rhino,
    Razorback, DropPod, Corvus, Bikers, Terminators, Dreadnought,
    VenDreadnought, Vanguard, Veterans, LandRaider,
    LandRaiderRedeemer, LandRaiderCrusader, SquadDonatus, Setorax,
    Delassio, Garran, Suberei, Cassius, Jensus, Artemis, SquadCrull,
    SquadGalatael, Nihilus
]
