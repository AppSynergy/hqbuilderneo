__author__ = 'Ivan Truskov'

from builder.core2 import ListSubUnit, Gear, OptionsList,\
    UnitList, OptionalSubUnit, SubUnit, OneOf
from fast import DropPod
from builder.games.wh40k.roster import Unit


class Terminators(Unit):
    type_name = u'Terminators'
    type_id = 'terminators_v1'

    class Terminator(ListSubUnit):
        type_name = u'Terminator'

        class MeleeWeapon(OneOf):
            def __init__(self, parent):
                super(Terminators.Terminator.MeleeWeapon, self).__init__(parent, 'Melee weapon')
                self.variant('Power fist')
                self.variant('Power weapon')
                self.variant('Chainfist', 5)
                self.variant('Power fist with auxilary meltagun', 10, gear=[
                    Gear('Power fist'), Gear('Meltagun')
                ])

        class RangedOptions(OptionsList):
            def __init__(self, parent):
                super(Terminators.Terminator.RangedOptions, self).__init__(parent, 'Ranged options',
                                                                limit=1)
                self.variant('Heavy flamer', 10)
                self.variant('Assault cannon', 20)
                self.launcher = self.variant('Cyclone missile launcher', 25)

            @property
            def description(self):
                res = super(Terminators.Terminator.RangedOptions, self).description
                if not self.any or self.launcher.value:
                    res.extend([Gear('Storm bolter')])
                return res

        class PairWeapons(OptionsList):
            def __init__(self, parent):
                super(Terminators.Terminator.PairWeapons, self).__init__(parent, 'Pair weapons')
                self.variant('Two lightning claws', gear=[Gear('Lightning claw', count=2)])
                self.ham = self.variant('Thunder hammer and storm shield', 10,
                             gear=[Gear('Thunder hammer'), Gear('Storm shield')])

        def __init__(self, parent):
            super(Terminators.Terminator, self).__init__(parent, points=40,
                                              gear=[Gear('Terminator armour')])
            self.mle = self.MeleeWeapon(self)
            self.rng = self.RangedOptions(self)
            self.pair = self.PairWeapons(self)

        def check_rules(self):
            super(Terminators.Terminator, self).check_rules()
            self.mle.used = self.mle.visible =\
                            self.rng.used = self.rng.visible =\
                                            not self.pair.any

        @ListSubUnit.count_gear
        def has_hammer(self):
            return self.pair.ham.value

    def __init__(self, parent):
        super(Terminators, self).__init__(parent)
        self.models = UnitList(self, self.Terminator, 1, 5)

    def get_count(self):
        return self.models.count

    def hammers(self):
        return sum(m.has_hammer() for m in self.models.units)


class Dreadnought(Unit):
    type_name = u'Dreadnought'
    type_id = 'dreadnought_v1'
    model_points = 100

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(Dreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.builtinstormbolter = self.variant('Built-in Storm bolter', 0)
            self.builtinheavyflamer = self.variant('Built-in Heavy flamer', 10)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Dreadnought.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.extraarmour = self.variant('Extra armour', 10)
            # self.venerabledreadnought = self.variant('Venerable Dreadnought', 25, gear=[])

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Dreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.multimelta = self.variant('Multi-melta', 0)
            self.plasmacannon = self.variant('Plasma cannon', 5)
            self.assaultcannon = self.variant('Assault cannon', 10)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 15)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Dreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.powerfist = self.variant('Power fist', 0)
            self.missilelauncher = self.variant('Missile launcher', 10)

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Dreadnought.Transport, self).__init__(parent, 'Transport')
            SubUnit(self, DropPod(parent=None))

    def __init__(self, parent):
        super(Dreadnought, self).__init__(parent=parent, points=self.model_points,
                                          gear=[Gear('Smoke launchers'), Gear('Searchlight')])
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.build_in = self.BuildIn(self)
        self.opt = self.Options(self)
        self.transport = self.Transport(self)

    def check_rules(self):
        self.build_in.visible = self.build_in.used = self.wep2.cur == self.wep2.powerfist

    def build_statistics(self):
        return self.note_transport(super(Dreadnought, self).build_statistics())


class VenDreadnought(Dreadnought):
    type_name = u'Venerable dreadnought'
    type_id = 'vendread_v1'
    model_points = 125


class Vanguard(Unit):
    type_name = u'Vanguard Veterans'
    type_id = 'vanguard_v1'

    class Veteran(ListSubUnit):
        type_name = u'Vanguard Veteran'

        class Pistol(OneOf):
            def __init__(self, parent, *args):
                super(Vanguard.Veteran.Pistol, self).__init__(parent, *args)
                self.variant('Bolt pistol', gear=[
                    Gear('Bolt pistol'), Gear('Special issue ammunition')
                ])

        class Sword(OneOf):
            def __init__(self, parent, *args):
                super(Vanguard.Veteran.Sword, self).__init__(parent, *args)
                self.variant('Chainsword')

        class Weapons(OneOf):
            def __init__(self, parent, *args):
                super(Vanguard.Veteran.Weapons, self).__init__(parent, *args)
                self.variant('Lightning claw', 5)
                self.variant('Power weapon', 5)
                self.variant('Hand flamer', 10)
                self.variant('Grav pistol', 15)
                self.variant('Inferno pistol', 15)
                self.variant('Plasma pistol', 15)
                self.variant('Power fist', 15)
                self.hammer = self.variant('Thunder hammer', 20)
                self.variant('Storm shield', 10)

        class Weapon1(Weapons, Pistol):
            pass

        class Weapon2(Weapons, Sword):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Vanguard.Veteran.Options, self).__init__(parent, 'Options')
                self.ham = self.variant('Heavy thunder hammer', 30)
                self.variant('Melta bombs', 5)

        def __init__(self, parent):
            super(Vanguard.Veteran, self).__init__(parent, points=25, gear=[
                Gear('Frag grenades'), Gear('Krak grenades')
            ])
            self.wep1 = self.Weapon1(self, 'Weapon')
            self.wep2 = self.Weapon2(self, '')
            self.opt = self.Options(self)

        def check_rules(self):
            super(Vanguard.Veteran, self).check_rules()
            self.wep1.used = self.wep1.visible =\
                             self.wep2.used = self.wep2.visible =\
                                              not self.opt.ham.value

        @ListSubUnit.count_gear
        def has_hammer(self):
            return self.opt.ham.value or\
                (self.wep1.cur == self.wep1.hammer) or\
                (self.wep2.cur == self.wep2.hammer)

    def __init__(self, parent):
        super(Vanguard, self).__init__(parent)
        self.models = UnitList(self, self.Veteran, 1, 5)

    def get_count(self):
        return self.models.count

    def hammers(self):
        return sum(m.has_hammer() for m in self.models.units)
