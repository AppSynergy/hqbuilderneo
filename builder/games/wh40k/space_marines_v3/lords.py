__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OneOf
from armory import Armour
from hq import MarineUnit


class Calgar(MarineUnit):
    type_id = 'marneus_valgar_v1'
    type_name = u'Marneus Calgar'
    chapter = 'ultra'

    class Armour(OneOf):
        def __init__(self, parent):
            super(Calgar.Armour, self).__init__(parent, 'Armour')
            self.powerarmour = self.variant('Artificer armour', 0, gear=Armour.artificer_armour_set)
            self.armourofantilochus = self.variant('Armour of Antilochus', 10)

    def __init__(self, parent):
        super(Calgar, self).__init__(parent=parent, points=275, unique=True, gear=[
            Gear('Gauntlets of Ultramar'),
            Gear('Power sword'),
            Gear('Iron Halo'),
        ])
        self.armour = self.Armour(parent=self)


class Guilliman(MarineUnit):
    type_id = 'guilliman_v1'
    type_name = u'Roboute Guilliman, Primarch of the Ultramarines'
    chapter = 'ultra'

    def __init__(self, parent):
        super(Guilliman, self).__init__(parent, 'Roboute Guilliman', points=350, unique=True,
                                        static=True, gear=[Gear("The Emperor's Sword"),
                                                           Gear('The Hand of Dominion'),
                                                           Gear('Armour of Fate')])
