__author__ = 'Ivan Truskov'

from builder.core2 import OneOf
from builder.games.wh40k.imperial_armour import volume2
from builder.games.wh40k.imperial_armour.volume12.space_marines import KraatosDevastators
from builder.games.wh40k.imperial_armour.volume4.space_marines import HaasSternguards
from builder.games.wh40k.section import UnitType, Formation
from hq import Sicarius, Tigurius, Cassius, Telion, Chronus,\
    Khan, Vulkan, Shrike, Lysander, Cantor, Helbrecht, Champion, Grimaldus,\
    Captain, Librarian, Chaplain, Techmarine, Solaq, TerminatorCaptain
from elites import CommandSquad, HonourGuardSquad, CenturionAssault,\
    VanguardVeteranSquad, SternguardVeteranSquad, Dreadnoughts,\
    VenDreadnoughts, IronDreds, TerminatorSquad,\
    TerminatorAssaultSquad, SquadAmerex, SquadDarvos, CataphractiiTerminatorSquad, ContemptorDreadnoughts
from troops import TacticalSquad, ScoutSquad
from fast import AssaultSquad, ScoutBikers, BikeSquad,\
    AttackBikeSquad, LandSpeederSquadron, Stormtalon, Darkwind, Stormhawks,\
    Stormtalons
from heavy import LandRaider, LandRaiderCrusader, LandRaiderRedeemer, \
    Thunderfire, CenturionDevastators, Predators, Stalkers, Hunters, Vindicators,\
    Whirlwinds, StormravenGunship
from lords import Calgar


def check_chapter(unit):

    if hasattr(unit, 'chapter'):
        return [unit.chapter]
    else:
        return []


class SupplementOptions(OneOf):
    def __init__(self, parent):
        super(SupplementOptions, self).\
            __init__(parent=parent, name='Codex')
        self.base = self.variant(name='Space Marines')
        self.fists = self.variant(name='Imperial Fists')
        self.hands = self.variant(name='Iron Hands')
        self.scars = self.variant(name='White Scars')
        self.ravens = self.variant(name='Raven Guards')
        self.scorpions = self.variant(name='Red Scorpions')
        self.karchodons = self.variant(name='Carchodons')
        self.hawks = self.variant(name='Fire Hawks')
        self.minotaurs = self.variant(name='Minotaurs')
        self.salamanders = self.variant(name='Salamanders')
        self.templars = self.variant(name='Black Templars')
        self.ultra = self.variant(name='Ultramarines')

    def check_rules(self):
        super(SupplementOptions, self).check_rules()
        flag = self.roster.ia_enabled
        for _type in [self.scorpions, self.karchodons,
                      self.hawks, self.minotaurs]:
            _type.used = _type.visible = flag


class SpaceMarineFormation(volume2.SpaceMarinesLegaciesFormation):

    def get_chapter(self):
        result = []
        for unit in self.units:
            result.extend(check_chapter(unit))
        if self.is_hands():
            result.append('iron')
        if self.is_fists():
            result.append('fist')
        if self.is_scars():
            result.append('scar')
        if self.is_ravens():
            result.append('raven')
        if self.is_hawks():
            result.append('fire')
        if self.is_scorpions():
            result.append('scorpions')
        if self.is_karchodons():
            result.append('karchodons')
        if self.is_minotaurs():
            result.append('minotaurs')
        if self.is_salamanders():
            result.append('salamander')
        if self.is_templars():
            result.append('templar')
        if self.is_ultra():
            result.append('ultra')
        return [chap for chap in result if chap is not None]

    def check_rules(self):
        super(SpaceMarineFormation, self).check_rules()
        chaplist = list(set(self.get_chapter()))
        # import pdb; pdb.set_trace()
        if len(chaplist) > 1:
            self.error('Cannot include units from more then one chapter')
        elif len(chaplist) == 1:
            for utype in self.types:
                if hasattr(utype.unit_class, 'chapter'):
                    utype.active = utype.unit_class.chapter == chaplist[0]
        else:
            for utype in self.types:
                if hasattr(utype.unit_class, 'chapter'):
                    utype.active = True

    def __init__(self, *args, **kwargs):
        super(SpaceMarineFormation, self).__init__(*args, **kwargs)
        self.codex = SupplementOptions(self)

    def is_base_codex(self):
        return self.codex.cur in [self.codex.base, self.codex.scars,
                                  self.codex.ravens, self.codex.karchodons,
                                  self.codex.hawks, self.codex.scorpions,
                                  self.codex.minotaurs, self.codex.salamanders]

    def is_ultra(self):
        return self.codex.cur == self.codex.ultra

    def is_fists(self):
        return self.codex.cur == self.codex.fists

    def is_hands(self):
        return self.codex.cur == self.codex.hands

    def is_scars(self):
        return self.codex.cur == self.codex.scars

    def is_ravens(self):
        return self.codex.cur == self.codex.ravens

    def is_scorpions(self):
        return self.codex.cur == self.codex.scorpions

    def is_karchodons(self):
        return self.codex.cur == self.codex.karchodons

    def is_hawks(self):
        return self.codex.cur == self.codex.hawks

    def is_minotaurs(self):
        return self.codex.cur == self.codex.minotaurs

    def is_salamanders(self):
        return self.codex.cur == self.codex.salamanders

    def is_templars(self):
        return self.codex.cur == self.codex.templars


class DemiCompany(SpaceMarineFormation):
    army_id = 'space_marines_v3_demi'
    army_name = 'Battle Demi-Company (Space Marines)'
    discount = False

    class SentTactical(TacticalSquad):
        allow_garadon = True

    def __init__(self):
        super(DemiCompany, self).__init__()
        self.caps = [
            UnitType(self, Captain),
            UnitType(self, TerminatorCaptain),
            UnitType(self, Sicarius),
            UnitType(self, Khan),
            UnitType(self, Vulkan),
            UnitType(self, Shrike),
            UnitType(self, Lysander)
        ]
        self.chaps = [
            UnitType(self, Chaplain),
            UnitType(self, Cassius),
            UnitType(self, Grimaldus)
        ]
        self.add_type_restriction(self.caps + self.chaps, 1, 1)
        UnitType(self, CommandSquad, max_limit=1)
        UnitType(self, self.SentTactical, min_limit=3, max_limit=3)
        fast = [
            UnitType(self, AssaultSquad),
            UnitType(self, BikeSquad),
            UnitType(self, AttackBikeSquad),
            UnitType(self, LandSpeederSquadron),
            UnitType(self, CenturionAssault)
        ]
        self.add_type_restriction(fast, 1, 1)
        heavy = [
            UnitType(self, KraatosDevastators),
            UnitType(self, CenturionDevastators)
        ]
        self.add_type_restriction(heavy, 1, 1)
        dred = [
            UnitType(self, Dreadnoughts),
            UnitType(self, IronDreds),
            UnitType(self, VenDreadnoughts),
            UnitType(self, ContemptorDreadnoughts)
        ]
        self.add_type_restriction(dred, 0, 1)

    def has_cap(self):
        return any([u.count for u in self.caps])

    def has_chap(self):
        return any([u.count for u in self.chaps])

    def check_rules(self):
        super(DemiCompany, self).check_rules()
        for unit in self.caps[0].units:
            if unit.is_master():
                self.error("Chapter Masters do not lead Demi-companies")


class AntiAir(SpaceMarineFormation):
    army_id = 'space_marines_v3_air'
    army_name = 'Anti-Air defence force'

    def __init__(self):
        super(AntiAir, self).__init__()
        UnitType(self, Hunters, min_limit=1, max_limit=1)
        self.stalkers = UnitType(self, Stalkers, min_limit=1, max_limit=1)

    def check_rules(self):
        super(AntiAir, self).check_rules()
        count = 0
        for unit in self.stalkers.units:
            count += unit.count
        if count < 2:
            self.error("The unit of Stalkers must consist of at least 2 models")


class FirstComp(SpaceMarineFormation):
    army_id = 'space_marines_v3_1st'
    army_name = '1st company task force'

    def __init__(self):
        super(FirstComp, self).__init__()
        vets = [
            UnitType(self, TerminatorSquad),
            UnitType(self, TerminatorAssaultSquad),
            UnitType(self, CataphractiiTerminatorSquad),
            UnitType(self, HaasSternguards),
            UnitType(self, VanguardVeteranSquad)
        ]
        self.add_type_restriction(vets, 3, 5)


class Ultra(SpaceMarineFormation):
    army_id = 'space_marines_v3_ultra'
    army_name = 'Strike Force Ultra'

    def __init__(self):
        super(Ultra, self).__init__()
        self.cap = UnitType(self, Captain)
        self.tdacap = UnitType(self, TerminatorCaptain)
        lys = UnitType(self, Lysander)
        self.add_type_restriction([self.cap, self.tdacap, lys], 1, 1)
        UnitType(self, TerminatorSquad, min_limit=2, max_limit=2)
        UnitType(self, TerminatorAssaultSquad, min_limit=2, max_limit=2)
        UnitType(self, VenDreadnoughts.VenDreadnought, min_limit=1, max_limit=1)
        UnitType(self, StormravenGunship, min_limit=1, max_limit=1)
        raiders = [
            UnitType(self, LandRaiderCrusader),
            UnitType(self, LandRaiderRedeemer)
        ]
        self.add_type_restriction(raiders, 1, 1)

    def check_rules(self):
        super(Ultra, self).check_rules()
        for unit in self.cap.units:
            if not unit.armour.is_tda():
                self.error('Captain must wear Terminator Armour')


class ReclusiamCommand(SpaceMarineFormation):
    army_id = 'space_marines_v3_recluse'
    army_name = 'Reclusiam command squad'

    def __init__(self):
        super(ReclusiamCommand, self).__init__()
        chap = [
            UnitType(self, Chaplain),
            UnitType(self, Cassius)
        ]
        self.add_type_restriction(chap, 1, 1)
        self.cmd = UnitType(self, CommandSquad, min_limit=1, max_limit=1)

    def check_rules(self):
        super(ReclusiamCommand, self).check_rules()
        for unit in self.cmd.units:
            if not unit.transport.is_razor():
                self.error('Command Squad must be embarked in Razorback')


class TenthCompany(SpaceMarineFormation):
    army_id = 'space_marines_v3_10th'
    army_name = '10th company task force'

    def __init__(self):
        super(TenthCompany, self).__init__()
        self.bikes = UnitType(self, ScoutBikers)
        scouts = UnitType(self, ScoutSquad)
        self.add_type_restriction([self.bikes, scouts], 3, 5)
        UnitType(self, Telion, min_limit=0, max_limit=1)

    def check_rules(self):
        super(TenthCompany, self).check_rules()
        for unit in self.bikes.units:
            if not unit.opt.any:
                self.error('Scout Bike squad must be equipped with cluster mines')


class Stormwing(SpaceMarineFormation):
    army_id = 'space_marines_v3_stormwing'
    army_name = 'Storm wing'

    def __init__(self):
        super(Stormwing, self).__init__()
        UnitType(self, StormravenGunship, min_limit=1, max_limit=1)
        UnitType(self, Stormtalon, min_limit=2, max_limit=2)


class Strikewing(SpaceMarineFormation):
    army_id = 'space_marines_v3_strikewing'
    army_name = 'Strike wing'

    def __init__(self):
        super(Strikewing, self).__init__()

        class Hawks(Stormhawks):
            unit_min = 2
            unit_max = 2

        UnitType(self, Hawks, min_limit=1, max_limit=1)

        class Talons(Stormtalons):
            unit_min = 2
            unit_max = 2

        UnitType(self, Talons, min_limit=1, max_limit=1)


class Siegebreaker(SpaceMarineFormation):
    army_id = 'space_marines_v3_siege'
    army_name = 'Centurion Siegebreaker Cohort'

    def __init__(self):
        super(Siegebreaker, self).__init__()
        UnitType(self, CenturionAssault, min_limit=2, max_limit=4)
        UnitType(self, IronDreds, min_limit=1, max_limit=1)


class RaiderSpearhead(SpaceMarineFormation):
    army_id = 'space_marines_v3_raider'
    army_name = 'Land Raider Spearhead'

    def __init__(self):
        super(RaiderSpearhead, self).__init__()
        self.raiders = [
            UnitType(self, LandRaiderCrusader),
            UnitType(self, LandRaiderRedeemer),
            UnitType(self, LandRaider)
        ]
        self.add_type_restriction(self.raiders, 3, 3)


class LibrariusConclave(SpaceMarineFormation):
    army_id = 'space_marines_v3_conclave'
    army_name = 'Librarius Conclave'

    def __init__(self):
        super(LibrariusConclave, self).__init__()
        raiders = [
            UnitType(self, Librarian),
            UnitType(self, Tigurius)
        ]
        self.add_type_restriction(raiders, 3, 5)

    def check_rules(self):
        super(LibrariusConclave, self).check_rules()
        if 'templar' in self.get_chapter():
            self.error("Black Templars detachments cannot include librarians")


class ArmoredTaskForce(SpaceMarineFormation):
    army_id = 'space_marines_v3_armor'
    army_name = 'Armored Task Force'

    def __init__(self):
        super(ArmoredTaskForce, self).__init__()
        UnitType(self, Techmarine, min_limit=1, max_limit=1)
        UnitType(self, Thunderfire, max_limit=3)
        raiders = [
            UnitType(self, Vindicators),
            UnitType(self, Predators),
            UnitType(self, Whirlwinds)
        ]
        self.add_type_restriction(raiders, 3, 5)
        UnitType(self, Chronus, max_limit=1)


class Suppression(SpaceMarineFormation):
    army_id = 'space_marines_v3_suppression'
    army_name = 'Suppression Force'

    def __init__(self):
        super(Suppression, self).__init__()
        self.whirlwinds = UnitType(self, Whirlwinds, min_limit=1, max_limit=1)
        UnitType(self, LandSpeederSquadron, min_limit=1, max_limit=1)

    def check_rules(self):
        super(Suppression, self).check_rules()
        count = 0
        for unit in self.whirlwinds.units:
            count += unit.count
        if count < 2:
            self.error("The unit of Whirlwinds must consist of at least 2 models")


class Skyhammer(SpaceMarineFormation):
    army_id = 'space_marines_v3_skyhammer'
    army_name = 'Skyhammer Annihilation Force'

    def __init__(self):
        super(Skyhammer, self).__init__()
        self.devs = UnitType(self, KraatosDevastators, min_limit=2, max_limit=2)
        self.ass = UnitType(self, AssaultSquad, min_limit=2, max_limit=2)

    def check_rules(self):
        super(Skyhammer, self).check_rules()
        for unit in self.devs.units:
            if not unit.transport.drop.visible:
                self.error("Devastators unit must be embarked in Drop Pod")
        for unit in self.ass.units:
            if not unit.pack.any:
                self.error("Assault Squad must have Jump Packs")


class StrikeForceCommand(SpaceMarineFormation):
    army_id = 'space_marines_v3_strike_command'
    army_name = 'Strike Force Command'

    def __init__(self):
        super(StrikeForceCommand, self).__init__()
        self.caps = [
            UnitType(self, Captain),
            UnitType(self, TerminatorCaptain),
            UnitType(self, Sicarius),
            UnitType(self, Khan),
            UnitType(self, Vulkan),
            UnitType(self, Shrike),
            UnitType(self, Lysander)
        ]
        self.masters = [
            UnitType(self, Cantor),
            UnitType(self, Helbrecht),
            UnitType(self, Calgar)
        ]
        self.chaps = [
            UnitType(self, Chaplain),
            UnitType(self, Cassius),
            UnitType(self, Grimaldus)
        ]
        self.add_type_restriction(self.caps + self.chaps + self.masters, 1, 1)
        UnitType(self, HonourGuardSquad, max_limit=1)
        UnitType(self, CommandSquad, max_limit=1)
        UnitType(self, Champion, max_limit=1)


class StormlanceDemiCpmpany(SpaceMarineFormation):
    army_id = 'space_marines_v3_stormlance_demi'
    army_name = 'Stormlance Battle Demi-company'
    discount = False

    class SentTactical(TacticalSquad):
        allow_garadon = True

    def __init__(self):
        super(StormlanceDemiCpmpany, self).__init__()
        self.caps = [
            UnitType(self, Captain),
            UnitType(self, Sicarius),
            UnitType(self, Khan),
            UnitType(self, Vulkan),
            UnitType(self, Shrike),
            UnitType(self, Lysander),
            UnitType(self, Cantor),
            UnitType(self, Helbrecht),
        ]
        self.chaps = [
            UnitType(self, Chaplain),
            UnitType(self, Cassius),
            UnitType(self, Grimaldus)
        ]
        self.add_type_restriction(self.caps + self.chaps, 1, 1)
        self.command = UnitType(self, CommandSquad, max_limit=1)
        self.tacticals = UnitType(self, self.SentTactical, min_limit=3, max_limit=3)
        self.devs = UnitType(self, KraatosDevastators, min_limit=1, max_limit=1)
        fast = [
            UnitType(self, AssaultSquad),
            UnitType(self, BikeSquad),
            UnitType(self, AttackBikeSquad),
            UnitType(self, LandSpeederSquadron)
        ]
        self.add_type_restriction(fast, 1, 1)


class Hunting(SpaceMarineFormation):
    army_id = 'space_marines_v3_hunting_force'
    army_name = 'Hunting Force'
    discount = False

    def __init__(self):
        super(Hunting, self).__init__()
        self.caps = [
            UnitType(self, Captain),
            UnitType(self, Khan),
        ]
        self.add_type_restriction(self.caps, 1, 1)
        self.chaps = UnitType(self, Chaplain, max_limit=1)

        self.command = UnitType(self, CommandSquad, max_limit=1)
        self.bikes = UnitType(self, BikeSquad, min_limit=2, max_limit=5)
        self.abikes = UnitType(self, AttackBikeSquad, min_limit=1, max_limit=3)
        self.sbikes = UnitType(self, ScoutBikers, min_limit=1, max_limit=3)

    def check_rules(self):
        super(Hunting, self).check_rules()
        for cap in self.caps:
            for unit in cap.units:
                if not unit.has_bike():
                    self.error("Capitan must be equipped with a Space Marine bike")

        for unit in self.chaps.units:
            if not unit.has_bike():
                self.error("Chaplain must be equipped with a Space Marine bike")

        for unit in self.command.units:
            if not unit.has_bike():
                self.error("Command Squad must be equipped with a Space Marine bikes")


class Stormbringer(SpaceMarineFormation):
    army_id = 'space_marines_v3_stormbringer'
    army_name = 'Stormbringer Squadron'

    def __init__(self):
        super(Stormbringer, self).__init__()
        UnitType(self, LandSpeederSquadron, min_limit=1, max_limit=3)
        self.scouts = UnitType(self, ScoutSquad, min_limit=1, max_limit=3)

    def check_rules(self):
        super(Stormbringer, self).check_rules()
        for unit in self.scouts.units:
            if not unit.opt.speeder.value:
                self.error("Each Scout Squad must take a Land Speeder Storm as a Dedicated Transport")
                break


class Speartip(SpaceMarineFormation):
    army_id = 'space_marines_v3_speartip'
    army_name = 'Speartip Strike'

    def __init__(self):
        super(Speartip, self).__init__()
        UnitType(self, LandSpeederSquadron, min_limit=1, max_limit=3)
        fast = [
            UnitType(self, BikeSquad),
            UnitType(self, AttackBikeSquad),
            UnitType(self, ScoutBikers)
        ]
        self.add_type_restriction(fast, 2, 2)


class WhiteScarStrikeForceCommand(SpaceMarineFormation):
    army_id = 'space_marines_v3_white_scar_strike_command'
    army_name = 'Strike Force Command'

    def __init__(self):
        super(WhiteScarStrikeForceCommand, self).__init__()
        self.hq = [
            UnitType(self, Khan),
            UnitType(self, Captain),
            UnitType(self, TerminatorCaptain),
            UnitType(self, Chaplain),
        ]
        self.add_type_restriction(self.hq, 1, 1)
        UnitType(self, HonourGuardSquad, max_limit=1)
        UnitType(self, CommandSquad, max_limit=1)


class PinionDemiCpmpany(SpaceMarineFormation):
    army_id = 'space_marines_v3_pinion'
    army_name = 'Pinion Battle Demi-company'
    discount = False

    class SentTactical(TacticalSquad):
        allow_garadon = True

    def __init__(self):
        super(PinionDemiCpmpany, self).__init__()
        self.cap = UnitType(self, Captain)
        self.caps = [
            self.cap,
            UnitType(self, Sicarius),
            UnitType(self, Khan),
            UnitType(self, Vulkan),
            UnitType(self, Shrike),
            UnitType(self, Cantor),
            UnitType(self, Helbrecht),
        ]
        self.chap = UnitType(self, Chaplain)
        self.chaps = [
            self.chap,
            UnitType(self, Cassius),
            UnitType(self, Grimaldus)
        ]
        self.add_type_restriction(self.caps + self.chaps, 1, 1)
        self.command = UnitType(self, CommandSquad, max_limit=1)
        self.tacticals = UnitType(self, self.SentTactical, min_limit=3, max_limit=3)
        UnitType(self, KraatosDevastators, min_limit=1, max_limit=1)
        UnitType(self, AssaultSquad, min_limit=1, max_limit=1)
        scout = [
            UnitType(self, ScoutSquad),
            UnitType(self, ScoutBikers),
        ]
        self.add_type_restriction(scout, 1, 5)

    def check_rules(self):
        super(PinionDemiCpmpany, self).check_rules()
        for unit in self.cap.units:
            if unit.armour.is_tda():
                self.error('Captain may not be quipped with Terminator Armour')
        for unit in self.chap.units:
            if unit.armour.is_tda():
                self.error('Chaplain may not be quipped with Terminator Armour')


class RavenGuardStrikeForceCommand(SpaceMarineFormation):
    army_id = 'space_marines_v3_raven_guard_strike_command'
    army_name = 'Strike Force Command'

    def __init__(self):
        super(RavenGuardStrikeForceCommand, self).__init__()
        self.hq = [
            UnitType(self, Shrike),
            UnitType(self, Captain),
            UnitType(self, TerminatorCaptain),
            UnitType(self, Chaplain),
        ]
        self.add_type_restriction(self.hq, 1, 1)
        UnitType(self, HonourGuardSquad, max_limit=1)
        UnitType(self, CommandSquad, max_limit=1)


class Shadowstrike(SpaceMarineFormation):
    army_id = 'space_marines_v3_shadowstrike'
    army_name = 'Shadowstrike Kill Team'

    def __init__(self):
        super(Shadowstrike, self).__init__()
        self.scounts = UnitType(self, ScoutSquad, min_limit=2, max_limit=4)
        self.vang = UnitType(self, VanguardVeteranSquad, min_limit=1, max_limit=3)

    def check_rules(self):
        super(Shadowstrike, self).check_rules()
        for unit in self.vang.units:
            if not unit.jump.any:
                self.error("Vanguard Veteran Squad must have Jump Packs")


class Bladewing(SpaceMarineFormation):
    army_id = 'space_marines_v3_bladewing'
    army_name = 'Bladewing Assault Brotherhood'

    def __init__(self):
        super(Bladewing, self).__init__()
        self.cap = UnitType(self, Captain)
        self.chap = UnitType(self, Chaplain)
        self.hq = [self.cap, self.chap, UnitType(self, Shrike)]
        self.add_type_restriction(self.hq, 1, 1)
        self.vang = UnitType(self, VanguardVeteranSquad, min_limit=1, max_limit=3)
        self.ass = UnitType(self, AssaultSquad, min_limit=2, max_limit=4)

    def check_rules(self):
        super(Bladewing, self).check_rules()
        count = 0
        for unit in self.vang.units:
            count += unit.count
            if not unit.jump.any:
                self.error("Vanguard Veteran Squad must have Jump Packs")

        for unit in self.ass.units:
            count += unit.count
            if not unit.pack.any:
                self.error("Vanguard Veteran Squad must have Jump Packs")

        for unit in self.cap.units:
            count += unit.count
            if not unit.has_jump():
                self.error("Capitan must be equipped with a Jump Pack")

        for unit in self.chap.units:
            count += unit.count
            if not unit.has_jump():
                self.error("Chaplain must be equipped with a Jump Pack")

        if count > 30:
            self.error("This formation may include no more than 30 models")


class OrbitalSkyhammer(SpaceMarineFormation):
    army_id = 'space_marines_v3_orbital_skyhammer'
    army_name = 'Skyhammer Orbital Strike Force'

    def __init__(self):
        super(OrbitalSkyhammer, self).__init__()
        self.tacticals = UnitType(self, TacticalSquad, min_limit=3, max_limit=3)
        UnitType(self, LandSpeederSquadron, min_limit=1, max_limit=3)

    def check_rules(self):
        super(OrbitalSkyhammer, self).check_rules()
        for unit in self.tacticals.units:
            if not unit.transport.drop.visible:
                self.error("Each Tactical Squad mast take a Drop Pod as a Dedicated Transport")
                break


class Ravenhawk(SpaceMarineFormation):
    army_id = 'space_marines_v3_ravenhawk'
    army_name = 'Ravenhawk Assault Group'

    def __init__(self):
        super(Ravenhawk, self).__init__()
        UnitType(self, HaasSternguards, min_limit=1, max_limit=1)
        self.dreadnought = [
            UnitType(self, Dreadnoughts.Dreadnought),
            UnitType(self, IronDreds.IronDred),
            UnitType(self, VenDreadnoughts.VenDreadnought),
            UnitType(self, ContemptorDreadnoughts.ContDred)
        ]
        self.add_type_restriction(self.dreadnought, 1, 1)
        UnitType(self, StormravenGunship, min_limit=1, max_limit=1)


class RaptorWing(SpaceMarineFormation):
    army_id = 'space_marines_v3_raptor'
    army_name = 'Raptor Wing'

    def __init__(self):
        super(RaptorWing, self).__init__()
        UnitType(self, LandSpeederSquadron, min_limit=1, max_limit=1)
        UnitType(self, Stormtalon, min_limit=2, max_limit=2)


class ShadowForce(SpaceMarineFormation):
    army_id = 'space_marines_v3_shadow'
    army_name = 'Shadow Force'

    def __init__(self):
        super(ShadowForce, self).__init__()
        self.cap = UnitType(self, Captain)
        self.caps = [
            self.cap,
            UnitType(self, Sicarius),
            UnitType(self, Khan),
            UnitType(self, Vulkan),
            UnitType(self, Shrike),
            UnitType(self, Cantor),
            UnitType(self, Helbrecht),
        ]
        self.add_type_restriction(self.caps, 1, 1)
        UnitType(self, HaasSternguards, min_limit=1, max_limit=1)
        UnitType(self, VanguardVeteranSquad, min_limit=1, max_limit=1)
        UnitType(self, LandSpeederSquadron, min_limit=1, max_limit=1)

    def check_rules(self):
        super(ShadowForce, self).check_rules()
        for unit in self.cap.units:
            if unit.armour.is_tda():
                self.error('Captain may not be quipped with Terminator Armour')


class IronHandStrikeForceCommand(SpaceMarineFormation):
    army_id = 'space_marines_v3_iron_hand_strike_command'
    army_name = 'Strike Force Command'

    def __init__(self):
        super(IronHandStrikeForceCommand, self).__init__()
        self.hq = [
            UnitType(self, Captain),
            UnitType(self, TerminatorCaptain),
            UnitType(self, Chaplain),
            UnitType(self, VenDreadnoughts.VenDreadnought)
        ]
        self.add_type_restriction(self.hq, 1, 1)
        UnitType(self, HonourGuardSquad, max_limit=1)
        UnitType(self, CommandSquad, max_limit=1)


class HonouredAncients(SpaceMarineFormation):
    army_id = 'space_marines_v3_honoured_ancients'
    army_name = 'Honoured Ancients'

    def __init__(self):
        super(HonouredAncients, self).__init__()
        self.dreds = [
            UnitType(self, Dreadnoughts),
            UnitType(self, IronDreds),
            UnitType(self, VenDreadnoughts),
            UnitType(self, ContemptorDreadnoughts)
        ]
        self.add_type_restriction(self.dreds, 1, 1)


class IronGuardians(SpaceMarineFormation):
    army_id = 'space_marines_v3_iron_guardians'
    army_name = 'Iron Guardians'

    def __init__(self):
        super(IronGuardians, self).__init__()
        UnitType(self, TacticalSquad, min_limit=1, max_limit=1)
        self.dreds = [
            UnitType(self, Dreadnoughts),
            UnitType(self, IronDreds),
            UnitType(self, VenDreadnoughts),
            UnitType(self, ContemptorDreadnoughts)
        ]
        self.add_type_restriction(self.dreds, 1, 1)


class Firespear(SpaceMarineFormation):
    army_id = 'space_marines_v3_firespear'
    army_name = 'Firespear Strike Force'

    def __init__(self):
        super(Firespear, self).__init__()
        self.cap = UnitType(self, Captain)
        self.caps = [
            self.cap,
            UnitType(self, TerminatorCaptain)

        ]
        self.add_type_restriction(self.caps, 1, 1)
        UnitType(self, TacticalSquad, min_limit=1, max_limit=1)
        UnitType(self, VenDreadnoughts.VenDreadnought, min_limit=1, max_limit=1)

    def check_rules(self):
        super(Firespear, self).check_rules()
        for unit in self.cap.units:
            if not unit.armour.is_tda():
                self.error('Captain must wear Terminator Armour')


class ForceSolaq(Formation):
    army_id = 'space_marines_v3_solaq'
    army_name = 'Shadow Force Solaq'

    def __init__(self):
        super(ForceSolaq, self).__init__()
        UnitType(self, Solaq, min_limit=1, max_limit=1)
        UnitType(self, SquadAmerex, min_limit=1, max_limit=1)
        UnitType(self, SquadDarvos, min_limit=1, max_limit=1)
        UnitType(self, Darkwind, min_limit=1, max_limit=1)


class VictrixGuard(SpaceMarineFormation):
    army_id = 'space_marines_v3_vitrix'
    army_name = 'Victrix Guard'

    def __init__(self):
        super(VictrixGuard, self).__init__()
        self.codex.cur = self.codex.ultra
        self.codex.visible = False

        UnitType(self, Sicarius, min_limit=1, max_limit=1)
        self.transportable = [UnitType(self, HonourGuardSquad, min_limit=1, max_limit=1)]
        others = [UnitType(self, SternguardVeteranSquad),
                  UnitType(self, VanguardVeteranSquad)]
        self.add_type_restriction(others, 4, 4)
        self.transportable += others

    def check_rules(self):
        super(VictrixGuard, self).check_rules()
        for ut in self.transportable:
            for u in ut.units:
                if u.transport.used and u.transport.any:
                    self.error("No unit in this formation may take transport")
                    break
