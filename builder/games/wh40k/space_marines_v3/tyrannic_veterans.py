__author__ = 'Ivan Truskov'


from builder.core2 import Gear, OptionsList, SubUnit, Count, UnitDescription, UnitType
from hq import Cassius, MarineUnit
from fast import Stormtalon
from transport import DropPod, TerminatorTransport
from builder.games.wh40k.imperial_armour.volume2 import SpaceMarinesLegaciesFormation
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedUnit


class TyrannicWarVeterans(MarineUnit, IATransportedUnit):
    type_name = u'Tyrannic War Veterans'
    type_id = 'tyrannic_veteran'
    chapter = 'ultra'

    model_gear = [Gear('Power armour'), Gear('Bolt pistol'), Gear('Boltgun'),
                  Gear('Frag grenades'), Gear('Krak grenades'), Gear('Hellfire rounds')]
    model_points = 23

    class Options(OptionsList):
        def __init__(self, parent):
            super(TyrannicWarVeterans.Options, self).__init__(parent, 'Options')
            self.variant('Melta bombs', 5)

    class Transport(TerminatorTransport):
        def __init__(self, parent):
            super(TyrannicWarVeterans.Transport, self).__init__(parent)
            self.dp = SubUnit(self, DropPod(parent=None))
            self.models.append(self.dp)

    def __init__(self, parent):
        super(TyrannicWarVeterans, self).__init__(parent, points=self.model_points)
        self.models = Count(self, 'Tyrannic War Veterans', 3, 9, self.model_points, True,
                            gear=UnitDescription('Tyrannic War Veteran', self.model_points,
                                                 options=self.model_gear))
        self.opt = self.Options(self)
        self.transport = self.Transport(self)

    def get_count(self):
        return self.models.cur + 1

    def build_description(self):
        desc = UnitDescription(self.name, self.points, self.get_count())
        srg = UnitDescription('Veteran Sergeant', self.model_points,
                              options=self.model_gear)
        srg.add(self.opt.description).add_points(self.opt.points)
        desc.add(srg)
        desc.add(self.models.description)
        desc.add(self.transport.description)
        return desc


class SaintTylus(SpaceMarinesLegaciesFormation):
    army_id = 'saint_tylus'
    army_name = 'Saint Tylus Battle Force'

    def get_chapter(self):
        return ['ultra']

    def __init__(self):
        super(SaintTylus, self).__init__()
        UnitType(self, Cassius, min_limit=1, max_limit=1)
        UnitType(self, TyrannicWarVeterans, min_limit=1)
        UnitType(self, Stormtalon, max_limit=6)
