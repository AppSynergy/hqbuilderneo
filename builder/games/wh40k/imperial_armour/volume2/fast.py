from builder.core2 import *
from options import SpaceMarinesBaseVehicle

__author__ = 'dante'


class Caestus(SpaceMarinesBaseVehicle):
    type_name = 'Caestus Assault Ram (IA vol.2)'
    type_id = 'caestus_v1'
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(Caestus.Options, self).__init__(parent, 'Options')
            self.variant('Teleport homer', 15)
            self.variant('Frag assault launcher', 10)
            self.variant('Armoured Ceramite', 20)

    def __init__(self, parent):
        super(Caestus, self).__init__(parent=parent, name='Caestus', points=275, gear=[
            Gear('Twin-linked magna-melta'),
            Gear('Firefury missile battery'),
            Gear('Extra armour'),
        ], transport=True, tank=True)
        self.opt = self.Options(self)


class LandSpeederTempest(Unit):
    type_name = 'Land Speeder Tempest Squadron (IA vol.2)'
    type_id = 'land_speeder_tempest_v1'
    imperial_armour = True

    class SpeederTempest(ListSubUnit, SpaceMarinesBaseVehicle):
        def __init__(self, parent):
            super(LandSpeederTempest.SpeederTempest, self).__init__(
                parent=parent, name='Land Speeder Tempest', points=90, gear=[
                    Gear('Assault cannon'),
                    Gear('Twin-linked missile launcher'),
                    Gear('Armoured cockpit'),
                    Gear('Extra armour'),
                ]
            )

        @ListSubUnit.count_unique
        def get_unique_gear(self):
            return SpaceMarinesBaseVehicle.get_unique_gear(self)

    def __init__(self, parent):
        super(LandSpeederTempest, self).__init__(parent=parent, name='Land Speeder Tempest Squadron')
        self.speeders = UnitList(self, self.SpeederTempest, 1, 3)

    def get_unique_gear(self):
        return sum((u.get_unique_gear() for u in self.speeders.units), [])


class JavelinSpeeder(SpaceMarinesBaseVehicle):
    type_name = 'Javelin Attack Speeder (IA vol.2)'
    type_id = 'javelin_speeder_v1'
    imperial_armour = True

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(JavelinSpeeder.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Twin-linked cyclone missile launcher', 0)
            self.variant('Twin-linked lascannon', 10)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(JavelinSpeeder.Weapon2, self).__init__(parent, '')
            self.variant('Heavy bolter', 0)
            self.variant('Heavy flamer', 0)
            self.variant('Multi-melta', 10)

    class Options(OptionsList):
        def __init__(self, parent):
            super(JavelinSpeeder.Options, self).__init__(parent, 'Options')
            self.variant('Searchlight', 15)

    def __init__(self, parent):
        super(JavelinSpeeder, self).__init__(parent=parent, name='Javelin Attack Speeder', points=75)
        self.Weapon1(self)
        self.Weapon2(self)
        self.Options(self)
        Count(self, 'Hunter-killer missile', 0, 2, points=5)

    def is_relic(self):
        return True
