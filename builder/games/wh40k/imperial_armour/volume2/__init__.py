__author__ = 'dante'

from builder.core2.node import Node
from heavy import *
from elite import *
from fast import *
from super_heavy import *
from transport import *
from options import SpaceMarinesBaseSquadron
from builder.games.wh40k.roster import Wh40kBase, PrimaryDetachment
from builder.games.wh40k.section import HQSection, ElitesSection,\
    FastSection, HeavySection, LordsOfWarSection, UnitType, Formation


class SpaceMarineHQ(HQSection):
    def __init__(self, parent):
        super(SpaceMarineHQ, self).__init__(parent)
        self.damocles = UnitType(self, DamoclesCommandRhino, slot=0)

    def check_rules(self):
        super(SpaceMarineHQ, self).check_rules()
        if self.damocles.count > 1:
            self.error('Only one Damocles Rhino may be taken in HQ section in detachment')


class SpaceMarineFast(FastSection):
    def __init__(self, parent):
        super(SpaceMarineFast, self).__init__(parent)
        UnitType(self, Caestus)
        UnitType(self, LandSpeederTempest)
        javelin = UnitType(self, JavelinSpeeder)

        self.relics = [javelin]


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        UnitType(self, LandRaiderPrometheus)


class SpaceMarineElites(BaseElites):
    def __init__(self, parent):
        super(SpaceMarineElites, self).__init__(parent)
        UnitType(self, ChaplainDreadnought)
        UnitType(self, SiegeDreadnought)
        UnitType(self, MortisDreadnought)
        UnitType(self, ContemptorDreadnought)
        UnitType(self, ContemptorMortisDreadnought)


class DarkAngelsElites(SpaceMarineElites):
    pass


class BloodAngelsElites(BaseElites):
    pass


class SpaceWolvesElites(BaseElites):
    def __init__(self, parent):
        super(SpaceWolvesElites, self).__init__(parent)
        UnitType(self, SiegeDreadnought)


class SpaceMarineLordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(SpaceMarineLordsOfWar, self).__init__(parent)
        UnitType(self, Fellblade)
        UnitType(self, Typhon)
        UnitType(self, Cerberus)
        UnitType(self, IA2ThunderhawkGunship)
        UnitType(self, IA2ThunderhawkTransport)


class BloodAngelsHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BloodAngelsHeavySupport, self).__init__(parent)
        UnitType(self, LandRaiderHelios)
        UnitType(self, LandRaiderProteus)
        UnitType(self, LandRaiderAchilles)
        sicaran = UnitType(self, SicaranTank)
        UnitType(self, WhirlwindHyperios)
        scorpius = UnitType(self, WhirlwindScorpius)
        UnitType(self, Spartan)
        raptor = UnitType(self, FireRaptor)
        UnitType(self, DeathStormDropPod)
        UnitType(self, Tarantula)
        UnitType(self, Rapier)

        self.relics = [sicaran, scorpius, raptor]


class SpaceMarineHeavySupport(BloodAngelsHeavySupport):
    def __init__(self, parent):
        super(SpaceMarineHeavySupport, self).__init__(parent)
        deimos = UnitType(self, DeimosPredator)
        UnitType(self, StormEagleGunship)
        self.relics.append(deimos)


class MinotaurHeavySupport(SpaceMarineHeavySupport):
    def __init__(self, parent):
        super(MinotaurHeavySupport, self).__init__(parent)
        UnitType(self, StormEagleGunshipRoc)


class SpaceMarinesLegaciesNode(Node):

    @property
    def ia_enabled(self):
        try:
            return self.root.imperial_armour_on
        except Exception:
            return False

    def count_glory_legacies(self):
        return 0

    @staticmethod
    def root_check(roster):
        leg_sum = 0
        for sec in roster.sections:
            for unit in sec.units:
                if hasattr(unit, 'sub_roster') and isinstance(unit.sub_roster.roster, SpaceMarinesLegaciesNode):
                    leg_sum += unit.sub_roster.roster.count_glory_legacies()
        leg_limit = roster.points / 1000
        if leg_limit < leg_sum:
            roster.error("No more then 1 Legacy of Glory per 1000 points may be taken; taken: {}".format(leg_sum))


class SpaceMarinesLegaciesFormation(Formation, SpaceMarinesLegaciesNode):
    def count_glory_legacies(self):
        leg_sum = 0
        for unit in self.units:
            if isinstance(unit, SpaceMarinesBaseVehicle) or\
               isinstance(unit, IATransportedUnit) or\
               isinstance(unit, SpaceMarinesBaseSquadron):
                leg_sum += unit.count_glory_legacies()
            else:
                if hasattr(unit, 'sub_roster') and isinstance(unit.sub_roster.roster, SpaceMarinesLegaciesNode):
                    leg_sum += unit.sub_roster.roster.count_glory_legacies()
        return leg_sum

    def check_rules(self):
        super(SpaceMarinesLegaciesFormation, self).check_rules()
        self.root.extra_checks['legacies_of_glory'] = SpaceMarinesLegaciesNode.root_check


class SpaceMarinesLegaciesRoster(Roster, SpaceMarinesLegaciesNode):
    def count_glory_legacies(self):
        leg_sum = 0
        for sec in self.sections:
            for unit in sec.units:
                if isinstance(unit, SpaceMarinesBaseVehicle) or\
                   isinstance(unit, IATransportedUnit) or\
                   isinstance(unit, SpaceMarinesBaseSquadron):
                    leg_sum += unit.count_glory_legacies()
                else:
                    if hasattr(unit, 'sub_roster') and isinstance(unit.sub_roster.roster, SpaceMarinesLegaciesNode):
                        leg_sum += unit.sub_roster.roster.count_glory_legacies()
        return leg_sum

    def check_rules(self):
        super(SpaceMarinesLegaciesRoster, self).check_rules()
        self.root.extra_checks['legacies_of_glory'] = SpaceMarinesLegaciesNode.root_check


class SpaceMarinesRelicRoster(Wh40kBase, SpaceMarinesLegaciesRoster):
    """ Performs checks for relics of the armory and legacies of glory

    Must be subclassed with CAD or allied detachment
    Although any detachment with HS and fast attack will do
    """

    def has_keeper(self):
        """ Checks for appropriate Relic Keeper in current codex
        """
        return False

    def check_rules(self):
        super(SpaceMarinesRelicRoster, self).check_rules()
        relic_count = 0
        for section in [self.fast, self.heavy]:
            for relic_type in section.relics:
                relic_count += sum(u.is_relic() for u in relic_type.units)
        relic_limit = 1 if type(self.parent.parent) is PrimaryDetachment else 0
        if relic_count > relic_limit and not self.has_keeper():
            self.error("Must include a Relic Keeper to take {} Relics of the Armory".format(relic_count))
