from builder.core2 import *
from transport import DreadnoughtTransport, IATransportedDreadnought
from options import SpaceMarinesBaseVehicle

__author__ = 'dante'


class LandRaiderPrometheus(SpaceMarinesBaseVehicle):
    type_id = 'land_raider_prometheus_v1'
    type_name = "Land Raider Prometheus (IA vol.2)"
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(LandRaiderPrometheus.Options, self).__init__(parent, 'Options')
            self.hkm = self.variant('Hunter-killer missile', 10)
            self.dblade = self.variant('Dozer blade', 5)
            self.sbgun = self.variant('Storm bolter', 5)
            self.melta = self.variant('Multi-melta', 10)
            self.exarm = self.variant('Extra armour', 10)

    def __init__(self, parent):
        super(LandRaiderPrometheus, self).__init__(name="Land Raider Prometheus", parent=parent, points=270, gear=[
            Gear('Twin-linked heavy bolter', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
            Gear('Improved Comms'),
            Gear('Battle Auspex'),
        ], tank=True, transport=True)
        self.opt = self.Options(self)


class ChaplainDreadnought(IATransportedDreadnought):
    type_name = 'Chaplain Dreadnought (IA vol.2)'
    type_id = 'chaplain_dreadnought_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(ChaplainDreadnought, self).__init__(
            parent=parent, points=135, name='Chaplain Dreadnought',
            gear=[Gear('Smoke launchers'), Gear('Searchlight')], walker=True
        )
        self.wep1 = self.Weapon1(self)
        self.build_in = self.BuildIn(self)
        self.opt = self.Options(self)
        self.transport = DreadnoughtTransport(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(ChaplainDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Multi-melta', 0)
            self.variant('Twin-linked heavy flamer', 0)
            self.variant('Assault cannon', 10)
            self.variant('Twin-linked heavy bolter', 5)
            self.variant('Twin-linked autocannon', 10)
            self.variant('Plasma cannon', 10)
            self.variant('Flamestorm cannon', 20)
            self.variant('Twin-linked lascannon', 30)
            self.variant('Dreadnought close combat weapon with build-in storm bolter', 15,
                         gear=[Gear('Storm bolter'), Gear('Dreadnought close combat weapon')])

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(ChaplainDreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.variant('Built-in Storm bolter', 0, gear=[Gear('Storm bolter')])
            self.variant('Built-in Heavy flamer', 10, gear=[Gear('Heavy flamer')])

    class Options(OptionsList):
        def __init__(self, parent):
            super(ChaplainDreadnought.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.extraarmour = self.variant('Extra armour', 10)


class SiegeDreadnought(IATransportedDreadnought):
    type_name = 'Siege Dreadnought (IA vol.2)'
    type_id = 'siege_dreadnought_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(SiegeDreadnought, self).__init__(
            parent=parent, points=120, name='Siege Dreadnought', walker=True,
            gear=[Gear('Smoke launchers'), Gear('Searchlight'), Gear('Extra armour'), Gear('Assault drill'),
                  Gear('Heavy flamer')])
        self.wep1 = self.Weapon1(self)
        Count(self, 'Hunter-killer missile', 0, 2, 10)
        self.transport = DreadnoughtTransport(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(SiegeDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Flamestorm cannon', 0)
            self.variant('Multi-melta', 0)


class MortisDreadnought(IATransportedDreadnought):
    type_name = 'Mark V Mortis Pattern Dreadnought (IA vol.2)'
    type_id = 'mortis_dreadnought_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(MortisDreadnought, self).__init__(
            parent=parent, points=115, name='Mark V Mortis Dreadnought', walker=True,
            gear=[Gear('Smoke launchers'), Gear('Searchlight')])
        self.wep1 = self.Weapon1(self)
        self.opt = self.Options(self)
        self.transport = DreadnoughtTransport(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(MortisDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Two missile launchers', 0, gear=Gear('Missile launcher', count=2))
            self.variant('Two twin-linked heavy bolters', 5, gear=Gear('Twin-linked heavy bolter', count=2))
            self.variant('Two twin-linked autocannon', 10, gear=Gear('Twin-linked autocannon', count=2))
            self.variant('Two twin-linked lascannon', 30, gear=Gear('Twin-linked lascannon', count=2))

    class Options(OptionsList):
        def __init__(self, parent):
            super(MortisDreadnought.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.extraarmour = self.variant('Extra armour', 10)

    def get_unique_gear(self):
        return SpaceMarinesBaseVehicle.get_unique_gear(self) + self.transport.get_unique_gear()


class ContemptorDreadnought(IATransportedDreadnought):
    type_name = 'Contemptor Pattern Dreadnought (IA vol.2)'
    type_id = 'competitor_dreadnought_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(ContemptorDreadnought, self).__init__(
            parent=parent, points=175, name='Contemptor Dreadnought', walker=True,
            gear=[Gear('Smoke launchers'), Gear('Searchlight')]
        )
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.build_in1 = self.BuildIn(self)
        self.build_in2 = self.BuildIn(self)
        self.opt = self.Options(self)
        self.transport = DreadnoughtTransport(self)

    def check_rules(self):
        self.build_in1.visible = self.build_in1.used = self.wep1.cur == self.wep1.ccw
        self.build_in2.visible = self.build_in2.used = self.wep2.cur in [self.wep2.ccw, self.wep2.fist]

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(ContemptorDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Twin-linked heavy bolter', 0)
            self.variant('Multi-melta', 0)
            self.variant('Twin-linked autocannon', 5)
            self.variant('Plasma cannon', 10)
            self.variant('Kheres pattern assault cannon', 15)
            self.variant('Heavy conversion beamer', 35)
            self.ccw = self.variant('Dreadnought close combat weapon', 0)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(ContemptorDreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.ccw = self.variant('Dreadnought close combat weapon', 0)
            self.fist = self.variant('Chainfist', 10)
            self.variant('Multi-melta', 0)
            self.variant('Twin-linked autocannon', 10)
            self.variant('Plasma cannon', 10)
            self.variant('Kheres pattern assault cannon', 15)
            self.variant('Twin-linked lascannon', 25)

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(ContemptorDreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.variant('Built-in Storm bolter', 0, gear=Gear('Storm bolter'))
            self.variant('Built-in Heavy flamer', 10, gear=Gear('Heavy flamer'))
            self.variant('Built-in Plasma blaster', 20, gear=Gear('Plasma blaster'))
            self.variant('Built-in Graviton gun', 15, gear=Gear('Graviton gun'))
            self.variant('Built-in Meltagun', 15, gear=Gear('Meltagun'))

    class Options(OptionsList):
        def __init__(self, parent):
            super(ContemptorDreadnought.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.variant('Extra armour', 10)
            self.variant('Cyclone missile launcher', 35)


class ContemptorMortisDreadnought(IATransportedDreadnought):
    type_name = 'Contemptor-Mortis Pattern Dreadnought (IA vol.2)'
    type_id = 'competitor_mortis_dreadnought_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(ContemptorMortisDreadnought, self).__init__(
            parent=parent, points=175, name='Contemptor-Mortis Dreadnought', walker=True,
            gear=[Gear('Smoke launchers'), Gear('Searchlight')]
        )
        self.wep1 = self.Weapon1(self)
        self.opt = self.Options(self)
        self.transport = DreadnoughtTransport(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(ContemptorMortisDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Two twin-linked heavy bolters', 0, gear=Gear('Twin-linked heavy bolter', count=2))
            self.variant('Two Multi-meltas', 0, gear=Gear('Multi-melta', count=2))
            self.variant('Two twin-linked autocannon', 10, gear=Gear('Twin-linked autocannon', count=2))
            self.variant('Two Kheres pattern assault cannon', 25, gear=Gear('Kheres pattern assault cannon', count=2))
            self.variant('Two twin-linked lascannon', 30, gear=Gear('Twin-linked lascannon', count=2))

    class Options(OptionsList):
        def __init__(self, parent):
            super(ContemptorMortisDreadnought.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.variant('Extra armour', 10)
            self.variant('Cyclone missile launcher', 35)
