__authir = 'Ivan Truskov'
ia_suffix = ' (IA vol.1)'

from builder.games.wh40k.unit import Unit
from builder.core2 import OptionsList, OneOf, Gear, Count


class Baneblade(Unit):
    type_id = 'baneblade_ia_v1'
    type_name = 'Baneblade' + ia_suffix
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(Baneblade.Options, self).__init__(parent, name='Options')
            self.variant('Hunter-killer missile', 10)
            self.sb = self.variant('Storm bolter', 10)
            self.hs = self.variant('Heavy stubber', 10)

        def check_rules(self):
            super(Baneblade.Options, self).check_rules()
            self.process_limit([self.sb, self.hs], 1)

    class Command(OptionsList):
        def __init__(self, parent, comissariat=True):
            super(Baneblade.Command, self).__init__(parent, name='Crew upgrades',
                                                    limit=1)
            self.variant('Command Tank', 25)
            if comissariat:
                self.variant('Commissariat Crew', 25)

    class Sponsons(OneOf):
        def __init__(self, parent, extra_sponsons=True):
            super(Baneblade.Sponsons, self).__init__(parent, 'Sponsons')
            self.variant('2 sponsons',
                         gear=[Gear('Lascannon', count=2),
                               Gear('Twin-linked heavy bolter', count=2)])
            self.variant('Armour plates', gear=[])
            if extra_sponsons:
                self.variant('4 sponsons', 100,
                             gear=[Gear('Lascannon', count=4),
                                   Gear('Twin-linked heavy bolter', count=4)])

    def __init__(self, parent):
        super(Baneblade, self).__init__(parent=parent, name='Baneblade',
                                        points=500, gear=[
                                            Gear('Autocannon'),
                                            Gear('Baneblade cannon'),
                                            Gear('Demolisher cannon'),
                                            Gear('Twin-linked heavy bolter'),
                                            Gear('Searchlight'),
                                            Gear('Smoke launchers'),
                                        ])
        self.Sponsons(self)
        self.Options(self)
        self.Command(self)


class Shadowsword(Unit):
    type_name = 'Shadowsword' + ia_suffix
    type_id = 'shadowsword_ia_v1'
    imperial_armour = True

    class Options(Baneblade.Options):
        def __init__(self, parent):
            super(Shadowsword.Options, self).__init__(parent)
            self.variant('Twin-linked heavy bolter', 25)

    def __init__(self, parent):
        super(Shadowsword).__init__(parent=parent, name='Shadowsword',
                                    points=450, gear=[
                                        Gear('Volcano cannon'),
                                        Gear('Searchlight'),
                                        Gear('Smoke launchers'),
                                    ])
        Baneblade.Sponsons(self)
        self.Options(self)
        Baneblade.Command(self, False)


class Stormblade(Unit):
    type_name = 'Stormblade' + ia_suffix
    type_id = 'stormblade_ia_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Stormblade).__init__(parent=parent, name='Stormblade',
                                   points=450, gear=[
                                       Gear('Plasma blastgun'),
                                       Gear('Heavy bolter'),
                                       Gear('Searchlight'),
                                       Gear('Smoke launchers'),
                                   ])
        Baneblade.Sponsons(self, False)
        Baneblade.Options(self)
        Baneblade.Command(self, False)


class Stormsword(Unit):
    type_name = 'Stormsword' + ia_suffix
    type_id = 'stormsword_ia_v1'
    imperial_armour = True

    class Sponsons(OneOf):
        def __init__(self, parent):
            super(Stormsword.Sponsons, self).__init__(parent, 'Sponsons')
            self.variant('2 sponsons',
                         gear=[Gear('Heavy flamer', count=2),
                               Gear('Twin-linked heavy bolter', count=2)])
            self.variant('Armour plates', gear=[])

    def __init__(self, parent):
        super(Stormsword).__init__(parent=parent, name='Stormsword',
                                   points=450, gear=[
                                       Gear('Hellflamer cannon'),
                                       Gear('Heavy bolter'),
                                       Gear('Searchlight'),
                                       Gear('Smoke launchers'),
                                   ])
        self.Sponsons(self)
        Baneblade.Options(self)
        Baneblade.Command(self, False)


class Macharius(Unit):
    type_name = 'Macharius heavy tank' + ia_suffix
    type_id = 'macharius_ia_v1'
    imperial_armour = True

    class Sponsons(OneOf):
        def __init__(self, parent):
            super(Macharius.Sponsons, self).__init__(parent, 'Sponson weapons')
            self.variant('Heavy stubbers', gear=[Gear('Heavy stubber', count=2)])
            self.variant('Heavy bolters', 10, gear=[Gear('Heavy flamer', count=2)])
            self.variant('Heavy flamers', 10, gear=[Gear('Heavy bolter', count=2)])

    def __init__(self, parent):
        super(Macharius, self).__init__(parent, 'Macharius',
                                        points=325, gear=[
                                            Gear('Macharius battle cannon'),
                                            Gear('Twin-linked heavy stubber')
                                        ])
        Baneblade.Options(self)
        self.Sponsons(self)


class MachariusVanquisher(Unit):
    type_name = 'Macharius Vanquisher' + ia_suffix
    type_id = 'mvanquisher_ia_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(MachariusVanquisher, self).__init__(parent, 'Macharius Vanquisher',
                                                  points=375, gear=[
                                                      Gear('Macharius Vanquisher cannon'),
                                                      Gear('Twin-linked heavy stubber')
                                                  ])
        Baneblade.Options(self)
        Macharius.Sponsons(self)


class MachariusVulcan(Unit):
    type_name = 'Macharius Vulcan' + ia_suffix
    type_id = 'mvulcan_ia_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(MachariusVulcan, self).__init__(parent, 'Macharius Vulcan',
                                              points=405, gear=[
                                                  Gear('Vulcan mega-bolter'),
                                                  Gear('Twin-linked heavy stubber')
                                              ])
        Baneblade.Options(self)
        Macharius.Sponsons(self)


class MachariusOmega(Unit):
    type_name = 'Macharius Omega' + ia_suffix
    type_id = 'momega_ia_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(MachariusOmega, self).__init__(parent, 'Macharius Omega', points=355,
                                             gear=[Gear('Omega patern plasma blastgun')])
        Baneblade.Options(self)
        Macharius.Sponsons(self)


class Crassus(Unit):
    type_name = 'Crassus Armoured Assault Transport' + ia_suffix
    type_id = 'crassus_ia_v1'

    class Options(Baneblade.Options):
        def __init__(self, parent):
            super(Crassus.Options, self).__init__(parent)
            self.variant('Dozer blade', 10)
            self.armour = self.variant('Replace side sponsons with extra armour', 0, gear=[])

    def __init__(self, parent):
        super(Crassus, self).__init__(parent, 'Crassus', points=250,
                                      gear=[Gear('Searchlight'), Gear('Smoke launchers')])
        self.weapons = [
            Count(self, 'Heavy flamer', 0, 4),
            Count(self, 'Autocannon', 0, 4, 5),
            Count(self, 'Lascannon', 0, 4, 10)
        ]
        self.opt = self.Options(self)

    def check_rules(self):
        super(Crassus, self).check_rules()
        Count.norm_counts(0, 4 - (2 if self.opt.armour.value else 0), self.weapons)

    def build_description(self):
        result = super(Crassus, self).build_description()
        bolter_count = 4 - (2 if self.opt.armour.value else 0) - sum(cnt.cur for cnt in self.weapons)
        if bolter_count:
            result.add(Gear('Heavy bolter', count=bolter_count))
        return result


class Praetor(Unit):
    type_name = 'Praetor Armoured Assault Launcher' + ia_suffix
    type_id = 'praetor_ia_v1'

    class Options(Baneblade.Options):
        def __init__(self, parent):
            super(Praetor.Options, self).__init__(parent)
            self.variant('Dozer blade', 10)

    def __init__(self, parent):
        super(Praetor, self).__init__(parent, 'Praetor', 300,
                                      gear=[Gear('Praetor launcher')])
        self.weapons = [
            Count(self, 'Heavy flamer', 0, 2),
            Count(self, 'Autocannon', 0, 2, 5),
            Count(self, 'Lascannon', 0, 2, 10)
        ]
        self.Options(self)

    def check_rules(self):
        super(Praetor, self).check_rules()
        Count.norm_counts(0, 2, self.weapons)

    def build_description(self):
        result = super(Praetor, self).build_description()
        bolter_count = 2 - sum(cnt.cur for cnt in self.weapons)
        if bolter_count:
            result.add(Gear('Heavy bolter', count=bolter_count))
        return result


class Dominus(Unit):
    type_name = 'Dominus Armoured Siege Bombard' + ia_suffix
    type_id = 'dominus_ia_v1'

    def __init__(self, parent):
        super(Dominus, self).__init__(parent, 'Dominus', 280,
                                      gear=[Gear('Triple-barreled bombard')])
        self.weapons = [
            Count(self, 'Heavy flamer', 0, 2),
            Count(self, 'Autocannon', 0, 2, 5),
            Count(self, 'Lascannon', 0, 2, 10)
        ]
        Praetor.Options(self)

    def check_rules(self):
        super(Dominus, self).check_rules()
        Count.norm_counts(0, 2, self.weapons)

    def build_description(self):
        result = super(Dominus, self).build_description()
        bolter_count = 2 - sum(cnt.cur for cnt in self.weapons)
        if bolter_count:
            result.add(Gear('Heavy bolter', count=bolter_count))
        return result


class Gorgon(Unit):
    type_name = 'Gorgon Heavy Transporter' + ia_suffix
    type_id = 'gorgon_ia_v1'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Gorgon.Weapon, self).__init__(parent, 'Weapon')
            self.mortar = self.variant('Gorgon mortar')
            self.variant('Sponsons', gear=[])

    def __init__(self, parent):
        super(Gorgon, self).__init__(parent, 'Gorgon', 400,
                                     [Gear('Twin-linked heavy stubber', count=2)])
        self.wep = self.Weapon(self)
        self.sponsons = [
            Count(self, 'Heavy flamer', 0, 4, 5),
            Count(self, 'Heavy bolter', 0, 4, 5)
        ]
        Baneblade.Options(self)

    def check_rules(self):
        super(Gorgon, self).check_rules()
        Count.norm_counts(0, 4, self.sponsons)
        for c in self.sponsons:
            c.used = c.visible = not self.wep.cur == self.wep.mortar

    def build_description(self):
        result = super(Gorgon, self).build_description()
        if not self.wep.cur == self.wep.mortar:
            stubber_count = 4 - sum(c.cur for c in self.sponsons)
            if stubber_count:
                result.add(Gear('Heavy stubber', count=stubber_count))
        return result


class Malcador(Unit):
    type_name = 'Malcador Heavy Tank' + ia_suffix
    type_id = 'malcador_ia_v1'

    class HullMounted(OneOf):
        def __init__(self, parent):
            super(Malcador.HullMounted, self).__init__(parent, 'Hull-mounted weapon')
            self.variant('Heavy bolter')
            self.variant('Lascannon', 15)
            self.variant('Autocannon', 5)

    class Sponsons(OneOf):
        def __init__(self, parent):
            super(Malcador.Sponsons, self).__init__(parent, 'Sponsons')
            self.variant('Heavy stubbers', 0, gear=[Gear('Heavy stubber', count=2)])
            self.variant('Lascannon', 30, gear=[Gear('Lascannon', count=2)])
            self.variant('Heavy bolters', 10, gear=[Gear('Heavy bolter', count=2)])
            self.variant('Autocannon', 20, gear=[Gear('Autocannon', count=2)])

    def __init__(self, parent):
        super(Malcador, self).__init__(parent, 'Malcador', 235,
                                       [Gear('Searchlight'), Gear('Battle cannon')])
        self.HullMounted(self)
        self.Sponsons(self)
        Baneblade.Options(self)


class MalcadorAnnihilator(Unit):
    type_name = 'Malcador Annihilator' + ia_suffix
    type_id = 'mannihilator_ia_v1'

    class HullMounted(OneOf):
        def __init__(self, parent):
            super(MalcadorAnnihilator.HullMounted, self).__init__(parent, 'Hull-mounted weapon')
            self.variant('Demolisher cannon')
            self.variant('Lascannon', 15)
            self.variant('Autocannon', 5)

    def __init__(self, parent):
        super(MalcadorAnnihilator, self).__init__(parent, 'Malcador Annihilator', 275,
                                                  [Gear('Searchlight'),
                                                   Gear('Twin-linked lascannon')])
        self.HullMounted(self)
        Malcador.Sponsons(self)
        Baneblade.Options(self)


class MalcadorDefender(Unit):
    type_name = 'Malcador Defender' + ia_suffix
    type_id = 'mdefender_ia_v1'

    def __init__(self, parent):
        super(MalcadorDefender, self).__init__(parent, 'Malcador Defender', 285,
                                               [Gear('Searchlight'),
                                                Gear('Demolisher cannon'),
                                                Gear('Heavy bolter', count=5)])
        Malcador.Sponsons(self)
        Baneblade.Options(self)


class MalcadorInfernus(Unit):
    type_name = 'Malcador Infernus' + ia_suffix
    type_id = 'minfernus_ia_v1'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(MalcadorInfernus.Weapon, self).__init__('Hull-mounter weapon')
            self.variant('Inferno gun')
            self.variant('Chem inferno gun', 10)

    class Sponsons(OneOf):
        def __init__(self, parent):
            super(MalcadorInfernus.Sponsons, self).__init__(parent, 'Sponsons')
            self.variant('Heavy stubbers', 0, gear=[Gear('Heavy stubber', count=2)])
            self.variant('Heavy bolters', 5, gear=[Gear('Heavy bolter', count=2)])
            self.variant('Heavy flamers', 5, gear=[Gear('Heavy flamer', count=2)])
            self.variant('Autocannon', 20, gear=[Gear('Autocannon', count=2)])
            self.variant('Lascannon', 30, gear=[Gear('Lascannon', count=2)])

    def __init__(self, parent):
        super(MalcadorInfernus, self).__init__(parent, 'Malcador Infernus', 275,
                                               [Gear('Searchlight'),
                                                Gear('Smoke launchers')])
        self.Weapon(self)
        self.Sponsons(self)
        Baneblade.Options(self)


class Valdor(Unit):
    type_name = 'Valdor Tank Hunter' + ia_suffix
    type_id = 'valdor_ia_v1'

    class Sponsons(OneOf):
        def __init__(self, parent):
            super(Macharius.Sponsons, self).__init__(parent, 'Sponson')
            self.variant('Heavy stubber')
            self.variant('Heavy bolter', 5)
            self.variant('Heavy flamer', 5)
            self.variant('Autocannon', 10)
            self.variant('Lascannon', 15)

    def __init__(self, parent):
        super(Valdor, self).__init__(
            parent, name='Valdor Tank Hunter', points=320,
            gear=[Gear('Neutron laser projector'),
                  Gear('Searchlight'),
                  Gear('Smoke launchers')])
        self.Sponsons(self)
        Praetor.Options(self)


class Minotaur(Unit):
    type_name = 'Minotaur Artillery Tank' + ia_suffix
    type_id = 'minotaur_ia_v1'

    class Options(Praetor.Options):
        def __init__(self, parent):
            super(Minotaur.Options, self).__init__(parent)
            self.variant('Enclosed Crew Compartments', 15)

    def __init__(self, parent):
        super(Minotaur, self).__init__(parent, 'Minotaur', 275, gear=[
            Gear('Searchlight'), Gear('Smoke launchers'),
            Gear('Double Earthshaker cannon'),
            Gear('Heavy bolter', count=2)
        ])
        self.Options(self)
