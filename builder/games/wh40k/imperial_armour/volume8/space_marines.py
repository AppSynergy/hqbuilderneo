from builder.core2 import Gear
from builder.games.wh40k.roster import Unit


class Korvydae(Unit):
    type_name = 'Shadow Captain Korvydae, Master of Recruits  (IA vol.8)'
    type_id = 'korvydae_v1'
    imperial_armour = True
    chapter = 'raven'

    def __init__(self, parent):
        super(Korvydae, self).__init__(
            parent=parent, name='Shadow Captain Korvydae',
            points=155, unique=True, static=True, gear=[
                Gear('Bolt Pistol'),
                Gear('Frag grenades'),
                Gear('Krak grenades'),
                Gear('Jump pack'),
                Gear('Iron Halo'),
                Gear('Artificer Armour'),
                Gear('Melta-bombs'),
                Gear('Thunderhammer')
            ])
