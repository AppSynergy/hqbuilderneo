__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, Gear
from builder.games.wh40k.roster import Unit, LordsOfWarSection,\
    UnitType


class Acheron(Unit):
    type_name = u'Cerastus Knight-Acheron'
    type_id = 'acheron_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Acheron, self).__init__(
            parent, points=415, gear=[
                Gear('Acheron pattern flame cannon'),
                Gear('Foe-reaper chainsword'),
                Gear('Heavy bolter'), Gear('Ion shield')
            ], static=True)


class Atrapos(Unit):
    type_name = u'Mechanicum Cerastus Knight-Atrapos'
    type_id = 'atrapos_v1'
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(Atrapos.Options, self).__init__(parent, 'Options')
            self.variant('Occular augmetics', 10)

    def __init__(self, parent):
        super(Atrapos, self).__init__(
            parent, name='Cerastus Knight-Atrapos', points=435, gear=[
                Gear('Graviton singularity cannon'),
                Gear('Atrapos lascutter'),
                Gear('Ionic flare shield'),
                Gear('Blessed Autosimulacra')
            ])
        self.Options(self)


class Castigator(Unit):
    type_name = u'Cerastus Knight-Castigator'
    type_id = 'castigator_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Castigator, self).__init__(
            parent, points=415, gear=[
                Gear('Twin-linked Castigator pattern bolt cannon'),
                Gear('Tempest warblade'),
                Gear('Ion shield')
            ], static=True)


class Lancer(Unit):
    type_name = u'Cerastus Knight-Lancer'
    type_id = 'lancer_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Lancer, self).__init__(
            parent, points=400, gear=[
                Gear('Cerastus shock lance'),
                Gear('Ion gauntlet shield')
            ], static=True)


class CerastusSection(LordsOfWarSection):
    def __init__(self, parent):
        super(CerastusSection, self).__init__(parent)
        self.cerastuses = [
            UnitType(self, Acheron),
            UnitType(self, Atrapos),
            UnitType(self, Castigator),
            UnitType(self, Lancer)
        ]


class CerastusIKSection(CerastusSection):
    def check_rules(self):
        super(CerastusIKSection, self).check_rules()
        cer_count = sum(ut.count for ut in self.cerastuses)
        if len(self.units) < 2 * cer_count:
            self.error('You may not have more Cerastus knights in your army more then you have Knights of other kinds')
