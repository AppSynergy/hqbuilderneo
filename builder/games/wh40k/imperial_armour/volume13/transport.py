__author__ = 'Ivan Truskov'

from builder.core2 import OptionalSubUnit, SubUnit
from fast import Dreadclaw
from options import ChaosSpaceMarinesBaseVehicle
from builder.games.wh40k.roster import Unit
import sys


class IATransport(OptionalSubUnit):
    def __init__(self, parent, name, order=0):
        super(IATransport, self).__init__(parent=parent, name=name,
                                          limit=1, order=order)
        self.models = []
        self.ia_models = []

    def get_unique_gear(self):
        if not self.used:
            return []
        return sum((u.unit.get_unique_gear() for u in self.models + self.ia_models if u and u.used and u.unit.get_unique_gear() is not None), [])

    def count_ruin_legacies(self):
        return sum((u.unit.count_ruin_legacies()
                    for u in self.models + self.ia_models
                    if u and u.used and isinstance(u.unit, ChaosSpaceMarinesBaseVehicle)))

    def check_rules(self):
        self.options.set_visible(self.ia_models, self.roster.ia_enabled)
        if not len(self.models):
            self.used = self.visible = self.roster.ia_enabled


class IATransportedUnit(Unit):
    def __init__(self, *args, **kwargs):
        super(IATransportedUnit, self).__init__(*args, **kwargs)
        self.transport = None

    def get_unique_gear(self):
        ugear = super(IATransportedUnit, self).get_unique_gear()
        if ugear is None:
            ugear = []
        return ugear + (self.transport.get_unique_gear()
                        if self.transport is not None else [])

    def count_ruin_legacies(self):
        if self.transport is not None:
            return self.transport.count_ruin_legacies()
        else:
            return 0

    def build_statistics(self):
        return self.note_transport(super(IATransportedUnit, self).build_statistics())


class ChaosDroppod(IATransport):
    def __init__(self, parent, name, order=0):
        super(ChaosDroppod, self).__init__(parent, name, order=order)
        claw = SubUnit(self, Dreadclaw(parent=parent))
        self.ia_models += [claw]


class IAChaosDreadnought(ChaosSpaceMarinesBaseVehicle):
    def __init__(self, parent, *args, **kwargs):
        super(IAChaosDreadnought, self).__init__(parent, *args,
                                                 walker=True, **kwargs)
        self.transport = ChaosDroppod(self, 'Transport', order=sys.maxint)

    def get_unique_gear(self):
        ugear = super(IAChaosDreadnought, self).get_unique_gear()
        if ugear is None:
            ugear = []
        return ugear + (self.transport.get_unique_gear())

    def count_legacies(self):
        return super(IAChaosDreadnought, self).count_legacies() +\
            (self.transport.count_legacies() if self.transport is not None else 0)

    def build_statistics(self):
        return self.note_transport(super(IAChaosDreadnought, self).build_statistics())
