from builder.core2 import Gear
from builder.games.wh40k.roster import Unit


class Phoros(Unit):
    type_name = 'Malakim Phoros, Master of the Lamenters, Lord of Ruin, Watcher of the Deeps (IA vol.9)'
    type_id = 'phoros_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Phoros, self).__init__(parent, 'Malakim Phoros', 175, [
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Glaive Encarmine'),
            Gear('Catechist'),
            Gear('Iron Halo'),
            Gear('Artificer armour')
        ], static=True, unique=True)
