__author__ = 'Denis Romanov'

from builder.core2 import Gear, OneOf, SubUnit, OptionsList,\
    OptionalSubUnit, Count, UnitDescription, ListSubUnit, UnitList
from builder.games.wh40k.roster import Unit

ia_id = ' (IA vol.11)'


class Spectres(Unit):
    clear_name = 'Shadow Spectres'
    type_name = clear_name + ia_id
    type_id = 'spectres_v2'
    imperial_armour = True

    model_gear = [Gear('Jetpack'), Gear('Spectre Holo-field'), Gear('Heavy aspect armour'),
                  Gear('Haywire grenades')]
    min_models = 3
    max_models = 10

    class Exarch(Unit):
        def __init__(self, parent):
            super(Spectres.Exarch, self).__init__(parent=parent, name='Exarch',
                                                  points=10 + 25, gear=Spectres.model_gear)
            self.Weapon(self)
            # self.Options(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Spectres.Exarch.Weapon, self).__init__(parent=parent, name='Weapon')
                self.variant('Prism Rifle', 0)
                self.variant('Prism Blaster', 15)
                self.variant('Haywire Launcher', 10)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Spectres.Exarch.Options, self).__init__(parent=parent, name='Warrior powers', limit=2)
                self.variant('Hit & Run', 15)
                self.variant('Night Vision', 5)
                self.variant('Monster Hunter', 10)
                self.variant('Shadow of Death', 20)

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(Spectres.Leader, self).__init__(parent, '')
            SubUnit(self, Spectres.Exarch(None))

    def __init__(self, parent):
        super(Spectres, self).__init__(parent, name=self.clear_name)
        self.leader = self.Leader(self)
        self.warriors = Count(self, 'Shadow Spectre', Spectres.min_models, Spectres.max_models, 25,
                              gear=UnitDescription('Shadow Spectre', points=25,
                                                   options=Spectres.model_gear + [Gear('Prism Rifle')]))

    def check_rules(self):
        super(Spectres, self).check_rules()
        self.warriors.min = Spectres.min_models - self.leader.count
        self.warriors.max = Spectres.max_models - self.leader.count

    def get_count(self):
        return self.warriors.cur + self.leader.count

    def has_exarch(self):
        return self.leader.count


from builder.games.wh40k.eldar_v3.armory import VehicleEquipment


class Hornets(Unit):
    clear_name = 'Hornet Squadron'
    type_name = clear_name + ia_id
    type_id = 'hornet_squadron_v2'
    imperial_armour = True

    class Hornet(ListSubUnit):
        type_name = 'Hornet'
        type_id = 'hornet_v1'

        def __init__(self, parent):
            super(Hornets.Hornet, self).__init__(parent=parent, points=70, gear=[Gear('Star Engines')])
            self.Weapon(self, name='Weapon')
            self.Weapon(self, name='')
            #self.Options(self)

        def build_description(self):
            res = super(Hornets.Hornet, self).build_description()
            res.add(self.root_unit.opt.description)
            res.add_points(self.root_unit.opt.points)
            return res

        class Weapon(OneOf):
            def __init__(self, parent, name):
                super(Hornets.Hornet.Weapon, self).__init__(parent=parent, name=name)
                self.shurikencannon = self.variant('Shuriken cannon', 0)
                self.scatterlaser = self.variant('Scatter Laser', 0)
                self.eldarmissilelauncher = self.variant('Eldar Missile Launcher', 15)
                self.starcannon = self.variant('Starcannon', 5)
                self.brightlance = self.variant('Bright Lance', 5)
                self.pulselaser = self.variant('Pulse laser', 5)

    def get_count(self):
        return self.models.count

    def __init__(self, parent):
        super(Hornets, self).__init__(parent=parent, name=self.clear_name)
        self.models = UnitList(parent=self, unit_class=self.Hornet, min_limit=1, max_limit=3)
        self.opt = VehicleEquipment(self)

    def build_points(self):
        return self.models.points + self.opt.points * self.models.count

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count())
        res.add(self.models.description)
        return res


class Nightwing(Unit):
    clear_name = 'Eldar Nightwing'
    type_name = clear_name + ia_id
    type_id = 'nightwing_v2'
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(Nightwing.Options, self).__init__(parent, 'Options')
            self.variant('Holo-fields', 15, per_model=True)

    def __init__(self, parent):
        super(Nightwing, self).__init__(parent, name=self.clear_name)
        self.models = Count(self, 'Nightwing', 1, 2, 125, True,
                            gear=UnitDescription('Nightwing', 125, options=[
                                Gear('Shuriken cannon', count=2),
                                Gear('Bright lance', count=2)]))
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.cur

    def build_points(self):
        return self.models.points + self.opt.points * self.models.cur

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        models = self.models.description[0]
        models.add(self.opt.description).add_points(self.opt.points)
        res.add(models)
        return res


class Phoenix(Unit):
    clear_name = 'Eldar Phoenix Bomber'
    type_name = clear_name + ia_id
    type_id = 'phoenix_v2'
    imperial_armour = True

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Phoenix.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Pulse laser')
            self.variant('Twin-linked bright lance')
            self.variant('Twin-linked starcannon')

    class Missiles(OneOf):
        def __init__(self, parent):
            super(Phoenix.Missiles, self).__init__(parent, 'Missile launchers')
            self.variant('Phoenix missile launchers',
                         gear=[Gear('Phoenix missile launcher', count=2)])
            self.variant('Nightfire missile launchers', 10, per_model=True,
                         gear=[Gear('Nightfire missile launcher', count=2)])

    def __init__(self, parent):
        super(Phoenix, self).__init__(parent, name=self.clear_name)
        self.models = Count(self, 'Phoenix', 1, 2, 205, True,
                            gear=UnitDescription('Phoenix', 205, options=[
                                Gear('Shuriken cannon', count=2)]))
        self.wep = self.Weapon(self)
        self.mis = self.Missiles(self)
        self.opt = Nightwing.Options(self)

    def get_count(self):
        return self.models.cur

    def build_points(self):
        return self.models.points +\
            (self.opt.points + self.mis.points) * self.models.cur

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        models = self.models.description[0]
        models.add(self.wep.description)
        models.add(self.mis.description).add_points(self.mis.points)
        models.add(self.opt.description).add_points(self.opt.points)
        res.add(models)
        return res
