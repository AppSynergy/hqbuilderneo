__author__ = 'Denis Romanov'

from builder.core2 import Gear
from builder.games.wh40k.roster import Unit

ia_id = ' (IA vol.11)'


class Redmaw(Unit):
    clear_name = 'Bran Redmaw'
    type_name = clear_name + ia_id
    type_id = 'bran_v1'

    def __init__(self, parent):
        super(Redmaw, self).__init__(parent=parent, points=210, unique=True, name=self.clear_name, gear=[
            Gear('The Axe Langnvast'),
            Gear('Wolftooth Necklace'),
            Gear('The Belt of Russ'),
            Gear('Saga of the Hunter'),
            Gear('Bolt pistol'),
            Gear('Rune armour'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
        ])
