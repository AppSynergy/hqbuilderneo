__author__ = 'Denis Romanow'

from builder.games.wh40k.section import HQSection, UnitType
from hq import *


class IA11HQSection(HQSection):
    def __init__(self, parent):
        super(IA11HQSection, self).__init__(parent)
        self.ia_units.append(UnitType(self, Redmaw, slot=0.5))
