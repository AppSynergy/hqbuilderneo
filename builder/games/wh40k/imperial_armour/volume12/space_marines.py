__author__ = 'Ivan Truskov'

from builder.core2 import Gear, UpgradeUnit
from builder.games.wh40k.space_marines_v3.heavy import Devastators
from builder.games.wh40k.roster import Unit


class Hecaton(Unit):
    type_name = 'Hecaton Aiakos, Guardian of Daedalos Krata, the Terror of Bifrost (IA vol.12)'
    type_id = 'hecaton_v1'
    chapter = 'minotaurs'

    def __init__(self, parent):
        super(Hecaton, self).__init__(parent, 'Hecaton Auakos', 225, [
            Gear('Smoke launchers'),
            Gear('Searchlight'),
            Gear('Plasma cannon'),
            Gear('Dreadnought close combat weapon'),
            Gear('Storm bolter')
        ], static=True, unique=True)


class KraatosDevastators(Devastators):

    class Kraatos(UpgradeUnit, Devastators.Sergeant):
        def __init__(self, parent):
            super(KraatosDevastators.Kraatos, self).__init__(
                parent, 'Hamath Kraatos', 60, [
                    Gear('Power armour'),
                    Gear('Bolt pistol'),
                    Gear('Heavy bolter'),
                    Gear('Frag grenades'),
                    Gear('Krak grenades'),
                    Gear('Signum'),
                    Gear('Assassin bolts')
                ])

        def check_rules(self):
            self.up.used = self.up.visible = self.roster.is_minotaurs()\
                                             and self.roster.ia_enabled
            if not self.up.used:
                self.up.value = False
            super(KraatosDevastators.Kraatos, self).check_rules()

    def get_leader(self):
        return self.Kraatos

    def get_unique(self):
        return 'Vigilator-Sergeant Hamath Kraatos' if self.sergeant.unit.up.any else None
