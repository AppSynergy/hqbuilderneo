__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OneOf, OptionsList
from builder.games.wh40k.roster import Unit
from builder.core2.options import Option


class MechanicusOption(Option):
    def __init__(self, parent, *args, **kwargs):
        super(MechanicusOption, self).__init__(parent, *args, **kwargs)
        if hasattr(parent.roster, 'convocation') and parent.roster.convocation is True:
            self.freeflag = 0
        else:
            self.freeflag = 1


class ArtefactWeapon(OneOf):
    def __init__(self, parent):
        super(ArtefactWeapon, self).__init__(parent)
        self.gauntlet = self.variant('The Paragon Gauntlet', 15)
        self.ravager = self.variant('Ravager', 25)

    def check_rules(self):
        super(ArtefactWeapon, self).check_rules()
        for variant in [self.gauntlet, self.ravager]:
            variant.used = variant.visible = self.parent.roster.allow_relics()

    def get_unique_gear(self):
        if self.cur in [self.gauntlet, self.ravager]:
            return self.description
        return []


class Heirlooms(OptionsList):
    def __init__(self, parent, weapon=None):
        super(Heirlooms, self).__init__(parent, 'Heirlooms of the Knightly Houses', limit=1)
        self.variant('Banner of Macharius Triumphant', 10)
        self.variant('Sanctuary', 15)
        self.variant('Helm of the Nameless Warrior', 30)
        self.variant('Mark of the Omnissiah', 30)
        self.twin = weapon

    def check_rules(self):
        super(Heirlooms, self).check_rules()
        flag = self.parent.roster.allow_relics()
        if self.twin is not None:
            flag = flag and len(self.twin.get_unique_gear()) == 0
        self.used = self.visible = flag


class Caparace(OptionsList, MechanicusOption):
    def __init__(self, parent):
        super(Caparace, self).__init__(parent, 'Caparace weapon', limit=1)
        self.variant('Ironstorm missile pod', 30 * self.freeflag)
        self.icarus = self.variant('Twin Icarus autocannon', 35 * self.freeflag)
        self.variant('Stormspear missile pod', 40 * self.freeflag)


class Subcaliber(OneOf, MechanicusOption):
    def __init__(self, parent):
        super(Subcaliber, self).__init__(parent, 'Subcaliber weapon')
        self.variant('Heavy stubber', 0)
        self.variant('Meltagun', 5 * self.freeflag)


class Melee(OneOf, MechanicusOption):
    def __init__(self, parent):
        super(Melee, self).__init__(parent, 'Melee weapon')
        self.variant('Reaper chainsword', 0)
        self.variant('Thunderstrike gauntlet', 10 * self.freeflag)


class Errant(Unit):
    type_name = u'Knight Errant'
    type_id = 'knight_errant_v2'
    wikilink = Unit.template_link.format(codex_name='imperial-knights', unit_name='Knight-Errant')

    class Weapon(ArtefactWeapon, Melee):
        pass

    def __init__(self, parent):
        super(Errant, self).__init__(parent, points=370, gear=[
            Gear('Thermal cannon'),
            Gear('Ion shield')
        ])
        self.mle = self.Weapon(self)
        Subcaliber(self)
        self.cap = Caparace(self)
        self.heir = Heirlooms(self, self.mle)

    def get_unique_gear(self):
        return self.mle.get_unique_gear() + self.heir.description


class Paladin(Unit):
    type_name = u'Knight Paladin'
    wikilink = 'https://sites.google.com/site/wh40000rules/codices/imperial-knights/profiles#TOC-Knight-Paladinx'
    type_id = 'knight_paladin_v2'
    wikilink = Unit.template_link.format(codex_name='imperial-knights', unit_name='Knight-Paladin')

    class Weapon(ArtefactWeapon, Melee):
        pass

    def __init__(self, parent):
        super(Paladin, self).__init__(parent, points=375, gear=[
            Gear('Heavy stubber'),
            Gear('Rapid-fire battle cannon'),
            Gear('Ion shield')
        ])
        self.mle = self.Weapon(self)
        Subcaliber(self)
        self.cap = Caparace(self)
        self.heir = Heirlooms(self, self.mle)

    def get_unique_gear(self):
        return self.mle.get_unique_gear() + self.heir.description


class Warden(Unit):
    type_name = u'Knight Warden'
    type_id = 'knight_wardem_v2'
    wikilink = Unit.template_link.format(codex_name='imperial-knights', unit_name='Knight-Warden')

    class Weapon(ArtefactWeapon, Melee):
        pass

    def __init__(self, parent):
        super(Warden, self).__init__(parent, points=375, gear=[
            Gear('Heavy flamer'),
            Gear('Avenger gatling cannon'),
            Gear('Ion shield')
        ])
        self.mle = self.Weapon(self)
        Subcaliber(self)
        self.cap = Caparace(self)
        self.heir = Heirlooms(self, self.mle)

    def get_unique_gear(self):
        return self.mle.get_unique_gear() + self.heir.description


class Gallant(Unit):
    type_name = u'Knight Gallant'
    type_id = 'knight_gallant_v2'
    wikilink = Unit.template_link.format(codex_name='imperial-knights', unit_name='Knight-Gallant')

    class LWeapon(OneOf):
        def __init__(self, parent):
            super(Gallant.LWeapon, self).__init__(parent, 'Melee')
            self.variant('Reaper chainsword', 0)
            self.rav = self.variant('Ravager', 25)
            # if invisible, it will still be used and default value will be in gear list
            self.visible = self.parent.roster.allow_relics()

        def get_unique_gear(self):
            if self.cur == self.rav:
                return self.description
            return []

    class RWeapon(OneOf):
        def __init__(self, parent):
            super(Gallant.RWeapon, self).__init__(parent, '')
            self.variant('Thunderstrike gauntlet', 0)
            self.par = self.variant('The Paragon Gauntlet', 15)
            # if invisible, it will still be used and default value will be in gear list
            self.visible = self.parent.roster.allow_relics()

        def get_unique_gear(self):
            if self.cur == self.par:
                return self.description
            return []

    def __init__(self, parent):
        super(Gallant, self).__init__(parent, points=325, gear=[
            Gear('Ion shield')
        ])
        self.mle1 = self.LWeapon(self)
        self.mle2 = self.RWeapon(self)
        Subcaliber(self)
        self.cap = Caparace(self)
        self.heir = Heirlooms(self)

    def check_rules(self):
        super(Gallant, self).check_rules()
        if len(self.get_unique_gear()) > 1:
            self.error('Only one Heirloom of the Knightly House can be on model')

    def get_unique_gear(self):
        return self.mle1.get_unique_gear()\
            + self.mle2.get_unique_gear() + self.heir.description


class Crusader(Unit):
    type_name = u'Knight Crusader'
    type_id = 'knight_crusader_c2'
    wikilink = Unit.template_link.format(codex_name='imperial-knights', unit_name='Knight-Crusader')

    class Weapon(OneOf, MechanicusOption):
        def __init__(self, parent):
            super(Crusader.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Thermal cannon', 0)
            self.variant('Rapid-fire battle cannon and heavy stubber', 5 * self.freeflag,
                         gear=[Gear('Rapid-fire battle cannon'),
                               Gear('Heavy stubber')])

    def __init__(self, parent):
        super(Crusader, self).__init__(parent, points=425, gear=[
            Gear('Avenger gatling cannon'),
            Gear('Heavy flamer'),
            Gear('Ion shield')
        ])
        self.Weapon(self)
        Subcaliber(self)
        self.cap = Caparace(self)
        self.heir = Heirlooms(self)

    def get_unique_gear(self):
        return self.heir.description


class Gerantius(Unit):
    type_name = u'Gerantius, The Forgotten Knight'
    type_id = 'gerantius'

    def __init__(self, parent):
        super(Gerantius, self).__init__(parent, name='Gerantius',
                                        points=500, static=True,
                                        unique=True, gear=[
                                            Gear('Heavy Stubber'),
                                            Gear('Thermal cannon'),
                                            Gear('Reaper chainsword'),
                                            Gear('Ion shield')
                                        ])

    def get_unique_gear(self):
        return []
