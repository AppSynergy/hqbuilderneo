__author__ = 'Ivan Truskov'

from builder.games.wh40k.roster import Wh40k7ed, CombinedArmsDetachment, \
    AlliedDetachment, Wh40kImperial, Wh40kKillTeam, Wh40kBase,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection, \
    UnitType, Formation, FastSection, HeavySection, Detachment
from builder.games.wh40k.fortifications import Fort
from hq import Ghosar, Orthan, Vorgan, Patriarch, Magus, Primus, Iconward
from troops import Favoured, Faithful, AcolyteHybrids, NeophyteHybrids
from fast import Chimera, ArmouredSentinelSquad, ScoutSentinelSquad, GoliathTruck
from heavy import GoliathRockgrinderSquad, LemanRussSquadron
from elites import Purestrain, BrothersAberrants, PurestrainGenestealers, Aberrants, HybridMetamorphs


class Broodkin(Formation):
    army_name = 'Ghosar Quintus Broodkin'
    army_id = 'gc_v1_broodkin'

    def __init__(self):
        super(Broodkin, self).__init__()
        for _type in [Ghosar, Orthan, Vorgan,
                      Purestrain, Favoured,
                      Faithful, BrothersAberrants]:
            UnitType(self, _type, min_limit=1, max_limit=1)


class Hq(HQSection):
    def __init__(self, parent):
        super(Hq, self).__init__(parent)
        UnitType(self, Patriarch)
        UnitType(self, Magus)
        UnitType(self, Primus)
        UnitType(self, Iconward)
        UnitType(self, Ghosar)
        UnitType(self, Orthan)
        UnitType(self, Vorgan)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, AcolyteHybrids)
        UnitType(self, NeophyteHybrids)
        UnitType(self, Favoured)
        UnitType(self, Faithful)


class Fasts(FastSection):
    def __init__(self, parent):
        super(Fasts, self).__init__(parent)
        UnitType(self, Chimera)
        UnitType(self, ArmouredSentinelSquad)
        UnitType(self, ScoutSentinelSquad)
        UnitType(self, GoliathTruck)


class Heavy(HeavySection):
    def __init__(self, parent):
        super(Heavy, self).__init__(parent)
        UnitType(self, GoliathRockgrinderSquad)
        UnitType(self, LemanRussSquadron)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        UnitType(self, HybridMetamorphs)
        UnitType(self, PurestrainGenestealers)
        UnitType(self, Aberrants)
        UnitType(self, Purestrain)
        UnitType(self, BrothersAberrants)


class GenestealerFormation(Formation):
    def count_primus(self):
        return 0

    def count_magus(self):
        return 0

    def count_patriarch(self):
        return 0


class SubterraneanUprising(GenestealerFormation):
    army_name = 'Subterranean Uprising'
    army_id = 'subterranean_uprising_v1'

    def __init__(self):
        super(SubterraneanUprising, self).__init__()
        self.primus = UnitType(self, Primus, min_limit=0, max_limit=1)
        UnitType(self, HybridMetamorphs, min_limit=1, max_limit=3)
        UnitType(self, AcolyteHybrids, min_limit=2, max_limit=4)
        UnitType(self, Aberrants, min_limit=0, max_limit=3)

    def count_primus(self):
        return sum((u.count for u in self.primus.units))


class DeliveranceBroodsurge(GenestealerFormation):
    army_name = 'Deliverance Broodsurge'
    army_id = 'deliverance_broodsurge_v1'

    def __init__(self):
        super(DeliveranceBroodsurge, self).__init__()
        self.neophyte = UnitType(self, NeophyteHybrids, min_limit=2, max_limit=6)

    def check_rules(self):
        super(DeliveranceBroodsurge, self).check_rules()
        for unit in self.neophyte.units:
            if not unit.transport.truck.visible:
                self.error("Neophyte Hybrids unit must be embarked in Goliath Truck")


class DemolitionClaw(GenestealerFormation):
    army_name = 'Demolition Claw'
    army_id = 'demolition_claw_v1'
    free_charges = True

    def __init__(self):
        super(DemolitionClaw, self).__init__()
        self.acolyte = UnitType(self, AcolyteHybrids, min_limit=2, max_limit=3)
        self.goliath = UnitType(self, GoliathRockgrinderSquad, min_limit=2, max_limit=3)

    def check_rules(self):
        super(DemolitionClaw, self).check_rules()
        for unit in self.goliath.units:
            if not all((u.has_demolition_charges() for u in unit.tanks.units)):
                self.error("All Goliath Rockgrinder must take a cache of demolition charges")

        for unit in self.acolyte.units:
            count_charges = sum(pal.count_charges() for pal in unit.acolythes.units)
            if count_charges == 0:
                self.error('In Acolyte squad must take at least 1 demolition charge (taken: 0)')


class BroodCycle(GenestealerFormation):
    army_name = 'Brood Cycle'
    army_id = 'brood_cycle_v1'

    def __init__(self):
        super(BroodCycle, self).__init__()
        UnitType(self, Iconward, min_limit=1, max_limit=1)
        UnitType(self, AcolyteHybrids, min_limit=3, max_limit=3)
        UnitType(self, NeophyteHybrids, min_limit=2, max_limit=2)
        UnitType(self, HybridMetamorphs, min_limit=1, max_limit=1)
        UnitType(self, PurestrainGenestealers, min_limit=1, max_limit=1)
        UnitType(self, Aberrants, min_limit=0, max_limit=1)
        UnitType(self, GoliathRockgrinderSquad, min_limit=0, max_limit=1)


class TheFirstCurse(GenestealerFormation):
    army_name = 'The First Curse'
    army_id = 'the_first_curse_v1'

    def __init__(self):
        super(TheFirstCurse, self).__init__()
        UnitType(self, Patriarch, min_limit=1, max_limit=1)
        self.gens = UnitType(self, PurestrainGenestealers, min_limit=1, max_limit=1)

    def count_patriarch(self):
        return 1

    def check_rules(self):
        super(TheFirstCurse, self).check_rules()
        gen_counts = sum((u.count for u in self.gens.units))
        if gen_counts != 20:
            self.error("The unit of Purestrain Genestealers must contain 20 models")


class NeophyteCavalcade(GenestealerFormation):
    army_name = 'Neophyte Cavalcade'
    army_id = 'neophyte_cavalcade_v1'

    def __init__(self):
        super(NeophyteCavalcade, self).__init__()
        self.neophyte = UnitType(self, NeophyteHybrids, min_limit=2, max_limit=2)
        UnitType(self, LemanRussSquadron, min_limit=1, max_limit=1)
        sentinels = [
            UnitType(self, ScoutSentinelSquad),
            UnitType(self, ArmouredSentinelSquad),
        ]
        self.add_type_restriction(sentinels, 1, 2)

    def check_rules(self):
        super(NeophyteCavalcade, self).check_rules()
        for unit in self.neophyte.units:
            if not unit.transport.chimera.visible:
                self.error("Neophyte Hybrids unit must be embarked in Chimera")


class TheDotingThrong(GenestealerFormation):
    army_name = 'The Doting Throng'
    army_id = 'the_doting_throng_v1'

    def __init__(self):
        super(TheDotingThrong, self).__init__()
        self.magus = UnitType(self, Magus, min_limit=0, max_limit=1)
        hybrids = [
            UnitType(self, NeophyteHybrids),
            UnitType(self, AcolyteHybrids),
        ]
        self.add_type_restriction(hybrids, 3, 6)

    def count_magus(self):
        return sum((u.count for u in self.magus.units))


class Bloodcoven(GenestealerFormation):
    army_name = 'Bloodcoven'
    army_id = 'bloodcoven_v1'

    def __init__(self):
        super(Bloodcoven, self).__init__()
        UnitType(self, Patriarch, min_limit=1, max_limit=1)
        UnitType(self, Magus, min_limit=1, max_limit=1)
        UnitType(self, Primus, min_limit=1, max_limit=1)

    def count_patriarch(self):
        return 1

    def count_magus(self):
        return 1

    def count_primus(self):
        return 1


class CultInsurrection(Wh40k7ed):
    army_id = 'cult_insurrection_v1'
    army_name = 'Cult Insurrection'

    class Core(Detachment):
        def __init__(self, parent):
            super(CultInsurrection.Core, self).__init__(parent, 'core', 'Core',
                                                        [
                                                            BroodCycle,
                                                            NeophyteCavalcade
                                                        ], 1, 6)

    class LordOfTheCult(GenestealerFormation):
        army_name = 'Lord Of The Cult'
        army_id = 'gs_lord__cult_v1'

        def __init__(self):
            super(CultInsurrection.LordOfTheCult, self).__init__()
            self.patriarch = UnitType(self, Patriarch)
            self.magus = UnitType(self, Magus)
            self.primus = UnitType(self, Primus)
            lords = [
                self.patriarch,
                self.magus,
                self.primus,
                UnitType(self, Iconward),
            ]
            self.add_type_restriction(lords, 1, 1)

        def count_primus(self):
            return sum((u.count for u in self.primus.units))

        def count_magus(self):
            return sum((u.count for u in self.magus.units))

        def count_patriarch(self):
            return sum((u.count for u in self.patriarch.units))

    class Command(Detachment):
        def __init__(self, parent):
            super(CultInsurrection.Command, self).__init__(parent, 'command', 'Command',
                                                           [
                                                               CultInsurrection.LordOfTheCult,
                                                               TheFirstCurse,
                                                               Bloodcoven
                                                           ],
                                                           0, 3)

    class ShadowSkulkers(GenestealerFormation):
        army_name = 'Shadow Skulkers'
        army_id = 'shadow_skulkers_v1'

        def __init__(self):
            super(CultInsurrection.ShadowSkulkers, self).__init__()
            UnitType(self, PurestrainGenestealers, min_limit=1, max_limit=1)

    class CultMutants(GenestealerFormation):
        army_name = 'Cult Mutants'
        army_id = 'cult_mutants_v1'

        def __init__(self):
            super(CultInsurrection.CultMutants, self).__init__()
            mutants = [
                UnitType(self, Aberrants),
                UnitType(self, HybridMetamorphs),
            ]
            self.add_type_restriction(mutants, 1, 1)

    class BroodBrothers(GenestealerFormation):
        army_name = 'Brood Brothers'
        army_id = 'brood_brothers_v1'

        def __init__(self):
            super(CultInsurrection.BroodBrothers, self).__init__()
            tanks = [
                UnitType(self, ScoutSentinelSquad),
                UnitType(self, ArmouredSentinelSquad),
                UnitType(self, LemanRussSquadron),
            ]
            self.add_type_restriction(tanks, 1, 1)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(CultInsurrection.Auxilary, self).__init__(parent, 'auxilary', 'Auxilary',
                                                            [
                                                                TheDotingThrong,
                                                                SubterraneanUprising,
                                                                DemolitionClaw,
                                                                DeliveranceBroodsurge,
                                                                CultInsurrection.ShadowSkulkers,
                                                                CultInsurrection.CultMutants,
                                                                CultInsurrection.BroodBrothers,
                                                            ], 1, None)

    def __init__(self):
        core = self.Core(self)
        command = self.Command(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()
        count_primus = sum((u.sub_roster.roster.count_primus() for section in self.sections for u in section.units))
        if count_primus > 1:
            self.error('Only one Primus may be taken in Cult Insurrection Detachment; taken: {}'.format(count_primus))

        count_magus = sum((u.sub_roster.roster.count_magus() for section in self.sections for u in section.units))
        if count_magus > 1:
            self.error('Only one Magus may be taken in Cult Insurrection Detachment; taken: {}'.format(count_magus))

        count_patriarch = sum(
            (u.sub_roster.roster.count_patriarch() for section in self.sections for u in section.units))
        if count_patriarch > 1:
            self.error(
                'Only one Patriarch may be taken in Cult Insurrection Detachment; taken: {}'.format(count_patriarch))


class GCBase(Wh40kBase):
    def __init__(self):
        super(GCBase, self).__init__(
            hq=Hq(self), troops=Troops(self), fast=Fasts(self), heavy=Heavy(self),
            elites=Elites(self), fort=Fort(self))


class Allies(GCBase, AlliedDetachment):
    army_name = 'Genestealer Cult (Allied detachment)'
    army_id = 'gs_v1_ad'


class Combined(GCBase, CombinedArmsDetachment):
    army_name = 'Genestealer Cult (Combined arms detachment)'
    army_id = 'gs_v1_cad'


class GCPA(GCBase, PlanetstrikeAttacker):
    army_name = 'Genestealer Cult (Planetstrike attacker detachment)'
    army_id = 'gs_v1_pa'


class GCPD(GCBase, PlanetstrikeDefender):
    army_name = 'Genestealer Cult (Planetstrike defender detachment)'
    army_id = 'gs_v1_pd'


class GCSA(GCBase, SiegeAttacker):
    army_name = 'Genestealer Cult (Siege War attacker detachment)'
    army_id = 'gs_v1_sa'


class GCSD(GCBase, SiegeDefender):
    army_name = 'Genestealer Cult (Siege War defender detachment)'
    army_id = 'gs_v1_sd'



class GenestealerCultKillTeam(Wh40kKillTeam):
    army_id = 'gs_v1_kt'
    army_name = 'Genestealer Cult'

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(GenestealerCultKillTeam.KTTroops, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [
                AcolyteHybrids, NeophyteHybrids])
            UnitType(self, Favoured)
            UnitType(self, Faithful)

    class KTFast(FastSection):
        def __init__(self, parent):
            super(GenestealerCultKillTeam.KTFast, self).__init__(parent)
            Wh40kKillTeam.process_vehicle_units(self, [
                Chimera, ArmouredSentinelSquad,
                ScoutSentinelSquad, GoliathTruck
            ])

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(GenestealerCultKillTeam.KTElites, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [HybridMetamorphs])
            UnitType(self, PurestrainGenestealers)
            UnitType(self, Aberrants)
            UnitType(self, Purestrain)
            UnitType(self, BrothersAberrants)

    def __init__(self):
        super(GenestealerCultKillTeam, self).__init__(
            self.KTTroops(self), self.KTElites(self), self.KTFast(self)
        )


faction = 'Genestealer Cults'


class GenestealerCult(Wh40kImperial, Wh40k7ed):
    army_id = 'gs_v1'
    army_name = 'Genestealer Cult'
    faction = faction

    def __init__(self):
        super(GenestealerCult, self).__init__(
            [
                Combined,
                CultInsurrection,
                SubterraneanUprising,
                DeliveranceBroodsurge,
                DemolitionClaw,
                BroodCycle,
                TheFirstCurse,
                NeophyteCavalcade,
                TheDotingThrong,
                Bloodcoven,
                Broodkin,
            ], [Allies]
        )


class GenestealerCultMissions(Wh40kImperial, Wh40k7edMissions):
    army_id = 'gs_v1_mis'
    army_name = 'Genestealer Cult'
    faction = faction

    def __init__(self):
        super(GenestealerCultMissions, self).__init__(
            [
                Combined,
                GCPA, GCPD,
                GCSA, GCSD,
                CultInsurrection,
                SubterraneanUprising,
                DeliveranceBroodsurge,
                DemolitionClaw,
                BroodCycle,
                TheFirstCurse,
                NeophyteCavalcade,
                TheDotingThrong,
                Bloodcoven,
                Broodkin,
            ], [Allies]
        )


detachments = [
    Combined,
    Allies,
    GCPA, GCPD,
    GCSA, GCSD,
    CultInsurrection,
    SubterraneanUprising,
    DeliveranceBroodsurge,
    DemolitionClaw,
    BroodCycle,
    TheFirstCurse,
    NeophyteCavalcade,
    TheDotingThrong,
    Bloodcoven,
    Broodkin
]

unit_types = [
    Ghosar, Orthan, Vorgan, Patriarch, Magus, Primus, Iconward,
    Favoured, Faithful, AcolyteHybrids, NeophyteHybrids,
    Chimera, ArmouredSentinelSquad, ScoutSentinelSquad, GoliathTruck,
    GoliathRockgrinderSquad, LemanRussSquadron,
    Purestrain, BrothersAberrants, PurestrainGenestealers,
    Aberrants, HybridMetamorphs
]
