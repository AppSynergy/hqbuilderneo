from builder.core2 import *
from builder.games.wh40k.roster import Unit
from hs import Chimera
from builder.games.wh40k.grey_knights_v3.heavy import LandRaider, LandRaiderCrusader, LandRaiderRedeemer
from builder.games.wh40k.adepta_sororitas_v4.fast import Rhino
from builder.games.wh40k.aeronautica_imperialis.fast import Valkyrie


class Transport(OptionalSubUnit):
    def __init__(self, parent):
        super(Transport, self).__init__(parent=parent, name='Transport', limit=1)
        self.transports = [
            SubUnit(self, Chimera(parent=None)),
            SubUnit(self, Rhino(parent=None)),
            SubUnit(self, Valkyrie(parent=None)),
            SubUnit(self, LandRaider(parent=None)),
            SubUnit(self, LandRaiderCrusader(parent=None)),
            SubUnit(self, LandRaiderRedeemer(parent=None))
        ]


class Acolytes(Unit):
    type_name = u'Acolytes'
    type_id = 'inquisitorial_acolytes_v2'

    class Acolyte(ListSubUnit):
        def __init__(self, parent):
            super(Acolytes.Acolyte, self).__init__(parent=parent, name='Acolyte', points=4, gear=[])
            self.armour = self.Armour(self)
            self.weapon1 = self.Weapon(self, 'Weapon', laspistol=True)
            self.weapon2 = self.Weapon(self, '', chainsword=True)
            self.opt = self.Options(self)
            self.upg = self.Upgrades(self)

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.weapon1.has_spec() or self.weapon2.has_spec()

        class Weapon(OneOf):
            def __init__(self, parent, name, laspistol=False, chainsword=False):
                super(Acolytes.Acolyte.Weapon, self).__init__(parent=parent, name=name)
                if laspistol:
                    self.variant('Laspistol', 0)
                if chainsword:
                    self.variant('Chainsword', 0)
                self.variant('Bolter', 1)
                self.variant('Storm bolter', 3)
                self.variant('Hot-shot lasgun', 5)
                self.spec = [
                    self.variant('Combi-flamer', 10),
                    self.variant('Combi-melta', 10),
                    self.variant('Combi-plasma', 10),
                    self.variant('Plasma gun', 10),
                    self.variant('Meltagun', 10),
                    self.variant('Flamer', 10),
                    self.variant('Power weapon', 15),
                    self.variant('Plasma pistol', 15),
                    self.variant('Storm shield', 20),
                    self.variant('Power fist', 25)
                ]

            def has_spec(self):
                return self.cur in self.spec

        class Armour(OneOf):
            def __init__(self, parent):
                super(Acolytes.Acolyte.Armour, self).__init__(parent=parent, name='Armour')
                self.flakarmour = self.variant('Flak armour', 0)
                self.caparacearmour = self.variant('Caparace armour', 2)
                self.powerarmour = self.variant('Power armour', 5)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Acolytes.Acolyte.Options, self).__init__(parent=parent, name='Options', limit=None)
                self.meltabombs = self.variant('Melta bombs', 5)

        class Upgrades(OptionsList):
            def __init__(self, parent):
                super(Acolytes.Acolyte.Upgrades, self).__init__(parent, 'Upgrades',)
                self.mystic = self.variant('Mystic', gear=[Gear('Laspistol')], points=6)

        def check_rules(self):
            super(Acolytes.Acolyte, self).check_rules()
            self.armour.used = self.armour.visible = not self.upg.any
            self.weapon1.used = self.weapon1.visible = not self.upg.any
            self.weapon2.used = self.weapon2.visible = not self.upg.any
            self.opt.used = self.opt.visible = not self.upg.any

    def __init__(self, parent):
        super(Acolytes, self).__init__(parent)
        self.models = UnitList(self, self.Acolyte, 3, 12)
        self.transport = Transport(self)

    def get_count(self):
        return sum(u.count for u in self.models.units)

    def check_rules(self):
        super(Acolytes, self).check_rules()
        spec = sum(u.count_spec() for u in self.models.units)
        if spec > 3:
            self.error('Only 3 Acolytes can take special weapon. (Taken: {0})'.format(spec))

    def build_statistics(self):
        return self.note_transport(super(Acolytes, self).build_statistics())


class Daemonhost(Unit):
    type_name = u'Daemonhost'
    type_id = 'daemonhost_v1'

    def __init__(self, parent):
        super(Daemonhost, self).__init__(parent, points=10,
                                         gear=[Gear('Close combat weapon')],
                                         static=True)


class Jokaero(Unit):
    type_name = u'Jokaero Weaponsmith'
    type_id = 'jokaero_v1'

    def __init__(self, parent):
        super(Jokaero, self).__init__(parent, 'Jokaero', points=35,
                                      gear=[Gear('Defence orbs'),
                                            Gear('Digital weapons')],
                                      static=True)
