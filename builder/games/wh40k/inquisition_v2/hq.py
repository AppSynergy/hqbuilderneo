__author__ = 'Denis Romanov'

from builder.core2 import *
from builder.games.wh40k.roster import Unit
from builder.games.wh40k.adepta_sororitas_v4.armory import CadianRelicsHoly, CadianWeaponHoly,\
    CadianWeaponArcana


class Armour(OneOf):
    power_armour_set = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades')]
    artificer_armour_set = [Gear('Frag grenades'), Gear('Krak grenades')]
    scout_armour_set = [Gear('Scout armor'), Gear('Frag grenades'), Gear('Krak grenades')]

    def __init__(self, parent, tda=0):
        super(Armour, self).__init__(parent, 'Armour')
        self.car = self.variant('Caparace armour', 0, gear=[Gear('Frag grenades'),
                                                            Gear('Krak grenades')])
        self.pwr = self.variant('Power armour', 3, gear=self.power_armour_set)
        self.tda = tda and self.variant('Terminator armour', tda)

    def is_tda(self):
        return self.cur == self.tda


class HQInquisitor(Unit):

    def is_malleus(self):
        return False

    def is_hereticus(self):
        return False

    def is_xenos(self):
        return False


class Coteaz(HQInquisitor):
    type_name = u'Inquisitor Coteaz'
    type_id = 'inquisitorcoteaz_v2'

    def __init__(self, parent):
        super(Coteaz, self).__init__(
            parent=parent, points=100, static=True, unique=True,
            gear=Armour.artificer_armour_set + [Gear('Bolt pistol'), Gear('Psyk-out grenades'),
                                                Gear('Master-crafted Nemesis Daemon hammer'), Gear('Psyber-eagle')])

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(Coteaz, self).build_statistics())

    def is_malleus(self):
        return True


class Karamazov(HQInquisitor):
    type_name = u'Inquisitor Karamazov'
    type_id = 'inquisitorkaramazov_v2'

    def __init__(self, parent):
        super(Karamazov, self).__init__(
            parent=parent, points=200, static=True, unique=True,
            gear=[Gear('Master-crafted multi melta'), Gear('Master-crafted power sword'), Gear('Frag grenades'),
                  Gear('Krak grenades'), Gear('Psyk-out grenades'), Gear('Rad grenades'), Gear('Orbital strike relay')])

    def is_hereticus(self):
        return True


class Greyfax(HQInquisitor):
    type_name = u'Inquisitor Greyfax, Eye of the Emperor'
    type_id = 'greyfax_v2'

    def __init__(self, parent):
        super(Greyfax, self).__init__(
            parent, points=150, static=True, unique=True, name='Inquisitor Greyfax',
            gear=[Gear('Master-crafted condemnor boltgun'), Gear('Master-crafted power sword'),
                  Gear('Frag grenades'), Gear('Krak grenades'), Gear('Psyk-out grenades'), Gear('Psyocculum')]
        )

    def is_hereticus(self):
        return True


class Relics(OptionsList):
    def __init__(self, parent):
        super(Relics, self).__init__(parent, 'Relics', limit=1)
        if parent.is_malleus():
            self.variant('The Grimoire of True Names', 5)
        if parent.is_hereticus():
            self.variant('The Liber Heresius', 15)
        if parent.is_xenos():
            self.variant('The Tome of Vethric', 20)


class HolyRelics(CadianRelicsHoly, Relics):
    pass


class Malleus(HQInquisitor):
    type_name = u'Ordo Malleus Inquisitor'
    type_id = 'ordomalleusinquisitor_v2'

    def __init__(self, parent):
        super(Malleus, self).__init__(parent=parent, points=25, gear=[Gear('Psyk-out grenades')])
        self.armour = Armour(self, tda=40)

        self.weapon1 = self.ArcanaWeapon(self, 'Weapon', armour=self.armour, boltpistol=True, tda_ranged=True)
        self.weapon2 = self.HolyWeapon(self, '', armour=self.armour, chainsword=True, tda_melee=True)

        self.opt = self.Options(self)
        self.weapon1.psyker = self.weapon2.psyker = self.opt
        self.relics = HolyRelics(self)

    def get_unique_gear(self):
        return self.relics.description + self.weapon1.get_unique() + self.weapon2.get_unique()

    class Options(OptionsList):
        def __init__(self, parent):
            super(Malleus.Options, self).__init__(parent=parent, name='Options', limit=None)

            self.psyboltammunition = self.variant('Psybolt ammunition', 5)
            self.empyreanbrainmines = self.variant('Empyrean brain mines', 10)
            self.masterylevel1 = self.variant('Psyker', 30, gear=[Gear('Psyker'), Gear('Mastery level 1')])

        def is_psyker(self):
            return self.masterylevel1.value

    class Weapon(OneOf):
        def __init__(self, parent, name, armour, boltpistol=False, chainsword=False, tda_melee=False, tda_ranged=False):
            super(Malleus.Weapon, self).__init__(parent=parent, name=name)

            self.armour = armour

            self.power_weapon = []
            self.tda_weapon = []
            if chainsword:
                self.power_weapon.append(self.variant('Chainsword', 0))
            if boltpistol:
                self.power_weapon.append(self.variant('Bolt pistol', 0))
            if tda_melee:
                self.tda_weapon.append(self.variant('Nemesis Daemon hammer', 0))
            if tda_ranged:
                self.tda_weapon.append(self.variant('Storm bolter', 0))

            self.psyker = None
            self.forcesword = self.variant('Force weapon', 0)

            self.power_weapon.extend([
                self.variant('Power weapon', 15),
                self.variant('Power fist', 25),
                self.variant('Plasma pistol', 15)
            ])
            comby = [
                self.variant('Combi-flamer', 10),
                self.variant('Combi-melta', 10),
                self.variant('Combi-plasma', 10)
            ]
            if tda_melee:
                self.power_weapon.extend(comby)
            if tda_ranged:
                self.tda_weapon.append(self.variant('Psycannon', 15))
            self.power_weapon.extend([
                self.variant('Incinerator', 15),
                self.variant('Nemesis Daemon hammer', 15),
                self.variant('Daemonblade', 15),
                self.variant('Hellrifle', 15)
            ])

        def check_rules(self):
            super(Malleus.Weapon, self).check_rules()
            self.forcesword.active = self.forcesword.used = self.psyker.is_psyker()
            if self.armour.is_tda():
                visible = self.tda_weapon
                invisible = self.power_weapon
            else:
                visible = self.power_weapon
                invisible = self.tda_weapon

            for wep in visible:
                wep.visible = True
            for wep in invisible:
                wep.visible = False

    def check_rules(self):
        super(Malleus, self).check_rules()
        self.relics.used = self.relics.visible = self.weapon2.cur not in self.weapon2.relic_options

    class HolyWeapon(CadianWeaponHoly, Weapon):
        def __init__(self, *args, **kwargs):
            super(Malleus.HolyWeapon, self).__init__(*args, **kwargs)
            self.relic_options = [self.worthy_blade]

        def get_unique(self):
            return self.description if self.cur in self.relic_options else []

    class ArcanaWeapon(CadianWeaponArcana, Weapon):
        def __init__(self, *args, **kwargs):
            super(Malleus.ArcanaWeapon, self).__init__(*args, **kwargs)
            self.relic_options = [self.qann]

        def get_unique(self):
            return self.description if self.cur in self.relic_options else []

    def count_charges(self):
        if self.opt.is_psyker():
            return 1
        return 0

    def build_statistics(self):
        return self.note_charges(super(Malleus, self).build_statistics())

    def is_malleus(self):
        return True


class Hereticus(HQInquisitor):
    type_name = u'Ordo Hereticus Inquisitor'
    type_id = 'ordohereticusinquisitor_v2'

    def __init__(self, parent):
        super(Hereticus, self).__init__(parent=parent, points=25, gear=[Gear('Psyk-out grenades')])

        self.armour = Armour(self)

        self.weapon1 = self.ArcanaWeapon(self, 'Weapon', boltpistol=True)
        self.weapon2 = self.HolyWeapon(self, '', chainsword=True)

        self.opt = self.Options(self)
        self.relics = HolyRelics(self)

    def get_unique_gear(self):
        return self.relics.description + self.weapon1.get_unique() + self.weapon2.get_unique()

    class Weapon(OneOf):
        def __init__(self, parent, name, boltpistol=False, chainsword=False):
            super(Hereticus.Weapon, self).__init__(parent=parent, name=name)

            if chainsword:
                self.chainsword = self.variant('Chainsword', 0)
            if boltpistol:
                self.boltpistol = self.variant('Bolt pistol', 0)

            self.psyker = None
            self.forcesword = self.variant('Force weapon', 0)

            self.powerweapon = self.variant('Power weapon', 15)
            self.plasmapistol = self.variant('Plasma pistol', 15)
            self.plasmapistol = self.variant('Inferno pistol', 10)
            self.combiflamer = self.variant('Combi-flamer', 10)
            self.combimelta = self.variant('Combi-melta', 10)
            self.combiplasma = self.variant('Combi-plasma', 10)
            self.powerfist = self.variant('Power fist', 25)
            self.condemnorboltgun = self.variant('Condemnor boltgun', 15)
            self.thunderhammer = self.variant('Thunder hammer', 20)
            self.nullrod = self.variant('Null rod', 25)

    class HolyWeapon(CadianWeaponHoly, Weapon):
        def __init__(self, *args, **kwargs):
            super(Hereticus.HolyWeapon, self).__init__(*args, **kwargs)
            self.relic_options = [self.worthy_blade]

        def get_unique(self):
            return self.description if self.cur in self.relic_options else []

    class ArcanaWeapon(CadianWeaponArcana, Weapon):
        def __init__(self, *args, **kwargs):
            super(Hereticus.ArcanaWeapon, self).__init__(*args, **kwargs)
            self.relic_options = [self.qann]

        def get_unique(self):
            return self.description if self.cur in self.relic_options else []

    class Options(OptionsList):
        def __init__(self, parent):
            super(Hereticus.Options, self).__init__(parent=parent, name='Options', limit=None)

            self.psyocculum = self.variant('Psyocculum', 25)
            self.masterylevel1 = self.variant('Psyker', 30, gear=[Gear('Psyker'), Gear('Mastery level 1')])

        def check_rules(self):
            super(Hereticus.Options, self).check_rules()
            for o in (self.parent.weapon1, self.parent.weapon2):
                o.forcesword.active = o.forcesword.used = self.masterylevel1.value

        def is_psyker(self):
            return self.masterylevel1.value

    def check_rules(self):
        super(Hereticus, self).check_rules()
        self.relics.used = self.relics.visible = self.weapon2.cur not in self.weapon2.relic_options

    def count_charges(self):
        if self.opt.is_psyker():
            return 1
        return 0

    def build_statistics(self):
        return self.note_charges(super(Hereticus, self).build_statistics())

    def is_hereticus(self):
        return True


class Xenos(HQInquisitor):
    type_name = u'Ordo Xenos Inquisitor'
    type_id = 'ordoxenosinquisitor_v2'

    def __init__(self, parent):
        super(Xenos, self).__init__(parent=parent, points=25, gear=[Gear('Psyk-out grenades')])

        self.armour = Armour(self)

        self.weapon1 = self.ArcanaWeapon(self, 'Weapon', boltpistol=True)
        self.weapon2 = self.HolyWeapon(self, '', chainsword=True)

        self.opt = self.Options(self)
        self.relics = HolyRelics(self)

    def get_unique_gear(self):
        return self.relics.description + self.weapon2.get_unique() + self.weapon1.get_unique()

    def check_rules(self):
        super(Xenos, self).check_rules()
        self.relics.used = self.relics.visible = self.weapon2.cur not in self.weapon2.relic_options

    class Weapon(OneOf):
        def __init__(self, parent, name, boltpistol=False, chainsword=False):
            super(Xenos.Weapon, self).__init__(parent=parent, name=name)

            if chainsword:
                self.chainsword = self.variant('Chainsword', 0)
            if boltpistol:
                self.boltpistol = self.variant('Bolt pistol', 0)

            self.psyker = None
            self.forcesword = self.variant('Force weapon', 0)

            self.powersword = self.variant('Power weapon', 10)
            self.plasmapistol = self.variant('Plasma pistol', 15)
            self.combiflamer = self.variant('Combi-flamer', 10)
            self.combimelta = self.variant('Combi-melta', 10)
            self.combiplasma = self.variant('Combi-plasma', 10)
            self.needlepistol = self.variant('Needle pistol', 15)
            self.scythianvenomtalon = self.variant('Scythian venom talon', 15)
            self.conversionbeamer = self.variant('Conversion beamer', 45)

    class HolyWeapon(CadianWeaponHoly, Weapon):
        def __init__(self, *args, **kwargs):
            super(Xenos.HolyWeapon, self).__init__(*args, **kwargs)
            self.relic_options = [self.worthy_blade]

        def get_unique(self):
            return self.description if self.cur in self.relic_options else []

    class ArcanaWeapon(CadianWeaponArcana, Weapon):
        def __init__(self, *args, **kwargs):
            super(Xenos.ArcanaWeapon, self).__init__(*args, **kwargs)
            self.relic_options = [self.qann]

        def get_unique(self):
            return self.description if self.cur in self.relic_options else []

    class Options(OptionsList):
        def __init__(self, parent):
            super(Xenos.Options, self).__init__(parent=parent, name='Options', limit=None)

            self.digitalweapons = self.variant('Digital weapons', 5)
            self.ulumeathiplasmasyphon = self.variant('Ulumeathi Plasma Syphon', 10)
            self.radgrenades = self.variant('Rad grenades', 15)
            self.psychotrokegrenades = self.variant('Psychotroke grenades', 15)
            self.masterylevel1 = self.variant('Psyker', 30, gear=[Gear('Psyker'), Gear('Mastery level 1')])

        def check_rules(self):
            super(Xenos.Options, self).check_rules()
            for o in (self.parent.weapon1, self.parent.weapon2):
                o.forcesword.active = o.forcesword.used = self.masterylevel1.value

        def is_psyker(self):
            return self.masterylevel1.value

    def count_charges(self):
        if self.opt.is_psyker():
            return 1
        return 0

    def build_statistics(self):
        return self.note_charges(super(Xenos, self).build_statistics())

    def is_xenos(self):
        return True
