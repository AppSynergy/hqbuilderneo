from builder.core2 import Unit, OneOf, Gear, OptionsList


class Vehicle(OptionsList):
    def __init__(self, parent):
        super(Vehicle, self).__init__(parent, 'Options')

        self.sbgun = self.Variant(self, 'Storm bolter', 5)
        self.dblade = self.Variant(self, 'Dozer blade', 5)
        self.psybolt = self.Variant(self, 'Psybolt ammunition', 5)
        self.exarm = self.Variant(self, 'Extra armour', 10)
        self.hkm = self.Variant(self, 'Hunter-killer missile', 10)
        self.tsarm = self.Variant(self, 'Truesilver armour', 10)


class Chimera(Unit):
    type_id = 'inquisitorial_chimera_v3'
    type_name = u'Chimera'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Chimera.Weapon1, self).__init__(parent, 'Weapon')
            self.tlhbgun = self.variant('Heavy bolter', 0)
            self.tlhflame = self.variant('Heavy flamer', 0)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Chimera.Weapon2, self).__init__(parent, '')
            self.tlasscan = self.variant('Multi-laser', 0)
            self.tlhbgun = self.variant('Heavy bolter', 0)
            self.tlhflame = self.variant('Heavy flamer', 0)

    def __init__(self, parent):
        super(Chimera, self).__init__(parent=parent, points=55, gear=[
            Gear('Smoke launchers'), Gear('Searchlight'),
        ])
        Chimera.Weapon1(self)
        Chimera.Weapon2(self)
        Vehicle(self)
