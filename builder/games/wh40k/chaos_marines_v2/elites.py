__author__ = 'Denis Romanow'

from builder.core2 import *
from armory import *
from heavy import LandRaider
from troops import RhinoTransport, MarineTransport
from builder.games.wh40k.imperial_armour.volume13.transport import IAChaosDreadnought,\
    IATransport, IATransportedUnit
from builder.games.wh40k.roster import Unit


class LandRaiderTransport(IATransport):
    def __init__(self, parent):
        super(LandRaiderTransport, self).__init__(parent, 'Transport', order=1001)
        self.models += [SubUnit(self, LandRaider(parent=None))]


class Chosen(IconicUnit, IATransportedUnit):
    type_name = u'Chosen'
    type_id = 'chosen_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Chosen')

    model_points = 18
    allow_marks = True
    allow_transport = True
    allow_veterans = True

    class SingleChosen(IconBearerGroup):

        class Weapon(OptionsList):
            def __init__(self, parent):
                super(Chosen.SingleChosen.Weapon, self).__init__(parent, name='Weapon', limit=1)
                bp = [Gear('Bolt pistol')]
                bg = [Gear('Boltgun')]
                ccw = [Gear('Close combat weapon')]

                self.lc = self.variant('Lightning claw', 15, gear=[Gear('Lightning claw')] + bg + bp)
                self.pw = self.variant('Power weapon', 15, gear=[Gear('Power weapon')] + bg + bp)
                self.pf = self.variant('Power fist', 25, gear=[Gear('Power fist')] + bg + bp)

                self.pp = self.variant('Plasma pistol', 15, gear=[Gear('Plasma pistol')] + bg + ccw)

                self.cb = self.variant('Combi-bolter', 3, gear=[Gear('Combi-bolter')] + bp + ccw)
                self.fl = self.variant('Flamer', 5, gear=[Gear('Flamer')] + bp + ccw)
                self.cm = self.variant('Combi-melta', 10, gear=[Gear('Combi-melta')] + bp + ccw)
                self.cf = self.variant('Combi-flamer', 10, gear=[Gear('Combi-flamer')] + bp + ccw)
                self.cp = self.variant('Combi-plasma', 10, gear=[Gear('Combi-plasma')] + bp + ccw)
                self.mg = self.variant('Meltagun', 10, gear=[Gear('Meltagun')] + bp + ccw)
                self.hb = self.variant('Heavy bolter', 10, gear=[Gear('Heavy bolter')] + bp + ccw)
                self.ac = self.variant('Autocannon', 10, gear=[Gear('Autocannon')] + bp + ccw)
                self.pg = self.variant('Plasma gun', 15, gear=[Gear('Plasma gun')] + bp + ccw)
                self.ml = self.variant('Missile launcher', 15, gear=[Gear('Missile launcher')] + bp + ccw)
                self.las = self.variant('Lascannon', 20, gear=[Gear('Lascannon')] + bp + ccw)

                self.lc2 = self.variant('Pair of lightning claws', 30, gear=Gear('Lightning claw', count=2))

                self.heavy = [self.hb, self.ac, self.ml, self.las]
                self.spec = [self.mg, self.pg, self.fl]
                self.wep = [self.lc, self.pw, self.pf, self.pp, self.lc2, self.cb, self.cm, self.cf, self.cp]

            def is_heavy(self):
                return any(o.value for o in self.heavy)

            def is_spec(self):
                return any(o.value for o in self.spec)

            def is_wep(self):
                return any(o.value for o in self.wep)

            @property
            def description(self):
                if self.any:
                    return super(Chosen.SingleChosen.Weapon, self).description
                return [Gear('Bolt pistol'), Gear('Boltgun'), Gear('Close combat weapon')]

        def __init__(self, parent):
            super(Chosen.SingleChosen, self).__init__(
                parent, name='Chosen', points=Chosen.model_points, gear=Armour.power_armour_set,
                wrath=20, flame=15, despair=10, excess=35, vengeance=25
            )
            self.weapon = self.Weapon(self)

        @ListSubUnit.count_gear
        def count_heavy(self):
            return self.weapon.is_heavy()

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.weapon.is_spec()

        @ListSubUnit.count_gear
        def count_wep(self):
            return self.weapon.is_wep()

    class Champion(IconBearer):

        class Weapon1(Ranged, Melee, Ccw):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        class Weapon3(Ranged, Boltgun):
            def __init__(self, parent, name, **kwargs):
                super(Chosen.Champion.Weapon3, self).__init__(parent, name, **kwargs)
                self.spec = [
                    self.variant('Flamer', 5),
                    self.variant('Meltagun', 10),
                    self.variant('Plasma gun', 15),
                ]

            def is_spec(self):
                return self.cur in self.spec

        class Options(OptionsList):
            def __init__(self, parent):
                super(Chosen.Champion.Options, self).__init__(parent, name='Options')
                self.variant('Melta bombs', 5)
                self.variant('Gift of mutation', 10)

        def __init__(self, parent):
            super(Chosen.Champion, self).__init__(
                parent, points=90 - Chosen.model_points * 4, name='Chosen Champion', gear=Armour.power_armour_set,
                wrath=20, flame=15, despair=10, excess=35, vengeance=25
            )

            self.wep1 = self.Weapon1(self, name='Weapon')
            self.wep2 = self.Weapon2(self, name='')
            self.wep3 = self.Weapon3(self, name='')
            self.Options(self)

        def count_spec(self):
            return 1 if self.wep3.is_spec() else 0

        def check_rules(self):
            super(Chosen.Champion, self).check_rules()
            self.wep1.set_unique_ranged(not self.wep2.has_unique_ranged() and not self.wep3.has_unique_ranged())
            self.wep2.set_unique_ranged(not self.wep1.has_unique_ranged() and not self.wep3.has_unique_ranged())
            self.wep3.set_unique_ranged(not self.wep1.has_unique_ranged() and not self.wep1.has_unique_ranged())

    def __init__(self, parent):
        super(Chosen, self).__init__(parent)
        if self.allow_marks:
            self.marks = Marks(self, khorne=2, tzeentch=2, nurgle=3, slaanesh=2, per_model=True)
        self.champion = SubUnit(self, self.Champion(None))
        self.models = UnitList(self, self.SingleChosen, min_limit=4, max_limit=9)
        if self.allow_veterans:
            self.veteran = Veterans(self, points=2, per_model=True)
        if self.allow_transport:
            self.transport = MarineTransport(self)

    def check_rules(self):
        super(Chosen, self).check_rules()

        heavy = sum(o.count_heavy() for o in self.models.units)
        spec = sum(o.count_spec() for o in self.models.units + [self.champion.unit])
        wep = sum(o.count_wep() for o in self.models.units)

        if heavy > 1:
            self.error("Only one chosen can exchange his bolter for heavy weapon. (taken: {})".format(heavy))
        elif heavy == 0:
            if spec > 0:
                spec -= 1
        if spec + wep > 4:
            self.error("Only 4 models (chosen and champion) can take special weapons (taken: {0}).".format(spec + wep))


class Possessed(IconBearer, IATransportedUnit):
    type_name = "Possessed"
    type_id = "possessed_v1"
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Possessed')

    model_points = 26
    model_gear = [Gear('Close combat weapon'), Gear('Power armour')]

    class Champion(IconBearer):

        def __init__(self, parent):
            super(Possessed.Champion, self).__init__(
                parent, points=130 - Possessed.model_points * 4, name='Possessed Champion',
                gear=Possessed.model_gear, wrath=15, flame=5, despair=5, excess=35, vengeance=5
            )
            Count(self, 'Gift of mutation', 0, 2, points=10)

    class Count(Count):
        @property
        def description(self):
            model = UnitDescription('Possessed', points=Possessed.model_points, options=Possessed.model_gear)
            if self.parent.count_icon():
                return [model.clone().set_count(self.cur - 1),
                        model.add(self.parent.icon.description).add_points(self.parent.icon.points)]
            return [model.set_count(self.cur)]

    def __init__(self, parent):
        super(Possessed, self).__init__(parent, wrath=15, flame=5, despair=5, excess=35,
                                        vengeance=5)
        self.marks = Marks(self, khorne=3, tzeentch=5, nurgle=4, slaanesh=3, per_model=True)

        self.champion = SubUnit(self, self.Champion(None))
        self.models = self.Count(self, 'Possessed', 4, 19, self.model_points)

        self.veteran = Veterans(self, points=2, per_model=True)
        self.transport = RhinoTransport(self)

    def get_count(self):
        return self.models.cur + 1

    def check_rules(self):
        super(Possessed, self).check_rules()
        count = 0
        for u in [self.champion.unit, self]:
            u.mark_icon(self.marks)
            count += u.count_icon()
        if count > 1:
            self.error("{0} can't take more then 1 icon (taken: {1}).".format(self.name, count))

    def build_description(self):
        description = UnitDescription(name=self.name, points=self.points, count=self.count)
        for o in [self.marks, self.champion, self.models, self.veteran, self.transport]:
            if o.used:
                description.add(o.description)
        return description

    def not_nurgle(self):
        return self.marks.any and not self.marks.nurgle


class Terminators(IconicUnit, IATransportedUnit):
    type_name = u'Chaos Terminators'
    type_id = 'chaos_terminators_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Chaos-Terminators')

    model_points = 31

    class Champion(IconBearer):

        class Weapon1(TerminatorMelee, TerminatorPower):
            pass

        class Weapon2(TerminatorRanged, TerminatorBolter):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Terminators.Champion.Options, self).__init__(parent, name='Options')
                self.variant('Gift of mutation', 10)

        def __init__(self, parent):
            super(Terminators.Champion, self).__init__(
                parent, points=95 - Terminators.model_points * 2, name='Terminator Champion',
                gear=[Gear('Terminator armour')], wrath=25, flame=15, despair=10, excess=40, vengeance=35
            )

            self.wep1 = self.Weapon1(self, name='Weapon')
            self.wep2 = self.Weapon2(self, name='')
            self.Options(self)

    class Terminator(IconBearerGroup):
        def __init__(self, parent):
            super(Terminators.Terminator, self).__init__(
                parent, name='Chaos Terminator', points=Terminators.model_points, gear=[Gear('Terminator armour')],
                wrath=25, flame=15, despair=10, excess=40, vengeance=35
            )
            self.weapon = self.Weapon(self)

        @ListSubUnit.count_gear
        def count_heavy(self):
            return self.weapon.is_heavy()

        class Weapon(OptionsList):
            def __init__(self, parent):
                super(Terminators.Terminator.Weapon, self).__init__(parent, name='Weapon')

                self.pw = [Gear('Power weapon')]
                self.ccw = [
                    self.variant('Lightning claw', 3),
                    self.variant('Power fist', 7),
                    self.variant('Chainfist', 12),
                ]
                self.ranged = [
                    self.variant('Combi-melta', 5),
                    self.variant('Combi-flamer', 5),
                    self.variant('Combi-plasma', 5),
                ]
                self.heavy = [
                    self.variant('Heavy flamer', 10),
                    self.variant('Reaper autocannon', 25),
                ]
                self.pair = self.variant('Pair of lightning claws', 7, gear=[Gear('Lightning claw', count=2)])

            @property
            def description(self):
                ccw = any(o.value for o in self.ccw)
                heavy = any(o.value for o in self.heavy)
                ranged = any(o.value for o in self.ranged)
                desc = super(Terminators.Terminator.Weapon, self).description
                if not ranged and not heavy and not self.pair.value:
                    desc.append(Gear('Combi-bolter'))
                if not ccw and not self.pair.value:
                    desc.append(Gear('Power weapon'))
                return desc

            def is_heavy(self):
                return any(o.value for o in self.heavy)

            def check_rules(self):
                super(Terminators.Terminator.Weapon, self).check_rules()
                if self.pair.value:
                    self.process_limit(self.options, 1)
                else:
                    self.process_limit(self.heavy + self.ranged, 1)
                    self.process_limit(self.ccw, 1)
                    self.pair.active = self.pair.used = not self.any
                # if self.pair.value.
                # ranged = any(o.value for o in self.ranged + self.pair)
                # heavy = any(o.value for o in self.heavy)
                # if ranged:
                #     self.process_limit(self.ccw + self.ranged + self.pair + self.heavy, 1)
                # elif heavy:
                #     self.process_limit(self.ccw, 1)
                #     self.process_limit(self.heavy + self.ranged + self.pair, 1)
                # else:
                #     self.process_limit(self.ccw + self.ranged + self.pair, 1)
                #     self.process_limit(self.heavy, 1)

    class Bringers(OptionsList):
        def __init__(self, parent):
            super(Terminators.Bringers, self).__init__(parent, name='')
            self.up = self.variant('The Bringers of Despair', 6, per_model=True)

        def check_rules(self):
            super(Terminators.Bringers, self).check_rules()
            # option is no longer available
            # kept here for backward compability
            self.visible = self.used = False  # self.roster.is_black and self.roster.has_abaddon

        @property
        def points(self):
            return self.parent.get_count() * super(Terminators.Bringers, self).points

    def __init__(self, parent):
        super(Terminators, self).__init__(parent)
        self.marks = Marks(self, khorne=3, tzeentch=5, nurgle=6, slaanesh=4, per_model=True)
        self.champion = SubUnit(self, self.Champion(None))
        self.models = UnitList(self, self.Terminator, min_limit=2, max_limit=9)

        self.veteran = Veterans(self, points=3, per_model=True)
        self.bringers = self.Bringers(self)
        self.transport = LandRaiderTransport(self)

    def check_rules(self):
        super(Terminators, self).check_rules()
        spec_lim = int(self.get_count() / 5)
        taken = sum((u.count_heavy() for u in self.models.units))
        if taken > spec_lim:
            self.error("Chaos Terminators can't have more then 1 special weapon for 5 terminators (taken {}).".
                       format(taken))

    @property
    def is_bringers(self):
        return self.bringers.up.value and self.bringers.used


class SOTerminators(IATransportedUnit):
    type_name = u'Scarab Occult Terminators'
    type_id = 'so_terminators_v1'

    veterans = True

    class Terminator(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(SOTerminators.Terminator.Weapon, self).__init__(parent, 'Ranged weapon')
                self.cb = self.variant('Inferno combi-bolter')
                self.variant('Heavy warpflamer', 15)
                self.variant('Soulreaper cannon', 30)

        class Missiles(OptionsList):
            def __init__(self, parent):
                super(SOTerminators.Terminator.Missiles, self).__init__(parent, '')
                self.variant('Hellfyre missile rack', 20)

        def __init__(self, parent):
            super(SOTerminators.Terminator, self).__init__(parent, 'Scarab Occult Terminator', 40,
                                                           [Gear('Terminator armour'),
                                                            Gear('Power sword')])
            self.wep = self.Weapon(self)
            self.rack = self.Missiles(self)

        @ListSubUnit.count_gear
        def has_cannon(self):
            return self.wep.cur != self.wep.cb

        @ListSubUnit.count_gear
        def has_missile(self):
            return self.rack.any

    class Sorcerer(Unit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(SOTerminators.Sorcerer.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Inferno combi-bolter')
                self.variant('Power sword', 5)

        class Gift(OptionsList):
            def __init__(self, parent):
                super(SOTerminators.Sorcerer.Gift, self).__init__(parent, 'Options')
                self.variant('Gift of mutation', 10)

        def __init__(self, parent):
            super(SOTerminators.Sorcerer, self).__init__(parent, 'Scarab Occult Sorcerer', 250 - 4 * 40,
                                                         [Gear('Terminator armour'),
                                                          Gear('Force stave')])
            self.Weapon(self)
            self.Gift(self)

    def __init__(self, parent):
        super(SOTerminators, self).__init__(parent, gear=[
            Gear('Veterans of the Long War'),
            Gear('Mark of Tzeentch')
        ])
        self.boss = SubUnit(self, self.Sorcerer(parent=None))
        self.models = UnitList(self, self.Terminator, 4, 9)
        self.transport = LandRaiderTransport(self)

    def get_count(self):
        return self.models.count + 1

    def check_rules(self):
        super(SOTerminators, self).check_rules()
        can_cnt = sum(u.has_cannon() for u in self.models.units)
        if can_cnt > (1 + self.models.count) / 5:
            self.error("Cannot take more then 1 alternate ranged weapon per 5 models (taken {})".format(can_cnt))
        rack_cnt = sum(u.has_missile() for u in self.models.units)
        if rack_cnt > (1 + self.models.count) / 5:
            self.error("Cannot take more then 1 missile rack per 5 models (taken {})".format(rack_cnt))

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(SOTerminators, self).build_statistics())


class Helbrute(IAChaosDreadnought):
    type_name = u'Helbrute'
    type_id = 'helbrute_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Helbrute')

    def __init__(self, parent):
        super(Helbrute, self).__init__(parent=parent, points=100, gear=[])
        self.wep1 = self.Weapon(self, 'Weapon')
        self.build1 = self.BuiltIn(self)
        self.wep2 = self.Fist(self, '')
        self.build2 = self.BuiltIn(self)

    def check_rules(self):
        super(Helbrute, self).check_rules()
        self.build1.visible = self.build1.used = self.wep1.has_fist()
        self.build2.visible = self.build2.used = self.wep2.has_fist()
        self.wep1.activate_missile(not self.wep2.has_missile())
        self.wep2.activate_missile(not self.wep1.has_missile())

    class Fist(OneOf):
        def __init__(self, parent, name):
            super(Helbrute.Fist, self).__init__(parent=parent, name=name)

            self.powerfist = self.variant('Power fist', 0)
            self.thunderhammer = self.variant('Thunder hammer', 5)
            self.powerscourge = self.variant('Power scourge', 10)
            self.missilelauncher = self.variant('Missile launcher', 10)

        def has_fist(self):
            return self.cur == self.powerfist

        def has_missile(self):
            return self.cur == self.missilelauncher

        def activate_missile(self, val):
            self.missilelauncher.active = val

    class BuiltIn(OptionsList):
        def __init__(self, parent):
            super(Helbrute.BuiltIn, self).__init__(parent=parent, name='', limit=1)

            self.combibolter = self.variant('Combi-bolter', 5)
            self.heavyflamer = self.variant('Heavy flamer', 15)

    class Melta(OneOf):
        def __init__(self, parent, name):
            super(Helbrute.Melta, self).__init__(parent=parent, name=name)

            self.multimelta = self.variant('Multi-melta', 0)

    class Heavy(OneOf):
        def __init__(self, parent, name):
            super(Helbrute.Heavy, self).__init__(parent=parent, name=name)
            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 5)
            self.reaperautocannon = self.variant('Reaper autocannon', 5)
            self.plasmacannon = self.variant('Plasma cannon', 10)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 25)

    class Weapon(Heavy, Fist, Melta):
        pass


class Mutilators(Unit):
    type_name = "Mutilators"
    type_id = "mutilators_v1"
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Mutilator')

    def __init__(self, parent):
        Unit.__init__(self, parent)
        self.models = Count(self, 'Mutilators', 1, 3, 55,
                            gear=UnitDescription('Mutilator', points=55, options=[Gear('Fleshmetal')]))
        self.marks = Marks(self, khorne=5, tzeentch=8, nurgle=6, slaanesh=6, per_model=True)
        Veterans(self, 4, per_model=True)

    def get_count(self):
        return self.models.cur

    def not_nurgle(self):
        return self.marks.any and not self.marks.nurgle


class Berzerks(IATransportedUnit):
    type_name = u'Khorne Berzerkers'
    type_id = 'khorne_berzerkers_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Khorne-Berzerkers')

    model_points = 19
    model_gear = Armour.power_armour_set

    def __init__(self, parent):
        super(Berzerks, self).__init__(parent, gear=[Gear('Mark of Khorne')])
        self.champion = SubUnit(self, self.Champion(None))
        self.models = UnitList(self, self.Berserk, min_limit=4, max_limit=19)

        self.veteran = Veterans(self, points=1, per_model=True, crimson=True)
        self.transport = RhinoTransport(self)

    def check_rules(self):
        super(Berzerks, self).check_rules()
        taken = sum((it.count_spec() for it in self.models.units))
        if taken > 2:
            self.error("Khorne Berzerkers can't have more then 2 plasma pistols in unit (taken: {0}).".format(taken))

        icon = sum(u.count_icon() for u in [self.champion.unit] + self.models.units)
        if icon > 1:
            self.error("Khorne Berzerkers can't take more then 1 icon (taken: {0}).".format(icon))

    def get_count(self):
        return self.models.count + 1

    class Options(OptionsList):
        def __init__(self, parent):
            super(Berzerks.Options, self).__init__(parent=parent, name='Options')
            self.chainaxe = self.variant('Chainaxe', 3)
            self.iconofwrath = self.variant('Icon of wrath', 15)

    class ChampionOptions(Options):
        def __init__(self, parent):
            super(Berzerks.ChampionOptions, self).__init__(parent)
            self.meltabombs = self.variant('Melta bombs', 5)
            self.giftofmutation = self.variant('Gift of mutation', 10)

    class Berserk(ListSubUnit):

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Berzerks.Berserk.Weapon, self).__init__(parent=parent, name='Weapon')
                self.boltpistol = self.variant('Bolt pistol', 0)
                self.plasmapistol = self.variant('Plasma pistol', 15)

        def __init__(self, parent):
            super(Berzerks.Berserk, self).__init__(
                parent, name='Khorne Berzerker', points=Berzerks.model_points,
                gear=Berzerks.model_gear + [Gear('Close combat weapon')]
            )

            self.weapon = self.Weapon(self)
            self.opt = Berzerks.Options(self)

        @ListSubUnit.count_gear
        def count_icon(self):
            return self.opt.iconofwrath.value

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.weapon.cur != self.weapon.boltpistol

    class Champion(Unit):

        class Weapon1(Ranged, Melee, Ccw):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        def __init__(self, parent):
            super(Berzerks.Champion, self).__init__(
                parent, name='Berzerker Champion', points=105 - Berzerks.model_points * 4,
                gear=Berzerks.model_gear
            )
            self.wep1 = self.Weapon1(self, name='Weapon')
            self.wep2 = self.Weapon2(self, name='')
            self.opt = Berzerks.ChampionOptions(self)

        def check_rules(self):
            super(Berzerks.Champion, self).check_rules()
            self.wep1.set_unique_ranged(not self.wep2.has_unique_ranged())
            self.wep2.set_unique_ranged(not self.wep1.has_unique_ranged())

        def count_icon(self):
            return 1 if self.opt.iconofwrath.value else 0


class ThousandSons(IATransportedUnit):
    type_name = "Thousand Sons"
    type_id = "thousand_sons_v1"
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Thousand-Sons')

    model_points = 23
    model_gear = [Gear('Power armour'), Gear('Infernno bolts'), Gear('Aura of Dark Glory')]

    veterans = True

    class Champion(Unit):

        def __init__(self, parent):
            super(ThousandSons.Champion, self).__init__(
                parent, points=150 - ThousandSons.model_points * 4, name='Aspiring Sorcerer',
                gear=ThousandSons.model_gear + [Gear('Bolt pistol'), Gear('Force weapon'), Gear('Mastery Level 1')],
            )
            self.opt = ThousandSons.ChampionOptions(self)

        def count_icon(self):
            return 1 if self.opt.icon.value else 0

    class Count(Count):
        @property
        def description(self):
            model = UnitDescription('Thousand Son', points=ThousandSons.model_points,
                                    options=ThousandSons.model_gear + [Gear('Boltgun')])
            if self.parent.icon.any:
                return [model.clone().set_count(self.cur - 1),
                        model.add(self.parent.icon.description).add_points(self.parent.icon.points)]
            return [model.set_count(self.cur)]

    class Icon(OptionsList):
        def __init__(self, parent):
            super(ThousandSons.Icon, self).__init__(parent=parent, name='Options')
            self.icon = self.variant('Icon of flame', 15)

    class ChampionOptions(Icon):
        def __init__(self, parent):
            super(ThousandSons.ChampionOptions, self).__init__(parent)
            self.meltabombs = self.variant('Melta bombs', 5)
            self.giftofmutation = self.variant('Gift of mutation', 10)

    def __init__(self, parent):
        super(ThousandSons, self).__init__(parent, gear=[Gear('Mark of Tzeentch'), Gear('Veterans of the Long War')])

        self.champion = SubUnit(self, self.Champion(None))
        self.models = self.Count(self, 'Thousand Son', 4, 19, self.model_points)
        self.icon = self.Icon(self)
        self.transport = RhinoTransport(self)

    def count_icon(self):
        return 1 if self.icon.icon.value else 0

    def get_count(self):
        return self.models.cur + 1

    def check_rules(self):
        super(ThousandSons, self).check_rules()
        count = sum(u.count_icon() for u in [self.champion.unit, self])
        if count > 1:
            self.error("{0} can't take more then 1 icon (taken: {1}).".format(self.name, count))

    def build_description(self):
        description = UnitDescription(name=self.name, points=self.points, count=self.count)
        for o in [self.champion, self.models, self.transport]:
            description.add(o.description)
        return description

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(super(ThousandSons, self).build_statistics())


class RubricMarines(IATransportedUnit):
    type_name = "Rubric marines"
    type_id = "rubric_marines_v1"

    model_points = 23
    model_gear = [Gear('Aura of Dark Glory')]

    veterans = True

    class Champion(Unit):

        class Weapon(OneOf):
            def __init__(self, parent):
                super(RubricMarines.Champion.Weapon, self).__init__(parent, 'Ranged weapon')
                self.variant('Inferno bolt pistol')
                self.variant('Warpflame pistol', 5)

        def __init__(self, parent):
            super(RubricMarines.Champion, self).__init__(
                parent, points=150 - RubricMarines.model_points * 4, name='Aspiring Sorcerer',
                gear=RubricMarines.model_gear + [Gear('Force stave')],
            )
            self.wep = self.Weapon(self)
            self.opt = RubricMarines.ChampionOptions(self)

        def count_icon(self):
            return 1 if self.opt.icon.value else 0

    class Icon(OptionsList):
        def __init__(self, parent):
            super(RubricMarines.Icon, self).__init__(parent=parent, name='Options')
            self.icon = self.variant('Icon of flame', 15)

    class ChampionOptions(Icon):
        def __init__(self, parent):
            super(RubricMarines.ChampionOptions, self).__init__(parent)
            self.meltabombs = self.variant('Melta bombs', 5)
            self.giftofmutation = self.variant('Gift of mutation', 10)

    class Rubricae(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(RubricMarines.Rubricae.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Inferno boltgun')
                self.variant('Warpflamer', 7)
                self.can = self.variant('Soulreaper cannon', 25)

        def __init__(self, parent):
            super(RubricMarines.Rubricae, self).__init__(parent, 'Rubric Marine',
                                                         RubricMarines.model_points, RubricMarines.model_gear)
            self.wep = self.Weapon(self)
            self.icon = RubricMarines.Icon(self)

        @ListSubUnit.count_gear
        def has_cannon(self):
            return self.wep.cur == self.wep.can

        @ListSubUnit.count_gear
        def count_icon(self):
            return self.icon.any

    def __init__(self, parent):
        super(RubricMarines, self).__init__(parent, gear=[Gear('Mark of Tzeentch'), Gear('Veterans of the Long War')])

        self.champion = SubUnit(self, self.Champion(None))
        self.models = UnitList(self, self.Rubricae, 4, 19)
        self.transport = RhinoTransport(self)

    def get_count(self):
        return self.models.count + 1

    def check_rules(self):
        super(RubricMarines, self).check_rules()
        icon_count = sum(u.count_icon() for u in self.models.units) + self.champion.unit.count_icon()
        if icon_count > 1:
            self.error("Only one model in the unit can carry icon (taken: {}).".format(icon_count))
        cannon_count = sum(u.has_cannon() for u in self.models.units)
        cannon_limit = (1 + self.models.count) / 10
        if cannon_count > cannon_limit:
            self.error("One Soulreaper cannon may be taken for every 10 models (taken: {})".format(cannon_count))

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(super(RubricMarines, self).build_statistics())


class PlagueMarines(IATransportedUnit):
    type_name = u'Plague Marines'
    type_id = 'plague_marines_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Plague-Marines')

    model_points = 24
    model_gear = [Gear('Power armour'), Gear('Blight grenades'), Gear('Krak grenades')]

    class Options(OptionsList):
        def __init__(self, parent):
            super(PlagueMarines.Options, self).__init__(parent=parent, name='Options')
            self.icon = self.variant('Icon of despair', 10)

    class ChampionOptions(Options):
        def __init__(self, parent):
            super(PlagueMarines.ChampionOptions, self).__init__(parent)
            self.meltabombs = self.variant('Melta bombs', 5)
            self.giftofmutation = self.variant('Gift of mutation', 10)

    class Marine(ListSubUnit):

        class Weapon(OptionsList):
            def __init__(self, parent):
                super(PlagueMarines.Marine.Weapon, self).__init__(parent, name='Weapon', limit=1)
                bp = [Gear('Bolt pistol')]
                bg = [Gear('Boltgun')]

                self.pp = self.variant('Plasma pistol', 15, gear=[Gear('Plasma pistol')] + bg)

                self.fl = self.variant('Flamer', 5, gear=[Gear('Flamer')] + bp)
                self.mg = self.variant('Meltagun', 10, gear=[Gear('Meltagun')] + bp)
                self.pg = self.variant('Plasma gun', 15, gear=[Gear('Plasma gun')] + bp)

            @property
            def description(self):
                if self.any:
                    return super(PlagueMarines.Marine.Weapon, self).description
                return [Gear('Bolt pistol'), Gear('Boltgun')]

        def __init__(self, parent):
            super(PlagueMarines.Marine, self).__init__(
                parent, name='Plague Marine', points=PlagueMarines.model_points,
                gear=PlagueMarines.model_gear + [Gear('Plague knife')]
            )
            self.weapon = self.Weapon(self)
            self.opt = PlagueMarines.Options(self)

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.weapon.any

        @ListSubUnit.count_gear
        def count_icon(self):
            return self.opt.icon.value

    class Champion(Unit):
        class Knife(BaseWeapon):
            def __init__(self, *args, **kwargs):
                super(PlagueMarines.Champion.Knife, self).__init__(*args, **kwargs)
                self.variant('Plague knife', 0)

        class Weapon1(Ranged, Melee, Knife):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        class Weapon3(Ranged, Boltgun):
            pass

        def __init__(self, parent):
            super(PlagueMarines.Champion, self).__init__(
                parent, points=120 - PlagueMarines.model_points * 4, name='Plague Champion',
                gear=PlagueMarines.model_gear
            )

            self.wep1 = self.Weapon1(self, name='Weapon')
            self.wep2 = self.Weapon2(self, name='')
            self.wep3 = self.Weapon3(self, name='')
            self.opt = PlagueMarines.ChampionOptions(self)

        def count_icon(self):
            return self.opt.icon.value

        def check_rules(self):
            super(PlagueMarines.Champion, self).check_rules()
            self.wep1.set_unique_ranged(not self.wep2.has_unique_ranged() and not self.wep3.has_unique_ranged())
            self.wep2.set_unique_ranged(not self.wep1.has_unique_ranged() and not self.wep3.has_unique_ranged())
            self.wep3.set_unique_ranged(not self.wep1.has_unique_ranged() and not self.wep1.has_unique_ranged())

    def __init__(self, parent):
        super(PlagueMarines, self).__init__(parent, gear=[Gear('Mark of Nurgle')])
        self.champion = SubUnit(self, self.Champion(None))
        self.models = UnitList(self, self.Marine, min_limit=4, max_limit=19)

        self.veteran = Veterans(self, points=1, per_model=True, crimson=True)
        self.transport = RhinoTransport(self)

    def check_rules(self):
        super(PlagueMarines, self).check_rules()
        taken = sum((it.count_spec() for it in self.models.units))
        if taken > 2:
            self.error("Plague Marines can't have more then 2 special weapon in unit (taken: {0}).".format(taken))

        icon = sum(u.count_icon() for u in [self.champion.unit] + self.models.units)
        if icon > 1:
            self.error("Plague Marines can't take more then 1 icon (taken: {0}).".format(icon))

    def get_count(self):
        return self.models.count + 1


class NoiseMarines(IATransportedUnit):
    type_name = u'Noise Marines'
    type_id = 'noise_marines_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Noise-Marines')

    model_points = 17
    model_gear = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades')]

    def __init__(self, parent):
        super(NoiseMarines, self).__init__(parent, gear=[Gear('Mark of Slaanesh')])
        self.champion = SubUnit(self, self.Champion(None))
        self.models = UnitList(self, self.Marine, min_limit=4, max_limit=19)

        self.veteran = Veterans(self, points=1, per_model=True, crimson=True)
        self.transport = RhinoTransport(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(NoiseMarines.Options, self).__init__(parent=parent, name='Options')
            self.icon = self.variant('Icon of excess', 30)

    class ChampionOptions(Options):
        def __init__(self, parent):
            super(NoiseMarines.ChampionOptions, self).__init__(parent)
            self.meltabombs = self.variant('Melta bombs', 5)
            self.giftofmutation = self.variant('Gift of mutation', 10)
            self.siren = self.variant('Doom siren', 15)

    class MarineOptions(Options):
        def __init__(self, parent):
            super(NoiseMarines.MarineOptions, self).__init__(parent)
            self.siren = self.variant('Close combat weapon', 1)

    class Champion(Unit):
        class Weapon1(Ranged, Melee, Ccw):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        class Weapon3(Ranged, Boltgun):
            pass

        def __init__(self, parent):
            super(NoiseMarines.Champion, self).__init__(
                parent, points=95 - NoiseMarines.model_points * 4, name='Noise Champion',
                gear=NoiseMarines.model_gear
            )

            self.wep1 = self.Weapon1(self, name='Weapon')
            self.wep2 = self.Weapon2(self, name='')
            self.wep3 = self.Weapon3(self, name='')
            self.opt = NoiseMarines.ChampionOptions(self)

        def count_icon(self):
            return self.opt.icon.value

        def check_rules(self):
            super(NoiseMarines.Champion, self).check_rules()
            self.wep1.set_unique_ranged(not self.wep2.has_unique_ranged() and not self.wep3.has_unique_ranged())
            self.wep2.set_unique_ranged(not self.wep1.has_unique_ranged() and not self.wep3.has_unique_ranged())
            self.wep3.set_unique_ranged(not self.wep1.has_unique_ranged() and not self.wep1.has_unique_ranged())

    class Marine(ListSubUnit):

        class Weapon(OneOf):
            def __init__(self, parent):
                super(NoiseMarines.Marine.Weapon, self).__init__(parent, name='Weapon')

                self.variant('Boltgun', 0)
                self.variant('Close combat weapon', 0)
                self.variant('Sonic blaster', 3)
                self.master = self.variant('Blastmaster', 30)

        def __init__(self, parent):
            super(NoiseMarines.Marine, self).__init__(
                parent, name='Noise Marine', points=NoiseMarines.model_points,
                gear=NoiseMarines.model_gear + [Gear('Bolt pistol')]
            )
            self.weapon = self.Weapon(self)
            self.opt = NoiseMarines.MarineOptions(self)

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.weapon.cur == self.weapon.master

        @ListSubUnit.count_gear
        def count_icon(self):
            return self.opt.icon.value

    def check_rules(self):
        super(NoiseMarines, self).check_rules()
        taken = sum((it.count_spec() for it in self.models.units))
        limit = 1 if self.get_count() < 10 else 2
        if taken > limit:
            self.error("Noise Marines can't have more then {0} blastmaster in unit (taken: {1}).".format(limit, taken))

        icon = sum(u.count_icon() for u in [self.champion.unit] + self.models.units)
        if icon > 1:
            self.error("Noise Marines can't take more then 1 icon (taken: {0}).".format(icon))

    def get_count(self):
        return self.models.count + 1
