# -*- coding: utf-8 -*-

from armory import *
from builder.games.wh40k.roster import Unit

__author__ = 'Denis Romanov'


class Abaddon(Unit):
    type_name = u'Abaddon the Despoiler'
    type_id = 'abaddonthedespoiler_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Abaddon-the-Despoiler')

    veterans = True

    def __init__(self, parent):
        super(Abaddon, self).__init__(parent=parent, points=265, static=True, unique=True,
                                      gear=[Gear('Terminator armour'), Gear('Drach\'nyen'), Gear('Talon of Horus'),
                                            Gear('Veterans of the Long War'), Gear('Mark of Chaos Ascendant')])


class Huron(Unit):
    type_name = u'Huron Blackheart'
    type_id = 'huronblackheart_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Huron-Blackheart')

    veterans = True

    def __init__(self, parent):
        super(Huron, self).__init__(parent=parent, points=160, static=True, unique=True,
                                    gear=[Gear('Power armour'), Gear('Power axe'), Gear('Frag grenades'),
                                          Gear('Krak grenades'), Gear('Sigil of corruption'),
                                          Gear('The Tyrant\'s claw'), Gear('Veterans of the Long War')])

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(super(Huron, self).build_statistics())


class Kharn(Unit):
    type_name = u'Khârn the Betrayer'
    type_id = 'kharnthebetrayer_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Kharn-the-Betrayer')

    veterans = True

    def __init__(self, parent):
        super(Kharn, self).__init__(parent=parent, points=160, static=True, unique=True,
                                    gear=[Gear('Power armour'), Gear('Plasma pistol'), Gear('Frag grenades'),
                                          Gear('Krak grenades'), Gear('Aura of dark glory'), Gear('Gorechild'),
                                          Gear('Mark of Khorne'), Gear('Veterans of the Long War')])


class Ahriman(Unit):
    type_name = u'Ahriman, Arch-Sorcerer of Tzeentch'
    type_id = 'ahriman_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Ahriman')

    veterans = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(Ahriman.Options, self).__init__(parent, "Options")
            self.variant('Disc of Tzeentch', points=30)

    def __init__(self, parent):
        super(Ahriman, self).__init__(parent=parent, name='Ahriman', points=230, unique=True,
                                      gear=[Gear('Inferno bolt pistol'), Gear('Frag grenades'),
                                            Gear('Krak grenades'), Gear('Aura of dark glory'),
                                            Gear('The Black Staff of Ahriman'), Gear('Mark of Tzeentch'),
                                            Gear('Veterans of the Long War')])
        self.Options(self)

    def count_charges(self):
        return 4

    def build_statistics(self):
        return self.note_charges(super(Ahriman, self).build_statistics())


class Typhus(Unit):
    type_name = u'Typhus'
    type_id = 'typhus_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Typhus')

    veterans = True

    def __init__(self, parent):
        super(Typhus, self).__init__(parent=parent, points=230, static=True, unique=True,
                                     gear=[Gear('Terminator armour'), Gear('Blight grenades'), Gear('Manreaper'),
                                           Gear('Mark of Nurgle'), Gear('Veterans of the Long War')])

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(Typhus, self).build_statistics())


class Lucius(Unit):
    type_name = u'Lucius the Eternal'
    type_id = 'luciustheeternal_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Lucius-the-Eternal')

    veterans = True

    def __init__(self, parent):
        super(Lucius, self).__init__(parent=parent, points=165, static=True, unique=True,
                                     gear=[Gear('Power sword'), Gear('Doom syren'), Gear('Frag grenades'),
                                           Gear('Krak grenades'), Gear('Armour of Shrieking Souls'),
                                           Gear('Lash of Torment'), Gear('Mark of Slaanesh'),
                                           Gear('Veterans of the Long War')])


class Fabius(Unit):
    type_name = u'Fabius Bile'
    type_id = 'fabiusbile_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Fabius-Bile')

    def __init__(self, parent):
        super(Fabius, self).__init__(parent=parent, points=165, static=True, unique=True,
                                     gear=[Gear('Power armour'), Gear('Bolt pistol'), Gear('Frag grenades'),
                                           Gear('Krak grenades'), Gear('Xyclos Needler'), Gear('Rod of Torment'),
                                           Gear('The Chirurgeon')])


class Character(Unit):
    def __init__(self, *args, **kwargs):
        super(Character, self).__init__(*args, **kwargs)
        self.weapon1 = None
        self.weapon2 = None
        self.artefacts = None

    def check_rules(self):
        super(Character, self).check_rules()
        self.weapon1.set_unique_ranged(not self.weapon2.has_unique_ranged())
        self.weapon2.set_unique_ranged(not self.weapon1.has_unique_ranged())
        ArtefactWeapon.process_artefacts(self.weapon1, self.weapon2)
        # duuno which one is actually melee, so doing it symmetrically
        self.weapon1.used = self.weapon1.visible = not self.weapon2.cur == self.weapon2.blackclaws
        self.weapon2.used = self.weapon2.visible = not self.weapon1.cur == self.weapon1.blackclaws
        if len(self.get_unique_gear()) > 1:
            self.error('Cannot carry more then 1 artifact')

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique() + self.artefacts.get_unique()


class Lord(Character):
    type_name = u'Chaos Lord'
    type_id = 'chaos_lord_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Chaos-Lord')

    class Weapon1(ArtefactWeapon, TerminatorRanged, TerminatorBolter, Ranged, Melee, BoltPistol):
        pass

    class Weapon2(ArtefactWeapon, TerminatorMelee, TerminatorPower, Ranged, Melee, Ccw):
        pass

    def __init__(self, parent):
        super(Lord, self).__init__(parent, points=65)

        self.marks = Marks(self)
        self.armour = Armour(self, tda=40)
        self.weapon1 = self.Weapon1(self, name='Weapon', armour=self.armour,
                                    marks=self.marks, melee=False)
        self.weapon2 = self.Weapon2(self, name='', armour=self.armour, marks=self.marks)
        self.artefacts = ArtefactGear(self, self.marks, lord=True)
        self.rewards = Rewards(self, armour=self.armour, marks=self.marks)
        self.veterans = Veterans(self)

    def not_nurgle(self):
        return self.marks.any and not self.marks.nurgle

    def is_tda(self):
        return self.armour.is_tda()


class Sorcerer(Character):
    type_name = u'Chaos Sorcerer'
    type_id = 'chaos_sorcerer_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Sorcerer')

    class Psy(OneOf):
        def __init__(self, parent):
            super(Sorcerer.Psy, self).__init__(parent, name='Psyker')
            self.lvl1 = self.variant('Mastery level 1')
            self.lvl2 = self.variant('Mastery level 2', 25)
            self.lvl3 = self.variant('Mastery level 3', 50)

        def count_charges(self):
            if self.cur == self.lvl1:
                return 1
            elif self.cur == self.lvl2:
                return 2
            elif self.cur == self.lvl3:
                return 3

    class Weapon2(ArtefactWeapon, TerminatorMelee, TerminatorForce, Ranged, Melee, ForceWeapon):
        pass

    def __init__(self, parent):
        super(Sorcerer, self).__init__(parent, points=60)

        self.marks = Marks(self, khorne=False)
        self.armour = Armour(self, tda=25)
        self.weapon1 = Lord.Weapon1(self, name='Weapon', armour=self.armour,
                                    marks=self.marks, melee=True)
        self.weapon2 = self.Weapon2(self, name='', armour=self.armour, marks=self.marks)
        self.artefacts = ArtefactGear(self, self.marks, sorc=True, is_psy=lambda: True)
        self.rewards = Rewards(self, armour=self.armour, marks=self.marks)
        self.veterans = Veterans(self)
        self.psy = self.Psy(self)

    def count_charges(self):
        return self.psy.count_charges() + self.artefacts.count_charges()

    def build_statistics(self):
        return self.note_charges(super(Sorcerer, self).build_statistics())

    def not_nurgle(self):
        return self.marks.any and not self.marks.nurgle


class ExaltedSorc(Unit):
    type_name = u'Exalted Sorcerer'
    type_id = 'exalted_sorcerer_v1'

    veterans = True

    class Psy(OneOf):
        def __init__(self, parent):
            super(ExaltedSorc.Psy, self).__init__(parent, name='Psyker')
            self.lvl2 = self.variant('Mastery level 2')
            self.lvl3 = self.variant('Mastery level 3', 25)

        def count_charges(self):
            if self.cur == self.lvl2:
                return 2
            elif self.cur == self.lvl3:
                return 3

    class Weapon1(ArtefactWeapon, ForceStave):
        pass

    class Weapon2(ArtefactWeapon, InfernoBoltPistol):
        pass

    def __init__(self, parent):
        super(ExaltedSorc, self).__init__(parent, points=160, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Aura of the dark glory'),
            Gear('Mark of Tzeentch'), Gear('Veterans of the Long War')
        ])

        class FalseMark(object):
            tzeentch = True
            khorne = False
            nurgle = False
            slaanesh = False
        mark = FalseMark()
        self.weapon1 = self.Weapon1(self, name='Weapon', marks=mark)
        self.weapon2 = self.Weapon2(self, name='', marks=mark, melee=False)
        self.artefacts = ArtefactGear(self, mark, sorc=True, is_psy=lambda: True)
        self.rewards = Rewards(self, marks=mark, spec=False)
        self.psy = self.Psy(self)
        self.rewards.auraofdarkglory.visible = False

    def check_rules(self):
        super(ExaltedSorc, self).check_rules()
        ArtefactWeapon.process_artefacts(self.weapon1, self.weapon2)
        if len(self.get_unique_gear()) > 1:
            self.error('Cannot carry more then 1 artifact')

    def count_charges(self):
        return self.psy.count_charges() + self.artefacts.count_charges()

    def build_statistics(self):
        return self.note_charges(super(ExaltedSorc, self).build_statistics())

    def not_nurgle(self):
        return True

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique() + self.artefacts.get_unique()


class DaemonPrince(Unit):
    type_name = "Daemon Prince"
    type_id = "daemon_prince_v1"
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Daemon-Prince')

    class Options(OptionsList):
        def __init__(self, parent):
            super(DaemonPrince.Options, self).__init__(parent, name='Options')
            self.variant('Wings', 40)
            self.variant('Power armour', 20)

    class Psy(OptionsList):
        def __init__(self, parent):
            super(DaemonPrince.Psy, self).__init__(parent, name='Psyker', limit=1)
            self.lvl1 = self.variant('Mastery level 1', 25)
            self.lvl2 = self.variant('Mastery level 2', 50)
            self.lvl3 = self.variant('Mastery level 3', 75)

        def count_charges(self):
            if self.lvl1.value:
                return 1
            elif self.lvl2.value:
                return 2
            elif self.lvl3.value:
                return 3

    class Weapon(ArtefactWeapon, Ccw):
        pass

    class Marks(OneOf):
        def __init__(self, parent):
            super(DaemonPrince.Marks, self).__init__(parent, name='Mark of Chaos')

            self._khorne = self.variant('Daemon of Khorne', 15)
            self._tzeentch = self.variant('Daemon of Tzeentch', 15)
            self._nurgle = self.variant('Daemon of Nurgle', 15)
            self._slaanesh = self.variant('Daemon of Slaanesh', 10)

        @property
        def khorne(self):
            return self.cur == self._khorne

        @property
        def tzeentch(self):
            return self.cur == self._tzeentch

        @property
        def nurgle(self):
            return self.cur == self._nurgle

        @property
        def slaanesh(self):
            return self.cur == self._slaanesh

    def __init__(self, parent):
        super(DaemonPrince, self).__init__(parent, points=145, gear=[Gear('Veterans of the Long War')])
        self.marks = self.Marks(self)
        self.weapon = self.Weapon(self, name='Weapon', marks=self.marks)
        self.artefacts = ArtefactGear(self, marks=self.marks, prince=True, is_psy=lambda: self.psy.any)
        self.rewards = Rewards(self, marks=self.marks, spec=False, ride=False)
        self.psy = self.Psy(self)
        self.opt = self.Options(self)

    def check_rules(self):
        super(DaemonPrince, self).check_rules()
        self.psy.visible = self.psy.used = not self.marks.khorne
        if len(self.get_unique_gear()) > 1:
            self.error('Cannot carry more then 1 artifact')
        if self.roster.is_thousand:
            if not self.marks.tzeentch:
                self.error('Daemon Prince in Thousand Sons detachment must be Daemon of Tzeentch')
        if self.roster.is_eater:
            if not self.marks.khorne:
                self.error('Daemon Prince in World Eaters detachment must be Daemon of Khorne')
        if self.roster.is_guard:
            if not self.marks.nurgle:
                self.error('Daemon Prince in Death Guard detachment must be Daemon of Nurgle')
        if self.roster.is_child:
            if not self.marks.slaanesh:
                self.error("Daemon Prince in Emperor's Children detachment must be Daemon of Slaanesh")

    def get_unique_gear(self):
        return self.weapon.get_unique() + self.artefacts.get_unique()

    def count_charges(self):
        return self.psy.count_charges()

    def build_statistics(self):
        return self.note_charges(super(DaemonPrince, self).build_statistics())

    def not_nurgle(self):
        return not self.marks.nurgle


class Warpsmith(Character):
    type_name = "Warpsmith"
    type_id = "warpsmith_v1"
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Warpsmith')

    class Weapon1(ArtefactWeapon, Ranged, BoltPistol):
        pass

    class Weapon2(ArtefactWeapon, Ranged, PowerAxe):
        pass

    def __init__(self, parent):
        super(Warpsmith, self).__init__(parent, points=110, gear=[Gear('Fleshmetal'), Gear('Frag grenades'),
                                                                  Gear('Krak grenades'), Gear('Mechatendrils')])

        self.marks = Marks(self)
        self.weapon1 = self.Weapon1(self, name='Weapon', marks=self.marks, melee=False)
        self.weapon2 = self.Weapon2(self, name='', marks=self.marks)
        self.artefacts = ArtefactGear(self, self.marks, smith=True)
        self.rewards = Rewards(self, marks=self.marks, ride=False)
        self.veterans = Veterans(self)

    def not_nurgle(self):
        return self.marks.any and not self.marks.nurgle


class Apostle(Character):
    type_name = "Dark Apostle"
    type_id = "dark_apostle_v1"
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Dark-Apostle')

    class Weapon1(ArtefactWeapon, Ranged, BoltPistol):
        pass

    class Weapon2(ArtefactWeapon, Ranged, PowerMaul):
        pass

    def __init__(self, parent):
        super(Apostle, self).__init__(parent, points=105, gear=Armour.power_armour_set + [Gear('Sigil of Corruption')])

        self.marks = Marks(self)
        self.weapon1 = self.Weapon1(self, name='Weapon', marks=self.marks, melee=False)
        self.weapon2 = self.Weapon2(self, name='', marks=self.marks, crozius=True)
        self.artefacts = ArtefactGear(self, self.marks)
        self.rewards = Rewards(self, marks=self.marks, ride=False, apostle=True)
        self.veterans = Veterans(self, points=0)

    def not_nurgle(self):
        return self.marks.any and not self.marks.nurgle
