# -*- coding: utf-8 -*-

__author__ = 'Ivan Truskov'

from builder.games.wh40k.section import Detachment
from builder.games.wh40k.imperial_armour.volume13 import ChaosSpaceMarinesLegaciesFormation
from builder.games.wh40k.escalation.chaos import LordOfSkulls
from hq import *
from elites import *
from troops import *
from fast import *
from heavy import *
from lords import Magnus


class SupplementOptions(OneOf):
    def __init__(self, parent):
        super(SupplementOptions, self).__init__(parent=parent, name='Legion/warband')
        self.base = self.variant(name='Chaos Space Marines')
        self.black = self.variant(name='Black Legion')
        self.crimson = self.variant(name='Crimson Slaughter')
        self.thousand = self.variant(name='Thousand Sons')
        self.alpha = self.variant(name='Alpha Legion')
        self.iron = self.variant(name='Iron Warriors')
        self.night = self.variant(name='Night Lords')
        self.word = self.variant(name='Word Bearers')
        self.eater = self.variant(name='World Eaters')
        self.guard = self.variant(name='Death Guard')
        self.child = self.variant(name="Emperor's Children")


class Mayhem(ChaosSpaceMarinesLegaciesFormation):
    army_id = 'csm_v2_mayhem'
    army_name = 'Mayhem pack'

    def __init__(self):
        super(Mayhem, self).__init__()
        UnitType(self, Helbrute, min_limit=3, max_limit=3)


class Helcult(ChaosSpaceMarinesLegaciesFormation):
    army_id = 'csm_v2_helcult'
    army_name = 'Helcult'

    @property
    def is_base(self):
        return True

    @property
    def is_black(self):
        return False

    @property
    def is_crimson(self):
        return False

    @property
    def is_thousand(self):
        return False

    @property
    def is_alpha(self):
        return False

    @property
    def is_iron(self):
        return False

    @property
    def is_night(self):
        return False

    @property
    def is_word(self):
        return False

    @property
    def is_eater(self):
        return False

    @property
    def is_guard(self):
        return False

    @property
    def is_child(self):
        return False

    def __init__(self):
        super(Helcult, self).__init__()
        UnitType(self, Helbrute, min_limit=1, max_limit=1)
        UnitType(self, ChaosCultists, min_limit=2, max_limit=2)


class Helfist(ChaosSpaceMarinesLegaciesFormation):
    army_id = 'csm_v2_helfist'
    army_name = 'Helfist Murderpack'

    def __init__(self):
        super(Helfist, self).__init__()
        UnitType(self, Helbrute, min_limit=5, max_limit=5)


class Butcherhorde(ChaosSpaceMarinesLegaciesFormation):
    army_id = 'csm_v2_butcherhorde'
    army_name = "Khârn's Butcherhorde"

    @property
    def is_base(self):
        return True

    @property
    def is_black(self):
        return False

    @property
    def is_crimson(self):
        return False

    @property
    def is_thousand(self):
        return False

    @property
    def is_alpha(self):
        return False

    @property
    def is_iron(self):
        return False

    @property
    def is_night(self):
        return False

    @property
    def is_word(self):
        return False

    @property
    def is_eater(self):
        return False

    @property
    def is_guard(self):
        return False

    @property
    def is_child(self):
        return False

    class KhorneMarines(ChaosSpaceMarines):
        def __init__(self, *args, **kwargs):
            super(Butcherhorde.KhorneMarines, self).__init__(*args, **kwargs)
            self.marks.set_mark_only(self.marks._khorne, 2)

    def __init__(self):
        super(Butcherhorde, self).__init__()
        UnitType(self, Kharn, min_limit=1, max_limit=1)
        self.csm = UnitType(self, self.KhorneMarines, min_limit=4, max_limit=4)
        UnitType(self, Berzerks, min_limit=4, max_limit=4)

    def check_rules(self):
        super(Butcherhorde, self).check_rules()
        for unit in self.csm.units:
            if not unit.marks.khorne:
                self.error("All units of Chaos Space Marines must take the mark of Khorne upgrade")
                break


class GuardFormation(ChaosSpaceMarinesLegaciesFormation):
    @property
    def is_base(self):
        return False

    @property
    def is_black(self):
        return False

    @property
    def is_crimson(self):
        return False

    @property
    def is_thousand(self):
        return False

    @property
    def is_alpha(self):
        return False

    @property
    def is_iron(self):
        return False

    @property
    def is_night(self):
        return False

    @property
    def is_word(self):
        return False

    @property
    def is_eater(self):
        return False

    @property
    def is_guard(self):
        return True

    @property
    def is_child(self):
        return False


class Colony(GuardFormation):
    army_name = 'Plague Colony'
    army_id = 'csm_v2_5_colony'

    def __init__(self):
        super(Colony, self).__init__()
        lord = [UnitType(self, Lord),
                UnitType(self, Typhus)]
        self.add_type_restriction(lord, 1, 1)
        UnitType(self, PlagueMarines, min_limit=3, max_limit=7)


class ChildFormation(ChaosSpaceMarinesLegaciesFormation):
    @property
    def is_base(self):
        return False

    @property
    def is_black(self):
        return False

    @property
    def is_crimson(self):
        return False

    @property
    def is_thousand(self):
        return False

    @property
    def is_alpha(self):
        return False

    @property
    def is_iron(self):
        return False

    @property
    def is_night(self):
        return False

    @property
    def is_word(self):
        return False

    @property
    def is_eater(self):
        return False

    @property
    def is_guard(self):
        return False

    @property
    def is_child(self):
        return True


class Kakophoni(ChildFormation):
    army_name = 'Kakophoni'
    army_id = 'csm_v2_5_kakophoni'

    def __init__(self):
        super(Kakophoni, self).__init__()
        lord = [UnitType(self, Lord),
                UnitType(self, Lucius)]
        self.add_type_restriction(lord, 1, 1)
        UnitType(self, NoiseMarines, min_limit=3, max_limit=6)


class BlackFormation(ChaosSpaceMarinesLegaciesFormation):
    @property
    def is_base(self):
        return False

    @property
    def is_black(self):
        return True

    @property
    def is_crimson(self):
        return False

    @property
    def is_thousand(self):
        return False

    @property
    def is_alpha(self):
        return False

    @property
    def is_iron(self):
        return False

    @property
    def is_night(self):
        return False

    @property
    def is_word(self):
        return False

    @property
    def is_eater(self):
        return False

    @property
    def is_guard(self):
        return False

    @property
    def is_child(self):
        return False


class Bringers(BlackFormation):
    army_name = 'The Bringers of Despair'
    army_id = 'csm_v2_5_despair'

    def __init__(self):
        super(Bringers, self).__init__()
        UnitType(self, Abaddon, min_limit=1, max_limit=1)
        UnitType(self, Terminators, min_limit=1, max_limit=1)


class BlackChosen(BlackFormation):
    army_name = 'The Chosen of Abaddon'
    army_id = 'csm_v2_5_chosen'

    def __init__(self):
        super(BlackChosen, self).__init__()
        self.lords = [
            UnitType(self, Lord),
            UnitType(self, Sorcerer)
        ]
        self.toadies = [
            UnitType(self, Terminators),
            UnitType(self, Chosen)
        ]
        self.add_type_restriction(self.lords, 1, 4)

    def check_rules(self):
        super(BlackChosen, self).check_rules()
        lcount = sum(ut.count for ut in self.lords)
        tcount = sum(ut.count for ut in self.toadies)
        if not (lcount == tcount):
            self.error("Number of Terminators/Chosen squads must be equal to number of Chaos Lords/Sorcerers")


class BlackHounds(BlackFormation):
    army_name = 'The Hounds of Abaddon'
    army_id = 'csm_v2_5_hounds'

    class KhorneDevotee(Unit):
        def __init__(self, *args, **kwargs):
            super(BlackHounds.KhorneDevotee, self).__init__(*args, **kwargs)
            self.marks.set_mark_only(self.marks._khorne)

    class KhorneLord(KhorneDevotee, Lord):
        pass

    class KhorneMarines(KhorneDevotee, ChaosSpaceMarines):
        pass

    class KhorneRaptors(KhorneDevotee, Raptors):
        pass

    class KhorneTalons(KhorneDevotee, WarpTalons):
        pass

    class KhorneBikers(KhorneDevotee, Bikers):
        pass

    def __init__(self):
        super(BlackHounds, self).__init__()
        UnitType(self, self.KhorneLord, min_limit=1, max_limit=1)
        UnitType(self, Berzerks, min_limit=1, max_limit=3)
        UnitType(self, self.KhorneMarines, min_limit=1, max_limit=3)
        rest = [
            UnitType(self, self.KhorneRaptors),
            UnitType(self, self.KhorneTalons),
            UnitType(self, self.KhorneBikers)
        ]
        self.add_type_restriction(rest, 1, 3)


class EnginePack(BlackFormation):
    army_name = 'Daemon Engine Pack'
    army_id = 'csm_v2_5_engines'

    def __init__(self):
        super(EnginePack, self).__init__()
        UnitType(self, Warpsmith, min_limit=1, max_limit=1)
        engines = [UnitType(self, Forgefiend), UnitType(self, Maulerfiend)]
        self.add_type_restriction(engines, 2, 2)


class Cabal(BlackFormation):
    army_name = 'Cyclopia Cabal'
    army_id = 'csm_v2_5_cabal'

    def __init__(self):
        super(Cabal, self).__init__()
        UnitType(self, Sorcerer, min_limit=3, max_limit=5)


class Tormented(BlackFormation):
    army_name = 'The Tormented'
    army_id = 'csm_v2_5_tormented'

    def __init__(self):
        super(Tormented, self).__init__()
        UnitType(self, DaemonPrince, min_limit=1, max_limit=1)
        UnitType(self, Possessed, min_limit=2, max_limit=5)


class Warband(BlackFormation):
    army_name = 'Black Legion Warband'
    army_id = 'csm_v2_5_warband'

    def __init__(self):
        super(Warband, self).__init__()
        UnitType(self, Lord, min_limit=1, max_limit=1)
        UnitType(self, Sorcerer, min_limit=0, max_limit=1)
        troops = [UnitType(self, ChaosSpaceMarines),
                  UnitType(self, Chosen)]
        self.add_type_restriction(troops, 2, 6)
        elites = [UnitType(self, Terminators),
                  UnitType(self, Possessed)]
        self.add_type_restriction(elites, 1, 3)
        fast = [UnitType(self, Raptors),
                UnitType(self, WarpTalons),
                UnitType(self, Bikers)]
        self.add_type_restriction(fast, 1, 3)
        heavy = [UnitType(self, Havoks),
                 UnitType(self, Helbrute)]
        self.add_type_restriction(heavy, 1, 3)


class CrimsonFormation(ChaosSpaceMarinesLegaciesFormation):
    @property
    def is_base(self):
        return False

    @property
    def is_black(self):
        return False

    @property
    def is_crimson(self):
        return True

    @property
    def is_thousand(self):
        return False

    @property
    def is_alpha(self):
        return False

    @property
    def is_iron(self):
        return False

    @property
    def is_night(self):
        return False

    @property
    def is_word(self):
        return False

    @property
    def is_eater(self):
        return False

    @property
    def is_guard(self):
        return False

    @property
    def is_child(self):
        return False


class Ravagers(CrimsonFormation):
    army_name = 'The Ravagers'
    army_id = 'csm_v2_5_ravagers'

    def __init__(self):
        super(Ravagers, self).__init__()
        UnitType(self, Chosen, min_limit=1, max_limit=1)
        UnitType(self, Possessed, min_limit=1, max_limit=1)


class MannonDisciples(CrimsonFormation):
    army_name = 'Disciples of Mannon'
    army_id = 'csm_v2_5_mannon'

    def __init__(self):
        super(MannonDisciples, self).__init__()
        UnitType(self, Sorcerer, min_limit=1, max_limit=1)
        UnitType(self, Possessed, min_limit=1, max_limit=1)


class DarkCovenant(CrimsonFormation):
    army_name = 'Brethren of the Dark Covenant'
    army_id = 'csm_v2_5_covenant'

    def __init__(self):
        super(DarkCovenant, self).__init__()
        UnitType(self, Apostle, min_limit=1, max_limit=1)
        UnitType(self, Possessed, min_limit=1, max_limit=1)
        UnitType(self, ChaosSpaceMarines, min_limit=1, max_limit=3)


class SlaughterCult(CrimsonFormation):
    army_name = 'Cult of Slaughter'
    army_id = 'csm_v2_5_slaughtercult'

    def __init__(self):
        super(SlaughterCult, self).__init__()
        UnitType(self, Apostle, min_limit=1, max_limit=1)
        UnitType(self, ChaosCultists, min_limit=2, max_limit=8)


class SlaughterLord(CrimsonFormation):
    army_name = 'Lords of Slaughter'
    army_id = 'csm_v2_5_slaughterlords'

    def __init__(self):
        super(SlaughterLord, self).__init__()
        UnitType(self, Lord, min_limit=1, max_limit=1)
        UnitType(self, Possessed, min_limit=1, max_limit=1)
        UnitType(self, Terminators, min_limit=1, max_limit=1)
        UnitType(self, ChaosSpaceMarines, min_limit=1, max_limit=3)

    def build_statistics(self):
        stat = super(SlaughterLord, self).build_statistics()
        stat['Warp charges'] = 1
        return stat


class Helguard(CrimsonFormation):
    army_name = 'Kranon\'s Helguard'
    army_id = 'csm_v2_5_helguard'

    def __init__(self):
        super(Helguard, self).__init__()
        UnitType(self, Lord, min_limit=1, max_limit=1)
        UnitType(self, Chosen, min_limit=1, max_limit=1)
        UnitType(self, Terminators, min_limit=1, max_limit=1)
        UnitType(self, ChaosCultists, min_limit=2, max_limit=2)
        UnitType(self, Raptors, min_limit=1, max_limit=1)
        UnitType(self, LandRaider, min_limit=1, max_limit=1)
        UnitType(self, Helbrute, min_limit=1, max_limit=1)


class Onslaught(CrimsonFormation):
    army_name = 'The Red Onslaught'
    army_id = 'csm_v2_5_onslaught'

    def __init__(self):
        super(Onslaught, self).__init__()
        Detachment.build_detach(self, Ravagers, min_limit=1, max_limit=1)
        Detachment.build_detach(self, MannonDisciples, min_limit=1, max_limit=1)
        Detachment.build_detach(self, DarkCovenant, min_limit=1, max_limit=1)
        Detachment.build_detach(self, SlaughterCult, min_limit=1, max_limit=1)
        Detachment.build_detach(self, SlaughterLord, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Helguard, min_limit=1, max_limit=1)


class CommonChaosFormation(ChaosSpaceMarinesLegaciesFormation):

    thousand_sons_banned = [
        Abaddon, Huron, Kharn, Typhus, Lucius, Fabius,
        NoiseMarines, PlagueMarines, Berzerks
    ]
    black_legion_banned = [
        Ahriman, Huron, Kharn, Typhus, Lucius, Fabius
    ]
    alpha_banned = thousand_sons_banned + [Ahriman, RubricMarines, SOTerminators]
    eater_banned = [
        Abaddon, Huron, Ahriman, Typhus, Lucius, Fabius,
        NoiseMarines, PlagueMarines, RubricMarines, SOTerminators,
        Sorcerer
    ]
    guard_banned = [
        Abaddon, Huron, Kharn, Ahriman, Lucius, Fabius,
        NoiseMarines, RubricMarines, SOTerminators, Berzerks
    ]
    child_banned = [
        Abaddon, Huron, Kharn, Ahriman, Typhus, Fabius,
        PlagueMarines, RubricMarines, SOTerminators, Berzerks
    ]

    @property
    def is_base(self):
        return self.codex.cur == self.codex.base

    @property
    def is_black(self):
        return self.codex.cur == self.codex.black

    @property
    def is_crimson(self):
        return self.codex.cur == self.codex.crimson

    @property
    def is_thousand(self):
        return self.codex.cur == self.codex.thousand

    @property
    def is_alpha(self):
        return self.codex.cur == self.codex.alpha

    @property
    def is_iron(self):
        return self.codex.cur == self.codex.iron

    @property
    def is_night(self):
        return self.codex.cur == self.codex.night

    @property
    def is_word(self):
        return self.codex.cur == self.codex.word

    @property
    def is_eater(self):
        return self.codex.cur == self.codex.eater

    @property
    def is_guard(self):
        return self.codex.cur == self.codex.guard

    @property
    def is_child(self):
        return self.codex.cur == self.codex.child

    def __init__(self):
        super(CommonChaosFormation, self).__init__()
        self.codex = SupplementOptions(self)

    def check_banned_types(self, type_list, det_name):
        for ut in self.types:
            if ut.unit_class in type_list:
                ut.active = False
                if ut.count > 0:
                    self.error(u"{} detachment may not include {}".format(det_name, ut.unit_class.type_name))

    def check_rules(self):
        super(CommonChaosFormation, self).check_rules()
        # todo: change to accomodate that cult units are allowed
        if self.is_crimson:
            for ut in self.types:
                if hasattr(ut.unit_class, 'veterans') and ut.unit_class.veterans:
                    ut.active = False
                    if ut.count > 0:
                        self.error(u"Crimson Slaughter detachment may not include Veterans of the Long War")
        if self.is_thousand:
            self.check_banned_types(self.thousand_sons_banned, u'Thousand Sons')
        if self.is_black:
            self.check_banned_types(self.black_legion_banned, u'Black Legion')
        if self.is_alpha:
            self.check_banned_types(self.alpha_banned, u'Alpha Legion')
        if self.is_iron:
            self.check_banned_types(self.alpha_banned, u'Iron Warriors')
        if self.is_night:
            self.check_banned_types(self.alpha_banned, u'Night Lords')
        if self.is_word:
            self.check_banned_types(self.alpha_banned, u'Word Bearers')
        if self.is_eater:
            self.check_banned_types(self.eater_banned, u'World Eaters')
        if self.is_guard:
            self.check_banned_types(self.guard_banned, u'Death Guard')
        if self.is_child:
            self.check_banned_types(self.child_banned, u"Emperor's Children")


class Wrathborn(CommonChaosFormation):
    army_id = 'csm_v2_Wrathborn'
    army_name = 'Wrathborn'

    def __init__(self):
        super(Wrathborn, self).__init__()
        self.lord = UnitType(self, Lord, min_limit=1, max_limit=1)
        UnitType(self, ChaosSpaceMarines, min_limit=1, max_limit=1)
        UnitType(self, Helbrute, min_limit=1, max_limit=1)
        self.codex = SupplementOptions(self)

    def check_rules(self):
        super(Wrathborn, self).check_rules()

        for unit in self.lord.units:
            if not unit.armour.is_tda():
                self.error('The Chaos Lord must be quipped with Terminator Armour')


class ChaosWarband(CommonChaosFormation):
    army_id = 'csm_v2_5_chaos_warband'
    army_name = 'Chaos Warband'

    def __init__(self):
        super(ChaosWarband, self).__init__()
        self.lords = [
            UnitType(self, Lord),
            UnitType(self, Abaddon),
            UnitType(self, Huron),
            UnitType(self, Kharn),
            UnitType(self, Typhus),
            UnitType(self, Lucius),
            UnitType(self, Fabius)
        ]
        self.add_type_restriction(self.lords, 1, 1)
        sorc = [
            UnitType(self, Sorcerer),
            UnitType(self, Ahriman)
        ]
        self.add_type_restriction(sorc, 0, 1)
        elites = [
            UnitType(self, Chosen),
            UnitType(self, Terminators),
            UnitType(self, Possessed)
        ]
        self.add_type_restriction(elites, 1, 3)
        UnitType(self, ChaosSpaceMarines, min_limit=2, max_limit=6)
        fast = [
            UnitType(self, Raptors),
            UnitType(self, WarpTalons),
            UnitType(self, Bikers)
        ]
        self.add_type_restriction(fast, 1, 3)
        support = [
            UnitType(self, Havoks),
            UnitType(self, Helbrute)
        ]
        self.add_type_restriction(support, 1, 3)


class Maelstorm(CommonChaosFormation):
    army_id = 'csm_v2_5_maelstorm'
    army_name = 'Maelstorm of Gore'

    class KhorneLord(Lord):
        def __init__(self, parent):
            super(Maelstorm.KhorneLord, self).__init__(parent)
            self.marks.set_mark_only(self.marks._khorne)

    def __init__(self):
        super(Maelstorm, self).__init__()
        lords = [
            UnitType(self, Maelstorm.KhorneLord),
            UnitType(self, Kharn)
        ]
        self.add_type_restriction(lords, 1, 1)
        UnitType(self, Berzerks, min_limit=4, max_limit=8)
        self.codex.thousand.visible = False
        self.codex.alpha.visible = False
        self.codex.iron.visible = False
        self.codex.night.visible = False
        self.codex.word.visible = False


class LostDamned(CommonChaosFormation):
    army_id = 'csm_v2_5_lost_damned'
    army_name = 'The Lost and the Damned'

    def __init__(self):
        super(LostDamned, self).__init__()
        UnitType(self, Apostle, min_limit=1, max_limit=1)
        UnitType(self, ChaosCultists, min_limit=4, max_limit=9)


class HellforgedWarpack(CommonChaosFormation):
    army_id = 'csm_v2_5_warpack'
    army_name = 'Hellforged Warpack'

    def __init__(self):
        super(HellforgedWarpack, self).__init__()
        UnitType(self, Warpsmith, min_limit=1, max_limit=1)
        pack = [
            UnitType(self, Helbrute),
            UnitType(self, Maulerfiend),
            UnitType(self, Forgefiend),
            UnitType(self, Defiler)
        ]
        self.add_type_restriction(pack, 3, 5)


class TerrorPack(CommonChaosFormation):
    army_id = 'csm_v2_5_terror'
    army_name = 'Heldrake Terror Pack'

    class PackHeldrake(Heldrakes):
        unit_min = 2

    def __init__(self):
        super(TerrorPack, self).__init__()
        UnitType(self, self.PackHeldrake, min_limit=1, max_limit=1)


class DestructionCult(CommonChaosFormation):
    army_id = 'csm_v2_5_destruction'
    army_name = 'Cult of Destruction'

    def __init__(self):
        super(DestructionCult, self).__init__()
        UnitType(self, Warpsmith, min_limit=1, max_limit=3)
        uglies = [
            UnitType(self, Obliterators),
            UnitType(self, Mutilators)
        ]
        self.add_type_restriction(uglies, 3, 5)


class FistOfGods(CommonChaosFormation):
    army_id = 'csm_v2_5_fist'
    army_name = 'Fist of Gods'

    def __init__(self):
        super(FistOfGods, self).__init__()
        UnitType(self, Warpsmith, min_limit=1, max_limit=3)
        vehicles = [
            UnitType(self, ChaosPredators),
            UnitType(self, ChaosVindicators),
            UnitType(self, LandRaider)
        ]
        self.add_type_restriction(vehicles, 3, 5)


class RaptorTalon(CommonChaosFormation):
    army_id = 'csm_v2_5_raptortalon'
    army_name = 'Raptor Talon'

    class PackLord(Lord):
        def __init__(self, parent):
            super(RaptorTalon.PackLord, self).__init__(parent)
            self.armour.visible = False
            self.rewards.pick_free_ride(self.rewards.jumppack)

    def __init__(self):
        super(RaptorTalon, self).__init__()
        UnitType(self, self.PackLord, min_limit=1, max_limit=1)
        jumpers = [
            UnitType(self, Raptors),
            UnitType(self, WarpTalons)
        ]
        self.add_type_restriction(jumpers, 3, 5)


class TerminatorAnnihilationForce(CommonChaosFormation):
    army_id = 'csm_v2_5_annoholation'
    army_name = 'Terminator Annihilation Force'

    class TermoLord(Lord):
        def __init__(self, parent):
            super(TerminatorAnnihilationForce.TermoLord, self).__init__(parent)
            self.armour.take_free_tda()

    class TermoSorc(Sorcerer):
        def __init__(self, parent):
            super(TerminatorAnnihilationForce.TermoSorc, self).__init__(parent)
            self.armour.take_free_tda()

    def __init__(self):
        super(TerminatorAnnihilationForce, self).__init__()
        self.leader = [
            UnitType(self, self.TermoLord),
            UnitType(self, self.TermoSorc),
            UnitType(self, Abaddon),
            UnitType(self, Typhus)
        ]
        self.add_type_restriction(self.leader, 1, 1)
        UnitType(self, Terminators, min_limit=3, max_limit=5)


class Favoured(CommonChaosFormation):
    army_name = 'Favoured of Chaos'
    army_id = 'csm_v2_5_favoured'

    def __init__(self):
        super(Favoured, self).__init__()
        UnitType(self, DaemonPrince, min_limit=1, max_limit=1)
        UnitType(self, Possessed, min_limit=3, max_limit=5)


class Trinity(Formation):
    army_name = 'Trinity of Blood'
    army_id = 'csm_v2_5_trinity'

    def __init__(self):
        super(Trinity, self).__init__()
        UnitType(self, LordOfSkulls, min_limit=3, max_limit=3)


class ThousandFormation(ChaosSpaceMarinesLegaciesFormation):
    @property
    def is_base(self):
        return False

    @property
    def is_black(self):
        return False

    @property
    def is_crimson(self):
        return False

    @property
    def is_thousand(self):
        return True

    @property
    def is_alpha(self):
        return False

    @property
    def is_iron(self):
        return False

    @property
    def is_night(self):
        return False

    @property
    def is_word(self):
        return False

    @property
    def is_eater(self):
        return False

    @property
    def is_guard(self):
        return False

    @property
    def is_child(self):
        return False


class TzeentchPrince(DaemonPrince):
    def __init__(self, parent):
        super(TzeentchPrince, self).__init__(parent)
        self.marks.cur = self.marks._tzeentch
        self.marks.visible = False


class WarCabal(ThousandFormation):
    army_name = 'War Cabal'
    army_id = 'csm_v2_5_warcabal'

    def __init__(self):
        super(WarCabal, self).__init__()
        self.lords = [UnitType(self, Ahriman),
                      UnitType(self, TzeentchPrince)]
        self.sorc = [UnitType(self, ExaltedSorc),
                     UnitType(self, Sorcerer)]
        self.add_type_restriction(self.lords, 0, 1)
        self.add_type_restriction(self.sorc, 1, 4)
        UnitType(self, RubricMarines, min_limit=1, max_limit=3)
        UnitType(self, SOTerminators, min_limit=1, max_limit=3)

    def check_rules(self):
        super(WarCabal, self).check_rules()
        sum0 = sum(ut.count for ut in self.lords)
        sum1 = sum(ut.count for ut in self.sorc)
        if sum0 > 0:
            if sum1 > 3:
                self.error("Cannot take more then 3 Sorcerers or Exalted Sorcerers in this War Cabal")
        else:
            if sum1 < 2:
                self.error("War Cabal must include at least one more Ahriman, Daemon Prince, Sorcerer or Exalted Sorcerer")


class WarCoven(ThousandFormation):
    army_name = 'War Coven'
    army_id = 'csm_v2_5_warcoven'

    def __init__(self):
        super(WarCoven, self).__init__()
        self.prince = UnitType(self, TzeentchPrince,
                               min_limit=0, max_limit=1)
        self.sorc = [UnitType(self, ExaltedSorc),
                     UnitType(self, Sorcerer)]
        self.add_type_restriction(self.sorc, 3, 10)

    def check_rules(self):
        super(WarCoven, self).check_rules()
        sum1 = sum(ut.count for ut in self.sorc)
        if self.prince.count > 0:
            if sum1 > 9:
                self.error("Cannot take more then 9 Sorcerers or Exalted Sorcerers in this War Coven")
        else:
            if sum1 < 4:
                self.error("War Coven must include at least 1 more Daemon Prince, Sorcerer or Exalted Sorcerer".format(4 - sum1))


class Warherd(ThousandFormation):
    army_name = 'Tzaangor Warherd'
    army_id = 'csm_v2_5_warherd'

    def __init__(self):
        super(Warherd, self).__init__()
        sorc = [UnitType(self, Sorcerer),
                UnitType(self, ExaltedSorc)]
        self.add_type_restriction(sorc, 1, 1)
        herd = [UnitType(self, Tzaangors, min_limit=3),
                UnitType(self, Spawn)]
        self.add_type_restriction(herd, 3, 9)


class SekhmetConclave(ThousandFormation):
    army_name = 'Sekhmet Conclave'
    army_id = 'csm_v2_5_sekhmet'

    def __init__(self):
        super(SekhmetConclave, self).__init__()
        lords = [UnitType(self, Magnus),
                 UnitType(self, Ahriman),
                 UnitType(self, TzeentchPrince),
                 UnitType(self, ExaltedSorc),
                 UnitType(self, Sorcerer)]
        self.add_type_restriction(lords, 1, 1)
        UnitType(self, SOTerminators, min_limit=3, max_limit=9)


class Exiles(ThousandFormation):
    army_name = "Ahriman's Exiles"
    army_id = 'csm_v2_5_exiles'

    def __init__(self):
        super(Exiles, self).__init__()
        UnitType(self, Ahriman, min_limit=1, max_limit=1)
        UnitType(self, ExaltedSorc, min_limit=3, max_limit=9)


class Rehati(ThousandFormation):
    army_name = 'Rehati War Sect'
    army_id = 'csm_v2_5_rehati'

    def __init__(self):
        super(Rehati, self).__init__()
        UnitType(self, Magnus, min_limit=1, max_limit=1)
        self.casters = [UnitType(self, ExaltedSorc),
                        UnitType(self, TzeentchPrince)]
        self.add_type_restriction(self.casters, 3, 9)

    def check_rules(self):
        super(Rehati, self).check_rules()
        for ut in self.casters:
            for u in ut.units:
                if u.count_charges() < 3:
                    self.error('All Daemon Princes and Exalted Sorcerers in this formation must have Mastery Level 3')
                    return
