__author__ = 'dante'

from builder.core2 import OptionsList, OneOf, SubUnit, Gear
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle
from builder.games.wh40k.imperial_armour.volume2.transport import InfernumRazorback, LuciusDropPod, IATransport
from armory import VehicleEquipment
from builder.games.wh40k.roster import Unit


class Rhino(SpaceMarinesBaseVehicle):
    type_name = u'Rhino'

    type_id = 'rhino_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Rhino')

    def __init__(self, parent, off=False):
        super(Rhino, self).__init__(parent=parent, points=45 if not off else 0, tank=True, transport=True,
                                    gear=[Gear('Storm Bolter'), Gear('Smoke Launchers'), Gear('Overcharged engines')])
        VehicleEquipment(self)


class Razorback(SpaceMarinesBaseVehicle):
    type_name = u'Razorback'
    type_id = 'razorback_v1'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Razorback')

    def __init__(self, parent):
        super(Razorback, self).__init__(parent=parent, points=65,
                                        gear=[Gear('Smoke Launchers'), Gear('Searchlight'), Gear('Overcharged engines')],
                                        tank=True, transport=True)
        self.Weapon(self)
        VehicleEquipment(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Razorback.Weapon, self).__init__(parent=parent, name='Weapon')

            self.twinlinkedheavybolter = self.variant('Twin-linked Heavy Bolter', 0)
            self.twinlinkedheavyflamer = self.variant('Twin-linked Heavy Flamer', 0)
            self.twinlinkedassaultcannon = self.variant('Twin-linked Assault Cannon', 20)
            self.twinlinkedlascannon = self.variant('Twin-linked Lascannon', 20)
            self.lascannonandtwinlinkedplasmagun = self.variant('Lascannon and Twin-linked Plasma Gun', 20)


class DropPod(SpaceMarinesBaseVehicle):
    type_name = u'Drop Pod'
    type_id = 'droppod_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Drop-Pod')

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DropPod.Weapon, self).__init__(parent=parent, name='Weapon')
            self.stormbolter = self.variant('Storm Bolter', 0)
            self.deathwindmissilelauncher = self.variant('Deathwind Missile Launcher', 15)

    class Options(OptionsList):
        def __init__(self, parent):
            super(DropPod.Options, self).__init__(parent=parent, name='Options')
            self.locatorbeacon = self.variant('Locator Beacon', 10)

    def __init__(self, parent, off=False):
        super(DropPod, self).__init__(parent=parent, points=35 if not off else 0, deep_strike=True, transport=True)
        self.Weapon(self)
        self.Options(self)


class LandRaider(SpaceMarinesBaseVehicle):
    type_name = u'Land Raider'
    type_id = 'landraider_v3'

    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Land-Raider')

    engines = False

    def __init__(self, parent, off=0):
        super(LandRaider, self).__init__(
            parent=parent, points=250 - off, tank=True, transport=True,
            gear=[Gear('Twin-linked Heavy Bolter'), Gear('Twin-linked Lascannon', count=2),
                  Gear('Smoke Launchers'), Gear('Searchlight')] + ([Gear('Overcharged engines')] if self.engines else [])
        )
        self.Options(self)

    class Options(VehicleEquipment):
        def __init__(self, parent):
            super(LandRaider.Options, self).__init__(parent, blade=False)
            self.multimelta = self.variant('Multi-melta', 10)


class LandRaiderCrusader(SpaceMarinesBaseVehicle):
    type_name = u'Land Raider Crusader'
    type_id = 'landraidercrusader_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Land-Raider-Crusader')

    engines = False

    def __init__(self, parent, off=0):
        super(LandRaiderCrusader, self).__init__(
            parent=parent, points=250 - off, tank=True, transport=True,
            gear=[Gear('Twin-linked Assault Cannon'), Gear('Hurricane Bolter', count=2),
                  Gear('Smoke Launchers'), Gear('Frag Assault Launcher'),
                  Gear('Searchlight')] + ([Gear('Overcharged engines')] if self.engines else [])
        )
        LandRaider.Options(self)


class LandRaiderRedeemer(SpaceMarinesBaseVehicle):
    type_name = u'Land Raider Redeemer'
    type_id = 'landraiderredeemer_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Land-Raider-Redeemer')

    engines = False

    def __init__(self, parent, off=0):
        super(LandRaiderRedeemer, self).__init__(
            parent=parent, points=240 - off, tank=True, transport=True,
            gear=[Gear('Twin-linked Assault Cannon'), Gear('Flamestorm Cannon', count=2),
                  Gear('Frag Assault Launcher'), Gear('Smoke Launchers'), Gear('Searchlight')] +
            ([Gear('Overcharged engines')] if self.engines else []))
        LandRaider.Options(self)


class Transport(IATransport):
    def __init__(self, parent, off=False):
        super(Transport, self).__init__(parent=parent, name='Transport')
        self.droppod = SubUnit(self, DropPod(parent=None, off=off))
        self.models.extend([
            self.droppod,
            SubUnit(self, Rhino(parent=None, off=off))])
        if not off:
            self.models.extend([SubUnit(self, Razorback(parent=None))])
            self.ia_models.append(SubUnit(self, InfernumRazorback(parent=None)))

    def is_droppod(self):
        return self.droppod.visible


class TerminatorTransport(IATransport):
    def __init__(self, parent, off=0):
        super(TerminatorTransport, self).__init__(parent=parent, name='Transport')
        self.models.extend([
            SubUnit(self, LandRaider(parent=None, off=off)),
            SubUnit(self, LandRaiderCrusader(parent=None, off=off)),
            SubUnit(self, LandRaiderRedeemer(parent=None, off=off)),
        ])


class DreadnoughtTransport(IATransport):
    def __init__(self, parent):
        super(DreadnoughtTransport, self).__init__(parent=parent, name='Transport')
        self.models.append(SubUnit(self, DropPod(parent=None)))
        self.ia_models.append(SubUnit(self, LuciusDropPod(parent=None)))
