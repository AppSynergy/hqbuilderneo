__author__ = 'Ivan Truskov'

from builder.core2 import OneOf, OptionsList, Gear, Count,\
    UnitDescription
from armory import RelicBlade, SpecialOptions, HolyRelic, HolyRelicWeapon,\
    Melee, CommonRanged, Armour, TerminatorMelee, TerminatorRanged,\
    Boltgun, BoltPistol, PowerAxe, Crozius, PowerFist,\
    TerminatorComby, Wrath, TearerArmour, BaseWeapon, ArcanaRelicWeapon
from transport import DreadnoughtTransport
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedDreadnought
from builder.games.wh40k.roster import Unit


class Captain(Unit):
    type_id = 'captain_v3'
    type_name = u'Captain'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Captain')

    class MeleeWeapon(HolyRelicWeapon, TerminatorMelee, CommonRanged,
                      RelicBlade, Melee):
        pass

    class RangedWeapon(Wrath, ArcanaRelicWeapon, TerminatorRanged, Melee,
                       CommonRanged, Boltgun, BoltPistol):
        pass

    class Options(SpecialOptions):
        def __init__(self, *args, **kwargs):
            super(Captain.Options, self).__init__(*args, **kwargs)
            self.sshield = self.variant('Storm shield', 15)

        def check_rules(self):
            super(Captain.Options, self).check_rules()
            if self.armour:
                self.sshield.used = self.sshield.visible = not self.armour.is_tda()

    def __init__(self, parent):
        super(Captain, self).__init__(parent, self.type_name, 90, [Gear('Iron halo')])
        if self.parent.roster.supplement == 'tearer':
            self.armour = TearerArmour(self, art=20, tda=30)
        else:
            self.armour = Armour(self, art=20, tda=30)
        self.relic = HolyRelic(self, self.armour)
        self.opt = self.Options(self, armour=self.armour, relic=self.relic, bike=True, jump=True)
        self.wep1 = self.MeleeWeapon(self, 'Melee weapon', armour=self.armour)
        self.wep2 = self.RangedWeapon(self, 'Ranged weapon', armour=self.armour)

    def check_rules(self):
        super(Captain, self).check_rules()
        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.wep1.get_unique() + self.wep2.get_unique()\
            + self.relic.get_unique() + self.armour.get_unique()


class TerminatorCaptain(Unit):
    type_id = 'terminator_captain_v3'
    type_name = u'Terminator Captain'

    class MeleeWeapon(HolyRelicWeapon, TerminatorMelee):
        def __init__(self, parent, *args, **kwargs):
            super(TerminatorCaptain.MeleeWeapon, self).__init__(parent, *args, free_hammer=True, **kwargs)
            self.variant('Hammer of Baal', 10)

    class RangedWeapon(ArcanaRelicWeapon, TerminatorRanged):
        pass

    def __init__(self, parent):
        super(TerminatorCaptain, self).__init__(parent, points=135, gear=[
            Gear('Terminator armour'), Gear('Iron halo')
        ])

        class DummyArmour:
            def is_tda(self):
                return True

        self.armour = DummyArmour()
        self.relic = HolyRelic(self, self.armour)
        self.opt = SpecialOptions(self, armour=self.armour, relic=self.relic, bike=False, jump=False)
        self.wep1 = self.MeleeWeapon(self, 'Melee weapon', armour=self.armour)
        self.wep2 = self.RangedWeapon(self, 'Ranged weapon', armour=self.armour)

    def check_rules(self):
        super(TerminatorCaptain, self).check_rules()
        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.wep1.get_unique() + self.wep2.get_unique()\
            + self.relic.get_unique()


class Librarian(Unit):
    type_id = 'librarian_v3'
    type_name = u'Librarian'

    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Librarian')

    class Mastery(OneOf):
        def __init__(self, parent):
            super(Librarian.Mastery, self).__init__(parent, 'Mastery level')
            self.lvl1 = self.variant('Mastery Level 1', 0)
            self.lvl2 = self.variant('Mastery Level 2', 25)

        def count_charges(self):
            if self.cur == self.lvl1:
                return 1
            else:
                return 2

    class RangedWeapon(Wrath, ArcanaRelicWeapon, CommonRanged, Boltgun, BoltPistol):
        pass

    class ForceWeapon(BaseWeapon):
        def __init__(self, *args, **kwargs):
            super(Librarian.ForceWeapon, self).__init__(*args, **kwargs)
            self.force = self.variant('Force weapon', 0)
            self.staff = self.variant('Gallian\'s Staff', 10)
            self.relic_options += [self.staff]

    class MeleeWeapon(HolyRelicWeapon, ForceWeapon):
        pass

    class ArchangelOption(OptionsList):
        def __init__(self, parent):
            super(Librarian.ArchangelOption, self).__init__(parent, '')
            self.variant('The Executioner\'s Hood', 5)

    class TerminatorWeapon(OptionsList):
        def __init__(self, parent, armour):
            super(Librarian.TerminatorWeapon, self).__init__(
                parent, 'Terminator weapon', limit=1)
            self.variant('Storm bolter', 5)
            self.variant('Combi-flamer', 10)
            self.variant('Combi-melta', 10)
            self.variant('Combi-plasma', 10)
            self.variant('Storm shield', 10)
            self.armour = armour

        def check_rules(self):
            super(Librarian.TerminatorWeapon, self).check_rules()
            self.used = self.visible = self.armour.is_tda()

    def __init__(self, parent):
        super(Librarian, self).__init__(parent, self.type_name, 65,
                                        gear=[Gear('Psychic hood')])
        self.psy = self.Mastery(self)
        if self.parent.roster.supplement == 'tearer':
            self.armour = TearerArmour(self, tda=25)
        else:
            self.armour = Armour(self, tda=25)
        self.relics = HolyRelic(self, armour=self.armour)
        self.archangel = self.ArchangelOption(self)
        if not self.roster.supplement == 'archangel':
            self.archangel.used = self.archangel.visible = False
        self.opt = SpecialOptions(self, armour=self.armour, relic=self.relics, bike=True, jump=True)
        self.mle = self.MeleeWeapon(self, 'Melee weapon', armour=self.armour)
        self.rng1 = self.RangedWeapon(self, 'Ranged weapon', armour=self.armour)
        self.rng2 = self.TerminatorWeapon(self, armour=self.armour)

    def check_rules(self):
        super(Librarian, self).check_rules()
        self.rng1.used = self.rng1.visible = not self.armour.is_tda()
        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.relics.get_unique() + self.mle.get_unique() + self.rng1.get_unique() \
               + self.archangel.description + self.armour.get_unique()

    def count_charges(self):
        return self.psy.count_charges()

    def build_statistics(self):
        return self.note_charges(super(Librarian, self).build_statistics())


class Tycho(Unit):
    type_name = u'Captain Tycho, Master of Sacrifice'
    type_id = 'tycho_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Captain-Tycho')

    def __init__(self, parent):
        super(Tycho, self).__init__(parent, 'Captain Tycho', 130, [
            Gear('Artificer armour'), Gear('Bolt pistol'),
            Gear('Frag grenades'), Gear('Krak grenades'),
            Gear('Digital weapons'), Gear('Iron halo'), Gear('Blood song')], True, True)


class Karlaen(Unit):
    type_name = u'Captain Karlaen, The Shield of Baal'
    type_id = 'karlaen_v3'

    def __init__(self, parent):
        super(Karlaen, self).__init__(parent, 'Captain Karlaen', 160, [
            Gear('Terminator armour'), Gear('Storm bolter'),
            Gear('Iron halo'), Gear('The Hammer of Baal')], True, True)

        class FakeArmour(object):
            def is_tda(self):
                return True

        self.armour = FakeArmour()


class DCTycho(Unit):
    type_name = u'Tycho the Lost, the Fallen Star'
    type_id = 'dc_tycho_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Tycho-the-Lost')

    def __init__(self, parent):
        super(DCTycho, self).__init__(parent, 'Tycho the Lost', 145, [
            Gear('Artificer armour'), Gear('Bolt pistol'),
            Gear('Frag grenades'), Gear('Krak grenades'),
            Gear('Digital weapons'), Gear('Iron halo'), Gear('Blood song')], True, True)

    def get_unique(self):
        """Returns the same as regular Tycho to make them mutually exclusive"""
        return 'Captain Tycho'


class TearerDreadnought(IATransportedDreadnought):
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Dreadnought')

    class Relic(OptionsList):
        def __init__(self, parent):
            super(TearerDreadnought.Relic, self).__init__(parent, 'Relic of Cretacia')
            self.variant('Bones of Baelor', 15)

    def __init__(self, parent, *args, **kwargs):
        super(TearerDreadnought, self).__init__(parent, *args, **kwargs)
        self.relic = self.Relic(self)
        self.relic.used = self.relic.visible = self.roster.supplement == 'tearer'

    def get_unique_gear(self):
        result = super(TearerDreadnought, self).get_unique_gear()
        if self.relic.used:
            result += self.relic.description
        return result


class LibrarianDreadnought(TearerDreadnought):
    type_name = u'Librarian Dreadnought'
    type_id = 'lib_dread_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Librarian-Dreadnought')

    class BuiltInWeapon(OneOf):
        def __init__(self, parent):
            super(LibrarianDreadnought.BuiltInWeapon, self).__init__(
                parent, 'Built-in weapon')
            self.variant('Storm bolter', 0)
            self.variant('Heavy flamer', 10)
            self.variant('Meltagun', 10)

    class Mastery(OneOf):
        def __init__(self, parent):
            super(LibrarianDreadnought.Mastery, self).__init__(parent, 'Psychic Pilot')
            self.variant('Mastery level 1', 0)
            self.variant('Mastery level 2', 25)

    def __init__(self, parent):
        super(LibrarianDreadnought, self).__init__(
            parent, self.type_name, 150, gear=[Gear('Furioso force halberd'),
                                               Gear('Power fist'), Gear('Psychic hood'),
                                               Gear('Searchlight'), Gear('Smoke launchers')])
        self.Mastery(self)
        self.BuiltInWeapon(self)
        self.transport = DreadnoughtTransport(self)


class Mephiston(Unit):
    type_name = u'Mephiston, Lord of Death'
    type_id = 'mephiston_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Mephiston')

    def __init__(self, parent):
        super(Mephiston, self).__init__(parent, 'Mephiston', 175, [
            Gear('Artificer armour'), Gear('Plasma pistol'),
            Gear('Force sword'), Gear('Frag grenades'),
            Gear('Krak grenades'), Gear('Psychic hood')], True, True)

    def count_charges(self):
        return 3

    def build_statistics(self):
        return self.note_charges(super(Mephiston, self).build_statistics())


class Sanguinor(Unit):
    type_name = u'The Sanguinor, The Golden Angel'
    type_id = 'sanguinor_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='The-Sanguinor')

    def __init__(self, parent):
        super(Sanguinor, self).__init__(parent, 'The Sanguinor', 200, [
            Gear('Artificer armour'), Gear('Encarmine sword'),
            Gear('Death mask'), Gear('Frag grenades'),
            Gear('Krak grenades'), Gear('Iron Halo'), Gear('Jump pack')],
            True, True)


class Astorath(Unit):
    type_name = u'Astorath, Redeemer of the Lost'
    type_id = 'astorath_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Astorath')

    def __init__(self, parent):
        super(Astorath, self).__init__(parent, 'Astorath', 165, [
            Gear('Artificer armour'), Gear('Bolt Pistol'),
            Gear('Rosarius'), Gear('Frag grenades'),
            Gear('Krak grenades'), Gear('The Executioner\'s Axe'),
            Gear('Jump pack')], True, True)


class Priest(Unit):
    type_name = u'Sanguinary priest'
    type_id = 'sangpriest_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Sanguinary-Priest')

    class MeleeWeapon(HolyRelicWeapon, CommonRanged, Melee):
        pass

    class RangedOption(OptionsList):
        def __init__(self, parent):
            super(Priest.RangedOption, self).__init__(parent, '')
            self.variant('Take bolt pistol', 1, gear=[])

    class RangedWeapon(Wrath, ArcanaRelicWeapon, Melee, CommonRanged, BoltPistol):
        pass

    def __init__(self, parent):
        super(Priest, self).__init__(parent, self.type_name, 60,
                                     gear=[Gear('Power armour'), Gear('Frag grenades'),
                                           Gear('Krak grenades'), Gear('Blood chalice'),
                                           Gear('Narthecium')])
        self.relics = HolyRelic(self)
        self.opt = SpecialOptions(self, relic=self.relics, bike=True, jump=True)
        self.mle = self.MeleeWeapon(self, 'Melee weapon')
        self.ropt = self.RangedOption(self)
        self.rng = self.RangedWeapon(self, 'Ranged weapon')

    def check_rules(self):
        super(Priest, self).check_rules()
        self.rng.used = self.rng.visible = self.ropt.any
        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.relics.get_unique() + self.mle.get_unique() + self.rng.get_unique()


class Corbulo(Unit):
    type_name = u'Brother Corbulo, Keeper of the Red Grail'
    type_id = 'corbulo_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Brother-Corbulo')

    def __init__(self, parent):
        super(Corbulo, self).__init__(parent, 'Corbulo', 120,
                                      gear=[Gear('Power armour'), Gear('Bolt pistol'),
                                            Gear('Frag grenades'), Gear('Krak grenades'),
                                            Gear('Red Grail'), Gear('Narthecium'),
                                            Gear('Heaven\'s Teeth')], unique=True, static=True)


class Techmarine(Unit):
    type_name = u'Techmarine'
    type_id = 'techmarine_v3'
    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Techmarine')

    class ServoOptions(OneOf):
        def __init__(self, parent):
            super(Techmarine.ServoOptions, self).__init__(parent, 'Techmarine gear')
            self.variant('Servo-arm', 0)
            self.variant('Servo-harness', 25)
            self.jump = self.variant('Jump pack', 0)

    class TechmarineSpecial(SpecialOptions):
        def __init__(self, parent, servo):
            super(Techmarine.TechmarineSpecial, self).__init__(parent, bike=True)
            self.servo = servo

        def check_rules(self):
            self.spacemarinebike.used = self.spacemarinebike.visible\
                                        = not self.servo.cur == self.servo.jump

    class Weapon1(Melee, CommonRanged, PowerAxe, BoltPistol):
        pass

    class Weapon2(Melee, CommonRanged, PowerAxe, Boltgun):
        pass

    class ServitorWeapons(OneOf):
        def __init__(self, parent):
            super(Techmarine.ServitorWeapons, self).__init__(parent, 'Servitor weapon')
            self.servo = self.variant('Servo-arm', 0)
            self.variant('Heavy bolter', 10)
            self.variant('Multi-melta', 10)
            self.variant('Plasma cannon', 20)

    def __init__(self, parent):
        super(Techmarine, self).__init__(parent, self.type_name, 50, gear=[
            Gear('Artificer armour'), Gear('Frag grenades'), Gear('Krak grenades')])
        self.arm = self.ServoOptions(self)
        self.opt = self.TechmarineSpecial(self, self.arm)
        self.wep1 = self.Weapon1(self, 'Ranged weapon')
        self.wep2 = self.Weapon2(self, '')
        self.serv = Count(self, 'Servitors', 0, 5, 10, True)
        self.swep1 = self.ServitorWeapons(self)
        self.swep2 = self.ServitorWeapons(self)

    def check_rules(self):
        self.swep1.used = self.swep1.visible = self.serv.cur > 0
        self.swep2.used = self.swep2.visible = self.serv.cur > 1

    def build_description(self):
        if self.serv.cur == 0:
            return super(Techmarine, self).build_description()
        desc = UnitDescription(self.type_name, self.points)
        mar = UnitDescription(self.type_name, 50)
        mar.add(self.arm.description).add_points(self.arm.points)
        mar.add(self.opt.description).add_points(self.opt.points)
        mar.add(self.wep1.description).add_points(self.wep1.points)
        mar.add(self.wep2.description).add_points(self.wep2.points)
        desc.add(mar)
        serv = UnitDescription('Servitor', 10)
        scnt = self.serv.cur
        if self.swep1.used and self.swep1.cur != self.swep1.servo:
            scnt -= 1
            desc.add(serv.clone().add(self.swep1.description).add_points(self.swep1.points))
        if self.swep2.used and self.swep2.cur != self.swep2.servo:
            scnt -= 1
            desc.add(serv.clone().add(self.swep2.description).add_points(self.swep2.points))
        if scnt > 0:
            desc.add(serv.add(Gear('Servo-arm')).set_count(scnt))
        return desc


class Chaplain(Unit):
    type_id = 'chaplain_v3'
    type_name = u'Chaplain'

    wikilink = Unit.template_link.format(codex_name='blood-angels', unit_name='Chaplain')

    class RangedWeapon(Wrath, ArcanaRelicWeapon, TerminatorComby, PowerFist,
                       CommonRanged, Boltgun, BoltPistol):
        pass

    class MeleeWeapon(HolyRelicWeapon, Crozius):
        pass

    def __init__(self, parent):
        super(Chaplain, self).__init__(parent, self.type_name, 90,
                                       gear=[Gear('Rosarius')])
        if self.parent.roster.supplement == 'tearer':
            self.armour = TearerArmour(self, tda=30)
        else:
            self.armour = Armour(self, tda=30)
        self.relics = HolyRelic(self, armour=self.armour)
        self.opt = SpecialOptions(self, armour=self.armour, relic=self.relics, bike=True, jump=True)
        self.mle = self.MeleeWeapon(self, 'Melee weapon', armour=self.armour)
        self.rng = self.RangedWeapon(self, 'Ranged weapon', armour=self.armour)

    def check_rules(self):
        super(Chaplain, self).check_rules()
        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.relics.get_unique() + self.mle.get_unique()\
            + self.rng.get_unique() + self.armour.get_unique()


class DeathCompanyChaplain(Unit):
    type_id = 'dc_chaplain_v3'
    type_name = u'Death Company Chaplain'

    class Pistol(BaseWeapon):
        def __init__(self, *args, **kwargs):
            super(DeathCompanyChaplain.Pistol, self).__init__(*args, **kwargs)
            self.ipistol = self.variant('Inferno pistol', 0)

    class RangedWeapon(ArcanaRelicWeapon, Pistol):
        pass

    class MeleeWeapon(HolyRelicWeapon, Crozius):
        def __init__(self, *args, **kwargs):
            lost_relics = kwargs.pop('lost', False)
            super(DeathCompanyChaplain.MeleeWeapon, self).__init__(*args, lost=lost_relics, **kwargs)
            if lost_relics:
                self.relic_options += [
                    self.variant("The Gilded Crozius", 25)]

    def __init__(self, parent):
        super(DeathCompanyChaplain, self).__init__(parent, points=125,
                                                   gear=[Gear('Jump pack'), Gear('Frag grenades'),
                                                         Gear('Krak grenades'), Gear('Rosarius')])

        # for RelicBearer checks
        class DummyArmour:
            def is_tda(self):
                return False

        self.armour = DummyArmour()
        self.relics = HolyRelic(self, armour=self.armour,
                            lost_relics=parent.roster.supplement == 'death_company')
        self.opt = SpecialOptions(self, armour=self.armour, relic=self.relics, bike=False, jump=False)
        self.mle = self.MeleeWeapon(self, 'Melee weapon', armour=self.armour,
                                    lost=parent.roster.supplement == 'death_company')
        self.rng = self.RangedWeapon(self, 'Ranged weapon', armour=self.armour,
                                     lost=parent.roster.supplement == 'death_company')

    def check_rules(self):
        super(DeathCompanyChaplain, self).check_rules()
        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.relics.get_unique() + self.mle.get_unique()
