__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear,\
    Count, UnitList, ListSubUnit
from builder.games.wh40k.orks_v3.fast import Deffkoptas as CodexDeffkoptas
from builder.games.wh40k.orks_v3.heavy import KillaKans as CodexKans
from builder.games.wh40k.roster import Unit


class GrotTanks(Unit):
    type_name = "Grot Tank Mob"
    type_id = 'grot_tanks_v2'
    imperial_armour = True

    class GrotTank(ListSubUnit):
        class Upgrades(OptionsList):
            def __init__(self, parent):
                super(GrotTanks.GrotTank.Upgrades, self).__init__(parent, 'Upgrades')
                self.kommanda = self.variant('Kommanda', 15)
                self.variant('Pintle-mounted shoota', 5)

        class Weapon(OptionsList):
            def __init__(self, parent, kommanda):
                super(GrotTanks.GrotTank.Weapon, self).__init__(parent, 'Primary weapons')
                self.variant('Big shoota', 5)
                self.variant('Skorcha', 5)
                self.variant('Grotzooka', 10)
                self.variant('Rokkit luncha', 15)
                self.variant('Kustom mega-blasta', 20)
                self.kommanda = kommanda

            def check_rules(self):
                limit = 1 + self.kommanda.value
                OptionsList.process_limit(self.options, limit)

        def __init__(self, parent):
            super(GrotTanks.GrotTank, self).__init__(parent, 'Grot Tank', 30)
            self.up = self.Upgrades(self)
            self.wep = self.Weapon(self, self.up.kommanda)

        def build_description(self):
            desc = super(GrotTanks.GrotTank, self).build_description()
            if self.root_unit.paint.any:
                desc.add_points(5)
            return desc

        @ListSubUnit.count_gear
        def is_kommanda(self):
            return self.up.kommanda.value

    class Paintjob(OptionsList):
        def __init__(self, parent):
            super(GrotTanks.Paintjob, self).__init__(parent, 'Paintjobs')
            self.variant('Red paint jobs', 5)

        @property
        def points(self):
            return super(GrotTanks.Paintjob, self).points * self.parent.get_count()

    def __init__(self, parent):
        super(GrotTanks, self).__init__(parent)
        self.paint = self.Paintjob(self)
        self.models = UnitList(self, self.GrotTank, 3, 6)

    def get_count(self):
        return self.models.count

    def check_rules(self):
        super(GrotTanks, self).check_rules()
        bosses = sum(u.is_kommanda() for u in self.models.units)
        if bosses > 1:
            self.error("Only one Grot Tank may be a Kommanda")


class GrotMegaTank(Unit):
    type_name = u'Grot Mega-tank'
    type_id = 'grot_mega_v2'
    imperial_armour = True

    class HeavyTurret(OneOf):
        def __init__(self, parent, index):
            super(GrotMegaTank.HeavyTurret, self).__init__(
                parent, "" if index > 0 else "Heavy turret")
            self.variant('Twin-linked big shoota', 10)
            self.variant('Twin-linked skorcha', 10)
            self.variant('Twin-linked grotzooka', 20)
            self.variant('Twin-linked rokkit launcha', 20)
            self.variant('Twin-linked kustom mega-blasta', 30)

    class LightTurret(OneOf):
        def __init__(self, parent, index):
            super(GrotMegaTank.LightTurret, self).__init__(
                parent, "" if index > 0 else "Light turret")
            self.variant('Big shoota', 5)
            self.variant('Skorcha', 5)
            self.variant('Grotzooka', 10)
            self.variant('Rokkit launcha', 10)
            self.variant('Kustom mega-blasta', 20)

    class Options(OptionsList):
        def __init__(self, parent):
            super(GrotMegaTank.Options, self).__init__(parent, 'Options')
            self.variant('Pintle-mounted shoota', 5)
            self.variant('Red paint job', 5)
            self.variant('Wreckin\' ball', 10)

    def __init__(self, parent):
        super(GrotMegaTank, self).__init__(parent, points=70, gear=[
            Gear('Doza Blade'), Gear('Grot Riggers')])
        [self.HeavyTurret(self, i) for i in range(0, 2)]
        [self.LightTurret(self, i) for i in range(0, 3)]
        Count(self, 'Boom kannister', 0, 2, 10)
        self.Options(self)


class Warkoptas(Unit):
    type_name = u'Warkopta Skwadron'
    type_id = 'warkoptas_v2'
    imperial_armour = True

    class Warkopta(ListSubUnit):
        type_name = u'Warkopta'
        base_points = 65

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Warkoptas.Warkopta.Weapon1, self).__init__(parent, 'Weapon')
                self.variant("Big Shoota", 0)
                self.variant("Skorcha", 0)
                self.variant("Rokkit launcha", 10)
                self.variant("Kustom mega-blasta", 15)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(Warkoptas.Warkopta.Weapon2, self).__init__(parent, '')
                self.variant("Twin-linked deffgun", 0)
                self.variant("Twin-linked rattler kannon", 10)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Warkoptas.Warkopta.Options, self).__init__(parent, 'Options')
                self.variant('Red paint job', 5)
                self.variant('Stikkbomb chukkas', 5)

        def __init__(self, parent):
            super(Warkoptas.Warkopta, self).__init__(parent, points=65)
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.opt = self.Options(self)
            self.bmb = Count(self, 'Bigbomm', 0, 2, 15)

    def __init__(self, parent):
        super(Warkoptas, self).__init__(parent)
        self.models = UnitList(self, self.Warkopta, 1, 3)

    def get_count(self):
        return self.models.count


class Deffkoptas(CodexDeffkoptas):
    model_points = 35


class Kans(CodexKans):
    model_points = 35
