__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear, UnitDescription,\
    OptionalSubUnit, SubUnit, Count, UnitList, ListSubUnit
from builder.games.wh40k.orks_v3.heavy import DeffDread
from builder.games.wh40k.roster import Unit


class ScrapTrukk(Unit):
    type_name = "Scrap Trukk"
    type_id = 'scrap_trukk_v2'
    base_points = 35

    class Options(OptionsList):
        def __init__(self, parent):
            super(ScrapTrukk.Options, self).__init__(parent, 'Options')
            self.variant("Red paint job", 5)
            self.variant("Grot riggers", 5)
            self.variant("Stikkbomb chukka", 5)
            self.variant("Boarding plank", 5)
            self.variant("Wreckin' ball", 10)
            self.variant("Reinforces ram", 5)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ScrapTrukk.Weapon, self).__init__(parent, 'Weapon')
            self.variant("Big shoota", 0)
            self.variant("Rokkit launcha", 5)

    def __init__(self, parent):
        super(ScrapTrukk, self).__init__(parent, points=35,
                                         gear=[
                                             Gear('Armour plates'),
                                             Gear('Grabbin\' Klaw')])
        self.Weapon(self)
        self.Options(self)


class SpannaBoyz(Unit):
    type_name = u'Spanna Boyz'
    type_id = 'spanna_boyz_v2'

    class Options(OptionsList):
        def __init__(self, parent):
            super(SpannaBoyz.Options, self).__init__(parent, 'Options')
            self.bomb = self.variant('Stikkbombz', 1, per_model=True)
            self.cybork = self.variant('Cybork body', 3, per_model=True)

        def check_rules(self):
            self.cybork.used = self.cybork.visible = self.roster.has_painboss()

        @property
        def points(self):
            return super(SpannaBoyz.Options, self).points * self.parent.get_count()

    class Weapon(OneOf):
        def __init__(self, parent):
            super(SpannaBoyz.Weapon, self).__init__(parent, 'Weapon')
            self.slugga = self.variant('Slugga', 0)
            self.shoota = self.variant('Shoota', 0)

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(SpannaBoyz.Transport, self).__init__(parent, 'Transport')
            SubUnit(self, ScrapTrukk(parent=None))

    class Mek(Unit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(SpannaBoyz.Mek.Weapon, self).__init__(parent, 'Weapon')
                self.slugga = self.variant('Slugga', 0)
                self.shoota = self.variant('Shoota', 0)
                self.variant('Burna', 10)
                self.variant("Rokkit launcha")
                self.variant("Big shoota", 5)
                self.variant("Kustom mega-blasta", 15)

        class Oiler(OptionsList):
            def __init__(self, parent):
                super(SpannaBoyz.Mek.Oiler, self).__init__(parent, 'Options')
                self.variant('Grot Oiler', 5)

        def __init__(self, parent):
            super(SpannaBoyz.Mek, self).__init__(parent, 'Mek', 6 + 10, gear=[
                Gear('Choppa'), Gear('Mek\'s Tools')])
            self.wep = self.Weapon(self)
            self.opt = self.Oiler(self)

        def build_description(self):
            res = super(SpannaBoyz.Mek, self).build_description()
            if self.parent.parent.parent.opt.bomb.value:
                res.add_points(1)
                res.add(Gear('Stikkbombz'))
            if self.parent.parent.parent.opt.cybork.value:
                res.add_points(3)
                res.add(Gear('Cybork body'))
            return res

    class Boss(OptionalSubUnit):
        def __init__(self, parent):
            super(SpannaBoyz.Boss, self).__init__(parent, 'Boss')
            SubUnit(self, SpannaBoyz.Mek(parent=None))

    def __init__(self, parent):
        super(SpannaBoyz, self).__init__(parent)
        self.boyz = Count(self, "Spanna Boyz", 10, 20, 6, True,
                          gear=UnitDescription('Spanna Boy', 6, options=[Gear('Choppa')]))

        self.weapon = self.Weapon(self)

        self.opt = self.Options(self)

        self.mek = self.Boss(self)
        self.transport = self.Transport(self)

    def check_rules(self):
        self.boyz.min, self.boyz.max = 10 - self.mek.count, 20 - self.mek.count
        self.transport.used = self.transport.visible = self.count <= 12

    def get_count(self):
        return self.boyz.cur + self.mek.count

    def are_cyb(self):
        return self.opt.cybork.value

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        models = self.boyz.description[0]  # Count.description is a list
        if self.opt.bomb.value:
            models.add_points(1)
            models.add(Gear('Stikkbombz'))
        if self.opt.cybork.value:
            models.add_points(3)
            models.add(Gear('Cybork body'))
        models.add(self.weapon.description)
        desc.add(self.mek.description)
        desc.add(self.transport.description)
        return desc

    def build_statistics(self):
        return self.note_transport(super(SpannaBoyz, self).build_statistics())


class Gretchins(Unit):
    type_name = u'Gretchin Scavenger Mob'
    type_type = 'scavengers_v2'

    class Runtherd(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(Gretchins.Runtherd.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Grabba stikk', 0)
                self.variant('Grot-prods', 5)

        def __init__(self, parent):
            super(Gretchins.Runtherd, self).__init__(parent, 'Runtherd', 10,
                                                     gear=[Gear('Slugga'), Gear('Squig hound')])
            self.Weapon(self)

    def __init__(self, parent):
        super(Gretchins, self).__init__(parent)

        self.models = Count(self, 'Grot scavs', 10, 30, 4,
                            gear=UnitDescription('Grot Scav', 4, options=[
                                Gear('Grot blasta'), Gear('Firebombs')]))
        self.runth = UnitList(self, self.Runtherd, 1, 1)

    def check_rules(self):
        runth = int(self.models.cur + 9) / 10
        self.runth.min = self.runth.max = runth

    def get_count(self):
        return self.models.cur


class DeffDreadMob(Unit):
    type_name = u'Deff Dread Mob'
    type_id = 'ddread_mob_v2'

    class DeffDread(DeffDread, ListSubUnit):
        def __init__(self, parent):
            super(DeffDreadMob.DeffDread, self).__init__(parent, points=75)

    def __init__(self, parent):
        super(DeffDreadMob, self).__init__(parent)
        self.models = UnitList(self, self.DeffDread, 1, 3)

    def get_count(self):
        return self.models.count
