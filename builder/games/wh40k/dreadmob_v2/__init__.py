__author__ = 'Ivan Truskov'
__summary__ = ['Dreadmob Update for 6th Edition']


from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, PrimaryDetachment,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, LordsOfWarSection, UnitType
from builder.games.wh40k.fortifications import Fort

# from builder.games.wh40k.escalation.tau import LordsOfWar as EscLordsOfWar
from hq import KustomMekaDread, PainBoss, BigMek
from builder.games.wh40k.imperial_armour.volume8.orks import Buzzgob
from elites import Junka, SlashaMob, BurnaBoyz
from troops import SpannaBoyz, Gretchins, DeffDreadMob
from fast import GrotTanks, GrotMegaTank, Warkoptas, Deffkoptas, Kans
from builder.games.wh40k.orks_v3.fast import Dakkajet
from builder.games.wh40k.imperial_armour.aeronautika.orks import AttakFightas
from heavy import Lifta, BigTrakk, MegaDread, LootedWagon, Lootas
from builder.games.wh40k.orks_v3.fast import BurnaBommer, BlitzaBommer
from builder.games.wh40k.escalation.orks import Squiggoth, Stompa, BigMekStompa
from builder.games.wh40k.imperial_armour.apocalypse.orks import KustomFortress, KillKrusha, KillBursta, KillBlasta


class HQ(HQSection):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        self.buzzgob = UnitType(self, Buzzgob)
        self.dread = UnitType(self, KustomMekaDread)
        self.painboss = UnitType(self, PainBoss)
        UnitType(self, BigMek)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        UnitType(self, Junka)
        UnitType(self, SlashaMob)
        UnitType(self, BurnaBoyz)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.spanna = UnitType(self, SpannaBoyz, min_limit=1)
        UnitType(self, Gretchins)
        UnitType(self, DeffDreadMob)
        self.slashas = UnitType(self, SlashaMob)

    def check_rules(self):
        if not self.spanna.count:
            self.error("Dreadmob must include at least one Spanna Boyz unit")


class Fast(FastSection):
    def __init__(self, parent):
        super(Fast, self).__init__(parent)
        UnitType(self, GrotTanks)
        UnitType(self, GrotMegaTank)
        UnitType(self, Warkoptas)
        UnitType(self, Deffkoptas)
        UnitType(self, Kans)
        UnitType(self, Dakkajet)
        UnitType(self, AttakFightas)


class Heavy(HeavySection):
    def __init__(self, parent):
        super(Heavy, self).__init__(parent)
        UnitType(self, Lifta)
        UnitType(self, BigTrakk)
        UnitType(self, MegaDread)
        UnitType(self, BurnaBommer)
        UnitType(self, BlitzaBommer)
        UnitType(self, LootedWagon)
        UnitType(self, Lootas)


class Lords(LordsOfWarSection):
    def __init__(self, parent):
        super(Lords, self).__init__(parent)
        UnitType(self, KustomFortress)
        UnitType(self, KillBlasta)
        UnitType(self, KillBursta)
        UnitType(self, KillKrusha)
        UnitType(self, Squiggoth)
        UnitType(self, Stompa)
        UnitType(self, BigMekStompa)


class DreadmobV2Base(Wh40kBase):
    army_id = 'dreadmob_v2_base'
    army_name = 'Dreadmob'
    imperial_armour = True

    def __init__(self):
        self.hq = HQ(parent=self)
        self.troops = Troops(parent=self)
        super(DreadmobV2Base, self).__init__(
            hq=self.hq, elites=Elites(parent=self), troops=self.troops,
            fast=Fast(parent=self), heavy=Heavy(parent=self),
            fort=Fort(parent=self), lords=Lords(parent=self))

    def check_rules(self):
        super(DreadmobV2Base, self).check_rules()

        pain_bosses = self.hq.painboss.count
        cyb_sp = reduce(lambda acc, cur: acc + 1 if cur.are_cyb() else 0, self.troops.spanna.units, 0)
        if pain_bosses < cyb_sp:
            self.error("Only one unit of Spanna Boyz per Pain Boss may be upgraded to have cybork bodies.")
        slasha_cnt = 0
        if type(self.parent.parent) is PrimaryDetachment:
            slasha_cnt += self.hq.painboss.count > 0
            self.troops.slashas.visible = True
        else:
            self.troops.slashas.visible = False
            if self.hq.buzzgob.count > 0:
                self.error("Buzzgob must be warlord in Dread Mob army!")
            if self.hq.dread.count == len(self.hq.units):
                self.error("Kustom Meka-Dread must be Warlord of Dread Mob army (if there are no other candidates)")

        if self.troops.slashas.count > slasha_cnt:
            self.error("At most one Cybork Slashas mob may be taken as a troops choice if Pain Boss is Warlord")

    def has_painboss(self):
        return self.hq.painboss.count > 0

    def is_ghazkull(self):
        return False


class DreadmobV2CAD(DreadmobV2Base, CombinedArmsDetachment):
    army_id = 'dreadmob_v2_cad'
    army_name = 'Dreadmob (Combined arms detachment)'


class DreadmobV2AD(DreadmobV2Base, AlliedDetachment):
    army_id = 'dreadmob_v2_ad'
    army_name = 'Dreadmob (Allied detachment)'


class DreadmobV2PD(DreadmobV2Base, PlanetstrikeDefender):
    army_id = 'dreadmob_v2_pd'
    army_name = 'Dreadmob (Planetstrike defender detachment)'


class DreadmobV2PA(DreadmobV2Base, PlanetstrikeAttacker):
    army_id = 'dreadmob_v2_pa'
    army_name = 'Dreadmob (Planetstrike attacker detachment)'


class DreadmobV2SA(DreadmobV2Base, SiegeAttacker):
    army_id = 'dreadmob_v2_sa'
    army_name = 'Dreadmob (Siege War attacker detachment)'


class DreadmobV2SD(DreadmobV2Base, SiegeDefender):
    army_id = 'dreadmob_v2_sd'
    army_name = 'Dreadmob (Siege War defender detachment)'


faction = 'Dreadmob'


class DreadmobV2(Wh40k7ed):
    army_id = 'dreadmob_v2'
    army_name = 'Dreadmob'
    imperial_armour = True

    @property
    def imperial_armour_on(self):
        return self.imperial_armour

    faction = faction

    def __init__(self):
        super(DreadmobV2, self).__init__([DreadmobV2CAD], [])


class DreadmobV2Missions(Wh40k7edMissions):
    army_id = 'dreadmob_v2_mis'
    army_name = 'Dreadmob'
    imperial_armour = True

    @property
    def imperial_armour_on(self):
        return self.imperial_armour

    faction = faction

    def __init__(self):
        super(DreadmobV2Missions, self).__init__([DreadmobV2CAD, DreadmobV2PA,
                                                  DreadmobV2PD, DreadmobV2SA,
                                                  DreadmobV2SD], [])


detachments = [DreadmobV2CAD, DreadmobV2AD, DreadmobV2PA,
               DreadmobV2PD, DreadmobV2SA, DreadmobV2SD]
