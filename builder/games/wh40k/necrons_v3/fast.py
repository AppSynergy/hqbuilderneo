__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OptionsList, UnitList,\
    ListSubUnit, Count, UnitDescription, OneOf, SubUnit, OptionalSubUnit
from builder.games.wh40k.unit import Unit, Squadron


class NightScythe(Unit):
    type_name = u'Night Scythe'
    type_id = 'night_scythe_v3'

    def __init__(self, parent):
        super(NightScythe, self).__init__(parent, self.type_name, 130,
                                          [Gear('Twin-linked tesla destructor')],
                                          static=True)


class NightScythes(Squadron):
    type_name = u'Night Scythes'
    type_id = 'scythes_v3'
    unit_class = NightScythe
    unit_max = 4


class GhostArk(Unit):
    type_name = u'Ghost Ark'
    type_id = 'ghost_ark_v3'

    def __init__(self, parent):
        super(GhostArk, self).__init__(parent, self.type_name, 105,
                                       [Gear('Quantum shielding'),
                                        Gear('Gauss flayer array', count=2)],
                                       static=True)


class Transport(OptionalSubUnit):
    def __init__(self, parent, warriors=False):
        super(Transport, self).__init__(
            parent, 'Transport', 1)
        self.subopt = []
        if warriors:
            self.subopt += [SubUnit(self, GhostArk(parent=None))]
        self.subopt += [SubUnit(self, NightScythe(parent=None))]


class Wraiths(Unit):
    type_name = u'Canoptek Wraiths'
    type_id = 'wraiths_v3'

    class Wraith(ListSubUnit):
        class Weapon(OptionsList):
            def __init__(self, parent):
                super(Wraiths.Wraith.Weapon, self).__init__(
                    parent, 'Weapon', limit=1)
                self.variant('Whip coils', 3)
                self.variant('Particle caster', 5)
                self.variant('Transdimensional beamer', 10)

        def __init__(self, parent):
            super(Wraiths.Wraith, self).__init__(parent, 'Canoptek Wraith', 40)
            self.Weapon(self)

    def __init__(self, parent):
        super(Wraiths, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.Wraith, 3, 6)

    def get_count(self):
        return self.models.count


class Scarabs(Unit):
    type_name = u'Canoptek Scarabs'
    type_id = 'scarabs_v3'

    def __init__(self, parent):
        super(Scarabs, self).__init__(parent, self.type_name)
        self.models = Count(self, self.type_name, 3, 9, 20, True,
                            gear=UnitDescription(self.type_name, points=20))

    def get_count(self):
        return self.models.cur


class TombBlades(Unit):
    type_name = u'Tomb Blades'
    type_id = 'tomb_blades_v3'

    class TombBlade(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(TombBlades.TombBlade.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Twin-linked gauss blaster', 0)
                self.variant('Twin-linked tesla carbine', 0)
                self.variant('Particla beamer', 2)

        class Option1(OptionsList):
            def __init__(self, parent):
                super(TombBlades.TombBlade.Option1, self).__init__(parent, 'Options')
                self.variant('Shieldvanes', 2)

        class Option2(OptionsList):
            def __init__(self, parent):
                super(TombBlades.TombBlade.Option2, self).__init__(parent, '', limit=1)
                self.variant('Shadowloom', 1)
                self.variant('Nebuloscope', 2)

        def __init__(self, parent):
            super(TombBlades.TombBlade, self).__init__(parent, 'Tomb Blade', 18)
            self.Weapon(self)
            self.Option1(self)
            self.Option2(self)

    def __init__(self, parent):
        super(TombBlades, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.TombBlade, 3, 9)

    def get_count(self):
        return self.models.count


class Destroyers(Unit):
    type_name = u'Destroyers'
    type_id = 'destroyers_v3'

    class Upgrade(OptionsList):
        def __init__(self, parent):
            super(Destroyers.Upgrade, self).__init__(parent, 'Upgrade')
            self.variant('Heavy Destroyer', 50, gear=[])

    def __init__(self, parent):
        super(Destroyers, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Destroyers', 1, 5, 40, True,
                            gear=UnitDescription('Destroyer', 40,
                                                 options=[Gear('Gauss cannon')]))
        self.up = self.Upgrade(self)

    def check_rules(self):
        super(Destroyers, self).check_rules()
        self.models.min, self.models.max = (0 if self.up.any else 1, 4 if self.up.any else 5)

    def get_count(self):
        return self.models.cur + (1 if self.up.any else 0)

    def build_description(self):
        desc = UnitDescription(self.name, self.points, self.count)
        desc.add(self.models.description)
        if self.up.any:
            desc.add(UnitDescription('Heavy Destroyer', 50,
                                     options=[Gear('Heavy gauss cannon')]))
        return desc
