__author__ = 'Ivan Truskov'
from builder.core2 import OneOf, OptionsList


class Weapon(OneOf):
    def __init__(self, parent, name, default, bolt=True, fist=True):
        super(Weapon, self).__init__(parent, name=name)
        import collections
        if isinstance(default, collections.Iterable) and not isinstance(default, str):
            for d in default:
                self.variant(d, 0)
        else:
            self.variant(default, 0)
        if self.parent.roster.is_siege():
            if bolt:
                self.variant('Bolt pistol', 1)
            self.variant('Plasma pistol', 15)
            self.variant('Power weapon', 10)
        else:
            if bolt:
                self.variant('Bolt pistol', 2)
            self.variant('Plasma pistol', 10)
            self.variant('Power sword', 10)
        if fist:
            self.variant('Power fist', 15)


class Bombs(OptionsList):
    def __init__(self, parent):
        super(Bombs, self).__init__(parent, 'Options')
        self.variant('Melta-bombs', 5)
