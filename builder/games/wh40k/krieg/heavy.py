__author__ = 'Ivan Truskov'
from builder.core2 import OptionsList, Gear, OneOf,\
    UnitDescription, ListSubUnit, UnitList,\
    Count
from builder.games.wh40k.unit import Unit, Squadron
from fast import VehicleOptions
from troops import HeavyWeaponSquad


class Thunderer(Unit):
    type_name = 'Thunderer'

    def __init__(self, parent):
        super(Thunderer, self).__init__(parent, points=140, gear=[
            Gear('Demolisher cannon'), Gear('Smoke launchers'), Gear('Searchlight')
        ])
        VehicleOptions(self, camo=True)


class ThundererSquadron(Squadron):
    type_name = u'Thunderer Siege Squadron'
    type_id = 'thunderers_krieg_v1'
    unit_class = Thunderer
    imperial_armour = True


class LemanRuss(Unit):
    type_name = 'Leman Russ'

    class Type(OneOf):
        def __init__(self, parent):
            super(LemanRuss.Type, self).__init__(parent=parent, name='Type')
            self.variant('Leman Russ Battle Tank', 150, gear=[Gear('Battle cannon')])
            self.variant('Leman Russ Exterminator', 130, gear=[Gear('Exterminator autocannon')])
            self.vanq = self.variant('Leman Russ Vanquisher', 135, gear=[Gear('Vanquisher battle cannon')])
            self.variant('Leman Russ Eradicator', 120, gear=[Gear('Eradicator nova cannon')])
            self.variant('Leman Russ Demolisher', 170, gear=[Gear('Demolisher siege cannon')])
            self.variant('Leman Russ Punisher', 140, gear=[Gear('Punisher gatling cannon')])
            self.variant('Leman Russ Executioner', 155, gear=[Gear('Executioner plasma cannon')])
            self.variant('Leman Russ Annihilator', 150, gear=[Gear('Twin-linked lascannon')])
            self.variant('Leman Russ Conqueror', 115, gear=[Gear('Conqueror cannon'), Gear('Storm bolter')])

    class Weapon(OneOf):
        def __init__(self, parent, lascannon=False):
            super(LemanRuss.Weapon, self).__init__(parent=parent, name='Weapon')

            self.heavybolter = self.variant('Heavy bolter', 0)
            self.lascannon = lascannon and self.variant('Lascannon', lascannon)
            self.heavyflamer = self.variant('Heavy flamer', 0)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(LemanRuss.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)

            self.variant('Heavy bolters', 20, gear=[Gear('Heavy bolter', count=2)])
            self.variant('Heavy flamers', 10, gear=[Gear('Heavy flamer', count=2)])
            self.variant('Multi-meltas', 20, gear=[Gear('Multimelta', count=2)])
            self.variant('Plasma cannons', 30, gear=[Gear('Plasma cannon', count=2)])

    class CoAxial(OptionsList):
        def __init__(self, parent):
            super(LemanRuss.CoAxial, self).__init__(parent, 'Co-axial weapon', limit=1)
            self.variant('Storm bolter', 15)
            self.variant('Heavy stubber', 15)

    def __init__(self, parent):
        super(LemanRuss, self).__init__(
            parent=parent, gear=[Gear('Searchlight'), Gear('Smoke launchers')])
        self.tank_type = self.Type(self)
        self.Weapon(self, lascannon=15)
        self.coax = self.CoAxial(self)
        self.Sponsons(self)
        VehicleOptions(self)

    def check_rules(self):
        super(LemanRuss, self).check_rules()
        self.coax.used = self.coax.visible = self.tank_type.cur == self.tank_type.vanq

    def build_description(self):
        desc = super(LemanRuss, self).build_description()
        desc.name = self.tank_type.cur.title
        if hasattr(self, 'root_unit'):
            desc.add_points(self.root_unit.opt.points)
        return desc


class LemanRussSquadron(Squadron):
    type_name = u'Death Korps Leman Russ Tank Squadron'
    type_id = 'tanks_krieg_v1'
    unit_class = LemanRuss
    imperial_armour = True

    class Netting(OptionsList):
        def __init__(self, parent):
            super(LemanRussSquadron.Netting, self).__init__(parent, 'Options')
            self.variant('Camo netting', 15, per_model=True)

    def __init__(self, parent):
        super(LemanRussSquadron, self).__init__(parent)
        self.opt = self.Netting(self)

    def build_points(self):
        res = super(LemanRussSquadron, self).build_points()
        res += self.opt.points * (self.get_count() - 1)
        return res


class AssaultLemanRuss(Unit):
    type_name = 'Leman Russ'

    base_points = 0

    class Type(OneOf):
        def __init__(self, parent):
            super(AssaultLemanRuss.Type, self).__init__(parent=parent, name='Type')
            self.variant('Leman Russ Battle Tank', 150, gear=[Gear('Battle cannon')])
            self.variant('Leman Russ Exterminator', 150, gear=[Gear('Exterminator autocannon')])
            self.variant('Leman Russ Annihilator', 150, gear=[Gear('Twin-linked lascannon')])
            self.variant('Leman Russ Conqueror', 150, gear=[Gear('Conqueror cannon'), Gear('Storm bolter')])
            self.vanq = self.variant('Leman Russ Vanquisher', 155, gear=[Gear('Vanquisher battle cannon')])
            self.variant('Leman Russ Eradicator', 160, gear=[Gear('Eradicator nova cannon')])
            self.variant('Leman Russ Demolisher', 165, gear=[Gear('Demolisher siege cannon')])
            self.variant('Leman Russ Punisher', 180, gear=[Gear('Punisher gatling cannon')])
            self.variant('Leman Russ Executioner', 190, gear=[Gear('Executioner plasma cannon')])

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(AssaultLemanRuss.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)

            self.variant('Heavy bolters', 20, gear=[Gear('Heavy bolter', count=2)])
            self.variant('Heavy flamers', 20, gear=[Gear('Heavy flamer', count=2)])
            self.variant('Multi-meltas', 30, gear=[Gear('Multimelta', count=2)])
            self.variant('Plasma cannons', 40, gear=[Gear('Plasma cannon', count=2)])

    def __init__(self, parent):
        super(AssaultLemanRuss, self).__init__(points=self.base_points,
            parent=parent, gear=[Gear('Searchlight'), Gear('Smoke launchers')])
        self.tank_type = self.Type(self)
        LemanRuss.Weapon(self, lascannon=15)
        self.coax = LemanRuss.CoAxial(self)
        self.Sponsons(self)
        VehicleOptions(self)

    def check_rules(self):
        super(AssaultLemanRuss, self).check_rules()
        self.coax.used = self.coax.visible = self.tank_type.cur == self.tank_type.vanq

    def build_description(self):
        desc = super(AssaultLemanRuss, self).build_description()
        desc.name = self.tank_type.cur.title
        if hasattr(self, 'root_unit'):
            desc.add_points(self.root_unit.opt.points)
        return desc


class LemanRussAssaultSquadron(Squadron):
    type_name = u'Death Korps Leman Russ Tank Squadron'
    type_id = 'tanks_krieg_assault_v1'
    unit_class = AssaultLemanRuss
    imperial_armour = True

    class Netting(OptionsList):
        def __init__(self, parent):
            super(LemanRussAssaultSquadron.Netting, self).__init__(parent, 'Options')
            self.variant('Camo netting', 20, per_model=True)

    def __init__(self, parent):
        super(LemanRussAssaultSquadron, self).__init__(parent)
        self.opt = self.Netting(self)

    def build_points(self):
        res = super(LemanRussAssaultSquadron, self).build_points()
        res += self.opt.points * (self.get_count() - 1)
        return res


class HeavyWeaponsPlatoon(Unit):
    type_name = u'Death Korps Heavy Weapons Platoon'
    type_id = 'heavy_platoon_krieg_v1'

    def __init__(self, parent):
        super(HeavyWeaponsPlatoon, self).__init__(parent)
        self.squads = UnitList(self, HeavyWeaponSquad, 1, 3)

    def build_statistics(self):
        return {
            'Units': self.squads.count,
            'Models': 3 * self.squads.count
        }


class Colossus(Unit):
    type_name = u'Colossus Bombard'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Colossus.Weapon, self).__init__(parent)
            self.variant('Heavy bolter')
            self.variant('Heavy flamer')

    class VehicleOptions(OptionsList):
        def __init__(self, parent, camo=False):
            super(Colossus.VehicleOptions, self).__init__(parent, 'Options')
            self.variant('Enclosing crew compartment', 15)
            self.sb = self.variant('Storm bolter', 5)
            self.hs = self.variant('Heavy stubber', 5)
            self.variant('Hunter-killer missile', 10)
            self.variant('Extra armour', 10)

        def check_rules(self):
            super(Colossus.VehicleOptions, self).check_rules()
            self.process_limit([self.hs, self.sb], 1)

    def __init__(self, parent):
        super(Colossus, self).__init__(parent, 'Colossus', 140, [
            Gear('Searchlight'), Gear('Smoke launchers'), Gear('Colossus siege mortar')
        ])
        self.VehicleOptions(self)

    def build_description(self):
        desc = super(Colossus, self).build_description()
        if hasattr(self, 'root_unit'):
            desc.add_points(self.root_unit.opt.points)
        return desc


class BombardBattery(Squadron):
    type_name = u'Death Korps Bombard Battery'
    type_id = 'bombard_krieg_v1'
    unit_class = Colossus
    imperial_armour = True

    def __init__(self, parent):
        super(BombardBattery, self).__init__(parent)
        self.opt = LemanRussSquadron.Netting(self)

    def build_points(self):
        res = super(BombardBattery, self).build_points()
        res += self.opt.points * (self.get_count() - 1)
        return res


class HeavyArtilleryBattery(Unit):
    type_name = u'Death Korps Heavy Artillery Battery'
    type_id = 'heavy_battery_krieg_v1'
    imperial_armour = True

    class ArtilleryPiece(ListSubUnit):
        type_name = 'Heavy Artillery Carriege'

        class Gun(OneOf):
            def __init__(self, parent):
                super(HeavyArtilleryBattery.ArtilleryPiece.Gun, self).__init__(parent, 'Gun')
                self.variant('Earthshaker Cannon')
                self.medusa = self.variant('Medusa Siege Gun', 25)

        def __init__(self, parent):
            super(HeavyArtilleryBattery.ArtilleryPiece, self).__init__(parent, points=75 - 8 * 4)
            self.gun = self.Gun(self)
            self.crew = Count(self, 'Crew', 4, 8, 8, True,
                              gear=UnitDescription('Death Korps Crew', options=[
                                  Gear('Flak armour'), Gear('Close combat weapon'),
                                  Gear('Frag grenades'), Gear('Krak grenades'), Gear('Lasgun')
                              ]))

    class Options(OptionsList):
        def __init__(self, parent):
            super(HeavyArtilleryBattery.Options, self).__init__(parent, 'Battery options')
            self.camo = self.variant('Camo netting', 15, per_model=True)
            self.shells = self.variant('Bastion Breacher shells', 5, per_model=True)

        def check_rules(self):
            super(HeavyArtilleryBattery.Options, self).check_rules()
            self.shells.used = self.shells.visible = all(u.gun.cur == u.gun.medusa for u in self.parent.pieces.units)

        @property
        def points(self):
            return super(HeavyArtilleryBattery.Options, self).points * self.parent.get_count()

    def __init__(self, parent):
        super(HeavyArtilleryBattery, self).__init__(parent)
        self.pieces = UnitList(self, self.ArtilleryPiece, 1, 3)
        self.opt = self.Options(self)

    def get_count(self):
        return self.pieces.count

    def build_statistics(self):
        pieces = sum((1 + p.crew.cur) * p.count for p in self.pieces.units)
        return {'Models': pieces,
                'Units': 1}


class Ordnance(Unit):
    type_name = 'Ordnance vehicle'

    class Type(OneOf):
        def __init__(self, parent):
            super(Ordnance.Type, self).__init__(parent=parent, name='Type')
            self.variant('Basilisk Artillery Tank', 125,
                         gear=UnitDescription('Basilisk', options=[Gear('Earthshaker cannon')]))
            self.medusa = self.variant('Medusa Siege gun', 135,
                                       gear=UnitDescription('Medusa', options=[Gear('Medusa siege cannon')]))
            self.variant('Colossus Bombard', 140,
                         gear=UnitDescription('Colossus', options=[Gear('Colossus siege mortar')]))

    class Options(OptionsList):
        def __init__(self, parent, gun_type):
            self.gun_type = gun_type
            super(Ordnance.Options, self).__init__(parent, 'Options')
            self.variant('Enclosed crew compartment', 15)
            self.sb = self.variant('Storm bolter', 10)
            self.hs = self.variant('Heavy stubber', 10)
            self.variant('Hunter-killer missile', 10)
            self.dozer = self.variant('Dozer blade', 5)
            self.shells = self.variant('Breacher shells', 5)

        def check_rules(self):
            super(Ordnance.Options, self).check_rules()
            self.process_limit([self.hs, self.sb], 1)
            self.shells.used = self.shells.visible = self.gun_type.cur == self.gun_type.medusa

    def __init__(self, parent):
        super(Ordnance, self).__init__(parent)
        self.gun_type = self.Type(self)
        self.secondary = LemanRuss.Weapon(self)
        self.opt = self.Options(self, self.gun_type)

    def build_description(self):
        result = self.gun_type.cur.gear.clone()
        result.points = self.build_points()
        result.add([Gear('Searchlight'), Gear('Smoke launchers')])
        result.add(self.secondary.description)
        result.add(self.opt.description)
        result.count = self.get_count()
        if hasattr(self, 'root_unit'):
            result.add_points(self.root_unit.opt.points)
        return result


class OrdnanceBattery(Squadron):
    type_name = 'Death Korps Ordnance Tank Battery'
    type_id = 'ordnancebat_krieg_v1'
    unit_class = Ordnance
    imperial_armour = True

    def __init__(self, parent):
        super(OrdnanceBattery, self).__init__(parent)
        self.opt = LemanRussAssaultSquadron.Netting(self)

    def build_points(self):
        res = super(OrdnanceBattery, self).build_points()
        res += self.opt.points * (self.get_count() - 1)
        return res


class Thunderbolt(Unit):
    type_name = u'Thunderbolt'

    class CommonOptions(OptionsList):
        def __init__(self, parent):
            super(Thunderbolt.CommonOptions, self).__init__(parent, 'Options')
            self.variant('Flare/chaff launcher', 10)
            self.variant('Infra-red targeting', 5)
            self.variant('Illum flares', 5)
            self.variant('Distinctive paint scheme', 10)

    class Payload(OptionsList):
        def __init__(self, parent):
            super(Thunderbolt.Payload, self).__init__(parent, 'Payload', limit=1)
            self.variant('Four Hellstrike Missiles', 40, gear=[Gear('Hellstrike missile', count=4)])
            self.variant('Six tactical bombs', 40, gear=[Gear('Tactical bomb', count=6)])
            self.variant('Six Skystrike missiles', 40, gear=[Gear('Skystrike missile', count=6)])

    def __init__(self, parent):
        super(Thunderbolt, self).__init__(parent, points=160, gear=[
            Gear('Twin-linked autocannon', count=2),
            Gear('Twin-linked lascannon'),
            Gear('Armoured cockpit')
        ])
        self.Payload(self)
        self.CommonOptions(self)


class ThunderSquadron(Squadron):
    type_name = u'Imperial Navy Air Support Squadron (Thunderbolt)'
    type_id = 'thunderbolt_squadron_krieg_v1'
    unit_class = Thunderbolt
    imperial_armour = True

    def __init__(self, parent):
        super(ThunderSquadron, self).__init__(parent, 'Imperial Navy Air Support Squadron')


class Lightning(Unit):
    type_name = u'Lightning'

    class Payload(Thunderbolt.Payload):
        def __init__(self, parent):
            super(Lightning.Payload, self).__init__(parent)
            self.variant('Four Hellfury Missiles', 40, gear=[Gear('Hellfury missile', count=4)])

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Lightning.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Long-barreled autocannon')
            self.variant('Two Hellstrike missiles', gear=[Gear('Hellstrike missile', count=2)])
            self.variant('Four tactical bombs', gear=[Gear('Tactical bomb', count=4)])

    def __init__(self, parent):
        super(Lightning, self).__init__(parent, points=145, gear=[
            Gear('Twin-linked lascannon'),
            Gear('Armoured cockpit')
        ])
        self.Weapon(self)
        self.Payload(self)
        Thunderbolt.CommonOptions(self)


class LightningSquadron(Squadron):
    type_name = u'Imperial Navy Air Support Squadron (Lightning)'
    type_id = 'lightning_squadron_krieg_v1'
    unit_class = Lightning
    imperial_armour = True

    def __init__(self, parent):
        super(LightningSquadron, self).__init__(parent, 'Imperial Navy Air Support Squadron')


class Avenger(Unit):
    type_name = u'Avenger'

    class Payload2(OptionsList):
        def __init__(self, parent):
            super(Avenger.Payload2, self).__init__(parent, 'Payload', limit=1)
            self.variant('Six tactical bombs', 40, gear=[Gear('Tactical bomb', count=6)])
            self.variant('Two Hellstrike missiles', 20, gear=[Gear('Hellstrike missile', count=2)])
            self.variant('Two Hellfury Missiles', 20, gear=[Gear('Hellfury missile', count=2)])
            self.variant('Two Hellfury Missiles', 20, gear=[Gear('Hellfury missile', count=2)])
            self.variant('Two missile launchers', 40, gear=[Gear('Missile launcher', count=2)])
            self.variant('Two autocannons', 30, gear=[Gear('Autocannon', count=2)])
            self.variant('Two multi-lasers', 30, gear=[Gear('Multi-laser', count=2)])

    def __init__(self, parent):
        super(Avenger, self).__init__(parent, points=150, gear=[
            Gear('Avenger bolt cannon'),
            Gear('Lascannon', count=2),
            Gear('Armoured cockpit'),
            Gear('Heavy stubber')
        ])
        self.Payload2(self)
        Thunderbolt.CommonOptions(self)


class AvengerSquadron(Squadron):
    type_name = u'Imperial Navy Air Support Squadron (Avenger)'
    type_id = 'avenger_squadron_krieg_v1'
    unit_class = Avenger
    imperial_armour = True

    def __init__(self, parent):
        super(AvengerSquadron, self).__init__(parent, 'Imperial Navy Air Support Squadron')
