__author__ = 'Ivan Truskov'
from builder.core2 import OptionsList, Gear, OptionalSubUnit,\
    SubUnit, OneOf
from builder.games.wh40k.unit import Unit


class Centaur(Unit):
    type_name = 'Centaur Carrier'
    type_id = 'centaur_krieg_v1'
    imperial_armour = True

    class VehicleOptions(OptionsList):
        def __init__(self, parent):
            super(Centaur.VehicleOptions, self).__init__(parent, 'Options')
            self.variant('Hunter-killer missile', 10)
            self.variant('Dozer blade', 5)
            self.variant('Extra armour', 10)
            self.variant('Camo netting', 15)

    def __init__(self, parent):
        super(Centaur, self).__init__(parent, points=40, gear=[
            Gear('Heavy stubber'), Gear('Searchlight'), Gear('Smoke launchers')
        ])
        self.VehicleOptions(self)


class Hades(Unit):
    type_name = 'Hades Breaching Drill'
    type_id = 'hades_krieg_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Hades, self).__init__(parent, points=60, gear=[
            Gear('Melta-cutter drill')], static=True)


class SiegeTransport(OptionalSubUnit):
    def __init__(self, parent):
        super(SiegeTransport, self).__init__(parent, 'Transport')
        self.centaur = SubUnit(self, Centaur(parent=None))

    def check_rules(self):
        super(SiegeTransport, self).check_rules()
        self.options.set_visible(self.centaur, self.parent.get_count() <= 5)


class HadesTransport(OptionalSubUnit):
    def __init__(self, parent):
        super(HadesTransport, self).__init__(parent, 'Transport')
        self.hades = SubUnit(self, Hades(parent=None))

        self.options.options[0].value = 1
        self.options.options[0].active = False


class StormChimera(Unit):
    type_name = 'Storm Chimera'
    type_id = 'storm_chimera_krieg_v1'

    class VehicleOptions(OptionsList):
        def __init__(self, parent):
            super(StormChimera.VehicleOptions, self).__init__(parent, 'Options')
            self.sb = self.variant('Storm bolter', 10)
            self.hs = self.variant('Heavy stubber', 10)
            self.variant('Hunter-killer missile', 10)
            self.variant('Camo netting', 20)
            self.bl = self.variant('Dozer blade', 5)
            self.pl = self.variant('Mine plough', 15)

        def check_rules(self):
            super(StormChimera.VehicleOptions, self).check_rules()
            self.process_limit([self.hs, self.sb], 1)
            self.process_limit([self.bl, self.pl], 1)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(StormChimera.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Heavy bolter')
            self.variant('Heavy flamer')

    def __init__(self, parent):
        super(StormChimera, self).__init__(parent, points=75, gear=[
            Gear('Autocannon'), Gear('Searchlight'), Gear('Smoke launchers'),
            Gear('Armoured track guards'), Gear('Extra armour')
        ])
        self.Weapon(self)
        self.VehicleOptions(self)


class StormTransport(OptionalSubUnit):
    hades = False

    def __init__(self, parent, centaur=False):
        super(StormTransport, self).__init__(parent, 'Transport', limit=1)
        self.centaur = centaur and SubUnit(self, Centaur(parent=None))
        self.chimera = SubUnit(self, StormChimera(self))
        if self.hades:
            SubUnit(self, Hades(parent=None))

    def check_rules(self):
        super(StormTransport, self).check_rules()
        if self.centaur:
            self.options.set_visible(self.centaur, self.parent.get_count() <= 5)
