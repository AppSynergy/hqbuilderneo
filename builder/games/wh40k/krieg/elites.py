__author__ = 'Ivan Truskov'
from builder.core2 import OptionsList, Gear, OneOf,\
    UnitDescription, SubUnit, ListSubUnit, UnitList,\
    Count
from builder.games.wh40k.unit import Unit, Squadron
from transport import SiegeTransport, Centaur, HadesTransport,\
    StormTransport
from heavy import AssaultLemanRuss
from troops import Engineers
from armory import Weapon, Bombs


class Grenadiers(Unit):
    type_name = u'Death Korps Grenadier Squad'
    type_id = 'grenadier_krieg_v1'

    imperial_armour = True

    grenadier = UnitDescription('Grenadier', options=[
        Gear('Close combat weapon'),
        Gear('Frag grenades'),
        Gear('Krak grenades'),
        Gear('Caparace armour')
    ], points=12)

    def get_transport(self):
        return SiegeTransport

    class Options(OptionsList):
        def __init__(self, parent):
            super(Grenadiers.Options, self).__init__(parent, name='Options')
            spec = Grenadiers.grenadier.clone().add(Gear('Hot-shot lasgun'))
            self.caster = self.variant('Vox-caster', 5, gear=[spec.clone().add(Gear('Vox-caster')).add_points(5)])
            spec2 = UnitDescription('Grenadier Heavy Weapons Team', 10 + 12 * 2, options=[
                Gear('Caparace armour'), Gear('Heavy flamer')
            ])
            self.team = self.variant('Heavy weapons team', 10, gear=spec2.clone())

        def get_count(self):
            return self.caster.value + 2 * self.team.value

    class Grenadier(Count):
        @property
        def description(self):
            return Grenadiers.grenadier.clone().add(Gear('Hot-shot lasgun')).set_count(
                self.cur - self.parent.opt.get_count() - sum(o.cur for o in self.parent.spec))

    class Watchmaster(Unit):
        type_name = u'Watchmaster'

        def __init__(self, parent):
            super(Grenadiers.Watchmaster, self).__init__(parent, points=60 - 12 * 4, gear=[
                Gear('Caparace armour'),
                Gear('Frag grenades'), Gear('Krak grenades')])
            self.melee = Weapon(self, 'Weapon', ['Close combat weapon', 'Bolt pistol', 'Boltgun'], bolt=False)
            self.ranged = Weapon(self, '', ['Hot-shot laspistol', 'Bolt pistol', 'Boltgun'], bolt=False)
            Bombs(self)

    def __init__(self, parent):
        super(Grenadiers, self).__init__(parent)
        SubUnit(self, self.Watchmaster(self))
        self.models = self.Grenadier(self, 'Grenadier', 4, 9, points=12)
        self.opt = self.Options(self)
        self.spec = [
            Count(self, name=g['name'], min_limit=0, max_limit=2, points=g['points'],
                  gear=Grenadiers.grenadier.clone().add(Gear(g['name'])).add_points(g['points']))
            for g in [
                dict(name='Flamer', points=5),
                dict(name='Greanade launcher', points=5),
                dict(name='Meltagun', points=10),
                dict(name='Plasma gun', points=15),
                dict(name='Heavy stubber', points=10)
            ]
        ]
        self.transport = self.get_transport()(self)

    def check_rules(self):
        super(Grenadiers, self).check_rules()
        Count.norm_counts(0, 2, self.spec)
        min_models = self.opt.get_count() + sum(o.cur for o in self.spec)
        self.models.min = max(4, min_models)

    def get_count(self):
        return 1 + self.models.cur

    def build_statistics(self):
        return self.note_transport(super(Grenadiers, self).build_statistics())


class AssaultGrenadiers(Grenadiers):
    def get_transport(self):
        return StormTransport

    def __init__(self, parent):
        super(AssaultGrenadiers, self).__init__(parent)
        self.models.min = 9
        self.models.visible = False


class HydraPlatform(Unit):
    type_name = u'Hydra Flak Platform'
    type_id = 'hydra_platform_krieg_v1'

    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(HydraPlatform.Options, self).__init__(parent, 'Options')
            self.variant('Camo netting', 15)

    def __init__(self, parent):
        super(HydraPlatform, self).__init__(parent, points=50,
                                            gear=[Gear('Twin-linked Hydra autocannon', count=2)])
        self.Options(self)


class Rapiers(Unit):
    type_name = 'Death Korps Rapier Laser Destroyer Battery'
    type_id = 'rapiers_krieg_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Rapiers, self).__init__(parent)
        self.guns = Count(self, 'Rapier', 1, 3, 45, True)
        self.crew = Count(self, 'Additional crew', 0, 1, 8, True)

    def check_rules(self):
        super(Rapiers, self).check_rules()
        self.crew.max = self.guns.cur

    def build_description(self):
        res = UnitDescription(self.name, self.build_points())
        rap = UnitDescription('Rapier', points=45, options=[Gear('Laser destroyer array')])
        crew = UnitDescription('Combat Engineer', options=[Gear('Caparace armour'),
                                                           Gear('Close combat weapon'),
                                                           Gear('Frag grenades'),
                                                           Gear('Combat shotgun')])
        if self.guns.cur > self.crew.cur:
            res.add(rap.clone().add(crew).set_count(self.guns.cur - self.crew.cur))
        if self.crew.cur > 0:
            crew.set_count(2)
            res.add(rap.clone().add_points(6).add(crew).set_count(self.crew.cur))
        return res

    def get_count(self):
        return self.guns.cur

    def build_statistics(self):
        return {'Models': 2 * self.guns.cur + self.crew.cur, 'Units': 1}


class FieldArtilleryBattery(Unit):
    type_name = u'Death Korps Field Artillery Battery'
    type_id = 'field_battery_krieg_v1'
    imperial_armour = True

    class ArtilleryPiece(ListSubUnit):
        type_name = 'Artillery Piece'

        class Gun(OneOf):
            def __init__(self, parent):
                super(FieldArtilleryBattery.ArtilleryPiece.Gun, self).__init__(parent, 'Gun')
                self.variant('Heavy Quad Launcher', gear=[])
                self.variant('Heavy Mortar', gear=[])

        def __init__(self, parent):
            super(FieldArtilleryBattery.ArtilleryPiece, self).__init__(parent, points=55 - 8 * 3)
            self.gun = self.Gun(self)
            self.crew = Count(self, 'Crew', 3, 5, 8, True,
                              gear=UnitDescription('Death Korps Crew', options=[
                                  Gear('Flak armour'), Gear('Close combat weapon'),
                                  Gear('Frag grenades'), Gear('Lasgun')
                              ]))

        def build_description(self):
            result = super(FieldArtilleryBattery.ArtilleryPiece, self).build_description()
            result.name = self.gun.cur.name
            return result

    class SquadronCentaur(Centaur, ListSubUnit):
        pass

    class Towers(OptionsList):
        def __init__(self, parent):
            super(FieldArtilleryBattery.Towers, self).__init__(parent, 'Towing vehicles')
            self.centaurs = self.variant('Centaur artillery tractors', gear=[])

    def __init__(self, parent):
        super(FieldArtilleryBattery, self).__init__(parent)
        self.pieces = UnitList(self, self.ArtilleryPiece, 1, 3)
        self.towers = self.Towers(self)
        self.centaurs = UnitList(self, self.SquadronCentaur, 1, 3)

    def get_count(self):
        return self.pieces.count

    def check_rules(self):
        super(FieldArtilleryBattery, self).check_rules()
        self.centaurs.used = self.centaurs.visible = self.towers.centaurs.value
        cc = self.towers.centaurs.value and self.centaurs.count
        if cc > 0:
            if self.get_count() != cc:
                self.error('Number of towing vehicles must be equal to number of artillery pieces')

    def build_statistics(self):
        centaurs = self.towers.centaurs.value and self.centaurs.count
        pieces = sum((1 + p.crew.cur) * p.count for p in self.pieces.units)
        return {'Models': pieces + centaurs,
                'Units': 1 + centaurs}


class EliteEngineers(Engineers):
    def get_transport(self):
        return HadesTransport


class AssaultEngineers(Engineers):
    class AssaultTransport(StormTransport):
        hades = True

        def __init__(self, parent):
            super(AssaultEngineers.AssaultTransport, self).__init__(parent, True)

    def get_transport(self):
        return self.AssaultTransport


class Griffon(Unit):
    type_name = u'Griffon'

    class Options(OptionsList):
        def __init__(self, parent, compartment=True):
            super(Griffon.Options, self).__init__(parent, 'Options')
            if compartment:
                self.variant('Enclosed crew compartment', 15)
            self.sb = self.variant('Storm bolter', 10)
            self.hs = self.variant('Heavy stubber', 10)
            self.variant('Hunter-killer missile', 10)
            self.variant('Extra armour', 10)
            self.dozer = self.variant('Dozer blade', 5)
            self.plough = self.variant('Mine plough', 15)

        def check_rules(self):
            super(Griffon.Options, self).check_rules()
            self.process_limit([self.hs, self.sb], 1)
            self.process_limit([self.dozer, self.plough], 1)

    def build_description(self):
        desc = super(Griffon, self).build_description()
        if hasattr(self, 'root_unit'):
            desc.add_points(self.root_unit.opt.points)
        return desc

    class Weapon(OneOf):
        def __init__(self, parent, lascannon=False):
            super(Griffon.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant('Heavy bolter', 0)
            self.heavyflamer = self.variant('Heavy flamer', 0)

    def __init__(self, parent):
        super(Griffon, self).__init__(parent, points=75, gear=[
            Gear('Searchlight'), Gear('Smoke launchers'), Gear('Griffon heavy mortar')])
        self.secondary = self.Weapon(self)
        self.opt = self.Options(self)


class GriffonStrikeBattery(Squadron):
    type_name = u'Griffon Strike battery'
    type_id = 'griffon_krieg_v1'

    unit_class = Griffon
    imperial_armour = True

    class Netting(OptionsList):
        def __init__(self, parent):
            super(GriffonStrikeBattery.Netting, self).__init__(parent, 'Options')
            self.variant('Camo netting', 20, per_model=True)
            self.variant('Carcass shell', 5, per_model=True)

    def __init__(self, parent):
        super(GriffonStrikeBattery, self).__init__(parent)
        self.opt = self.Netting(self)

    def build_points(self):
        res = super(GriffonStrikeBattery, self).build_points()
        res += self.opt.points * (self.get_count() - 1)
        return res


class ForwardTank(AssaultLemanRuss):
    type_name = u'Leman Russ Forward Command Tank'
    type_id = 'leman_russ_forward_krieg_v1'
    base_points = 35
    imperial_armour = True

    def build_description(self):
        res = UnitDescription(self.type_name, points=self.points)
        res.add(super(ForwardTank, self).build_description())
        return res


class HydraTank(Unit):
    type_name = u'Hydra Flak Tank'
    type_id = 'hydra_tank_krieg_v1'

    def __init__(self, parent):
        super(HydraTank, self).__init__(parent, points=75,
                                        gear=[Gear('Twin-linked Hydra autocannon', count=2),
                                              Gear('Searchlight'), Gear('Smoke launchers'),
                                              Gear('Auto-targeting system')])
        Griffon.Weapon(self)
        Griffon.Options(self, compartment=False)

    def build_description(self):
        desc = super(HydraTank, self).build_description()
        if hasattr(self, 'root_unit'):
            desc.add_points(self.root_unit.opt.points)
        return desc


class HydraBattery(Squadron):
    type_name = u'Hydra Flak Tank Battery'
    type_id = 'hydra_battery_krieg_v1'
    unit_class = HydraTank
    imperial_armour = True

    class Netting(OptionsList):
        def __init__(self, parent):
            super(HydraBattery.Netting, self).__init__(parent, 'Options')
            self.variant('Camo netting', 20, per_model=True)

    def __init__(self, parent):
        super(HydraBattery, self).__init__(parent)
        self.opt = self.Netting(self)

    def build_points(self):
        res = super(HydraBattery, self).build_points()
        res += self.opt.points * (self.get_count() - 1)
        return res
