__author__ = 'Ivan Truskov'
from builder.core2 import OptionsList, Gear, OneOf,\
    UnitDescription, SubUnit, ListSubUnit, UnitList,\
    Count, OptionalSubUnit
from builder.games.wh40k.unit import Unit, Squadron
from armory import Bombs


class CyclopsSquad(Unit):
    type_name = u'Cyclops Demolition Squad'
    type_id = 'cyclops_krieg_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(CyclopsSquad, self).__init__(parent)
        self.models = Count(self, 'Cyclops Team', 1, 3, 30, True)

    def get_count(self):
        return self.models.cur

    def build_description(self):
        res = UnitDescription(self.name, self.build_points(),
                              self.get_count())
        cyclops = UnitDescription('Cyclops Demolition Vehicle', count=self.models.cur,
                                  options=[Gear('Cyclops Demolition charge')])
        operator = UnitDescription('Death Korps Operator', count=self.models.cur,
                                   options=[Gear('Flak armour'), Gear('Lasgun'), Gear('Close combat weapon')])
        res.add(cyclops).add(operator)
        return res

    def build_statistics(self):
        return {'Models': 2 * self.get_count(), 'Units': 1}


class VehicleOptions(OptionsList):
    def __init__(self, parent, camo=False, guards=True):
        plus_point = 0 if parent.roster.is_siege() else 5
        super(VehicleOptions, self).__init__(parent, 'Options')
        self.sb = self.variant('Storm bolter', 5 + plus_point)
        self.hs = self.variant('Heavy stubber', 5 + plus_point)
        self.variant('Hunter-killer missile', 10)
        self.variant('Extra armour', 10)
        if camo:
            self.variant('Camp netting', 15 + plus_point)
        self.variant('Armoured track guards', 10)
        self.dozer = self.variant('Dozer blade', 5)
        self.plough = self.variant('Mine plough', 15)

    def check_rules(self):
        super(VehicleOptions, self).check_rules()
        self.process_limit([self.hs, self.sb], 1)
        self.process_limit([self.dozer, self.plough], 1)


class Hellhound(Unit):
    type_name = 'Hellhound'

    def __init__(self, parent):
        super(Hellhound, self).__init__(parent=parent, gear=[
            Gear('Searchlight'), Gear('Smoke launchers')
        ])
        self.type = self.Type(self)
        self.Weapon(self)
        VehicleOptions(self)

    class Type(OneOf):
        def __init__(self, parent):
            super(Hellhound.Type, self).__init__(parent=parent, name='Type')
            self.hellhound = self.variant('Hellhound', 125, gear=[Gear('Inferno cannon')])
            self.devildog = self.variant('Devil dog', 135, gear=[Gear('Melta cannon')])
            self.banewolf = self.variant('Banewolf', 130, gear=[Gear('Chem cannon')])

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Hellhound.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant('Heavy bolter', 0)
            self.heavyflamer = self.variant('Heavy flamer', 0)
            self.multymelta = self.variant('Multi-melta', 10)

    def build_description(self):
        desc = super(Hellhound, self).build_description()
        desc.name = self.type.cur.title
        return desc


class HellhoundSquadron(Squadron):
    type_name = u'Hellhound Flame Tank Squadron'
    type_id = 'hellhounds_krieg_v1'
    unit_class = Hellhound
    imperial_armour = True


class AssaultHellhound(Unit):
    type_name = 'Hellhound'

    def __init__(self, parent):
        super(AssaultHellhound, self).__init__(parent=parent, gear=[
            Gear('Searchlight'), Gear('Smoke launchers')
        ])
        self.type = self.Type(self)
        self.Weapon(self)
        VehicleOptions(self)

    class Type(OneOf):
        def __init__(self, parent):
            super(AssaultHellhound.Type, self).__init__(parent=parent, name='Type')
            self.hellhound = self.variant('Hellhound', 130, gear=[Gear('Inferno cannon')])
            self.devildog = self.variant('Devil dog', 120, gear=[Gear('Melta cannon')])
            self.banewolf = self.variant('Banewolf', 130, gear=[Gear('Chem cannon')])

    class Weapon(OneOf):
        def __init__(self, parent):
            super(AssaultHellhound.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant('Heavy bolter', 0)
            self.heavyflamer = self.variant('Heavy flamer', 0)
            self.multymelta = self.variant('Multi-melta', 15)

    def build_description(self):
        desc = super(AssaultHellhound, self).build_description()
        desc.name = self.type.cur.title
        return desc


class AssaultHellhoundSquadron(Squadron):
    type_name = u'Hellhound Flame Tank Squadron'
    type_id = 'hellhounds_krieg_assault_v1'
    unit_class = AssaultHellhound
    imperial_armour = True


class Commissar(Unit):
    type_name = u'Death Rider Commissar'

    class Weapon(OneOf):
        def __init__(self, parent, name, default):
            super(Commissar.Weapon, self).__init__(parent, name=name)
            self.variant(default, 0)
            if self.parent.roster.is_siege():
                self.variant('Bolt pistol', 1)
                self.variant('Plasma pistol', 15)
                self.variant('Power weapon', 10)
            else:
                self.variant('Bolt pistol', 2)
                self.variant('Plasma pistol', 10)
                self.variant('Power sword', 10)
                self.variant('Power axe', 10)

    def __init__(self, parent):
        plus_point = 0 if parent.roster.is_siege() else 5
        super(Commissar, self).__init__(parent, points=30 + plus_point, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Caparace armour')
        ])
        self.Weapon(self, 'Weapon', 'Laspistol')
        self.Weapon(self, '', 'Close combat weapon')


class Commander(Unit):
    type_name = u'Squadron Commander'

    class Options(OptionsList):
        def __init__(self, parent):
            super(Commander.Options, self).__init__(parent, 'Options')
            self.variant('Demolition charge', 20)
            self.variant('Melta bombs', 5)
            self.colonel = self.variant('Upgrade to Colonel', 50, gear=[Gear('Refractor field')])
            self.colonel.visible = False

    def __init__(self, parent):
        super(Commander, self).__init__(parent, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Caparace armour'),
            Gear('Death Rider hunting lance')
        ])
        Commissar.Weapon(self, 'Weapon', 'Laspistol')
        Commissar.Weapon(self, '', 'Close combat weapon')
        self.opt = self.Options(self)

    def promote(self):
        self.opt.colonel.value = True
        self.opt.colonel.visible = True
        self.opt.colonel.active = False

    def build_description(self):
        res = super(Commander, self).build_description()
        if self.opt.colonel.value:
            res.name = u'Death Rider Colonel'
        return res


class DeathRiderCommand(Unit):
    type_name = u'Death Rider Command Squadron'

    class Overseer(OptionalSubUnit):
        def __init__(self, parent):
            super(DeathRiderCommand.Overseer, self).__init__(parent, 'Commissar')
            SubUnit(self, Commissar(parent=self))

    class Standard(OptionsList):
        def __init__(self, parent):
            super(DeathRiderCommand.Standard, self).__init__(parent, 'Options')
            self.variant('Regimental Standard', gear=[
                UnitDescription('Death Rider Veteran', options=[
                    Gear('Frag grenades'), Gear('Krak grenades'), Gear('Caparace armour'),
                    Gear('Close combat weapon'), Gear('Laspistol'), Gear('Regimental standard')
                ])
            ])

    def __init__(self, parent):
        super(DeathRiderCommand, self).__init__(parent, points=100)
        self.com = SubUnit(self, Commander(parent=self))
        self.ovr = self.Overseer(self)
        self.stand = self.Standard(self)
        self.stand.visible = False

    def count_models(self):
        return 5 + self.ovr.any

    def build_description(self):
        res = super(DeathRiderCommand, self).build_description()
        gnum = 4 - self.stand.any
        res.add(UnitDescription('Death Rider Veteran', count=gnum, options=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Caparace armour'),
            Gear('Close combat weapon'), Gear('Laspistol'), Gear('Death Rider hunting lance')
        ]))
        return res


class DeathRiderSquadron(ListSubUnit):
    type_name = u'Death Rider Squadron'

    class Ridemaster(Unit):
        type_name = u'Ridemaster'

        def __init__(self, parent):
            super(DeathRiderSquadron.Ridemaster, self).__init__(parent, points=85 - 15 * 4, gear=[
                Gear('Frag grenades'), Gear('Krak grenades'), Gear('Caparace armour'),
                Gear('Death Rider hunting lance')
            ])
            Commissar.Weapon(self, 'Weapon', 'Laspistol')
            Commissar.Weapon(self, '', 'Close combat weapon')
            self.opt = Bombs(self)

    def __init__(self, parent):
        super(DeathRiderSquadron, self).__init__(parent)
        SubUnit(self, self.Ridemaster(self))
        self.riders = Count(self, 'Death Riders', 4, 9, 15, True,
                            gear=UnitDescription('Death Rider Veteran', options=[
                                Gear('Frag grenades'), Gear('Krak grenades'), Gear('Caparace armour'),
                                Gear('Close combat weapon'), Gear('Laspistol'), Gear('Death Rider hunting lance')
                            ]))

    @ListSubUnit.count_gear
    def count_models(self):
        return 1 + self.riders.cur


class DeathRiderPlatoon(Unit):
    type_name = u'Death Rider Platoon'
    type_id = 'death_rider_platoon_krieg'

    class Command(OptionalSubUnit):
        def __init__(self, parent):
            super(DeathRiderPlatoon.Command, self).__init__(parent, 'Command Squad')
            self.squad = SubUnit(self, DeathRiderCommand(parent=self))

    def __init__(self, parent):
        super(DeathRiderPlatoon, self).__init__(parent)
        self.com = self.Command(self)
        self.rest = UnitList(self, DeathRiderSquadron, 1, 6)

    def build_statistics(self):
        un = self.rest.count + self.com.any
        models = sum(u.count_models() for u in self.rest.units)
        if self.com.any:
            models += self.com.squad.unit.count_models()
        return {'Models': models, 'Units': un}


class DeathRiderCommandPlatoon(Unit):
    type_name = u'Death Rider Platoon'
    type_id = 'death_rider_platoon_krieg_v1'

    class CompanyCommand(DeathRiderCommand):
        type_name = u'Death Rider Company Command Squad'

        def __init__(self, parent):
            super(DeathRiderCommandPlatoon.CompanyCommand, self).__init__(parent)
            self.stand.visible = True
            self.com.unit.promote()

    def __init__(self, parent):
        super(DeathRiderCommandPlatoon, self).__init__(parent)
        self.com = SubUnit(self, self.CompanyCommand(parent=self))
        self.rest = UnitList(self, DeathRiderSquadron, 1, 6)

    def build_statistics(self):
        un = self.rest.count + 1
        models = sum(u.count_models() for u in self.rest.units) + self.com.unit.count_models()
        return {'Models': models, 'Units': un}


class Salamander(Unit):
    type_name = 'Salamander Scout'

    def __init__(self, parent):
        super(Salamander, self).__init__(parent=parent, points=55,
                                         gear=[Gear('Heavy bolter'), Gear('Autocannon'),
                                               Gear('Searchlight'), Gear('Smoke launchers')])
        VehicleOptions(self)


class SalamanderSquadron(Squadron):
    type_name = 'Salamander Reconnaissance Squadron'
    type_id = 'salamanders_krieg_v1'
    unit_class = Salamander
    imperial_armour = True
