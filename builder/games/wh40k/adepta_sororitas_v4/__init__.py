from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as BaseLords
from hq import Jacobus, Canoness, Priest, Veridyan, Celestine
from elites import Celestians, Repentia, CommandSquad, ArcoFlagellants,\
    Crusaders, DeathCultAssasins
from troops import Sisters
from fast import Seraphims, Dominions, Rhino, Immolator
from heavy import Retributors, Exorcist, PenitentEngine
from builder.games.wh40k.imperial_armour.dataslates.imperial_knights import CerastusSection
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kKillTeam, PrimaryDetachment, Wh40kImperial,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, UnitType, Formation
from builder.games.wh40k.fortifications import Fort


class BaseHq(HQSection):
    def __init__(self, parent):
        super(BaseHq, self).__init__(parent)
        self.veridyan = UnitType(self, Veridyan)
        self.jacobus = UnitType(self, Jacobus)
        self.canoness = UnitType(self, Canoness)
        self.priest = UnitType(self, Priest)
        UnitType(self, Celestine)


class HQ(BaseHq):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        from builder.games.wh40k.dataslates.cypher import Cypher
        self.cypher = UnitType(self, Cypher, slot=0)
        from builder.games.wh40k.inquisition_v2 import Greyfax
        UnitType(self, Greyfax)
        from builder.games.wh40k.cult_mechanicus import Cawl
        UnitType(self, Cawl)
        from builder.games.wh40k.grey_knights_v3 import Voldus
        UnitType(self, Voldus)

    def check_rules(self):
        super(HQ, self).check_rules()
        self.cypher.visible = isinstance(self.parent.parent.parent, PrimaryDetachment)


class MinistorumHQ(HQSection):
    def __init__(self, parent):
        super(MinistorumHQ, self).__init__(parent)
        self.priest = UnitType(self, Priest)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        self.celestians = UnitType(self, Celestians)
        self.repentia = UnitType(self, Repentia)
        self.command_squad = UnitType(self, CommandSquad)
        self.flagellants = UnitType(self, ArcoFlagellants)
        self.crusaders = UnitType(self, Crusaders)
        self.assasins = UnitType(self, DeathCultAssasins)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.sisters = UnitType(self, Sisters)


class FastAttack(FastSection):
    def __init__(self, parent):
        super(FastAttack, self).__init__(parent)
        self.seraphims = UnitType(self, Seraphims)
        self.dominions = UnitType(self, Dominions)
        self.rhino = UnitType(self, Rhino)
        self.immolator = UnitType(self, Immolator)


class HeavySupport(HeavySection):
    def __init__(self, parent):
        super(HeavySupport, self).__init__(parent)
        self.retributors = UnitType(self, Retributors)
        self.exorcist = UnitType(self, Exorcist)
        self.penitentengine = UnitType(self, PenitentEngine)


class LordsOfWar(CerastusSection, BaseLords):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent)
        from builder.games.wh40k.space_marines_v3 import Guilliman
        UnitType(self, Guilliman)


class AdeptaSororitasV4Base(Wh40kBase):
    army_id = 'adepta_sororitas_v4'
    army_name = 'Adepta Sororitas'

    def __init__(self):

        super(AdeptaSororitasV4Base, self).__init__(
            hq=HQ(parent=self),
            elites=Elites(parent=self),
            troops=Troops(parent=self),
            fast=FastAttack(parent=self),
            heavy=HeavySupport(parent=self),
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self)
        )


class AdeptaSororitasV4CAD(AdeptaSororitasV4Base, CombinedArmsDetachment):
    army_id = 'adepta_sororitas_v4_cad'
    army_name = 'Adepta Sororitas (Combined arms detachment)'


class AdeptaSororitasV4AD(AdeptaSororitasV4Base, AlliedDetachment):
    army_id = 'adepta_sororitas_v4_ad'
    army_name = 'Adepta Sororitas (Allied detachment)'


class AdeptaSororitasV4PA(AdeptaSororitasV4Base, PlanetstrikeAttacker):
    army_id = 'adepta_sororitas_v4_pa'
    army_name = 'Adepta Sororitas (Planetstrike attacker detachment)'


class AdeptaSororitasV4PD(AdeptaSororitasV4Base, PlanetstrikeDefender):
    army_id = 'adepta_sororitas_v4_pd'
    army_name = 'Adepta Sororitas (Planetstrike defender detachment)'


class AdeptaSororitasV4SA(AdeptaSororitasV4Base, SiegeAttacker):
    army_id = 'adepta_sororitas_v4_sa'
    army_name = 'Adepta Sororitas (Siege War attacker detachment)'


class AdeptaSororitasV4SD(AdeptaSororitasV4Base, SiegeDefender):
    army_id = 'adepta_sororitas_v4_sd'
    army_name = 'Adepta Sororitas (Siege War defender detachment)'


class AdeptaSororitasV4VTF(AdeptaSororitasV4CAD):
    army_id = 'adepta_sororitas_v4_vtf'
    army_name = 'Adepta Sororitas (Vestal Task Force detachment)'

    def __init__(self):
        super(AdeptaSororitasV4Base, self).__init__(
            hq=HQ(parent=self),
            elites=Elites(parent=self),
            troops=Troops(parent=self),
            fast=FastAttack(parent=self),
            heavy=HeavySupport(parent=self),
            fort=None,
            lords=None
        )
        self.elites.min, self.elites.max = (1, 3)


class AdeptaSororitasV4MD(AdeptaSororitasV4CAD):
    army_id = 'adepta_sororitas_v4_md'
    army_name = 'Adepta Sororitas (Ministorum Delegation)'

    def __init__(self):
        super(AdeptaSororitasV4Base, self).__init__(
            hq=MinistorumHQ(parent=self),
            elites=Elites(parent=self),
            troops=None,
            fast=None,
            heavy=None,
            fort=None,
            lords=None
        )
        self.hq.min, self.hq.max = (1, 1)
        self.elites.min, self.elites.max = (0, 1)


class AdeptaSororitasV4KillTeam(Wh40kKillTeam):
    army_id = 'adepta_sororitas_v4_kt'
    army_name = 'Adepta Sororitas'

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(AdeptaSororitasV4KillTeam.KTTroops, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [Sisters])

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(AdeptaSororitasV4KillTeam.KTElites, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [
                Celestians, Repentia, CommandSquad,
                ArcoFlagellants, Crusaders, DeathCultAssasins])

    class KTFastAttack(FastSection):
        def __init__(self, parent):
            super(AdeptaSororitasV4KillTeam.KTFastAttack, self).__init__(parent)
            Wh40kKillTeam.process_vehicle_units(self, [Rhino, Immolator])
            Wh40kKillTeam.process_transported_units(self, [Dominions])
            UnitType(self, Seraphims)

    def __init__(self):
        super(AdeptaSororitasV4KillTeam, self).__init__(
            self.KTTroops(self), self.KTElites(self),
            self.KTFastAttack(self)
        )


class Conclave(Formation):
    army_id = 'adepta_sororitas_v4_conclave'
    army_name = 'Ecclesiarchy Battle Conclave'

    def __init__(self):
        super(Conclave, self).__init__()
        priest = UnitType(self, Priest)
        jacobus = UnitType(self, Jacobus)
        self.add_type_restriction([priest, jacobus], 1, 1)
        flagellants = UnitType(self, ArcoFlagellants)
        crusaders = UnitType(self, Crusaders)
        assasins = UnitType(self, DeathCultAssasins)
        self.add_type_restriction([flagellants, crusaders, assasins], 3, 10)


class WrathfulCrusade(Formation):
    army_id = 'adepta_sororitas_v4_wrathful_crusade'
    army_name = 'Wrathful Crusade'
    ia_enabled = False

    def __init__(self):
        from builder.games.wh40k.inquisition_v2 import Greyfax
        from builder.games.wh40k.space_marines_v3 import Captain, SternguardVeteranSquad, CrusaderSquad, AssaultSquad
        from builder.games.wh40k.astra_militarum import MilitarumTempestusPlatoon
        super(WrathfulCrusade, self).__init__()
        UnitType(self, Celestine, min_limit=1, max_limit=1)
        UnitType(self, Greyfax, min_limit=1, max_limit=1)
        self.cap = UnitType(self, Captain, min_limit=1, max_limit=1)
        self.stern = UnitType(self, SternguardVeteranSquad, min_limit=0, max_limit=1)
        self.crusaders = UnitType(self, CrusaderSquad, min_limit=2, max_limit=4)
        self.assault = UnitType(self, AssaultSquad, min_limit=1, max_limit=2)

        UnitType(self, MilitarumTempestusPlatoon, min_limit=1, max_limit=1)

    def is_base_codex(self):
        return False

    def is_fists(self):
        return False

    def is_hands(self):
        return False

    def is_scars(self):
        return False

    def is_ravens(self):
        return False

    def is_karchodons(self):
        return False

    def is_hawks(self):
        return False

    def is_scorpions(self):
        return False

    def is_minotaurs(self):
        return False

    def is_salamanders(self):
        return False

    def is_ultra(self):
        return False

    def is_templars(self):
        return True


faction = 'Adepta Sororotas'


class AdeptaSororitasV4(Wh40k7ed, Wh40kImperial):
    army_id = 'adepta_sororitas_v4'
    army_name = 'Adepta Sororitas'
    faction = faction

    def __init__(self):
        super(AdeptaSororitasV4, self).__init__([
            AdeptaSororitasV4CAD,
            AdeptaSororitasV4VTF,
            AdeptaSororitasV4MD,
            Conclave,
            WrathfulCrusade
        ])


class AdeptaSororitasV4Missions(Wh40k7edMissions, Wh40kImperial):
    army_id = 'adepta_sororitas_v4_mis'
    army_name = 'Adepta Sororitas'
    faction = faction

    def __init__(self):
        super(AdeptaSororitasV4Missions, self).__init__([
            AdeptaSororitasV4CAD,
            AdeptaSororitasV4PA,
            AdeptaSororitasV4PD,
            AdeptaSororitasV4SA,
            AdeptaSororitasV4SD,
            AdeptaSororitasV4VTF,
            AdeptaSororitasV4MD,
            Conclave,
            WrathfulCrusade
        ])


detachments = [
    AdeptaSororitasV4CAD,
    AdeptaSororitasV4AD,
    AdeptaSororitasV4PA,
    AdeptaSororitasV4PD,
    AdeptaSororitasV4SA,
    AdeptaSororitasV4SD,
    AdeptaSororitasV4VTF,
    AdeptaSororitasV4MD,
    Conclave,
    WrathfulCrusade
]


unit_types = [
    Jacobus, Canoness, Priest, Veridyan, Celestine,
    Celestians, Repentia, CommandSquad, ArcoFlagellants,
    Crusaders, DeathCultAssasins,
    Sisters,
    Seraphims, Dominions, Rhino, Immolator,
    Retributors, Exorcist, PenitentEngine
]
