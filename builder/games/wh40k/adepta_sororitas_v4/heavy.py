from builder.core2 import Gear, Count, OptionsList, UnitDescription, SubUnit
from armory import BoltPistol, Boltgun, Melee, Ranged, VehicleOptions
from builder.games.wh40k.roster import Unit
from builder.games.wh40k.adepta_sororitas_v4.fast import Transport


class Retributors(Unit):
    type_name = u'Retributor Squad'
    type_id = 'retributors_v4'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Retributor-Squad')

    common_gear = [Gear('Frag grenades'), Gear('Krak grenades')]
    model_points = 12

    class Superior(Unit):
        name = 'Retributor Superior'

        class Weapon1(Ranged, Melee, Boltgun):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Retributors.Superior.Options, self).__init__(parent, 'Options')
                self.variant('Melta bombs', 5)
                self.vet = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent):
            super(Retributors.Superior, self).__init__(parent, self.name, Retributors.model_points,
                                                       Retributors.common_gear)
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.opt = self.Options(self)

        def check_rules(self):
            super(Retributors.Superior, self).check_rules()
            self.wep1.allow_melee(not self.wep2.is_melee())
            self.wep1.allow_ranged(not self.wep2.is_ranged())
            self.wep2.allow_melee(not self.wep1.is_melee())
            self.wep2.allow_ranged(not self.wep1.is_ranged())

        def build_description(self):
            res = super(Retributors.Superior, self).build_description()
            if self.opt.vet.value:
                res.name = 'Veteran Retributor Superior'
            return res

    class Simulacrum(OptionsList):
        def __init__(self, parent):
            super(Retributors.Simulacrum, self).__init__(parent, 'Icon of Faith')
            self.variant('Simulacrum imperialis', 10)

    def __init__(self, parent):
        super(Retributors, self).__init__(parent)
        self.sup = SubUnit(self, self.Superior(parent=True))
        self.squad = Count(self, 'Retributor', 4, 9, self.model_points, True)
        self.opt = self.Simulacrum(self)
        self.hb = Count(self, 'Heavy bolter', 0, 4, 10)
        self.fl = Count(self, 'Heavy flamer', 0, 4, 10)
        self.mm = Count(self, 'Multi-melta', 0, 4, 10)
        self.transport = Transport(self)

    def check_rules(self):
        free = min(4, self.squad.cur - self.opt.any)
        Count.norm_counts(0, free, [self.fl, self.hb, self.mm])

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count())
        res.add(self.sup.description)
        desc = UnitDescription('Retributor', points=self.model_points, options=self.common_gear + [Gear('Bolt Pistol')])
        count = self.squad.cur
        if self.opt.any:
            res.add(desc.clone().add(Gear('Boltgun')).add(self.opt.description).add_points(self.opt.points))
            count -= 1
        for cnt in [self.fl, self.hb, self.mm]:
            if cnt.cur:
                res.add(desc.clone().add(cnt.gear).add_points(cnt.option_points).set_count(cnt.cur))
                count -= cnt.cur
        if count:
            res.add(desc.clone().add(Gear('Boltgun')).set_count(count))
        res.add(self.transport.description)
        return res

    def get_count(self):
        return self.squad.cur + 1

    def build_statistics(self):
        return self.note_transport(super(Retributors, self).build_statistics())


class Exorcist(Unit):
    type_name = "Exorcist"
    type_id = 'exorcist_v4'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Exorcist')

    def __init__(self, parent):
        super(Exorcist, self).__init__(parent, points=125,
                                       gear=[Gear('Exorcist missile launcher'),
                                             Gear('Smoke launchers'), Gear('Searchlight')])
        self.opt = VehicleOptions(self)


class PenitentEngine(Unit):
    type_name = u'Penitent Engine'
    type_id = 'penitents_v4'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Penitent-Engine')

    def __init__(self, parent):
        super(PenitentEngine, self).__init__(parent)
        self.models = Count(self, 'Penitent Engine', 1, 3, 80, True,
                           gear=UnitDescription('Penitent Engine', 80,
                                                options=[Gear('Dreadnought close combat weapon', count=2),
                                                         Gear('Heavy flamer', count=2)]))

    def get_count(self):
        return self.models.cur
