__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OptionsList
from builder.games.wh40k.roster import Unit


class Logan(Unit):
    type_name = u'Logan Grimnar, High King of Fenris'
    type_id = 'logan_v4'

    class Chariot(OptionsList):
        def __init__(self, parent):
            super(Logan.Chariot, self).__init__(parent, 'Chariot')
            self.variant('Stormrider', 70)

    def __init__(self, parent):
        super(Logan, self).__init__(parent, 'Logan Grimnar', 250,
                                    gear=[Gear('Terminator armour'),
                                          Gear('Storm bolter'),
                                          Gear('Belt of Russ'),
                                          Gear('The Axe Morkai')],
                                    unique=True)
        self.ride = self.Chariot(self)
