__author__ = 'Ivan Truskov'

from builder.core2 import ListSubUnit, UnitList, OneOf, Gear, OptionsList
from armory import VehicleEquipment
from builder.games.wh40k.unit import Unit, Squadron


class Talos(Unit):
    type_name = u'Talos'
    type_id = 'talos_unit'

    max_size = 3

    class SingleTalos(Unit):
        type_name = u'Talos'
        type_id = 'talos'

        class Melee(OneOf):
            def __init__(self, parent):
                super(Talos.SingleTalos.Melee, self).__init__(parent, 'Melee')
                self.variant('Close combat weapon', 0)
                self.variant('Chain-flails', 5)
                self.variant('Ichor injector', 10)
                self.variant('Twin-linked liquifier gun', 15)

        class Ranged(OneOf):
            def __init__(self, parent):
                super(Talos.SingleTalos.Ranged, self).__init__(parent, 'Ranged')
                self.variant('Twin-linked splinter cannon', 0)
                self.variant('Stinger pod', 5)
                self.variant('Twin-linked heat lance', 5)
                self.variant('Twin-linked haywire blaster', 10)

        def __init__(self, parent):
            super(Talos.SingleTalos, self).__init__(parent, 'Talos', 120,
                                              gear=[Gear('Armoured caparace'), Gear('Close combat weapon')])
            self.Melee(self)
            self.Ranged(self)

    class Talos(SingleTalos, ListSubUnit):
        pass

    def __init__(self, parent):
        super(Talos, self).__init__(parent, 'Talos')
        self.models = UnitList(self, self.Talos, 1, self.max_size)

    def get_count(self):
        return self.models.count


class Chronos(Unit):
    type_name = u'Chronos'
    type_id = 'chronos_unit'

    class SingleChronos(Unit):
        type_name = u'Chronos'
        type_id = 'chronos'

        class OptWeapon(OptionsList):
            def __init__(self, parent):
                super(Chronos.SingleChronos.OptWeapon, self).__init__(parent, 'Extra weapon', limit=1)
                self.variant('Spirit probe', 25)
                self.variant('Spirit vortex', 25)

        def __init__(self, parent):
            super(Chronos.SingleChronos, self).__init__(parent, 'Chronos', 100,
                                              gear=[Gear('Armoured caparace'), Gear('Spirit syphon')])
            self.OptWeapon(self)

    class Chronos(SingleChronos, ListSubUnit):
        pass

    def __init__(self, parent):
        super(Chronos, self).__init__(parent, 'Chronos')
        self.models = UnitList(self, self.Chronos, 1, 3)

    def get_count(self):
        return self.models.count


class Ravager(Unit):
    type_name = u'Ravager'
    type_id = 'ravager'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Ravager.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Disintegrator cannon', 0)
            self.variant('Dark lance', 5)

    def __init__(self, parent):
        super(Ravager, self).__init__(parent, 'Ravager', 110)
        [self.Weapon(self) for i in range(0, 3)]
        VehicleEquipment(self)


class Voidraven(Unit):
    type_name = u'Voidraven bomber'
    wikilink = 'https://sites.google.com/site/wh40000rules/codices/dark-eldar/profiles#TOC-Voidraven-Bomber'
    type_id = 'voidraven'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Voidraven.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Two void lances', 0, gear=[Gear('Void lance', count=2)])
            self.variant('Two dark scythes', 0, gear=[Gear('Dark scythe', count=2)])

    class Payload(OptionsList):
        def __init__(self, parent):
            super(Voidraven.Payload, self).__init__(parent, 'Payload', limit=1)
            self.variant('Four shatterfield missiles', points=40,
                         gear=[Gear('Shatterfield missile', count=4)])
            self.variant('Two shatterfield missiles and two implosion missiles', points=50,
                         gear=[Gear('Shatterfield missile', count=2), Gear('Implosion missile', count=2)])
            self.variant('Four implosion missiles', points=60,
                         gear=[Gear('Implosion missile', count=4)])

    class Options(OptionsList):
        def __init__(self, parent):
            super(Voidraven.Options, self).__init__(parent, 'Options')
            self.variant('Night shields')

    def __init__(self, parent):
        super(Voidraven, self).__init__(parent, self.type_name, 160, gear=[Gear('Void mine')])
        self.Weapon(self)
        self.Payload(self)
        self.Options(self)


class Voidravens(Squadron):
    type_name = u'Voidraven bombers'
    wikilink = 'https://sites.google.com/site/wh40000rules/codices/dark-eldar/profiles#TOC-Voidraven-Bombers'
    type_id = 'voidravens_v2'
    unit_class = Voidraven
    unit_max = 4
