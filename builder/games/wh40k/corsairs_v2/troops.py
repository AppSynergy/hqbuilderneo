__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OneOf, OptionsList,\
    UnitList, ListSubUnit, Count, UnitDescription, OptionalSubUnit,\
    SubUnit
from hq import CharacterTraits
from transport import Falcon, Venom
from builder.games.wh40k.roster import Unit


class CloudDancerBand(Unit):
    type_name = u'Corsair Cloud Dancer band'
    type_id = 'cloud_dancer_v2'
    imperial_armour = True

    class Dancer(ListSubUnit):
        class Upgrade(OptionsList):
            def __init__(self, parent):
                super(CloudDancerBand.Dancer.Upgrade, self).__init__(parent, 'Upgrade')
                self.variant('Upgrade to Corsair Felarch', 10, gear=[])

        class BikeWeapon(OneOf):
            def __init__(self, parent):
                super(CloudDancerBand.Dancer.BikeWeapon, self).__init__(parent, 'Bike weapon')
                self.variant('Twin-linked shuriken catapult')
                self.variant('Shuriken cannon', 10)
                self.variant('Scatter laser', 10)
                self.variant('Dark lance', 20)
                self.variant('Splinter cannon', 5)
                self.variant('Dissonance cannon', 10)

        class Weapon(OptionsList):
            def __init__(self, parent):
                super(CloudDancerBand.Dancer.Weapon, self).__init__(parent, 'Weapons', limit=2)
                self.variant('Close combat weapon')
                self.variant('Power weapon', 15)
                self.variant('Venom blade', 10)
                self.variant('Blast pistol', 20)
                self.variant('Dissonance pistol', 15)

        def __init__(self, parent):
            super(CloudDancerBand.Dancer, self).__init__(parent, 'Cloud dancer Corsair', 20, [
                Gear('Mesh armour'), Gear('Eldar jetbike')
            ])
            self.BikeWeapon(self)
            self.up = self.Upgrade(self)
            self.wep = self.Weapon(self)
            self.traits = CharacterTraits(self)

        def check_rules(self):
            super(CloudDancerBand.Dancer, self).check_rules()
            self.wep.used = self.wep.visible = self.up.any
            self.traits.used = self.traits.visible = self.traits.used and self.up.any

        def build_description(self):
            res = super(CloudDancerBand.Dancer, self).build_description()
            if self.up.any:
                res.name = 'Cloud Dancer Felarch'
                if not self.wep.any:
                    res.add(Gear('Brace of pistols'))
            else:
                res.add(Gear('Brace of pistols'))
            return res

        @ListSubUnit.count_gear
        def is_felarch(self):
            return self.up.any

    class Grenades(OptionsList):
        def __init__(self, parent, name='Grenades'):
            super(CloudDancerBand.Grenades, self).__init__(parent, name)
            self.variant('Haywire grenades', 25)
            self.variant('Tanglefield grenades', 10)

    def __init__(self, parent):
        super(CloudDancerBand, self).__init__(parent)
        self.Grenades(self)
        self.models = UnitList(self, self.Dancer, 3, 10)

    def get_count(self):
        return self.models.count

    def check_rules(self):
        super(CloudDancerBand, self).check_rules()
        felarch_count = sum(u.is_felarch() for u in self.models.units)
        if felarch_count > 3:
            self.error('No more than 3 Cloud Dancers may be upgraded to Felarchs')


class GhostwalkerBand(Unit):
    type_name = u'Corsair Ghostwalker Band'
    type_id = 'ghostwalker_v2'
    imperial_Armour = True

    class Packs(OptionsList):
        def __init__(self, parent):
            super(GhostwalkerBand.Packs, self).__init__(parent, 'Options')
            self.variant('Corsair Jet Packs', 5, per_model=True)

    class Special(OptionsList):
        def __init__(self, parent):
            super(GhostwalkerBand.Special, self).__init__(parent, 'Special weapon', limit=1)
            self.variant('Flamer', 5)
            self.variant('Fusion gun', 10)
            self.variant('Shredder', 5)
            self.variant('Blaster', 10)

    def __init__(self, parent):
        super(GhostwalkerBand, self).__init__(parent)
        self.models = Count(self, 'Corsair Ghostwalkers', 5, 10, 12, per_model=True)
        self.rifles = Count(self, 'Eldar longrifle', 0, 5, 1)
        self.ccw = Count(self, 'Close combat and a brace of pistols', 0, 5, 0)
        self.special = [self.Special(self) for q in range(2)]
        self.packs = self.Packs(self)
        self.grenades = CloudDancerBand.Grenades(self)

    def get_count(self):
        return self.models.cur

    def check_rules(self):
        super(GhostwalkerBand, self).check_rules()
        self.special[1].used = self.special[1].visible = self.models.cur >= 10
        spec_cnt = sum(ol.any for ol in self.special if ol.used)
        Count.norm_counts(0, self.models.cur - spec_cnt, [self.rifles, self.ccw])

    def build_points(self):
        res = super(GhostwalkerBand, self).build_points()
        res += self.packs.points * (self.count - 1)
        return res

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        res.add(self.grenades.description)
        model = UnitDescription(name='Corsair Ghostwalker', points=12,
                                options=[Gear('Shadowwave grenades'),
                                         Gear('Plasma grenades'),
                                         Gear('Mesh armour')])
        model.add(self.packs.description).add_points(self.packs.points)
        spec_cnt = sum(ol.any for ol in self.special if ol.used)
        def_cnt = self.count - spec_cnt - self.rifles.cur - self.ccw.cur
        if def_cnt:
            res.add(model.clone().add(Gear('Lasblaster')).set_count(def_cnt))
        if self.rifles.cur:
            res.add(model.clone().add(Gear('Eldar longrifle'))
                    .add_points(1).set_count(self.rifles.cur))
        if self.ccw.cur:
            res.add(model.clone().add(Gear('Close combat weapon'))
                    .add(Gear('Brace of pistols')).set_count(self.ccw.cur))
        for spec in self.special:
            if spec.used and spec.any:
                res.add(model.clone().add(spec.description).add_points(spec.points))
        return res


class ReaverBand(Unit):
    type_name = u'Corsair Reaver Band'
    type_id = 'reaver_v2'
    imperial_armour = True

    class Felarch(Unit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(ReaverBand.Felarch.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Lasblaster')
                self.variant('Splinter rifle')
                self.variant('Shuriken catapult')
                self.variant('Close combat weapon and a brace of pistols',
                             gear=[Gear('Close combat weapon'),
                                   Gear('Brace of pistols')])

        class AlternateWeapon(OptionsList):
            def __init__(self, parent):
                super(ReaverBand.Felarch.AlternateWeapon, self).__init__(parent, 'Alternate weapons', limit=2)
                self.variant('Close combat weapon')
                self.variant('Power weapon', 15)
                self.variant('Venom blade', 10)
                self.variant('Blast pistol', 20)
                self.variant('Dissonance pistol', 10)

        def __init__(self, parent):
            super(ReaverBand.Felarch, self).__init__(parent, 'Corsair Felarch', 15, gear=[
                Gear('Shadowwave grenades'), Gear('Plasma grenades')
            ])
            self.default = self.Weapon(self)
            self.alternate = self.AlternateWeapon(self)
            CharacterTraits(self)

        def check_rules(self):
            super(ReaverBand.Felarch, self).check_rules()
            self.default.used = self.default.visible = not self.alternate.any

        def build_description(self):
            res = super(ReaverBand.Felarch, self).build_description()
            if self.parent.parent.parent.options1.armour.value:
                res.add(Gear('Heavy mesh armour')).add_points(5)
            else:
                res.add(Gear('Mesh armour'))
            if self.parent.parent.parent.options1.packs.value:
                res.add(Gear('Corsair jet pack')).add_points(5)
            res.add(self.parent.parent.parent.options2.description)
            return res

    class Options1(OptionsList):
        def __init__(self, parent):
            super(ReaverBand.Options1, self).__init__(parent, 'Options')
            self.packs = self.variant('Corsair jet packs', 5, per_model=True)
            self.armour = self.variant('Heavy mesh armour', 5, per_model=True)

    class Options2(CloudDancerBand.Grenades):
        def __init__(self, parent):
            super(ReaverBand.Options2, self).__init__(parent, '')
            self.variant('Void hardened armour', 10)

    class SpecialWeapon(OneOf):
        def __init__(self, parent, index):
            super(ReaverBand.SpecialWeapon, self).__init__(parent, 'Special Weapon {}' % index)
            self.none = self.variant('(None)', gear=[])
            self.variant('Flamer', 5)
            self.variant('Fusion gun', 10)
            self.variant('Shredder', 5)
            self.variant('Blaster', 10)

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(ReaverBand.Leader, self).__init__(parent, 'Leader')
            SubUnit(self, ReaverBand.Felarch(parent=parent))

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(ReaverBand.Transport, self).__init__(parent, 'Transport', limit=1)
            self.venom = SubUnit(self, Venom(parent=None))
            self.falcon = SubUnit(self, Falcon(parent=None))

        def check_rules(self):
            super(ReaverBand.Transport, self).check_rules()
            self.used = self.visible = not self.parent.options1.packs.value
            if self.venom.visible and self.parent.get_count() > 5:
                self.parent.error("Corsair Reaver Band must be no more then 5 in number to take a Venom")
            if self.falcon.visible and self.parent.get_count() <= 5:
                self.parent.error("Corsair Reaver Band must be more then 5 in number to take a Falcon")

    def __init__(self, parent):
        super(ReaverBand, self).__init__(parent)
        self.models = Count(self, 'Corsair', 5, 20, 10, True)
        self.boss = self.Leader(self)
        self.splinters = Count(self, 'Splinter rifle', 0, 5)
        self.shuriken = Count(self, 'Shuriken catapult', 0, 5)
        self.ccw = Count(self, 'Close combat weapon and a brace of pistols', 0, 5)
        self.specials = [
            Count(self, 'Flamer', 0, 2, 5),
            Count(self, 'Fusion gun', 0, 2, 10),
            Count(self, 'Shredder', 0, 2, 5),
            Count(self, 'Blaster', 0, 2, 10)
        ]
        self.options1 = self.Options1(self)
        self.options2 = self.Options2(self)
        self.transport = self.Transport(self)

    def get_count(self):
        return self.models.cur + self.boss.count

    def build_points(self):
        return super(ReaverBand, self).build_points() + self.options1.points * (self.count - 1)

    def check_rules(self):
        super(ReaverBand, self).check_rules()
        spec_cnt = 2 * ((self.models.cur + self.boss.count) / 5)
        Count.norm_counts(0, spec_cnt, self.specials)
        spec_cnt = sum(spec.cur for spec in self.specials)
        self.models.min = 5 - self.boss.count
        self.models.max = 20 - self.boss.count
        Count.norm_counts(0, self.models.cur - spec_cnt, [self.splinters, self.shuriken, self.ccw])

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        res.add(self.options2.description)
        res.add(self.boss.description)
        model = UnitDescription(name='Corsair', points=10, options=[
            Gear('Shadowwave grenades'), Gear('Plasma grenades')])
        model.add(self.options1.description).add_points(self.options1.points)
        if not self.options1.armour.value:
            model.add(Gear('Mesh armour'))
        spec_cnt = sum(spec.cur for spec in self.specials)
        blastcount = self.models.cur - spec_cnt - self.shuriken.cur - self.splinters.cur - self.ccw.cur
        if blastcount:
            res.add(model.clone().add(Gear('Lasblaster')).set_count(blastcount))
        if self.shuriken.cur:
            res.add(model.clone().add(Gear('Shuriken catapult')).set_count(self.shuriken.cur))
        if self.splinters.cur:
            res.add(model.clone().add(Gear('Splinter rifle')).set_count(self.splinters.cur))
        if self.ccw.cur:
            res.add(model.clone().add([Gear('Close combat weapon'), Gear('Brace of pistols')]).set_count(self.ccw.cur))
        for spec in self.specials:
            if spec.cur:
                res.add(model.clone().add(spec.gear.clone()).add_points(spec.option_points).set_count(spec.cur))
        res.add(self.transport.description)
        return res

    def build_statistics(self):
        return self.note_transport(super(ReaverBand, self).build_statistics())
