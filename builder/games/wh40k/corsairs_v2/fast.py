__author__ = 'dvarh'
from builder.core2 import Gear, OptionsList, UnitList,\
    ListSubUnit, Count, UnitDescription, OneOf
from builder.games.wh40k.unit import Unit


class VyperSquadron(Unit):
    type_id = 'vypers_v2'
    type_name = u'Corsair Vyper Squadron'
    imperial_armour = True

    class Vyper(ListSubUnit):
        name = 'Vyper'
        base_points = 40

        class TopWeapon(OneOf):
            def __init__(self, parent):
                super(VyperSquadron.Vyper.TopWeapon, self).__init__(parent, 'Top Weapon')
                self.variant('Shuriken cannon', 0)
                self.variant('Splinter cannon', 0)
                self.variant('Scatter laser', 0)
                self.variant('Starcannon', 5)
                self.variant('Bright lance', 5)
                self.variant('Dark lance', 5)
                self.variant('Eldar missile launcher', 15)

        class BottomWeapon(OneOf):
            def __init__(self, parent):
                super(VyperSquadron.Vyper.BottomWeapon, self).__init__(parent, 'Bottom Weapon')
                self.variant('Twin-linked lasblaster', 0)
                self.variant('Twin-linked splinter rifles', 0)
                self.variant('Twin-linked shuriken catapults', 0)
                self.variant('Splinter cannon')
                self.variant('Shuriken cannon', 20)

        def __init__(self, parent):
            super(VyperSquadron.Vyper, self).__init__(parent, self.name, self.base_points)
            self.topwep = self.TopWeapon(self)
            self.bottomwep = self.BottomWeapon(self)

        def build_description(self):
            desc = super(VyperSquadron.Vyper, self).build_description()
            desc.add(self.root_unit.opt.description)
            desc.add_points(self.root_unit.opt.points)
            return desc

    class Options(OptionsList):
        def __init__(self, parent):
            super(VyperSquadron.Options, self).__init__(parent, 'Options')
            self.variant('Corsair void burners', 5, per_model=True)
            self.variant('Corsair kinetic shroud', 15, per_model=True)
            self.variant('Star engines', 15, per_model=True)
            self.variant('Chainsnares', 5, per_model=True)

    def __init__(self, parent):
        super(VyperSquadron, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.Vyper, 1, 6)
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.count

    def build_points(self):
        return super(VyperSquadron, self).build_points()\
            + self.opt.points * (self.models.count - 1)

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        res.add(self.models.description)
        return res


class Nightwing(Unit):
    type_name = u'Corsair Nightwing'
    type_id = 'nightwing_v2'
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(Nightwing.Options, self).__init__(parent, 'Options')
            self.variant('Corsair kinetic shroud', 15, per_model=True)

    def __init__(self, parent):
        super(Nightwing, self).__init__(parent)
        self.models = Count(self, 'Nightwing', 1, 2, 125, True,
                            gear=UnitDescription('Nightwing', 125, options=[
                                Gear('Shuriken cannon', count=2),
                                Gear('Bright lance', count=2)]))
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.cur

    def build_points(self):
        return self.models.points + self.opt.points * self.models.cur

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        models = self.models.description[0]
        models.add(self.opt.description).add_points(self.opt.points)
        res.add(models)
        return res


class Phoenix(Unit):
    type_name = u'Corsair Phoenix'
    type_id = 'phoenix_v2'
    imperial_armour = True

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Phoenix.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Pulse laser')
            self.variant('Twin-linked bright lance')
            self.variant('Twin-linked starcannon')

    class Missiles(OneOf):
        def __init__(self, parent):
            super(Phoenix.Missiles, self).__init__(parent, 'Missile launchers')
            self.variant('Phoenix missile launchers',
                         gear=[Gear('Phoenix missile launcher', count=2)])
            self.variant('Nightfire missile launchers', 10, per_model=True,
                         gear=[Gear('Nightfire missile launcher', count=2)])

    def __init__(self, parent):
        super(Phoenix, self).__init__(parent)
        self.models = Count(self, 'Phoenix', 1, 2, 205, True,
                            gear=UnitDescription('Phoenix', 205, options=[
                                Gear('Shuriken cannon', count=2)]))
        self.wep = self.Weapon(self)
        self.mis = self.Missiles(self)
        self.opt = Nightwing.Options(self)

    def get_count(self):
        return self.models.cur

    def build_points(self):
        return self.models.points +\
            (self.opt.points + self.mis.points) * self.models.cur

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        models = self.models.description[0]
        models.add(self.wep.description)
        models.add(self.mis.description).add_points(self.mis.points)
        models.add(self.opt.description).add_points(self.opt.points)
        res.add(models)
        return res


class HornetSquadron(Unit):
    type_name = u'Corsair Hornet Squadron'
    type_id = 'hornet_v2'
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(HornetSquadron.Options, self).__init__(parent, 'Options')
            self.variant('Corsair kinetic shroud', 15, per_model=True)
            self.variant('Chain snares', 5, per_model=True)
            self.variant('Corsair void burners', 5, per_model=True)

    class Hornet(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent, has_name=True):
                super(HornetSquadron.Hornet.Weapon, self).__init__(
                    parent, 'Weapon' if has_name else '')
                self.variant('Shuriken cannon')
                self.variant('Scatter laser')
                self.variant('Splinter cannon')
                self.variant('Eldar missile launcher', 15)
                self.variant('Starcannon', 5)
                self.variant('Bright lance', 5)
                self.variant('Pulse laser', 5)

        def __init__(self, parent):
            super(HornetSquadron.Hornet, self).__init__(parent, 'Hornet', 70,
                                                        gear=[Gear('Star engines')])
            self.Weapon(self)
            self.Weapon(self, False)

        def build_description(self):
            res = super(HornetSquadron.Hornet, self).build_description()
            res.add(self.root_unit.opt.description)\
               .add_points(self.root_unit.opt.points)
            return res

    def __init__(self, parent):
        super(HornetSquadron, self).__init__(parent)
        self.models = UnitList(self, self.Hornet, 1, 5)
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.count

    def build_points(self):
        return super(HornetSquadron, self).build_points()\
            + self.opt.points * (self.count - 1)

    def build_description(self):
        res = UnitDescription(name=self.name, points=self.points,
                              count=self.get_count())
        res.add(self.models.description)
        return res
