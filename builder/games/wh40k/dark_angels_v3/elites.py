__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'

from builder.core2 import *
from transport import TerminatorTransport, Transport, DropPod
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle
from builder.games.wh40k.imperial_armour.volume2.transport import LuciusDropPod, IATransportedUnit, IATransport
from armory import Weapon, Special, Heavy, Standards
from builder.games.wh40k.roster import Unit


class CompanyVeteransSquad(IATransportedUnit):
    type_name = u'Company Veterans Squad'
    type_id = 'company_veterans_squad_v3'

    class WeaponHeavy(Heavy, Special, Weapon):
        def __init__(self, parent, name):
            super(CompanyVeteransSquad.WeaponHeavy, self).__init__(parent, name=name, bolt_gun=True, melee=True,
                                                                   ranged=True)

    class Options(OptionsList):
        def __init__(self, parent):
            super(CompanyVeteransSquad.Options, self).__init__(parent=parent, name='Options')
            self.meltabombs = self.variant('Melta bombs', 5)
            self.combatshield = self.variant('Combat shield', 5)
            self.stormshield = self.variant('Storm shield', 10)

        def check_rules(self):
            super(CompanyVeteransSquad.Options, self).check_rules()
            self.process_limit([self.combatshield, self.stormshield], 1)

    model_points = 18

    class Sergeant(Unit):
        model_name = 'Veteran Sergeant'

        def __init__(self, parent):
            super(CompanyVeteransSquad.Sergeant, self).__init__(
                parent=parent, name=self.model_name,
                points=CompanyVeteransSquad.model_points, gear=[
                    Gear('Frag grenades'),
                    Gear('Krak grenades')
                ])
            self.wep1 = Weapon(self, 'Weapon', bolt_gun=True, melee=True, ranged=True)
            self.wep2 = Weapon(self, 'Weapon', pistol=True, melee=True, ranged=True)
            self.opt = CompanyVeteransSquad.Options(parent=self)

    class Veteran(ListSubUnit):
        model_name = 'Veteran'

        def __init__(self, parent):
            super(CompanyVeteransSquad.Veteran, self).__init__(
                parent=parent, name=self.model_name,
                points=CompanyVeteransSquad.model_points, gear=[
                    # Gear('Power armor'),
                    Gear('Frag grenades'),
                    Gear('Krak grenades')
                ])
            self.wep1 = CompanyVeteransSquad.WeaponHeavy(parent=self, name='Weapon')
            self.wep2 = Weapon(self, '', pistol=True, melee=True, ranged=True)
            self.opt = CompanyVeteransSquad.Options(parent=self)

        @ListSubUnit.count_gear
        def count_special(self):
            return self.wep1.is_special()

        @ListSubUnit.count_gear
        def count_heavy(self):
            return self.wep1.is_heavy()

    def __init__(self, parent):
        super(CompanyVeteransSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=CompanyVeteransSquad.Sergeant(parent=None))
        self.vets = UnitList(parent=self, unit_class=CompanyVeteransSquad.Veteran, min_limit=4, max_limit=9)
        self.transport = Transport(parent=self)

    def check_rules(self):
        spec_limit = int(self.get_count() / 5)
        spec_total = sum(u.count_special() for u in self.vets.units)
        if spec_limit < spec_total:
            self.error('Veterans may take only {0} special weapon (taken {1})'.format(spec_limit, spec_total))
        heavy_total = sum(u.count_heavy() for u in self.vets.units)
        if heavy_total > 1:
            self.error('Veterans may take only one heavy weapon (taken {0})'.format(heavy_total))

    def get_count(self):
        return self.vets.count + 1


class CommandSquad(Unit):
    type_id = 'command_squad_v3'
    type_name = u'Command Squad'

    model_points = 18

    class Veteran(ListSubUnit):
        class Weapon(Special, Weapon):
            def __init__(self, parent, name, **kwargs):
                super(CommandSquad.Veteran.Weapon, self).__init__(parent, name, **kwargs)

        class Options(OptionsList):
            def __init__(self, parent):
                super(CommandSquad.Veteran.Options, self).__init__(parent=parent, name='Options')

                self.mbomb = self.variant('Melta bombs', 5)
                self.ss = self.variant('Storm Shield', 10)
                self.champ = self.variant('Company Champion', 15, gear=[
                    Gear('Blade of Caliban'), Gear('Combat shield')
                ])
                self.apoth = self.variant('Apothecary', 15, gear=[
                    Gear('Bolt pistol'), Gear('Chainsword'), Gear('Narthecium')
                ])

            def check_rules(self):
                super(CommandSquad.Veteran.Options, self).check_rules()
                self.process_limit([self.champ, self.apoth], 1)
                character = not (self.champ.value or self.apoth.value)
                self.mbomb.used = self.mbomb.visible = self.ss.used = self.ss.visible = character

        def __init__(self, parent):
            super(CommandSquad.Veteran, self).__init__(
                parent=parent, name='Veteran', points=CommandSquad.model_points,
                gear=[
                    # Gear('Power Armour'),
                    Gear('Frag grenades'),
                    Gear('Krak grenades')
                ])

            self.weapon1 = CommandSquad.Veteran.Weapon(self, 'Weapon', melee=True, ranged=True)
            self.weapon2 = CommandSquad.Veteran.Weapon(self, '', pistol=True, melee=True, ranged=True)
            self.opt = CommandSquad.Veteran.Options(self)
            self.banners = Standards(self)

        def check_rules(self):
            super(CommandSquad.Veteran, self).check_rules()
            if self.weapon1.is_special() and self.weapon2.is_special():
                self.error("Only one special weapon may be carried by model")
            for opt in [self.weapon1, self.weapon2, self.banners]:
                opt.used = opt.visible = not (self.opt.champ.value or self.opt.apoth.value)

        def build_description(self):
            desc = super(CommandSquad.Veteran, self).build_description()
            if self.opt.champ.value:
                desc.name = 'Company Champion'
            if self.opt.apoth.value:
                desc.name = 'Apothecary'
            return desc

        @ListSubUnit.count_gear
        def count_medics(self):
            return self.opt.apoth.value

        @ListSubUnit.count_gear
        def count_duelists(self):
            return self.opt.champ.value

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.banners.any

    def __init__(self, parent):
        super(CommandSquad, self).__init__(parent=parent)
        self.veterans = UnitList(self, self.Veteran, min_limit=5, max_limit=5, start_value=5)
        self.transport = Transport(self)

    def get_count(self):
        return self.veterans.count

    def check_rules(self):
        if self.get_count() != 5:
            self.error('Command Squad must include 5 Veterans (include Apothecary and Company champion) '
                       '(taken: {})'.format(self.get_count()))
        flag = sum(c.count_banners() for c in self.veterans.units)
        if flag > 1:
            self.error('Only one Veteran can take a banner (taken: {})'.format(flag))
        flag = sum(c.count_medics() for c in self.veterans.units)
        if flag > 1:
            self.error('Only one Apothecary may be included in unit (taken: {})'.format(flag))
        flag = sum(c.count_duelists() for c in self.veterans.units)
        if flag > 1:
            self.error('Only one Company Champion may be included in unit (taken: {})'.format(flag))

    def get_unique_gear(self):
        return sum((unit.banners.get_unique() for unit in self.veterans.units), []) + self.transport.get_unique_gear()

    def build_statistics(self):
        return self.note_transport(super(CommandSquad, self).build_statistics())


class Dreadnoughts(IATransportedUnit):
    type_name = u'Dreadnoughts'
    type_id = 'dreadnoughts_v3'

    class Dreadnought(SpaceMarinesBaseVehicle):
        type_id = 'dreadnought_v3'
        model_points = 100
        model_name = 'Dreadnought'

        class Ranged(OneOf):
            def __init__(self, parent):
                super(Dreadnoughts.Dreadnought.Ranged, self).__init__(parent=parent, name='Ranged weapon')
                self.multimelta = self.variant('Multi-melta', 0)
                self.twinlinkedheavyflamer = self.variant('Twin-linked heavy flamer', 5)
                self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 5)
                self.twinlinkedautocannon = self.variant('Twin-linked autocannon', 5)
                self.plasmacannon = self.variant('Plasma cannon', 5)
                self.assaultcannon = self.variant('Assault cannon', 10)
                self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 15)

        class Melee(OneOf):
            def __init__(self, parent):
                super(Dreadnoughts.Dreadnought.Melee, self).__init__(parent=parent, name='')
                self.build_in = None

                self.powerfist = self.variant('Power fist', 0)
                self.missilelauncher = self.variant('Missile launcher', 10)
                self.twinlinkedautocannon = self.variant('Twin-linked autocannon', 15)

            def check_rules(self):
                if self.build_in:
                    self.build_in.visible = self.build_in.used = (self.cur == self.powerfist)

        class BuildIn(OneOf):
            def __init__(self, parent):
                super(Dreadnoughts.Dreadnought.BuildIn, self).__init__(parent=parent, name='')

                self.builtinstormbolter = self.variant('Built-in Storm bolter', 0, gear=Gear('Storm bolter'))
                self.builtinheavyflamer = self.variant('Built-in Heavy flamer', 10, gear=Gear('Heavy flamer'))

        class Options(OptionsList):
            def __init__(self, parent):
                super(Dreadnoughts.Dreadnought.Options, self).__init__(parent=parent, name='Options')
                self.extraarmour = self.variant('Extra armour', 10)

        def __init__(self, parent):
            super(Dreadnoughts.Dreadnought, self).__init__(parent=parent, points=self.model_points,
                                                           name=self.model_name,
                                                           gear=[
                                                               Gear('Smoke launchers'),
                                                               Gear('Searchlight'),
                                                           ]
                                                           )
            self.ranged = self.Ranged(self)
            self.melee = self.Melee(self)
            self.build_in = self.melee.build_in = self.BuildIn(self)
            self.opt = self.Options(self)

    class ListDreadnoughts(Dreadnought, ListSubUnit):
        pass

    class Transport(IATransport):
        def __init__(self, parent):
            super(Dreadnoughts.Transport, self).__init__(parent=parent, name='Transport')
            self.drop = SubUnit(self, DropPod(parent=parent))
            self.lucius = SubUnit(self, LuciusDropPod(parent=None))
            self.models += [self.drop]
            self.ia_models += [self.lucius]

    def get_dred_type(self):
        return self.ListDreadnoughts

    def __init__(self, parent):
        super(Dreadnoughts, self).__init__(parent)
        self.models = UnitList(self, self.get_dred_type(), 1, 3)
        self.transport = self.Transport(self)

    def check_rules(self):
        super(Dreadnoughts, self).check_rules()
        self.transport.used = self.transport.visible = self.models.count == 1

    def get_count(self):
        return self.models.count

    def get_unique_gear(self):
        return super(Dreadnoughts, self).get_unique_gear() + \
               sum([u.get_unique_gear() * u.count for u in self.models.units], [])


class VenDreadnoughts(Dreadnoughts):
    type_name = u'Venerable Dreadnoughts'
    type_id = 'vendreadnoughts_v3'

    class VenDreadnought(Dreadnoughts.Dreadnought):
        model_points = 125
        model_name = 'Venerable Dreadnought'

    class ListVenDreads(VenDreadnought, ListSubUnit):
        pass

    def get_dred_type(self):
        return self.ListVenDreads


class DeathwingTerminatorSquad(IATransportedUnit):
    type_name = u'Deathwing Terminator Squad'
    type_id = 'deathwing_terminator_squad_v3'

    model_points = 200 / 5

    class Veteran(ListSubUnit):
        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(DeathwingTerminatorSquad.Veteran.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.power_fist = self.variant('Power fist', 0)
                self.chain_fist = self.variant('Chainfist', 5)

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(DeathwingTerminatorSquad.Veteran.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant('Storm bolter', 0)
                self.hf = self.variant('Heavy flamer', 10)
                self.pc = self.variant('Plasma Cannon', 15)
                self.ac = self.variant('Assault Cannon', 20)

            def enable_spec(self, value):
                self.hf.active = self.pc.active = self.ac.active = value

            def has_spec(self):
                return self.cur != self.bolt

        class Weapon(OptionsList):
            def __init__(self, parent):
                super(DeathwingTerminatorSquad.Veteran.Weapon, self).__init__(parent=parent, name='')
                self.cyc = self.variant('Cyclone missile launcher', 25)

                self.lc = self.variant('Pair of lightning claws', 0, gear=Gear('Lightning claw', count=2))
                self.thss = self.variant('Thunder hammer and storm shield', 10,
                                         gear=[Gear('Thunder hammer'), Gear('Storm shield')])

                self.both = [self.lc, self.thss]

            def has_both(self):
                return any(opt.value for opt in self.both)

            def check_rules(self):
                super(DeathwingTerminatorSquad.Veteran.Weapon, self).check_rules()
                self.process_limit(self.both, 1)

        def __init__(self, parent):
            super(DeathwingTerminatorSquad.Veteran, self).__init__(
                parent=parent, name='Deathwing Terminator', points=DeathwingTerminatorSquad.model_points,
                gear=[Gear('Terminator Armour')]
            )

            self.left_weapon = self.LeftWeapon(self)
            self.right_weapon = self.RightWeapon(self)
            self.weapon = self.Weapon(self)

        def check_rules(self):
            super(DeathwingTerminatorSquad.Veteran, self).check_rules()
            for wep in [self.right_weapon, self.left_weapon]:
                wep.visible = wep.used = not self.weapon.has_both()
            self.right_weapon.enable_spec(not self.weapon.cyc.value)
            self.weapon.cyc.active = not self.right_weapon.has_spec()

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.weapon.cyc.value or self.right_weapon.has_spec()

    class TerminatorSergeant(Unit):
        class Weapons(OneOf):
            def __init__(self, parent):
                super(DeathwingTerminatorSquad.TerminatorSergeant.Weapons, self).__init__(parent, 'Weapons')
                self.variant('Storm bolter and power sword', 0, gear=[Gear('Storm bolter'), Gear('Power sword')])
                self.variant('Two lightning claws', 0, gear=[Gear('Lightning claw', count=2)])
                self.variant('Thunder hammer and storm shield', 10, gear=[Gear('Thunder hammer'), Gear('Storm shield')])

        def __init__(self, parent):
            super(DeathwingTerminatorSquad.TerminatorSergeant, self).__init__(
                name='Deathwing Sergeant',
                parent=parent, points=DeathwingTerminatorSquad.model_points,
                gear=[Gear('Terminator Armour')],
            )
            self.Weapons(self)

    class SquadOptions(OptionsList):
        def __init__(self, parent):
            super(DeathwingTerminatorSquad.SquadOptions, self).__init__(parent, 'Options')
            self.variant('Perfidious Relic of the Unforgiven', 15)

    def __init__(self, parent):
        super(DeathwingTerminatorSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.TerminatorSergeant(parent=None))
        self.terms = UnitList(self, self.Veteran, min_limit=4, max_limit=9)
        self.opt = self.SquadOptions(self)
        self.transport = TerminatorTransport(self)

    def get_count(self):
        return self.terms.count + 1

    def check_rules(self):
        spec_limit = 1 if self.get_count() < 10 else 2
        spec = sum(c.has_spec() for c in self.terms.units)
        if spec > spec_limit:
            self.error('Deathwing Terminators may take only {0} heavy weapon (taken {1})'.format(spec_limit, spec))


class DeathwingCommandSquad(IATransportedUnit):
    type_name = u'Deathwing Command Squad'
    type_id = 'deathwing_command_squad_v3'

    model_points = 200 / 5

    class Veteran(ListSubUnit):

        class Promotions(OptionsList):
            def __init__(self, parent):
                super(DeathwingCommandSquad.Veteran.Promotions, self).__init__(parent, 'Promotions', limit=1)
                self.serg = self.variant('Deathwing Sergeant', 0, gear=[
                    Gear('Power sword'), Gear('Storm bolter')
                ])
                self.champ = self.variant('Deathwing Champion', 5, gear=[
                    Gear('Halberd of Caliban')
                ])
                self.medic = self.variant('Deathwing Apothecary', 5, gear=[
                    Gear('Narthecium'), Gear('Storm bolter')
                ])

        class Banners(OptionsList):
            def __init__(self, parent):
                super(DeathwingCommandSquad.Veteran.Banners, self).__init__(parent=parent, name='Banner', limit=1)

                self.com_banner = self.variant('Deathwing Company Banner', 20)
                self.rev_std = self.variant('Sacred Standard', 35)

        def __init__(self, parent):
            super(DeathwingCommandSquad.Veteran, self).__init__(
                parent=parent, name='Deathwing Terminator', points=DeathwingTerminatorSquad.model_points,
                gear=[Gear('Terminator Armour')]
            )

            self.left_weapon = DeathwingTerminatorSquad.Veteran.LeftWeapon(self)
            self.right_weapon = DeathwingTerminatorSquad.Veteran.RightWeapon(self)
            self.weapon = DeathwingTerminatorSquad.Veteran.Weapon(self)
            self.standard = self.Banners(self)
            self.promotion = self.Promotions(self)

        def build_description(self):
            desc = super(DeathwingCommandSquad.Veteran, self).build_description()
            if self.promotion.serg.value:
                desc.name = 'Deathwing Sergeant'
            if self.promotion.medic.value:
                desc.name = 'Deathwing Apothecary'
            if self.promotion.champ.value:
                self.name = 'Deathwing Champion'
            return desc

        def check_rules(self):
            super(DeathwingCommandSquad.Veteran, self).check_rules()
            for wep in [self.right_weapon, self.left_weapon]:
                wep.visible = wep.used = not (self.weapon.has_both() or self.promotion.any)
            self.right_weapon.enable_spec(not self.weapon.cyc.value)
            self.weapon.cyc.active = not self.right_weapon.has_spec()

            for opt in [self.weapon, self.standard]:
                opt.used = opt.visible = not self.promotion.any

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.weapon.cyc.value or self.right_weapon.has_spec()

        @ListSubUnit.count_gear
        def is_sergeant(self):
            return self.promotion.serg.value

        @ListSubUnit.count_gear
        def is_medic(self):
            return self.promotion.medic.value

        @ListSubUnit.count_gear
        def is_champion(self):
            return self.promotion.champ.value

        @ListSubUnit.count_gear
        def has_banner(self):
            return self.standard.any

    def __init__(self, parent):
        super(DeathwingCommandSquad, self).__init__(parent=parent)
        self.terms = UnitList(self, self.Veteran, min_limit=5, max_limit=5)
        self.opt = DeathwingTerminatorSquad.SquadOptions(self)
        self.transport = TerminatorTransport(self)

    def get_count(self):
        return self.terms.count

    def check_rules(self):
        if self.terms.count < 5:
            self.error('Deatheing comand squad must include 5 models')
        spec = sum(c.has_spec() for c in self.terms.units)
        if spec > 1:
            self.error('Deathwing Command may take only 1 heavy weapon (taken {0})'.format(spec))
        char = sum(c.is_sergeant() for c in self.terms.units)
        if char > 1:
            self.error('Only one model in Deathwing Command Squad can be sergeant')
        char = sum(c.is_medic() for c in self.terms.units)
        if char > 1:
            self.error('Only one model in Deathwing Command Squad can be apothecary')
        char = sum(c.is_champion() for c in self.terms.units)
        if char > 1:
            self.error('Only one model in Deathwing Command Squad can be champion')
        ban = sum(c.has_banner() for c in self.terms.units)
        if ban > 1:
            self.error('Only one model from Deathwing Command Squad can carry a banner')

    def get_unique_gear(self):
        uniq = super(DeathwingCommandSquad, self).get_unique_gear()
        if any(c.has_banner() for c in self.terms.units):
            uniq.append(Gear('Deathwing standard'))
        return uniq


class DeathwingKnights(IATransportedUnit):
    type_name = u'Deathwing Knights'
    type_id = 'deathwing_knights_v3'
    knights_points = 45
    master_points = 235 - 4 * knights_points

    def __init__(self, parent):
        super(DeathwingKnights, self).__init__(parent=parent, points=self.master_points, gear=[
            UnitDescription('Knight master', points=self.master_points, options=[
                Gear('Terminator Armour'), Gear('Flail of the Unforgiven'), Gear('Storm shield')
            ])
        ])
        self.terms = Count(self, name='Deathwing Knight', min_limit=4, max_limit=9, points=self.knights_points,
                           per_model=True, gear=UnitDescription('Deathwing Knight', points=self.knights_points,
                                                                options=[
                                                                    Gear('Terminator Armour'),
                                                                    Gear('Mace of absolution'), Gear('Storm shield')
                                                                ]))
        self.relic = DeathwingTerminatorSquad.SquadOptions(self)
        self.transport = TerminatorTransport(self)

    def get_count(self):
        return self.terms.cur + 1


class RavenwingCommandSquad(Unit):
    type_id = 'ravenwing_command_squad_v3'
    type_name = u'Ravenwing Command Squad'

    model_points = 40

    class Veteran(ListSubUnit):
        base_gear = [
            Gear('Power Armour'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Teleport Homer')
        ]

        class Weapon(OneOf):
            def __init__(self, parent):
                super(RavenwingCommandSquad.Veteran.Weapon, self).__init__(parent=parent, name='Weapon')

                self.pt = self.variant('Plasma talon', 0)
                self.rfl = self.variant('Ravenwing grenade launcher', 0)

        class Banners(OptionsList):
            def __init__(self, parent):
                super(RavenwingCommandSquad.Veteran.Banners, self).__init__(parent=parent, name='Banner')

                self.cb = self.variant('Ravenwing Company Banner', 20)
                self.revstd = self.variant('Sacred Standard', 35)

        class Promotions(OptionsList):
            def __init__(self, parent):
                super(RavenwingCommandSquad.Veteran.Promotions, self).__init__(parent, 'Promotions', limit=1)
                self.champ = self.variant('Ravenwing Champion', 5, gear=[
                    Gear('Blade of Caliban'), Gear('Plasma talon')
                ])
                self.medic = self.variant('Ravenwing Apothecary', 30, gear=[
                    Gear('Narthecium'), Gear('Plasma talon')
                ])

        def __init__(self, parent):
            super(RavenwingCommandSquad.Veteran, self).__init__(
                parent=parent, name='Ravenwing Black Knight',
                points=RavenwingCommandSquad.model_points,
                gear=self.base_gear)
            self.weapon = self.Weapon(self)
            self.banners = self.Banners(self)
            self.prom = self.Promotions(self)

        def check_rules(self):
            super(RavenwingCommandSquad.Veteran, self).check_rules()
            for opt in [self.weapon, self.banners]:
                opt.used = opt.visible = not self.prom.any

        def build_description(self):
            desc = super(RavenwingCommandSquad.Veteran, self).build_description()
            if self.prom.champ.value:
                desc.name = 'Ravenwing Champion'
            else:
                desc.add(Gear('Corvus hammer'))
                if self.prom.medic.value:
                    desc.name = 'Ravenwing Apothecary'

            return desc

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.banners.any

        @ListSubUnit.count_gear
        def count_grenade(self):
            return self.weapon.cur == self.weapon.rfl

        @ListSubUnit.count_gear
        def is_champion(self):
            return self.prom.champ.value

        @ListSubUnit.count_gear
        def is_medic(self):
            return self.prom.medic.value

    def __init__(self, parent):
        super(RavenwingCommandSquad, self).__init__(parent=parent)
        self.veterans = UnitList(self, self.Veteran, min_limit=3, max_limit=6, start_value=3)

    def get_count(self):
        return self.veterans.count

    def check_rules(self):

        flag = sum(c.count_banners() for c in self.veterans.units)
        if flag > 1:
            self.error('Only one Ravenwing Black Knight can take a banner (taken: {})'.format(flag))
        cnt = sum(c.count_grenade() for c in self.veterans.units)
        if cnt > 2:
            self.error('No more than 2 Ravenwing Black Knights may take grenade launchers (taken:{}'.format(cnt))
        char = sum(c.is_medic() for c in self.veterans.units)
        if char > 1:
            self.error('Only one model in Ravenwing Command Squad can be apothecary')
        char = sum(c.is_champion() for c in self.veterans.units)
        if char > 1:
            self.error('Only one model in Ravenwing Command Squad can be champion')

    def get_unique_gear(self):
        if any(unit.count_banners() for unit in self.veterans.units):
            return [Gear('Ravenwing Standard')]
