__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'

from builder.core2 import OptionsList, Gear, OneOf, Count, SubUnit, UnitDescription,\
    ListSubUnit, UnitList
from armory import Vehicle, Heavy
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle
from troops import TacticalSquad
from builder.games.wh40k.roster import Unit


class LandSpeederVengeance(SpaceMarinesBaseVehicle):
    type_name = "Ravenwing Land Speeder Vengeance"
    type_id = "land_speeder_vengeance_v3"

    class Weapon(OneOf):
        def __init__(self, parent):
            super(LandSpeederVengeance.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant('Heavy bolter', 0)
            self.assaultcannon = self.variant('Assault cannon', 15)

    def __init__(self, parent):
        super(LandSpeederVengeance, self).__init__(parent=parent, points=120, gear=[Gear('Plasma storm battery')],
                                                   tank=True)
        self.weapon = self.Weapon(self)


class Predators(Unit):
    type_name = "Predators"
    type_id = "predators_v3"

    class Predator(SpaceMarinesBaseVehicle):
        type_name = "Predator"
        type_id = "predator_v3"

        class Turret(OneOf):
            def __init__(self, parent):
                super(Predators.Predator.Turret, self).__init__(parent=parent, name='Turret')
                self.autocannon = self.variant('Autocannon', 0)
                self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 25)

        class Sponsons(OptionsList):
            def __init__(self, parent):
                super(Predators.Predator.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)
                self.sponsonswithheavybolters = self.variant('Sponsons with heavy bolters', 20,
                                                             gear=Gear('Heavy bolter', count=2))
                self.sponsonswithlascannons = self.variant('Sponsons with lascannons', 40,
                                                           gear=Gear('Lascannon', count=2))

        def __init__(self, parent):
            super(Predators.Predator, self).__init__(parent=parent, points=75, gear=[
                Gear('Smoke launchers'),
                Gear('Searchlight'),
            ], tank=True)
            self.turret = self.Turret(self)
            self.side = self.Sponsons(self)
            self.opt = Vehicle(self)

    class ListPredator(Predator, ListSubUnit):
        pass

    def __init__(self, parent):
        super(Predators, self).__init__(parent)
        self.models = UnitList(self, self.ListPredator, min_limit=1, max_limit=3)

    def get_count(self):
        return self.models.count

    def get_unique_gear(self):
        return sum([unit.get_unique_gear() * unit.count for unit in self.models.units], [])


class Whirlwinds(Unit):
    type_name = "Whirlwinds"
    type_id = "whirlwinds_v3"

    class Whirlwind(SpaceMarinesBaseVehicle):
        type_name = "Whirlwind"
        type_id = "whirlwind_v3"

        def __init__(self, parent):
            super(Whirlwinds.Whirlwind, self).__init__(parent=parent, points=65, gear=[
                Gear('Whirlwind multiple missile launcher'),
                Gear('Smoke launchers'),
                Gear('Searchlight'),
            ], tank=True)
            self.opt = Vehicle(self)

    class ListWhirlwind(Whirlwind, ListSubUnit):
        pass

    def __init__(self, parent):
        super(Whirlwinds, self).__init__(parent)
        self.models = UnitList(self, self.ListWhirlwind, min_limit=1, max_limit=3)

    def get_count(self):
        return self.models.count

    def get_unique_gear(self):
        return sum([unit.get_unique_gear() * unit.count for unit in self.models.units], [])


class Vindicators(Unit):
    type_name = "Vindicators"
    type_id = "vindicators_v3"

    class Vindicator(SpaceMarinesBaseVehicle):
        type_name = "Vindicator"
        type_id = "vindicator_v3"

        def __init__(self, parent):
            super(Vindicators.Vindicator, self).__init__(parent=parent, points=120, gear=[
                Gear('Demolisher cannon'),
                Gear('Storm bolter'),
                Gear('Smoke launchers'),
                Gear('Searchlight'),
            ], tank=True)
            self.opt = Vehicle(self, shield=True)

    class ListVindicator(Vindicator, ListSubUnit):
        pass

    def __init__(self, parent):
        super(Vindicators, self).__init__(parent)
        self.models = UnitList(self, self.ListVindicator, min_limit=1, max_limit=3)

    def get_count(self):
        return self.models.count

    def get_unique_gear(self):
        return sum([unit.get_unique_gear() * unit.count for unit in self.models.units], [])


class Devastators(Unit):
    type_name = u'Devastator Squad'
    type_id = 'devastators_v3'

    model_points = 14
    model_gear = [Gear('Bolt pistol'), Gear('Frag grenades'), Gear('Krak grenades')]

    class Sergeant(TacticalSquad.Sergeant):
        def __init__(self, parent):
            super(Devastators.Sergeant, self).__init__(parent)
            self.gear = TacticalSquad.model_gear + [Gear('Signum')]

    class Options(OptionsList):
        def __init__(self, parent):
            super(Devastators.Options, self).__init__(parent, 'Options')
            self.variant('Armorium cherub', 5)

    def __init__(self, parent):
        super(Devastators, self).__init__(parent=parent)

        from transport import Transport
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.heavy = [Heavy(self, bolt_gun=True, name='Heavy weapon' if i == 0 else '') for i in range(0, 4)]
        self.opt = self.Options(self)
        self.transport = Transport(parent=self)

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count(),
                               options=self.opt.description)
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=Devastators.model_gear,
            points=Devastators.model_points
        )
        count = self.marines.cur - 4 + sum(opt.cur == opt.boltgun for opt in self.heavy)
        for o in self.heavy:
            if o.cur != o.boltgun:
                desc.add_dup(marine.clone().add(o.description).add_points(o.points + o.flakk.points))

        marine.add(Gear('Boltgun'))
        marine.count = count
        desc.add(marine)
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.marines.cur + 1

    def build_statistics(self):
        return self.note_transport(super(Devastators, self).build_statistics())


from builder.games.wh40k.imperial_armour.dataslates.space_marines import BaseDeimos


class DeimosVindicator(BaseDeimos, SpaceMarinesBaseVehicle):
    def get_options(self):
        class Options(Vehicle):
            def __init__(self, parent):
                super(Options, self).__init__(parent, shield=True)
        return Options
