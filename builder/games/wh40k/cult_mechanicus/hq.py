__author__ = 'Ivan Truskov'

from builder.core2 import OneOf, Gear, OptionsList
from armory import SpecialIssue, Field, CadiaArcana, MechanicusOption, CadianWeaponArcana
from builder.games.wh40k.roster import Unit


class Dominus(Unit):
    type_id = 'dominus_v1'
    type_name = u'Tech-Priest Dominus'

    class Weapon1(OneOf, MechanicusOption):
        def __init__(self, parent):
            super(Dominus.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Volkite blaster', 0)
            self.variant('Eradication ray', 15 * self.freeflag)

    class Weapon2(OneOf, MechanicusOption):
        def __init__(self, parent):
            super(Dominus.Weapon2, self).__init__(parent, '')
            self.variant('Macrostubber', 0)
            self.variant('Phosphor serpenta', 5 * self.freeflag)

    class RelicWeapon1(CadianWeaponArcana, Weapon1):
        pass

    class RelicWeapon2(CadianWeaponArcana, Weapon2):
        pass

    def __init__(self, parent):
        super(Dominus, self).__init__(parent, points=105, gear=[
            Gear('Artificer armour'), Gear('Power axe'),
            Gear('Mechadendrite harness'), Gear('Scryerskull')
        ])
        self.weapons = [
            self.RelicWeapon1(self),
            self.RelicWeapon2(self)
        ]
        SpecialIssue(self)
        Field(self)
        self.arcana = CadiaArcana(self, dominus=True)

    def get_unique_gear(self):
        wep_rel = []
        for wep in self.weapons:
            if wep.cur == wep.qann:
                wep_rel += wep.description
        return self.arcana.description + wep_rel

    def check_rules(self):
        super(Dominus, self).check_rules()

        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')


class Techpriest(Unit):
    type_name = u'Tech-Priest Enginseer'
    type_id = 'cm_techpriestenginseer_v1'
    # wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Enginseer')

    def __init__(self, parent):
        super(Techpriest, self).__init__(parent=parent, points=40, gear=[
            Gear('Power armour'),
            Gear('Laspistol'),
            Gear('Power axe'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Servo-arm')
        ])
        self.Options(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Techpriest.Options, self).__init__(parent=parent, name='Options')
            self.variant('Melta bombs', 5)
            self.variant('Digital weapons', 10)
            self.variant('Refractor field', 10)


class Cawl(Unit):
    type_name = u'Belisarius Cawl, Archmagos Dominus'
    type_id = 'cawl_v1'

    def __init__(self, parent):
        super(Cawl, self).__init__(parent, 'Belisarius Cawl', points=200,
                                   gear=[Gear('Arc scourge'), Gear('Master-crafted power axe'),
                                         Gear('Mechadendrite Hive'), Gear('Scryerskull'),
                                         Gear('Refractor field'), Gear('Solar atomizer')],
                                   static=True, unique=True)
