__author__ = 'Ivan Truskov'

from builder.core2 import OneOf, OptionsList
from builder.games.wh40k.imperial_knight_v2.lords import MechanicusOption
from builder.games.wh40k.adepta_sororitas_v4.armory import CadianRelicsHoly, CadianWeaponArcana


class SpecialIssue(OptionsList, MechanicusOption):
    def __init__(self, parent):
        super(SpecialIssue, self).__init__(parent, 'Special issue wargear')
        self.variant('Digital weapons', 10 * self.freeflag)
        self.variant('Infoslave skull', 10 * self.freeflag)


class Field(OneOf, MechanicusOption):
    def __init__(self, parent):
        super(Field, self).__init__(parent, '')
        self.variant('Refractor field', 0)
        self.variant('Conversion field', 5 * self.freeflag)
        self.variant('Stasis field', 15 * self.freeflag)


class Arcana(OptionsList, MechanicusOption):
    def __init__(self, parent, dominus=False):
        super(Arcana, self).__init__(parent, 'Arcana Mechanicum',
                                     limit=1)
        self.variant('Anzion\'s Pseudogenetor', 15 * self.freeflag)
        self.variant('Mask of the Alpha Dominus', 15 * self.freeflag)
        if dominus:
            self.variant('The Scryerskull Perspicatus', 25 * self.freeflag)
            self.variant('Autocaduceus of the Arkhan Land', 30 * self.freeflag)
        self.variant('Raiment of the Technomartyr', 30 * self.freeflag)
        self.variant('Uncreator Gauntlet', 35 * self.freeflag)


class CadiaArcana(CadianRelicsHoly, Arcana):
    pass
