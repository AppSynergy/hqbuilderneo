__author__ = 'Denis Romanov'

from builder.core2 import *
from builder.core2.section import Section
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed,\
    Wh40kImperial, Wh40kKillTeam
from elites import TheDamned


class DamnedSection(Section):
    def __init__(self, parent):
        super(DamnedSection, self).__init__(parent, 'lod', 'Elites', min_limit=1, max_limit=4)
        UnitType(self, TheDamned)


class DamnedKillTeam(Wh40kKillTeam):
    army_id = 'damned_kt'
    army_name = 'Legion of the Damned'

    def __init__(self):
        super(DamnedKillTeam, self).__init__(
            None, DamnedSection(self), None
        )


class DamnedDetachment(Wh40kBase):
    army_id = 'damned_det'
    army_name = 'Spectral Host'

    def __init__(self):
        self.lod = DamnedSection(parent=self)
        super(DamnedDetachment, self).__init__(
            added=[self.lod]
        )


faction = 'Damned Legion'


class Damned(Wh40kImperial, Wh40k7ed):
    army_id = 'damned'
    army_name = 'Legion of the Damned'
    faction = faction

    def __init__(self):
        super(Damned, self).__init__([DamnedDetachment])


detachments = [DamnedDetachment]
