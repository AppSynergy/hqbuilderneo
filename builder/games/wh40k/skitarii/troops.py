__author__ = 'Ivan Truskov'
from builder.core2 import OptionsList, Count,\
    SubUnit, UnitDescription, Gear
from armory import Melee, SpecialIssue, Ranged, CadiaRelics,\
    Carbine, Special, Rifle, MechanicusOption, HolyWeaponRelic,\
    ArcanaWeaponRelic
from builder.games.wh40k.roster import Unit


class TroopOptions(OptionsList, MechanicusOption):
    def __init__(self, parent):
        super(TroopOptions, self).__init__(parent, 'Options', limit=1)
        self.variant('Enchanced data-tether', 5 * self.freeflag)
        self.variant('Omnispex', 10 * self.freeflag)


class Vanguard(Unit):
    type_name = u'Skitarii Vanguard'
    type_id = 'vanguard_v1'

    class Alpha(Unit):
        class Weapon(ArcanaWeaponRelic, Ranged, Carbine):
            pass

        class Weapon2(HolyWeaponRelic, Melee):
            pass

        def __init__(self, parent):
            super(Vanguard.Alpha, self).__init__(
                parent, 'Vanguard Alpha', 19, [Gear('Skitarii war plate')])
            self.rng = self.Weapon(self)
            self.mle = self.Weapon2(self)
            SpecialIssue(self)
            self.relics = CadiaRelics(self)

        def get_unique_gear(self):
            return self.relics.description + self.rng.get_unique()\
                + self.mle.get_unique()

        def check_rules(self):
            super(Vanguard.Alpha, self).check_rules()

            if len(self.get_unique_gear()) > 1:
                self.error('Model cannot carry more then one relic')

    class SpecialWeapon(Special, Carbine):
        pass

    def __init__(self, parent):
        super(Vanguard, self).__init__(parent, self.type_name)
        self.ldr = SubUnit(self, self.Alpha(parent=self))
        self.wars = Count(self, self.type_name, 4, 9, 9, True)
        self.opt = TroopOptions(self)
        self.spec = [self.SpecialWeapon(self) for i in range(0, 3)]

    def get_unique_gear(self):
        return self.ldr.unit.get_unique_gear()

    def get_count(self):
        return self.wars.cur + 1  # leader always present

    def check_rules(self):
        super(Vanguard, self).check_rules()
        self.spec[2].used = self.spec[2].visible = self.get_count() == 10

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.ldr.description)
        model = UnitDescription(name=self.type_name, points=9, options=[Gear('Skitarii war plate')])
        spec_count = sum([(o.used and not o.cur == o.carbine) for o in self.spec]) + (1 if self.opt.any else 0)
        desc.add(model.clone().add(Gear('Radium carbine')).set_count(self.wars.cur - spec_count))
        if self.opt.any:
            desc.add(model.clone().add(Gear('Radium carbine')).add(self.opt.description).add_points(self.opt.points))
        for g in self.spec:
            if g.used and not g.cur == g.carbine:
                desc.add_dup(model.clone().add(g.description).add_points(g.points))
        return desc


class Rangers(Unit):
    type_name = u'Skitarii Rangers'
    type_id = 'rangers_v1'

    class Alpha(Unit):
        class Weapon(ArcanaWeaponRelic, Ranged, Rifle):
            pass

        class Weapon2(HolyWeaponRelic, Melee):
            pass

        def __init__(self, parent):
            super(Rangers.Alpha, self).__init__(
                parent, 'Ranger Alpha', 21, [Gear('Skitarii war plate')])
            self.rng = self.Weapon(self)
            self.mle = self.Weapon2(self)
            SpecialIssue(self)
            self.relics = CadiaRelics(self)

        def get_unique_gear(self):
            return self.relics.description + self.mle.get_unique()\
                + self.rng.get_unique()

        def check_rules(self):
            super(Rangers.Alpha, self).check_rules()

            if len(self.get_unique_gear()) > 1:
                self.error('Model cannot carry more then one relic')

    class SpecialWeapon(Special, Rifle):
        pass

    def __init__(self, parent):
        super(Rangers, self).__init__(parent, self.type_name)
        self.ldr = SubUnit(self, self.Alpha(parent=self))
        self.wars = Count(self, 'Skitarii Ranger', 4, 9, 11, True)
        self.opt = TroopOptions(self)
        self.spec = [self.SpecialWeapon(self) for i in range(0, 3)]

    def get_unique_gear(self):
        return self.ldr.unit.get_unique_gear()

    def get_count(self):
        return self.wars.cur + 1  # leader always present

    def check_rules(self):
        super(Rangers, self).check_rules()
        self.spec[2].used = self.spec[2].visible = self.get_count() == 10

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.ldr.description)
        model = UnitDescription(name='Skitarii Ranger', points=11, options=[Gear('Skitarii war plate')])
        spec_count = sum([(o.used and not o.cur == o.rifle) for o in self.spec]) + (1 if self.opt.any else 0)
        desc.add(model.clone().add(Gear('Galvanic rifle')).set_count(self.wars.cur - spec_count))
        if self.opt.any:
            desc.add(model.clone().add(Gear('Galvanic rifle')).add(self.opt.description).add_points(self.opt.points))
        for g in self.spec:
            if g.used and not g.cur == g.rifle:
                desc.add_dup(model.clone().add(g.description).add_points(g.points))
        return desc
