__author__ = 'Ivan Truskov'
__summary__ = 'Codex Khorne Daemonkin 7th edition'

from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kImperial, Wh40kKillTeam,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, UnitType, Detachment, Formation, FlyerSection
from builder.games.wh40k.fortifications import Fort
from hq import Lord, DaemonPrince, Herald, BloodThrone, Skulltaker,\
    FuryThirster, RageThirster, WrathThirster
from troops import ChaosCultists, ChaosSpaceMarines, Bloodletters,\
    Berzerks
from elites import Possessed, Terminators, Bloodcrushers
from fast import Spawn, Rhino, Bikers, Raptors, Hounds, Talons, Heldrake,\
    Heldrakes
from heavy import LandRaider, Forgefiend, Maulerfiend, Defiler,\
    Grinder, Helbrute, ScullCannon

from builder.games.wh40k.escalation.chaos import LordOfSkulls,\
    LordsOfWar as BaseLordsOfWar
from builder.games.wh40k.imperial_armour.dataslates import KhorneKnights,\
    Kytan


class HQ(HQSection):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        UnitType(self, Lord)
        UnitType(self, DaemonPrince)
        UnitType(self, Herald)
        UnitType(self, BloodThrone)
        UnitType(self, Skulltaker)
        UnitType(self, FuryThirster)
        UnitType(self, RageThirster)
        UnitType(self, WrathThirster)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, ChaosCultists)
        UnitType(self, ChaosSpaceMarines)
        UnitType(self, Berzerks)
        UnitType(self, Bloodletters)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        UnitType(self, Possessed)
        UnitType(self, Terminators)
        UnitType(self, Bloodcrushers)


class FastAttack(FastSection):
    def __init__(self, parent):
        super(FastAttack, self).__init__(parent)
        UnitType(self, Spawn)
        UnitType(self, Rhino)
        UnitType(self, Bikers)
        UnitType(self, Raptors)
        UnitType(self, Hounds)
        UnitType(self, Talons)
        drake = UnitType(self, Heldrake)
        drake.visible = False
        UnitType(self, Heldrakes)


class HeavySupport(HeavySection):
    def __init__(self, parent):
        super(HeavySupport, self).__init__(parent)
        UnitType(self, LandRaider)
        UnitType(self, Forgefiend)
        UnitType(self, Maulerfiend)
        UnitType(self, Defiler)
        UnitType(self, Grinder)
        UnitType(self, Helbrute)
        UnitType(self, ScullCannon)


class LordsOfWar(KhorneKnights, BaseLordsOfWar):
    pass


class Fliers(FlyerSection):
    def __init__(self, parent):
        super(Fliers, self).__init__(parent, [Heldrakes])


class Slaughtercult(Formation):
    army_id = 'khorne_daemonkin_slaughtercult'
    army_name = 'Slaughtercult'

    def __init__(self):
        super(Slaughtercult, self).__init__()
        lord = UnitType(self, Lord)
        prince = UnitType(self, DaemonPrince)
        herald = UnitType(self, Herald)
        throne = UnitType(self, BloodThrone)
        fury = UnitType(self, FuryThirster)
        skully = UnitType(self, Skulltaker)
        self.add_type_restriction([lord, prince, herald, throne, fury, skully], 1, 1)
        marines = UnitType(self, ChaosSpaceMarines)
        zerkers = UnitType(self, Berzerks)
        letters = UnitType(self, Bloodletters)
        self.add_type_restriction([marines, zerkers, letters], 2, 8)
        UnitType(self, Possessed, min_limit=1, max_limit=4)
        UnitType(self, ChaosCultists, min_limit=0, max_limit=2)
        UnitType(self, Spawn, min_limit=0, max_limit=2)


class BrazenOnslaught(Formation):
    army_id = 'khorne_daemonkin_brazen'
    army_name = 'Brazen Onslaught'

    def __init__(self):
        super(BrazenOnslaught, self).__init__()
        UnitType(self, Terminators, min_limit=1, max_limit=4)
        UnitType(self, Bloodcrushers, min_limit=2, max_limit=4)


class Bloodstorm(Formation):
    army_id = 'khorne_daemonkin_bloodstorm'
    army_name = 'Khorne\'s Bloodstorm'

    def __init__(self):
        super(Bloodstorm, self).__init__()
        UnitType(self, Raptors, min_limit=2, max_limit=4)
        UnitType(self, Talons, min_limit=1, max_limit=4)
        UnitType(self, Heldrake, max_limit=1)


class Gorepack(Formation):
    army_id = 'khorne_daemonkin_gorepack'
    army_name = 'Gorepack'

    def __init__(self):
        super(Gorepack, self).__init__()
        UnitType(self, Bikers, min_limit=2, max_limit=4)
        UnitType(self, Hounds, min_limit=1, max_limit=4)


class Cohort(Formation):
    army_id = 'khorne_daemonkin_Cohort'
    army_name = 'Charnel Cohort'

    def __init__(self):
        super(Cohort, self).__init__()
        prince = UnitType(self, DaemonPrince)
        herald = UnitType(self, Herald)
        throne = UnitType(self, BloodThrone)
        skully = UnitType(self, Skulltaker)
        self.add_type_restriction([prince, herald, throne, skully], 1, 1)
        UnitType(self, Bloodletters, min_limit=2, max_limit=8)
        UnitType(self, Hounds, min_limit=1, max_limit=4)
        UnitType(self, Bloodcrushers, min_limit=1, max_limit=4)
        UnitType(self, ScullCannon, min_limit=0, max_limit=4)


class KhorneDaemonkinBase(Wh40kBase):
    army_id = 'khorne_daaemonkin_v1_base'
    army_name = 'Khorne Daemonkin'

    def __init__(self):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        super(KhorneDaemonkinBase, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self)
        )


class BloodHost(Wh40k7ed):
    army_id = 'khorne_daemonkin_bloodhost'
    army_name = 'Blood Host Detachment'

    class LordOfSlaughter(Formation):
        army_name = 'Lord of Slaughter'
        army_id = 'khornce_daemonkin_lord'

        def __init__(self):
            super(BloodHost.LordOfSlaughter, self).__init__()
            fury = UnitType(self, FuryThirster)
            rage = UnitType(self, RageThirster)
            wrath = UnitType(self, WrathThirster)
            lord = UnitType(self, LordOfSkulls)
            kytan = UnitType(self, Kytan)
            self.add_type_restriction([fury, rage, wrath, lord, kytan], 1, 1)

    class WarEngine(Formation):
        army_name = 'War Engine'
        army_id = 'khornce_daemonkin_engine'

        def __init__(self):
            super(BloodHost.WarEngine, self).__init__()
            brute = UnitType(self, Helbrute)
            crabe = UnitType(self, Defiler)
            crabe2 = UnitType(self, Grinder)
            forge = UnitType(self, Forgefiend)
            mauler = UnitType(self, Maulerfiend)
            lord = UnitType(self, LordOfSkulls)
            kytan = UnitType(self, Kytan)
            self.add_type_restriction([brute, crabe, crabe2, forge, mauler, lord, kytan], 1, 1)

    class Core(Detachment):
        def __init__(self, parent):
            super(BloodHost.Core, self).__init__(parent, 'core', 'Core',
                                                 [Slaughtercult], 1, None)

    class Command(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(BloodHost.Command, self).__init__(parent, 'command', 'Command',
                                                    [BloodHost.LordOfSlaughter], 0, 1)

        def check_limits(self):
            self.max = max([1, len(self.core.units)])
            return super(BloodHost.Command, self).check_limits()

    class Auxilary(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(BloodHost.Auxilary, self).__init__(parent, 'auxilary', 'Auxilary',
                                                    [BrazenOnslaught,
                                                     Cohort,
                                                     Bloodstorm,
                                                     BloodHost.WarEngine,
                                                     Gorepack], 1, 8)

        def check_limits(self):
            self.min = max([1, len(self.core.units)])
            self.max = max([1, len(self.core.units)]) * 8
            return super(BloodHost.Auxilary, self).check_limits()

    def __init__(self):
        core = self.Core(self)
        command = self.Command(self, core)
        aux = self.Auxilary(self, core)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class KhorneDaemonkinCAD(KhorneDaemonkinBase, CombinedArmsDetachment):
    army_id = 'khorne_daemonkin_cad'
    army_name = 'Khorne Daemonkin (Combined arms detachment)'


class KhorneDaemonkinAD(KhorneDaemonkinBase, AlliedDetachment):
    army_id = 'khorne_daemonkin_ad'
    army_name = 'Khorne Daemonkin (Allied detachment)'


class KhorneDaemonkinPA(KhorneDaemonkinBase, PlanetstrikeAttacker):
    army_id = 'khorne_daemonkin_pa'
    army_name = 'Khorne Daemonkin (Planetstrike attacker detachment)'


class KhorneDaemonkinPD(KhorneDaemonkinBase, PlanetstrikeDefender):
    army_id = 'khorne_daemonkin_pd'
    army_name = 'Khorne Daemonkin (Planetstrike defender detachment)'


class KhorneDaemonkinSA(KhorneDaemonkinBase, SiegeAttacker):
    army_id = 'khorne_daemonkin_sa'
    army_name = 'Khorne Daemonkin (Siege War attacker detachment)'


class KhorneDaemonkinSD(KhorneDaemonkinBase, SiegeDefender):
    army_id = 'khorne_daemonkin_sd'
    army_name = 'Khorne Daemonkin (Siege War defender detachment)'


class FlierDetachment(Wh40kBase):
    army_id = 'khorne_daemonkin_asd'
    army_name = 'Khorne Daemonkin (Air superiority detachment)'

    def __init__(self):
        super(FlierDetachment, self).__init__(wings=Fliers(self))


class KhorneDaemonkinKillTeam(Wh40kKillTeam):
    army_id = 'khorne_daemonkin_kt'
    army_name = 'Khorne Daemonkin'

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(KhorneDaemonkinKillTeam.KTTroops, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [ChaosSpaceMarines, Berzerks])
            UnitType(self, ChaosCultists)
            UnitType(self, Bloodletters)

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(KhorneDaemonkinKillTeam.KTElites, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [Possessed])
            UnitType(self, Bloodcrushers)

    class KTFastAttack(FastSection):
        def __init__(self, parent):
            super(KhorneDaemonkinKillTeam.KTFastAttack, self).__init__(parent)
            Wh40kKillTeam.process_vehicle_units(self, [Rhino])
            UnitType(self, Spawn)
            UnitType(self, Bikers)
            UnitType(self, Raptors)
            UnitType(self, Hounds)
            UnitType(self, Talons)

    def __init__(self):
        super(KhorneDaemonkinKillTeam, self).__init__(
            self.KTTroops(self), self.KTElites(self),
            self.KTFastAttack(self)
        )


faction = 'Khorne_Daemonkin'


class KhorneDaemonkin(Wh40kImperial, Wh40k7ed):
    army_id = 'khorne_daemonkin'
    army_name = 'Khorne Daemonkin'
    faction = faction
    # development = True

    def __init__(self):
        super(KhorneDaemonkin, self).__init__([
            KhorneDaemonkinCAD, FlierDetachment, BloodHost, Slaughtercult, BrazenOnslaught,
            Bloodstorm, Gorepack, Cohort])


class KhorneDaemonkinMissions(Wh40kImperial, Wh40k7edMissions):
    army_id = 'khorne_daemonkin_mis'
    army_name = 'Khorne Daemonkin'
    faction = faction
    # development = True

    def __init__(self):
        super(KhorneDaemonkinMissions, self).__init__([
            KhorneDaemonkinCAD, KhorneDaemonkinPA, KhorneDaemonkinPD,
            KhorneDaemonkinSA, KhorneDaemonkinSD,
            FlierDetachment, BloodHost, Slaughtercult, BrazenOnslaught,
            Bloodstorm, Gorepack, Cohort])


detachments = [
    KhorneDaemonkinCAD,
    KhorneDaemonkinAD,
    KhorneDaemonkinPA, KhorneDaemonkinPD,
    KhorneDaemonkinSA, KhorneDaemonkinSD,
    FlierDetachment,
    BloodHost,
    Slaughtercult,
    BrazenOnslaught,
    Bloodstorm,
    Gorepack,
    Cohort
]


unit_types = [
    Lord, DaemonPrince, Herald, BloodThrone, Skulltaker,
    FuryThirster, RageThirster, WrathThirster,
    ChaosCultists, ChaosSpaceMarines, Bloodletters,
    Berzerks,
    Possessed, Terminators, Bloodcrushers,
    Spawn, Rhino, Bikers, Raptors, Hounds, Talons, Heldrake,
    Heldrakes,
    LandRaider, Forgefiend, Maulerfiend, Defiler,
    Grinder, Helbrute, ScullCannon
]
