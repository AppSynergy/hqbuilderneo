__author__ = 'Denis Romanow'

from builder.core2 import *
from armory import Vehicle
from builder.games.wh40k.roster import Unit


class Squadron(Unit):
    unit_class = None
    unit_min = 1
    unit_max = 3

    def __init__(self, parent):
        super(Squadron, self).__init__(parent)
        self.tanks = UnitList(self, self.unit_class, self.unit_min, self.unit_max)

    def get_count(self):
        return self.tanks.count


class LemanRuss(Unit):
    type_name = u'Leman Russ'
    type_id = 'lemanruss_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Leman-Russ-Squadron')

    def __init__(self, parent, command=False):
        super(LemanRuss, self).__init__(parent=parent, gear=[Gear('Searchlight'), Gear('Smoke launchers')])
        self.command = command
        self.type = self.Type(self)
        self.Weapon(self)
        self.Sponsons(self)
        Vehicle(self, lights=False, smoke=False)

    class Type(OneOf):
        def __init__(self, parent):
            super(LemanRuss.Type, self).__init__(parent=parent, name='Type')
            self.variant('Leman Russ Battle Tank', 150, gear=[Gear('Battle cannon')])
            self.variant('Leman Russ Exterminator', 130, gear=[Gear('Exterminator autocannon')])
            self.variant('Leman Russ Vanquisher', 135, gear=[Gear('Vanquisher battle cannon')])
            self.variant('Leman Russ Eradicator', 120, gear=[Gear('Eradicator nova cannon')])
            self.variant('Leman Russ Demolisher', 170, gear=[Gear('Demolisher siege cannon')])
            self.variant('Leman Russ Punisher', 140, gear=[Gear('Punisher gatling cannon')])
            self.variant('Leman Russ Executioner', 155, gear=[Gear('Executioner plasma cannon')])

    class Weapon(OneOf):
        def __init__(self, parent):
            super(LemanRuss.Weapon, self).__init__(parent=parent, name='Weapon')

            self.heavybolter = self.variant('Heavy bolter', 0)
            self.heavyflamer = self.variant('Heavy flamer', 0)
            self.lascannon = self.variant('Lascannon', 10)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(LemanRuss.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)

            self.variant('Heavy flamers', 10, gear=[Gear('Heavy flamer', count=2)])
            self.variant('Heavy bolters', 20, gear=[Gear('Heavy bolter', count=2)])
            self.variant('Multi-meltas', 20, gear=[Gear('Multimelta', count=2)])
            self.variant('Plasma cannons', 30, gear=[Gear('Plasma cannon', count=2)])

    def build_description(self):
        desc = super(LemanRuss, self).build_description()
        desc.name = self.type.cur.title
        if self.command:
            desc.name = 'Command ' + desc.name
        return desc


class LemanRussSquadron(Squadron):
    type_name = u'Leman Russ Squadron'
    type_id = 'leman_russ_squadron_v1'

    class Tank(LemanRuss, ListSubUnit):
        pass

    unit_class = Tank


class Hydra(Unit):
    type_name = u'Hydra'
    type_id = 'hydraflaktank_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Hydra-Battery')

    def __init__(self, parent):
        super(Hydra, self).__init__(parent=parent, points=70, gear=[
            Gear('Twin-linked hydra autocannon', count=2),
            Gear('Searchlight'),
            Gear('Smoke launchers')
        ])
        self.Weapon(self)
        Vehicle(self, open_tank=True, lights=False, smoke=False)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Hydra.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Heavy bolter', 0)
            self.variant('Heavy flamer', 0)


class HydraSquadron(Squadron):
    type_name = u'Hydra Battery'
    type_id = 'hydra_battery_v1'

    class SquadronHydra(Hydra, ListSubUnit):
        pass

    unit_class = SquadronHydra


class Basilisk(ListSubUnit):
    type_name = u'Basilisk'
    type_id = 'basilisk_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Basilisk-Battery')

    def __init__(self, parent):
        super(Basilisk, self).__init__(parent=parent, points=125, gear=[
            Gear('Earthshaker cannon'),
            Gear('Searchlight'),
            Gear('Smoke launchers')
        ])
        Hydra.Weapon(self)
        Vehicle(self, open_tank=True, lights=False, smoke=False)


class BasiliskSquadron(Squadron):
    type_name = u'Basilisk Battery'
    type_id = 'basilisk_battery_v1'
    unit_class = Basilisk


class Wyvern(ListSubUnit):
    type_name = u'Wyvern'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Wyvern-Battery')

    def __init__(self, parent):
        super(Wyvern, self).__init__(parent=parent, points=65, gear=[
            Gear('Twin-linked stormshard mortar', count=2),
            Gear('Searchlight'),
            Gear('Smoke launchers')
        ])
        Hydra.Weapon(self)
        Vehicle(self, open_tank=True, lights=False, smoke=False)


class WyvernSquadron(Squadron):
    type_name = u'Wyvern Battery'
    type_id = 'wyvern_battery_v1'
    unit_class = Wyvern


class Manticore(Unit):
    type_name = u'Manticore'
    type_id = 'manticorerocketlauncher_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Manticore')

    def __init__(self, parent):
        super(Manticore, self).__init__(parent=parent, points=170, gear=[
            Gear('Storm eagle rockets'),
            Gear('Searchlight'),
            Gear('Smoke launchers')
        ])
        Hydra.Weapon(self)
        Vehicle(self, lights=False, smoke=False)


class Deathstrike(Unit):
    type_name = u'Deathstrike'
    type_id = 'deathstrikerocketlauncher_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Deathstrike')

    def __init__(self, parent):
        super(Deathstrike, self).__init__(parent=parent, points=160, gear=[
            Gear('Deathstrike missile'),
            Gear('Searchlight'),
            Gear('Smoke launchers')
        ])
        Hydra.Weapon(self)
        Vehicle(self, lights=False, smoke=False)
