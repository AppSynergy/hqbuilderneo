__author__ = 'Denis Romanov'

from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as Titans
from builder.games.wh40k.escalation.imperial_guard import LordsOfWar as IGLordsOfWar,\
    Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer, Shadowsword,\
    Stormlord, Stormsword, unit_types as esc_unit_types
from builder.games.wh40k.imperial_armour.volume1 import IGFast, IGElites,\
    IGHeavy, IGLords, unit_types as ia1_unit_types
from builder.games.wh40k.imperial_armour.dataslates.imperial_knights import CerastusSection
from hq import *
from elites import *
from troops import *
from fast import *
from heavy import *
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kImperial, Wh40kKillTeam, PrimaryDetachment,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, UnitType, Detachment, Formation,\
    FlyerSection
from builder.games.wh40k.fortifications import Fort


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.yarrick = UnitType(self, Yarrick)
        self.command = UnitType(self, CommandSquad)

        class PaskTankCommander(TankCommander):
            allow_pask = True

        UnitType(self, PaskTankCommander)
        UnitType(self, LordCommissar)
        self.commissar = UnitType(self, Commissar, slot=0)
        self.priest = UnitType(self, Priest, slot=0)
        self.psy = UnitType(self, PrimarisPsyker, slot=0)
        self.tech = UnitType(self, Techpriest, slot=0)
        self.serv = UnitType(self, Servitors, slot=0)


class HQ(BaseHQ):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        from builder.games.wh40k.dataslates.cypher import Cypher
        self.cypher = UnitType(self, Cypher, slot=0)
        from builder.games.wh40k.adepta_sororitas_v4 import Celestine
        UnitType(self, Celestine)
        from builder.games.wh40k.inquisition_v2 import Greyfax
        UnitType(self, Greyfax)
        from builder.games.wh40k.cult_mechanicus import Cawl
        UnitType(self, Cawl)
        from builder.games.wh40k.grey_knights_v3 import Voldus
        UnitType(self, Voldus)

    def check_rules(self):
        super(HQ, self).check_rules()
        self.cypher.visible = isinstance(self.parent.parent.parent, PrimaryDetachment)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.ogrynsquad = UnitType(self, OgrynSquad)
        self.bullgrynsquad = UnitType(self, BullgrynSquad)
        self.ratlingsquad = UnitType(self, RatlingSquad)
        self.psykersquad = UnitType(self, PsykerSquad)
        UnitType(self, MilitarumTempestusPlatoon)


class Elites(IGElites, BaseElites):
    pass


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.infantryplatoon = UnitType(self, InfantryPlatoon)
        self.veteransquad = UnitType(self, VeteranSquad)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        UnitType(self, ScoutSentinelSquad)
        UnitType(self, ArmouredSentinelSquad)
        UnitType(self, Riders)
        UnitType(self, HellhoundSquadron)
        UnitType(self, Valkyries)
        UnitType(self, VendettaSquadron)


class FastAttack(IGFast, BaseFastAttack):
    pass


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        UnitType(self, LemanRussSquadron)
        UnitType(self, HydraSquadron)
        UnitType(self, BasiliskSquadron)
        UnitType(self, WyvernSquadron)
        UnitType(self, Manticore)
        UnitType(self, Deathstrike)


class HeavySupport(IGHeavy, BaseHeavySupport):
    pass


class LordsOfWar(CerastusSection, IGLords, Titans, IGLordsOfWar):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent)
        from builder.games.wh40k.space_marines_v3 import Guilliman
        UnitType(self, Guilliman)


class Fliers(FlyerSection):
    def __init__(self, parent):
        super(Fliers, self).__init__(parent, [Valkyries, VendettaSquadron])


class AstraMilitarumBase(Wh40kBase):
    army_id = 'astra_militarum_v1_base'
    army_name = 'Astra Militarum'

    def __init__(self):
        self.hq = HQ(parent=self)

        super(AstraMilitarumBase, self).__init__(
            hq=self.hq, elites=Elites(parent=self), troops=Troops(parent=self), fast=FastAttack(parent=self), heavy=HeavySupport(parent=self),
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self)
        )

        self.codex = SupplementOptions(self)

    def check_rules(self):
        super(AstraMilitarumBase, self).check_rules()
        if self.hq.commissar.count > self.hq.command.count + self.troops.infantryplatoon.count:
            self.error("You may include only one Commissar for every Company Command Squad or Platoon Command Squad "
                       "in your army.")
        if self.hq.priest.count > 3:
            self.error("Each Astra Militarum detachment may include 0-3 Ministorum Priests")
        if self.hq.psy.count > 3:
            self.error("Each Astra Militarum detachment may include 0-3 Primaris Psykers")
        if self.hq.tech.count > 3:
            self.error("Each Astra Militarum detachment may include 0-3 Enginseers")
        self.check_unit_limit(self.hq.tech.count, self.hq.serv,
                              "You may include one unit of Servitors for every Enginseer")

        self.hq.yarrick.active = not self.roster.is_cadia()
        if self.roster.is_cadia() and self.hq.yarrick.count:
            self.error("Yarrick is not from Cadia")

    def is_base_codex(self):
        return self.codex.cur == self.codex.base

    def is_cadia(self):
        return self.codex.cur == self.codex.cadia


class AstraMilitarumCAD(AstraMilitarumBase, CombinedArmsDetachment):
    army_id = 'astra_militarum_v1_cad'
    army_name = 'Astra Militarum (Combined arms detachment)'


class AstraMilitarumAD(AstraMilitarumBase, AlliedDetachment):
    army_id = 'astra_militarum_v1_ad'
    army_name = 'Astra Militarum (Allied detachment)'

class AstraMilitarumPA(AstraMilitarumBase, PlanetstrikeAttacker):
    army_id = 'astra_militarum_v1_pa'
    army_name = 'Astra Militarum (Planetstrike attacker detachment)'


class AstraMilitarumPD(AstraMilitarumBase, PlanetstrikeDefender):
    army_id = 'astra_militarum_v1_pd'
    army_name = 'Astra Militarum (Planetstrike defender detachment)'

class AstraMilitarumSA(AstraMilitarumBase, SiegeAttacker):
    army_id = 'astra_militarum_v1_sa'
    army_name = 'Astra Militarum (Siege War attacker detachment)'


class AstraMilitarumSD(AstraMilitarumBase, SiegeDefender):
    army_id = 'astra_militarum_v1_sd'
    army_name = 'Astra Militarum (Siege War defender detachment)'


class FlierDetachment(Wh40kBase):
    army_id = 'astra_militarum_v1_asd'
    army_name = 'Astra Militarum (Air superiority detachment)'

    def __init__(self):
        super(FlierDetachment, self).__init__(wings=Fliers(self))


class AstraMilitarumKillTeam(Wh40kKillTeam):
    army_id = 'astra_militarum_v1_kt'
    army_name = 'Astra Militarum'

    def is_base_codex(self):
        return self.codex.cur == self.codex.base

    def is_cadia(self):
        return self.codex.cur == self.codex.cadia

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(AstraMilitarumKillTeam.KTTroops, self).__init__(parent)

            class KTPlatoon(InfantryPlatoon):
                def note_transport(self, statistic):
                    count = self.transport_count()
                    if count:
                        statistic['Models'] += count
                        statistic['Units'] += count
                        statistic['Vehicles'] += count
                    return statistic

            UnitType(self, KTPlatoon)
            Wh40kKillTeam.process_transported_units(self, [VeteranSquad])

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(AstraMilitarumKillTeam.KTElites, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [
                OgrynSquad, BullgrynSquad, PsykerSquad
            ])
            self.psykersquad = UnitType(self, RatlingSquad)

            class KTTempestus(MilitarumTempestusPlatoon):
                def note_transport(self, statistic):
                    count = self.transport_count()
                    if count:
                        statistic['Models'] += count
                        statistic['Units'] += count
                        statistic['Vehicles'] = count
                    return statistic

            UnitType(self, KTTempestus)

    class KTFast(FastSection):
        def __init__(self, parent):
            super(AstraMilitarumKillTeam.KTFast, self).__init__(parent)
            Wh40kKillTeam.process_vehicle_units(self, [
                ScoutSentinelSquad, ArmouredSentinelSquad
            ])
            UnitType(self, Riders)

    def __init__(self):
        super(AstraMilitarumKillTeam, self).__init__(
            self.KTTroops(self), self.KTElites(self), self.KTFast(self))
        self.codex = SupplementOptions(self)


class SupplementOptions(OneOf):
    def __init__(self, parent):
        super(SupplementOptions, self).\
            __init__(parent=parent, name='Codex')
        self.base = self.variant(name='Astra Militarum')
        self.cadia = self.variant(name='Cadia Detachment')


class AstraMilitarumFormation(Formation):
    def __init__(self, *args, **kwargs):
        super(AstraMilitarumFormation, self).__init__(*args, **kwargs)
        self.codex = SupplementOptions(self)

    def is_base_codex(self):
        return self.codex.cur == self.codex.base

    def is_cadia(self):
        return self.codex.cur == self.codex.cadia


class SteelHost(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_steel_host'
    army_name = 'The Steel Host'

    def __init__(self):
        super(SteelHost, self).__init__()
        UnitType(self, TankCommander, min_limit=1, max_limit=1)
        UnitType(self, LemanRussSquadron, min_limit=3, max_limit=3)
        UnitType(self, Hydra, min_limit=1, max_limit=1)


class Rampart(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_steel_rampart'
    army_name = 'Rampart Detachment'

    def __init__(self):
        super(Rampart, self).__init__()
        UnitType(self, PlatoonCommandSquad, min_limit=1, max_limit=1)
        self.bullgryns = UnitType(self, BullgrynSquad, min_limit=2, max_limit=2)

    def check_rules(self):
        super(Rampart, self).check_rules()
        for unit in self.bullgryns.units:
            if unit.count < 5:
                self.error("Bullgryn squad must include at least 5 models")


class BattleGroupCommand(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_battle_group_command'
    army_name = 'Battle Group Command'

    class PaskTankCommander(TankCommander):
        allow_pask = True

    def __init__(self):
        super(BattleGroupCommand, self).__init__()
        commander = [
            UnitType(self, CommandSquad),
            UnitType(self, self.PaskTankCommander)
        ]
        self.add_type_restriction(commander, 1, 1)
        UnitType(self, LordCommissar, min_limit=0, max_limit=1)


class EmperorShieldPlatoon(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_shield_platoon'
    army_name = "'Emperor's Shield' Infantry Platoon"

    class ShieldPlatoon(InfantryPlatoon):
        def __init__(self, parent):
            super(EmperorShieldPlatoon.ShieldPlatoon, self).__init__(parent)
            self.infantry.min = 5

        def check_rules(self):
            super(EmperorShieldPlatoon.ShieldPlatoon, self).check_rules()
            if self.transport_count() > 0:
                self.error("Units taken as part of Infantry Platoon may not take Dedicated Transports")

    def __init__(self):
        super(EmperorShieldPlatoon, self).__init__()
        UnitType(self, self.ShieldPlatoon, min_limit=1, max_limit=1)
        sentinels = [
            UnitType(self, ScoutSentinelSquad),
            UnitType(self, ArmouredSentinelSquad)
        ]
        self.add_type_restriction(sentinels, 1, 3)


class EmperorShieldCompany(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_shield_company'
    army_name = "'Emperor's Shield' Infantry Company"

    allow_creed = True

    def __init__(self):
        super(EmperorShieldCompany, self).__init__()

        class CadiaCommandSquad(CommandSquad):
            allow_creed = self.allow_creed

        class CadianPlatoon(EmperorShieldPlatoon):
            def __init__(self):
                super(CadianPlatoon, self).__init__()
                self.codex.visible = False

            def is_base_codex(child):
                return self.is_base_codex()

            def is_cadia(child):
                return self.is_cadia()

        UnitType(self, CadiaCommandSquad, min_limit=1, max_limit=1)
        Detachment.build_detach(self, CadianPlatoon,
                                min_limit=3, max_limit=3)


class EmperorTalon(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_talon'
    army_name = "'Emperor's Talon' Recon Company"

    def __init__(self):
        super(EmperorTalon, self).__init__()
        self.scout = UnitType(self, ScoutSentinelSquad)
        self.armoured = UnitType(self, ArmouredSentinelSquad)
        sentinels = [self.scout, self.armoured]
        self.add_type_restriction(sentinels, 2, 4)

    def check_rules(self):
        super(EmperorTalon, self).check_rules()
        for unit in self.scout.units + self.armoured.units:
            if unit.count < 3:
                self.error("Sentinel Squad squad must include at least 3 models")


class TransportedUnit(Unit):
    def check_rules(self):
        super(TransportedUnit, self).check_rules()
        if not self.transport.any:
            self.error('{} must take a Chimera or Taurox as a Dedicated Transport'.format(self.type_name))


class EmperorBlade(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_blade'
    army_name = "'Emperor's Blade' Assault Company"
    allow_creed = True

    def __init__(self):
        super(EmperorBlade, self).__init__()

        class TransportedVeterans(VeteranSquad, TransportedUnit):
            pass

        class TransportedCommand(CommandSquad, TransportedUnit):
            allow_creed = self.allow_creed

        UnitType(self, TransportedCommand, min_limit=1, max_limit=1)
        UnitType(self, TransportedVeterans, min_limit=3, max_limit=3)
        UnitType(self, HellhoundSquadron, min_limit=1, max_limit=3)


class EmperorWrath(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_wrath'
    army_name = "'Emperor's Wrath' Artillery Company"

    allow_creed = True

    def __init__(self):
        super(EmperorWrath, self).__init__()

        class TransportedCommand(CommandSquad, TransportedUnit):
            allow_creed = self.allow_creed

        UnitType(self, TransportedCommand, min_limit=1, max_limit=1)
        batteries = [
            UnitType(self, BasiliskSquadron),
            UnitType(self, HydraSquadron),
            UnitType(self, WyvernSquadron)
        ]
        self.add_type_restriction(batteries, 2, 2)
        rockets = [
            UnitType(self, Manticore),
            UnitType(self, Deathstrike)
        ]
        self.add_type_restriction(rockets, 1, 1)
        UnitType(self, Techpriest, min_limit=1, max_limit=3)


class EmperorFist(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_fist'
    army_name = "'Emperor's Fist' Armoured Company"
    allow_pask = True

    def __init__(self):
        super(EmperorFist, self).__init__()

        class Commander(TankCommander):
            allow_pask = self.allow_pask

        UnitType(self, Commander, min_limit=1, max_limit=1)
        UnitType(self, LemanRussSquadron, min_limit=3, max_limit=3)
        UnitType(self, Techpriest, min_limit=1, max_limit=3)


class OgrynAuxilia(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_ogryn'
    army_name = 'Ogryn Auxilia'

    def __init__(self):
        super(OgrynAuxilia, self).__init__()
        UnitType(self, Commissar, min_limit=1, max_limit=1)
        UnitType(self, BullgrynSquad, min_limit=2, max_limit=2)
        UnitType(self, OgrynSquad, min_limit=2, max_limit=2)


class PsykanaDivision(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_psykana'
    army_name = 'Psykana Division'

    def __init__(self):
        super(PsykanaDivision, self).__init__()
        UnitType(self, PrimarisPsyker, min_limit=1, max_limit=1)
        UnitType(self, PsykerSquad, min_limit=3, max_limit=3)
        UnitType(self, Commissar, min_limit=1, max_limit=3)


class EmperorFury(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_fury'
    army_name = "'Emperor's Fury' Super-heavy Company"

    def __init__(self):
        super(EmperorFury, self).__init__()
        tanks = [
            UnitType(self, Baneblade),
            UnitType(self, Banehammer),
            UnitType(self, Banesword),
            UnitType(self, Doomhammer),
            UnitType(self, Hellhammer),
            UnitType(self, Shadowsword),
            UnitType(self, Stormlord),
            UnitType(self, Stormsword)
        ]
        self.add_type_restriction(tanks, 3, 3)
        UnitType(self, Techpriest, min_limit=1, max_limit=1)


class EmperorSpear(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_spear'
    army_name = "'Emperor's Spear' Aerial Company"

    def __init__(self):
        super(EmperorSpear, self).__init__()
        self.add_type_restriction([
            UnitType(self, VendettaSquadron),
            UnitType(self, Valkyries)
        ], 3, 3)


class CadianInfantryPlatoon(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_cadian_infantry_platoon'
    army_name = 'Infantry Platoon'

    def __init__(self):
        super(CadianInfantryPlatoon, self).__init__()
        UnitType(self, InfantryPlatoon, min_limit=1, max_limit=1)


class CadianMilitarumTempestusPlatoon(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_cadian_tempestus_platoon'
    army_name = 'Mititarum Tempestus Platoon'

    def __init__(self):
        super(CadianMilitarumTempestusPlatoon, self).__init__()
        UnitType(self, MilitarumTempestusPlatoon, min_limit=1, max_limit=1)


class SupportElement(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_support_element'
    army_name = 'Super-Heavy Support Element'

    class CadianEmperorFury(EmperorFury):
        def __init__(self, *args, **kwargs):
            super(SupportElement.CadianEmperorFury, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        def is_base_codex(self):
            return False

        def is_cadia(self):
            return True

    def __init__(self):
        super(SupportElement, self).__init__()
        self.add_type_restriction([
            UnitType(self, Baneblade),
            UnitType(self, Banehammer),
            UnitType(self, Banesword),
            UnitType(self, Doomhammer),
            UnitType(self, Hellhammer),
            UnitType(self, Shadowsword),
            UnitType(self, Stormlord),
            UnitType(self, Stormsword),
            Detachment.build_detach(self, SupportElement.CadianEmperorFury)
        ], 1, 1)


class ArmouredShield(AstraMilitarumFormation):
    army_id = 'astra_militarum_v1_armoured_shield'
    army_name = 'Armoured Shield'

    def __init__(self):
        super(ArmouredShield, self).__init__()
        UnitType(self, Commissar, min_limit=1, max_limit=1)
        UnitType(self, InfantrySquad, min_limit=1, max_limit=1)
        UnitType(self, LemanRuss, min_limit=1, max_limit=1)


faction = 'Astra Militarum'


class CadianBattleGroup(Wh40k7ed):
    army_id = 'astra_militarum_v1_cadian_battle_group'
    army_name = 'Cadian Battle Group'

    class AstraMilitarumSubFormation(AstraMilitarumFormation):
        def __init__(self, *args, **kwargs):
            super(CadianBattleGroup.AstraMilitarumSubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        def is_base_codex(self):
            return False

        def is_cadia(self):
            return True

    @staticmethod
    def make_cadian_battle_group(formation, creed_flag=True):
        class BaseSubformation(CadianBattleGroup.AstraMilitarumSubFormation, formation):
            allow_creed = creed_flag
        return BaseSubformation

    class CommandSection(Detachment):
        def __init__(self, parent):
            super(CadianBattleGroup.CommandSection, self).__init__(
                parent, 'command', 'Command', [
                    CadianBattleGroup.make_cadian_battle_group(BattleGroupCommand)
                ], 1, None)

    class Core(Detachment):
        def __init__(self, parent, command):
            self.command = command

            class InnerFist(EmperorFist):
                allow_pask = False

            super(CadianBattleGroup.Core, self).__init__(
                parent, 'core', 'Core', [
                    CadianBattleGroup.make_cadian_battle_group(f, False) for f in [
                        EmperorShieldCompany,
                        InnerFist,
                    ]
                ], 0, 3)

        def check_limits(self):
            self.min = 0
            self.max = max([0, len(self.command.units)]) * 3
            return super(CadianBattleGroup.Core, self).check_limits()

    class Auxilary(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(CadianBattleGroup.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary', [
                    CadianBattleGroup.make_cadian_battle_group(f, False) for f in [
                        CadianInfantryPlatoon,
                        SupportElement,
                        EmperorBlade,
                        EmperorSpear,
                        OgrynAuxilia,
                        EmperorWrath,
                        EmperorTalon,
                        PsykanaDivision,
                        CadianMilitarumTempestusPlatoon
                    ]
                ],
                0, 3)

        def check_limits(self):
            self.min = 0
            self.max = max([0, len(self.core.units)]) * 3
            return super(CadianBattleGroup.Auxilary, self).check_limits()

    def __init__(self):
        self.command = self.CommandSection(self)
        self.core = self.Core(self, self.command)
        self.aux = self.Auxilary(self, self.core)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([self.command, self.core, self.aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()

    def is_cadia(self):
        return self.codex.cur == self.codex.cadia


class AstraMilitarum(Wh40k7ed, Wh40kImperial):
    army_id = 'astra_militarum_v1'
    army_name = 'Astra Militarum'
    faction = faction

    def __init__(self):
        super(AstraMilitarum, self).__init__([
            AstraMilitarumCAD, FlierDetachment, CadianBattleGroup, SteelHost, Rampart, EmperorShieldPlatoon,
            EmperorShieldCompany, EmperorWrath, EmperorBlade, EmperorTalon,
            EmperorFist, EmperorFury, EmperorSpear, OgrynAuxilia,
            PsykanaDivision, ArmouredShield], [AstraMilitarumAD])


class AstraMilitarumMissions(Wh40k7edMissions, Wh40kImperial):
    army_id = 'astra_militarum_v1_mis'
    army_name = 'Astra Militarum'
    faction = faction

    def __init__(self):
        super(AstraMilitarumMissions, self).__init__([
            AstraMilitarumCAD, AstraMilitarumPA, AstraMilitarumPD, AstraMilitarumSA, AstraMilitarumSD,
            FlierDetachment, CadianBattleGroup, SteelHost, Rampart, EmperorShieldPlatoon,
            EmperorShieldCompany, EmperorWrath, EmperorBlade, EmperorTalon,
            EmperorFist, EmperorFury, EmperorSpear, OgrynAuxilia,
            PsykanaDivision, ArmouredShield], [AstraMilitarumAD])


detachments = [
    AstraMilitarumCAD,
    AstraMilitarumPA,
    AstraMilitarumPD,
    AstraMilitarumSA,
    AstraMilitarumSD,
    AstraMilitarumAD,
    FlierDetachment,
    CadianBattleGroup,
    SteelHost,
    Rampart,
    EmperorShieldPlatoon,
    EmperorShieldCompany,
    EmperorWrath,
    EmperorBlade,
    EmperorTalon,
    EmperorFist,
    EmperorFury,
    EmperorSpear,
    OgrynAuxilia,
    PsykanaDivision,
    ArmouredShield
]


unit_types = [
    Yarrick, CommandSquad, TankCommander, LordCommissar,
    Commissar, Priest, PrimarisPsyker, Techpriest, Servitors,
    OgrynSquad, BullgrynSquad, RatlingSquad, PsykerSquad,
    MilitarumTempestusPlatoon, InfantryPlatoon, VeteranSquad,
    ScoutSentinelSquad, ArmouredSentinelSquad, Riders,
    HellhoundSquadron, Valkyries, VendettaSquadron, LemanRussSquadron,
    HydraSquadron, BasiliskSquadron, WyvernSquadron, Manticore,
    Deathstrike
] + ia1_unit_types + esc_unit_types
