__author__ = 'Ivan Truskov'


from builder.core2 import *
from builder.games.wh40k.unit import Unit
from builder.games.wh40k.adepta_sororitas_v4.armory import CadianRelicsHoly, CadianWeaponHoly,\
    CadianWeaponArcana


class Melee(OneOf):
    def __init__(self, parent, name):
        super(Melee, self).__init__(parent, name=name)
        self.variant('Nemesis force halberd', 2)
        self.variant('Two Nemesis falchions', 4, gear=[Gear('Nemesis falchion', count=2)])
        self.variant('Nemesis warding stave', 5)
        self.variant('Nemesis Daemon hammer', 10)


class Special(OneOf):
    def __init__(self, parent):
        super(Special, self).__init__(parent, name='Ranged weapon')
        self.sb = self.variant('Storm bolter', 0)
        self.variant('Incinerator', 5)
        self.variant('Psilencer', 10)
        self.variant('Psycannon', 15)

    def has_special(self):
        return self.cur != self.sb


class TerminatorSpecial(OneOf):
    def __init__(self, parent, name):
        super(TerminatorSpecial, self).__init__(parent, name=name)
        self.sb = self.variant('Storm bolter', 0)
        self.variant('Incinerator', 10)
        self.variant('Psilencer', 15)
        self.variant('Psycannon', 20)


class SpecialIssue(OptionsList):

    def __init__(self, parent):
        super(SpecialIssue, self).__init__(parent, name='Special issue wargear')
        self.variant('Melta bombs', 5)
        self.variant('Digital weapons', 10)
        self.variant('Teleport homer', 10)


class MasterWeapon(OneOf):
    def __init__(self, parent, name, buddy):
        super(MasterWeapon, self).__init__(parent, name)
        self.buddy = buddy
        self.relic_options = []

    @property
    def description(self):
        if self.buddy.any and self.buddy.used:
            return [Gear('Master-crafted {}'.format(g.name), count=g.count) for g in super(MasterWeapon, self).description]
        else:
            return super(MasterWeapon, self).description

    def check_rules(self):
        super(MasterWeapon, self).check_rules()
        self.buddy.used = self.buddy.visible = not(self.cur in self.relic_options)

    def get_unique(self):
        return self.description if self.cur in self.relic_options else []


class SpecialIssueWeapon(object):
    class Upgrade(OptionsList):
        def __init__(self, parent):
            super(SpecialIssueWeapon.Upgrade, self).__init__(parent, '')
            self.variant('Upgrade weapon to Master-crafted', 10)

        @property
        def description(self):
            return []

    def __init__(self, parent, weapontype, name, **kwargs):
        self.flag = self.Upgrade(parent)
        self.wep = weapontype(parent, name, self.flag, **kwargs)

    def get_unique(self):
        return self.wep.get_unique()


class RelicWeapon(MasterWeapon):
    def __init__(self, parent, name, buddy, melee=True):
        super(RelicWeapon, self).__init__(parent, name, buddy)
        if melee:
            self.relic_options += [self.variant('The Soul Glaive', 20)]
        else:
            self.relic_options += [self.variant('The Fury of Deimos', 10)]


class HolyRelicWeapon(CadianWeaponHoly, RelicWeapon):
    def __init__(self, *args, **kwargs):
        super(HolyRelicWeapon, self).__init__(*args, **kwargs)
        self.relic_options += [self.worthy_blade]


class ArcanaRelicWeapon(CadianWeaponArcana, RelicWeapon):
    def __init__(self, *args, **kwargs):
        super(ArcanaRelicWeapon, self).__init__(*args, **kwargs)
        self.relic_options += [self.qann]


class Relics(OptionsList):
    def __init__(self, parent, armour_allowed=True):
        super(Relics, self).__init__(parent, name='Relics of Titan', limit=1)
        self.variant('Bone Shard of Solor', 10)
        if armour_allowed:
            self.armour = self.variant('The Cuirass of Sacrifice', 15)
        self.variant('Domina Liber Daemonica', 25)


class HolyRelics(CadianRelicsHoly, Relics):
    pass


class DreadWeapon(OneOf):
    def __init__(self, parent):
        super(DreadWeapon, self).__init__(parent, name='Ranged weapon')
        self.variant('Multi-melta', 0)
        self.variant('Twin-linked autocannon', 5)
        self.variant('Twin-linked heavy bolter', 5)
        self.variant('Twin-linked heavy flamer', 5)
        self.variant('Plasma cannon', 10)
        self.variant('Assault cannon', 20)
        self.variant('Twin-linked lascannon', 25)


class NFSword(OneOf):
    def __init__(self, parent, name):
        super(NFSword, self).__init__(parent, name=name)
        self.variant('Nemesis force sword', 0)


class PowerAxe(OneOf):
    def __init__(self, parent, name):
        super(PowerAxe, self).__init__(parent, name=name)
        self.variant('Power axe', 0)


class SBolter(OneOf):
    def __init__(self, parent, name):
        super(SBolter, self).__init__(parent, name=name)
        self.variant('Storm bolter', 0)


class VehicleEquipment(OptionsList):
    def __init__(self, parent, landraider=False):
        super(VehicleEquipment, self).__init__(parent, name='Grey Knights Vehicle Equipment')
        if landraider:
            self.variant('Multi-melta', 10)
        else:
            self.variant('Dozer blade', 5)
        self.variant('Storm bolter', 5)
        self.variant('Extra armour', 10)
        self.variant('Hunter-killer missile', 10)
