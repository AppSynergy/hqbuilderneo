
__author__ = 'Ivan Truskov'

from builder.games.wh40k.unit import Unit
from fast import Rhino, Razorback
from armory import *


class PurgationSquad(Unit):
    type_name = u'Purgation Squad'
    type_id = 'purge_squad_v3'

    model_points = 20
    model_gear = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Psyk-out grenades')]

    class Justicar(Unit):

        class MeleeWeapon(MasterWeapon, Melee, NFSword):
            pass

        class RangedOption(OptionsList):
            def __init__(self, parent):
                super(PurgationSquad.Justicar.RangedOption, self).__init__(parent, name='')
                self.variant('Upgrade Storm Bolter to Master-crafted', 10)

            @property
            def description(self):
                if self.any:
                    return [Gear('Master-crafted Storm bolter', 10)]
                else:
                    return [Gear('Storm bolter')]

        def __init__(self, parent):
            super(PurgationSquad.Justicar, self).__init__(
                name='Purgator Justicar',
                parent=parent, points=20 + 10,
                gear=PurgationSquad.model_gear
            )
            self.wep1 = SpecialIssueWeapon(self, self.MeleeWeapon, 'Melee weapon')
            self.wep2 = self.RangedOption(self)
            self.opt = SpecialIssue(self)

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(PurgationSquad.Transport, self).__init__(parent, 'Transport', limit=1)
            self.models = [
                SubUnit(self, Rhino(parent=None)),
                SubUnit(self, Razorback(parent=None))
            ]

    class Purgator(ListSubUnit):

        class MeleeWeapon(Melee, NFSword):
            pass

        class RangedWeapon(Special):
            pass

        def __init__(self, parent):
            super(PurgationSquad.Purgator, self).__init__(
                parent=parent, points=PurgationSquad.model_points, name='Purgator',
                gear=PurgationSquad.model_gear
            )
            self.mle = self.MeleeWeapon(self, 'Melee weapon')
            self.rng = self.RangedWeapon(self)

        @ListSubUnit.count_gear
        def count_special(self):
            return self.rng.cur != self.rng.sb

    def __init__(self, parent):
        super(PurgationSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Justicar(None))
        self.marines = UnitList(self, self.Purgator, 4, 9)
        self.transport = self.Transport(parent=self)

    def check_rules(self):
        super(PurgationSquad, self).check_rules()
        spec_total = sum(u.count_special() for u in self.marines.units)
        if spec_total > 4:
            self.error('Purgation squad may carry no more then 4 special weapons (taken: {0})'.format(spec_total))

    def get_count(self):
        return self.marines.count + 1

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(self.note_transport(super(PurgationSquad, self).build_statistics()))


class Dreadknight(Unit):
    type_id = 'dreadknight_v3'
    type_name = u'Nemesis Dreadknight'

    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Nemesis-Dreadknight')

    class Options(OptionsList):
        def __init__(self, parent):
            super(Dreadknight.Options, self).__init__(parent, 'Options')
            self.variant('Personal teleporter', 30)

    class RangedWeapons(OptionsList):
        def __init__(self, parent):
            super(Dreadknight.RangedWeapons, self).__init__(parent, 'Ranged weapons', limit=2)
            self.variant('Heavy incinerator', 20)
            self.variant('Gatling psilencer', 30)
            self.variant('Heavy psycannon', 35)

    class MeleeWeapon(OneOf):
        def __init__(self, parent):
            super(Dreadknight.MeleeWeapon, self).__init__(parent, 'Melee weapon')
            self.variant('Power fist', 0)
            self.variant('Nemesis Daemon hammer', 5)
            self.variant('Nemesis greatsword', 10)

    def __init__(self, parent):
        super(Dreadknight, self).__init__(parent, name='Nemesis Dreadknight', points=130, gear=[Gear('Power fist')])
        self.Options(self)
        self.RangedWeapons(self)
        self.MeleeWeapon(self)

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(super(Dreadknight, self).build_statistics())


class LandRaider(Unit):
    type_id = 'gk_land_raider_v3'
    type_name = "Land Raider"

    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Land-Raider')

    def __init__(self, parent):
        super(LandRaider, self).__init__(parent=parent, points=250, gear=[
            Gear('Twin-linked heavy bolter'),
            Gear('Twin-linked lascannon', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ])
        self.opt = VehicleEquipment(self, True)


class LandRaiderCrusader(Unit):
    type_id = 'gk_land_raider_crusader_v3'
    type_name = "Land Raider Crusader"
    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Land-Raider-Crusader')

    def __init__(self, parent):
        super(LandRaiderCrusader, self).__init__(parent=parent, points=250, gear=[
            Gear('Twin-linked assault cannon'),
            Gear('Hurricane bolter', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
            Gear('Frag assault aaunchers')
        ])
        self.opt = VehicleEquipment(self, True)


class LandRaiderRedeemer(Unit):
    type_id = 'gk_land_raider_redeemer_v3'
    type_name = "Land Raider Redeemer"
    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Land-Raider-Redeemer')

    def __init__(self, parent):
        super(LandRaiderRedeemer, self).__init__(parent=parent, points=240, gear=[
            Gear('Twin-linked assault cannon'),
            Gear('Flamestorm cannon', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
            Gear('Frag Assault Launcher'),
        ])
        self.opt = VehicleEquipment(self, True)


from builder.games.wh40k.imperial_armour.dataslates.space_marines import BaseDeimos


class DeimosVindicator(BaseDeimos):
    def get_options(self):
        class Options(VehicleEquipment):
            def __init__(self, parent):
                super(Options, self).__init__(parent)
                self.variant('Siege shield', 10)
        return Options
