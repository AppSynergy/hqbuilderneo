from builder.core2 import *
from armory import *
from fast import Rhino, Razorback
from heavy import LandRaider, LandRaiderCrusader, LandRaiderRedeemer
from builder.games.wh40k.grey_knights_v3.armory import Melee
from builder.games.wh40k.roster import Unit

__author__ = 'Ivan Truskov'


class StrikeSquad(Unit):
    type_name = u'Strike squad'
    type_id = 'strike_squad_v3'
    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Strike-Squad')

    model_points = 20
    model_gear = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Psyk-out grenades')]

    class Justicar(Unit):

        class MeleeWeapon(MasterWeapon, Melee, NFSword):
            pass

        class RangedOption(OptionsList):
            def __init__(self, parent):
                super(StrikeSquad.Justicar.RangedOption, self).__init__(parent, name='')
                self.variant('Upgrade Storm Bolter to Master-crafted', 10)

            @property
            def description(self):
                if self.any:
                    return [Gear('Master-crafted Storm bolter', 10)]
                else:
                    return [Gear('Storm bolter')]

        def __init__(self, parent):
            super(StrikeSquad.Justicar, self).__init__(
                name='Justicar',
                parent=parent, points=20 + 10,
                gear=StrikeSquad.model_gear
            )
            self.wep1 = SpecialIssueWeapon(self, self.MeleeWeapon, 'Melee weapon')
            self.wep2 = self.RangedOption(self)
            self.opt = SpecialIssue(self)

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(StrikeSquad.Transport, self).__init__(parent, 'Transport', limit=1)
            self.models = [
                SubUnit(self, Rhino(parent=None)),
                SubUnit(self, Razorback(parent=None))
            ]

    class GreyKnight(ListSubUnit):

        class MeleeWeapon(Melee, NFSword):
            pass

        class RangedWeapon(Special):
            pass

        def __init__(self, parent):
            super(StrikeSquad.GreyKnight, self).__init__(
                parent=parent, points=StrikeSquad.model_points, name='Grey Knight',
                gear=StrikeSquad.model_gear
            )
            self.mle = self.MeleeWeapon(self, 'Melee weapon')
            self.rng = self.RangedWeapon(self)

        @ListSubUnit.count_gear
        def count_special(self):
            return self.rng.cur != self.rng.sb

    def __init__(self, parent):
        super(StrikeSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Justicar(None))
        self.marines = UnitList(self, self.GreyKnight, 4, 9)
        self.transport = self.Transport(parent=self)

    def check_rules(self):
        super(StrikeSquad, self).check_rules()
        spec_total = sum(u.count_special() for u in self.marines.units)
        if 5 * spec_total > self.get_count():
            self.error('In Strike squad may be only 1 special weapon per 5 models (taken: {0})'.format(spec_total))

    def get_count(self):
        return self.marines.count + 1

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(self.note_transport(super(StrikeSquad, self).build_statistics()))


class TerminatorSquad(Unit):
    type_name = u'Terminator squad'
    type_id = 'terminator_squad_v3'

    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Terminator-Squad')

    model_points = 33
    model_gear = [Gear('Terminator armour'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Psyk-out grenades')]

    class Justicar(Unit):

        class MeleeWeapon(MasterWeapon, Melee, NFSword):
            pass

        class RangedOption(OptionsList):
            def __init__(self, parent):
                super(TerminatorSquad.Justicar.RangedOption, self).__init__(parent, name='')
                self.variant('Upgrade Storm Bolter to Master-crafted', 10)

            @property
            def description(self):
                if self.any:
                    return [Gear('Master-crafted Storm bolter', 10)]
                else:
                    return [Gear('Storm bolter')]

        def __init__(self, parent):
            super(TerminatorSquad.Justicar, self).__init__(
                name='Terminator Justicar',
                parent=parent, points=TerminatorSquad.model_points,
                gear=TerminatorSquad.model_gear
            )
            self.wep1 = SpecialIssueWeapon(self, self.MeleeWeapon, 'Melee weapon')
            self.wep2 = self.RangedOption(self)
            self.opt = SpecialIssue(self)

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(TerminatorSquad.Transport, self).__init__(parent, 'Transport', limit=1)
            self.models = [
                SubUnit(self, LandRaider(parent=None)),
                SubUnit(self, LandRaiderCrusader(parent=None)),
                SubUnit(self, LandRaiderRedeemer(parent=None))
            ]

    class Terminator(ListSubUnit):

        class MeleeWeapon(Melee, NFSword):
            pass

        class RangedWeapon(TerminatorSpecial):
            pass

        def __init__(self, parent):
            super(TerminatorSquad.Terminator, self).__init__(
                parent=parent, points=TerminatorSquad.model_points, name='Grey Knight Terminator',
                gear=TerminatorSquad.model_gear
            )
            self.mle = self.MeleeWeapon(self, 'Melee weapon')
            self.rng = self.RangedWeapon(self, 'Ranged weapon')

        @ListSubUnit.count_gear
        def count_special(self):
            return self.rng.cur != self.rng.sb

    def __init__(self, parent):
        super(TerminatorSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Justicar(None))
        self.marines = UnitList(self, self.Terminator, 4, 9)
        self.transport = self.Transport(parent=self)

    def check_rules(self):
        super(TerminatorSquad, self).check_rules()
        spec_total = sum(u.count_special() for u in self.marines.units)
        if 5 * spec_total > self.get_count():
            self.error('In Terminator squad may be only 1 special weapon per 5 models (taken: {0})'.format(spec_total))

    def get_count(self):
        return self.marines.count + 1

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(self.note_transport(super(TerminatorSquad, self).build_statistics()))
