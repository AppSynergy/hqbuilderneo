from builder.core2 import OneOf, Gear, OptionsList
from builder.games.wh40k.roster import Unit


class OfficerFleet(Unit):
    type_name = u'Officer of the Fleet'
    type_id = 'navy_officer_fleet_v1'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(OfficerFleet.Weapon, self).__init__(parent, name='Weapon')
            self.variant('Close combat weapon', 0)
            self.variant('Laspistol', 0)

    def __init__(self, parent, points=20):
        super(OfficerFleet, self).__init__(parent, points=points, gear=[Gear('Frag grenades')])
        self.Weapon(self)
        self.Options(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(OfficerFleet.Options, self).__init__(parent=parent, name='Options')
            self.variant('Digital weapons', 10)
            self.variant('Refractor field', 10)
