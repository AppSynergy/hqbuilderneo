from builder.core2 import OneOf, Gear, OptionsList, ListSubUnit, UnitList
from builder.games.wh40k.roster import Unit


class Squadron(Unit):
    unit_class = None
    unit_min = 1
    unit_max = 3

    def __init__(self, parent):
        super(Squadron, self).__init__(parent)
        self.tanks = UnitList(self, self.unit_class, self.unit_min, self.unit_max)

    def get_count(self):
        return self.tanks.count


class Valkyrie(ListSubUnit):
    # wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Valkyrie-Squadron')
    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Valkyrie.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Multi-laser', 0)
            self.variant('Lascannon', 10)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Valkyrie.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Hellstrike missiles', 0, gear=[Gear('Hellstrike missile', count=2)])
            self.variant('Multiple rocket pods', 10, gear=[Gear('Multiple rocket pod', count=2)])

    class Weapon3(OptionsList):
        def __init__(self, parent):
            super(Valkyrie.Weapon3, self).__init__(parent=parent, name='Sponsons')
            self.variant('Heavy bolters', 20, gear=[Gear('Heavy bolter', count=2)])

    def __init__(self, parent):
        super(Valkyrie, self).__init__(parent=parent, points=125, name="Valkyrie", gear=[Gear('Extra armour'),
                                                                                         Gear('Searchlight')])
        self.Weapon1(self)
        self.Weapon2(self)
        self.Weapon3(self)


class Valkyries(Squadron):
    type_name = u'Valkyries'

    type_id = 'navy_valkyries_v1'
    unit_class = Valkyrie
    unit_max = 4
