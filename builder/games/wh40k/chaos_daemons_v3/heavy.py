__author__ = 'Ivan Truskov'

from builder.core2 import Gear, Count, UnitList,\
    SubUnit, OneOf, OptionsList, ListSubUnit
from builder.games.wh40k.roster import Unit
from options import SlaaneshArtifacts


class SoulGrinder(Unit):
    type_name = u'Soul Grinder'
    type_id = 'sg_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Soul-Grinder')

    class Allegiance(OneOf):
        def __init__(self, parent):
            super(SoulGrinder.Allegiance, self).__init__(parent, 'Devotion')
            self.variant('Daemon of Khorne', 0)
            self.variant('Daemon of Tzeentch', 5)
            self.variant('Daemon of Nurgle', 15)
            self.variant('Daemon of Slaanesh', 15)

    class Options(OptionsList):
        def __init__(self, parent):
            super(SoulGrinder.Options, self).__init__(parent, 'Weapons')
            self.resticted = [
                self.variant('Baleful torrent', 20),
                self.variant('Warp gaze', 25),
                self.variant('Phlegm bombardment', 30)
            ]
            self.variant('Warpsword', 25)

        def check_rules(self):
            super(SoulGrinder.Options, self).check_rules()
            OptionsList.process_limit(self.resticted, 1)

    base_points = 135
    gear = []

    def __init__(self, parent):
        super(SoulGrinder, self).__init__(parent, points=135, gear=[
            Gear('Iron claw'), Gear('Harvester cannon')
        ])
        self.god = self.Allegiance(self)
        self.opt = self.Options(self)


class SkullCannon(Unit):
    type_name = "Skull Cannon of Khorne"
    type_id = 'scannon_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Skull-Cannon-of-Khorne')

    def __init__(self, parent):
        super(SkullCannon, self).__init__(parent, points=125, static=True)


class BurningChariot(Unit):
    type_name = "Burning Chariot of Tzeentch"
    type_id = 'bchariot_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Burning-Chariot-of-Tzeentch')

    class Options(OptionsList):
        def __init__(self, parent):
            super(BurningChariot.Options, self).__init__(parent, 'Options')
            self.variant('Blue Horror Crew', 10)

    class Exalted(Unit):
        name = "Exalted Flamer"
        gear = ['Blue Fire of Tzeentch', 'Pink Fire of Tzeentch']

        def __init__(self, parent):
            super(BurningChariot.Exalted, self).__init__(
                parent, 'Exalted Flamer', gear=[
                    Gear('Blue Fire of Tzeentch'), Gear('Pink Fire of Tzeentch')
                ])
            self.les = Count(self, "Lesser Rewards", 0, 2, 10)
            self.grt = Count(self, "Greater Rewards", 0, 1, 20)

        def check_rules(self):
            super(BurningChariot.Exalted, self).check_rules()
            Count.norm_points(20, [self.les, self.grt])

    def __init__(self, parent):
        super(BurningChariot, self).__init__(parent, points=100)
        self.rider = SubUnit(self, self.Exalted(parent=None))
        self.opt = self.Options(self)


class SeekerCavalcade(Unit):
    type_name = u'Seeker Cavalcade'
    type_id = 'cavalcade_v3'
    # wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Exalted-Seeker-Chariot-of-Slaanesh')

    class Chariot(ListSubUnit):
        type_name = u'Seeker Chariot of Slaanesh'

        class Exalted(Unit):
            type_name = "Exalted Alluress"

            def __init__(self, parent):
                super(SeekerCavalcade.Chariot.Exalted, self).__init__(parent)
                self.les = Count(self, "Lesser Rewards", 0, 2, 10)
                self.grt = Count(self, "Greater Rewards", 0, 1, 20)
                self.art = SlaaneshArtifacts(self, alluress=True)

            def check_rules(self):
                super(SeekerCavalcade.Chariot.Exalted, self).check_rules()
                Count.norm_points(20, [self.les, self.grt])

        class Upgrade(OptionsList):
            def __init__(self, parent):
                super(SeekerCavalcade.Chariot.Upgrade, self).__init__(parent, 'Upgrade')
                self.variant('Upgrade to Exalted Chariot', 35, gear=[])

        def __init__(self, parent):
            super(SeekerCavalcade.Chariot, self).__init__(parent, points=40)
            self.rider = SubUnit(self, self.Exalted(parent=None))
            self.opt = self.Upgrade(self)

        def build_description(self):
            res = super(SeekerCavalcade.Chariot, self).build_description()
            if self.opt.any:
                res.name = 'Exalted Seeker Chariot of Slaanesh'
            return res

        @ListSubUnit.count_unique
        def get_artefacts(self):
            return self.rider.unit.art.description if self.rider.unit.art.any else []

    def __init__(self, parent):
        super(SeekerCavalcade, self).__init__(parent)
        self.models = UnitList(self, self.Chariot, 1, 3)

    def get_unique_gear(self):
        return sum((u.get_artefacts() for u in self.models.units), [])
