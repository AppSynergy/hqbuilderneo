__author__ = 'Ivan Truskov'

from builder.core2 import Count,\
    UnitDescription, Gear
from builder.games.wh40k.roster import Unit
from troops import BaseDaemon


class Crushers(BaseDaemon):
    type_name = "Bloodcrushers of Khorne"
    type_id = 'crushers_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Bloodcrushers-of-Khorne')

    def __init__(self, parent):
        super(Crushers, self).__init__(
            parent,
            base_name='Bloodcrusher',
            leader_name='Bloodhunter',
            base_gear=[Gear('Hellblade')],
            base_points=45,
            banner_name='Banner of blood',
            min_count=3,
            max_count=9
        )


class Flamers(BaseDaemon):
    type_name = "Flamers of Tzeentch"
    type_id = 'flamers_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Flamers-of-Tzeentch')

    def __init__(self, parent):
        super(Flamers, self).__init__(
            parent,
            base_name='Flamer of Tzeentch',
            leader_name='Pyrocaster',
            base_gear=[Gear('Flames of Tzeentch')],
            base_points=23,
            banner_name=None,
            min_count=3,
            max_count=9,
            icon_available=False
        )


class Beasts(Unit):
    type_name = "Beasts of Nurgle"
    type_id = 'beasts_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Beast-of-Nurgle')

    def __init__(self, parent):
        super(Beasts, self).__init__(parent)
        self.models = Count(self, 'Beasts of Nurgle', 1, 9, 52, True,
                            gear=UnitDescription('Beast of Nurgle', 52))

    def get_count(self):
        return self.models.cur


class Fiends(Unit):
    type_name = "Fiends of Slaanesh"
    type_id = 'fiends_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Fiends-of-Slaanesh')

    def __init__(self, parent):
        super(Fiends, self).__init__(parent)
        self.models = Count(self, 'Fiends of Slaanesh', 3, 9, 35, True,
                            gear=UnitDescription('Fiend of Slaanesh', 35))

    def get_count(self):
        return self.models.cur
