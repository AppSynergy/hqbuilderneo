__author__ = 'Ivan Truskov'

from builder.core2 import Gear, Count, OptionalSubUnit,\
    UnitDescription, SubUnit, OptionsList
from builder.games.wh40k.roster import Unit
from troops import BaseDaemon


class Hounds(Unit):
    type_name = "Flesh Hounds of Khorne"
    type_id = 'hounds_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Flesh-Hounds-of-Khorne')

    def __init__(self, parent):
        super(Hounds, self).__init__(parent)
        self.models = Count(self, self.type_name, 5, 20, 16, True,
                            gear=UnitDescription('Flesh Hound', 16, options=[Gear('Collar of Khorne')]))

    def get_count(self):
        return self.models.cur


class Screamers(Unit):
    type_name = "Screamers of Tzeentch"
    type_id = 'screamers_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Screamers-of-Tzeentch')

    def __init__(self, parent):
        super(Screamers, self).__init__(parent)
        self.models = Count(self, self.type_name, 3, 9, 25, True,
                            gear=UnitDescription('Screamer', 25))

    def get_count(self):
        return self.models.cur


class Drones(Unit):
    type_name = u'Plague Drones of Nurgle'
    type_id = 'drones_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Plague-Drones-of-Nurgle')

    class Plaguebringer(Unit):
        def __init__(self, parent, common_opt):
            self.common_opt = common_opt
            super(Drones.Plaguebringer, self).__init__(parent, 'Plaguebringer', 42 + 5, [Gear('Plaguesword')])

            self.les = Count(self, "Lesser Rewards", 0, 2, 10)
            self.grt = Count(self, "Greater Rewards", 0, 1, 20)

        def check_rules(self):
            super(Drones.Plaguebringer, self).check_rules()
            Count.norm_points(20, [self.les, self.grt])

        def build_description(self):
            res = super(Drones.Plaguebringer, self).build_description()
            res.add(self.common_opt.description).add_points(self.common_opt.points)
            return res

    class Leader(OptionalSubUnit):
        def __init__(self, parent, common_opt):
            super(Drones.Leader, self).__init__(parent, 'Leader')
            SubUnit(self, Drones.Plaguebringer(None, common_opt))

    class Options(OptionsList):
        def __init__(self, parent):
            super(Drones.Options, self).__init__(parent, 'Options')
            self.icon = self.variant('Chaos Icon', 15)
            self.banner = self.variant('Plague banner', 25)
            self.instrument = self.variant('Instrument of Chaos', 10)

        def check_rules(self):
            super(Drones.Options, self).check_rules()
            self.banner.active = self.banner.used = self.icon.value

    class Common(OptionsList):
        def __init__(self, parent):
            super(Drones.Common, self).__init__(parent, 'Unit options', limit=1)
            self.variant("Death's heads", 5, per_model=True)
            self.variant('Rot proboscis', 5, per_model=True)

    def __init__(self, parent):
        super(Drones, self).__init__(parent)
        self.common = self.Common(self)
        self.warriors = Count(self, 'Plague Drones', 3, 9, 42, True,
                          gear=UnitDescription('Plague Drone', 42, options=[Gear('Plaguesword')]))
        self.leader = self.Leader(self, self.common)
        self.opt = self.Options(self)

    def check_rules(self):
        ldr = self.leader.count
        self.warriors.min, self.warriors.max = (3 - ldr, 9 - ldr)

    def get_count(self):
        return self.warriors.cur + self.leader.count

    def build_description(self):
        res = UnitDescription(self.type_name, self.build_points(), self.get_count())
        res.add(self.leader.description)

        model_desc = self.warriors.gear.clone()
        model_desc.add(self.common.description).add_points(self.common.points)
        generic = self.warriors.cur
        if self.opt.banner.value and self.opt.banner.used:
            res.add(model_desc.clone().add(self.opt.banner.gear)
                    .add_points(self.opt.banner.points)
                    .add_points(self.opt.icon.points))
            generic -= 1
        else:
            if self.opt.icon.value:
                res.add(model_desc.clone().add(self.opt.icon.gear)
                        .add_points(self.opt.icon.points))
                generic -= 1
        if self.opt.instrument.value:
            res.add(model_desc.clone().add(self.opt.instrument.gear)
                    .add_points(self.opt.instrument.points))
            generic -= 1
        res.add(model_desc.set_count(generic))
        return res

    def build_points(self):
        return super(Drones, self).build_points() + (self.get_count() - 1) * self.common.points


class Fury(Unit):
    type_name = "Chaos Furies"
    type_id = 'furies_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Chaos-Furies')

    class Devotion(OptionsList):
        def __init__(self, parent):
            super(Fury.Devotion, self).__init__(parent, 'Devotion', limit=1)
            self.variant('Daemons of Khorne', 2, per_model=True)
            self.variant('Daemons of Tzeentch', 1, per_model=True)
            self.variant('Daemons of nurgle', 2, per_model=True)
            self.variant('Daemons of Slaanesh', 2, per_model=True)

    def __init__(self, parent):
        super(Fury, self).__init__(parent, points=5)
        self.models = Count(self, self.type_name, 5, 20, 6, True)
        self.devotion = self.Devotion(self)

    def build_points(self):
        return self.base_points + self.models.points + self.get_count() * self.devotion.points

    def build_description(self):
        res = UnitDescription('Chaos Furies', count=self.models.cur, points=self.build_points())
        res.add(self.devotion.description)
        model = UnitDescription('Chaos Fury', 6, self.models.cur)
        model.add_points(self.devotion.points)
        res.add(model)
        return res

    def get_count(self):
        return self.models.cur


class Seekers(BaseDaemon):
    type_name = "Seekers of Slaanesh"
    type_id = 'seekers_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Seekers-of-Slaanesh')

    def __init__(self, parent):
        super(Seekers, self).__init__(
            parent,
            base_name='Seeker of Slaanesh',
            leader_name='Heartseeker',
            base_gear=[],
            base_points=12,
            banner_name='Rapturous standard',
            min_count=5,
            max_count=20
        )


class Hellflayer(Unit):
    type_name = u'Hellflayer of Slaanesh'
    type_id = 'hellflayer_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Hellflayer-of-Slaanesh')

    def __init__(self, parent):
        super(Hellflayer, self).__init__(parent, points=60, gear=[
            Gear("Exalted Alluress"), Gear("Hellflayer Chariot")
        ], static=True)
