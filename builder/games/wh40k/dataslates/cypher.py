__author__ = 'Denis Romanow'

from builder.core2 import *
from builder.games.wh40k.roster import Unit, Wh40kImperial, Wh40k7ed
from builder.games.wh40k.dark_angels_v3 import faction as fac_da


class Cypher(Unit):
    type_name = u'Cypher, Lord of the Fallen'
    type_id = 'cypher_v1'

    def __init__(self, parent):
        super(Cypher, self).__init__(parent, name='Cypher', points=190, unique=True,
                                     static=True, gear=[
                                         Gear('Cypher\'s Pistols'),
                                         Gear('Frag grenades'), Gear('Krak grenades')
                                     ])

    @staticmethod
    def unforgiven_check(roster):
        for sec in roster.sections:
            for unit in sec.units:
                # for now, we simply check faction as there are no mixes
                if hasattr(unit, 'faction') and\
                   unit.faction == fac_da:
                    roster.error('Cypher cannot be included in army containing Dark Angel models')
                    return

    def check_rules(self):
        self.root.extra_checks['the_hunt'] = Cypher.unforgiven_check


class Fallen(Unit):
    type_name = u'Fallen'
    type_id = 'fallen_v1'

    class SingleFallen(ListSubUnit):

        class Weapon(OptionsList):
            def __init__(self, parent):
                super(Fallen.SingleFallen.Weapon, self).__init__(parent, name='Weapon', limit=1)
                bp = [Gear('Bolt pistol')]
                bg = [Gear('Boltgun')]
                ccw = [Gear('Close combat weapon')]

                self.lc = self.variant('Lightning claw', 15, gear=[Gear('Lightning claw')] + bg + bp)
                self.pw = self.variant('Power weapon', 15, gear=[Gear('Power weapon')] + bg + bp)
                self.pf = self.variant('Power fist', 25, gear=[Gear('Power fist')] + bg + bp)

                self.pp = self.variant('Plasma pistol', 15, gear=[Gear('Plasma pistol')] + bg + ccw)

                self.fl = self.variant('Flamer', 5, gear=[Gear('Flamer')] + bp + ccw)
                self.mg = self.variant('Meltagun', 10, gear=[Gear('Meltagun')] + bp + ccw)
                self.hb = self.variant('Heavy bolter', 10, gear=[Gear('Heavy bolter')] + bp + ccw)
                self.ac = self.variant('Autocannon', 10, gear=[Gear('Autocannon')] + bp + ccw)
                self.pg = self.variant('Plasma gun', 15, gear=[Gear('Plasma gun')] + bp + ccw)
                self.ml = self.variant('Missile launcher', 15, gear=[Gear('Missile launcher')] + bp + ccw)
                self.las = self.variant('Lascannon', 20, gear=[Gear('Lascannon')] + bp + ccw)

                self.lc2 = self.variant('Pair of lightning claws', 30, gear=Gear('Lightning claw', count=2))

                self.heavy = [self.hb, self.ac, self.ml, self.las]
                self.spec = [self.mg, self.pg, self.fl]
                self.wep = [self.lc, self.pw, self.pf, self.pp, self.lc2]

            def is_heavy(self):
                return any(o.value for o in self.heavy)

            def is_spec(self):
                return any(o.value for o in self.spec)

            def is_wep(self):
                return any(o.value for o in self.wep)

            @property
            def description(self):
                if self.any:
                    return super(Fallen.SingleFallen.Weapon, self).description
                return [Gear('Bolt pistol'), Gear('Boltgun'), Gear('Close combat weapon')]

        def __init__(self, parent):
            super(Fallen.SingleFallen, self).__init__(
                parent, name='Fallen', points=20, gear=[
                    Gear('Frag grenades'), Gear('Krak grenades')])
            self.weapon = self.Weapon(self)

        @ListSubUnit.count_gear
        def count_heavy(self):
            return self.weapon.is_heavy()

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.weapon.is_spec()

        @ListSubUnit.count_gear
        def count_wep(self):
            return self.weapon.is_wep()

    class Champion(Unit):

        class Weapon(OneOf):
            def __init__(self, parent, name, default, melee=False):
                super(Fallen.Champion.Weapon, self).__init__(parent, name)
                self.variant(default)
                if melee:
                    self.variant('Lightning claw', 15)
                    self.variant('Power weapon', 15)
                    self.variant('Power fist', 25)
                self.spec = [
                    self.variant('Combi-flamer', 10),
                    self.variant('Combi-melta', 10),
                    self.variant('Combi-plasma', 10),
                    self.variant('Plasma pistol', 15)
                ]

            def is_spec(self):
                return self.cur in self.spec

        class Options(OptionsList):
            def __init__(self, parent):
                super(Fallen.Champion.Options, self).__init__(parent, name='Options')
                self.variant('Melta bombs', 5)

        def __init__(self, parent):
            super(Fallen.Champion, self).__init__(
                parent, points=20, name='Fallen Champion', gear=[
                    Gear('Frag grenades'), Gear('Krak grenades')]
            )

            self.wep1 = self.Weapon(self, name='Weapon', default='Close combat weapon', melee=True)
            self.wep2 = self.Weapon(self, name='', default='Bolt pistol', melee=True)
            self.wep3 = self.Weapon(self, name='', default='Boltgun')
            self.Options(self)

        def count_spec(self):
            return sum((1 if w.is_spec() else 0) for w in [self.wep1, self.wep2, self.wep3])

        def check_rules(self):
            super(Fallen.Champion, self).check_rules()
            if self.count_spec() > 1:
                self.error('Only one combi-weapon or plasma pistol may be taken')

    def __init__(self, parent):
        super(Fallen, self).__init__(parent)
        self.champion = SubUnit(self, self.Champion(None))
        self.models = UnitList(self, self.SingleFallen, min_limit=4, max_limit=9)

    def check_rules(self):
        super(Fallen, self).check_rules()

        heavy = sum(o.count_heavy() for o in self.models.units)
        spec = sum(o.count_spec() for o in self.models.units)
        wep = sum(o.count_wep() for o in self.models.units)

        if heavy > 1:
            self.error("Only one Fallen can exchange his bolter for heavy weapon. (taken: {})".format(heavy))
        elif heavy == 0:
            if spec > 0:
                spec -= 1
        if spec + wep > 4:
            self.error("Only 4 Fallen can take special weapons (taken: {0}).".format(spec + wep))


class FallenChampions(Formation):
    army_name = 'Fallen Champions'
    army_id = 'fallen_champion'

    def __init__(self):
        super(FallenChampions, self).__init__()
        UnitType(self, Cypher, min_limit=1, max_limit=1)
        UnitType(self, Fallen, min_limit=1, max_limit=3)


faction = 'Fallen Angels'


class FallenAngels(Wh40kImperial, Wh40k7ed):
    army_id = 'fallen_angels_v1'
    army_name = 'Fallen Angels'
    faction = faction

    def __init__(self):
        super(FallenAngels, self).__init__([FallenChampions])


detachments = [FallenChampions]
unit_types = [Cypher, Fallen]
