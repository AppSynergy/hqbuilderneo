from builder.core2 import Unit as CoreUnit, ListSubUnit, UnitList


class Unit(CoreUnit):
    template_link = 'https://sites.google.com/site/wh40000rules/codices/{codex_name}/profiles#TOC-{unit_name}'

    def build_statistics(self):
        return {'Models': self.get_count(), 'Units': 1}

    def count_charges(self):
        return 0

    def note_transport(self, statistic):
        if getattr(self, 'transport', None) and self.transport.any:
            statistic['Models'] += 1
            statistic['Units'] += 1
        return statistic

    def note_charges(self, statistic):
        wc = self.count_charges()
        if wc:
            statistic['Warp charges'] = wc
        return statistic


class Squadron(Unit):
    unit_class = None
    unit_min = 1
    unit_max = 3

    def get_subclass(self):
        class VehicleSubclass(self.unit_class, ListSubUnit):
            pass
        return VehicleSubclass

    def __init__(self, *args, **kwargs):
        super(Squadron, self).__init__(*args, **kwargs)
        self.vehicles = UnitList(self, self.get_subclass(),
                                 self.unit_min, self.unit_max)

    def get_count(self):
        return self.vehicles.count
