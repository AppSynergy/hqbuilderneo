__author__ = 'Denis Romanow'

from builder.core2 import *
from builder.games.wh40k.roster import LordsOfWarSection


class Obelisk(Unit):
    type_id = 'obelisk_v1'
    type_name = 'Obelisk'

    def __init__(self, parent):
        super(Obelisk, self) .__init__(parent=parent, points=335, gear=[Gear('Tesla sphere', count=4)])


class TesseractVault(Unit):
    type_id = 'tesseract_vault_v1'
    type_name = 'Tesseract Vault'

    def __init__(self, parent):
        super(TesseractVault, self) .__init__(parent=parent, points=315)
        self.opt = self.Options(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(TesseractVault.Options, self).__init__(parent=parent, name='Ascendant Powers', limit=2)
            self.variant('Cosmic Fire', 60)
            self.variant('Sky of Falling Stars', 115)
            self.variant('Transdimensional Maelstrom', 120)
            self.variant('Wave of Withering', 120)
            self.variant('Antimatter Meteor', 150)
            self.variant('Seismic Assault', 200)

        def check_rules(self):
            super(TesseractVault.Options, self).check_rules()
            if self.count != 2:
                self.parent.error('A {} must take two different Ascendant Powers.'.format(self.parent.type_name))


class TranscendentCtan(Unit):
    type_id = 'transcendent_ctan_v1'
    type_name = 'Transcendent C\'tan'

    def __init__(self, parent):
        super(TranscendentCtan, self) .__init__(parent=parent, points=420)
        self.pwr = self.Powers(self)
        self.opt = TesseractVault.Options(self)

    class Powers(OneOf):
        def __init__(self, parent):
            super(TranscendentCtan.Powers, self).__init__(parent=parent, name='Ascendant Powers')
            self.variant('Storm of Heavenly Fire', 50)
            self.variant('Seismic Shockwave', 60)
            self.variant('Transliminal Stride', 120)


class LordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent=parent)
        UnitType(self, Obelisk)
        UnitType(self, TesseractVault)
        UnitType(self, TranscendentCtan)
