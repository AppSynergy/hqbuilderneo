__author__ = 'dante'

from builder.core2 import *
from builder.games.wh40k.roster import Unit
from armory import *


class CarnifexBrood(Unit):
    type_name = u'Carnifex Brood'
    type_id = 'carnifex_brood_v1'

    class Warrior(ListSubUnit):

        class Options(OptionsList):
            def __init__(self, parent):
                super(CarnifexBrood.Warrior.Options, self).__init__(parent, name='Options')
                self.variant('Spine banks', 5, per_model=True)
                self.variant('Bio-plasma', 20, per_model=True)

        class Tail(OptionsList):
            def __init__(self, parent):
                super(CarnifexBrood.Warrior.Tail, self).__init__(parent, name='Tail biomorphs', limit=1)
                self.variant('Thresher scythe', 10, per_model=True)
                self.variant('Bone mace', 15, per_model=True)

        class Claws(MonstrousWeapon):
            def __init__(self, parent, name):
                super(CarnifexBrood.Warrior.Claws, self).__init__(parent, name=name)
                self.claws = self.variant('Crushing claws', 15)

            def get_model_unique_gear(self):
                return super(CarnifexBrood.Warrior.Claws, self).get_model_unique_gear() + [self.claws]

        class Weapon(MonstrousBioCannons, Claws, ScythingTalons):
            pass

        def __init__(self, parent):
            super(CarnifexBrood.Warrior, self).__init__(parent, name='Carnifex', points=120)

            self.w1 = self.Weapon(self, name='Weapon')
            self.w2 = self.Weapon(self, name='')
            Biomorphs(self)
            self.Tail(self)
            self.Options(self)

        def check_rules(self):
            for r1, r2 in zip(self.w1.get_model_unique_gear(), self.w2.get_model_unique_gear()):
                r2.active = not self.w1.cur == r1
                r1.active = not self.w2.cur == r2

        @ListSubUnit.count_gear
        def has_cannon(self):
            return self.w1.is_monstrous_cannon() or self.w2.is_monstrous_cannon()

    def __init__(self, parent):
        super(CarnifexBrood, self).__init__(parent=parent)
        self.models = UnitList(parent=self, unit_class=self.Warrior, min_limit=1, max_limit=3)

    def get_count(self):
        return self.models.count

    def all_have_cannons(self):
        return self.get_count() == sum(u.has_cannon() for u in self.models.units)


class Biovore(Unit):
    type_name = u'Biovore Brood'
    type_id = 'biovore_v1'

    model_name = 'Biovore'
    model_points = 40

    def __init__(self, parent):
        super(Biovore, self).__init__(parent)
        self.models = Count(
            self, self.model_name, 1, 3, self.model_points, per_model=True,
            gear=UnitDescription(self.model_name, points=self.model_points, options=[Gear('Spore Mine launcher')])
        )

    def get_count(self):
        return self.models.cur


class Trygon(Unit):
    type_name = u'Trygon'
    type_id = 'trygon_v1'

    class Options(OptionsList):
        def __init__(self, parent):
            super(Trygon.Options, self).__init__(parent, name='Tail biomorphs', limit=1)
            self.variant('Prehensile pincer', 10)
            self.variant('Toxinspike', 10)

    def __init__(self, parent):
        super(Trygon, self).__init__(parent, points=190, gear=[
            Gear('Bio-electric pulse'),
            Gear('Scything talons', count=2)
        ])
        Biomorphs(self)
        self.Options(self)


class Exocrine(Unit):
    type_name = u'Exocrine'
    type_id = 'exocrine_v1'

    class Options(OptionsList):
        def __init__(self, parent):
            super(Exocrine.Options, self).__init__(parent, name='Tail biomorphs')
            self.variant('Thresher scythe', 10)

    def __init__(self, parent):
        super(Exocrine, self).__init__(parent, points=170, gear=[
            Gear('Bio-plasmic cannon'),
            Gear('Scything talons')
        ])
        Biomorphs(self)
        self.Options(self)


class Mawloc(Unit):
    type_name = u'Mawloc'
    type_id = 'mawloc_v1'

    def __init__(self, parent):
        super(Mawloc, self).__init__(parent, points=140)
        Biomorphs(self)
        Trygon.Options(self)


class TrygonPrime(Unit):
    type_name = u'Trygon Prime'
    type_id = 'trygon_prime_v1'

    class Weapon(BioArtefactsWeapons, ScythingTalons):
        pass

    def __init__(self, parent):
        super(TrygonPrime, self).__init__(parent, points=230, gear=[
            Gear('Bio-electric pulse'),
        ])
        self.w1 = self.Weapon(self, name='Weapon')
        self.w2 = self.Weapon(self, name='')
        self.art = BioArtefacts(self)
        Biomorphs(self)
        Trygon.Options(self)

    def check_rules(self):
        for r1, r2 in zip(self.w1.get_model_unique_gear(), self.w2.get_model_unique_gear()):
            r2.active = not self.w1.cur == r1
            r1.active = not self.w2.cur == r2

    def get_unique_gear(self):
        return self.w1.get_unique_gear() + self.w2.get_unique_gear() + self.art.get_unique_gear()


class Tyrannofex(Unit):
    type_name = u'Tyrannofex'
    type_id = 'tyrannofex_v1'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Tyrannofex.Weapon, self).__init__(parent, name='Weapon')
            self.variant('Acid spray', 0)
            self.variant('Fleshborer hive', 5)
            self.variant('Rupture cannon', 30)

    def __init__(self, parent):
        super(Tyrannofex, self).__init__(parent, points=175, gear=[
            Gear('Stinger salvo'),
        ])
        self.Weapon(self)
        Biomorphs(self)
        ThoraxBiomorphs(self)


class Toxicrene(Unit):
    type_name = u'Toxicrene'
    type_id = 'toxicrene_v1'

    def __init__(self, parent):
        super(Toxicrene, self).__init__(parent, self.type_name,
                                        160, [Gear('Acid blood'), Gear('Choking cloud'), Gear('Lash whips'), Gear('Toxic miasma')],
                                        static=True)


class Tyrannocyte(Unit):
    type_name = u'Tyrannocyte'
    type_id = 'tyrannocyte_v1'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Tyrannocyte.Weapon, self).__init__(parent, name='Weapon')
            self.variant('Five deathspitters', 0,
                         gear=[Gear('Deathspitter', count=5)])
            self.variant('Five barbed stranglers', 25,
                         gear=[Gear('Barbed strangler', count=5)])
            self.variant('Five venom cannons', 25,
                         gear=[Gear('Venom cannon', count=5)])

    def __init__(self, parent):
        super(Tyrannocyte, self).__init__(parent, self.type_name, points=75)
        self.Weapon(self)


class Sporocyst(Unit):
    type_name = u'Sporocyst'
    type_id = 'sporocyst_v1'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Sporocyst.Weapon, self).__init__(parent, name='Weapon')
            self.variant('Five deathspitters', 0,
                         gear=[Gear('Deathspitter', count=5)])
            self.variant('Five barbed stranglers', 25,
                         gear=[Gear('Barbed strangler', count=5)])
            self.variant('Five venom cannons', 25,
                         gear=[Gear('Venom cannon', count=5)])

    def __init__(self, parent):
        super(Sporocyst, self).__init__(parent, self.type_name, points=75)
        self.Weapon(self)


class BeastOfPhodia(Unit):
    type_name = u'The Beast of Phodia'
    type_id = 'beast_phodia_v3'

    def __init__(self, parent):
        super(BeastOfPhodia, self).__init__(parent, points=170, static=True,
                                            unique=True, gear=[
                                                Gear('Bio-plasma'),
                                                Gear('Stranglethorn cannon'),
                                                Gear('Scything talons')])
