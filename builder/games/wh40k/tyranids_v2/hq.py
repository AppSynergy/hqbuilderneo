__author__ = 'dante'

from builder.core2 import *
from builder.games.wh40k.roster import Unit
from armory import *


class HiveTyrant(Unit):
    type_name = u'Hive Tyrant'
    type_id = 'hive_tyrant_1'
    tyrant_gear = []

    class Options(OptionsList):
        def __init__(self, parent):
            super(HiveTyrant.Options, self).__init__(parent, name='Options')
            self.variant('Indescribable Horror', 10)
            self.variant('Old Adversary', 15)
            self.variant('Hive Commander', 20)
            self.variant('Prehensile pincer', 10)
            self.wings = self.variant('Wings', 35)

    class Weapon(BioArtefactsWeapons, MeleeBioWeapons, MonstrousBioCannons, ScythingTalons):
        pass

    def __init__(self, parent):
        super(HiveTyrant, self). __init__(parent, points=165, gear=self.tyrant_gear)
        self.w1 = self.Weapon(self, name='Weapon')
        self.w2 = self.Weapon(self, name='')

        self.art = BioArtefacts(self)
        self.morphs = Biomorphs(self)
        ThoraxBiomorphs(self)
        self.opt = self.Options(self)

    def check_rules(self):
        for r1, r2 in zip(self.w1.get_model_unique_gear(), self.w2.get_model_unique_gear()):
            r2.active = not self.w1.cur == r1
            r1.active = not self.w2.cur == r2

    def get_unique_gear(self):
        return self.w1.get_unique_gear() + self.w2.get_unique_gear() + self.art.get_unique_gear()

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(HiveTyrant, self).build_statistics())


class TheSwarmlord(Unit):
    type_name = u'The Swarmlord'
    type_id = 'the_swarmlord_v1'

    def __init__(self, parent):
        super(TheSwarmlord, self).__init__(parent, points=285, unique=True, gear=[Gear('Pair of Bone Sabres', count=2)])

    def count_charges(self):
        return 3

    def build_statistics(self):
        return self.note_charges(super(TheSwarmlord, self).build_statistics())


class GuardBrood(Unit):
    type_name = u'Tyrant Guard Brood'
    type_id = 'tyrant_guard_brood_v1'

    model_gear = [Gear('Rending claws')]
    model_points = 50
    model = UnitDescription('Tyrant Guard', points=model_points, options=model_gear)

    class GuardCount(Count):
        @property
        def description(self):
            return [GuardBrood.model.clone().add(Gear('Scything talons')).set_count(
                self.cur - self.parent.claws.cur - self.parent.bs.cur
            )]

    class Options(OptionsList):
        def __init__(self, parent):
            super(GuardBrood.Options, self).__init__(parent, name='Options')
            self.variant('Toxin sacs', points=3, per_model=True)
            self.variant('Adrenal glands', points=5, per_model=True)

        @property
        def points(self):
            return super(GuardBrood.Options, self).points * self.parent.get_count()

    def __init__(self, parent):
        super(GuardBrood, self).__init__(parent)
        self.models = self.GuardCount(self, 'Tyrant Guard', 1, 3, 50)
        self.claws = Count(
            self, name='Crushing claws', min_limit=0, max_limit=1, points=20, per_model=True,
            gear=GuardBrood.model.clone().add(Gear('Crushing claws')).add_points(20)
        )
        self.bs = Count(
            self, name='Lash whip and bonesword', min_limit=0, max_limit=1, points=20, per_model=True,
            gear=GuardBrood.model.clone().add(Gear('Lash whip')).add(Gear('Bonesword')).add_points(20)
        )
        self.Options(self)

    def check_rules(self):
        super(GuardBrood, self).check_rules()
        Count.norm_counts(0, self.get_count(), [self.claws, self.bs])

    def get_count(self):
        return self.models.cur


class OldOneEye(Unit):
    type_name = u'Old One Eye'
    type_id = 'oldoneeye_v1'

    def __init__(self, parent):
        super(OldOneEye, self).__init__(
            parent, points=220, unique=True,
            gear=[
                Gear('Crushing claws'),
                Gear('Scything talons'),
                Gear('Thresher scythe'),
                Gear('Regeneration'),
            ]
        )


class Tervigon(Unit):
    type_name = u'Tervigon'
    type_id = 'tervigon_v1'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Tervigon.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Stinger salvo', 0)
            self.variant('Cluster spines', 5)

    class TervigonWeapon(OneOf):
        def __init__(self, parent, name):
            super(Tervigon.TervigonWeapon, self).__init__(parent, name=name)
            self.variant('Scything talons', 0)
            self.variant('Crushing claws', 15)

    class Weapon2(BioArtefactsWeapons, TervigonWeapon):
        pass

    def __init__(self, parent):
        super(Tervigon, self).__init__(parent, name='Tervigon', points=195)

        self.Weapon1(self)
        self.w2 = self.Weapon2(self, name='')
        Biomorphs(self)
        ThoraxBiomorphs(self)
        self.art = BioArtefacts(self)

    def get_unique_gear(self):
        return self.w2.get_unique_gear() + self.art.get_unique_gear()

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(super(Tervigon, self).build_statistics())


class TyranidPrime(Unit):
    type_name = u'Tyranid Prime'
    type_id = 'tyranid_prime_v1'

    class Options(OptionsList):
        def __init__(self, parent):
            super(TyranidPrime.Options, self).__init__(parent, name='Options')
            self.variant('Flesh hooks', 5)

    class Weapon1(BioArtefactsWeapons, MeleeBioWeapons, ScythingTalons):
        pass

    class Weapon2(BioArtefactsWeapons, MeleeBioWeapons, BasicBioWeapons, Devourer):
        pass

    def __init__(self, parent):
        super(TyranidPrime, self).__init__(parent, points=125)

        self.w1 = self.Weapon1(self, name='Weapon')
        self.w2 = self.Weapon2(self, name='')
        Biomorphs(self)
        self.art = BioArtefacts(self)
        self.Options(self)

    def check_rules(self):
        for r1, r2 in zip(self.w1.get_model_unique_gear(), self.w2.get_model_unique_gear()):
            r2.active = not self.w1.cur == r1
            r1.active = not self.w2.cur == r2

    def get_unique_gear(self):
        return self.w1.get_unique_gear() + self.w2.get_unique_gear() + self.art.get_unique_gear()


class Deathleaper(Unit):
    type_name = "Deathleaper"
    type_id = 'deathleaper_v1'

    def __init__(self, parent):
        super(Deathleaper, self).__init__(parent, points=130, unique=True, gear=[
            Gear('Rending claws'),
            Gear('Scything talons'),
            Gear('Flesh hooks'),
        ])
