__author__ = 'Denis Romanov'

from builder.core2 import *
from builder.games.wh40k.roster import Unit
from armory import *


class TyranidWarriorBrood(Unit):
    type_name = u'Tyranid Warrior Brood'
    type_id = 'tyranid_warrior_brood_v1'

    class Options(OptionsList):
        def __init__(self, parent):
            super(TyranidWarriorBrood.Options, self).__init__(parent, name='Options')
            self.variant('Toxin sacs', 3, per_model=True)
            self.variant('Flesh hooks', 4, per_model=True)
            self.variant('Adrenalin glands', 5, per_model=True)

        @property
        def points(self):
            return super(TyranidWarriorBrood.Options, self).points * self.parent.get_count()

    class Warrior(ListSubUnit):
        class Weapon1(MeleeBioWeapons, ScythingTalons):
            pass

        class Weapon2(BasicBioCannons, MeleeBioWeapons, BasicBioWeapons, Devourer):
            pass

        def __init__(self, parent):
            super(TyranidWarriorBrood.Warrior, self).__init__(parent, name='Tyranid Warrior', points=30)

            self.w1 = self.Weapon1(self, name='Weapon')
            self.w2 = self.Weapon2(self, name='')

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.w2.has_heavy()

    def __init__(self, parent):
        super(TyranidWarriorBrood, self).__init__(parent=parent)
        self.models = UnitList(parent=self, unit_class=self.Warrior, min_limit=3, max_limit=9)
        self.Options(self)

    def count_cannons(self):
        return sum(c.has_heavy() for c in self.models.units)

    def check_rules(self):
        super(TyranidWarriorBrood, self).check_rules()
        spec = self.count_cannons()
        if spec > 1:
            self.error('Only one Tyranid Warrior may take basic bio-cannons (taken {})'.format(spec))

    def get_count(self):
        return self.models.count


class Genestealer(Unit):
    type_name = u'Genestealer Brood'
    type_id = 'genestealer_brood_v1'

    model_gear = [Gear('Rending claws')]
    model_points = 14
    model = UnitDescription('Genestealer', points=model_points, options=model_gear)

    class Broodlord(Unit):

        class Options(OptionsList):
            def __init__(self, parent):
                super(Genestealer.Broodlord.Options, self).__init__(parent, name='Weapon')
                self.variant('Scything talons', points=4)

        def __init__(self, parent):
            super(Genestealer.Broodlord, self).__init__(parent, points=60, name='Broodlord',
                                                        gear=Genestealer.model_gear)
            self.Options(self)
            Biomorphs(self)

    class GenestealerCount(Count):
        @property
        def description(self):
            return [Genestealer.model.clone().set_count(self.cur - self.parent.talons.cur)]

    class Options(OptionsList):
        def __init__(self, parent):
            super(Genestealer.Options, self).__init__(parent, name='Options')
            self.variant('Adrenal glands', points=2, per_model=True)
            self.variant('Toxin sacs', points=3, per_model=True)

        @property
        def points(self):
            return super(Genestealer.Options, self).points * self.parent.models.cur

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(Genestealer.Leader, self).__init__(parent=parent, name='')
            self.lord = SubUnit(self, Genestealer.Broodlord(parent=None))

    def __init__(self, parent):
        super(Genestealer, self).__init__(parent)
        self.leader = self.Leader(self)
        self.models = self.GenestealerCount(self, 'Genestealer', 5, 20, self.model_points)
        self.talons = Count(
            self, name='Scything talons', min_limit=0, max_limit=5, points=4, per_model=True,
            gear=Genestealer.model.clone().add(Gear('Scything talons')).add_points(4)
        )
        self.Options(self)

    def check_rules(self):
        super(Genestealer, self).check_rules()
        self.talons.max = self.models.cur

    def get_count(self):
        return self.models.cur + self.leader.count

    def count_charges(self):
        return self.leader.count

    def build_statistics(self):
        return self.note_charges(super(Genestealer, self).build_statistics())


class Termagant(Unit):
    type_name = u'Termagant Brood'
    type_id = 'termagant_brood_v1'

    model_name = 'Termagant'
    model_points = 4
    model = UnitDescription(model_name, points=model_points)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Termagant.Options, self).__init__(parent, name='Options')
            self.variant('Adrenal glands', points=2, per_model=True)
            self.variant('Toxin sacs', points=2, per_model=True)

        @property
        def points(self):
            return super(Termagant.Options, self).points * self.parent.models.cur

    class GantCount(Count):
        @property
        def description(self):
            return [Termagant.model.clone().add(Gear('Fleshborer')).set_count(
                self.cur - sum(o.cur for o in [self.parent.fist, self.parent.rifle, self.parent.devourer,
                                               self.parent.strangleweb]))]

    def __init__(self, parent):
        super(Termagant, self).__init__(parent)
        self.models = self.GantCount(self, self.model_name, 10, 30, self.model_points, per_model=True)
        self.fist = Count(
            self, name='Spinefists', points=0, min_limit=0, max_limit=10,
            gear=self.model.clone().add(Gear('Spinefists'))
        )
        self.rifle = Count(
            self, name='Spike rifle', points=0, min_limit=0, max_limit=10,
            gear=self.model.clone().add(Gear('Spike rifle'))
        )
        self.devourer = Count(
            self, name='Devourer', min_limit=0, max_limit=10, points=4, per_model=True,
            gear=self.model.clone().add(Gear('Devourer')).add_points(4)
        )
        self.strangleweb = Count(
            self, name='Strangleweb', min_limit=0, max_limit=1, points=5, per_model=True,
            gear=self.model.clone().add(Gear('Strangleweb')).add_points(5)
        )
        self.Options(self)

    def check_rules(self):
        super(Termagant, self).check_rules()
        self.strangleweb.max = int(self.models.cur / 10)
        Count.norm_counts(0, self.get_count() - self.strangleweb.cur, [self.rifle, self.fist, self.devourer])

    def get_count(self):
        return self.models.cur


class Hormagant(Unit):
    type_name = u'Hormagant Brood'
    type_id = 'hormagant_brood_v1'

    model_name = 'Hormagant'
    model_points = 5

    class Options(OptionsList):
        def __init__(self, parent):
            super(Hormagant.Options, self).__init__(parent, name='Options')
            self.variant('Adrenal glands', points=2, per_model=True)
            self.variant('Toxin sacs', points=3, per_model=True)

        @property
        def points(self):
            return super(Hormagant.Options, self).points * self.parent.models.cur

    def __init__(self, parent):
        super(Hormagant, self).__init__(parent)
        self.models = Count(
            self, self.model_name, 10, 30, self.model_points, per_model=True,
            gear=UnitDescription(self.model_name, points=self.model_points, options=[Gear('Scything talons')])
        )
        self.Options(self)

    def get_count(self):
        return self.models.cur


class Ripper(Unit):
    type_name = u'Ripper Swarm Brood'
    type_id = 'swarm_brood_v1'

    model_name = 'Ripper Swarm'
    model_points = 13

    class Options(OptionsList):
        def __init__(self, parent):
            super(Ripper.Options, self).__init__(parent, name='Options')
            self.fist = self.variant('Spinefists', points=4, per_model=True, gear=[])
            self.variant('Toxin sacs', points=4, per_model=True)
            self.variant('Adrenal glands', points=6, per_model=True)
            self.variant('Deep strike', points=2, per_model=True)

        @property
        def points(self):
            return super(Ripper.Options, self).points * self.parent.models.cur

    class RipperCount(Count):
        @property
        def description(self):
            return [UnitDescription(
                Ripper.model_name,
                points=Ripper.model_points,
                options=[Gear('Spinefist')] if self.parent.opt.fist.value else [],
                count=self.cur
            )]

    def __init__(self, parent):
        super(Ripper, self).__init__(parent)
        self.models = self.RipperCount(self, self.model_name, 3, 9, self.model_points, per_model=True)
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.cur


class MucolidSpores(Unit):
    type_name = u'Mucolid Spore Cluster'
    type_id = 'mucolid'

    def __init__(self, parent):
        super(MucolidSpores, self).__init__(parent, name=self.type_name)
        self.models = Count(self, 'Mucolid Spores', 1, 3, 15, True, gear=UnitDescription('Mucolid spore', 15))

    def get_count(self):
        return self.models.cur


class CryptusChildren(Unit):
    type_name = u'The Children of Cryptus'
    type_id = 'children_cryptus_v3'

    def __init__(self, parent):
        super(CryptusChildren, self).__init__(parent, points=215, static=True,
                                              unique=True, gear=[
                                                  UnitDescription(name='Spawn of Cryptus', options=[Gear('Rending claws')]),
                                                  UnitDescription(name='Genestealer', count=8, options=[Gear('Rending claws'), Gear('Scything talons')])])

    def get_count(self):
        return 9

    def count_charges(self):
        return 1

    def build_statistics(self):
        return self.note_charges(super(CryptusChildren, self).build_statistics())


class PhodianWarriors(Unit):
    type_name = u'Phodian Hive Warriors'
    type_id = 'phodian_warriors_v3'

    common_gear = [Gear('Toxin sacs'), Gear('Adrenal glands')]

    def __init__(self, parent):
        super(PhodianWarriors, self).__init__(parent, points=170, static=True,
                                              unique=True, gear=[
                                                  UnitDescription(name='Phodian Hive Warrior', options=PhodianWarriors.common_gear + [Gear('Scything talons', count=2), Gear('Venom cannon')]),
                                                  UnitDescription(name='Phodian Hive Warrior', options=PhodianWarriors.common_gear + [Gear('Scything talons', count=2), Gear('Bonesword', count=2)]),
                                                  UnitDescription(name='Phodian Hive Warrior', options=PhodianWarriors.common_gear + [Gear('Rending claws', count=2), Gear('Lash whip'), Gear('Bonesword')])])

    def get_count(self):
        return 3
