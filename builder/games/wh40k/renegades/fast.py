__author__ = 'Ivan Truskov'


from builder.core2 import OptionsList, OneOf, Gear,\
    ListSubUnit, UnitList, UnitDescription
from builder.games.wh40k.roster import Unit


class SingleArvus(Unit):
    type_name = u'Renegade Arvus Lighter'

    class Options(OptionsList):
        def __init__(self, parent):
            super(SingleArvus.Options, self).__init__(parent, 'Options')
            self.variant('Flare/chaff laucher', 10)
            self.variant('Armoured cockpit', 15)
            self.variant('Illum flares', 5)
            self.variant('Searchlight', 1)

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(SingleArvus.Weapon, self).__init__(parent, 'Weapon', limit=1)
            self.variant('Twin-linked multi-laser', 10)
            self.variant('Two hellstrike missiles', 5,
                         gear=[Gear('Hellstrike missile', count=2)])
            self.variant('Twin-linked autocannon', 10)
            self.variant('Two twin-linked heavy stubbers', 5,
                         gear=[Gear('Twin-linked heavy stubber', count=2)])

    def __init__(self, parent):
        super(SingleArvus, self).__init__(parent, points=55)
        self.Options(self)
        self.Weapon(self)


class ArvusSquadron(Unit):
    type_name = u'Renegade Arvus Lighter Squadron'
    type_id = 'r_arvus_v1'

    class Arvus(SingleArvus, ListSubUnit):
        pass

    def __init__(self, parent):
        super(ArvusSquadron, self).__init__(parent)
        self.models = UnitList(self, self.Arvus, 1, 3)

    def get_count(self):
        return self.models.count

    def build_statistics(self):
        return {'Models': self.get_count(),
                'Units': self.get_count()}


class Squadron(Unit):
    unit_class = None
    min_unit = 1
    max_unit = 3

    def __init__(self, parent):
        super(Squadron, self).__init__(parent)
        self.tanks = UnitList(self, self.unit_class, self.min_unit, self.max_unit)

    def get_count(self):
        return self.tanks.count


class Hellhound(ListSubUnit):
    type_name = u'Renegade Hellhound'

    def __init__(self, parent):
        super(Hellhound, self).__init__(parent=parent)
        self.type = self.Type(self)
        self.Weapon(self)
        self.Vehicle(self)

    class Type(OneOf):
        def __init__(self, parent):
            super(Hellhound.Type, self).__init__(parent=parent, name='Type')
            self.hellhound = self.variant('Renegade Hellhound', 115, gear=[Gear('Inferno cannon')])
            self.devildog = self.variant('Renegade Devil dog', 125, gear=[Gear('Melta cannon')])
            self.banewolf = self.variant('Renegade Banewolf', 120, gear=[Gear('Chem cannon')])

    class Vehicle(OptionsList):
        def __init__(self, parent):
            super(Hellhound.Vehicle, self).__init__(parent, 'Options')
            self.sb = self.variant('Storm bolter', 5)
            self.hs = self.variant('Heavy stubber', 5)
            self.variant('Hunter-killer missile', 10)
            self.dozen = self.variant('Dozer blade', 5)
            self.variant('Mine plough', 10)
            self.variant('Extra armour', 10)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Hellhound.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant('Heavy bolter', 0)
            self.heavyflamer = self.variant('Heavy flamer', 0)

    def build_points(self):
        res = super(Hellhound, self).build_points()
        return res + self.root_unit.opt.points

    def build_description(self):
        desc = super(Hellhound, self).build_description()
        desc.name = self.type.cur.title
        desc.add(self.root_unit.opt.description)
        return desc


class HellhoundSquadron(Squadron):
    type_name = u'Renegade Hellhound Squadron'
    type_id = 'r_hellhounds_v1'

    class SquadOptions(OptionsList):
        def __init__(self, parent):
            super(HellhoundSquadron.SquadOptions, self).__init__(parent, 'Squadron options')
            self.mt = self.variant('Militia training', 10, per_model=True)
            self.variant('Smoke launchers', 5, per_model=True)
            self.variant('Camo netting', 20, per_model=True)

        def check_rules(self):
            if self.roster.demagogue.is_reaver():
                self.mt.value = True
                self.mt.active = False
            else:
                self.mt.active = True

    unit_class = Hellhound
    max_unit = 4

    def __init__(self, parent):
        super(HellhoundSquadron, self).__init__(parent)
        self.opt = self.SquadOptions(self)

    def build_points(self):
        return self.tanks.points

    def build_description(self):
        return UnitDescription(self.name, self.points, self.get_count(),
                               options=self.tanks.description)


class RenegadeSentinel(ListSubUnit):
    type_name = u'Renegade Sentinel'

    def __init__(self, parent):
        super(RenegadeSentinel, self).__init__(parent=parent, points=35)
        self.Weapon(self)
        self.Options(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(RenegadeSentinel.Weapon, self).__init__(parent=parent, name='Weapon')
            self.multilaser = self.variant('Multi-laser', 0)
            self.heavyflamer = self.variant('Heavy flamer', 0)
            self.autocannon = self.variant('Autocannon', 5)
            self.missilelauncher = self.variant('Missile launcher', 5)
            self.lascannon = self.variant('Lascannon', 10)
            self.variant('Multiple rocket pod', 10)

    class Options(OptionsList):
        def __init__(self, parent):
            super(RenegadeSentinel.Options, self).__init__(parent, 'Options')
            self.variant('Hunter-killer missile', 10)

    def build_description(self):
        res = super(RenegadeSentinel, self).build_description()
        res.add(self.root_unit.opt.description)
        return res


class SentinelSquadron(Squadron):
    type_name = u'Renegade Sentinels Squadron'
    type_id = 'r_sentinels_v1'

    class SquadOptions(OptionsList):
        def __init__(self, parent):
            super(SentinelSquadron.SquadOptions, self).__init__(parent, 'Squadron options')
            self.mt = self.variant('Militia training', 40)
            self.variant('Smoke launchers', 20)
            self.variant('Camo netting', 50)
            self.variant('Heavy arour', 80)

        def check_rules(self):
            if self.roster.demagogue.is_reaver():
                self.mt.value = True
                self.mt.active = False
            else:
                self.mt.active = True

    unit_class = RenegadeSentinel
    min_unit = 3
    max_unit = 6

    def __init__(self, parent):
        super(SentinelSquadron, self).__init__(parent)
        self.opt = self.SquadOptions(self)

    def build_description(self):
        return UnitDescription(self.name, self.points, self.get_count(),
                               options=self.tanks.description)


class Salamander(ListSubUnit):
    type_name = u'Renegade Salamander'

    def __init__(self, parent):
        super(Salamander, self).__init__(parent=parent, points=45,
                                         gear=[Gear('Heavy bolter'), Gear('Searchlight'),
                                               Gear('Smoke launchers')])
        self.Weapon(self)
        self.Vehicle(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Salamander.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavyflamer = self.variant('Heavy flamer', 0)
            self.heavybolter = self.variant('Heavy bolter', 0)
            self.variant('Autocannon', 0)

    class Vehicle(OptionsList):
        def __init__(self, parent):
            super(Salamander.Vehicle, self).__init__(parent, 'Options')
            self.mt = self.variant('Militia training', 10)
            self.sb = self.variant('Storm bolter', 5)
            self.hs = self.variant('Heavy stubber', 5)
            self.variant('Hunter-killer missile', 10)
            self.dozen = self.variant('Dozer blade', 5)
            self.variant('Mine plough', 15)
            self.variant('Extra armour', 15)

        def check_rules(self):
            super(Salamander.Vehicle, self).check_rules()
            if self.roster.demagogue.is_reaver():
                self.mt.value = True
                self.mt.active = False
            else:
                self.mt.active = True


class SalamanderSquadron(Squadron):
    type_name = u'Renegade Salamander Squadron'
    type_id = 'r_salamander_v1'
    unit_class = Salamander


class Valkyrie(ListSubUnit):
    type_name = u'Renegade Valkyrie'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Valkyrie.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Multi-laser', 0)
            self.variant('Lascannon', 10)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Valkyrie.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Hellstrike missiles', 0,
                         gear=[Gear('Hellstrike missile', count=2)])
            self.variant('Multiple rocket pods', 10,
                         gear=[Gear('Multiple rocket pod', count=2)])

    class Weapon3(OptionsList):
        def __init__(self, parent):
            super(Valkyrie.Weapon3, self).__init__(parent=parent, name='Options')
            self.variant('Heavy bolters', 20, gear=[Gear('Heavy bolter', count=2)])
            self.mt = self.variant('Milita training', 10)

        def check_rules(self):
            super(Valkyrie.Weapon3, self).check_rules()
            if self.roster.demagogue.is_reaver():
                self.mt.value = True
                self.mt.active = False
            else:
                self.mt.active = True

    def __init__(self, parent):
        super(Valkyrie, self).__init__(parent=parent, points=115,
                                       name="Renegade Valkyrie",
                                       gear=[Gear('Extra armour'),
                                             Gear('Searchlight')])
        self.Weapon1(self)
        self.Weapon2(self)
        self.Weapon3(self)


class ValkyrieSquadron(Squadron):
    type_name = u'Renegade Valkyrie Squadron'
    type_id = 'r_valkyries_v1'
    unit_class = Valkyrie
