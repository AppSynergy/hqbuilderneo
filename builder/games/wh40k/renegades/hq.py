__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear, SubUnit,\
    Count, OptionalSubUnit, UnitDescription, ListSubUnit,\
    UnitList
from builder.games.wh40k.roster import Unit, PrimaryDetachment
from transport import Chimera


class Devotions(OptionsList):
    def __init__(self, parent):
        super(Devotions, self).__init__(parent, 'Devotions')
        self.tyrant = self.variant('Ordnance Tyrant', 30)
        self.witch = self.variant('Primaris-rogue Witch', 35)
        self.ml2 = self.variant('Mastery level 2', 25)
        self.mutant = self.variant('Mutant Overlord', 15)
        self.horde = self.variant('Master of the Horde', 20)
        self.che = self.variant('Arch-heretic Revolutionary', 25)
        self.shock = self.variant('Shock Legion Taskmaster', 25,
                                  gear=[Gear('Neural Goad'), Gear('Shock Legion Taskmaster')])
        self.magus = self.variant('Heretek Magus', 30)
        self.reaver = self.variant('Bloody-handed Reaver', 20)

    def check_rules(self):
        super(Devotions, self).check_rules()
        OptionsList.process_limit([self.witch, self.mutant, self.horde,
                                   self.che, self.magus, self.reaver,
                                   self.tyrant, self.shock], 1)
        self.witch.points = 25 if self.roster.is_vraks else 35
        self.ml2.value = self.ml2.value and self.witch.value
        self.ml2.active = self.witch.value
        if self.roster.is_vraks:
            for var in [self.mutant, self.magus, self.reaver]:
                var.used = var.visible = False
        else:
            for var in [self.tyrant, self.shock]:
                var.used = var.visible = False


class Covenant(OneOf):
    def __init__(self, parent, cost):
        super(Covenant, self).__init__(parent, 'Chaos Covenant')
        self.none = self.variant('No Chaos Covenant', gear=[])
        self.kh = self.variant('Covenant of Khorne', cost)
        self.ng = self.variant('Covenant of Nurgle', cost)
        self.sl = self.variant('Covenant of Slaanesh', cost)
        self.tz = self.variant('Covenant of Tzeentch', cost)

    def check_rules(self):
        if self.roster.is_vraks:
            self.sl.used = self.sl.visible = False
            self.tz.used = self.tz.visible = False

    def set_cost(self, newcost):
        for opt in [self.kh, self.ng, self.tz, self.sl]:
            opt.points = newcost

    def not_nurgle(self):
        return self.cur not in [
            self.none, self.ng
        ]


class RenegadeCommand(Unit):
    type_name = u'Renegade Command Squad'
    type_id = 'r_command_v1'

    class Demagogue(Unit):
        class Weapon(OneOf):
            def __init__(self, parent, ccw=False):
                super(RenegadeCommand.Demagogue.Weapon, self).__init__(parent, '' if ccw else 'Weapon')
                if ccw:
                    self.variant('Close combat weapon')
                else:
                    self.variant('Autopistol')
                    self.variant('Laspistol')
                self.hotshot = [
                    self.variant('Hot-shot laspistol', 5),
                    self.variant('Hot-shot lasgun', 5)
                ]
                self.variant('Autogun')
                self.variant('Lasgun')
                self.variant('Shotgun')
                self.variant('Bolt pistol', 2)
                self.variant('Plasma pistol', 15)
                self.variant('Power weapon', 10)
                self.variant('Power fist', 15)

        class Options(OptionsList):
            def __init__(self, parent):
                super(RenegadeCommand.Demagogue.Options, self).__init__(parent, 'Options')
                self.variant('Caparace armour', 5)
                self.variant('Melta bombs', 5)
                self.field = self.variant('Refractor field', 15)

        def __init__(self, parent):
            super(RenegadeCommand.Demagogue, self).__init__(parent, 'Arch-demagogue',
                                                            gear=[Gear('Frag grenades')])
            self.wep1 = self.Weapon(self)
            self.wep2 = self.Weapon(self, True)
            self.devotion = Devotions(self)
            self.covenant = Covenant(self, 10)
            self.opt = self.Options(self)

        def check_rules(self):
            super(RenegadeCommand.Demagogue, self).check_rules()
            self.covenant.set_cost(0 if self.devotion.che.value else 10)
            for wep in [self.wep1, self.wep2]:
                for shot in wep.hotshot:
                    shot.active = self.devotion.reaver.value
            self.opt.field.active = self.opt.field.used = not self.devotion.reaver.value
            self.covenant.kh.active = not self.devotion.witch.value
            self.devotion.used = self.devotion.visible = isinstance(self.roster.parent.parent, PrimaryDetachment)

        def build_description(self):
            res = super(RenegadeCommand.Demagogue, self).build_description()
            if self.devotion.reaver.value:
                res.add([Gear('Refractor field'), Gear('Krak grenades')])
            else:
                if self.parent.parent.opt.krak.value:
                    res.add(Gear('Krak grenades'))
            return res

    class DiscipleUpgrades(OptionsList):
        common_gear = []

        def __init__(self, parent):
            super(RenegadeCommand.DiscipleUpgrades, self).__init__(parent, 'Disciple wargear')
            self.banners = [
                self.variant('Banner of the Apostate', 10),
                self.variant('Banner of Hate', 25)
            ]
            self.vox = self.variant('Command net vox', 5)
            self.special = [
                self.variant('Flamer', 5),
                self.variant('Grenade launcher', 5),
                self.variant('Meltagun', 10),
                self.variant('Plasma gun', 15)
            ]
            self.banner_weapon = RenegadeCommand.Weapon(parent, text='Banner bearer weapon')
            self.vox_weapon = RenegadeCommand.Weapon(parent, text='Vox operator weapon')

        def check_rules(self):
            super(RenegadeCommand.DiscipleUpgrades, self).check_rules()
            OptionsList.process_limit(self.banners, 1)
            OptionsList.process_limit(self.special, 1)
            has_banner = any(opt.value for opt in self.banners)
            self.banner_weapon.visible = has_banner
            has_vox = self.vox.value
            self.vox_weapon.visible = has_vox

        def count_upgrades(self):
            return sum(opt.value for opt in self.options)

        @property
        def description(self):
            res = []
            model = UnitDescription('Disciple', options=self.common_gear)
            banner = [opt for opt in self.banners if opt.value]
            if len(banner):
                res.append(model.clone().add(self.banner_weapon.description)
                        .add(banner[0].gear))
            has_vox = self.vox.value
            if has_vox:
                res.append(model.clone().add(self.vox_weapon.description)
                        .add(self.vox.gear))
            special = [opt for opt in self.special if opt.value]
            if len(special):
                res.append(model.clone().add(special[0].gear))
            return res

    class HeavyWeapon(OptionsList):
        common_gear = []

        def __init__(self, parent):
            super(RenegadeCommand.HeavyWeapon, self).__init__(parent, 'Heavy weapon')
            self.weapons = [
                self.variant('Heavy stubber', 5),
                self.variant('Mortar', 5),
                self.variant('Heavy bolter', 10),
                self.variant('Autocannon', 10)
            ]
            self.ml = self.variant('Missile launcher', 15)
            self.flak = self.variant('Flak missiles', 10)
            self.weapons += [
                self.ml, self.variant('Lascannon', 20)
            ]

        def check_rules(self):
            super(RenegadeCommand.HeavyWeapon, self).check_rules()
            OptionsList.process_limit(self.weapons, 1)
            self.flak.active = self.flak.used = self.ml.value

        @property
        def description(self):
            if self.any:
                res = UnitDescription('Disciple Heavy Weapons team', options=self.common_gear)\
                      .add(super(RenegadeCommand.HeavyWeapon, self).description)
                return [res]
            else:
                return []

    class SquadOptions(OptionsList):
        def __init__(self, parent):
            super(RenegadeCommand.SquadOptions, self).__init__(parent, 'Squad upgrades')
            self.krak = self.variant('Krak grenades', 5)
            self.cap = self.variant('Caparace armour', 20)
            self.fnp = self.variant('Feel no Pain (6+)', 10)

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(RenegadeCommand.Transport, self).__init__(parent, 'Transport')
            SubUnit(self, Chimera(parent=None))

    class Weapon(OneOf):
        def __init__(self, parent, text='Weapon'):
            super(RenegadeCommand.Weapon, self).__init__(parent, name=text)
            self.variant('Autogun')
            self.variant('Lasgun')
            self.variant('Shotgun')

    def __init__(self, parent):
        super(RenegadeCommand, self).__init__(parent, points=45)
        self.boss = SubUnit(self, self.Demagogue(parent=None))
        self.extra = Count(self, 'Additional Disciples', 0, 10, 10)
        self.up = self.DiscipleUpgrades(self)
        self.heavy = self.HeavyWeapon(self)
        self.wep3 = Count(self, 'Lasguns', 0, 4)
        self.wep4 = Count(self, 'Shotguns', 0, 4)
        self.opt = self.SquadOptions(self)
        self.transport = self.Transport(self)

    def check_rules(self):
        super(RenegadeCommand, self).check_rules()
        self.opt.fnp.used = self.opt.fnp.visible = self.boss.unit.devotion.magus.value
        has_heavy = 2 * self.heavy.any
        min_free = has_heavy + self.up.count_upgrades()
        # in case there are all upgrades and HWT, at least 1 Disciple must be also taken
        self.extra.min = min_free > 4
        max_free = 4 + self.extra.cur - min_free
        Count.norm_counts(0, max_free, [self.wep3, self.wep4])
        self.transport.used = self.transport.visible = (has_heavy + self.get_count()) <= 12

    def get_count(self):
        return 5 + self.extra.cur - self.heavy.any

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count())
        if self.opt.fnp.value and self.opt.fnp.used:
            res.add(self.opt.fnp.gear)
        res.add(self.boss.description)
        common_gear = [Gear('Close combat weapon'), Gear('Frag grenades')]
        if self.opt.krak.value:
            common_gear.append(Gear('Krak grenades'))
        if self.opt.cap.value:
            common_gear.append(Gear('Caparace armour'))
        else:
            common_gear.append(Gear('Flak armour'))
        model = UnitDescription('Disciple', options=common_gear)
        self.up.common_gear = common_gear
        res.add(self.up.description)

        has_heavy = 2 * self.heavy.any
        min_free = has_heavy + self.up.count_upgrades()
        autoguns = 4 + self.extra.cur - min_free
        if self.wep3.cur:
            res.add(model.clone().add(Gear('Lasgun')).set_count(self.wep3.cur))
            autoguns -= self.wep3.cur
        if self.wep4.cur:
            res.add(model.clone().add(Gear('Shotgun')).set_count(self.wep4.cur))
            autoguns -= self.wep4.cur
        if autoguns:
            res.add(model.clone().add(Gear('Autogun')).set_count(autoguns))
        if has_heavy:
            self.heavy.common_gear = common_gear
            res.add(self.heavy.description)
        if self.transport.used:
            res.add(self.transport.description)
        return res

    def count_charges(self):
        return self.boss.unit.devotion.witch.value + self.boss.unit.devotion.ml2.value

    def build_statistics(self):
        return self.note_transport(
            self.note_charges(
                super(RenegadeCommand, self).build_statistics()))

    def is_heretek(self):
        return self.boss.unit.devotion.magus.value

    def is_witch(self):
        return self.boss.unit.devotion.witch.value

    def is_reaver(self):
        return self.boss.unit.devotion.reaver.value

    def is_mutant(self):
        return self.boss.unit.devotion.mutant.value

    def is_revolutionary(self):
        return self.boss.unit.devotion.che.value

    def is_horde(self):
        return self.boss.unit.devotion.horde.value

    def is_tyrant(self):
        return self.boss.unit.devotion.tyrant.value

    def is_shock(self):
        return self.boss.unit.devotion.shock.value

    def is_devoted(self):
        return self.boss.unit.devotion.any

    def has_covenant(self):
        return self.boss.unit.covenant.cur in [
            self.boss.unit.covenant.kh,
            self.boss.unit.covenant.ng,
            self.boss.unit.covenant.sl,
            self.boss.unit.covenant.tz,
        ]

    def khorne_covenant(self):
        return self.boss.unit.covenant.cur == self.boss.unit.covenant.kh

    def nurgle_covenant(self):
        return self.boss.unit.covenant.cur == self.boss.unit.covenant.ng

    def not_nurgle(self):
        return self.boss.unit.covenant.not_nurgle()

    def slaanesh_covenant(self):
        return self.boss.unit.covenant.cur == self.boss.unit.covenant.sl

    def tzeentch_covenant(self):
        return self.boss.unit.covenant.cur == self.boss.unit.covenant.tz


class PsykerCoven(Unit):
    type_name = u'Rogue Psykers Coven'
    type_id = 'r_coven_v1'

    def __init__(self, parent):
        super(PsykerCoven, self).__init__(parent)
        self.models = Count(self, 'Rogue Psykers', 1, 5, 35, True,
                            gear=UnitDescription('Rogue Psyker', 35,
                                                 options=[Gear('Close combat weapon')]))

    def get_count(self):
        return self.models.cur

    def build_statistics(self):
        return {'Models': self.get_count(),
                'Units': self.get_count(),
                'Warp charges': self.get_count()}


class EnforcerCadre(Unit):
    type_name = u'Renegade Enforcers Cadre'
    type_id = 'r_enforcers_v1'

    class Enforcer(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent, ccw=False):
                super(EnforcerCadre.Enforcer.Weapon, self).__init__(parent, '' if ccw else 'Weapon')
                if ccw:
                    self.variant('Close combat weapon')
                else:
                    self.variant('Autopistol')
                    self.variant('Laspistol')
                    self.variant('Stub gun')
                self.variant('Autogun')
                self.variant('Lasgun')
                self.variant('Shotgun')
                self.variant('Bolt pistol', 5)
                self.variant('Power weapon', 15)
                self.variant('Plasma pistol', 10)
                self.variant('Power fist', 25)

        class Options(OptionsList):
            def __init__(self, parent):
                super(EnforcerCadre.Enforcer.Options, self).__init__(parent, 'Options')
                self.cap = self.variant('Caparace armour', 5)
                self.variant('Combat drug injectors', 10)
                self.variant('Melta bombs', 5)

        def __init__(self, parent):
            super(EnforcerCadre.Enforcer, self).__init__(parent, 'Enforcer', 25)
            self.Weapon(self)
            self.Weapon(self, True)
            self.opt = self.Options(self)

        def buid_description(self):
            res = super(EnforcerCadre.Enforcer, self).buid_description()
            if not self.opt.cap.value:
                res.add(Gear('Flak armour'))
            return res

    def __init__(self, parent):
        super(EnforcerCadre, self).__init__(parent)
        self.models = UnitList(self, self.Enforcer, 1, 10)

    def check_rules(self):
        super(EnforcerCadre, self).check_rules()
        self.models.update_range(1, 10 if self.roster.demagogue.is_revolutionary() else 5)

    def get_count(self):
        return self.models.count

    def build_statistics(self):
        return {'Models': self.get_count()}
