__author__ = 'Ivan Truskov'

from builder.games.wh40k.section import UnitType, Formation
from builder.games.wh40k.roster import PrimaryDetachment
from hq import *
from elites import *
from troops import *
from fast import *
from heavy import *
from lords import Ghazghkull


class OrksWarband(Formation):
    army_id = 'orks_v3_warband'
    army_name = 'Ork Warband'

    def is_ghazkull(self):
        return False

    def __init__(self):
        super(OrksWarband, self).__init__()
        UnitType(self, Warboss, min_limit=1, max_limit=1)
        UnitType(self, Mek, min_limit=1, max_limit=1)
        nobz = [
            UnitType(self, Nobz),
            UnitType(self, Meganobz)
        ]
        self.add_type_restriction(nobz, 1, 1)
        UnitType(self, Boyz, min_limit=6, max_limit=6)
        UnitType(self, Gretchin, min_limit=1, max_limit=1)


class GhazkullFormation(Formation):
    def is_ghazkull(self):
        return True


class Council(GhazkullFormation):
    army_id = 'orks_v3_council'
    army_name = 'Council of Waaagh!'

    def __init__(self):
        super(Council, self).__init__()
        UnitType(self, Ghazghkull, min_limit=1, max_limit=1)
        UnitType(self, Grotsnik, min_limit=1, max_limit=1)
        UnitType(self, Warboss, min_limit=2, max_limit=2)
        UnitType(self, BigMek, min_limit=1, max_limit=1)
        self.nobz = UnitType(self, Nobz, min_limit=1, max_limit=1)

    def check_rules(self):
        super(Council, self).check_rules()
        for nob in self.nobz.units:
            if not nob.count_banners():
                self.error("Nobz must caarry a Waaagh! banner")


class Bullyboyz(GhazkullFormation):
    army_id = 'orks_v3_bully'
    army_name = 'Ghazghull\'s Bullyboyz'

    def __init__(self):
        super(Bullyboyz, self).__init__()
        self.nobz = UnitType(self, Meganobz, min_limit=3, max_limit=3)

    def check_rules(self):
        super(Bullyboyz, self).check_rules()
        for nob in self.nobz.units:
            if nob.count < 5:
                self.error("Meganobz unit must have at least 5 models")


class Vulcha(GhazkullFormation):
    army_id = 'orks_v3_vulcha'
    army_name = 'Da Vulcha Skwad'

    def __init__(self):
        super(Vulcha, self).__init__()
        UnitType(self, Zagstruk, min_limit=1, max_limit=1)
        UnitType(self, Stormboyz, min_limit=3, max_limit=3)


class Blitz(GhazkullFormation):
    army_id = 'orks_v3_blitz'
    army_name = 'Blitz Brigade'

    def __init__(self):
        super(Blitz, self).__init__()
        self.wagonz = UnitType(self, Battlewagon, min_limit=5, max_limit=5)

    def check_rules(self):
        super(Blitz, self).check_rules()
        for wagon in self.wagonz.units:
            if not(wagon.opt.has_deffrolla() or wagon.opt.ram.value):
                self.error("Battlewagon must have deffrolla or reinforced ram")


class Dreadmob(GhazkullFormation):
    army_id = 'orks_v3_dreadmob'
    army_name = 'Dread mob'

    def __init__(self):
        super(Dreadmob, self).__init__()
        UnitType(self, BigMek, min_limit=1, max_limit=1)
        UnitType(self, Painboy, min_limit=1, max_limit=1)
        gork = UnitType(self, Gorkanaut)
        mork = UnitType(self, Morkanaut)
        self.add_type_restriction([gork, mork], 2, 2)
        UnitType(self, DeffDread, min_limit=3, max_limit=3)
        self.kanz = UnitType(self, KillaKans, min_limit=3, max_limit=3)

    def check_rules(self):
        super(Dreadmob, self).check_rules()
        for kanun in self.kanz.units:
            if kanun.count < 3:
                self.error('Killa Kans unit must include at least 3 models')


class RedSkull(GhazkullFormation):
    army_id = 'orks_v3_redskull'
    army_name = 'Boss Snikrot\'s Red Skull Kommandos'

    def __init__(self):
        super(RedSkull, self).__init__()
        UnitType(self, Snikrot, min_limit=1, max_limit=1)
        UnitType(self, Kommandos, min_limit=4, max_limit=4)


class BadrukkGitz(GhazkullFormation):
    army_id = 'orks_v3_badrukk'
    army_name = "Kaptin Badrukk's Flash Gitz"

    def __init__(self):
        super(BadrukkGitz, self).__init__()
        UnitType(self, Badrukk, min_limit=1, max_limit=1)
        self.gitz = UnitType(self, Flashgitz, min_limit=2, max_limit=2)

    def check_rules(self):
        super(BadrukkGitz, self).check_rules()
        for gunit in self.gitz.units:
            if gunit.get_count() < 10:
                self.error("All units of Flash Gitz must have 10 models")
                break


class GreenTide(GhazkullFormation):
    army_id = 'orks_v3_greentide'
    army_name = 'Green Tide'

    class WalkingBoyz(Boyz):
        def __init__(self, parent):
            super(GreenTide.WalkingBoyz, self).__init__(parent)
            self.transport.used = self.transport.visible = False

    def __init__(self):
        super(GreenTide, self).__init__()
        self.boss = UnitType(self, Warboss, min_limit=1, max_limit=1)
        UnitType(self, self.WalkingBoyz, min_limit=10, max_limit=10)

    def check_rules(self):
        super(GreenTide, self).check_rules()
        for b in self.boss.units:
            if b.wots.bike.value:
                self.error('Warboss must be on foot')


class WaaghBand(OrksWarband):
    army_id = 'orks_v3_waaaghband'
    army_name = 'Waaagh!-Band'

    def is_ghazkull(self):
        return True


class GoffKillmob(GhazkullFormation):
    army_id = 'orks_v3_goffmob'
    army_name = 'Goff killmob'

    def __init__(self):
        super(GoffKillmob, self).__init__()
        bosses = [
            UnitType(self, Warboss),
            UnitType(self, Grukk)
        ]
        self.add_type_restriction(bosses, 1, 1)
        UnitType(self, Nobz, min_limit=1, max_limit=1)
        self.boyz = UnitType(self, Boyz, min_limit=3, max_limit=3)
        UnitType(self, Gorkanaut, min_limit=1, max_limit=1)
        UnitType(self, DeffDread, min_limit=2, max_limit=2)
        self.kanz = UnitType(self, KillaKans, min_limit=1, max_limit=1)

    def check_rules(self):
        super(GoffKillmob, self).check_rules()
        if not all(u.get_count() >= 20 for u in self.boyz.units):
            self.error("All units of Boyz must have at least 20 models")
        if not all(u.get_count() >= 3 for u in self.kanz.units):
            self.error("Unit of Killa Kanz must have at least 3 models")


class BlitzaSkwadron(GhazkullFormation):
    army_id = 'orks_v3_blitzas'
    army_name = 'Blitza-bommer Skwadron'

    class Blitzas(BlitzaBommers):
        unit_min = 3
        unit_max = 3

    def __init__(self):
        super(BlitzaSkwadron, self).__init__()
        UnitType(self, self.Blitzas, min_limit=1, max_limit=1)


class BurnaSkwadron(GhazkullFormation):
    army_id = 'orks_v3_burnas'
    army_name = 'Burna-bommer Skwadron'

    class Burnas(BurnaBommers):
        unit_min = 3
        unit_max = 3

    def __init__(self):
        super(BurnaSkwadron, self).__init__()
        UnitType(self, self.Burnas, min_limit=1, max_limit=1)


class JetSkwadron(GhazkullFormation):
    army_id = 'orks_v3_jets'
    army_name = 'Dakkajet Skwadron'

    class Jets(Dakkajets):
        unit_min = 3
        unit_max = 3

    def __init__(self):
        super(JetSkwadron, self).__init__()
        UnitType(self, self.Jets, min_limit=1, max_limit=1)


class AirArmada(GhazkullFormation):
    army_id = 'orks_v3_armada'
    army_name = 'Air Armada'

    def __init__(self):
        super(AirArmada, self).__init__()
        self.jets = UnitType(self, JetSkwadron.Jets, min_limit=1, max_limit=1)
        UnitType(self, BurnaBommer, min_limit=1, max_limit=1)
        UnitType(self, BlitzaBommer, min_limit=1, max_limit=1)

    def check_rules(self):
        super(AirArmada, self).check_rules()
        for junit in self.jets.units:
            if junit.boss_count < 1:
                self.error("One Dakkajet must take a flyboss")
                break


class KrushingKrew(Formation):

    army_id = 'orks_v3_krushing'
    army_name = 'Gorkanaut Krushin\' Krew'

    def __init__(self):
        super(KrushingKrew, self).__init__()
        UnitType(self, Gorkanaut, min_limit=3, max_limit=3)


class Bossboyz(Formation):

    def is_ghazkull(self):
        return False

    army_id = 'orks_v3_bossboyz'
    army_name = 'Mogrok\'s Bossboyz'

    def __init__(self):
        super(Bossboyz, self).__init__()
        UnitType(self, BigMek, min_limit=3, max_limit=3)
        UnitType(self, Warboss, min_limit=1, max_limit=1)
        self.warp = UnitType(self, Weirdboy, min_limit=1, max_limit=1)

    def check_rules(self):
        super(Bossboyz, self).check_rules()
        for unit in self.warp.units:
            if unit.mastery.cur != unit.mastery.lvl2:
                self.error("Warphead must have mastery level 2")
        if not (type(self.parent.parent) is PrimaryDetachment):
            self.error('Big Mek Mogrok must be warlord')


class RippingKrew(Formation):
    army_name = "Grukk's Rippin' krew"
    army_id = 'orks_v3_rippinkrew'

    def is_ghazkull(self):
        return False

    def __init__(self):
        super(RippingKrew, self).__init__()
        UnitType(self, Grukk, min_limit=1, max_limit=1)
        UnitType(self, SkrakNobz, min_limit=1, max_limit=1)
        UnitType(self, RustgobRunts, min_limit=1, max_limit=1)
        UnitType(self, KrumpaKanz, min_limit=1, max_limit=1)


class Skwadron(Formation):
    army_name = 'Ork Skwadron'
    army_id = 'orks_v3_skwadron'

    def __init__(self):
        super(Skwadron, self).__init__()

        class Blitzas(BlitzaBommers):
            unit_min = 2

        class Burnas(BurnaBommers):
            unit_min = 2

        class Dakkas(Dakkajets):
            unit_min = 2

        wings = [
            UnitType(self, Blitzas),
            UnitType(self, Burnas),
            UnitType(self, Dakkas)
        ]
        self.add_type_restriction(wings, 3, 3)


class KustomWazmob(Formation):
    army_name = 'Kustom Wazmob'
    army_id = 'orks_v3_kwazmob'

    def __init__(self):
        super(KustomWazmob, self).__init__()
        self.wazbom = UnitType(self, WazbomBlastajets.SingleWazbom, min_limit=1, max_limit=4)
        planes = [
            self.wazbom,
            UnitType(self, Dakkajet),
            UnitType(self, BlitzaBommer),
            UnitType(self, BurnaBommer),
            # UnitType(self, FightaBommer),
            # UnitType(self, AttakFightas.AttakFighta)
        ]
        self.add_type_restriction(planes, 4, 4)

    def check_rules(self):
        super(KustomWazmob, self).check_rules()
        if not any(u.eq.cur == u.eq.field for u in self.wazbom.units):
            self.error('The Wing Leader must be a pilot of a Wazbom Blastajet and must take a kustom force field')
