__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear


class Grenades(OptionsList):
    def __init__(self, parent):
        super(Grenades, self).__init__(parent, 'Grenades')
        self.variants('Stikkbombs', 25, var_dicts=[
            {'name':'Weapon reload', 'points':13}
        ])


class H2H(OptionsList):
    def __init__(self, parent, nob=False):
        super(H2H, self).__init__(parent, "Hand-to-Hand Weapons")
        self.variant("Shank", 5)
        self.variant("Choppa", 10)
        self.variant("Buzz-choppa", 15)
        if nob:
            self.variant("Big choppa", 15)
            self.variant("Power klaw", 85)


class Pistols(OptionsList):
    def __init__(self, parent):
        super(Pistols, self).__init__(parent, "Pistols")
        self.variants("Slugga", 10, var_dicts=[
            {'name':'Red-dot laser sight', 'points':15},
            {'name':'Weapon reload', 'points':5}
        ])


class Basic(OptionsList):
    def __init__(self, parent, nob=False):
        super(Basic, self).__init__(parent, 'Basic weapons')
        self.variants("Shoota", 25, var_dicts=[
            {'name':'Red-dot laser sight', 'points':15},
            {'name':'Weapon reload', 'points':13}
        ])
        if nob:
            self.variants("Kombi-shoota", 40, var_dicts=[
                {'name':'Red-dot laser sight', 'points':15},
                {'name':'Weapon reload', 'points':20}
            ])
            self.variants("Kombi-weapon", 50, var_dicts=[
                {'name':'Red-dot laser sight', 'points':15},
                {'name':'Weapon reload', 'points':25}
            ])


class Misc(OptionsList):
    def __init__(self, parent):
        super(Misc, self).__init__(parent, 'Miscellaneous')
        self.variant('Clip harness', 10)
        self.variant('\'Eavy armour', 25)


class Special(OptionsList):
    def __init__(self, parent):
        super(Special, self).__init__(parent, 'Special Weapons')
        self.variants("Rokkit launcha", 130, var_dicts=[
            {'name':'Red-dot laser sight', 'points':15},
            {'name':'Weapon reload', 'points':65}
        ])
        self.variants("Big shoota", 150, var_dicts=[
            {'name':'Red-dot laser sight', 'points':15},
            {'name':'Weapon reload', 'points':75}
        ])
