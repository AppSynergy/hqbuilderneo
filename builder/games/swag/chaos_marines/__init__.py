__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.swag.roster import Leader, Troopers, Specialists,\
    NewRecruits, SWAGRoster
from units import Champion, Marine, Cultist, Gunner


class CSMLeader(Leader):
    def __init__(self, *args, **kwargs):
        super(CSMLeader, self).__init__(*args, **kwargs)
        UnitType(self, Champion)


class CSMTroopers(Troopers):
    def __init__(self, *args, **kwargs):
        super(CSMTroopers, self).__init__(*args, **kwargs)
        UnitType(self, Marine)


class CSMNewRecruits(NewRecruits):
    def __init__(self, *args, **kwargs):
        super(CSMNewRecruits, self).__init__(*args, **kwargs)
        UnitType(self, Cultist)


class CSMSpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(CSMSpecialists, self).__init__(*args, **kwargs)
        UnitType(self, Gunner)


class CSMKillTeam(SWAGRoster):
    army_name = 'Chaos Space Marine Kill Team'
    army_id = 'csm_swag_v1'

    def __init__(self):
        super(CSMKillTeam, self).__init__(
            CSMLeader, CSMTroopers, CSMSpecialists,
            CSMNewRecruits)
