__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.swag.roster import Leader, Troopers, Specialists,\
    NewRecruits, SWAGRoster
from units import Sergeant, Scout, Novitiate, Gunner, SWGunner


class SMLeader(Leader):
    def __init__(self, *args, **kwargs):
        super(SMLeader, self).__init__(*args, **kwargs)
        UnitType(self, Sergeant)


class SMTroopers(Troopers):
    def __init__(self, *args, **kwargs):
        super(SMTroopers, self).__init__(*args, **kwargs)
        UnitType(self, Scout)


class SMNewRecruits(NewRecruits):
    def __init__(self, *args, **kwargs):
        super(SMNewRecruits, self).__init__(*args, **kwargs)
        UnitType(self, Novitiate)


class SMSpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(SMSpecialists, self).__init__(*args, **kwargs)
        UnitType(self, Gunner)


class SWSpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(SWSpecialists, self).__init__(*args, **kwargs)
        UnitType(self, SWGunner)


class SpaceMarinesKillTeam(SWAGRoster):
    army_name = 'Space Marines Kill Team'
    army_id = 'space_marines_swag_v1'

    def __init__(self):
        super(SpaceMarinesKillTeam, self).__init__(
            SMLeader, SMTroopers, SMSpecialists, SMNewRecruits)


class SpaceWolvesKillTeam(SWAGRoster):
    army_name = 'Space Wolves Kill Team'
    army_id = 'space_wolves_swag_v1'

    def __init__(self):
        super(SpaceWolvesKillTeam, self).__init__(
            SMLeader, SMTroopers, SWSpecialists, None)
