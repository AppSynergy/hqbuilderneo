__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear


class Grenades(OptionsList):
    def __init__(self, parent, sergeant=False):
        super(Grenades, self).__init__(parent, 'Grenades')
        self.variants('Frag grenades', 25, var_dicts=[
            {'name':'Weapon reload', 'points':13}
        ])
        if sergeant:
            self.variants('Melta bombs', 30, var_dicts=[
                {'name':'Weapon reload', 'points':15}
            ])
        self.variants('Krak grenades', 40, var_dicts=[
            {'name':'Weapon reload', 'points':20}
        ])


class H2H(OptionsList):
    def __init__(self, parent, sergeant=False):
        super(H2H, self).__init__(parent, "Hand-to-Hand Weapons")
        self.variant("Combat blade", 5)
        self.variant("Assault blade", 15)
        self.variant("Chainsword", 25)
        if sergeant:
            self.variant("Power sword", 50)


class Pistols(OptionsList):
    def __init__(self, parent, sergeant=False):
        super(Pistols, self).__init__(parent, "Pistols")
        self.bp = self.variants("Bolt Pistol", 25, var_dicts=[
            {'name':'Hellfire bolts', 'points':20},
            {'name':'Red-dot laser sight', 'points':20},
            {'name':'Weapon reload', 'points':13}
        ])
        if sergeant:
            self.variants("Plasma pistol", 50, var_dicts=[
                {'name':'Red-dot laser sight', 'points':20},
                {'name':'Weapon reload', 'points':25}
            ])


class Basic(OptionsList):
    def __init__(self, parent):
        super(Basic, self).__init__(parent, 'Basic weapons')
        self.variants("Shotgun", 20, var_dicts=[
            {'name':'Red-dot laser sight', 'points':20},
            {'name':'Weapon reload', 'points':10}
        ])
        self.variants("Boltgun", 35, var_dicts=[
            {'name':'Hellfire bolts', 'points':20},
            {'name':'Red-dot laser sight', 'points':20},
            {'name':'Telescopic sight', 'points':20},
            {'name':'Weapon reload', 'points':18}
        ])
        self.variants("Sniper rifle", 40, var_dicts=[
            {'name':'Toxic rounds', 'points':20},
            {'name':'Red-dot laser sight', 'points':20},
            {'name':'Telescopic sight', 'points':20},
            {'name':'Weapon reload', 'points':20}
        ])


class Heavy(OptionsList):
    def __init__(self, parent):
        super(Heavy, self).__init__(parent, 'Heavy weapons')
        hb = self.variants('Heavy bolter', 180, var_dicts=[{'name':'Weapon reload', 'points':90}])
        ml1 = self.variants('Missile launcher (frag missiles)', 175, var_dicts=[{'name':'Weapon reload', 'points':88}])
        ml2 = self.variants('Missile launcher (super krak missiles)', 190, var_dicts=[{'name':'Weapon reload', 'points':95}])
        ml3 = self.variants('Missile launcher (frag and super krak missiles)', 225, var_dicts=[{'name':'Weapon reload', 'points':113}])
        self.weapons = [hb, ml1, ml2, ml3]

    def check_rules(self):
        super(Heavy, self).check_rules()
        OptionsList.process_limit(self.weapons, 1)


class Misc(OptionsList):
    def __init__(self, parent):
        super(Misc, self).__init__(parent, 'Miscellaneous')
        self.variant('Camo gear', 5)
        self.variant('Clip harness', 10)
        self.variant('Photo-visor', 15)


class Special(OptionsList):
    def __init__(self, parent):
        super(Special, self).__init__(parent, 'Space Wolves Special Weapons')
        opts = [{'name':'Red-dot laser sight', 'points':20},
                {'name':'Telescopic sight', 'points':20}]
        self.variants('Flamer', 40, var_dicts=[{'name':'Weapon reload', 'points':20}])
        self.variants('Plasma gun', 80, var_dicts=opts + [{'name':'Weapon reload', 'points':40}])
        self.variants('Meltagun', 95, var_dicts=opts + [{'name':'Weapon reload', 'points':48}])
