__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.swag.unit import Unit
from builder.games.swag.options import Characteristics, Skills
from armory import *


class Sergeant(Unit):
    type_name = 'Scout Sergeant'
    type_id = 'sm_sergeant_v1'

    def __init__(self, parent):
        super(Sergeant, self).__init__(parent, points=200, gear=[
            Gear('Combat blade'), Gear('Scout armour')
        ])
        H2H(self, sergeant=True)
        Pistols(self, sergeant=True)
        Basic(self)
        Grenades(self, sergeant=True)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Scout(Unit):
    type_name = 'Scout'
    type_id = 'sm_scout_v1'

    def __init__(self, parent):
        super(Scout, self).__init__(parent, points=100, gear=[
            Gear('Combat blade'), Gear('Scout armour')])
        H2H(self)
        Pistols(self)
        Basic(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Novitiate(Unit):
    type_name = 'Novitiate Scout'
    type_id = 'sm_novitiate_v1'

    def __init__(self, parent):
        super(Novitiate, self).__init__(parent, points=75, gear=[
            Gear('Combat blade'), Gear('Scout armour')])
        H2H(self)
        Pistols(self)
        Basic(self)
        Grenades(self)
        Misc(self)


class Gunner(Unit):
    type_name = 'Scout Gunner'
    type_id = 'sm_gunner_v1'

    def __init__(self, parent):
        super(Gunner, self).__init__(parent, points=110, gear=[
            Gear('Combat blade'), Gear('Scout armour')])
        H2H(self)
        Pistols(self)
        Heavy(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class SWGunner(Unit):
    type_name = 'Scout Gunner'
    type_id = 'sm_swgunner_v1'

    def __init__(self, parent):
        super(SWGunner, self).__init__(parent, points=110, gear=[
            Gear('Combat blade'), Gear('Scout armour')])
        H2H(self)
        Pistols(self)
        Heavy(self)
        Special(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)
