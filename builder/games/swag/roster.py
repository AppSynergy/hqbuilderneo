__author__ = 'Ivan Truskov'

from builder.core2 import Roster
from sections import Leader, Troopers, Specialists, NewRecruits


class SWAGRoster(Roster):
    game_name = 'Shadow War: Armmageddon'
    base_tags = ['Shadow War: Armageddon']

    def __init__(self, leader_sectype, trooper_sectype,
                 spec_sectype, recruit_sectype, max_limit=10):
        self.max_limit = max_limit
        self.min_limit = 3
        leader = leader_sectype(self)
        specs = spec_sectype and spec_sectype(self)
        new_recruits = recruit_sectype and recruit_sectype(self)
        troopers = trooper_sectype and trooper_sectype(self, max_limit=self.max_limit,
                                                       other_sections=[s
                                                                       for s in [leader, specs, new_recruits]
                                                                       if s is not None])
        if new_recruits:
            new_recruits.peer_sections += [s for s in [leader, specs, troopers] if s is not None]
        super(SWAGRoster, self).__init__([s for s in [leader, troopers, specs, new_recruits] if s is not None],
                                         parent=None, limit=1000)

    def check_rules(self):
        super(SWAGRoster, self).check_rules()
        models = sum(len(s.units) for s in self.sections)
        if models < self.min_limit:
            self.error("Kill team must include no less then {} models; taken: {}".format(self.min_limit, models))
        if models > self.max_limit:
            self.error("Kill team must include no more then {} models; taken: {}".format(self.max_limit, models))

    def dump(self):
        res = super(SWAGRoster, self).dump()
        res.update({
            'statistics': self.build_statistics()
        })
        return res

    @property
    def delta(self):
        res = super(SWAGRoster, self).delta
        res.update({
            'statistics': self.build_statistics()
        })
        return res
