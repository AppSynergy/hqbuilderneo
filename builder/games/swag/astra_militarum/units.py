__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.swag.unit import Unit
from builder.games.swag.options import Characteristics, Skills
from armory import *


class Sergeant(Unit):
    type_name = 'Veteran Sergeant'
    type_id = 'am_sergeant_v1'

    def __init__(self, parent):
        super(Sergeant, self).__init__(parent, points=120, gear=[
            Gear('Combat blade'), Gear('Flak armour')
        ])
        H2H(self, sergeant=True)
        Pistols(self, sergeant=True)
        Basic(self, sergeant=True)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Veteran(Unit):
    type_name = 'Veteran Guardsman'
    type_id = 'am_veteran_v1'

    def __init__(self, parent):
        super(Veteran, self).__init__(parent, points=60, gear=[
            Gear('Combat blade'), Gear('Flak armour')])
        H2H(self)
        Pistols(self)
        Basic(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Guardsman(Unit):
    type_name = 'Guardsman'
    type_id = 'am_guardsman_v1'

    def __init__(self, parent):
        super(Guardsman, self).__init__(parent, points=50, gear=[
            Gear('Combat blade'), Gear('Flak armour')])
        H2H(self)
        Pistols(self)
        Basic(self)
        Grenades(self)
        Misc(self)


class Operative(Unit):
    type_name = 'Special Weapons Operative'
    type_id = 'am_operative_v1'

    def __init__(self, parent):
        super(Operative, self).__init__(parent, points=70, gear=[
            Gear('Combat blade'), Gear('Flak armour')])
        H2H(self)
        Pistols(self)
        Special(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)
