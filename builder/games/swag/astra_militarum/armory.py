__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear


class Grenades(OptionsList):
    def __init__(self, parent):
        super(Grenades, self).__init__(parent, 'Grenades')
        self.variants('Frag grenades', 25, var_dicts=[
            {'name':'Weapon reload', 'points':13}
        ])
        self.variants('Melta bombs', 30, var_dicts=[
            {'name':'Weapon reload', 'points':15}
        ])
        self.variants('Krak grenades', 40, var_dicts=[
            {'name':'Weapon reload', 'points':20}
        ])


class H2H(OptionsList):
    def __init__(self, parent, sergeant=False):
        super(H2H, self).__init__(parent, "Hand-to-Hand Weapons")
        self.variant("Combat blade", 5)
        self.variant("Assault blade", 15)
        self.variant("Chainsword", 25)
        if sergeant:
            self.variant("Power sword", 50)


class Pistols(OptionsList):
    def __init__(self, parent, sergeant=False):
        super(Pistols, self).__init__(parent, "Pistols")
        self.lp = self.variants("Laspistol", 15, var_dicts=[
            {'name':'Hotshot laser power pack', 'points':15},
            {'name':'Red-dot laser sight', 'points':20},
            {'name':'Weapon reload', 'points':8}
        ])
        if sergeant:
            self.bp = self.variants("Bolt Pistol", 25, var_dicts=[
                {'name':'Red-dot laser sight', 'points':20},
                {'name':'Weapon reload', 'points':13}
            ])

            self.variants("Plasma pistol", 50, var_dicts=[
                {'name':'Red-dot laser sight', 'points':20},
                {'name':'Weapon reload', 'points':25}
            ])


class Basic(OptionsList):
    def __init__(self, parent, sergeant=False):
        super(Basic, self).__init__(parent, 'Basic weapons')
        self.variants("Shotgun", 20, var_dicts=[
            {'name':'Red-dot laser sight', 'points':20},
            {'name':'Weapon reload', 'points':10}
        ])
        self.variants("Lasgun", 25, var_dicts=[
            {'name':'Hotshot laser power pack', 'points':15},
            {'name':'Red-dot laser sight', 'points':20},
            {'name':'Telescopic sight', 'points':20},
            {'name':'Weapon reload', 'points':13}
        ])
        if sergeant:
            self.variants("Boltgun", 35, var_dicts=[
                {'name':'Red-dot laser sight', 'points':20},
                {'name':'Telescopic sight', 'points':20},
                {'name':'Weapon reload', 'points':18}
            ])


class Misc(OptionsList):
    def __init__(self, parent):
        super(Misc, self).__init__(parent, 'Miscellaneous')
        self.variant('Camo gear', 5)
        self.variant('Clip harness', 10)
        self.variant('Caparace armour', 20)


class Special(OptionsList):
    def __init__(self, parent):
        super(Special, self).__init__(parent, 'Special Weapons')
        opts = [{'name':'Red-dot laser sight', 'points':20},
                {'name':'Telescopic sight', 'points':20}]
        self.variants("Sniper rifle", 40, var_dicts=[
            {'name':'Toxic rounds', 'points':20},
            {'name':'Red-dot laser sight', 'points':20},
            {'name':'Telescopic sight', 'points':20},
            {'name':'Weapon reload', 'points':20}
        ])
        self.variants('Flamer', 40, var_dicts=[{'name':'Weapon reload', 'points':20}])
        self.variants('Demolition charge', 50, var_dicts=[{'name':'Weapon reload', 'points':25}])
        self.variants('Plasma gun', 80, var_dicts=opts + [{'name':'Weapon reload', 'points':40}])
        self.variants('Meltagun', 95, var_dicts=[{'name':'Red-dot laser sight', 'points':20},
                                                 {'name':'Weapon reload', 'points':48}])
        self.variants('Heavy flamer', 100, var_dicts=[{'name':'Weapon reload', 'points':50}])
        ml1 = self.variants('Grenade launcher (frag grenades)', 85, var_dicts=[{'name':'Weapon reload', 'points':43}])
        ml2 = self.variants('Grenade launcher (krak grenades)', 100, var_dicts=[{'name':'Weapon reload', 'points':50}])
        ml3 = self.variants('Grenade launcher (frag and krak grenades)', 125, var_dicts=[{'name':'Weapon reload', 'points':63}])
