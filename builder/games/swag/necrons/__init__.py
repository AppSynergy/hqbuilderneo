__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.swag.roster import Leader, Troopers, Specialists,\
    NewRecruits, SWAGRoster
from units import Appointed, Immortal, Warrior, Deathmark


class NecronLeader(Leader):
    def __init__(self, *args, **kwargs):
        super(NecronLeader, self).__init__(*args, **kwargs)
        UnitType(self, Appointed)


class NecronTroopers(Troopers):
    def __init__(self, *args, **kwargs):
        super(NecronTroopers, self).__init__(*args, **kwargs)
        UnitType(self, Immortal)


class NecronNewRecruits(NewRecruits):
    def __init__(self, *args, **kwargs):
        super(NecronNewRecruits, self).__init__(*args, **kwargs)
        UnitType(self, Warrior)


class NecronSpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(NecronSpecialists, self).__init__(*args, **kwargs)
        UnitType(self, Deathmark)


class NecronKillTeam(SWAGRoster):
    army_name = 'Necron Kill Team'
    army_id = 'necrons_swag_v1'

    def __init__(self):
        super(NecronKillTeam, self).__init__(
            NecronLeader, NecronTroopers, NecronSpecialists,
            NecronNewRecruits)
