__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.swag.unit import Unit
from builder.games.swag.options import Characteristics, Skills
from armory import *


class Alpha(Unit):
    type_name = 'Tyranid Alpha'
    type_id = 'tyr_alpha_v1'

    def __init__(self, parent):
        super(Alpha, self).__init__(parent, points=250, gear=[
            Gear('Scything talon', count=2), Gear('Chitin caparace')])
        self.hth = H2H(self)
        self.basic = Basic(self)
        Biomorphs(self)
        Characteristics(self)
        Skills(self)

    def check_rules(self):
        super(Alpha, self).check_rules()
        if not (self.hth.any or self.basic.any):
            self.error('Tyranid Alpha must be armed with one item chosen from Hand-to-Hand Bio-weapons list or Basic Bio-weapons list')
        self.hth.used = self.hth.visible = not self.basic.any
        self.basic.used = self.basic.visible = not self.hth.any

class Warrior(Unit):
    type_name = 'Tyranid Warrior'
    type_id = 'tyr_warrior_v1'

    def __init__(self, parent):
        super(Warrior, self).__init__(parent, points=200, gear=[
            Gear('Scything talon', count=2), Gear('Chitin caparace')])
        self.hth = H2H(self)
        self.basic = Basic(self)
        Biomorphs(self)
        Characteristics(self)
        Skills(self)

    def check_rules(self):
        super(Warrior, self).check_rules()
        if not (self.hth.any or self.basic.any):
            self.error('Tyranid Warrior must be armed with one item chosen from Hand-to-Hand Bio-weapons list or Basic Bio-weapons list')
        self.hth.used = self.hth.visible = not self.basic.any
        self.basic.used = self.basic.visible = not self.hth.any


class Spawn(Unit):
    type_name = 'Tyranid New-spawn'
    type_id = 'tyr_spawn_v1'

    def __init__(self, parent):
        super(Spawn, self).__init__(parent, points=175, gear=[
            Gear('Scything talon', count=2), Gear('Chitin caparace')])
        self.hth = H2H(self)
        self.basic = Basic(self)
        Biomorphs(self)

    def check_rules(self):
        super(Spawn, self).check_rules()
        if not (self.hth.any or self.basic.any):
            self.error('Tyranid New-spawn must be armed with one item chosen from Hand-to-Hand Bio-weapons list or Basic Bio-weapons list')
        self.hth.used = self.hth.visible = not self.basic.any
        self.basic.used = self.basic.visible = not self.hth.any


class Gunbeast(Unit):
    type_name = 'Tyranid Gun-beast'
    type_id = 'tyr_gunbeast_v1'

    def __init__(self, parent):
        super(Gunbeast, self).__init__(parent, points=225, gear=[
            Gear('Scything talon', count=2), Gear('Chitin caparace')])
        self.hth = H2H(self)
        self.basic = Basic(self)
        self.can = Cannons(self)
        Biomorphs(self)
        Characteristics(self)
        Skills(self)

    def check_rules(self):
        super(Gunbeast, self).check_rules()
        if not (self.hth.any or self.basic.any or self.can.any):
            self.error('Tyranid Gun-beast must be armed with one item chosen from Hand-to-Hand Bio-weapons list, Basic Bio-weapons list or Bio-cannons list')
        self.hth.used = self.hth.visible = not (self.basic.any or self.can.any)
        self.basic.used = self.basic.visible = not (self.hth.any or self.can.any)
        self.can.used = self.can.visible = not(self.hth.any or self.basic.any)
