__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.swag.unit import Unit
from builder.games.swag.options import Characteristics, Skills
from armory import *


class Inquisitor(Unit):
    type_name = 'Ordo Xenos Inquisitor'
    type_id = 'inq_xenos_v1'

    def __init__(self, parent):
        super(Inquisitor, self).__init__(parent, points=225, gear=[
            Gear('Combat blade'), Gear('Caparace armour')
        ])
        H2H(self, inquisitor=True)
        Pistols(self, inquisitor=True)
        Basic(self)
        Grenades(self)
        Misc(self, inquisitor=True)
        Special(self, inquisitor=True)
        Characteristics(self)
        Skills(self)


class Acolyte(Unit):
    type_name = 'Inquisitorial Acolyte'
    type_id = 'inq_acolyte_v1'

    def __init__(self, parent):
        super(Acolyte, self).__init__(parent, points=65, gear=[
            Gear('Combat blade'), Gear('Flak armour')])
        H2H(self)
        Pistols(self)
        Basic(self)
        Grenades(self)
        Misc(self)
        Special(self)
        Characteristics(self)
        Skills(self)


class Initiate(Unit):
    type_name = 'Inquisitorial Initiate'
    type_id = 'inq_initiate_v1'

    def __init__(self, parent):
        super(Initiate, self).__init__(parent, points=50, gear=[
            Gear('Combat blade'), Gear('Flak armour')])
        H2H(self)
        Pistols(self)
        Basic(self)
        Grenades(self)
        Misc(self)


class Crusader(Unit):
    type_name = 'Crusader'
    type_id = 'inq_crusader_v1'

    def __init__(self, parent):
        super(Crusader, self).__init__(parent, points=85, gear=[
            Gear('Combat blade'), Gear('Caparace armour')])
        H2H(self)
        Pistols(self)
        Grenades(self)
        Misc(self, crusader=True)
        Characteristics(self)
        Skills(self)
