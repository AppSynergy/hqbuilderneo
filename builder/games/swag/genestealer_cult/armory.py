__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear


class Grenades(OptionsList):
    def __init__(self, parent):
        super(Grenades, self).__init__(parent, 'Grenades')
        self.variants('Blasting charges', 40, var_dicts=[
            {'name': 'Weapon reload', 'points': 20}
        ])


class H2H(OptionsList):
    def __init__(self, parent, leader=False):
        super(H2H, self).__init__(parent, "Hand-to-Hand Weapons")
        self.variant("Combat blade", 5)
        self.variant("Chainsword", 25)
        if leader:
            self.variant("Power maul", 50)
            self.variant('Power pick', 50)


class Pistols(OptionsList):
    def __init__(self, parent, leader=False):
        super(Pistols, self).__init__(parent, "Pistols")
        self.variants("Autopistol", 15, var_dicts=[
            {'name': 'Weapon reload', 'points': 8}
        ])
        self.variants("Laspistol", 15, var_dicts=[
            {'name': 'Hotshot laser power pack', 'points': 15},
            {'name': 'Weapon reload', 'points': 8}
        ])
        self.variants("Bolt Pistol", 25, var_dicts=[
            {'name': 'Weapon reload', 'points': 13}
        ])
        if leader:
            self.variants("Web pistol", 75, var_dicts=[
                {'name': 'Weapon reload', 'points': 38}
            ])


class Basic(OptionsList):
    def __init__(self, parent):
        super(Basic, self).__init__(parent, 'Basic weapons')
        self.variants("Autogun", 20, var_dicts=[
            {'name': 'Weapon reload', 'points': 10}
        ])
        self.variants("Shotgun", 20, var_dicts=[
            {'name': 'Weapon reload', 'points': 10}
        ])
        self.variants("Lasgun", 35, var_dicts=[
            {'name': 'Hotshot laser power pack', 'points': 15},
            {'name': 'Weapon reload', 'points': 18}
        ])


class Misc(OptionsList):
    def __init__(self, parent):
        super(Misc, self).__init__(parent, 'Miscellaneous')
        self.variant('Clip harness', 10)
        self.variant('Photo-visor', 15)


class Special(OptionsList):
    def __init__(self, parent):
        super(Special, self).__init__(parent, 'Special Weapons')
        self.variants('Flamer', 40, var_dicts=[{'name': 'Weapon reload', 'points': 20}])
        self.variants('Webber', 100, var_dicts=[{'name': 'Weapon reload', 'points': 50}])
        ml1 = self.variants('Grenade launcher (frag grenades)', 85,
                            var_dicts=[{'name': 'Weapon reload', 'points': 43}])
        ml2 = self.variants('Grenade launcher (krak grenades)', 100,
                            var_dicts=[{'name': 'Weapon reload', 'points': 50}])
        ml3 = self.variants('Grenade launcher (frag and krak grenades)', 125,
                            var_dicts=[{'name': 'Weapon reload', 'points': 63}])


class Heavy(OptionsList):
    def __init__(self, parent):
        super(Heavy, self).__init__(parent, 'Heavy weapons')
        w1 = self.variants('Heavy stubber', 120, var_dicts=[{'name': 'Weapon reload', 'points': 60}])
        w2 = self.variants('Mining laser', 200, var_dicts=[{'name': 'Weapon reload', 'points': 100}])
        w3 = self.variants('Seismic cannon', 250, var_dicts=[{'name': 'Weapon reload', 'points': 125}])
        self.weapons = [w1, w2, w3]

    def check_rules(self):
        super(Heavy, self).check_rules()
        OptionsList.process_limit(self.weapons, 1)
