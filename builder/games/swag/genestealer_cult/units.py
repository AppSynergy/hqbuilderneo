__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.swag.unit import Unit
from builder.games.swag.options import Characteristics, Skills
from armory import *


class NLeader(Unit):
    type_name = 'Neophyte Leader'
    type_id = 'gc_leader_v1'

    def __init__(self, parent):
        super(NLeader, self).__init__(parent, points=120, gear=[
            Gear('Combat blade'), Gear('Mining suit')
        ])
        H2H(self, leader=True)
        Pistols(self, leader=True)
        Basic(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Hybrid(Unit):
    type_name = 'Neophyte Hybrid'
    type_id = 'gc_hybrid_v1'

    def __init__(self, parent):
        super(Hybrid, self).__init__(parent, points=60, gear=[
            Gear('Combat blade'), Gear('Mining suit')])
        H2H(self)
        Pistols(self)
        Basic(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Initiate(Unit):
    type_name = 'Neophyte Initiate'
    type_id = 'gc_initiate_v1'

    def __init__(self, parent):
        super(Initiate, self).__init__(parent, points=50, gear=[
            Gear('Combat blade'), Gear('Mining suit')])
        H2H(self)
        Pistols(self)
        Basic(self)
        Grenades(self)
        Misc(self)


class NHeavy(Unit):
    type_name = 'Neophyte Heavy'
    type_id = 'gc_heavy_v1'

    def __init__(self, parent):
        super(NHeavy, self).__init__(parent, points=70, gear=[
            Gear('Combat blade'), Gear('Mining suit')])
        H2H(self)
        Pistols(self)
        Special(self)
        Heavy(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)
