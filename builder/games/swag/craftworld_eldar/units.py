__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.swag.unit import Unit
from builder.games.swag.options import Characteristics, Skills
from armory import *


class Exarch(Unit):
    type_name = 'Dire Avenger Exarch'
    type_id = 'ce_exarch_v1'

    def __init__(self, parent):
        super(Exarch, self).__init__(parent, points=240, gear=[
            Gear('Combat blade'), Gear('Aspect armour')
        ])
        H2H(self, exarch=True)
        Pistols(self)
        Basic(self, exarch=True)
        Grenades(self)
        Misc(self, exarch=True)
        Characteristics(self)
        Skills(self)


class Avenger(Unit):
    type_name = 'Dire Avenger'
    type_id = 'ce_avenger_v1'

    def __init__(self, parent):
        super(Avenger, self).__init__(parent, points=100, gear=[
            Gear('Combat blade'), Gear('Aspect armour')])
        Basic(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Guardian(Unit):
    type_name = 'Guardian Defender'
    type_id = 'ce_guardian_v1'

    def __init__(self, parent):
        super(Guardian, self).__init__(parent, points=80, gear=[
            Gear('Combat blade'), Gear('Mesh armour')])
        Basic(self, guardian=True)
        Grenades(self)
        Misc(self)


class Gunner(Unit):
    type_name = 'Guardian Defender Gunner'
    type_id = 'ce_gunner_v1'

    def __init__(self, parent):
        super(Gunner, self).__init__(parent, points=90, gear=[
            Gear('Combat blade'), Gear('Mesh armour')])
        Basic(self, guardian=True)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class WeaponPlatform(Unit):
    type_name = 'Heavy Weapon Platform'
    type_id = 'ce_platform_v1'

    def __init__(self, parent):
        super(WeaponPlatform, self).__init__(parent)
        Platform(self)

    def build_statistics(self):
        return {}

    def check_rules(self):
        pass
