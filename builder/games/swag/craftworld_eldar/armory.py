__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear


class Grenades(OptionsList):
    def __init__(self, parent):
        super(Grenades, self).__init__(parent, 'Grenades')
        self.variants('Plasma grenades', 25, var_dicts=[
            {'name': 'Weapon reload', 'points': 13}
        ])


class H2H(OptionsList):
    def __init__(self, parent, exarch=False):
        super(H2H, self).__init__(parent, "Hand-to-Hand Weapons")
        self.variant("Combat blade", 5)
        if exarch:
            self.variant("Diresword", 60)
            self.variant('Powerglaive', 50)


class Pistols(OptionsList):
    def __init__(self, parent):
        super(Pistols, self).__init__(parent, "Pistols")
        self.variants("Shuriken pistol", 35, var_dicts=[
            {'name': 'Weapon reload', 'points': 18}
        ])


class Basic(OptionsList):
    def __init__(self, parent, exarch=False, guardian=False):
        super(Basic, self).__init__(parent, 'Basic weapons')
        if not guardian:
            self.variants("Avenger shuriken catapult", 45, var_dicts=[
                {'name': 'Weapon reload', 'points': 23}
            ])
        if guardian:
            self.variants("Shuriken catapult", 40, var_dicts=[
                {'name': 'Weapon reload', 'points': 20}
            ])
        if exarch:
            self.variants("Twin Avenger shuriken catapult", 70, var_dicts=[
                {'name': 'Weapon reload', 'points': 35}
            ])


class Misc(OptionsList):
    def __init__(self, parent, exarch=False):
        super(Misc, self).__init__(parent, 'Miscellaneous')
        self.variant('Photo-visor', 15)
        if exarch:
            self.variant('Shimmershield', 30)


class Platform(OneOf):
    def __init__(self, parent):
        super(Platform, self).__init__(parent, 'Heavy weapon')
        self.variant('Bright lance', 250)
        self.variant('Eldar missile launcher with plasma missiles, starshot missiles', 200)
        self.variant('Scatter laser', 160)
        self.variant('Shuriken cannon', 180)
        self.variant('Starcannon', 200)
