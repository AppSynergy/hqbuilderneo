from builder.games.aos.unit import Unit

__author__ = 'Ivan Truskov'

com_al = 'Shadowblades'


class Assassin(Unit):
    type_name = 'Assassin'
    type_id = 'assas_v1'

    model_name = type_name
    default_points = 80
    allegiance = com_al

    roles = ['leader']


class DarkRiders(Unit):
    type_name = 'Dark Riders'
    type_id = 'dark_rider_v1'

    model_name = 'Dark Rider'
    min_size = 5
    max_size = 20
    default_points = 140
    allegiance = com_al

    def get_roles(self):
        if self.roster.allegiance == com_al:
            return ['battleline']
        else:
            return []
