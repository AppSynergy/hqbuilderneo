__author__ = 'Ivan Truskov'

from order import AllianceOrderVanguard, AllianceOrderBattlehost,\
    AllianceOrderWarhost, AllianceOrderPoints

armies = [
    AllianceOrderVanguard,
    AllianceOrderBattlehost,
    AllianceOrderWarhost,
    AllianceOrderPoints
]
