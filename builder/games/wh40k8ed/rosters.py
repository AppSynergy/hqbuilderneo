__author__ = 'Ivan Truskov'

from builder.core2 import Roster, OneOf
from sections import *
from string import capwords


class FactionSelector(OneOf):
    def __init__(self, parent, selector_base, default_value, selector_variants):
        name_str = selector_base
        if name_str[0:1] == '<':
            name_str = selector_base[1:-1]
        super(FactionSelector, self).__init__(parent, capwords(name_str.lower()))
        self.lookup = dict()
        if default_value[0:1] == '<':
            self.default_value = self.variant('<{}>'.format(capwords(default_value[1:-1].lower())))
            self.lookup[self.default_value] = default_value
        else:
            self.default_value = self.variant(capwords(default_value.lower()))
            self.lookup[self.default_value] = default_value
        for var in selector_variants:
            if var[0:1] == '<':
                self.lookup[self.variant('<{}>'.format(capwords(var[1:-1].lower())))] = var
            else:
                self.lookup[self.variant(capwords(var.lower()))] = var

    def is_default(self):
        return self.cur == self.default_value

    @property
    def faction(self):
        return self.lookup[self.cur]

    def check_rules(self):
        super(FactionSelector, self).check_rules()
        # do not apply on toplevel
        if self.roster == self.root:
            return
        # check army faction
        if not self.parent.second_variants:
            army_faction = self.root.faction_base
            if army_faction == self.parent.faction_base:
                # set the same option
                for var, fac in self.lookup.items():
                    var.active = fac == self.root.faction
                else:
                    from gen_metadata import exclusive_factions
                    if army_faction in exclusive_factions.keys():
                        for var, fac in self.lookup.items():
                            var.active = fac not in exclusive_factions[army_faction]


class DetachBase8ed(Roster):
    command = 0

    hq_sec = HQSection
    fast_sec = FastSection
    troops_sec = TroopsSection
    elite_sec = ElitesSection
    heavy_sec = HeavySection
    lords_sec = LordsOfWarSection
    transport_sec = TransportSection
    fort_sec = FortSection
    flier_sec = FlyerSection

    faction_base = 'UNALIGNED'
    alternate_factions = []
    second_variants = []

    def __init__(self, parent=None, hq=False, elite=False, troops=False,
                 fast=False, heavy=False, lords=False, transports=False,
                 fliers=False, fort=False):
        self.hq = None
        if hq:
            self.hq = self.hq_sec(self)
        self.elite = None
        if elite:
            self.elite = self.elite_sec(self)
        self.troops = None
        if troops:
            self.troops = self.troops_sec(self)
        self.fast = None
        if fast:
            self.fast = self.fast_sec(self)
        self.heavy = None
        if heavy:
            self.heavy = self.heavy_sec(self)
        self.lords = None
        if lords:
            self.lords = self.lords_sec(self)
        self.transports = None
        if transports:
            self.transports = self.transport_sec(self)
        self.fliers = None
        if fliers:
            self.fliers = self.flier_sec(self)
        self.fort = None
        if fort:
            self.fort = self.fort_sec(self)
        super(DetachBase8ed, self).__init__([
            sec for sec in [self.hq, self.elite, self.troops,
                            self.fast, self.heavy, self.lords,
                            self.transports, self.fliers, self.fort]
            if sec is not None
        ], parent=parent)
        from gen_metadata import second_factions
        if self.faction_base in second_factions:
            self.second_variants = second_factions[self.faction_base]
        if len(self.alternate_factions):
            self.fac_sel = FactionSelector(self, self.faction_base, self.faction_base, self.alternate_factions)
        else:
            if len(self.second_variants):
                self.fac_sel = FactionSelector(self, self.second_variants[0], 'None', self.second_variants[1:])
            else:
                self.fac_sel = None
        # if self.fac_sel is not None:
        #     self.make_property(self.fac_sel)

    def check_rules(self):
        from gen_metadata import exclusive_factions
        if self.root.faction in exclusive_factions.get(self.faction, []):
            self.error('Cannot include {} detachment in {} army'.format(self.faction, self.root.faction))
        if self.fac_sel is not None:
            if self.root.fac_sel is not None:
                self.fac_sel.used = self.fac_sel.visible = False
            else:
                self.fac_sel.used = not self.fac_sel.is_default()

    @property
    def faction(self):
        if self.fac_sel and len(self.alternate_factions):
            return self.fac_sel.faction
        else:
            return self.faction_base

    @property
    def second_faction(self):
        if self.fac_sel and len(self.second_variants):
            if self.fac_sel.is_default():
                return None
            else:
                if self.root.fac_sel is None:
                    return self.fac_sel.faction
                else:
                    return None
        else:
            return None

    def build_statistics(self):
        res = super(DetachBase8ed, self).build_statistics()
        # accouting for warlords
        cp = res.get('Command points', 0)
        res['Command points'] = cp + self.command
        res['Detachments'] = 1
        return res


class DetachPlanetstrikeAttacker(DetachBase8ed):
    army_name = 'Planetstrike Attacker detachment'
    army_id = 'planetstrike attacker'
    command = 5

    def __init__(self, troops=False,
                 heavy=False, transports=False,
                 fliers=False, **kwargs):
        super(DetachPlanetstrikeAttacker, self).__init__(hq=True, fast=True, elite=True,
                                                         troops=troops, heavy=heavy,
                                                         transports=transports, fliers=fliers)
        self.hq.min, self.hq.max = (2, 3)
        if troops:
            self.troops.min, self.troops.max = (0, 6)
        self.elite.min, self.elite.max = (3, 9)
        self.fast.min, self.fast.max = (3, 6)
        if heavy:
            self.heavy.min, self.heavy.max = (0, 2)
        if fliers:
            self.fliers.min, self.fliers.max = (0, 2)

    def check_rules(self):
        super(DetachPlanetstrikeAttacker, self).check_rules()
        if self.transports:
            total_cur = sum(len(sec.units) for sec in
                            [self.hq, self.elite, self.troops,
                             self.fast, self.heavy,
                             self.fliers] if sec is not None)
            if len(self.transports.units) > total_cur:
                self.error('May include only 1 dedicated transport for one other choice')


class DetachPlanetstrikeDefender(DetachBase8ed):
    army_name = 'Planetstrike Defender detachment'
    army_id = 'planetstrike attacker'
    command = 5

    def __init__(self, elite=False,
                 fast=False, transports=False,
                 fliers=False, **kwargs):
        super(DetachPlanetstrikeDefender, self).__init__(hq=True, troops=True, heavy=True,
                                                         fast=fast, elite=elite,
                                                         transports=transports, fliers=fliers)
        self.hq.min, self.hq.max = (2, 3)
        self.troops.min, self.troops.max = (3, 9)
        if elite:
            self.elite.min, self.elite.max = (0, 6)
        if fast:
            self.fast.min, self.fast.max = (0, 3)
        self.heavy.min, self.heavy.max = (3, 6)
        if fliers:
            self.fliers.min, self.fliers.max = (0, 2)

    def check_rules(self):
        super(DetachPlanetstrikeDefender, self).check_rules()
        if self.transports:
            total_cur = sum(len(sec.units) for sec in
                            [self.hq, self.elite, self.troops,
                             self.fast, self.heavy,
                             self.fliers] if sec is not None)
            if len(self.transports.units) > total_cur:
                self.error('May include only 1 dedicated transport for one other choice')


class DetachPatrol(DetachBase8ed):
    army_name = 'Patrol detachment'
    army_id = 'patrol'
    command = 0

    def __init__(self, elite=False, fast=False,
                 heavy=False, transports=False,
                 fliers=False, **kwargs):
        super(DetachPatrol, self).__init__(hq=True, troops=True,
                                           elite=elite, fast=fast, heavy=heavy,
                                           transports=transports, fliers=fliers)
        self.hq.min, self.hq.max = (1, 2)
        self.troops.min, self.troops.max = (1, 3)
        if elite:
            self.elite.min, self.elite.max = (0, 2)
        if fast:
            self.fast.min, self.fast.max = (0, 2)
        if heavy:
            self.heavy.min, self.heavy.max = (0, 2)
        if fliers:
            self.fliers.min, self.fliers.max = (0, 2)

    def check_rules(self):
        super(DetachPatrol, self).check_rules()
        if self.transports:
            total_cur = sum(len(sec.units) for sec in
                            [self.hq, self.elite, self.troops,
                             self.fast, self.heavy,
                             self.fliers] if sec is not None)
            if len(self.transports.units) > total_cur:
                self.error('May include only 1 dedicated transport for one other choice')


class DetachBatallion(DetachBase8ed):
    army_name = 'Batallion detachment'
    army_id = 'batallion'
    command = 5

    def __init__(self, elite=False, fast=False,
                 heavy=False, transports=False,
                 fliers=False, **kwargs):
        super(DetachBatallion, self).__init__(hq=True, troops=True,
                                              elite=elite, fast=fast, heavy=heavy,
                                              transports=transports, fliers=fliers)
        self.hq.min, self.hq.max = (2, 3)
        self.troops.min, self.troops.max = (3, 6)
        if elite:
            self.elite.min, self.elite.max = (0, 6)
        if fast:
            self.fast.min, self.fast.max = (0, 3)
        if heavy:
            self.heavy.min, self.heavy.max = (0, 3)
        if fliers:
            self.fliers.min, self.fliers.max = (0, 2)

    def check_rules(self):
        super(DetachBatallion, self).check_rules()
        if self.transports:
            total_cur = sum(len(sec.units) for sec in
                            [self.hq, self.elite, self.troops,
                             self.fast, self.heavy,
                             self.fliers] if sec is not None)
            if len(self.transports.units) > total_cur:
                self.error('May include only 1 dedicated transport for one other choice')


class DetachBrigade(DetachBase8ed):
    army_name = 'Brigade detachment'
    army_id = 'brigade'
    command = 12

    def __init__(self, transports=False,
                 fliers=False, **kwargs):
        super(DetachBrigade, self).__init__(hq=True, troops=True,
                                              elite=True, fast=True, heavy=True,
                                              transports=transports, fliers=fliers)
        self.hq.min, self.hq.max = (3, 5)
        self.troops.min, self.troops.max = (6, 12)
        self.elite.min, self.elite.max = (3, 8)
        self.fast.min, self.fast.max = (3, 5)
        self.heavy.min, self.heavy.max = (3, 5)
        if fliers:
            self.fliers.min, self.fliers.max = (0, 2)

    def check_rules(self):
        super(DetachBrigade, self).check_rules()
        if self.transports:
            total_cur = sum(len(sec.units) for sec in
                            [self.hq, self.elite, self.troops,
                             self.fast, self.heavy,
                             self.fliers] if sec is not None)
            if len(self.transports.units) > total_cur:
                self.error('May include only 1 dedicated transport for one other choice')


class DetachVanguard(DetachBase8ed):
    army_name = 'Vanguard detachment'
    army_id = 'vanguard'
    command = 1

    def __init__(self, troops=False, fast=False,
                 heavy=False, transports=False,
                 fliers=False, **kwargs):
        hq_flag = kwargs.pop('hq', True)
        super(DetachVanguard, self).__init__(hq=hq_flag, troops=troops,
                                             elite=True, fast=fast, heavy=heavy,
                                             transports=transports, fliers=fliers)
        # there are cases...
        if hq_flag:
            self.hq.min, self.hq.max = (1, 2)
        if troops:
            self.troops.min, self.troops.max = (0, 3)
        self.elite.min, self.elite.max = (3, 6)
        if fast:
            self.fast.min, self.fast.max = (0, 2)
        if heavy:
            self.heavy.min, self.heavy.max = (0, 2)
        if fliers:
            self.fliers.min, self.fliers.max = (0, 2)

    def check_rules(self):
        super(DetachVanguard, self).check_rules()
        if self.transports:
            total_cur = sum(len(sec.units) for sec in
                            [self.hq, self.elite, self.troops,
                             self.fast, self.heavy,
                             self.fliers] if sec is not None)
            if len(self.transports.units) > total_cur:
                self.error('May include only 1 dedicated transport for one other choice')


class DetachSpearhead(DetachBase8ed):
    army_name = 'Spearhead detachment'
    army_id = 'spearhead'
    command = 1

    def __init__(self, troops=False, elite=False,
                 fast=False, transports=False,
                 fliers=False, **kwargs):
        super(DetachSpearhead, self).__init__(hq=True, troops=troops,
                                              elite=elite, fast=fast, heavy=True,
                                              transports=transports, fliers=fliers)
        self.hq.min, self.hq.max = (1, 2)
        if troops:
            self.troops.min, self.troops.max = (0, 3)
        if elite:
            self.elite.min, self.elite.max = (0, 2)
        if fast:
            self.fast.min, self.fast.max = (0, 2)
        self.heavy.min, self.heavy.max = (3, 6)
        if fliers:
            self.fliers.min, self.fliers.max = (0, 2)

    def check_rules(self):
        super(DetachSpearhead, self).check_rules()
        if self.transports:
            total_cur = sum(len(sec.units) for sec in
                            [self.hq, self.elite, self.troops,
                             self.fast, self.heavy,
                             self.fliers] if sec is not None)
            if len(self.transports.units) > total_cur:
                self.error('May include only 1 dedicated transport for one other choice')


class DetachOutrider(DetachBase8ed):
    army_name = 'Outrider detachment'
    army_id = 'outrider'
    command = 1

    def __init__(self, troops=False, elite=False,
                 heavy=False, transports=False,
                 fliers=False, **kwargs):
        super(DetachOutrider, self).__init__(hq=True, troops=troops,
                                             elite=elite, fast=True, heavy=heavy,
                                             transports=transports, fliers=fliers)
        self.hq.min, self.hq.max = (1, 2)
        if troops:
            self.troops.min, self.troops.max = (0, 3)
        if elite:
            self.elite.min, self.elite.max = (0, 2)
        self.fast.min, self.fast.max = (3, 6)
        if heavy:
            self.heavy.min, self.heavy.max = (0, 2)
        if fliers:
            self.fliers.min, self.fliers.max = (0, 2)

    def check_rules(self):
        super(DetachOutrider, self).check_rules()
        if self.transports:
            total_cur = sum(len(sec.units) for sec in
                            [self.hq, self.elite, self.troops,
                             self.fast, self.heavy,
                             self.fliers] if sec is not None)
            if len(self.transports.units) > total_cur:
                self.error('May include only 1 dedicated transport for one other choice')


class DetachCommand(DetachBase8ed):
    army_name = 'Supreme command detachment'
    army_id = 'command'
    command = 1

    def __init__(self, elite=False,
                 lords=False, transports=False, **kwargs):
        super(DetachCommand, self).__init__(hq=True, elite=elite,
                                            transports=transports, lords=lords)
        self.hq.min, self.hq.max = (3, 5)
        if elite:
            self.elite.min, self.elite.max = (0, 1)
        if lords:
            self.lords.min, self.lords.max = (0, 1)

    def check_rules(self):
        super(DetachCommand, self).check_rules()
        if self.transports:
            total_cur = sum(len(sec.units) for sec in
                            [self.hq, self.elite, self.lords]
                            if sec is not None)
            if len(self.transports.units) > total_cur:
                self.error('May include only 1 dedicated transport for one other choice')


class DetachSuperHeavy(DetachBase8ed):
    army_name = 'Super-Heavy detachment'
    army_id = 'super_heavy'
    command = 3

    def __init__(self, **kwargs):
        super(DetachSuperHeavy, self).__init__(lords=True)
        self.lords.min, self.lords.max = (3, 5)


class DetachAirWing(DetachBase8ed):
    army_name = 'Air Wing detachment'
    army_id = 'air_wing'
    command = 1

    def __init__(self, **kwargs):
        super(DetachAirWing, self).__init__(fliers=True)
        self.fliers.min, self.fliers.max = (3, 5)


class DetachSuperHeavyAux(DetachBase8ed):
    army_name = 'Super-Heavy auxilary detachment'
    army_id = 'super_heavy_aux'
    command = 0

    def __init__(self, **kwargs):
        super(DetachSuperHeavyAux, self).__init__(lords=True)
        self.lords.min, self.lords.max = (1, 1)


class DetachFort(DetachBase8ed):
    army_name = 'Fortification Network'
    army_id = 'fort'
    command = 0
    faction_base = 'UNALIGNED'

    def __init__(self, **kwargs):
        """
        Always add fortifications that are unalifned
        """
        super(DetachFort, self).__init__(fort=True)
        self.fort.min, self.fort.max = (1, 3)
        from imperium_other import fortification_types
        self.fort.add_classes(fortification_types)


class DetachFortPlanetstrike(DetachFort):
    """
Fortifications for Planetstrike Defender are free
    """
    def calc_points(self):
        return 0


class DetachAuxilary(Roster):
    army_name = 'Auxilary Support Detachment'
    army_id = 'Auxilary'
    command = -1

    class SupportSection(section.Section):
        def __init__(self, parent):
            super(DetachAuxilary.SupportSection, self).__init__(parent, 'aux', 'Support', 1, 1)

        def add_classes(self, class_list):
            for ut in class_list:
                UnitType(self, ut)

    def __init__(self, **kwargs): 
        self.aux = self.SupportSection(self)
        # here this one section plays the role of every section
        self.hq = self.troops = self.elite = self.fast = self.heavy =\
                  self.transports = self.fliers = self.aux
        parent = kwargs.pop('parent', None)
        super(DetachAuxilary, self).__init__([self.aux], parent)

    def build_statistics(self):
        res = super(DetachAuxilary, self).build_statistics()
        res['Command points'] = self.command
        return res


class Wh40k8edBase(Roster):
    game_name = 'Warhammer 40k'
    roster_type = 'Battle-forged'
    base_tags = ['Warhammer 40k', roster_type]
    faction_base = None
    alternate_factions = []
    known_warlord = None

    common_fort = True

    class PowerPoints(OneOf):
        def __init__(self, parent):
            super(Wh40k8edBase.PowerPoints, self).__init__(parent, 'Point system',
                                                           used=False, node_id='power_switch')
            self.variant('Matched play cost')
            self.power = self.variant('Power points')

    def __init__(self, parent=None):
        self.det = DetachmentsSection(self)
        super(Wh40k8edBase, self).__init__(self.section_list(), parent)
        self.pchoice = self.PowerPoints(self)
        self.add_detachments()

        if self.common_fort:
            self.det.build_detach(DetachFort, self.faction_base, order=333)

        if len(self.alternate_factions):
            self.fac_sel = FactionSelector(self, self.faction_base, self.faction_base, self.alternate_factions)
            self.make_property(self.fac_sel)
        else:
            self.fac_sel = None

    def section_list(self):
        return [self.det]

    def add_detachments(self):
        pass

    def save(self):
        res = super(Wh40k8edBase, self).save()
        res.update(
            {'properties': map(lambda o: o.dump(), self.properties)}
        )
        return res

    def check_rules(self):
        super(Wh40k8edBase, self).check_rules()
        if self.fac_sel is not None:
            self.fac_sel.used = not self.fac_sel.is_default()
    
    def load(self, data):
        for option_data in data.get('properties', []):
            try:
                next(o for o in self.properties if o.id == option_data['id']).load(option_data)
            except StopIteration:
                print 'StopIteration for', option_data['id']
        super(Wh40k8edBase, self).load(data)

    def update(self, data):
        self.known_warlord = None
        super(Wh40k8edBase, self).update(data)
        if 'power_switch' in [od['id'] for od in data.get('options', [])]:
            from builder.tools import ReloadRosterError
            raise ReloadRosterError

    @property
    def faction(self):
        if len(self.alternate_factions):
            return self.fac_sel.faction
        else:
            return self.faction_base

    def _build_unique_map(self, names_getter):
        unique_map = {}
        for sec in self.sections:
            for unit in sec.units:
                if hasattr(unit, 'get_unique_map'):
                    submap = unit.get_unique_map(names_getter)
                    for (k, v) in submap.iteritems():
                        if k in unique_map.keys():
                            unique_map[k] += v
                        else:
                            unique_map[k] = v
                else:
                    un = names_getter(unit)
                    if un is None:
                        continue
                    if not isinstance(un, (list, type)):
                        un = [un]
                    for name in un:
                        if name in unique_map.keys():
                            unique_map[name] += 1
                        else:
                            unique_map[name] = 1
        return unique_map

    def build_statistics(self):
        res = super(Wh40k8edBase, self).build_statistics()
        cp = res.pop('Command points', 0)
        cp += 3
        res['Command points'] = cp
        if not self.use_power:
            if self.limit is not None:
                if self.points < self.limit:
                    res['Reinforcements points'] = self.limit - self.points
        return res

    def dump(self):
        res = super(Wh40k8edBase, self).dump()
        if self == self.root:
            res.update({
                'statistics': self.build_statistics()
            })
        return res

    @property
    def delta(self):
        res = super(Wh40k8edBase, self).delta
        if self == self.root:
            res.update({
                'statistics': self.build_statistics()
            })
        return res

    @property
    def use_power(self):
        return self.pchoice.cur == self.pchoice.power


class Wh40k8edMission(Wh40k8edBase):
    class MissionSection(DetachmentsSection):
        sec_id = 'mission'
        sec_name = 'Mission detachments'

    def __init__(self, parent=None):
        self.mis = self.MissionSection(self)
        super(Wh40k8edMission, self).__init__(parent)
        self.det.min = 0

    def section_list(self):
        return [self.mis, self.det]


class Wh8edPlanetstrikeAttacker(Wh40k8edMission):
    common_fort = False
    roster_type = u'Planetstrike (Attacker)'
    base_tags = ['Warhammer 40k', roster_type]

    def __init__(self, parent=None):
        super(Wh8edPlanetstrikeAttacker, self).__init__(parent)
        self.det.remove_unit_class(DetachFort)


class Wh8edPlanetstrikeDefender(Wh40k8edMission):
    common_fort = False
    roster_type = u'Planetstrike (Defender)'
    base_tags = ['Warhammer 40k', roster_type]

    def __init__(self, parent=None):
        super(Wh8edPlanetstrikeDefender, self).__init__(parent)
        self.mis.min = 2
        self.det.remove_unit_class(DetachFort)
        cand = [uc for uc in self.mis.types if issubclass(uc.unit_class, DetachFortPlanetstrike)]
        if not cand:
            self.mis.build_detach(DetachFortPlanetstrike, self.faction_base)

    def check_rules(self):
        super(Wh8edPlanetstrikeDefender, self).check_rules()
        has_def = False
        has_fort = False
        for u in self.mis.units:
            if isinstance(u.sub_roster.roster, DetachFortPlanetstrike):
                has_fort = True
            if isinstance(u.sub_roster.roster, DetachPlanetstrikeDefender):
                has_def = True

        if not (has_fort and has_def):
            self.error('Army must include at least one fortification and Planetstrike (Defender) detachment')
