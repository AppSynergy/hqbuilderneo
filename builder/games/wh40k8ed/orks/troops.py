import armory
from builder.games.wh40k8ed.unit import Unit, TroopsUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList, \
    Gear, UnitDescription, Count, ListSubUnit, SubUnit, UnitList,\
    OptionalSubUnit
import melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *


class Boyz(TroopsUnit, armory.OrkClanUnit):
    type_name = u'Boyz'
    type_id = 'boyz_v1'
    keywords = ['Infantry', 'Boyz']
    model = UnitDescription('Boy', options=create_gears(ranged.Stikkbombs))
    power = 4

    class BossNob(Unit):
        type_name = 'Boss Nob'

        class BossRanged(OneOf):
            def __init__(self, parent):
                super(Boyz.BossNob.BossRanged, self).__init__(parent, 'Ranged Weapon')
                self.variant(*ranged.Slugga)
                armory.add_nob_weapons(self, slugga=False, double=True)

        class BossMelee(OneOf):
            def __init__(self, parent):
                super(Boyz.BossNob.BossMelee, self).__init__(parent, 'Melee Weapon')
                self.variant(*melee.Choppa)
                armory.add_nob_weapons(self, choppa=False, double=False)

        def build_points(self):
            res = super(Boyz.BossNob, self).build_points()
            if self.rng.cur == self.rng.ksaw and self.mle.cur == self.mle.ksaw:
                res -= 2 * get_cost(melee.Killsaw) - get_cost(melee.KillsawPair)
            return res

        def __init__(self, parent):
            super(Boyz.BossNob, self).__init__(parent, gear=create_gears(ranged.Stikkbombs),
                                               points=get_costs(units.Boyz))
            self.rng = self.BossRanged(self)
            self.mle = self.BossMelee(self)

    def variant(self, name, points):
        self.eavy.append(Count(self, name, 0, 1, points, gear=Boyz.model.clone().add(Gear(name))))

    class Boy(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(Boyz.Boy.Weapon, self).__init__(parent, 'Weapon')
                self.variant(
                    join_and(ranged.Slugga, melee.Choppa),
                    get_costs(ranged.Slugga, melee.Choppa),
                    gear=create_gears(ranged.Slugga, melee.Choppa))
                self.variant(*ranged.Shoota)
                self.heavy = [
                    self.variant(*ranged.RokkitLauncha),
                    self.variant(*ranged.BigShoota)
                ]

        class Options(OptionsList):
            def __init__(self, parent):
                super(Boyz.Boy.Options, self).__init__(parent, 'Options')
                self.variant(*ranged.TankbustaBombs)

        def __init__(self, parent):
            gear = [ranged.Stikkbombs]
            cost = points_price(
                get_costs(units.Boyz),
                *gear)
            super(Boyz.Boy, self).__init__(parent, 'Boy', points=cost, gear=create_gears(*gear))
            self.wep = self.Weapon(self)
            self.opt = self.Options(self)

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.wep.cur in self.wep.heavy

        @ListSubUnit.count_gear
        def has_bomb(self):
            return self.opt.any

    class BossOption(OptionalSubUnit):
        def __init__(self, parent):
            super(Boyz.BossOption, self).__init__(parent, 'Boss')
            SubUnit(self, Boyz.BossNob(parent))

    def __init__(self, parent):
        super(Boyz, self).__init__(parent)
        self.models = UnitList(self, self.Boy, 9, 30, start_value=10)
        self.boss_opts = self.BossOption(self)

    def check_rules(self):
        super(Boyz, self).check_rules()
        self.models.min, self.models.max = (10 - self.boss_opts.any, 30 - self.boss_opts.any)
        max_num = self.get_count() / 10
        eavy_cnt = sum(u.has_heavy() for u in self.models.units)
        if eavy_cnt > max_num:
            self.error('Only one heavy weapon may be taken for every 10 models')
        bomb_cnt = sum(u.has_bomb() for u in self.models.units)
        if bomb_cnt > max_num:
            self.error('Only one tankbusta bomb may be taken for every 10 models')


    def get_count(self):
        return self.models.count + self.boss_opts.count

    def build_power(self):
        return self.power + (7 if self.get_count() > 20 else (3 if self.get_count() > 10 else 0))


class Gretchin(TroopsUnit, armory.OrkClanUnit):
    type_name = u'Gretchin'
    type_id = 'gretchin_v1'
    keywords = ['Infantry', 'Gretchin']
    power = 1

    model = UnitDescription('Gretchin', options=create_gears(ranged.GrotBlasta))

    class Gretchins(Count):
        pass

    def __init__(self, parent):
        super(Gretchin, self).__init__(parent)
        self.models = self.Gretchins(self, 'Gretchin', 10, 30, points=3, per_model=True,
                                     gear=self.model.clone())

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power + (3 if self.models.cur > 20 else (1 if self.models.cur > 10 else 0))
