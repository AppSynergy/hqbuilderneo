from builder.games.wh40k8ed.unit import Unit
import ranged


def add_caparace_weapons(instance):
    instance.variant(*ranged.TwinIcarusAutocannon)
    instance.variant(*ranged.StormspearRocketPod)
    instance.variant(*ranged.IronstormMissilePod)


class ImperialKnightUnit(Unit):
    faction = ['Imperium', 'Imperial Knights']
    wiki_faction = 'Questor Imperialis'


class KnightUnit(ImperialKnightUnit):
    faction = ['<Questor Allegiance>', '<Household>']
    keywords = ['Vehicle']
    is_armiger = False
    is_questoris = False
    is_dominus = False

    @classmethod
    def calc_faction(cls):
        imperialis = ['TERRIN', 'GRIFFIN', 'HAWKSHROUD', 'CADMUS', 'MORTAN']
        mechanicus = ['RAVEN', 'TARANIS', 'KRAST', 'VULKER']
        return imperialis + mechanicus


class ArmigerUnit(KnightUnit):
    keywords = ['Armiger Class']

    is_armiger = True


class QuestorisUnit(KnightUnit):
    keywords = ['Titanic', 'Questoris Class']

    is_questoris = True


class DominusUnit(KnightUnit):
    keywords = ['Titanic', 'Dominus Class']

    is_dominus = True
