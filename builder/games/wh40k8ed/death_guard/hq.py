__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HqUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, OptionsList
import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class DGChaosLord(HqUnit, armory.ClawUser, armory.GuardUnit):
    type_name = 'Death Guard ' + get_name(units.ChaosLord)
    type_id = 'dg_clord_v1'
    keywords = ['Character', 'Infantry']
    power = 5

    class Pistol(OneOf):
        def __init__(self, parent):
            super(DGChaosLord.Pistol, self).__init__(parent, 'Weapon')
            self.variant(*ranged.BoltPistol)
            self.variant(*ranged.PlasmaPistol)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    class Sword(OneOf):
        def __init__(self, parent):
            super(DGChaosLord.Sword, self).__init__(parent, '')
            self.variant(*melee.Chainsword)
            armory.add_melee_weapons(self)

    def __init__(self, parent):
        super(DGChaosLord, self).__init__(parent, points=get_cost(units.ChaosLord),
                                        gear=[Gear('Blight grenades'), Gear('Krak grenades')])
        self.wep1 = self.Pistol(self)
        self.wep2 = self.Sword(self)


class DGTermoLord(HqUnit, armory.ClawUser, armory.GuardUnit):
    type_name = 'Death Guard ' + get_name(units.TerminatorChaosLord)
    type_id = 'dg_tlord_v1'
    keywords = ['Character', 'Infantry', 'Terminator']
    power = 6

    class Bolter(OneOf):
        def __init__(self, parent):
            super(DGTermoLord.Bolter, self).__init__(parent, 'Weapon')
            armory.add_combi_weapons(self)
            armory.add_terminator_melee(self)

    class Sword(OneOf):
        def __init__(self, parent):
            super(DGTermoLord.Sword, self).__init__(parent, '')
            self.variant(*melee.PowerSword)
            armory.add_terminator_melee(self, sword=False)

    def __init__(self, parent):
        super(DGTermoLord, self).__init__(parent, points=get_cost(units.TerminatorChaosLord))
        self.wep1 = self.Bolter(self)
        self.wep2 = self.Sword(self)


class Typhus(HqUnit, armory.GuardUnit):
    type_name = get_name(units.Typhus)
    type_id = 'typhus_v1'
    keywords = ['Character', 'Infantry', 'Lord of Contagion', 'Terminator', 'Psyker']
    power = 9

    def __init__(self, parent):
        super(Typhus, self).__init__(parent=parent, points=get_cost(units.Typhus),
                                     unique=True, gear=[
                                         Gear('Blight grenades'),
                                         Gear("Master-crafted manreaper"), Gear('Destroyer Hive')
                                     ], static=True)


class NurgleDaemonPrince(HqUnit, armory.GuardUnit):
    type_name = get_name(units.DaemonPrinceOfNurgle)
    type_id = 'dg_dp_v1'
    kwname = 'Daemon Prince'
    keywords = ['Character', 'Monster', 'Daemon']

    power = 8

    class Weapon(OneOf):
        def __init__(self, parent):
            super(NurgleDaemonPrince.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*melee.HellforgedSword)
            self.variant(*melee.DaemonicAxe)
            self.variant('Malefic talons', 10)

    class Options(OptionsList):
        def __init__(self, parent):
            super(NurgleDaemonPrince.Options, self).__init__(parent, 'Options')
            self.wings = self.variant(*wargear.Wings)

    def __init__(self, parent):
        super(NurgleDaemonPrince, self).__init__(parent, gear=[Gear('Malefic talons')],
                                           points=get_cost(units.DaemonPrinceOfNurgle))
        self.Weapon(self)
        self.opt = self.Options(self)

    def build_power(self):
        return self.power + self.opt.any


class ContagionLord(HqUnit, armory.GuardUnit):
    type_name = get_name(units.LordOfContagion)
    type_id = 'cont_lord_v1'
    keywords = ['Character', 'Infantry', 'Terminator']
    power = 7

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ContagionLord.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*melee.Plaguereaper)
            self.variant(*melee.Manreaper)

    def __init__(self, parent):
        super(ContagionLord, self).__init__(parent=parent,
                                            points=get_cost(units.LordOfContagion))
        self.Weapon(self)


class DGSorcerer(HqUnit, armory.GuardUnit):
    type_name = 'Death Guard ' + get_name(units.Sorcerer)
    type_id = 'dgsorc_v1'
    keywords = ['Character', 'Infantry', 'Psyker']
    power = 6

    class Pistol(OneOf):
        def __init__(self, parent):
            super(DGSorcerer.Pistol, self).__init__(parent, 'Weapon')
            self.variant(*ranged.BoltPistol)
            self.variant(*ranged.PlasmaPistol)
            armory.add_combi_weapons(self)

    class Sword(OneOf):
        def __init__(self, parent):
            super(DGSorcerer.Sword, self).__init__(parent, '')
            self.variant(*melee.ForceSword)
            self.variant(*melee.ForceAxe)
            self.variant(*melee.ForceStave)

    def __init__(self, parent):
        super(DGSorcerer, self).__init__(parent, points=get_cost(units.Sorcerer),
                                         gear=[Gear('Blight grenades'), Gear('Krak grenades')])
        self.Pistol(self)
        self.Sword(self)


class DGTermoSorcerer(HqUnit, armory.ClawUser, armory.GuardUnit):
    type_name = 'Death Guard ' + get_name(units.TerminatorSorcerer)
    type_id = 'dg_tsorc_v1'
    keywords = ['Character', 'Infantry', 'Psyker', 'Terminator']
    power = 8

    class Bolter(OneOf):
        def __init__(self, parent):
            super(DGTermoSorcerer.Bolter, self).__init__(parent, 'Weapon')
            armory.add_combi_weapons(self)
            armory.add_terminator_melee(self)

    class Sword(OneOf):
        def __init__(self, parent):
            super(DGTermoSorcerer.Sword, self).__init__(parent, '')
            self.variant(*melee.ForceStave)
            self.variant(*melee.ForceAxe)
            self.variant(*melee.ForceSword)
            armory.add_terminator_melee(self)

    def __init__(self, parent):
        super(DGTermoSorcerer, self).__init__(parent, points=get_cost(units.TerminatorSorcerer))
        self.wep1 = self.Bolter(self)
        self.wep2 = self.Sword(self)


class Plaguecaster(HqUnit, armory.GuardUnit):
    type_name = get_name(units.MalignantPlaguecaster)
    type_id = 'plaguecaster_v1'

    keywords = ['Character', 'Infantry', 'Psyker']
    power = 6

    def __init__(self, parent):
        gear = [melee.CorruptedStaff, ranged.BoltPistol,
                ranged.BlightGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.MalignantPlaguecaster), *gear)
        super(Plaguecaster, self).__init__(parent=parent, points=cost,
                                           gear=create_gears(*gear), static=True)
