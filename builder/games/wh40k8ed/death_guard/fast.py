__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit
import armory
import ranged, melee, units
from builder.games.wh40k8ed.utils import *


class BloatDrone(FastUnit, armory.GuardUnit):
    type_name = get_name(units.FoetidBloatdrone)
    type_id = "bloat_drone_v1"

    keywords = ['Vehicle', 'Daemon', 'Daemon Engine', 'Fly']
    power = 8

    class Weapon(OneOf):
        def __init__(self, parent):
            super(BloatDrone.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Two plaguesplitters', 2 * get_cost(ranged.Plaguespitter),
                         gear=create_gears(ranged.Plaguespitter, ranged.Plaguespitter))
            self.variant(*ranged.HeavyBlightLauncher)
            self.variant(*melee.Fleshmower)

    def __init__(self, parent):
        super(BloatDrone, self).__init__(parent, points=get_costs(units.FoetidBloatdrone, melee.PlagueProbe),
                                         gear=create_gears(melee.PlagueProbe))
        self.Weapon(self)


class BlightHaulers(FastUnit, armory.GuardUnit):
    type_name = get_name(units.MyphiticBlighthaulers)
    type_id = 'blight_haulers_v1'
    keywords = ['Vehicle', 'Daemon', 'Daemon engine']
    power = 7

    def __init__(self, parent):
        super(BlightHaulers, self).__init__(parent)
        model_gear = [ranged.MissileLauncher, ranged.MultiMelta, ranged.BileSpurt, melee.GnashingMaw]
        model_cost = points_price(get_cost(units.MyphiticBlighthaulers), *model_gear)
        self.models = Count(self, 'Myphitic Blight-Haulers', 1, 3, model_cost,
                            gear=UnitDescription('Mythicit Blight-Hauler', options=create_gears(*model_gear)))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * self.models.cur
