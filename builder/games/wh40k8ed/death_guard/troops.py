__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit
import armory
import ranged, melee, units
from builder.games.wh40k8ed.utils import *


class DGPlagueMarinesV2(TroopsUnit, armory.GuardUnit):
    type_name = get_name(units.PlagueMarines)
    type_id = 'dg_plag_v2'
    keywords = ['Infantry']
    power = 7

    class Champion(Unit):
        type_name = u'Plague Champion'

        class Melee(OneOf):
            def __init__(self, parent):
                super(DGPlagueMarinesV2.Champion.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.PlagueKnife)
                self.variant(*melee.Plaguesword)

        class Ranged(OneOf):
            def __init__(self, parent):
                super(DGPlagueMarinesV2.Champion.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.Boltgun)
                self.variant(*ranged.BoltPistol)
                self.variant(*ranged.PlasmaPistol)
                self.variant(*ranged.PlasmaGun)

        class Fist(OptionsList):
            def __init__(self, parent):
                super(DGPlagueMarinesV2.Champion.Fist, self).__init__(parent, '')
                self.variant(*melee.PowerFist)

        def __init__(self, parent):
            super(DGPlagueMarinesV2.Champion, self).__init__(parent, points=get_cost(units.PlagueMarines),
                                                         gear=[Gear('Blight grenades'),
                                                               Gear('Krak grenades')])
            self.wep1 = self.Ranged(self)
            self.wep2 = self.Melee(self)
            self.Fist(self)

    class PlagueMarine(ListSubUnit):
        type_name = u'Plague Marine'

        class Gun(OneOf):
            def __init__(self, parent):
                super(DGPlagueMarinesV2.PlagueMarine.Gun, self).__init__(parent, 'Gun')
                self.bgun = self.variant(*ranged.Boltgun)
                self.spec = [self.variant(*ranged.PlagueSpewer),
                             self.variant(*ranged.PlagueBelcher),
                             self.variant(*ranged.BlightLauncher),
                             self.variant(*ranged.Meltagun),
                             self.variant(*ranged.PlasmaGun)]
                self.variant(*melee.BuboticAxe)
                self.pknife = self.variant(*melee.PlagueKnife)
                self.pair = self.variant(join_and(melee.MaceOfContagion, melee.BuboticAxe),
                                         get_costs(melee.MaceOfContagion, melee.BuboticAxe),
                                         gear=create_gears(melee.MaceOfContagion, melee.BuboticAxe))
                self.melee = [self.variant(*melee.GreatPlagueCleaver),
                              self.variant(*melee.FlailOfCorruption)]
                
        class Icon(OptionsList):
            def __init__(self, parent):
                super(DGPlagueMarinesV2.PlagueMarine.Icon, self).__init__(parent, 'Icon of Chaos')
                self.variant('Icon of Despair', 10)

        def __init__(self, parent):
            gear = [ranged.BlightGrenades, ranged.KrakGrenades, melee.PlagueKnife]
            cost = points_price(get_cost(units.PlagueMarines), *gear)
            super(DGPlagueMarinesV2.PlagueMarine, self).__init__(parent, points=cost,
                                                               gear=create_gears(*gear))
            self.gun = self.Gun(self)
            self.icon = self.Icon(self)

        def check_rules(self):
            super(DGPlagueMarinesV2.PlagueMarine, self).check_rules()
            self.icon.visible = self.icon.used = self.gun.cur in [self.gun.bgun, self.gun.pknife]

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.gun.cur in self.gun.spec

        @ListSubUnit.count_gear
        def has_pair(self):
            return self.gun.cur == self.gun.pair

        @ListSubUnit.count_gear
        def has_melee(self):
            return self.gun.cur in self.gun.melee

        @ListSubUnit.count_gear
        def has_icon(self):
            return self.icon.used and self.icon.any

    def __init__(self, parent):
        super(DGPlagueMarinesV2, self).__init__(parent, get_name(units.PlagueMarines))
        self.ldr = SubUnit(self, self.Champion(parent=self))
        self.models = UnitList(self, self.PlagueMarine, 4, 19)

    def check_rules(self):
        super(DGPlagueMarinesV2, self).check_rules()
        icnt = sum(u.has_icon() for u in self.models.units)
        if icnt > 1:
            self.error('Only one Plague Marine may carry Icon of Despair')
        pcnt = sum(u.has_spec() for u in self.models.units)
        if pcnt > 2:
             self.error('No more then 2 Plague Marines may take special weapons; taken: {}'.format(pcnt))
        paircnt = sum(u.has_pair() for u in self.models.units)
        if paircnt > 2:
            self.error('No more then 2 Plague Marines may take mace of contagion and bubotic axe; taken: {}'.format(paircnt))
        mcnt = sum(u.has_melee() for u in self.models.units)
        if mcnt > 2:
            self.error('No more then 2 Plague Marines may take great plague cleaver or a flail of corruption; taken: {}'.format(mcnt))

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power + (16 if self.get_count() > 15 else
                             (11 if self.get_count() > 10 else
                              (6 if self.get_count() > 7 else
                               (3 if self.get_count() > 5 else 0))))


class Poxwalkers(TroopsUnit, armory.GuardUnit):
    type_name = get_name(units.Poxwalkers)
    type_id = "poxwalkers_v1"

    keywords = ['Infantry']
    power = 3

    member = UnitDescription('Poxwalker', options=[Gear('Improvised weapon')])

    def __init__(self, parent):
        super(Poxwalkers, self).__init__(parent)
        self.models = Count(self, 'Poxwalkers', 10, 20,
                            get_cost(units.Poxwalkers), per_model=True, gear=Poxwalkers.member.clone())

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * (1 + (self.models.cur > 10))
