from builder.games.wh40k8ed.space_marines.elites import CodexTerminatorAssaultSquad
from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, ListSubUnit,\
    SubUnit, UnitList
import armory, melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *


class BATerminatorAssaultSquad(CodexTerminatorAssaultSquad, armory.BAUnit):
    type_name = 'Blood Angels ' + get_name(units.TerminatorAssaultSquad)
    type_id = 'ba_terminator_assault_v1'
    power = 11


class BACompanyChampionIndex(ElitesUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + get_name(units.JumpCompanyChampion) + ' (Index)'
    type_id = 'ba_company_champion_v0'

    keywords = ['Character', 'Infantry']
    power = 4

    def __init__(self, parent):
        gear = [ranged.BoltPistol, melee.MCPowerSword, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.JumpCompanyChampion), *gear)
        super(BACompanyChampionIndex, self).__init__(parent, get_name(units.JumpCompanyChampion),
                                                     points=cost, static=True,
                                                gear=create_gears(*gear))


class BACompanyChampion(ElitesUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + get_name(units.CompanyChampion)
    type_id = 'ba_company_champion_v1'

    keywords = ['Character', 'Infantry']
    power = 3

    def __init__(self, parent):
        gear = [ranged.BoltPistol, melee.MCPowerSword, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.CompanyChampion), *gear)
        super(BACompanyChampion, self).__init__(parent, get_name(units.CompanyChampion),
                                                points=cost, static=True,
                                                gear=create_gears(*gear))


class BACompanyAncientIndex(ElitesUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + get_name(units.JumpCompanyAncient) + ' (Index)'
    type_id = 'ba_company_ancient_v0'

    keywords = ['Character', 'Infantry', 'Ancient']
    power = 5

    def __init__(self, parent):
        super(BACompanyAncientIndex, self).__init__(parent, get_name(units.JumpCompanyAncient),
                                                    points=get_cost(units.JumpCompanyAncient),
                                                    gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        BACompanyAncient.Weapon1(self)


class BACompanyAncient(ElitesUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + get_name(units.CompanyAncient)
    type_id = 'ba_company_ancient_v1'

    keywords = ['Character', 'Infantry', 'Ancient']
    power = 4

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(BACompanyAncient.Weapon1, self).__init__(parent, 'Weapon')
            armory.add_pistol_options(self)
            self.variant('Boltgun')
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    def __init__(self, parent):
        super(BACompanyAncient, self).__init__(parent, get_name(units.CompanyAncient),
                                               points=get_cost(units.CompanyAncient),
                                               gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        self.Weapon1(self)


class BACompanyVeterans(ElitesUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + armory.get_name(units.CompanyVeterans)
    type_id = 'ba_company_veterans_v1'
    keywords = ['Infantry']

    power = 3
    model_cost = get_cost(units.CompanyVeterans)

    class Sergeant(armory.ClawUser):
        type_name = u'Veteran Sergeant'

        def __init__(self, parent):
            super(BACompanyVeterans.Sergeant, self).__init__(parent, points=parent.model_cost,
                                                             gear=[Gear('Frag grenades'), Gear('Krak grenades')])

            class Sword(armory.SergeantWeapon):
                def add_double(self, sword):
                    sword = [self.variant(*melee.Chainsword)]
                    super(Sword, self).add_double(sword)
                    self.double += sword
            self.wep1 = armory.SergeantWeapon(self, double=True)
            self.wep2 = Sword(self, double=True, sword=False)

        def check_rules(self):
            super(BACompanyVeterans.Sergeant, self).check_rules()
            armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

    class Veteran(armory.ClawUser, ListSubUnit):
        type_name = u'Space Marine Veteran'

        class Pistols(OneOf):
            def __init__(self, parent):
                super(BACompanyVeterans.Veteran.Pistols, self).__init__(parent, 'Pistols')
                armory.add_pistol_options(self)
                self.variant(*wargear.StormShield)
                armory.add_melee_weapons(self)

        class Melee(OneOf):
            def __init__(self, parent):
                super(BACompanyVeterans.Veteran.Melee, self).__init__(parent, 'Melee weapon')
                armory.add_melee_weapons(self)
                self.variant(*wargear.StormShield)
                self.variant(*ranged.Boltgun)
                armory.add_pistol_options(self)
                armory.add_combi_weapons(self)
                armory.add_special_weapons(self)

        def __init__(self, parent):
            super(BACompanyVeterans.Veteran, self).__init__(parent,
                                                            gear=[Gear('Frag grenades'), Gear('Krak grenades')])
            self.base_points = self.root_unit.model_cost
            self.wep1 = self.Pistols(self)
            self.wep2 = self.Melee(self)

    def get_leader(self):
        return self.Sergeant

    def get_member(self):
        return self.Veteran

    def __init__(self, parent):
        super(BACompanyVeterans, self).__init__(parent)
        self.ldr = SubUnit(self, self.get_leader()(parent=self))
        self.models = UnitList(self, self.get_member(), 1, 4)

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power + 5 * (self.models.count > 1)


class BAJumpCompanyVeterans(BACompanyVeterans):
    type_name = u'Blood Angels Company Veterans with Jump Packs (Index)'
    type_id = 'ba_company_veterans_v0'

    armory = armory
    model_cost = get_cost(units.JumpCompanyVeterans)

    def __init__(self, parent):
        super(BAJumpCompanyVeterans, self).__init__(parent)
        self._name = u'Blood Angels Company Veterans with Jump Packs'


class SanguinaryNovitiateIndex(ElitesUnit, armory.BAUnit):
    type_name = get_name(units.JumpSanguinaryNovitiate) + ' (Index)'
    type_id = 'ba_apothecary_v0'

    keywords = ['Character', 'Infantry']
    power = 4

    def __init__(self, parent):
        super(SanguinaryNovitiate, self).__init__(parent, get_name(units.JumpSanguinaryNovitiate),
                                                  points=get_cost(units.JumpSanguinaryNovitiate), gear=[
                                                      Gear("Bolt pistol"), Gear("Chainsword"),
                                                      Gear('Frag grenades'), Gear('Krak grenades')
        ], static=True)


class SanguinaryNovitiate(ElitesUnit, armory.BAUnit):
    type_name = get_name(units.SanguinaryNovitiate)
    type_id = 'ba_apothecary_v1'

    keywords = ['Character', 'Infantry']
    power = 3

    def __init__(self, parent):
        super(SanguinaryNovitiate, self).__init__(parent, points=get_cost(units.SanguinaryNovitiate), gear=[
            Gear("Bolt pistol"), Gear("Chainsword"), Gear('Frag grenades'), Gear('Krak grenades')
        ], static=True)


class BASternguardVeteranSquad(ElitesUnit, armory.BAUnit):
    type_name = u'Blood Angels Sternguard Veteran Squad'
    type_id = 'ba_sternguard_veteran_squad_v1'

    power = 7
    keywords = ['Infantry']

    class Veteran(ListSubUnit):
        type_name = u'Space Marine Veteran'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(BASternguardVeteranSquad.Veteran.Weapon, self).__init__(parent, 'Weapon')
                self.bolt = self.variant(*ranged.SpecialIssueBoltgun)
                armory.add_combi_weapons(self)
                armory.add_special_weapons(self)
                armory.add_heavy_weapons(self)

        def __init__(self, parent):
            gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol]
            super(BASternguardVeteranSquad.Veteran, self).__init__(
                parent=parent, gear=armory.create_gears(*gear),
                points=armory.get_cost(units.SternguardVeteranSquad)
            )
            self.weapon = self.Weapon(self)

        @ListSubUnit.count_gear
        def count_weapon(self):
            return self.weapon.cur not in ([self.weapon.bolt] + self.weapon.combi)

    class Sergeant(armory.ClawUser):
        type_name = 'Veteran Sergeant'

        def __init__(self, parent):
            super(BASternguardVeteranSquad.Sergeant, self).__init__(
                parent=parent, points=16, gear=[Gear('Frag grenades'), Gear('Krak grenades')]
            )

            class SpecWeapon(armory.SergeantWeapon):
                def add_single(self):
                    self.variant(*ranged.SpecialIssueBoltgun)
                    super(SpecWeapon, self).add_single()
            self.wep1 = SpecWeapon(self, stern=True)
            self.wep2 = armory.SergeantWeapon(self, double=True, stern=True)

        def check_rules(self):
            armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

    def get_leader(self):
        return self.Sergeant

    def __init__(self, parent):
        super(BASternguardVeteranSquad, self).__init__(parent,
                                                       armory.get_name(units.SternguardVeteranSquad))
        self.leader = SubUnit(self, self.get_leader()(self))
        self.vets = UnitList(self, self.Veteran, 4, 9)

    def check_rules(self):
        spec_total = sum(u.count_weapon() for u in self.vets.units)
        if 2 < spec_total:
            self.error('Veterans can take only 2 special or heavy weapon (taken: {0})'.format(spec_total))

    def get_count(self):
        return self.vets.count + 1

    def build_power(self):
        return self.power * (1 + (self.vets.count > 4))


class BAVanguardVeteranSquad(ElitesUnit, armory.BAUnit):
    type_name = u'Blood Angels Vanguard Veteran Squad'
    type_id = 'ba_vanguard_veteran_squad_v1'
    keywords = ['Infantry']
    power = 7

    class Options(OptionsList):
        def __init__(self, parent):
            super(BAVanguardVeteranSquad.Options, self).__init__(parent=parent, name='Options', limit=None)

            self.meltabombs = self.variant

    class VeteranWeapon(OneOf):
        def __init__(self, parent, name='Weapon', has_melee=False, relic=False):
            super(BAVanguardVeteranSquad.VeteranWeapon, self).__init__(parent, name)
            if has_melee:
                armory.add_melee_weapons(self)
            armory.add_pistol_options(self)
            if not has_melee:
                armory.add_melee_weapons(self)
            if relic:
                self.variant(*melee.RelicBlade)
            self.shield = self.variant(*wargear.StormShield)

    class Veteran(armory.ClawUser, ListSubUnit):
        type_name = u'Space Marine Veteran'

        def __init__(self, parent):
            super(BAVanguardVeteranSquad.Veteran, self).__init__(
                parent=parent, points=armory.get_cost(units.VanguardVeteranSquad),
                gear=armory.create_gears(ranged.FragGrenades, ranged.KrakGrenades)
            )
            self.wep1 = BAVanguardVeteranSquad.VeteranWeapon(self, has_melee=True)
            self.wep2 = BAVanguardVeteranSquad.VeteranWeapon(self, '', has_melee=False)
            self.opt = BAVanguardVeteranSquad.Options(self)

        def check_rules(self):
            super(BAVanguardVeteranSquad.Veteran, self).check_rules()
            self.wep1.shield.active = not self.wep2.cur == self.wep2.shield
            self.wep2.shield.active = not self.wep1.cur == self.wep1.shield

        @ListSubUnit.count_gear
        def has_bombs(self):
            return self.opt.any

    class Sergeant(armory.ClawUser):
        type_name = u'Vanguard veteran Sergeant'

        def __init__(self, parent):
            super(BAVanguardVeteranSquad.Sergeant, self).__init__(
                parent=parent, gear=[Gear('Frag grenades'), Gear('Krak grenades')],
                points=armory.get_cost(units.VanguardVeteranSquad)
            )
            self.wep1 = BAVanguardVeteranSquad.VeteranWeapon(self, has_melee=True, relic=True)
            self.wep2 = BAVanguardVeteranSquad.VeteranWeapon(self, '', has_melee=False, relic=True)
            self.opt = BAVanguardVeteranSquad.Options(self)

        def check_rules(self):
            super(BAVanguardVeteranSquad.Sergeant, self).check_rules()
            self.wep1.shield.active = not self.wep2.cur == self.wep2.shield
            self.wep2.shield.active = not self.wep1.cur == self.wep1.shield

    class Jump(OptionsList):
        def __init__(self, parent):
            super(BAVanguardVeteranSquad.Jump, self).__init__(parent=parent, name='Options', limit=None)
            self.jump = self.variant('Jump packs', armory.get_cost(units.JumpVanguardVeteranSquad) -
                                     armory.get_cost(units.VanguardVeteranSquad), per_model=True)

    def __init__(self, parent):
        super(BAVanguardVeteranSquad, self).__init__(parent, armory.get_name(units.VanguardVeteranSquad))
        self.leader = SubUnit(self, self.Sergeant(self))
        self.vets = UnitList(self, self.Veteran, 4, 9)
        self.jump = self.Jump(self)

    def get_count(self):
        return self.vets.count + 1

    def check_rules(self):
        super(BAVanguardVeteranSquad, self).check_rules()
        mcnt = self.leader.unit.opt.any + sum(u.has_bombs() for u in self.vets.units)
        if mcnt > 1:
            self.error('Only one model may take melta bombs')

    def build_power(self):
        return (self.power + (self.jump.any)) * (1 + self.vets.count > 4)

    def build_points(self):
        return super(BAVanguardVeteranSquad, self).build_points() + self.jump.points * self.vets.count


class SanguinaryAncient(ElitesUnit, armory.BAUnit):
    type_name = get_name(units.SanguinaryAncient)
    type_id = 'sang_ancient_v1'
    kwname = 'Sanguinary Guard'
    keywords = ['Character', 'Infantry', 'Ancient', 'Jump pack', 'Fly']
    power = 6

    class Ranged(OneOf):
        def __init__(self, parent):
            super(SanguinaryAncient.Ranged, self).__init__(parent, 'Ranged weapon')
            self.variant(*ranged.AngelusBoltgun)
            self.variant(*ranged.InfernoPistol)
            self.variant(*ranged.PlasmaCutter)

    class Melee(OneOf):
        def __init__(self, parent):
            super(SanguinaryAncient.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.EncarmineSword)
            self.variant(*melee.EncarmineAxe)
            self.variant(*melee.PowerFist)

    class Options(OptionsList):
        def __init__(self, parent):
            super(SanguinaryAncient.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.DeathMask)

    def __init__(self, parent):
        super(SanguinaryAncient, self).__init__(parent, gear=[
            Gear('Frag grenades'), Gear('Krak grenades')
        ], points=get_cost(units.SanguinaryAncient))
        self.Ranged(self)
        self.Melee(self)
        self.Options(self)


class TerminatorAncient(ElitesUnit, armory.BAUnit):
    type_name = get_name(units.TerminatorAncient)
    type_id = 'ba_terminator_ancient_v1'
    kwname = 'Terminator'
    keywords = ['Character', 'Infantry', 'Ancient']
    power = 6

    class Weapon(OneOf):
        def __init__(self, parent):
            super(TerminatorAncient.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*melee.LightningClaw)
            self.variant(*ranged.StormBolter)
            self.variant(*melee.ThunderHammerCharacter)

    def __init__(self, parent):
        super(TerminatorAncient, self).__init__(parent, points=get_cost(units.TerminatorAncient))
        self.Weapon(self)


class DeathCompany(ElitesUnit, armory.BAUnit):
    type_name = get_name(units.DeathCompany)
    type_id = 'death_company_v1'
    faction = ['Death Company']
    keywords = ['Infantry']
    power = 8

    class JumpPack(OptionsList):
        def __init__(self, parent):
            super(DeathCompany.JumpPack, self).__init__(parent=parent, name='Options')
            self.variant('Jump packs', get_cost(units.JumpDeathCompany) - get_cost(units.DeathCompany),
                         per_model=True)

    class Pistol(OneOf):
        def __init__(self, parent):
            super(DeathCompany.Pistol, self).__init__(parent=parent, name='Weapon')
            self.variant(*ranged.BoltPistol)
            self.variant(*ranged.Boltgun)
            self.variant(*ranged.HandFlamer)
            self.variant(*ranged.InfernoPistol)
            self.variant(*ranged.PlasmaPistol)
            self.variant(*melee.PowerAxe)
            self.variant(*melee.PowerFist)
            self.variant(*melee.PowerMaul)
            self.variant(*melee.PowerSword)
            self.ham = self.variant(*melee.ThunderHammer)

    class Sword(OneOf):
        def __init__(self, parent):
            super(DeathCompany.Sword, self).__init__(parent, '')
            self.variant(*melee.Chainsword)
            self.variant(*melee.PowerAxe)
            self.variant(*melee.PowerFist)
            self.variant(*melee.PowerMaul)
            self.variant(*melee.PowerSword)

    class Marine(ListSubUnit):
        type_name = u'Death Company'

        def __init__(self, parent):
            super(DeathCompany.Marine, self).__init__(parent=parent, points=get_cost(units.DeathCompany),
                                                      gear=[Gear('Frag grenades'),
                                                            Gear('Krak grenades')])
            self.wep1 = DeathCompany.Pistol(self)
            self.wep2 = DeathCompany.Sword(self)

        def check_rules(self):
            super(DeathCompany.Marine, self).check_rules()
            self.wep2.used = self.wep2.visible = not self.wep1.cur == self.wep1.ham

    def __init__(self, parent):
        super(DeathCompany, self).__init__(parent)
        self.marines = UnitList(self, self.Marine, 5, 15)
        self.opt = self.JumpPack(self)

    def build_power(self):
        return (self.power + self.opt.any) * ((self.marines.count + 4) / 5)

    def get_count(self):
        return self.marines.count

    def build_points(self):
        return super(DeathCompany, self).build_points() + \
            self.opt.points * (self.get_count() - 1)


class BADreadnought(ElitesUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + armory.get_name(units.Dreadnought)
    type_id = 'ba_dreadnought_v1'
    keywords = ['Vehicle']
    power = 7

    model_points = armory.get_cost(units.Dreadnought)

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(BADreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.builtinstormbolter = self.variant(*ranged.StormBolter)
            self.builtinheavyflamer = self.variant(*ranged.HeavyFlamer)
            self.variant(*ranged.Meltagun)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(BADreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.assaultcannon = self.variant(*ranged.AssaultCannon)
            armory.add_dred_heavy_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(BADreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.powerfist = self.variant(*melee.DreadnoughtCombatWeapon)
            self.missilelauncher = self.variant(*ranged.MissileLauncher)

    def __init__(self, parent):
        super(BADreadnought, self).__init__(parent=parent, name=armory.get_name(units.Dreadnought),
                                               points=self.model_points)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.build_in = self.BuildIn(self)

    def check_rules(self):
        super(BADreadnought, self).check_rules()
        self.build_in.visible = self.build_in.used = self.wep2.cur == self.wep2.powerfist


class DeathCompanyDreadnought(ElitesUnit, armory.BAUnit):
    type_name = get_name(units.DeathCompanyDreadnought)
    type_id = 'deathcompanydreadnought_v1'
    keywords = ['Vehicle', 'Dreadnought']
    faction = ['Death Company']
    power = 9

    def __init__(self, parent):
        super(DeathCompanyDreadnought, self).__init__(parent, points=get_cost(units.DeathCompanyDreadnought))
        self.Ccw(self)
        self.BuildIn1(self)
        self.BuildIn2(self)
        self.Options(self)

    class BuildIn1(OneOf):
        def __init__(self, parent):
            super(DeathCompanyDreadnought.BuildIn1, self).__init__(parent=parent, name='')

            self.builtinstormbolter = self.variant('Built-in Storm bolter', get_cost(ranged.StormBolter),
                                                   gear=[Gear('Storm bolter')])
            self.builtinheavyflamer = self.variant('Built-in Heavy flamer', get_cost(ranged.HeavyFlamer),
                                                   gear=[Gear('Heavy flamer')])

    class BuildIn2(OneOf):
        def __init__(self, parent):
            super(DeathCompanyDreadnought.BuildIn2, self).__init__(parent=parent, name='')

            self.builtinstormbolter = self.variant('Built-in meltagun', get_cost(ranged.Meltagun),
                                                   gear=[Gear('Meltagun')])
            self.builtinheavyflamer = self.variant('Built-in Heavy flamer', get_cost(ranged.HeavyFlamer),
                                                   gear=[Gear('Heavy flamer')])

    class Ccw(OneOf):
        def __init__(self, parent):
            super(DeathCompanyDreadnought.Ccw, self).__init__(parent=parent, name='Weapon')
            self.variant('Furioso fists', get_cost(melee.FuriosoFistPair), gear=[Gear('Furioso fist', count=2)])
            self.variant('Blood talons', get_cost(melee.BloodTalons))

    class Options(OneOf):
        def __init__(self, parent):
            super(DeathCompanyDreadnought.Options, self).__init__(parent=parent, name='Options')
            self.smoke = self.variant(*wargear.SmokeLaunchers)
            self.magnagrapple = self.variant(*wargear.MagnaGrapple)


class SanguinaryGuard(ElitesUnit, armory.BAUnit):
    type_name = get_name(units.SanguinaryGuard)
    type_id = 'sang_guard_v1'
    keywords = ['Character', 'Infantry', 'Jump pack', 'Fly']
    power = 8

    class Options(OptionsList):
        def __init__(self, parent):
            super(SanguinaryAncient.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.DeathMask)

        @property
        def points(self):
            return super(SanguinaryGuard.Options, self).points() * self.parent.get_count()

    class SingleGuard(ListSubUnit):
        type_name = u'Sanguinary Guard'

        def __init__(self, parent):
            super(SanguinaryGuard.SingleGuard, self).__init__(parent, gear=[
                Gear('Frag grenades'), Gear('Krak grenades')
            ], points=get_cost(units.SanguinaryGuard))
            SanguinaryAncient.Ranged(self)
            SanguinaryAncient.Melee(self)
            SanguinaryAncient.Options(self)

    def __init__(self, parent):
        super(SanguinaryGuard, self).__init__(parent)
        self.models = UnitList(self, self.SingleGuard, 4, 10)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power + 12 * (self.models.count > 4)


class FuriosoDreadnought(ElitesUnit, armory.BAUnit):
    type_name = get_name(units.FuriosoDreadnought)
    type_id = 'furioso_v1'
    keywords = ['Vehicle', 'Dreadnought']
    power = 9

    class Weapon(OneOf):
        def __init__(self, parent):
            super(FuriosoDreadnought.Weapon, self).__init__(parent=parent, name='')
            self.fist = self.variant(*melee.FuriosoFist)
            self.cannon = self.variant(*ranged.FragCannon)

        def check_rules(self):
            super(FuriosoDreadnought.Weapon, self).check_rules()
            self.visible = self.used = not self.parent.claws.any

        @property
        def is_dccw(self):
            return self.cur == self.fist

    class BuildIn1(OneOf):
        def __init__(self, parent, mount):
            super(FuriosoDreadnought.BuildIn1, self).__init__(parent, name='')
            self.variant('Built-in Storm bolter', get_cost(ranged.StormBolter), gear=[Gear('Storm bolter')])
            self.variant('Built-in Heavy flamer', get_cost(ranged.HeavyFlamer), gear=[Gear('Heavy flamer')])
            self.mount = mount

        def check_rules(self):
            super(FuriosoDreadnought.BuildIn1, self).check_rules()
            self.visible = self.used = self.parent.claws.any or self.mount.is_dccw

    class BuildIn2(OneOf):
        def __init__(self, parent, mount):
            super(FuriosoDreadnought.BuildIn2, self).__init__(parent, name='')
            self.variant('Built-in meltagun', get_cost(ranged.Meltagun), gear=[Gear('Meltagun')])
            self.variant('Built-in Heavy flamer', get_cost(ranged.HeavyFlamer), gear=[Gear('Heavy flamer')])
            self.mount = mount

        def check_rules(self):
            super(FuriosoDreadnought.BuildIn2, self).check_rules()
            self.visible = self.used = self.parent.claws.any or self.mount.is_dccw

    class Claws(OptionsList):
        def __init__(self, parent):
            super(FuriosoDreadnought.Claws, self).__init__(parent=parent, name='Weapon')
            self.variant(*melee.BloodTalons)

    def __init__(self, parent):
        super(FuriosoDreadnought, self).__init__(parent=parent, points=get_cost(units.FuriosoDreadnought))
        self.claws = self.Claws(self)
        self.wep1 = self.Weapon(self)
        self.build_in1 = self.BuildIn1(self, self.wep1)
        self.wep2 = self.Weapon(self)
        self.build_in2 = self.BuildIn2(self, self.wep2)
        self.opt = DeathCompanyDreadnought.Options(self)

    def check_rules(self):
        super(FuriosoDreadnought, self).check_rules()
        if not self.claws.any and (not self.wep1.is_dccw and not self.wep2.is_dccw):
            self.error('Two frag cannons may not be taken')

    def build_points(self):
        return super(FuriosoDreadnought, self).build_points() - (
            (get_costs(melee.FuriosoFist, melee.FuriosoFist) -
             get_cost(melee.FuriosoFistPair)) if
            ((not self.claws.any) and self.wep1.is_dccw
             and self.wep2.is_dccw) else 0)


class BARedemptorDreadnought(ElitesUnit, armory.BAUnit):
    type_name = 'Blood Angels ' + get_name(units.RedemptorDreadnought)
    type_id = 'redemptor_v1'
    keywords = ['Vehicle', 'Dreadnought']
    power = 10

    @classmethod
    def calc_faction(cls):
        return super(BARedemptorDreadnought, cls).calc_faction() + ['DEATHWATCH']

    gears = [melee.RedemptorFist]

    class Options(OptionsList):
        def __init__(self, parent):
            super(BARedemptorDreadnought.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.IcarusRocketPod)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(BARedemptorDreadnought.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.HeavyFlamer)
            self.variant(*ranged.OnslaughtGatlingCannon)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(BARedemptorDreadnought.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.HeavyOnslaughtGatlingCannon)
            self.variant(*ranged.MacroPlasmaIncinerator)

    class Weapon3(OneOf):
        def __init__(self, parent):
            super(BARedemptorDreadnought.Weapon3, self).__init__(parent, 'Secondary weapon')
            self.variant('Two fragstorm grenade launchers', 2 * armory.get_cost(ranged.FragstormGrenadeLauncher),
                         gear=armory.create_gears(*[ranged.FragstormGrenadeLauncher] * 2))
            self.variant('Two storm bolters', 2 * armory.get_cost(ranged.StormBolter),
                         gear=armory.create_gears(*[ranged.StormBolter] * 2))

    def __init__(self, parent):
        cost = armory.points_price(armory.get_cost(units.RedemptorDreadnought), *self.gears)
        gear = armory.create_gears(*self.gears)
        super(BARedemptorDreadnought, self).__init__(parent, points=cost, gear=gear,
                                                     name=get_name(units.RedemptorDreadnought))
        self.Weapon1(self)
        self.Weapon2(self)
        self.Weapon3(self)
        self.Options(self)
