from builder.games.wh40k8ed.space_marines.armory import AstartesUnit, SMUnit
from builder.games.wh40k8ed.unit import Unit
from builder.games.wh40k8ed.options import OneOf, Gear, OptionsList
import melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class SergeantWeapon(OneOf):
    def add_single(self):
        self.single = [
            self.variant(*ranged.Boltgun),
            self.variant(*ranged.CombiFlamer),
            self.variant(*ranged.CombiGrav),
            self.variant(*ranged.CombiMelta),
            self.variant(*ranged.CombiPlasma),
            self.variant(*ranged.StormBolter)
        ]

    def add_double(self, sword=True):
        self.double = [
            self.variant(*ranged.BoltPistol),
            self.variant(*ranged.GravPistol),
            self.variant(*ranged.InfernoPistol),
            self.variant(*ranged.HandFlamer),
            self.variant(*ranged.PlasmaPistol)] +\
            ([self.variant(*melee.Chainsword)] if sword else [])
        self.claw = self.variant(*melee.LightningClaw)
        self.double += [self.claw,
                        self.variant(*melee.PowerAxe),
                        self.variant(*melee.PowerFist),
                        self.variant(*melee.PowerMaul),
                        self.variant(*melee.PowerSword)]

    def __init__(self, parent, sword=True, double=False, stern=False):
        super(SergeantWeapon, self).__init__(parent, 'Sergeant Equipment')
        if double:
            self.add_double(sword)
            if not stern:
                self.double.append(self.variant(*melee.ThunderHammer))
        self.add_single()
        if not double:
            self.add_double(sword)
            if not stern:
                self.double.append(self.variant(*melee.ThunderHammer))

    @staticmethod
    def ensure_pairs(first, second):
        for opt in second.single:
            opt.active = first.cur not in first.single
        for opt in first.single:
            opt.active = second.cur not in second.single


def add_special_weapons(obj):
    obj.spec = [
        obj.variant(*ranged.Flamer),
        obj.variant(*ranged.GravGun),
        obj.variant(*ranged.PlasmaGun),
        obj.variant(*ranged.Meltagun)]


def add_melee_weapons(obj, axe=True):
    obj.variant(*melee.Chainsword)
    obj.claw = obj.variant(*melee.LightningClaw)
    if axe:
        obj.variant(*melee.PowerAxe)
    obj.variant(*melee.PowerFist)
    obj.variant(*melee.PowerMaul)
    obj.variant(*melee.PowerSword)
    if 'CHARACTER' in obj.parent.keywords:
        obj.variant(*melee.ThunderHammerCharacter)
    else:
        obj.variant(*melee.ThunderHammer)


class ClawUser(Unit):
    def build_points(self):
        res = super(ClawUser, self).build_points()
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -= 2 * get_cost(melee.LightningClaw) - 12
        return res


def add_pistol_options(obj):
    obj.variant(*ranged.BoltPistol)
    obj.variant(*ranged.GravPistol)
    obj.variant(*ranged.HandFlamer)
    obj.variant(*ranged.InfernoPistol)
    obj.variant(*ranged.PlasmaPistol)


def add_term_melee_weapons(obj):
    obj.claw = obj.variant(*melee.LightningClaw)
    obj.fist = obj.variant(*melee.PowerFist)
    if 'CHARACTER' in obj.parent.keywords:
        obj.variant(*wargear.StormShieldCharacter)
        obj.variant(*melee.ThunderHammerCharacter)
    else:
        obj.variant(*wargear.StormShield)
        obj.variant(*melee.ThunderHammer)


def add_term_combi_weapons(obj):
    obj.variant(*ranged.StormBolter)
    obj.variant(*ranged.CombiPlasma)
    obj.variant(*ranged.CombiFlamer)
    obj.variant(*ranged.CombiMelta)


def add_combi_weapons(obj):
    obj.combi = [
        obj.variant(*ranged.CombiFlamer),
        obj.variant(*ranged.CombiGrav),
        obj.variant(*ranged.CombiMelta),
        obj.variant(*ranged.CombiPlasma),
        obj.variant(*ranged.StormBolter)]


def add_term_heavy_weapons(obj):
    obj.heavy = [
        obj.variant(*ranged.HeavyFlamer),
        obj.variant(*ranged.AssaultCannon),
        obj.variant('Cyclone missile launcher and storm bolter',
                    get_costs(ranged.CycloneMissileLauncher, ranged.StormBolter),
                    gear=create_gears(ranged.CycloneMissileLauncher, ranged.StormBolter))]


def add_heavy_weapons(obj):
    obj.heavy = [
        obj.variant(*ranged.GravCannonAmp),
        obj.variant(*ranged.HeavyBolter),
        obj.variant(*ranged.HeavyFlamer),
        obj.variant(*ranged.Lascannon),
        obj.variant(*ranged.MissileLauncher),
        obj.variant(*ranged.MultiMelta),
        obj.variant(*ranged.PlasmaCannon)]


def add_dred_heavy_weapons(obj):
    obj.variant(*ranged.AssaultCannon)
    obj.variant(*ranged.MultiMelta)
    obj.variant(*ranged.TwinLascannon)


class BAUnit(SMUnit):
    faction_substitution = {'Blood Angels': '<Chapter>'}
    wiki_faction = 'Blood Angels'

    @classmethod
    def calc_faction(cls):
        return ['<BLOOD ANGELS SUCCESSORS>', 'FLESH TEARERS']
