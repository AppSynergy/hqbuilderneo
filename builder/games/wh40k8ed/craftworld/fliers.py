from builder.games.wh40k8ed.unit import FlierUnit
from builder.games.wh40k8ed.options import OneOf, Gear
from builder.games.wh40k8ed.utils import get_name, get_cost, points_price
import armory
import units
import ranged
import wargear

__author__ = 'Ivan Truskov'


class CrimsonHunter(FlierUnit, armory.CraftworldUnit):
    type_name = get_name(units.CrimsonHunter)
    type_id = 'hunter_v1'
    faction = ['Aspect Warrior']
    keywords = ['Vehicle', 'Fly']

    power = 8

    def __init__(self, parent):
        points = points_price(get_cost(units.CrimsonHunter),
                              *[ranged.BrightLance, ranged.BrightLance, ranged.PulseLaser])
        super(CrimsonHunter, self).__init__(parent, static=True, points=points,
                                            gear=[Gear('Bright lance', count=2),
                                                  Gear('Pulse laser')])


class HunterExarch(FlierUnit, armory.CraftworldUnit):
    type_name = get_name(units.CrimsonHunterExarch)
    type_id = 'hunter_ex_v1'
    faction = ['Aspect Warrior']
    keywords = ['Vehicle', 'Fly']

    power = 9

    class Weapon(OneOf):
        def __init__(self, parent):
            super(HunterExarch.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Two bright lances', gear=[Gear('Bright lance', count=2)],
                         points=get_cost(ranged.BrightLance) * 2)
            self.variant('Two starcannons', gear=[Gear('Starcannon', count=2)], points=get_cost(ranged.Starcannon) * 2)

    def __init__(self, parent):
        points = points_price(get_cost(units.CrimsonHunterExarch),
                              *[ranged.PulseLaser])
        super(HunterExarch, self).__init__(parent, points=points,
                                           gear=[Gear('Pulse laser')])
        self.Weapon(self)


class Hemlock(FlierUnit, armory.CraftworldUnit):
    type_name = get_name(units.HemlockWraithfighter)
    type_id = 'hemlock_v1'
    faction = ['Spirit Host']
    keywords = ['Vehicle', 'Fly', 'Psyker']

    power = 10

    def __init__(self, parent):
        points = points_price(get_cost(units.HemlockWraithfighter),
                              *[ranged.HeavyDScythe, ranged.HeavyDScythe, wargear.SpiritStones])
        super(Hemlock, self).__init__(parent, static=True, points=points,
                                      gear=[Gear('Heavy D-scythe', count=2),
                                            Gear('Spirit stones')])
