__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FortUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, SubUnit,\
    OptionsList, ListSubUnit, UnitList
import ranged, melee, armory, units, biomorphs
from builder.games.wh40k8ed.utils import *
from transports import Tyrannocyte


class Sporocyst(FortUnit, armory.FleetUnit):
    type_name = get_name(units.Sporocyst)
    type_id = 'sporocyst_v1'
    keywords = ['Monster']
    power = 7

    def __init__(self, parent):
        super(Sporocyst, self).__init__(parent, points=get_cost(units.Sporocyst))
        Tyrannocyte.Weapon(self)
