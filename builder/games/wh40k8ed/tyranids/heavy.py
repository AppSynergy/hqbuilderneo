__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HeavyUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, SubUnit,\
    OptionsList, ListSubUnit, UnitList
import ranged, melee, armory, units, biomorphs
from builder.games.wh40k8ed.utils import *


class Exocrine(HeavyUnit, armory.FleetUnit):
    type_name = get_name(units.Exocrine)
    type_id = 'exocrine_v1'
    keywords = ['Monster']
    power = 11

    def __init__(self, parent):
        gear = [ranged.BioPlasmicCannon, melee.PowerfulLimbs]
        cost = points_price(get_cost(units.Exocrine), *gear)
        super(Exocrine, self).__init__(parent, points=cost, static=True,
                                       gear=create_gears(*gear))


class Tyrannofex(HeavyUnit, armory.FleetUnit):
    type_name = get_name(units.Tyrannofex)
    type_id = 'tyrannofex_v1'
    keywords = ['Monster']
    power = 11

    class Options(OptionsList):
        def __init__(self, parent):
            super(Tyrannofex.Options, self).__init__(parent, 'Options')
            self.variant(*biomorphs.ToxinSacs)
            self.variant(*biomorphs.AdrenalGlandsMonsters)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Tyrannofex.Weapon, self).__init__(parent, name='Weapon')
            self.variant(*ranged.AcidSpray)
            self.variant(*ranged.FleshborerHive)
            self.variant(*ranged.RuptureCannon)

    def __init__(self, parent):
        gear = [ranged.StingerSalvo, melee.PowerfulLimbs]
        cost = points_price(get_cost(units.Tyrannofex), *gear)
        super(Tyrannofex, self).__init__(parent, points=cost,
                                         gear=create_gears(*gear))
        self.Weapon(self)
        self.Options(self)


class Biovore(HeavyUnit, armory.FleetUnit):
    type_name = get_name(units.Biovores)
    type_id = 'biovore_v1'
    keywords = ['Infantry']

    model_name = 'Biovore'
    model_gear = [ranged.SporeMineLauncher]
    model_points = points_price(get_cost(units.Biovores), *model_gear)
    power = 2

    def __init__(self, parent):
        super(Biovore, self).__init__(parent)
        self.models = Count(
            self, self.model_name, 1, 3, self.model_points, per_model=True,
            gear=UnitDescription(self.model_name, options=create_gears(*self.model_gear))
        )

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * self.get_count()


class Toxicrene(HeavyUnit, armory.FleetUnit):
    type_name = get_name(units.Toxicrene)
    type_id = 'toxicrene_v1'
    keywords = ['Monster']
    power = 8

    def __init__(self, parent):
        gear = [ranged.ChokingSpores, melee.MassiveToxicLashes]
        cost = points_price(get_cost(units.Toxicrene), *gear)
        super(Toxicrene, self).__init__(parent, points=cost, static=True,
                                        gear=create_gears(*gear))


class CarnifexBrood(HeavyUnit, armory.FleetUnit):
    type_name = get_name(units.Carnifexes)
    type_id = 'carnifex_brood_v1'
    keywords = ['Monster']

    class Warrior(ListSubUnit):
        type_name = u'Carnifex'
        power = 6

        class Options(OptionsList):
            def __init__(self, parent):
                super(CarnifexBrood.Warrior.Options, self).__init__(parent, name='Options')
                self.variant(*biomorphs.ToxinSacsHTCWar)
                self.variant(*biomorphs.AdrenalGlandsMonsters)
                self.variant(*biomorphs.ChitinThorns)

        class Head(OptionsList):
            def __init__(self, parent):
                super(CarnifexBrood.Warrior.Head, self).__init__(parent, 'Head weapon', limit=1)
                self.variant(*ranged.BioPlasma)
                self.variant(*biomorphs.EnhancedSenses)
                self.variant(*melee.MonstrousAcidMaw)
                self.variant(*biomorphs.Tusks)

        class Tail(OptionsList):
            def __init__(self, parent):
                super(CarnifexBrood.Warrior.Tail, self).__init__(parent, name='Tail biomorphs', limit=1)
                self.variant(*melee.ThresherScythe)
                self.variant(*melee.BoneMace)

        class Back(OptionsList):
            def __init__(self, parent):
                super(CarnifexBrood.Warrior.Back, self).__init__(parent, 'Back', limit=1)
                self.variant(*ranged.SpineBanks)
                self.variant(*biomorphs.SporeCysts)

        class Claws(OneOf):
            def __init__(self, parent, name):
                super(CarnifexBrood.Warrior.Claws, self).__init__(parent, name=name)
                self.talons = self.variant(*melee.MonstrousScythingTalonsCarnifex)
                self.claws = self.variant(*melee.MonstrousCrushingClaws)
                armory.add_monster_cannon(self)

            def apply_restricted(self, flag):
                for v in self.restricted:
                    v.active = flag

        class Weapon(OneOf):
            def __init__(self, parent, name):
                super(CarnifexBrood.Warrior.Weapon, self).__init__(parent, name=name)
                self.talons = self.variant(*melee.MonstrousScythingTalonsCarnifex)
                armory.add_monster_cannon(self)

            def apply_restricted(self, flag):
                for v in self.restricted:
                    v.active = flag

        def __init__(self, parent):
            super(CarnifexBrood.Warrior, self).__init__(parent, name='Carnifex',
                                                        points=get_cost(units.Carnifexes))
            
            self.w1 = self.Claws(self, name='Weapon')
            self.w2 = self.Weapon(self, name='')
            self.Head(self)
            self.Back(self)
            self.Tail(self)
            self.Options(self)

        def check_rules(self):
            super(CarnifexBrood.Warrior, self).check_rules()
            self.w1.apply_restricted(self.w2.cur not in self.w2.restricted)
            self.w2.apply_restricted(self.w1.cur not in self.w1.restricted)

        def build_points(self):
            res = super(CarnifexBrood.Warrior, self).build_points()
            if self.w1.cur == self.w1.talons and self.w2.cur == self.w2.talons:
                res -= 2 * get_cost(melee.MonstrousScythingTalonsCarnifex) - get_cost(melee.MonstrousScythingTalonsCarnifex2)
            return res

    def __init__(self, parent):
        super(CarnifexBrood, self).__init__(parent=parent)
        self.models = UnitList(parent=self, unit_class=self.Warrior, min_limit=1, max_limit=3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.Warrior.power * self.get_count()


class ScreamerKillers(HeavyUnit, armory.FleetUnit):
    type_name = get_name(units.ScreamerKillers)
    type_id = 'screamer_brood_v1'
    keywords = ['Monster', 'Carnifex']

    class Warrior(ListSubUnit):
        type_name = u'Screamer-Killer'
        power = 6

        class Options(OptionsList):
            def __init__(self, parent):
                super(ScreamerKillers.Warrior.Options, self).__init__(parent, name='Options')
                self.variant(*biomorphs.ToxinSacsHTCWar)
                self.variant(*biomorphs.AdrenalGlandsMonsters)
                self.variant(*biomorphs.SporeCysts)

        def __init__(self, parent):
            gear = [ranged.BioPlasmicScream, melee.MonstrousScythingTalonsCarnifex2]
            cost = points_price(get_cost(units.ScreamerKillers), *gear)
            super(ScreamerKillers.Warrior, self).__init__(parent, points=cost,
                                                    gear=create_gears(melee.MonstrousScythingTalonsCarnifex, *gear))
            self.Options(self)

    def __init__(self, parent):
        super(ScreamerKillers, self).__init__(parent=parent)
        self.models = UnitList(parent=self, unit_class=self.Warrior, min_limit=1, max_limit=3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.Warrior.power * self.get_count()


class Thornbacks(HeavyUnit, armory.FleetUnit):
    type_name = get_name(units.Thornbacks)
    type_id = 'thornback_brood_v1'
    keywords = ['Monster', 'Carnifex']

    class Warrior(ListSubUnit):
        type_name = u'Thornback'
        power = 6

        class Options(OptionsList):
            def __init__(self, parent):
                super(Thornbacks.Warrior.Options, self).__init__(parent, name='Options')
                self.variant(*biomorphs.ToxinSacsHTCWar)
                self.variant(*biomorphs.AdrenalGlandsMonsters)
                self.variant(*ranged.SpineBanks)
                self.variant(*biomorphs.EnhancedSenses)
                self.variant(*melee.ThresherScythe)

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Thornbacks.Warrior.Weapon1, self).__init__(parent, 'Weapon')
                self.variant('Two devourers with brainleech worms',
                             2 * get_cost(ranged.DevourerWithBrainleechWorms),
                             gear=create_gears(ranged.DevourerWithBrainleechWorms,
                                               ranged.DevourerWithBrainleechWorms))
                self.variant('Two deathspitters with slimer maggots',
                             2 * get_cost(ranged.DeathspitterWithSlimerMaggots),
                             gear=create_gears(ranged.DeathspitterWithSlimerMaggots,
                                               ranged.DeathspitterWithSlimerMaggots))

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(Thornbacks.Warrior.Weapon2, self).__init__(parent, '')
                self.variant(*melee.MonstrousScythingTalonsCarnifex)
                self.variant(*ranged.StranglethornCannon)

        def __init__(self, parent):
            gear = [biomorphs.ChitinThorns]
            cost = points_price(get_cost(units.Thornbacks), *gear)
            super(Thornbacks.Warrior, self).__init__(parent, points=cost,
                                                     gear=create_gears(*gear))
            self.Weapon1(self)
            self.Weapon2(self)
            self.Options(self)

    def __init__(self, parent):
        super(Thornbacks, self).__init__(parent=parent)
        self.models = UnitList(parent=self, unit_class=self.Warrior, min_limit=1, max_limit=3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.Warrior.power * self.get_count()


class Mawloc(HeavyUnit, armory.FleetUnit):
    type_name = get_name(units.Mawloc)
    type_id = 'mawloc_v1'
    keywords = ['Monster']
    power = 6

    class Options(OptionsList):
        def __init__(self, parent):
            super(Mawloc.Options, self).__init__(parent, name='Options')
            self.variant(*biomorphs.ToxinSacsTrygon)
            self.variant(*biomorphs.AdrenalGlandsMonsters)

    class Tail(OneOf):
        def __init__(self, parent, spike=False, rattle=False):
            super(Mawloc.Tail, self).__init__(parent, name='Tail')
            if spike:
                self.variant(*melee.Toxinspike)
            if rattle:
                self.variant(*melee.BiostaticRattle)
            self.variant(*melee.PrehensilePincerTail)
            if not rattle:
                self.variant(*melee.BiostaticRattle)
            if not spike:
                self.variant(*melee.Toxinspike)

    def __init__(self, parent):
        gear = [melee.DistensibleJaws, melee.ScythingTalons,
                melee.ScythingTalons, melee.ScythingTalons]
        cost = points_price(get_cost(units.Mawloc), *gear)
        super(Mawloc, self).__init__(parent, points=cost,
                                     gear=create_gears(*gear))
        self.Tail(self)
        self.Options(self)


class Trygon(HeavyUnit, armory.FleetUnit):
    type_name = get_name(units.Trygon)
    type_id = 'trygon_v1'
    keywords = ['Monster']
    power = 9

    def __init__(self, parent):
        gear = [ranged.BioElectricPulse, melee.MassiveScythingTalonsTrygon]
        cost = points_price(get_cost(units.Trygon), *gear)
        gear += [melee.MassiveScythingTalonsTrygon, melee.MassiveScythingTalonsTrygon]
        super(Trygon, self).__init__(parent, points=cost,
                                     gear=create_gears(*gear))
        Mawloc.Tail(self, spike=True)
        Mawloc.Options(self)


class TrygonPrime(HeavyUnit, armory.FleetUnit):
    type_name = get_name(units.TrygonPrime)
    type_id = 'trygon_prime_v1'
    keywords = ['Monster', 'Character', 'Synapse']
    power = 10

    def __init__(self, parent):
        gear = [ranged.BioElectricPulseWithContainmentSpines,
                melee.MassiveScythingTalonsTrygon]
        cost = points_price(get_cost(units.TrygonPrime), *gear)
        gear += [melee.MassiveScythingTalonsTrygon, melee.MassiveScythingTalonsTrygon]
        super(TrygonPrime, self).__init__(parent, points=cost,
                                          gear=create_gears(*gear))
        Mawloc.Tail(self, rattle=True)
        Mawloc.Options(self)
