__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit
from builder.games.wh40k8ed.options import OptionsList, Count, Gear
import ranged, melee
from builder.games.wh40k8ed.utils import *


def add_basic_weapons(instance):
    instance.variant(*melee.ScythingTalons)
    instance.variant(*ranged.Spinefists)
    instance.variant(*ranged.Deathspitter)


def add_melee_weapons(obj):
    obj.variant(*melee.RendingClaws)
    obj.variant(*melee.Boneswords, gear=[Gear('Bonesword', count=2)])
    obj.variant(*melee.LashWhipAndBonesword, gear=[
        Gear('Lash whip'), Gear('Bonesword')
    ])


def add_basic_cannons(obj):
    obj.cannons = [
        obj.variant(*ranged.BarbedStrangler),
        obj.variant(*ranged.VenomCannon)]


def add_monster_weapon(obj):
    obj.variant(*melee.MonstrousRendingClaws)
    obj.variant(*melee.MonstrousBoneswords,
                gear=[Gear('Monstrous bonesword', count=2)])
    obj.variant(*melee.LashWhipAndMonstrousBonesword, gear=[
        Gear('Lash whip'), Gear('Monstrous bonesword')
    ])

def add_monster_cannon(obj):
    obj.variant('Two deathspitter with slimer maggots',
                get_cost(ranged.DeathspitterWithSlimerMaggots) * 2,
                gear=create_gears(ranged.DeathspitterWithSlimerMaggots,
                                  ranged.DeathspitterWithSlimerMaggots))
    obj.variant('Two devourers with brainleech worms',
                get_cost(ranged.DevourerWithBrainleechWorms) * 2,
                gear=create_gears(ranged.DevourerWithBrainleechWorms,
                                  ranged.DevourerWithBrainleechWorms))
    obj.restricted = [
        obj.variant(*ranged.StranglethornCannon),
        obj.variant(*ranged.HeavyVenomCannon)
    ]


class FleetUnit(Unit):
    faction = ['Tyranids', '<Hive fleet>']
    wiki_faction = 'Tyranids'

    @classmethod
    def calc_faction(cls):
        return ['BEHEMOTH', 'KRAKEN', 'GORGON', 'JORMUGAND',
                'HYDRA', 'KRONOS', 'LEVIATHAN']
