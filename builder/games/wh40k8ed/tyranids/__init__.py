from hq import HiveTyrant, TheSwarmlord, OldOneEye, Broodlord, TyranidPrime,\
    Tervigon, Neurothrope
from elites import GuardBrood, HiveGuard, Lictor, Maleceptor, Zoanthrope,\
    Venomethrope, Pyrovore, Haruspex, Deathleaper, RedTerror
from troops import TyranidWarriorBrood, Genestealers, Termagant, Hormagant,\
    Ripper
from fast import TyranidShrikeBrood, RavenerBrood, SkySlasherBrood, Gargoyle,\
    MucolidSpores, SporeMine
from heavy import CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc,\
    Tyrannofex, Toxicrene, Exocrine, ScreamerKillers, Thornbacks
from fliers import HiveCrone, Harpy
from transports import Tyrannocyte
from fort import Sporocyst

unit_types = [HiveTyrant, TheSwarmlord, OldOneEye, Broodlord,
              TyranidPrime, Tervigon, GuardBrood, HiveGuard, Lictor,
              Maleceptor, Zoanthrope, Venomethrope, Pyrovore,
              Haruspex, Deathleaper, RedTerror, TyranidWarriorBrood,
              Genestealers, Termagant, Hormagant, Ripper,
              TyranidShrikeBrood, RavenerBrood, SkySlasherBrood,
              Gargoyle, Harpy, HiveCrone, MucolidSpores, SporeMine,
              CarnifexBrood, Biovore, TrygonPrime, Trygon, Mawloc,
              Tyrannofex, Toxicrene, Exocrine, Tyrannocyte, Sporocyst,
              Neurothrope, ScreamerKillers, Thornbacks]
