__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import LordsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, OptionsList
import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class KnightErrantQM(LordsUnit, armory.KnightUnit):
    type_name = get_name(units.KnightErrant) + ' (Questor Mechanicus)'
    type_id = 'kn_errant_qm_v1'
    obsolete = True

    power = 23

    class CaparaceWeapons(OptionsList):
        def __init__(self, parent):
            super(KnightErrantQM.CaparaceWeapons, self).__init__(parent, 'Caparace Weapon',
                                                               limit=1)
            armory.add_caparace_weapons(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(KnightErrantQM.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*melee.ReaperChainsword)
            self.variant(*melee.ThunderstrikeGauntlet)

    class SecondaryWeapon(OneOf):
        def __init__(self, parent):
            super(KnightErrantQM.SecondaryWeapon, self).__init__(parent, 'Secondary weapon')
            self.variant(*ranged.HeavyStubber)
            self.variant(*ranged.Meltagun)

    def __init__(self, parent):
        gear = [ranged.ThermalCannon, melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightErrant), *gear)
        super(KnightErrantQM, self).__init__(parent, get_name(units.KnightErrant), points=cost,
                                             gear=create_gears(*gear))
        self.Weapon1(self)
        self.SecondaryWeapon(self)
        self.CaparaceWeapons(self)


class KnightPaladinQM(LordsUnit, armory.KnightUnit):
    type_name = get_name(units.KnightPaladin) + ' (Questor Mechanicus)'
    type_id = 'kn_paladin_qm_v1'
    obsolete = True

    power = 24

    def __init__(self, parent):
        gear = [ranged.RapidFireBattleCannon, ranged.HeavyStubber, melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightPaladin), *gear)
        super(KnightPaladinQM, self).__init__(parent, get_name(units.KnightPaladin), points=cost,
                                              gear=create_gears(*gear))
        KnightErrantQM.Weapon1(self)
        KnightErrantQM.SecondaryWeapon(self)
        KnightErrantQM.CaparaceWeapons(self)


class KnightWardenQM(LordsUnit, armory.KnightUnit):
    type_name = get_name(units.KnightWarden) + ' (Questor Mechanicus)'
    type_id = 'kn_warden_qm_v1'
    obsolete = True

    power = 25

    def __init__(self, parent):
        gear = [ranged.AvengerGatlingCannon, ranged.HeavyFlamer, melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightWarden), *gear)
        super(KnightWardenQM, self).__init__(parent, get_name(units.KnightWarden), points=cost,
                                             gear=create_gears(*gear))
        KnightErrantQM.Weapon1(self)
        KnightErrantQM.SecondaryWeapon(self)
        KnightErrantQM.CaparaceWeapons(self)


class KnightGallantQM(LordsUnit, armory.KnightUnit):
    type_name = get_name(units.KnightGallant) + ' (Questor Mechanicus)'
    type_id = 'kn_gallant_qm_v1'
    obsolete = True

    power = 21

    def __init__(self, parent):
        gear = [melee.ReaperChainsword, melee.ThunderstrikeGauntlet, melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightPaladin), *gear)
        super(KnightGallantQM, self).__init__(parent, get_name(units.KnightGallant), points=cost,
                                              gear=create_gears(*gear))
        KnightErrantQM.SecondaryWeapon(self)
        KnightErrantQM.CaparaceWeapons(self)


class KnightCrusaderQM(LordsUnit, armory.KnightUnit):
    type_name = get_name(units.KnightCrusader) + ' (Questor Mechanicus)'
    type_id = 'kn_crusader_qm_v1'
    obsolete = True

    power = 27

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(KnightCrusaderQM.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.ThermalCannon)
            self.variant(join_and(ranged.RapidFireBattleCannon, ranged.HeavyStubber),
                         get_costs(ranged.RapidFireBattleCannon, ranged.HeavyStubber),
                         gear=create_gears(ranged.RapidFireBattleCannon, ranged.HeavyStubber))

    def __init__(self, parent):
        gear = [ranged.AvengerGatlingCannon, melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightCrusader), *gear)
        super(KnightCrusaderQM, self).__init__(parent, get_name(units.KnightCrusader), points=cost,
                                               gear=create_gears(*gear))
        self.Weapon1(self)
        KnightErrantQM.SecondaryWeapon(self)
        KnightErrantQM.CaparaceWeapons(self)
