__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import ElitesUnit
from builder.games.wh40k8ed.options import (OneOf, Gear, Count,
                                            UnitDescription, OptionsList, ListSubUnit, UnitList)
from builder.games.wh40k8ed.utils import get_cost, points_price, get_name, create_gears
import armory
import units
import ranged
import melee
import wargear


class Shaper(ElitesUnit, armory.TauUnit):
    type_name = get_name(units.KrootShaper)
    type_id = 'shaper_v1'
    faction = ['Kroot']
    keywords = ['Character', 'Infantry']
    power = 2

    def __init__(self, parent):
        gear = [melee.RitualBlade]
        cost = points_price(get_cost(units.KrootShaper), *gear)
        super(Shaper, self).__init__(parent=parent, points=cost, gear=create_gears(*gear))
        self.weapon = self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Shaper.Weapon, self).__init__(parent=parent, name='Weapon')

            self.krootrifle = self.variant(*ranged.KrootRifle)
            self.pulserifle = self.variant(*ranged.PulseRifle)
            self.pulsecarbine = self.variant(*ranged.PulseCarbine)


class KrootoxSquad(ElitesUnit, armory.TauUnit):
    type_name = get_name(units.KrootoxRiders)
    type_id = 'krootox_v1'
    faction = ['Kroot']
    keywords = ['Cavalry']

    def __init__(self, parent):
        super(KrootoxSquad, self).__init__(parent=parent)
        gear = [ranged.KrootGun, melee.KrootoxFists]
        cost = points_price(get_cost(units.KrootoxRiders), *gear)
        self.kroot = Count(
            self, 'Krootos Rider', min_limit=1, max_limit=3, points=cost,
            gear=UnitDescription('Krootox Rider', options=[Gear('Kroot gun'),
                                                           UnitDescription('Krootox', options=[Gear('Krootox fists')])])
        )

    def get_count(self):
        return self.kroot.cur

    def build_power(self):
        return 2 * self.kroot.cur


class StealthTeam(ElitesUnit, armory.SeptUnit):
    type_name = get_name(units.XV25StealthBattlesuit)
    type_id = 'stealthteam_v1'
    keywords = ['Battlesuit', 'Infantry', 'Jetpack', 'Fly']

    class Suit(ListSubUnit):
        type_name = u'Stealth Shas\'ui'
        type_id = 'shasui_v1'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(StealthTeam.Suit.Weapon, self).__init__(parent=parent, name='Weapon')
                self.burstcannon = self.variant(*ranged.BurstCannon)
                self.fusionblaster = self.variant(*ranged.FusionBlaster)

        class ShasVre(OptionsList):
            def __init__(self, parent):
                super(StealthTeam.Suit.ShasVre, self).__init__(parent=parent, name='Leader', limit=None)
                self.shasvre = self.variant('Stealth Shas\'vre', 0, gear=[])

        class Options(OptionsList):
            def __init__(self, parent):
                super(StealthTeam.Suit.Options, self).__init__(parent=parent, name='Options')
                self.marketlightandtargetlock = self.variant('Marketlight and target lock',
                                                             get_cost(ranged.Markerlight) +
                                                             get_cost(wargear.TargetLock),
                                                             gear=[
                                                                 Gear('Marketlight'), Gear('Target lock')
                                                             ])

        def __init__(self, parent):
            super(StealthTeam.Suit, self).__init__(
                parent=parent, points=get_cost(units.XV25StealthBattlesuit),
            )
            self.weapon = self.Weapon(self)
            self.support = armory.SupportSystem(self, slots=1)
            self.shasvre = self.ShasVre(self)
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(StealthTeam.Suit, self).build_description()
            if self.shasvre.shasvre.value:
                desc.name = self.shasvre.shasvre.title
            return desc

        def check_rules(self):
            super(StealthTeam.Suit, self).check_rules()
            # if self.get_count() > 1:
            #     self.shasvre.shasvre.value = False
            self.opt.used = self.opt.visible = self.shasvre.any

        @ListSubUnit.count_gear
        def count_shasvre(self):
            return self.shasvre.shasvre.value

        @ListSubUnit.count_gear
        def count_blasters(self):
            return self.weapon.cur == self.weapon.fusionblaster

    def __init__(self, parent):
        super(StealthTeam, self).__init__(parent=parent)
        self.team = UnitList(self, self.Suit, min_limit=3, max_limit=6)
        self.opt = self.Options(self)
        armory.add_drones(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(StealthTeam.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.HomingBeacon)

    def check_rules(self):
        super(StealthTeam, self).check_rules()
        shasvre = sum(u.count_shasvre() for u in self.team.units)
        if shasvre > 1:
            self.error('Only one Shas\'ui can be upgraded to Shas\'vre (upgraded: {0}).'.format(shasvre))

        blaster = sum(m.count_blasters() for m in self.team.units)
        blaster_lim = int(self.get_count() / 3)
        if blaster > blaster_lim:
            self.error('Only one blaster may be taken per 3 models in suites in unit.')
        Count.norm_counts(0, 2, self.drones)

    def get_count(self):
        return self.team.count

    def build_statistics(self):
        res = super(StealthTeam, self).build_statistics()
        res['Models'] += sum(c.cur for c in self.drones)
        return res

    def build_power(self):
        return 6 + 6 * (self.team.count > 3) + 1 * (sum(c.cur for c in self.drones) > 0)


class CrisisTeam(ElitesUnit, armory.SeptUnit):
    type_name = get_name(units.XV8CrisisBattlesuits)
    type_id = 'crisisbattlesuitteam_v3'
    keywords = ['Battlesuit', 'Jetpack', 'Fly']

    class CrisisSuit(ListSubUnit):
        type_name = u'Crisis Shas\'ui'
        type_id = 'shasui_v1'

        def variant(self, name, points=None):
            return Count(self, name, 0, 3, points)

        def __init__(self, parent):
            super(CrisisTeam.CrisisSuit, self).__init__(parent, points=get_cost(units.XV8CrisisBattlesuits))
            armory.add_ranged_weapons(self)
            self.rng[1].cur = 1
            self.support = armory.SupportSystem(self, slots=3)
            self.shasvre = self.ShasVre(self)

        class ShasVre(OptionsList):
            def __init__(self, parent):
                super(CrisisTeam.CrisisSuit.ShasVre, self).__init__(parent=parent, name='Leader', limit=None)

                self.shasvre = self.variant('Crisis Shas\'vre')

        def check_rules(self):
            super(CrisisTeam.CrisisSuit, self).check_rules()
            Count.norm_counts(max(0, 1 - self.support.count), 3 - self.support.count, self.rng)

        @ListSubUnit.count_gear
        def count_shasvre(self):
            return self.shasvre.shasvre.value

        def build_description(self):
            desc = super(CrisisTeam.CrisisSuit, self).build_description()
            if self.shasvre.shasvre.value:
                desc.name = self.shasvre.shasvre.title
            return desc

    def __init__(self, parent):
        super(CrisisTeam, self).__init__(parent=parent)
        self.team = UnitList(self, self.CrisisSuit, min_limit=3, max_limit=9)
        armory.add_drones(self)

    def get_count(self):
        return self.team.count

    def check_rules(self):
        super(CrisisTeam, self).check_rules()
        shasvre = sum(u.count_shasvre() for u in self.team.units)
        if shasvre > 1:
            self.error('Only one Shas\'ui can be upgraded to Shas\'vre (upgraded: {0}).'.format(shasvre))
        Count.norm_counts(0, 2 * self.team.count, self.drones)

    def build_statistics(self):
        res = super(CrisisTeam, self).build_statistics()
        res['Models'] += sum(c.cur for c in self.drones)
        return res

    def build_power(self):
        return 12 * ((2 + self.team.count) / 3) + (1 + sum(c.cur for c in self.drones)) / 2


class Bodyguards(ElitesUnit, armory.SeptUnit):
    type_name = get_name(units.XV8CrisisBodyguards)
    type_id = 'crisisbodyguards_v1'
    keywords = ['Battlesuit', 'Jetpack', 'Fly']

    class CrisisSuit(ListSubUnit):
        type_name = u'Crisis Bodyguard'

        def variant(self, name, points=None):
            return Count(self, name, 0, 3, points)

        def __init__(self, parent):
            super(Bodyguards.CrisisSuit, self).__init__(parent, points=get_cost(units.XV8CrisisBodyguards))
            armory.add_ranged_weapons(self)
            self.rng[1].cur = 1
            self.support = armory.SupportSystem(self, slots=3)

        def check_rules(self):
            super(Bodyguards.CrisisSuit, self).check_rules()
            Count.norm_counts(max(0, 1 - self.support.count), 3 - self.support.count, self.rng)

    def __init__(self, parent):
        super(Bodyguards, self).__init__(parent=parent)
        self.team = UnitList(self, self.CrisisSuit, min_limit=3, max_limit=9)
        armory.add_drones(self)

    def get_count(self):
        return self.team.count

    def check_rules(self):
        super(Bodyguards, self).check_rules()
        Count.norm_counts(0, 2 * self.team.count, self.drones)

    def build_statistics(self):
        res = super(Bodyguards, self).build_statistics()
        res['Models'] += sum(c.cur for c in self.drones)
        return res

    def build_power(self):
        return 1 + 12 * ((2 + self.team.count) / 3) + (1 + sum(c.cur for c in self.drones)) / 2


class Ghostkeel(ElitesUnit, armory.SeptUnit):
    type_name = get_name(units.XV95GhostkeelBattlesuit)
    type_id = 'ghostkeel_v1'
    keywords = ['Battlesuit', 'Jetpack', 'Fly', 'Monster']
    power = 10

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Ghostkeel.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.FusionCollider)
            self.variant(*ranged.CyclicIonRaker)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Ghostkeel.Weapon2, self).__init__(parent, '')
            self.variant('Two flamers', get_cost(ranged.Flamer) * 2, gear=[Gear('Flamer', count=2)])
            self.variant('Two burst cannons', get_cost(ranged.BurstCannon) * 2, gear=[Gear('Burst cannon', count=2)])
            self.variant('Two fusion blasters', get_cost(ranged.FusionBlaster) * 2,
                         gear=[Gear('Fusion blaster', count=2)])

    def __init__(self, parent):
        gear = [units.MV5StealthDrones, units.MV5StealthDrones]
        cost = points_price(get_cost(units.XV95GhostkeelBattlesuit), *gear)
        super(Ghostkeel, self).__init__(parent, points=cost)
        self.Weapon1(self)
        self.Weapon2(self)
        armory.SupportSystem(self, 2, ghostkeel=True)

    def build_description(self):
        res = UnitDescription(self.name, self.points)
        suit = super(Ghostkeel, self).build_description()
        suit.name = "Ghostkeel Shas'vre"
        res.add(suit)
        res.add(UnitDescription('MV5 Stealth Drone', count=2))
        return res


class Riptide(ElitesUnit, armory.SeptUnit):
    type_name = get_name(units.XV104RiptideBattlesuit)
    type_id = 'xv104riptide_v3'
    keywords = ['Battlesuit', 'Monster', 'Jetpack', 'Fly']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Riptide.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.heavyburstcannon = self.variant(*ranged.HeavyBurstCannon)
            self.ionaccelerator = self.variant(*ranged.IonAccelerator)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Riptide.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Two smart missile system', get_cost(ranged.SmartMissileSystem) * 2,
                         gear=[Gear('Smart missile system', count=2)])
            self.variant('Two plasma rifles', get_cost(ranged.PlasmaRifle) * 2,
                         gear=[Gear('Plasma rifle', count=2)])
            self.variant('Two fusion blasters', get_cost(ranged.FusionBlaster) * 2,
                         gear=[Gear('Fusion blaster', count=2)])

    class Drones(Count):
        @property
        def description(self):
            return []

    def __init__(self, parent):
        super(Riptide, self).__init__(parent, points=get_cost(units.XV104RiptideBattlesuit))
        self.Weapon1(self)
        self.Weapon2(self)
        self.supp = armory.SupportSystem(self, slots=2, riptide=True)
        self.drones = self.Drones(self, get_name(units.MV84ShieldedMissileDrone), min_limit=0, max_limit=2,
                                  points=get_cost(units.MV84ShieldedMissileDrone),
                                  per_model=True)

    def build_description(self):
        res = UnitDescription(self.name, self.points)
        suit = super(Riptide, self).build_description()
        suit.name = "Riptide Shas'vre"
        res.add(suit)
        if self.drones.cur:
            res.add(UnitDescription('MV84 Shielded Missile Drone', count=self.drones.cur,
                                    options=[Gear('Missile pod')]))
        return res

    def build_power(self):
        return 14 + 2 * (self.drones.cur > 0)


class Marksman(ElitesUnit, armory.SeptUnit):
    type_name = get_name(units.FiresightMarksman)
    type_id = 'marksman_v1'
    keywords = ['Character', 'Infantry']
    power = 1

    def __init__(self, parent):
        gear = [ranged.Markerlight, ranged.PulsePistol]
        cost = points_price(get_cost(units.FiresightMarksman), *gear)
        super(Marksman, self).__init__(parent=parent, gear=create_gears(*gear),
                                       points=cost)
