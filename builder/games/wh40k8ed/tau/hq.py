__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import HqUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList
from heavy import Skyray, Hammerhead
from builder.games.wh40k8ed.utils import get_cost, points_price, get_name, create_gears
import armory
import units
import ranged
import melee
import wargear


class Commander(HqUnit, armory.SeptUnit):
    type_name = get_name(units.CrisisCommander)
    type_id = 'crisis_commander_v1'
    kwname = 'Commander'
    keywords = ['Battlesuit', 'Character', 'XV8 Crisis', 'Jetpack', 'Fly']

    power = 7

    def variant(self, name, points=None):
        return Count(self, name, 0, 4, points)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Commander.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.XV8_02CrisisIridiumBattlesuit)

    def __init__(self, parent):
        super(Commander, self).__init__(parent, points=get_cost(units.CrisisCommander))
        armory.add_ranged_weapons(self)
        # set initial weapons
        self.rng[1].cur = 1
        self.rng[-2].cur = 1
        self.support = armory.SupportSystem(self, 4)
        armory.add_drones(self)
        self.opt = self.Options(self)

    def check_rules(self):
        super(Commander, self).check_rules()
        Count.norm_counts(0, 2, self.drones)
        Count.norm_counts(max(0, 2 - self.support.count), 4 - self.support.count, self.rng)

    def build_statistics(self):
        res = super(Commander, self).build_statistics()
        res['Models'] += sum(c.cur for c in self.drones)
        return res

    def build_power(self):
        return self.power + any(c.cur for c in self.drones)


class EnforcerCommander(HqUnit, armory.SeptUnit):
    type_name = get_name(units.EnforcerCommander)
    type_id = 'commander_v1'
    kwname = 'Commander'
    keywords = ['Battlesuit', 'Character', 'XV85 Enforcer', 'Jetpack', 'Fly']

    power = 7

    def variant(self, name, points=None):
        return Count(self, name, 0, 4, points)

    def __init__(self, parent):
        super(EnforcerCommander, self).__init__(parent, points=get_cost(units.EnforcerCommander))
        armory.add_ranged_weapons(self)
        # set initial weapons
        self.rng[1].cur = 1
        self.rng[-2].cur = 1
        self.support = armory.SupportSystem(self, 4)
        armory.add_drones(self)

    def check_rules(self):
        super(EnforcerCommander, self).check_rules()
        Count.norm_counts(0, 2, self.drones)
        Count.norm_counts(max(0, 2 - self.support.count), 4 - self.support.count, self.rng)

    def build_statistics(self):
        res = super(EnforcerCommander, self).build_statistics()
        res['Models'] += sum(c.cur for c in self.drones)
        return res

    def build_power(self):
        return self.power + any(c.cur for c in self.drones)


class ColdstarCommander(HqUnit, armory.SeptUnit):
    type_name = get_name(units.ColdstarCommander)
    type_id = 'cold_commander_v1'
    kwname = 'Commander'
    keywords = ['Battlesuit', 'Character', 'Jetpack', 'Fly']

    power = 7

    def variant(self, name, points=None):
        return Count(self, name, 0, 4, points)

    class LeftWeapon(OneOf):
        def __init__(self, parent):
            super(ColdstarCommander.LeftWeapon, self).__init__(parent=parent, name='Weapon')
            base = self.variant(*ranged.HighOutputBurstCannon)
            armory.add_ranged_weapons(self)
            self.rng.insert(0, base)
            # armory.add_support_systems(self)
            self.support = self.variant('Support system', gear=[])

    class RightWeapon(OneOf):
        def __init__(self, parent):
            super(ColdstarCommander.RightWeapon, self).__init__(parent=parent, name='Weapon')
            base = self.variant(*ranged.MissilePod)
            armory.add_ranged_weapons(self)
            self.rng.insert(0, base)
            # armory.add_support_systems(self)
            self.support = self.variant('Support system', gear=[])

    def __init__(self, parent):
        super(ColdstarCommander, self).__init__(parent, points=get_cost(units.ColdstarCommander))
        self.wep1 = self.LeftWeapon(self)
        self.wep2 = self.RightWeapon(self)
        armory.add_ranged_weapons(self)
        self.support = armory.SupportSystem(self, 4)
        armory.add_drones(self)

    def check_rules(self):
        super(ColdstarCommander, self).check_rules()
        Count.norm_counts(0, 2, self.drones)
        extra_support = (self.wep1.cur == self.wep1.support) + (self.wep2.cur == self.wep2.support)
        self.support.limit = 2 + extra_support
        Count.norm_counts(0, 2 + extra_support - self.support.count, self.rng)
        if self.support.count < extra_support:
            self.error("Take at least {} support systems".format(extra_support))

    def build_power(self):
        return self.power + any(c.cur for c in self.drones)

    def build_statistics(self):
        res = super(ColdstarCommander, self).build_statistics()
        res['Models'] += sum(c.cur for c in self.drones)
        return res


class Ethereal(HqUnit, armory.TauUnit):
    type_name = get_name(units.Ethereal)
    type_id = 'ethereal_v1'
    keywords = ['Character', 'Infantry']
    power = 2

    @classmethod
    def calc_faction(cls):
        return ['<SEPT>', "T'AU SEPT", "VIOR'LA SEPT", "DAL'YTH SEPT", "SA'CEA SEPT", "BORK'AN SEPT"]

    def __init__(self, parent):
        super(Ethereal, self).__init__(parent=parent, points=get_cost(units.Ethereal))
        self.Weapon(self)
        self.opt = self.Options(self)
        armory.add_drones(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Ethereal.Weapon, self).__init__(parent=parent, name='Weapon')
            self.honourblade = self.variant(*melee.HonourBlade)
            self.twoequalisers = self.variant(*melee.Equalizers)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Ethereal.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.hover = self.variant(*wargear.HoverDrone)

    def check_rules(self):
        super(Ethereal, self).check_rules()
        Count.norm_counts(0, 2, self.drones)

    def build_statistics(self):
        res = super(Ethereal, self).build_statistics()
        res['Models'] += sum(c.cur for c in self.drones)
        return res

    def build_power(self):
        return self.power + any(c.cur for c in self.drones)


class Fireblade(HqUnit, armory.SeptUnit):
    type_name = get_name(units.CadreFireblade)
    type_id = 'cadrefireblade_v1'
    power = 2

    def __init__(self, parent):
        gear = [ranged.PulseRifle, ranged.PhotonGrenades, ranged.Markerlight]
        cost = points_price(get_cost(units.CadreFireblade), *gear)
        super(Fireblade, self).__init__(
            parent=parent, points=cost,
            gear=create_gears(*gear))
        armory.add_drones(self)

    def check_rules(self):
        super(Fireblade, self).check_rules()
        Count.norm_counts(0, 2, self.drones)

    def build_statistics(self):
        res = super(Fireblade, self).build_statistics()
        res['Models'] += sum(c.cur for c in self.drones)
        return res

    def build_power(self):
        return self.power + any(c.cur for c in self.drones)


class Farsight(HqUnit, armory.TauUnit):
    type_name = get_name(units.CommanderFarsight)
    type_id = 'commanderfarsight_v1'
    faction = ['Farsight Enclaves']
    kwname = 'Farsight'
    keywords = ['Battlesuit', 'Character', 'Jetpack', 'Fly', 'Commander']
    power = 8

    def __init__(self, parent):
        super(Farsight, self).__init__(
            parent, unique=True, gear=[
                Gear('Plasma rifle'), Gear('Dawn blade')
            ], static=True, points=get_cost(units.CommanderFarsight)
        )


class Shadowsun(HqUnit, armory.TauUnit):
    type_name = get_name(units.CommanderShadowsun)
    type_id = 'commandershadowsun_v1'
    faction = ["T'au Sept"]
    kwname = 'Shadowsun'
    keywords = ['Battlesuit', 'Character', 'Jetpack', 'Fly', 'Commander', 'XV22 Stalker']
    power = 9

    def __init__(self, parent):
        super(Shadowsun, self).__init__(
            parent=parent,
            unique=True, gear=[
                Gear('Fusion blaster', count=2)
            ], points=get_cost(units.CommanderShadowsun)
        )
        self.commandlinkdrone = Count(self, 'Command link drone', min_limit=0, max_limit=1,
                                      points=get_cost(units.MV62CommandLinkDrone),
                                      gear=UnitDescription('MV62 Command-link drone'))
        self.mv52shielddrone = Count(self, 'Shield drones', min_limit=0, max_limit=2,
                                     points=get_cost(units.MV52ShieldDrone),
                                     gear=UnitDescription('MV52 Shield Drone'))

    def build_statistics(self):
        res = super(Shadowsun, self).build_statistics()
        res['Models'] += self.commandlinkdrone.cur + self.mv52shielddrone.cur
        return res


class Aunshi(HqUnit, armory.TauUnit):
    type_name = get_name(units.AunShi)
    type_id = 'aunshi_v1'
    faction = ["Vior'La Sept"]

    keywords = ['Character', 'Infantry', 'Ethereal']
    power = 5

    def __init__(self, parent):
        super(Aunshi, self).__init__(
            parent, unique=True, static=True,
            gear=[Gear('Honour blade')], points=get_cost(units.AunShi)
        )


class Aunva(HqUnit, armory.TauUnit):
    type_name = get_name(units.AunVa)
    type_id = 'aunva_v1'
    faction = ["T'au Sept"]

    keywords = ['Character', 'Infantry', 'Ethereal']
    power = 4

    def __init__(self, parent):
        super(Aunva, self).__init__(parent, unique=True, static=True,
                                    points=get_cost(units.AunVa) + get_cost(units.EtherealGuard) * 2, gear=[
                UnitDescription('Aun\'va', options=[Gear('The Paradox of Duality')]),
                UnitDescription(name='Honour Guard', options=[Gear('Honour blade')], count=2)])

    def build_statistics(self):
        res = super(Aunva, self).build_statistics()
        res['Models'] += 2
        return res


class Darkstider(HqUnit, armory.TauUnit):
    type_name = get_name(units.Darkstrider)
    type_id = 'darkstider_v1'
    faction = ["T'au Sept"]

    keywords = ['Character', 'Infantry']
    power = 3

    def __init__(self, parent):
        super(Darkstider, self).__init__(
            parent=parent, static=True, unique=True,
            gear=[Gear('Pulse carbine'),
                  Gear('Photon greandes'),
                  Gear('Markerlight')
                  ], points=get_cost(units.Darkstrider)
        )


class Longstrike(HqUnit, armory.TauUnit):
    type_name = get_name(units.Longstrike)
    type_id = 'longstrike_v1'
    faction = ["T'au Sept"]
    keywords = ['Vehicle', 'Fly', 'Character', 'Hammerhead']
    power = 10

    def __init__(self, parent):
        super(Longstrike, self).__init__(parent=parent, unique=True, points=get_cost(units.Longstrike))
        self.weapon = Hammerhead.Weapon(self)
        self.support_weapon = Skyray.Weapon(self)
        self.seekermissile = Count(self, 'Seeker missile', min_limit=0, max_limit=2,
                                   points=get_cost(ranged.SeekerMissile))

    def build_statistics(self):
        res = super(Longstrike, self).build_statistics()
        if self.support_weapon.cur == self.support_weapon.twogundrones:
            res['Models'] += 2
        return res
