__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HeavyUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, UnitList,\
    UnitDescription, OptionsList, Count, SubUnit, ListSubUnit
import armory, units, melee, ranged
from builder.games.wh40k8ed.utils import *


class CultLemanRuss(HeavyUnit, armory.CultUnit):
    type_name = get_name(units.CultLemanRuss)
    type_id = 'gs_lemanruss_v1'
    power = 11

    class MainWeapon(OneOf):
        def __init__(self, parent):
            super(CultLemanRuss.MainWeapon, self).__init__(parent=parent, name='MainWeapon')
            self.variant(*ranged.BattleCannon)
            self.variant(*ranged.EradicatorNovaCannon)
            self.variant(*ranged.ExterminatorAutocannon)
            self.variant(*ranged.VanquisherBattleCannon)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(CultLemanRuss.Weapon, self).__init__(parent=parent, name='Weapon')

            self.heavybolter = self.variant(*ranged.HeavyBolter)
            self.heavyflamer = self.variant(*ranged.HeavyFlamer)
            self.lascannon = self.variant(*ranged.Lascannon)

    class Weapon3(OptionsList):
        def __init__(self, parent):
            super(CultLemanRuss.Weapon3, self).__init__(parent, '')
            self.variant(*ranged.HunterKillerMissile)
            self.sb = self.variant(*ranged.StormBolter)
            self.hs = self.variant(*ranged.HeavyStubber)

        def check_rules(self):
            super(CultLemanRuss.Weapon3, self).check_rules()
            OptionsList.process_limit([self.sb, self.hs], 1)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(CultLemanRuss.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)

            self.variant('Heavy flamers', get_costs(ranged.HeavyFlamer, ranged.HeavyFlamer),
                         gear=[Gear('Heavy flamer', count=2)])
            self.variant('Heavy bolters', get_costs(ranged.HeavyBolter, ranged.HeavyBolter),
                         gear=[Gear('Heavy bolter', count=2)])
            self.variant('Multi-meltas', get_costs(ranged.MultiMelta, ranged.MultiMelta),
                         gear=[Gear('Multimelta', count=2)])
            self.variant('Plasma cannons', get_costs(ranged.PlasmaCannon, ranged.PlasmaCannon),
                         gear=[Gear('Plasma cannon', count=2)])

    def __init__(self, parent):
        super(CultLemanRuss, self).__init__(parent=parent, points=get_cost(units.CultLemanRuss))
        self.type = self.MainWeapon(self)
        self.Weapon(self)
        self.Weapon3(self)
        self.Sponsons(self)


class GoliathRockgrinder(HeavyUnit, armory.CultUnit):
    type_name = get_name(units.GoliathRockgrinder)
    type_id = 'gs_goliath_rockgrinder_v1'
    power = 6

    class Weapon(OneOf):
        def __init__(self, parent):
            super(GoliathRockgrinder.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant(*ranged.HeavyMiningLaser)
            self.variant(*ranged.ClearanceIncinerator)
            self.variant(*ranged.HeavySeismicCannon)

    class Charges(OptionsList):
        def __init__(self, parent):
            super(GoliathRockgrinder.Charges, self).__init__(parent, 'Options')
            self.variant(*ranged.CacheOfDemolitionCharges)

    def __init__(self, parent):
        gear = [ranged.HeavyStubber, melee.DrilldozerBlade]
        cost = points_price(get_cost(units.GoliathRockgrinder), *gear)
        super(GoliathRockgrinder, self) .__init__(parent=parent, points=cost,
                                                  gear=create_gears(*gear))
        self.Weapon(self)
        self.Charges(self)

