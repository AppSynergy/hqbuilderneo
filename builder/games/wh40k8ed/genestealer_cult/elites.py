__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, UnitList,\
    UnitDescription, OptionsList, Count, SubUnit, ListSubUnit,\
    OptionalSubUnit
import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class HybridMetamorphs(ElitesUnit, armory.CultUnit):
    type_name = get_name(units.HybridMetamorphs)
    type_id = 'gs_hybrid_metamorphs_v1'

    power = 6

    class Metamorph(ListSubUnit):
        type_name = 'Hybrid Metamorph'

        class Claws(OneOf):
            def __init__(self, parent):
                super(HybridMetamorphs.Metamorph.Claws, self).__init__(parent, 'Claws')
                self.variant('Rending claw')
                self.talon = self.variant(*melee.MetamorphTalon)

        class Talons(OneOf):
            def __init__(self, parent):
                super(HybridMetamorphs.Metamorph.Talons, self).__init__(parent, 'Talons')
                self.talons = self.variant(*melee.MetamorphTalon)
                self.variant(*melee.MetamorphWhip)
                self.claw = self.variant(*melee.MetamorphClaw)

        class Pistols(OneOf):
            def __init__(self, parent):
                super(HybridMetamorphs.Metamorph.Pistols, self).__init__(parent, 'Pistols')
                self.variant(*ranged.Autopistol)
                self.variant(*ranged.HandFlamer)

        class Banners(OptionsList):
            def __init__(self, parent):
                super(HybridMetamorphs.Metamorph.Banners, self).__init__(parent, "Icon", limit=1)
                self.variant(*wargear.CultIcon)

        def __init__(self, parent):
            super(HybridMetamorphs.Metamorph, self).__init__(
                parent=parent, points=get_cost(units.HybridMetamorphs),
                gear=[Gear('Blasting charges'), ]
            )
            self.claws = self.Claws(self)
            self.talons = self.Talons(self)
            self.pistols = self.Pistols(self)
            self.ban = self.Banners(self)

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.ban.any

        def check_rules(self):
            super(HybridMetamorphs.Metamorph, self).check_rules()
            self.claws.used = self.claws.visible = self.talons.cur !=self.talons.claw

    class MetamorphLeader(Unit):
        type_name = 'Metamorph Leader'

        class Bonesword(OptionsList):
            def __init__(self, parent):
                super(HybridMetamorphs.MetamorphLeader.Bonesword, self).__init__(parent, '')
                self.variant(*melee.Bonesword)

        def __init__(self, parent):
            super(HybridMetamorphs.MetamorphLeader, self).__init__(parent, points=get_cost(units.HybridMetamorphs),
                                                                   gear=[Gear('Blasting charges')])
            self.claws = HybridMetamorphs.Metamorph.Claws(self)
            self.talons = HybridMetamorphs.Metamorph.Talons(self)
            self.ccw = self.Bonesword(self)
            self.pistols = HybridMetamorphs.Metamorph.Pistols(self)

        def check_rules(self):
            super(HybridMetamorphs.MetamorphLeader, self).check_rules()
            self.claws.used = self.claws.visible = self.talons.cur !=self.talons.claw

    def __init__(self, parent):
        super(HybridMetamorphs, self).__init__(parent)
        self.leader = SubUnit(self, self.MetamorphLeader(self))
        self.metamorphs = UnitList(self, self.Metamorph, 4, 9)

    def check_rules(self):
        flags = sum(pal.count_banners() for pal in self.metamorphs.units)
        if flags > 1:
            self.error('Only one banner per unit may be taken; taken: {}'.format(flags))

    def get_count(self):
        return 1 + self.metamorphs.count

    def build_power(self):
        return self.power * (1 + (self.get_count() > 5))


class Aberrants(ElitesUnit, armory.CultUnit):
    type_name = get_name(units.Aberrants)
    type_id = 'gs_aberrants_v1'
    power = 7

    model = UnitDescription('Aberrant', options=[Gear('Rending claw')])

    class Aberrant(Count):
        @property
        def description(self):
            return [Aberrants.model.clone().add(create_gears(melee.PowerPick)).
                    set_count(self.cur - self.parent.ham.cur)]

    class Hypermorph(ListSubUnit):
        type_name = 'Aberrant Hypermorph'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Aberrants.Hypermorph.Weapon, self).__init__(parent, 'Weapon')
                self.variant(*melee.PowerHammer)
                self.variant(*melee.HeavyImprovisedWeapon)

        def __init__(self, parent):
            super(Aberrants.Hypermorph, self).__init__(parent, points=points_price(get_cost(units.Aberrants), melee.HypermorphTail),
                                                       gear=create_gears(melee.RendingClaws, melee.HypermorphTail))
            self.Weapon(self)

    class Leaders(OptionalSubUnit):
        def __init__(self, parent):
            super(Aberrants.Leaders, self).__init__(parent, 'Leaders')
            self.hypers = UnitList(self, Aberrants.Hypermorph, 1, 2)

    def __init__(self, parent):
        super(Aberrants, self).__init__(parent)
        self.ldr = self.Leaders(self)
        self.models = self.Aberrant(self, 'Aberrants', 5, 10,
                                    points=points_price(get_cost(units.Aberrants), melee.PowerPick),
                                    per_model=True)
        self.ham = Count(self, 'Power hammer', 0, 5, points=get_cost(melee.PowerHammer) - get_cost(melee.PowerPick),
                         gear=Aberrants.model.clone().add(create_gears(melee.PowerHammer)))

    def check_rules(self):
        super(Aberrants, self).check_rules()
        self.models.min, self.models.max = (5 - self.ldr.count, 10 - self.ldr.count)
        self.ham.max = self.models.cur
        if self.models.cur < 8 and self.ldr.count > 1:
            self.error("One Aberrant hypermorph may be taken per 5 models (taken 2)")

    def build_power(self):
        return self.power * (1 + ((self.models.cur + self.ldr.count) > 5))

    def get_count(self):
        return self.models.cur + self.ldr.count


class PurestrainGenestealers(ElitesUnit, armory.CultUnit):
    type_name = get_name(units.PurestrainGenestealers)
    type_id = 'gs_purestrain_genestealers_v3'
    power = 4
    model = UnitDescription('Purestrain genestealer', options=[Gear('Rending claws')])

    class Stealer(Count):
        @property
        def description(self):
            return [PurestrainGenestealers.model.clone().set_count(self.cur - self.parent.talons.cur)]
    
    def __init__(self, parent):
        super(PurestrainGenestealers, self).__init__(parent)
        self.gens = self.Stealer(self, 'Purestrain Genestealer', min_limit=5, max_limit=20,
                                 points=get_cost(units.PurestrainGenestealers), per_model=True)
        self.talons = Count(parent=self, name='Purestrain talons', min_limit=0, max_limit=5,
                            points=get_cost(melee.PurestrainTalons),
                            gear=PurestrainGenestealers.model.clone().add(create_gears(melee.PurestrainTalons)))

    def check_rules(self):
        super(PurestrainGenestealers, self).check_rules()
        self.talons.max = self.gens.cur

    def get_count(self):
        return self.gens.cur

    def build_power(self):
        return self.power * ((self.gens.cur + 4) / 5)
