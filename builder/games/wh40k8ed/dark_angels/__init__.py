from hq import CompanyMaster, Azrael, Belial,\
    Sammael, SpeederSammael, InterrogatorChaplain, IntTerminatorChaplain, IntBikeChaplain,\
    Asmodai, Ezekiel, CodexTerminatorMaster, CodexCataphractiiMaster,\
    GravisMaster, PrimarisMaster, RavenwingTalonmaster, DACodexChaplain,\
    DATermoLibrarian, DALieutenants, DACodexTechmarine, DAPrimarisLieutenants
from troops import DATacticalSquad, DAScoutSquad
from fast import RavenwingBikeSquad, RavenwingAttackBikeSquad, RavenwingSpeeders,\
    RavenwingDarkShroud, RavenwingBlackKnights, LandSpeederVengeance, DACodexAssaultSquad,\
    DAScoutBikers
from elites import DACompanyChampion, DACompanyVeterans, DWApothecary, DWAncient,\
    DWChampion, DeathwingTerminatorSquad, DeathwingKnights, RWAncient, RWApothecary,\
    RWChampion, DACataphractiiTerminatorSquad, DATartarosTerminatorSquad, DACodexCompanyAncient,\
    DACodexDreadnought, DACodexVenDreadnought, DAChapterAncient
from fliers import RavenwingDarkTalon, NephilimJetfighter

from builder.games.wh40k8ed.sections import HQSection, ElitesSection,\
    HeavySection


class DWCommand(HQSection):
    inner_circle = [  #CompanyMaster, CodexTerminatorMaster,
                      #CodexCataphractiiMaster,
        GravisMaster,
        PrimarisMaster]

    def __init__(self, *args, **kwargs):
        super(DWCommand, self).__init__(*args, **kwargs)
        self.dw_types = []
        for cls in self.inner_circle:
            class DWCommander(cls):
                faction = ['Deathwing']

                def build_statistics(self):
                    res = super(self.__class__, self).build_statistics()
                    res['Command points'] = -1
                    res['Inner Circle'] = 1
                    return res

            self.dw_types.append(DWCommander)

    def add_classes(self, class_list):
        super(DWCommand, self).add_classes(class_list + self.dw_types)


class DWElites(ElitesSection):
    from builder.games.wh40k8ed.space_marines.elites import RedemptorDreadnought
    inner_circle = [DACodexDreadnought, DACodexVenDreadnought,
                    RedemptorDreadnought]

    def __init__(self, *args, **kwargs):
        super(DWElites, self).__init__(*args, **kwargs)
        self.dw_types = []
        for cls in self.inner_circle:
            class DWDreadnought(cls):
                faction = ['Deathwing']

                def build_statistics(self):
                    res = super(self.__class__, self).build_statistics()
                    res['Command points'] = -1
                    res['Inner Circle'] = 1
                    return res

            self.dw_types.append(DWDreadnought)

    def add_classes(self, class_list):
        super(DWElites, self).add_classes(class_list + self.dw_types)


class DWHeavy(HeavySection):
    from builder.games.wh40k8ed.space_marines.heavy import LandRaider, LandRaiderCrusader, LandRaiderRedeemer
    inner_circle = [LandRaider, LandRaiderCrusader,
                    LandRaiderRedeemer]

    def __init__(self, *args, **kwargs):
        super(DWHeavy, self).__init__(*args, **kwargs)
        self.dw_types = []
        for cls in self.inner_circle:
            class DWRaider(cls):
                faction = ['Deathwing']

                def build_statistics(self):
                    res = super(self.__class__, self).build_statistics()
                    res['Command points'] = -1
                    res['Inner Circle'] = 1
                    return res

            self.dw_types.append(DWRaider)

    def add_classes(self, class_list):
        super(DWHeavy, self).add_classes(class_list + self.dw_types)


unit_types = [CompanyMaster, Azrael, Belial, Sammael, SpeederSammael,
              InterrogatorChaplain, IntTerminatorChaplain,
              IntBikeChaplain, Asmodai, Ezekiel,
              CodexTerminatorMaster, CodexCataphractiiMaster,
              GravisMaster, PrimarisMaster, RavenwingTalonmaster,
              DATacticalSquad, DAScoutSquad, DACodexChaplain,
              DATermoLibrarian, DALieutenants, DACodexTechmarine,
              DAPrimarisLieutenants, RavenwingBikeSquad,
              RavenwingAttackBikeSquad, RavenwingSpeeders,
              RavenwingDarkShroud, RavenwingBlackKnights,
              LandSpeederVengeance, DACompanyChampion,
              DACompanyVeterans, DWApothecary, DWAncient, DWChampion,
              DeathwingTerminatorSquad, DeathwingKnights, RWAncient,
              RWApothecary, RWChampion, DACataphractiiTerminatorSquad,
              DATartarosTerminatorSquad, DACodexCompanyAncient,
              DAChapterAncient, DACodexAssaultSquad,
              DACodexDreadnought, DACodexVenDreadnought,
              RavenwingDarkTalon, NephilimJetfighter, DAScoutBikers]
