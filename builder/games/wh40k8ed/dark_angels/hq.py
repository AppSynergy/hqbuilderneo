from builder.core2 import UnitList, UnitDescription
from builder.games.wh40k8ed.unit import HqUnit, ListSubUnit
from builder.games.wh40k8ed.options import OneOf, Gear, OptionsList, Count
from builder.games.wh40k8ed.utils import get_name, get_cost, points_price, create_gears, get_costs
import armory
import ranged
import melee
import units
import wargear


class Azrael(HqUnit, armory.DAUnit):
    type_id = 'azrael_v1'
    type_name = get_name(units.Azrael)
    faction = ['Deathwing']
    keywords = ['Character', 'Infantry', 'Chapter master']
    power = 9

    def __init__(self, parent):
        super(Azrael, self).__init__(parent=parent, points=get_cost(units.Azrael), unique=True, static=True, gear=[
            Gear('Sword of Secrets'),
            Gear('Lion\'s Wrath'),
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
        ])

    def build_statistics(self):
        res = super(Azrael, self).build_statistics()
        if self.star == 2:
            res['Command points'] = 1
        return res


class Belial(HqUnit, armory.DAUnit):
    type_id = 'belial_v1'
    type_name = get_name(units.Belial)
    faction = ['Deathwing']
    keywords = ['Character', 'Infantry', 'Grand master', 'Terminator']
    power = 8

    def __init__(self, parent):
        super(Belial, self).__init__(parent=parent, points=get_cost(units.Belial), unique=True,
                                     gear=[
                                         Gear('Sword of Silence'),
                                         Gear('Storm bolter'),
                                     ]
                                     )


class Sammael(HqUnit, armory.DAUnit):
    type_id = 'sammael_v1'
    type_name = get_name(units.SammaelOnCorvex)
    faction = ['Ravenwing']
    kwname = 'Sammael'
    keywords = ['Character', 'Biker', 'Grand master', 'Fly']
    power = 10

    def __init__(self, parent):
        super(Sammael, self).__init__(parent=parent,
                                      points=get_cost(units.SammaelOnCorvex), gear=[
                Gear('Raven Sword'),
                Gear('Bolt pistol'),
                Gear('Frag grenades'),
                Gear('Krak grenades'),
                Gear('Plasma cannon'),
                Gear('Twin storm bolter')
            ],
                                      static=True)

    def get_unique(self):
        return 'SAMMAEL'


class SpeederSammael(HqUnit, armory.DAUnit):
    type_id = 'speeder_sammael_v1'
    type_name = get_name(units.SammaelInSableclaw)
    faction = ['Ravenwing']
    kwname = 'Sammael'
    keywords = ['Character', 'Vehicle', 'Grand master', 'Land Speeder', 'Fly']
    power = 11

    def __init__(self, parent):
        super(SpeederSammael, self).__init__(parent, points=get_cost(units.SammaelInSableclaw),
                                             gear=[
                                                 Gear('Raven Sword'),
                                                 Gear('Twin assault cannon'),
                                                 Gear('Twin heavy bolter'),
                                             ], static=True)

    def get_unique(self):
        return 'SAMMAEL'


class InterrogatorChaplain(HqUnit, armory.DAUnit):
    type_name = get_name(units.InterrogatorChaplain)
    type_id = 'da_chaplain_v1'

    faction = ['Deathwing']
    keywords = ['Character', 'Infantry', 'Chaplain']
    power = 5

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(InterrogatorChaplain.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.Boltgun)
            armory.add_pistol_options(self)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(InterrogatorChaplain.Pack, self).__init__(parent, 'Options')
            self.pack = self.variant('Jump pack',
                                     get_cost(units.JumpInterrogatorChaplain) - get_cost(units.InterrogatorChaplain))
            self.variant(*melee.PowerFist)

    def __init__(self, parent):
        super(InterrogatorChaplain, self).__init__(parent, points=get_cost(units.InterrogatorChaplain) + get_cost(
            melee.CroziusArcanum), gear=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Crozius Arcanum')
        ])
        self.Weapon1(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + 1 * self.pack.pack.value


class IntBikeChaplain(HqUnit, armory.DAUnit):
    type_name = u'Interrogator-Chaplain on Bike'
    type_id = 'da_bike_chaplain_v1'
    kwname = 'Interrogator-Chaplain'
    keywords = ['Character', 'Biker', 'Chaplain']
    power = 7
    faction = ['Deathwing']

    def __init__(self, parent):
        super(IntBikeChaplain, self).__init__(parent, points=117 + 2, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Crozius Arcanum'), Gear('Twin boltgun'),
        ])


class RavenwingTalonmaster(HqUnit, armory.DAUnit):
    type_id = 'ravenwing_talonmaster_v1'
    type_name = get_name(units.RavenwingTalonmaster)
    faction = ['Ravenwing']
    keywords = ['Character', 'Vehicle', 'Land Speeder', 'Fly']
    power = 9

    def __init__(self, parent):
        gear = [melee.PowerSword, ranged.TwinAssaultCannon, ranged.TwinHeavyBolter]
        super(RavenwingTalonmaster, self).__init__(parent,
                                                   points=points_price(get_cost(units.RavenwingTalonmaster), *gear),
                                                   gear=create_gears(*gear), static=True)


class IntTerminatorChaplain(HqUnit, armory.DAUnit):
    type_name = get_name(units.TDAInterrogatorChaplain)
    type_id = 'da_termo_chaplain_v1'
    faction = ['Deathwing']
    kwname = 'Interrogator-Chaplain'
    keywords = ['Character', 'Infantry', 'Terminator', 'Chaplain']
    power = 6

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(IntTerminatorChaplain.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.StormBolter)
            armory.add_term_combi_weapons(self)

    def __init__(self, parent):
        gear = [melee.CroziusArcanum]
        super(IntTerminatorChaplain, self).__init__(parent,
                                                    points=points_price(get_cost(units.TDAInterrogatorChaplain), *gear),
                                                    gear=create_gears(*gear))
        self.Weapon1(self)


class Asmodai(HqUnit, armory.DAUnit):
    type_id = 'asmodai_v1'
    type_name = get_name(units.Asmodai)
    faction = ['Deathwing']
    keywords = ['Character', 'Infantry', 'Chaplain']
    power = 7

    def __init__(self, parent):
        super(Asmodai, self).__init__(parent=parent, unique=True, static=True, gear=[
            Gear('Blades of Reason'),
            Gear('Crozius arcanum'),
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
        ], points=get_cost(units.Asmodai))


class Ezekiel(HqUnit, armory.DAUnit):
    type_id = 'ezekiel_v1'
    type_name = get_name(units.Ezekiel)
    faction = ['Deathwing']
    keywords = ['Character', 'Infantry', 'Librarian', 'Psyker']
    power = 7

    def __init__(self, parent):
        super(Ezekiel, self).__init__(parent=parent, points=get_cost(units.Ezekiel), name='Ezekiel', unique=True,
                                      static=True, gear=[
                Gear('Traitor\'s Bane'),
                Gear('The Deliverer'),
                                      ])


class DACodexChaplain(HqUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.Chaplain)
    type_id = 'da_chaplain_v1'

    keywords = ['Character', 'Infantry']
    power = 5

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DACodexChaplain.Weapon1, self).__init__(parent, 'Weapon')
            armory.add_pistol_options(self)
            self.variant(*ranged.Boltgun)
            armory.add_combi_weapons(self)
            self.variant(*melee.PowerFist)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(DACodexChaplain.Pack, self).__init__(parent, 'Pack')
            self.pack = self.variant('Jump pack', get_cost(units.JumpChaplain) -
                                     get_cost(units.Chaplain))

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, melee.CroziusArcanum]
        cost = points_price(get_cost(units.Chaplain), *gear)
        super(DACodexChaplain, self).__init__(parent, get_name(units.Chaplain),
                                              points=cost, gear=create_gears(*gear))
        self.Weapon1(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + 1 * self.pack.any


class DAPrimarisChaplain(HqUnit, armory.DAUnit):
    type_name = get_name(units.PrimarisChaplain)
    type_id = 'da_primaris_chaplain_v1'
    kwname = 'Chaplain'
    keywords = ['Character', 'Infantry', 'Primaris']
    power = 6

    def __init__(self, parent):
        gear = [melee.CroziusArcanum, ranged.AbsolvorBoltPistol, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.PrimarisChaplain), *gear)
        super(DAPrimarisChaplain, self).__init__(parent, points=cost, gear=create_gears(*gear),
                                                 static=True)


class DALibrarian(HqUnit, armory.DAUnit):
    type_name = get_name(units.Librarian)
    type_id = 'da_librarian_v1'

    keywords = ['Character', 'Infantry', 'Psyker']
    faction = ['Deathwing']
    power = 8

    armory = armory

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DALibrarian.Weapon1, self).__init__(parent, 'Pistol')
            self.parent.armory.add_pistol_options(self)
            self.variant(*ranged.Boltgun)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(DALibrarian.Weapon2, self).__init__(parent, 'Force')
            self.variant(*melee.ForceStave)
            self.variant(*melee.ForceSword)
            self.variant(*melee.ForceAxe)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(DALibrarian.Pack, self).__init__(parent, 'Pack')
            self.pack = self.variant('Jump pack', get_cost(units.JumpLibrarian) -
                                     get_cost(units.Librarian))

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.Librarian), *gear)
        super(DALibrarian, self).__init__(parent, points=cost,
                                          gear=create_gears(*gear))
        self.Weapon1(self)
        self.Weapon2(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + 1 * self.pack.any


class DAPrimarisLibrarian(HqUnit, armory.DAUnit):
    type_name = get_name(units.PrimarisLibrarian)
    type_id = 'da_primaris_librarian_v1'
    kwname = 'Librarian'
    faction = ['Deathwing']
    keywords = ['Character', 'Infantry', 'Primaris', 'Psyker']
    power = 7

    def __init__(self, parent):
        gear = [melee.ForceSword, ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.PrimarisLibrarian), *gear)
        super(DAPrimarisLibrarian, self).__init__(parent, points=cost, gear=create_gears(*gear),
                                                  static=True)


class DATermoLibrarian(HqUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.TDALibrarian) + ' (Index)'
    type_id = 'da_termo_librarian_v1'

    faction = ['Deathwing']
    keywords = ['Character', 'Infantry', 'Terminator', 'Psyker']
    power = 8

    class Weapon1(OptionsList):
        def __init__(self, parent):
            super(DATermoLibrarian.Weapon1, self).__init__(parent, 'Combi')
            armory.add_term_combi_weapons(self)

    def __init__(self, parent):
        super(DATermoLibrarian, self).__init__(parent, name=get_name(units.TDALibrarian),
                                               points=get_cost(units.TDALibrarian))
        self.Weapon1(self)
        DALibrarian.Weapon2(self)


class CompanyMaster(HqUnit, armory.DAUnit):
    type_name = get_name(units.Master)
    type_id = 'captain_v1'

    faction = ['Deathwing']

    keywords = ['Character', 'Infantry']
    power = 5

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CompanyMaster.Weapon1, self).__init__(parent, 'Weapon 1')
            self.variant('Master-crafted boltgun', 3)
            armory.add_pistol_options(self)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CompanyMaster.Weapon2, self).__init__(parent, 'Weapon2')
            armory.add_melee_weapons(self)
            self.variant('Storm shield', 15)
            self.variant('Relic blade', 21)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(CompanyMaster.Pack, self).__init__(parent, 'Pack')
            self.pack = self.variant('Jump pack', 93 - 74)

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.Master), *gear)
        super(CompanyMaster, self).__init__(parent, points=cost, gear=create_gears(*gear))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.pack = self.Pack(self)

    def build_points(self):
        res = super(CompanyMaster, self).build_points()
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -= 5
        return res

    def build_power(self):
        return self.power + 1 * self.pack.any


class CodexTerminatorMaster(HqUnit, armory.ClawUser, armory.DAUnit):
    type_name = get_name(units.TDAMaster) + ' (Codex)'
    type_id = 'da_term_company_master_v1'

    faction = ['Deathwing']

    keywords = ['Character', 'Infantry', 'Terminator']
    power = 7

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexTerminatorMaster.Weapon1, self).__init__(parent, 'Bolter')
            self.variant(*ranged.StormBolter)
            armory.add_term_combi_weapons(self)
            armory.add_term_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CodexTerminatorMaster.Weapon2, self).__init__(parent, 'Weapon')
            self.variant(*melee.RelicBlade)
            self.variant(*melee.PowerSword)
            self.variant(*melee.Chainfist)
            self.variant(*wargear.StormShieldCharacters)
            armory.add_term_melee_weapons(self)

    class Launcher(OptionsList):
        def __init__(self, parent):
            super(CodexTerminatorMaster.Launcher, self).__init__(parent, 'Launcher')
            self.variant(*ranged.WristMountedGrenadeLauncher)

    def __init__(self, parent):
        super(CodexTerminatorMaster, self).__init__(parent, get_name(units.TDAMaster),
                                                    points=get_cost(units.TDAMaster))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.launcher = self.Launcher(self)

    def check_rules(self):
        super(CodexTerminatorMaster, self).check_rules()
        fist = self.wep1.fist == self.wep1.cur or self.wep2.fist == self.wep2.cur
        self.launcher.visible = self.launcher.used = fist


class CodexCataphractiiMaster(HqUnit, armory.ClawUser, armory.DAUnit):
    type_name = get_name(units.CataphractiiMaster) + ' (Codex)'
    type_id = 'da_cataphractii_captain_v2'

    faction = ['Deathwing']

    keywords = ['Character', 'Infantry', 'Terminator']
    power = 8

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexCataphractiiMaster.Weapon1, self).__init__(parent, 'Bolter')
            self.variant(*ranged.CombiBolter)
            armory.add_term_combi_weapons(self)
            armory.add_term_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CodexCataphractiiMaster.Weapon2, self).__init__(parent, 'Weapon')
            self.variant(*melee.Chainfist)
            self.variant(*melee.PowerSword)
            self.variant(*melee.RelicBlade)
            armory.add_term_melee_weapons(self)

    def __init__(self, parent):
        super(CodexCataphractiiMaster, self).__init__(parent, get_name(units.CataphractiiMaster),
                                                      points=get_cost(units.CataphractiiMaster))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)


class GravisMaster(HqUnit, armory.DAUnit):
    type_name = get_name(units.GravisMaster)
    type_id = 'da__gravis_company_master_v1'

    keywords = ['Character', 'Infantry', 'Mk X Gravis', 'Primaris']
    power = 7

    def __init__(self, parent):
        gear = [melee.MasterCraftedPowerSword, ranged.BoltstormGauntlet]
        super(GravisMaster, self).__init__(parent,
                                           points=points_price(get_cost(units.GravisMaster), *gear),
                                           gear=create_gears(*gear), static=True)


class PrimarisMaster(HqUnit, armory.DAUnit):
    type_name = get_name(units.PrimarisMaster)
    type_id = 'da_primaris_master_v1'
    kwname = 'Captain'
    keywords = ['Character', 'Infantry', 'Primaris']
    power = 6

    class RangedWeapon(OneOf):
        def __init__(self, parent):
            super(PrimarisMaster.RangedWeapon, self).__init__(parent, 'Ranged weapon')
            self.variant(*ranged.MasterCraftedAutoBoltRifle)
            self.variant(*ranged.MasterCraftedStalkerBoltRifle)

    class Melee(OptionsList):
        def __init__(self, parent):
            super(PrimarisMaster.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.PowerSword)

    class Pistol(OneOf):
        def __init__(self, parent):
            super(PrimarisMaster.Pistol, self).__init__(parent, 'Pistol')
            self.variant(*ranged.BoltPistol)
            self.fist = self.variant('Power fist and a plasma pistol',
                                     get_cost(melee.PowerFist) + get_cost(ranged.PlasmaPistol),
                                     gear=[
                                         Gear('Power fist'), Gear('Plasma pistol')
                                     ]
                                     )

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades]
        super(PrimarisMaster, self).__init__(parent,
                                             points=points_price(armory.get_cost(units.PrimarisMaster), *gear),
                                             gear=create_gears(*gear))
        self.rng = self.RangedWeapon(self)
        self.mle = self.Melee(self)
        self.pistol = self.Pistol(self)

    def check_rules(self):
        super(PrimarisMaster, self).check_rules()
        fist = self.pistol.fist == self.pistol.cur
        self.rng.visible = self.rng.used = not fist
        self.mle.visible = self.mle.used = not fist


class DALieutenants(HqUnit, armory.DAUnit):
    type_name = get_name(units.Lieutenants)
    type_id = 'da_leutanenta_v1'
    keywords = ['Character', 'Infantry']
    power = 4

    class Leutenant(armory.ClawUser, ListSubUnit):
        type_name = 'Leutenant'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(DALieutenants.Leutenant.Weapon1, self).__init__(parent, 'Weapon 1')
                self.variant(*ranged.MasterCraftedBoltgun)
                armory.add_pistol_options(self)
                armory.add_combi_weapons(self)
                armory.add_melee_weapons(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(DALieutenants.Leutenant.Weapon2, self).__init__(parent, 'Weapon 2')
                armory.add_melee_weapons(self)

        class Pack(OptionsList):
            def __init__(self, parent):
                super(DALieutenants.Leutenant.Pack, self).__init__(parent, 'Pack')
                self.pack = self.variant('Jump pack', armory.get_cost(units.JumpLieutenants) -
                                         armory.get_cost(units.Lieutenants))

        def __init__(self, parent):
            gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol]
            cost = points_price(armory.get_cost(units.Lieutenants), *gear)
            super(DALieutenants.Leutenant, self).__init__(parent, 'Leutenant', cost,
                                                          gear=create_gears(*gear))
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.Pack(self)

    def __init__(self, parent):
        super(DALieutenants, self).__init__(parent)
        self.models = UnitList(self, self.Leutenant, 1, 2)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count


class DACodexTechmarine(HqUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.Techmarine) + ' (Codex)'
    type_id = 'da_techmarine_v1'

    keywords = ['Character', 'Infantry']
    power = 4
    armory = armory

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DACodexTechmarine.Weapon1, self).__init__(parent, 'Pistol')
            armory.add_pistol_options(self)
            self.variant(*ranged.Boltgun)
            armory.add_combi_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(DACodexTechmarine.Weapon2, self).__init__(parent, 'Weapon')
            self.variant(*melee.PowerAxe)
            armory.add_melee_weapons(self, axe=False)

    class Harness(OptionsList):
        def __init__(self, parent):
            super(DACodexTechmarine.Harness, self).__init__(parent, 'Harness')
            gear = [melee.ServoArm, ranged.Flamer, ranged.PlasmaCutter]
            self.harness = self.variant('Servo-harness', get_costs(*gear),
                                        gear=create_gears(*gear))

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, melee.ServoArm]
        cost = points_price(armory.get_cost(units.Techmarine), *gear)
        super(DACodexTechmarine, self).__init__(parent, name=get_name(units.Techmarine),
                                                points=cost,
                                                gear=create_gears(*gear))
        self.Weapon1(self)
        self.Weapon2(self)
        self.harness = self.Harness(self)


class DAPrimarisLieutenants(HqUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.PrimarisLieutenants)
    type_id = 'da_primaris_lieutenants_v1'

    keywords = ['Character', 'Infantry', 'Primaris', 'Lieutenants']

    power = 5

    class Leutenant(ListSubUnit):
        type_name = 'Leutenant'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(DAPrimarisLieutenants.Leutenant.Weapon1, self).__init__(parent, 'Weapon 1')
                self.variant(*ranged.MasterCraftedAutoBoltRifle)
                self.variant(*melee.PowerSword)
                self.variant(*ranged.MasterCraftedStalkerBoltRifle)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(DAPrimarisLieutenants.Leutenant.Weapon2, self).__init__(parent, 'Weapon 2')
                self.variant(*ranged.BoltPistol)
                self.plasma_pistol = self.variant(*ranged.PlasmaPistol)

        class Plasma(OptionsList):
            def __init__(self, parent):
                super(DAPrimarisLieutenants.Leutenant.Plasma, self).__init__(parent, 'Pistol')
                self.plasma_pistol = self.variant(*ranged.PlasmaPistol)

        def __init__(self, parent):
            gear = [ranged.FragGrenades, ranged.KrakGrenades]
            cost = points_price(armory.get_cost(units.PrimarisLieutenants), *gear)
            super(DAPrimarisLieutenants.Leutenant, self).__init__(parent, 'Leutenant', cost,
                                                                  gear=create_gears(*gear))
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.plasma = self.Plasma(self)

        def check_rules(self):
            super(DAPrimarisLieutenants.Leutenant, self).check_rules()
            add_plasma = self.plasma.any
            # change_plasma = self.wep2.plasma_pistol = self.wep2.cur
            self.wep2.plasma_pistol.active = not add_plasma
            # self.plasma.visible = self.plasma.used = not change_plasma

    def __init__(self, parent):
        super(DAPrimarisLieutenants, self).__init__(parent, name=get_name(units.PrimarisLieutenants))
        self.models = UnitList(self, self.Leutenant, 1, 2)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count
