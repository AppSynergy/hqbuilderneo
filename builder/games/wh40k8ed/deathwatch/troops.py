from builder.games.wh40k8ed.space_marines.troops import Intercessors
from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, \
    ListSubUnit, UnitList, SubUnit, OptionalSubUnit
import armory
import melee
import ranged
import units
import wargear
from builder.games.wh40k8ed.utils import *


class Veteran(ListSubUnit):
    type_name = u'Deathwatch Veteran'
    model_points = 19
    power = 2

    class Shield(OptionsList):
        def __init__(self, parent):
            super(Veteran.Shield, self).__init__(parent, '')
            self.variant('Combat shield', 4)

    class RangedWeapon(OneOf):
        def __init__(self, parent):
            super(Veteran.RangedWeapon, self).__init__(parent, 'Weapon 1')
            armory.add_single(self)
            armory.add_double(self)
            armory.add_heavy_weapons(self)
            self.hth = self.variant('Heavy thunder hammer', 30)

    class MeleeWeapon(OptionsList):
        def __init__(self, parent):
            super(Veteran.MeleeWeapon, self).__init__(parent, 'Weapon 2', limit=1)
            armory.add_double(self)
            armory.add_single(self)

    class Upgrades(OptionsList):
        def __init__(self, parent):
            super(Veteran.Upgrades, self).__init__(parent, 'Upgrade to', limit=1)
            self.black = self.variant('Black Shield', gear=[])
            self.sarge = self.variant('Watch Sergeant', gear=[])

    def __init__(self, parent):
        super(Veteran, self).__init__(
            parent=parent, points=self.model_points,
            gear=[Gear('Frag grenades'), Gear('Krak renades')])
        self.rng = self.RangedWeapon(self)
        self.mle = self.MeleeWeapon(self)
        self.shield = self.Shield(self)
        self.up = self.Upgrades(self)

    def check_rules(self):
        super(Veteran, self).check_rules()
        for o in self.rng.heavy + [self.rng.hth]:
            o.active = not self.up.any
        self.mle.used = self.mle.visible = self.rng.cur not in self.rng.heavy + [self.rng.hth]
        # only sergeant uses special wargear or xenoblade
        self.shield.used = self.shield.visible = self.up.sarge.value
        self.mle.xeno.used = self.mle.xeno.visible = self.rng.xeno.used = self.rng.xeno.visible = self.up.sarge.value
        dflag = self.mle.used and any(op.value for op in self.mle.single)
        for op in self.rng.single:
            op.active = not dflag

    def build_description(self):
        res = super(Veteran, self).build_description()
        for vrt in [self.up.black, self.up.sarge]:
            if vrt.value:
                res.name = vrt.title
        return res

    def build_points(self):
        res = super(Veteran, self).build_points()
        if self.rng.cur == self.rng.claw and self.mle.cur == self.mle.claw:
            res -= 5
        return res

    @ListSubUnit.count_gear
    def has_heavy(self):
        return self.rng.cur in self.rng.heavy

    @ListSubUnit.count_gear
    def is_black(self):
        return self.up.black.value

    @ListSubUnit.count_gear
    def is_sarge(self):
        return self.up.sarge.value

    # @ListSubUnit.count_gear
    # def has_furor_weapon(self):
    #     return self.rng.has_furor()

    # @ListSubUnit.count_gear
    # def has_hammer(self):
    #     return self.mle. is_hammer() or\
    #         (self.ham.used and self.ham.any)

    # @ListSubUnit.count_gear
    # def has_stalker(self):
    #     return self.rng.is_stalker()


class Terminator(ListSubUnit):
    type_name = u'Deathwatch Terminator'
    model_points = 32
    power = 3

    class Ranged(OneOf):
        def __init__(self, parent):
            super(Terminator.Ranged, self).__init__(parent, 'Ranged weapon')
            self.bolt = self.variant('Storm bolter', 2)
            armory.add_term_heavy_weapons(self)

    class Melee(OneOf):
        def __init__(self, parent, sword=True):
            super(Terminator.Melee, self).__init__(parent, 'Melee weapon')
            if sword:
                self.variant('Power sword', 4)
            else:
                self.variant('Power fist', 20)
            armory.add_term_melee_weapons(self)
            self.double = [self.variant('Two lightning claws', 13, gear=[Gear('Lightning claw', count=2)]),
                           self.variant('Thunder hammer and storm shield', 20 + 5, gear=[
                               Gear('Thunder hammer'), Gear('Storm shield')
                           ])]

    def __init__(self, parent):
        super(Terminator, self).__init__(parent, points=self.model_points)
        self.rng = self.Ranged(self)
        self.mle = self.Melee(self, sword=False)

    def check_rules(self):
        super(Terminator, self).check_rules()
        self.rng.used = self.rng.visible = self.mle.cur not in self.mle.double

    @ListSubUnit.count_gear
    def has_heavy(self):
        return self.rng.used and (self.rng.cur != self.rng.bolt)


class Vanguard(ListSubUnit):
    type_name = u'Deathwatch Vanguard Veteran'
    model_points = 21
    power = 2

    class Melee(OneOf):
        def __init__(self, parent):
            super(Vanguard.Melee, self).__init__(parent, 'Weapon')
            armory.add_vanguard_weapons(self, bolt=False)
            self.hth = self.variant('Heavy thunder hammer', 30)

    class Ranged(OneOf):
        def __init__(self, parent):
            super(Vanguard.Ranged, self).__init__(parent, '')
            armory.add_vanguard_weapons(self)

    def __init__(self, parent):
        super(Vanguard, self).__init__(parent, points=self.model_points,
                                       gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        self.mle = self.Melee(self)
        self.rng = self.Ranged(self)

    def check_rules(self):
        super(Vanguard, self).check_rules()
        self.rng.used = self.rng.visible = not self.mle.cur == self.mle.hth

    def build_points(self):
        res = super(Vanguard, self).build_points()
        if self.rng.cur == self.rng.claw and self.mle.cur == self.mle.claw:
            res -= 5
        return res


class Biker(ListSubUnit):
    type_name = u'Deathwatch Biker'
    model_points = 34 + 2
    power = 2

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Biker.Weapon, self).__init__(parent, 'Weapon', limit=1)
            self.variant('Power axe', 5)
            self.variant('Power maul', 4)
            self.variant('Power sword', 4)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Biker.Options, self).__init__(parent, 'Options')
            self.variant('Teleport homer')

    def __init__(self, parent):
        super(Biker, self).__init__(parent, points=self.model_points,
                                    gear=[Gear('Frag grenades'), Gear('Krak grenades'),
                                          Gear('Twin boltgun')])
        self.Weapon(self)
        self.opt = self.Options(self)

    @ListSubUnit.count_gear
    def has_homer(self):
        return self.opt.any


class KillTeam(TroopsUnit, armory.DeathwatchUnit):
    type_name = 'Deathwatch Kill team (Index)'
    type_id = 'dw_kill_team_v1'
    keywords = ['Infantry']
    obsolete = True

    class Optionals(OptionalSubUnit):
        def __init__(self, parent):
            super(KillTeam.Optionals, self).__init__(parent, '')
            self.t = UnitList(self, Terminator, 1, 5)
            self.v = UnitList(self, Vanguard, 1, 5)
            self.b = UnitList(self, Biker, 1, 5)

    def __init__(self, parent):
        super(KillTeam, self).__init__(parent)
        self.vets = UnitList(self, Veteran, 5, 10)
        self.other = self.Optionals(self)

    def get_count(self):
        return self.vets.count + self.other.count

    def check_rules(self):
        super(KillTeam, self).check_rules()
        if self.get_count() > 10:
            self.error('Deathwatch Kill Team may include no more then 10 models; taken: {}'.format(self.get_count()))
        sc = sum(v.is_sarge() for v in self.vets.units)
        if sc > 1:
            self.error('Watch Sergeant may only take place of 1 Deathwatch Veteran')
        bc = sum(v.is_black() for v in self.vets.units)
        if bc > 1:
            self.error('Black Shield may only take place of 1 Deathwatch Veteran')
        hc = sum(v.has_heavy() for v in self.vets.units)
        if hc > 4:
            self.error('Up to 4 Deathwatch Veterans may take heavy weapons; taken: {}'.format(hc))
        if self.other.t.visible:
            thc = sum(t.has_heavy() for t in self.other.t.units)
            if thc > 3:
                self.error('Up to 3 Deathwatch Terminators may take heavy weapons; taken: {}'.format(thc))
        if self.other.b.visible:
            hbc = sum(b.has_homer() for b in self.other.b.units)
            if hbc > 1:
                self.error('Only one Deathwatch Biker may take teleport homer; taken: {}'.format(hbc))

    def build_power(self):
        return 9 + 2 * (self.vets.count - 5) + 3 * self.other.t.visible * self.other.t.count + \
               2 * self.other.v.visible * self.other.v.count + 2 * self.other.b.visible * self.other.v.count


class DWIntercessors(Intercessors):
    faction_substitution = {'Deathwatch': '<Chapter>'}
    type_name = u'Deathwatch Intercessor Squad (Index)'
    type_id = 'dw_intercessors_v1'
    swords = [melee.Chainsword, melee.PowerSword]
    obsolete = True

    @classmethod
    def calc_faction(cls):
        return []


class CodexVeteran(ListSubUnit):
    type_name = u'Deathwatch Veteran'
    model_points = get_cost(units.Veterans)
    power = 2

    class Shield(OptionsList):
        def __init__(self, parent):
            super(CodexVeteran.Shield, self).__init__(parent, '')
            self.variant(*wargear.CombatShield)

    class RangedWeapon(OneOf):
        def __init__(self, parent):
            super(CodexVeteran.RangedWeapon, self).__init__(parent, 'Weapon 1')
            armory.add_single(self)
            armory.add_double(self, sergant=True)
            armory.add_heavy_weapons(self)
            self.hth = self.variant(*melee.HeavyThunderHammer)

    class MeleeWeapon(OptionsList):
        def __init__(self, parent):
            super(CodexVeteran.MeleeWeapon, self).__init__(parent, 'Weapon 2', limit=1)
            armory.add_double(self, sergant=True)
            armory.add_single(self)

    class Upgrades(OptionsList):
        def __init__(self, parent):
            super(CodexVeteran.Upgrades, self).__init__(parent, 'Upgrade to', limit=1)
            self.black = self.variant('Black Shield', gear=[])
            self.sarge = self.variant('Watch Sergeant', gear=[])

    def __init__(self, parent):
        super(CodexVeteran, self).__init__(
            parent=parent, points=self.model_points,
            gear=[Gear('Frag grenades'), Gear('Krak renades')])
        self.rng = self.RangedWeapon(self)
        self.mle = self.MeleeWeapon(self)
        self.shield = self.Shield(self)
        self.up = self.Upgrades(self)

    def check_rules(self):
        super(CodexVeteran, self).check_rules()
        for o in self.rng.heavy + [self.rng.hth]:
            o.active = not self.up.any
        self.mle.used = self.mle.visible = self.rng.cur not in self.rng.heavy + [self.rng.hth]
        # only sergeant uses special wargear or xenoblade
        self.shield.used = self.shield.visible = self.up.sarge.value
        self.mle.xeno.used = self.mle.xeno.visible = self.rng.xeno.used = self.rng.xeno.visible = self.up.sarge.value
        dflag = self.mle.used and any(op.value for op in self.mle.single)
        for op in self.rng.single:
            op.active = not dflag

    def build_description(self):
        res = super(CodexVeteran, self).build_description()
        for vrt in [self.up.black, self.up.sarge]:
            if vrt.value:
                res.name = vrt.title
        return res

    def build_points(self):
        res = super(CodexVeteran, self).build_points()
        if self.rng.cur == self.rng.claw and self.mle.cur == self.mle.claw:
            res -= 5
        return res

    @ListSubUnit.count_gear
    def has_heavy(self):
        return self.rng.cur in self.rng.heavy

    @ListSubUnit.count_gear
    def is_black(self):
        return self.up.black.value

    @ListSubUnit.count_gear
    def is_sarge(self):
        return self.up.sarge.value


class CodexTerminator(ListSubUnit):
    type_name = u'Deathwatch Terminator'
    model_points = get_cost(units.KillTeamTerminators)
    power = 3

    class Ranged(OneOf):
        def __init__(self, parent):
            super(CodexTerminator.Ranged, self).__init__(parent, 'Ranged weapon')
            self.bolt = self.variant(*ranged.StormBolter)
            armory.add_term_heavy_weapons(self)

    class Melee(OneOf):
        def __init__(self, parent, sword=True):
            super(CodexTerminator.Melee, self).__init__(parent, 'Melee weapon')
            if sword:
                self.variant(*melee.PowerSword)
            else:
                self.variant(*melee.PowerFist)
            armory.add_term_melee_weapons(self)
            self.double = [self.variant('Two lightning claws', get_cost(melee.PairLightningClaws),
                                        gear=[Gear('Lightning claw', count=2)]),
                           self.variant('Thunder hammer and storm shield',
                                        get_cost(melee.ThunderHammer) + get_cost(wargear.StormShield), gear=[
                                   Gear('Thunder hammer'), Gear('Storm shield')
                               ])]

    def __init__(self, parent):
        super(CodexTerminator, self).__init__(parent, points=self.model_points)
        self.rng = self.Ranged(self)
        self.mle = self.Melee(self, sword=False)

    def check_rules(self):
        super(CodexTerminator, self).check_rules()
        self.rng.used = self.rng.visible = self.mle.cur not in self.mle.double

    @ListSubUnit.count_gear
    def has_heavy(self):
        return self.rng.used and (self.rng.cur != self.rng.bolt)


class CodexVanguard(ListSubUnit):
    type_name = u'Deathwatch Vanguard Veteran'
    model_points = get_cost(units.KillTeamVanguardVeterans)
    power = 2

    class Melee(OneOf):
        def __init__(self, parent):
            super(CodexVanguard.Melee, self).__init__(parent, 'Weapon')
            armory.add_vanguard_weapons(self, bolt=False)
            self.hth = self.variant(*melee.HeavyThunderHammer)

    class Ranged(OneOf):
        def __init__(self, parent):
            super(CodexVanguard.Ranged, self).__init__(parent, '')
            armory.add_vanguard_weapons(self)

    def __init__(self, parent):
        super(CodexVanguard, self).__init__(parent, points=self.model_points,
                                            gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        self.mle = self.Melee(self)
        self.rng = self.Ranged(self)

    def check_rules(self):
        super(CodexVanguard, self).check_rules()
        self.rng.used = self.rng.visible = not self.mle.cur == self.mle.hth

    def build_points(self):
        res = super(CodexVanguard, self).build_points()
        if self.rng.cur == self.rng.claw and self.mle.cur == self.mle.claw:
            res -= 5
        return res


class CodexBiker(ListSubUnit):
    type_name = u'Deathwatch Biker'
    model_points = get_cost(units.KillTeamBikers) + get_cost(ranged.TwinBoltgun)
    power = 2

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(CodexBiker.Weapon, self).__init__(parent, 'Weapon', limit=1)
            self.variant(*ranged.BoltPistol)
            self.variant(*melee.Chainsword)
            self.variant(*melee.PowerAxe)
            self.variant(*melee.PowerMaul)
            self.variant(*melee.PowerSword)

    class Options(OptionsList):
        def __init__(self, parent):
            super(CodexBiker.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.DeathwatchTeleportHomer)

    def __init__(self, parent):
        super(CodexBiker, self).__init__(parent, points=self.model_points,
                                         gear=[Gear('Frag grenades'), Gear('Krak grenades'),
                                               Gear('Twin boltgun')])
        self.Weapon(self)
        self.opt = self.Options(self)

    @ListSubUnit.count_gear
    def has_homer(self):
        return self.opt.any


class VeteranKillTeam(TroopsUnit, armory.DeathwatchUnit):
    type_name = 'Veterans'
    type_id = 'dw_kill_team_v2'
    keywords = ['Infantry']

    class Optionals(OptionalSubUnit):
        def __init__(self, parent):
            super(VeteranKillTeam.Optionals, self).__init__(parent, '')
            self.t = UnitList(self, CodexTerminator, 1, 5)
            self.v = UnitList(self, CodexVanguard, 1, 5)
            self.b = UnitList(self, CodexBiker, 1, 5)

    def __init__(self, parent):
        super(VeteranKillTeam, self).__init__(parent)
        self.vets = UnitList(self, CodexVeteran, 5, 10)
        self.other = self.Optionals(self)

    def get_count(self):
        return self.vets.count + self.other.count

    def check_rules(self):
        super(VeteranKillTeam, self).check_rules()
        if self.get_count() > 10:
            self.error('Deathwatch Veterans may include no more then 10 models; taken: {}'.format(self.get_count()))
        sc = sum(v.is_sarge() for v in self.vets.units)
        if sc > 1:
            self.error('Watch Sergeant may only take place of 1 Deathwatch Veteran')
        bc = sum(v.is_black() for v in self.vets.units)
        if bc > 1:
            self.error('Black Shield may only take place of 1 Deathwatch Veteran')
        hc = sum(v.has_heavy() for v in self.vets.units)
        if hc > 4:
            self.error('Up to 4 Deathwatch Veterans may take heavy weapons; taken: {}'.format(hc))
        if self.other.t.visible:
            thc = sum(t.has_heavy() for t in self.other.t.units)
            if thc > 3:
                self.error('Up to 3 Deathwatch Terminators may take heavy weapons; taken: {}'.format(thc))
        if self.other.b.visible:
            hbc = sum(b.has_homer() for b in self.other.b.units)
            if hbc > 1:
                self.error('Only one Deathwatch Biker may take teleport homer; taken: {}'.format(hbc))

    def build_power(self):
        return 9 + 2 * (self.vets.count - 5) + 3 * self.other.t.visible * self.other.t.count + \
               2 * self.other.v.visible * self.other.v.count + 2 * self.other.b.visible * self.other.v.count


class CodexIntercessor(ListSubUnit):
    type_name = u'Intercessor'
    model_points = get_cost(units.Intercessors) + get_cost(ranged.BoltPistol)
    power = 1

    class RangedWeapon(OneOf):
        def __init__(self, parent):
            super(CodexIntercessor.RangedWeapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.BoltRifle)
            self.variant(*ranged.AutoBoltRifle)
            self.variant(*ranged.StalkerBoltRifle)

    class Grenade(OptionsList):
        def __init__(self, parent):
            super(CodexIntercessor.Grenade, self).__init__(parent, 'Auxilary weapon')
            self.variant(*wargear.AuxiliaryGrenadeLauncher)

    def __init__(self, parent):
        super(CodexIntercessor, self).__init__(
            parent=parent, points=self.model_points,
            gear=[Gear('Frag grenades'), Gear('Krak renades')])
        self.rng = self.RangedWeapon(self)
        self.gl = self.Grenade(self)

    @ListSubUnit.count_gear
    def has_grenade_launcher(self):
        return self.gl.any
    


class CodexIntercessorSergeant(Unit):
    type_name = u'Intercessor Sergeant'
    model_points = get_cost(units.Intercessors) + get_cost(ranged.BoltPistol)
    power = 1

    class Ranged(CodexIntercessor.RangedWeapon):
        def __init__(self, parent):
            super(CodexIntercessorSergeant.Ranged, self).__init__(parent)
            self.swords = [self.variant(*melee.Chainsword),
                           self.variant(*melee.PowerSword)]

        def is_sword(self):
            return self.cur in self.swords

    class Melee(OptionsList):
        def __init__(self, parent):
            super(CodexIntercessorSergeant.Melee, self).__init__(parent, 'Melee weapon', limit=1)
            self.variant(*melee.Chainsword)
            self.variant(*melee.PowerSword)

    def __init__(self, parent):
        super(CodexIntercessorSergeant, self).__init__(
            parent=parent, points=self.model_points,
            gear=[Gear('Frag grenades'), Gear('Krak renades')])
        self.rng = self.Ranged(self)
        self.mle = self.Melee(self)

    def check_rules(self):
        super(CodexIntercessorSergeant, self).check_rules()
        self.mle.used = self.mle.visible = not self.rng.is_sword()


class CodexHellblaster(ListSubUnit):
    type_name = u'Hellblaster'
    model_points = get_cost(units.Hellblasters) + get_cost(ranged.BoltPistol)
    power = 2

    class RangedWeapon(OneOf):
        def __init__(self, parent):
            super(CodexHellblaster.RangedWeapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.PlasmaIncinerator)
            self.variant(*ranged.AssaultPlasmaIncinerator)
            self.variant(*ranged.HeavyPlasmaIncinerator)

    def __init__(self, parent):
        super(CodexHellblaster, self).__init__(
            parent=parent, points=self.model_points,
            gear=[Gear('Frag grenades'), Gear('Krak renades')])
        self.rng = self.RangedWeapon(self)


class CodexInceptor(ListSubUnit):
    type_name = u'Inceptor'
    model_points = get_cost(units.Inceptors)
    power = 3

    class RangedWeapon(OneOf):
        def __init__(self, parent):
            super(CodexInceptor.RangedWeapon, self).__init__(parent, 'Weapon')
            self.variant('Two assault bolters', 2 * get_cost(ranged.AssaultBolter),
                         gear=create_gears(ranged.AssaultBolter) * 2)
            self.variant('Two plasma exterminators', 2 * get_cost(ranged.PlasmaExterminator),
                         gear=create_gears(ranged.PlasmaExterminator) * 2)

    def __init__(self, parent):
        super(CodexInceptor, self).__init__(
            parent=parent, points=self.model_points)
        self.rng = self.RangedWeapon(self)


class CodexReiver(ListSubUnit):
    type_name = u'Reiver'
    model_points = get_cost(units.Reivers) + get_cost(ranged.HeavyBoltPistol)
    power = 1

    class Weapon(OneOf):
        def __init__(self, parent):
            super(CodexReiver.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.BoltCarbine)
            self.variant(*melee.CombatKnife)

    class Options(OptionsList):
        def __init__(self, parent):
            super(CodexReiver.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.GrapnelLauncher)

    def __init__(self, parent):
        super(CodexReiver, self).__init__(
            parent=parent, points=self.model_points,
            gear=[Gear('Frag grenades'), Gear('Krak renades'), Gear('Shock grenades')])
        self.rng = self.Weapon(self)
        self.gl = self.Options(self)


class CodexAggressor(ListSubUnit):
    type_name = u'Aggressor'
    model_points = get_cost(units.Aggressors)
    power = 3

    class RangedWeapon(OneOf):
        def __init__(self, parent):
            super(CodexAggressor.RangedWeapon, self).__init__(parent, 'Weapon')
            self.variant(join_and(ranged.AutoBoltstormGauntlets, ranged.FragstormGrenadeLauncher),
                         get_costs(ranged.AutoBoltstormGauntlets, ranged.FragstormGrenadeLauncher),
                         gear=create_gears(ranged.AutoBoltstormGauntlets, ranged.FragstormGrenadeLauncher))
            self.variant(*ranged.FlamestormGauntlets)

    def __init__(self, parent):
        super(CodexAggressor, self).__init__(
            parent=parent, points=self.model_points)
        self.rng = self.RangedWeapon(self)


class IntercessorKillTeam(TroopsUnit, armory.DeathwatchUnit):
    type_name = 'Intercessors'
    type_id = 'dw_intercessors_v2'
    keywords = ['Infantry']

    class Optionals(OptionalSubUnit):
        def __init__(self, parent):
            super(IntercessorKillTeam.Optionals, self).__init__(parent, '')
            self.h = UnitList(self, CodexHellblaster, 1, 5)
            self.i = UnitList(self, CodexInceptor, 1, 5)
            self.r = UnitList(self, CodexReiver, 1, 5)
            self.a = UnitList(self, CodexAggressor, 1, 5)

    def __init__(self, parent):
        super(IntercessorKillTeam, self).__init__(parent)
        self.sarge = SubUnit(self, CodexIntercessorSergeant(self))
        self.troopers = UnitList(self, CodexIntercessor, 4, 9)
        self.other = self.Optionals(self)

    def get_count(self):
        return 1 + self.troopers.count + self.other.count

    def check_rules(self):
        super(IntercessorKillTeam, self).check_rules()
        if self.get_count() > 10:
            self.error('Intercessors may include no more then 10 models; taken: {}'.format(self.get_count()))
        gc = sum(v.has_grenade_launcher() for v in self.troopers.units)
        if gc > (1 + self.troopers.count) / 5:
            self.error('Only 1 in five Intercessors may take grenade launcher; taken: {}'.format(gc))

    def build_power(self):
        return 1 + self.CodexIntercessor.power * self.troopers.count +\
            CodexReiver.power * self.other.r.count + CodexHellblaster.power * self.other.h.count +\
            CodexAggressor.power * self.other.a.count + CodexInceptor.power * self.other.i.count
