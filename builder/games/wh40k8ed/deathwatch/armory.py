import melee
import ranged
import wargear
from builder.core2 import Gear
from builder.games.wh40k8ed.space_marines.armory import AstartesUnit
from builder.games.wh40k8ed.utils import *


def add_pistol_options(obj):
    obj.variant(*ranged.BoltPistol)
    obj.variant(*ranged.GravPistol)
    obj.variant(*ranged.HandFlamer)
    obj.variant(*ranged.InfernoPistol)
    obj.variant(*ranged.PlasmaPistol)


def add_combi_weapons(obj):
    obj.combi = [
        obj.variant(*ranged.CombiFlamer),
        obj.variant(*ranged.CombiGrav),
        obj.variant(*ranged.CombiMelta),
        obj.variant(*ranged.CombiPlasma),
        obj.variant(*ranged.StormBolter), ]


def add_single(obj):
    obj.single = [
        obj.variant(*ranged.Boltgun),
        obj.variant(*ranged.CombiFlamer),
        obj.variant(*ranged.CombiGrav),
        obj.variant(*ranged.CombiMelta),
        obj.variant(*ranged.CombiPlasma),
        obj.variant(*ranged.DeathwatchShotgun),
        obj.variant(*ranged.Flamer),
        obj.variant(*ranged.GravGun),
        obj.variant(*ranged.Meltagun),
        obj.variant(*ranged.PlasmaGun),
        obj.variant(*ranged.StalkerPatternBoltgun),
        obj.variant(*ranged.StormBolter)
    ]


def add_double(obj, sergant=False):
    obj.double = [
        obj.variant(*ranged.BoltPistol),
        obj.variant(*melee.Chainsword),
        obj.variant(*ranged.GravPistol),
        obj.variant(*ranged.InfernoPistol),
        obj.variant(*ranged.HandFlamer)]
    obj.claw = obj.variant(*melee.SingleLightningClaw)
    obj.double += [obj.claw,
                   obj.variant(*ranged.PlasmaPistol),
                   obj.variant(*melee.PowerAxe),
                   obj.variant(*melee.PowerFist),
                   obj.variant('Power lance', 4),
                   obj.variant(*melee.PowerMaul),
                   obj.variant(*melee.PowerSword)]
    if 'CHARACTER' in obj.parent.keywords:
        rest = [obj.variant(*wargear.CharacterStormShield),
                obj.variant(*melee.CharacterThunderHammer)]
    else:
        rest = [obj.variant(*wargear.StormShield),
                obj.variant(*melee.ThunderHammer)]
    if sergant:
        obj.xeno = obj.variant(*melee.XenophaseBlade)
        obj.double += rest + [obj.xeno]


def add_heavy_weapons(obj):
    obj.heavy = [
        obj.variant(*ranged.DeathwatchFragCannon),
        obj.variant(*ranged.HeavyBolter),
        obj.variant(*ranged.HeavyFlamer),
        obj.variant(*ranged.InfernusHeavyBolter),
        obj.variant(*ranged.MissileLauncher)]

def add_term_heavy_weapons(obj):
    obj.variant(*ranged.AssaultCannon)
    obj.variant(join_and(ranged.CycloneMissileLauncher, ranged.StormBolter),
                get_costs(ranged.CycloneMissileLauncher, ranged.StormBolter),
                gear=create_gears(ranged.CycloneMissileLauncher, ranged.StormBolter))
    obj.variant(*ranged.HeavyFlamer)

def add_vanguard_weapons(obj, bolt=True):
    if bolt:
        obj.variant(*ranged.BoltPistol)
    obj.variant(*melee.Chainsword)
    obj.variant(*ranged.GravPistol)
    obj.variant(*ranged.HandFlamer)
    obj.variant(*ranged.InfernoPistol)
    obj.claw = obj.variant(*melee.SingleLightningClaw)
    obj.variant(*ranged.PlasmaPistol)
    obj.variant(*melee.PowerAxe)
    obj.variant(*melee.PowerFist)
    obj.variant('Power lance', 4)
    obj.variant(*melee.PowerMaul)
    obj.variant(*melee.PowerSword)
    obj.variant(*wargear.StormShield)
    obj.variant(*melee.ThunderHammer)


def add_term_melee_weapons(obj, sword=True):
    obj.variant(*melee.Chainfist)
    obj.variant(*melee.PowerAxe)
    obj.variant('Power lance', 4)
    obj.variant(*melee.PowerAxe)
    if sword:
        obj.variant(*melee.PowerSword)
    obj.variant('Power fist & meltagun', get_costs(melee.PowerFist, ranged.Meltagun), gear=[
        Gear('Power fist'), Gear('Meltagun')
    ])


def add_dred_heavy_weapons(obj):
    obj.variant(*ranged.TwinLascannon)
    obj.variant(*ranged.AssaultCannon)
    obj.variant(*ranged.HeavyPlasmaCannon)


class DeathwatchUnit(AstartesUnit):
    faction = ['Deathwatch']
    wiki_faction = 'Deathwatch'


class ClawUser(DeathwatchUnit):
    def build_points(self):
        res = super(ClawUser, self).build_points()
        # noinspection PyUnresolvedReferences
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -= get_cost(melee.PairightningClaws) - get_cost(melee.SingleLightningClaw)
        return res
