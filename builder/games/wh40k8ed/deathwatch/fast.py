import armory
import ranged
import units
from builder.games.wh40k8ed.deathwatch.troops import Biker
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, SubUnit, UnitList
from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.utils import get_cost, get_name, create_gears
from builder.games.wh40k8ed.utils import get_costs


class DWBikers(FastUnit, armory.DeathwatchUnit):
    type_name = u'Deathwatch Bikers (Index)'
    type_id = 'dwatch_bikers_v1'
    kwname = 'Bikers'
    obsolete = True
    power = 5

    class Sergeant(Unit):
        type_name = u'Deathwatch Biker Sergeant'

        def __init__(self, parent):
            super(DWBikers.Sergeant, self).__init__(parent, points=Biker.model_points,
                                                    gear=[Gear('Frag grenades'), Gear('Krak grenades'),
                                                          Gear('Twin boltgun')])
            Biker.Weapon(self)

    def __init__(self, parent):
        super(DWBikers, self).__init__(parent)
        SubUnit(self, self.Sergeant(parent=self))
        self.models = UnitList(self, Biker, 2, 5)

    def get_count(self):
        return 1 + self.models.count

    def check_rules(self):
        super(DWBikers, self).check_rules()
        hbc = sum(b.has_homer() for b in self.models.units)
        if hbc > 1:
            self.error('Only one Deathwatch Biker may take teleport homer; taken: {}'.format(hbc))

    def build_power(self):
        return self.power * (1 + (self.models.count > 2))


class CodexDWBikers(FastUnit, armory.DeathwatchUnit):
    type_name = u'Deathwatch Bikers (Codex)'
    type_id = 'dwatch_bikers_v2'
    kwname = 'Bikers'
    power = 5
    model_points = get_cost(units.Bikers)

    class Sergeant(Unit):
        type_name = u'Deathwatch Biker Sergeant'

        def __init__(self, parent):
            super(CodexDWBikers.Sergeant, self).__init__(parent, points=CodexDWBikers.model_points,
                                                         gear=[Gear('Frag grenades'), Gear('Krak grenades'),
                                                               Gear('Twin boltgun')])
            Biker.Weapon(self)

    def __init__(self, parent):
        super(CodexDWBikers, self).__init__(parent)
        SubUnit(self, self.Sergeant(parent=self))
        self.models = UnitList(self, Biker, 2, 5)

    def get_count(self):
        return 1 + self.models.count

    def check_rules(self):
        super(CodexDWBikers, self).check_rules()
        hbc = sum(b.has_homer() for b in self.models.units)
        if hbc > 1:
            self.error('Only one Deathwatch Biker may take teleport homer; taken: {}'.format(hbc))

    def build_power(self):
        return self.power * (1 + (self.models.count > 2))


class DWInceptors(FastUnit, armory.DeathwatchUnit):
    type_name = get_name(units.Inceptors) + ' (Codex)'
    type_id = 'dwatch_inceptors_v1'

    keywords = ['Infantry', 'Primaris', 'Jump pack', 'Mk X gravis', 'Fly']

    power = 10

    class Weapons(OneOf):
        def __init__(self, parent):
            super(DWInceptors.Weapons, self).__init__(parent, 'Weapons')
            self.variant('Two assault bolters', get_costs(*[ranged.AssaultBolter] * 2),
                         gear=create_gears(*[ranged.AssaultBolter] * 2))
            self.variant('Two plasma exterminators', get_costs(*[ranged.PlasmaExterminator] * 2),
                         gear=create_gears(*[ranged.PlasmaExterminator] * 2))

        @property
        def description(self):
            return [UnitDescription('Inceptor Sergeant').add(self.cur.gear)]

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Inceptor').add(self.parent.wep.cur.gear).set_count(self.cur)]

    def __init__(self, parent):
        super(DWInceptors, self).__init__(parent, points=armory.get_cost(units.Inceptors))
        self.wep = self.Weapons(self)
        self.models = self.Marines(self, 'Inceptors', 2, 5, points=armory.get_cost(units.Inceptors),
                                   per_model=True)

    def get_count(self):
        return 1 + self.models.cur

    def build_points(self):
        return super(DWInceptors, self).build_points() + self.wep.points * self.models.cur

    def build_power(self):
        return self.power * (1 + (self.models.cur > 2))
