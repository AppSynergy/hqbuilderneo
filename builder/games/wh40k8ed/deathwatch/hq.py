import armory
import melee
import ranged
import units
import wargear
from builder.games.wh40k8ed.options import OneOf, Gear, OptionsList
from builder.games.wh40k8ed.space_marines.old_armory import add_term_combi_weapons
from builder.games.wh40k8ed.unit import HqUnit
from builder.games.wh40k8ed.utils import get_cost, get_name, create_gears, points_price


class DWCaptain(HqUnit, armory.DeathwatchUnit):
    type_name = u'Watch Captain (Index)'
    type_id = 'dwatch_captain_v1'

    keywords = ['Character', 'Infantry']
    power = 5
    obsolete = True

    armory = armory

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DWCaptain.Weapon1, self).__init__(parent, 'Pistol')
            self.variant('Master-crafted boltgun', 3)
            self.parent.armory.add_pistol_options(self)
            self.parent.armory.add_combi_weapons(self)
            self.parent.armory.add_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(DWCaptain.Weapon2, self).__init__(parent, 'Weapon')
            self.parent.armory.add_melee_weapons(self)
            self.variant('Xenophase blade', 7)
            self.variant('Storm shield', 15)
            self.variant('Relic blade', 21)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(DWCaptain.Pack, self).__init__(parent, 'Pack')
            self.pack = self.variant('Jump pack', 93 - 74)

    def __init__(self, parent):
        super(DWCaptain, self).__init__(parent, points=74, gear=[
            Gear('Frag grenades'), Gear('Krak grenades')
        ])
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.pack = self.Pack(self)

    def build_points(self):
        res = super(DWCaptain, self).build_points()
        # noinspection PyUnresolvedReferences
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -= 5
        return res

    def build_power(self):
        return self.power + 1 * self.pack.any


class CodexDWCaptain(HqUnit, armory.ClawUser):
    type_name = u'Watch Captain (Codex)'
    type_id = 'dwatch_captain_v2'

    keywords = ['Character', 'Infantry']
    power = 5

    armory = armory

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexDWCaptain.Weapon1, self).__init__(parent, 'Pistol')
            self.variant(*ranged.MasterCraftedBoltgun)
            armory.add_double(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CodexDWCaptain.Weapon2, self).__init__(parent, 'Weapon')
            self.variant(*melee.Chainsword)
            self.variant(*melee.XenophaseBlade)
            self.variant(*melee.RelicBlade)
            armory.add_double(self)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(CodexDWCaptain.Pack, self).__init__(parent, 'Pack')
            self.pack = self.variant('Jump pack', get_cost(units.JumpWatchCaptain) - get_cost(units.WatchCaptain))

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.WatchCaptain), *gear)

        super(CodexDWCaptain, self).__init__(parent, points=cost, gear=create_gears(*gear))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + 1 * self.pack.any


class TerminatorDWCaptain(HqUnit, armory.DeathwatchUnit):
    type_name = u'Watch Captain in Terminator Armour (Index)'
    type_id = 'dwatch_termo_captain_v1'

    keywords = ['Character', 'Infantry', 'Terminator']
    power = 8
    obsolete = True

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(TerminatorDWCaptain.Weapon2, self).__init__(parent, 'Weapon')
            self.variant('Power sword', 4)
            self.variant('Relic blade', 21)

    def __init__(self, parent):
        super(TerminatorDWCaptain, self).__init__(parent, points=122)
        self.wep2 = self.Weapon2(self)


class CodexTerminatorDWCaptain(HqUnit, armory.DeathwatchUnit):
    type_name = u'Watch Captain in Terminator Armour'
    type_id = 'dwatch_termo_captain_v2'

    keywords = ['Character', 'Infantry', 'Terminator']
    power = 7

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexTerminatorDWCaptain.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.StormBolter)
            self.claw = self.variant(*melee.SingleLightningClaw)
            self.variant(*wargear.CharacterStormShield)
            self.variant(*melee.ThunderHammer)
            armory.add_combi_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CodexTerminatorDWCaptain.Weapon2, self).__init__(parent, 'Weapon')
            self.variant(*melee.RelicBlade)
            self.claw = self.variant(*melee.SingleLightningClaw)
            self.variant(*wargear.CharacterStormShield)
            armory.add_term_melee_weapons(self)

    def __init__(self, parent):
        super(CodexTerminatorDWCaptain, self).__init__(parent, points=get_cost(units.TDAWatchCaptain))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)

    def build_points(self):
        res = super(CodexTerminatorDWCaptain, self).build_points()
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -= 2 * get_cost(melee.SingleLightningClaw) - get_cost(melee.PairLightningClaws)
        return res


class DWLibrarian(HqUnit, armory.DeathwatchUnit):
    type_name = u'Deathwatch Librarian (Index)'
    type_id = 'dwatch_librarian_v1'

    keywords = ['Character', 'Infantry', 'Psyker']
    power = 6

    armory = armory

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DWLibrarian.Weapon1, self).__init__(parent, 'Pistol')
            self.parent.armory.add_pistol_options(self)
            self.variant('Boltgun', 0)
            self.parent.armory.add_combi_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(DWLibrarian.Weapon2, self).__init__(parent, 'Force')
            self.variant(*melee.ForceStave)
            self.variant(*melee.ForceSword)
            self.variant(*melee.ForceAxe)

    def __init__(self, parent):
        super(DWLibrarian, self).__init__(parent, points=93, gear=[
            Gear('Frag grenades'), Gear('Krak grenades')
        ])
        self.Weapon1(self)
        self.Weapon2(self)


class TermoDWLibrarian(HqUnit, armory.DeathwatchUnit):
    type_name = u'Deathwatch Librarian in Terminator Armor (Index)'
    type_id = 'dwatch_termo_librarian_v1'

    keywords = ['Character', 'Infantry', 'Terminator', 'Psyker']
    power = 9
    obsolete = True

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(TermoDWLibrarian.Weapon1, self).__init__(parent, 'Bolter')
            self.variant('Storm bolter', 2)
            add_term_combi_weapons(self)

    def __init__(self, parent):
        super(TermoDWLibrarian, self).__init__(parent, points=145)
        self.Weapon1(self)
        DWLibrarian.Weapon2(self)


class DWChaplain(HqUnit, armory.DeathwatchUnit):
    type_name = u'Deathwatch Chaplain (Index)'
    type_id = 'dwatch_chaplain_v1'

    keywords = ['Character', 'Infantry']
    power = 5
    obsolete = True

    armory = armory

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DWChaplain.Weapon1, self).__init__(parent, 'Weapon')
            self.parent.armory.add_pistol_options(self)
            self.variant('Boltgun', 0)
            self.parent.armory.add_combi_weapons(self)

    def __init__(self, parent):
        super(DWChaplain, self).__init__(parent, points=72, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Crozius Arcanum')
        ])
        self.Weapon1(self)


class WatchMaster(HqUnit, armory.DeathwatchUnit):
    type_name = get_name(units.WatchMaster)
    type_id = 'watch_master_v1'
    keywords = ['Infantry', 'Character']
    power = 7

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, melee.GuardianSpear]
        cost = points_price(get_cost(units.WatchMaster), *gear)
        super(WatchMaster, self).__init__(parent, points=cost, gear=create_gears(*gear), static=True)


class Artemis(HqUnit, armory.DeathwatchUnit):
    type_name = get_name(units.WatchCaptainArtemis)
    type_id = 'dw_artemis_v1'
    keywords = ['Imfantry', 'Character']
    power = 7

    def __init__(self, parent):
        super(Artemis, self).__init__(parent, points=get_cost(units.WatchCaptainArtemis), gear=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Power sword'),
            Gear('Hellfire Extremis'), Gear('Stasis Bomb')
        ], static=True, unique=True)


class PrimarisWatchCapitan(HqUnit, armory.DeathwatchUnit):
    type_name = get_name(units.PrimarisWatchCaptain)
    type_id = 'primaris_watch_capitan_v1'
    keywords = ['Infantry', 'Character', 'Primaris']
    power = 6

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.MasterCraftedAutoBoltRifle, ranged.BoltPistol]
        cost = points_price(get_cost(units.PrimarisWatchCaptain), *gear)
        super(PrimarisWatchCapitan, self).__init__(parent, points=cost, gear=create_gears(*gear), static=True)


class CodexDWLibrarian(HqUnit, armory.DeathwatchUnit):
    type_name = u'Deathwatch Librarian (Codex)'
    type_id = 'dwatch_librarian_v2'

    keywords = ['Character', 'Infantry', 'Psyker']
    power = 6

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexDWLibrarian.Weapon1, self).__init__(parent, 'Pistol')
            armory.add_pistol_options(self)
            armory.add_combi_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CodexDWLibrarian.Weapon2, self).__init__(parent, 'Force')
            self.variant(*melee.ForceStave)
            self.variant(*melee.ForceSword)
            self.variant(*melee.ForceAxe)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(CodexDWLibrarian.Pack, self).__init__(parent, 'Pack')
            self.pack = self.variant('Jump pack', get_cost(units.JumpLibrarian) - get_cost(units.Librarian))

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.Librarian), *gear)
        super(CodexDWLibrarian, self).__init__(parent, points=cost, gear=create_gears(*gear))
        self.Weapon1(self)
        self.Weapon2(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + 1 * self.pack.any


class CodexTermoDWLibrarian(HqUnit, armory.DeathwatchUnit):
    type_name = u'Deathwatch Librarian in Terminator Armor (Codex)'
    type_id = 'dwatch_termo_librarian_v2'

    keywords = ['Character', 'Infantry', 'Terminator', 'Psyker']
    power = 8

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexTermoDWLibrarian.Weapon1, self).__init__(parent, 'Bolter')
            self.variant(*ranged.StormBolter)
            armory.add_combi_weapons(self)

    def __init__(self, parent):
        super(CodexTermoDWLibrarian, self).__init__(parent, points=get_cost(units.TDALibrarian))
        self.Weapon1(self)
        CodexDWLibrarian.Weapon2(self)


class DWPrimarisLibrarian(HqUnit, armory.DeathwatchUnit):
    type_name = get_name(units.PrimarisLibrarian)
    type_id = 'primaris_dwatch_librarian_v1'
    keywords = ['Infantry', 'Character', 'Primaris', 'Psyker']
    power = 7

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, melee.ForceSword, ranged.BoltPistol]
        cost = points_price(get_cost(units.PrimarisLibrarian), *gear)
        super(DWPrimarisLibrarian, self).__init__(parent, points=cost, gear=create_gears(*gear), static=True)


class CodexDWChaplain(HqUnit, armory.DeathwatchUnit):
    type_name = u'Deathwatch Chaplain (Codex)'
    type_id = 'dwatch_chaplain_v2'

    keywords = ['Character', 'Infantry']
    power = 5

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexDWChaplain.Weapon1, self).__init__(parent, 'Weapon')
            armory.add_pistol_options(self)
            armory.add_combi_weapons(self)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(CodexDWChaplain.Pack, self).__init__(parent, 'Pack')
            self.pack = self.variant('Jump pack', get_cost(units.JumpChaplain) - get_cost(units.Chaplain))

    class Fist(OptionsList):
        def __init__(self, parent):
            super(CodexDWChaplain.Fist, self).__init__(parent, 'Fist')
            self.variant(*melee.PowerFist)

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, melee.CroziusArcanum]
        cost = points_price(get_cost(units.Chaplain), *gear)
        super(CodexDWChaplain, self).__init__(parent, points=cost, gear=create_gears(*gear))
        self.Weapon1(self)
        self.pack = self.Pack(self)
        self.Fist(self)

    def build_power(self):
        return self.power + 1 * self.pack.any


class CodexTermoDWChaplain(HqUnit, armory.DeathwatchUnit):
    type_name = u'Deathwatch Chaplain in Terminator Armor (Codex)'
    type_id = 'dwatch_termo_chaplain_v1'

    keywords = ['Character', 'Infantry', 'Terminator']
    power = 6

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CodexTermoDWChaplain.Weapon1, self).__init__(parent, 'Bolter')
            self.variant(*ranged.StormBolter)
            armory.add_combi_weapons(self)

    def __init__(self, parent):
        super(CodexTermoDWChaplain, self).__init__(parent, points=get_cost(units.TDAChaplain),
                                                   gear=[Gear('Crozius Arcanum')])
        self.Weapon1(self)


class DWPrimarisChaplain(HqUnit, armory.DeathwatchUnit):
    type_name = get_name(units.PrimarisChaplain)
    type_id = 'primaris_dwatch_chaplain_v1'
    keywords = ['Infantry', 'Character', 'Primaris']
    power = 6

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, melee.CroziusArcanum, ranged.AbsolvorBoltPistol]
        cost = points_price(get_cost(units.PrimarisChaplain), *gear)
        super(DWPrimarisChaplain, self).__init__(parent, points=cost, gear=create_gears(*gear), static=True)
