from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit
import armory, units, ranged, melee, wargear, old_armory


class TacticalSquad(TroopsUnit, armory.CodexDAUnit):
    type_name = u'Tactical Squad' + ' (Index)'
    type_id = 'tactical_squad_v1'

    keywords = ['Infantry']

    armory = old_armory

    common_gear = [Gear('Frag grenades'), Gear('Krak grenades')]
    obsolete = True
    model = UnitDescription('Tactical Squad', options=common_gear + [Gear('Bolt pistol')])

    class Sergeant(Unit):
        type_name = 'Space Marine Sergeant'

        def __init__(self, parent):
            super(TacticalSquad.Sergeant, self).__init__(parent, points=13,
                                                         gear=TacticalSquad.common_gear)
            self.wep1 = parent.armory.SergeantWeapon(self, double=True)
            self.wep2 = parent.armory.SergeantWeapon(self, double=False)

        def check_rules(self):
            super(TacticalSquad.Sergeant, self).check_rules()
            self.parent.parent.armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

        def build_points(self):
            res = super(TacticalSquad.Sergeant, self).build_points()
            if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
                res -= 5
            return res

    class Marine(Count):
        @property
        def description(self):
            return TacticalSquad.model.clone().add([Gear('Boltgun')]).\
                set_count(self.cur - sum(c.used for c in [self.parent.special, self.parent.heavy]))

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(TacticalSquad.SpecialWeapon, self).__init__(parent, 'Special weapon')
            self.bolt = self.variant('Boltgun', 0)
            self.parent.armory.add_special_weapons(self)

        @property
        def description(self):
            return []

    class HeavyWeapon(OneOf):
        def __init__(self, parent):
            super(TacticalSquad.HeavyWeapon, self).__init__(parent, 'Heavy weapon')
            self.bolt = self.variant('Boltgun', 0)
            self.parent.armory.add_heavy_weapons(self)

        @property
        def description(self):
            return []

    def __init__(self, parent):
        super(TacticalSquad, self).__init__(parent, 'Tactical Squad')
        self.sup = SubUnit(self, self.Sergeant(parent=self))
        self.squad = self.Marine(self, 'Tactical Squad', 4, 9, per_model=True, points=13)
        self.special = self.SpecialWeapon(self)
        self.heavy = self.HeavyWeapon(self)

    def build_description(self):
        res = super(TacticalSquad, self).build_description()
        if self.special.used:
            res.add_dup(TacticalSquad.model.clone().add(self.special.cur.gear))
        if self.heavy.used:
            res.add_dup(TacticalSquad.model.clone().add(self.heavy.cur.gear))
        return res

    def get_count(self):
        return self.squad.cur + 1

    def build_power(self):
        return 5 + 4 * (self.get_count() > 5)

    def check_rules(self):
        super(TacticalSquad, self).check_rules()

        self.special.visible = self.special.used = (not self.heavy.used or
                                                    self.heavy.cur == self.heavy.bolt or
                                                    self.get_count() == 10)
        self.heavy.visible = self.heavy.used = (not self.special.used or
                                                self.special.cur == self.special.bolt or
                                                self.get_count() == 10)


class CodexTacticalSquad(TroopsUnit, armory.SMUnit):
    type_name = armory.get_name(units.TacticalSquad) + ' (Codex)'
    type_id = 'tactical_squad_v2'

    keywords = ['Infantry']

    common_gear = [ranged.FragGrenades, ranged.KrakGrenades]

    model = UnitDescription('Space Marine', options=armory.create_gears(*(common_gear + [ranged.BoltPistol])))

    class Sergeant(armory.ClawUser):
        type_name = 'Space Marine Sergeant'

        def __init__(self, parent):
            super(CodexTacticalSquad.Sergeant, self).__init__(parent, points=armory.get_cost(units.TacticalSquad),
                                                              gear=armory.create_gears(*CodexTacticalSquad.common_gear))
            self.wep1 = armory.SergeantWeapon(self, double=True)
            self.wep2 = armory.SergeantWeapon(self, double=False)

        def check_rules(self):
            super(CodexTacticalSquad.Sergeant, self).check_rules()
            armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

    class Marine(Count):
        @property
        def description(self):
            return CodexTacticalSquad.model.clone().add(armory.create_gears(ranged.Boltgun)).\
                set_count(self.cur - sum(c.used for c in [self.parent.special, self.parent.heavy]))

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(CodexTacticalSquad.SpecialWeapon, self).__init__(parent, 'Special weapon')
            self.bolt = self.variant(ranged.Boltgun)
            armory.add_special_weapons(self)

        @property
        def description(self):
            return []

    class HeavyWeapon(OneOf):
        def __init__(self, parent):
            super(CodexTacticalSquad.HeavyWeapon, self).__init__(parent, 'Heavy weapon')
            self.bolt = self.variant(*ranged.Boltgun)
            armory.add_heavy_weapons(self)

        @property
        def description(self):
            return []

    def __init__(self, parent):
        super(CodexTacticalSquad, self).__init__(parent, armory.get_name(units.TacticalSquad))
        self.sup = SubUnit(self, self.Sergeant(parent=self))
        self.squad = self.Marine(self, 'Space Marines', 4, 9, per_model=True,
                                 points=armory.get_cost(units.TacticalSquad))
        self.special = self.SpecialWeapon(self)
        self.heavy = self.HeavyWeapon(self)

    def build_description(self):
        res = super(CodexTacticalSquad, self).build_description()
        if self.special.used:
            res.add_dup(CodexTacticalSquad.model.clone().add(self.special.cur.gear))
        if self.heavy.used:
            res.add_dup(CodexTacticalSquad.model.clone().add(self.heavy.cur.gear))
        return res

    def get_count(self):
        return self.squad.cur + 1

    def build_power(self):
        return 5 + 4 * (self.get_count() > 5)

    def check_rules(self):
        super(CodexTacticalSquad, self).check_rules()

        self.special.visible = self.special.used = (not self.heavy.used or
                                                    self.heavy.cur == self.heavy.bolt or
                                                    self.get_count() == 10)
        self.heavy.visible = self.heavy.used = (not self.special.used or
                                                self.special.cur == self.special.bolt or
                                                self.get_count() == 10)


class ScoutSquad(TroopsUnit, armory.CodexDAUnit):
    type_name = u'Scout Squad' + ' (Index)'
    type_id = 'scout_squad_v1'
    obsolete = True
    model_points = 11
    keywords = ['Infantry']
    model = UnitDescription('Scout', options=[Gear('Frag grenades'),
                                              Gear('Krak grenades'),
                                              Gear('Bolt pistol')])

    armory = old_armory

    class Sergeant(Unit):
        type_name = u'Scout Sergeant'

        class Options(OptionsList):
            def __init__(self, parent):
                super(ScoutSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.variant('Camo cloak', 3)

        def __init__(self, parent):
            super(ScoutSquad.Sergeant, self).__init__(parent, points=ScoutSquad.model_points,
                                                      gear=[Gear('Frag grenades'),
                                                            Gear('Krak grenades')])

            class Weapon1(armory.SergeantWeapon):
                def add_single(self):
                    super(Weapon1, self).add_single()
                    self.variant('Sniper rifle', 4)
                    self.variant('Astartes shotgun', 0)
                    self.variant('Combat knife', 0)

            self.wep1 = Weapon1(self)
            self.wep2 = armory.SergeantWeapon(self, double=True)
            self.opt = self.Options(self)

        def build_points(self):
            res = super(ScoutSquad.Sergeant, self).build_points()
            if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
                res -= 5
            return res

        def check_rules(self):
            super(ScoutSquad.Sergeant, self).check_rules()
            self.parent.parent.armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

    class Scout(ListSubUnit):
        type_name = u'Scout'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(ScoutSquad.Scout.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Boltgun', 0)
                self.variant('Sniper rifle', 4)
                self.variant('Astartes shotgun', 0)
                self.variant('Combat knife', 0)
                self.heavy = [self.variant('Heavy bolter', 10),
                              self.variant('Missile launcher', 25)]

        def __init__(self, parent):
            super(ScoutSquad.Scout, self).__init__(parent, points=ScoutSquad.model_points,
                                                   gear=[Gear('Frag grenades'),
                                                         Gear('Krak grenades'),
                                                         Gear('Bolt pistol')])
            self.wep = self.Weapon(self)
            ScoutSquad.Sergeant.Options(self)

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.wep.cur in self.wep.heavy

    def __init__(self, parent):
        super(ScoutSquad, self).__init__(parent=parent, name='Scout Squad')
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(self))
        self.squad = UnitList(self, self.Scout, 4, 9)

    def get_count(self):
        return self.squad.count + 1

    def check_rules(self):
        super(ScoutSquad, self).check_rules()
        hcnt = sum(u.has_heavy() for u in self.squad.units)
        if hcnt > 1:
            self.error('Only one Scout may carry heavy weapon; taken: {}'.format(hcnt))

    def build_power(self):
        return 6 + 4 * (self.squad.count > 4)


class CodexScoutSquad(TroopsUnit, armory.SMUnit):
    type_name = u'Scout Squad'
    type_id = 'scout_squad_v2'

    keywords = ['Infantry']
    model = UnitDescription('Scout', options=armory.create_gears(ranged.FragGrenades,
                                                                 ranged.KrakGrenades,
                                                                 ranged.BoltPistol))

    class Sergeant(armory.ClawUser):
        type_name = u'Scout Sergeant'

        class Options(OptionsList):
            def __init__(self, parent):
                super(CodexScoutSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.variant(*wargear.CamoCloak)

        def __init__(self, parent):
            super(CodexScoutSquad.Sergeant, self).__init__(parent, points=armory.get_cost(units.ScoutSquad),
                                                           gear=armory.create_gears(ranged.FragGrenades,
                                                                                    ranged.KrakGrenades))

            class Weapon1(armory.SergeantWeapon):
                def add_single(self):
                    super(Weapon1, self).add_single()
                    self.variant(*ranged.SniperRifle)
                    self.variant(*ranged.AstartesShotgun)
                    self.variant(*melee.CombatKnife)

            self.wep1 = Weapon1(self)
            self.wep2 = armory.SergeantWeapon(self, double=True)
            self.opt = self.Options(self)

        def check_rules(self):
            super(CodexScoutSquad.Sergeant, self).check_rules()
            armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

    class Scout(ListSubUnit):
        type_name = u'Scout'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(CodexScoutSquad.Scout.Weapon, self).__init__(parent, 'Weapon')
                self.variant(*ranged.Boltgun)
                self.variant(*ranged.SniperRifle)
                self.variant(*ranged.AstartesShotgun)
                self.variant(*melee.CombatKnife)
                self.heavy = [self.variant(*ranged.HeavyBolter),
                              self.variant(*ranged.MissileLauncher)]

        def __init__(self, parent):
            super(CodexScoutSquad.Scout, self).__init__(parent, points=armory.get_cost(units.ScoutSquad),
                                                        gear=armory.create_gears(ranged.FragGrenades,
                                                                                 ranged.KrakGrenades,
                                                                                 ranged.BoltPistol))
            self.wep = self.Weapon(self)
            CodexScoutSquad.Sergeant.Options(self)

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.wep.cur in self.wep.heavy

    def __init__(self, parent):
        super(CodexScoutSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(self))
        self.squad = UnitList(self, self.Scout, 4, 9)

    def get_count(self):
        return self.squad.count + 1

    def check_rules(self):
        super(CodexScoutSquad, self).check_rules()
        hcnt = sum(u.has_heavy() for u in self.squad.units)
        if hcnt > 1:
            self.error('Only one Scout may carry heavy weapon; taken: {}'.format(hcnt))

    def build_power(self):
        return 6 + 4 * (self.squad.count > 4)


class Intercessors(TroopsUnit, armory.CodexDAUnit):
    type_name = armory.get_name(units.IntercessorSquad)
    type_id = 'intercessors_v1'

    keywords = ['Infantry', 'Primaris']
    gearlist = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol]

    swords = [melee.PowerSword]

    power = 5

    class Sergeant(OptionsList):
        def __init__(self, parent):
            super(Intercessors.Sergeant, self).__init__(parent, 'Sergeant weapons')
            self.gl = self.variant(*wargear.AuxGrenadeLauncher)
            self.swords = [self.variant(*t) for t in self.parent.swords]

        def check_rules(self):
            super(Intercessors.Sergeant, self).check_rules()
            OptionsList.process_limit(self.swords, 1)

        @property
        def description(self):
            return [UnitDescription('Intercessor Sergeant', options=armory.create_gears(*Intercessors.gearlist)).
                    add(self.parent.wep.cur.gear).add(super(Intercessors.Sergeant, self).description)]

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Intercessors.Weapon, self).__init__(parent, 'Weapons')
            self.variant(*ranged.BoltRifle)
            self.variant(*ranged.AutoBoltRifle)
            self.variant(*ranged.StalkerBoltRifle)

        @property
        def description(self):
            return []

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Intercessor', options=armory.create_gears(*Intercessors.gearlist)).
                    add(self.parent.wep.cur.gear).set_count(self.cur - self.parent.aux.cur)]

    class AuxGL(Count):
        @property
        def description(self):
            return [UnitDescription('Intercessor', options=armory.create_gears(*Intercessors.gearlist)).
                    add(self.parent.wep.cur.gear).add(armory.create_gears(wargear.AuxGrenadeLauncher)).
                    set_count(self.cur)]
    
    def __init__(self, parent):
        super(Intercessors, self).__init__(parent, points=armory.get_cost(units.IntercessorSquad))
        self.sarge = self.Sergeant(self)
        self.wep = self.Weapon(self)
        self.marines = self.Marines(self, 'Intercessors', 4, 9, armory.get_cost(units.IntercessorSquad),
                                    per_model=True)
        self.aux = self.AuxGL(self, 'Auxilary grenade launchers', 0, 1, armory.get_costs(wargear.AuxGrenadeLauncher))

    def get_count(self):
        return 1 + self.marines.cur

    def check_rules(self):
        super(Intercessors, self).check_rules()
        self.aux.max = (self.get_count() / 5) - self.sarge.gl.value

    def build_points(self):
        return super(Intercessors, self).build_points() + self.wep.points * self.marines.cur

    def build_power(self):
        return self.power * (1 + (self.marines.cur > 4))


class CrusaderSquad(TroopsUnit, armory.AstartesUnit):
    type_name = armory.get_name(units.CrusaderSquad)
    type_id = 'crusader_squad_v1'
    faction = ['Black Templars']
    keywords = ['Infantry']

    class Initiate(ListSubUnit):
        type_name = u'Initiate'

        class CrusaderWeapon(OneOf):
            def __init__(self, parent):
                super(CrusaderSquad.Initiate.CrusaderWeapon, self).__init__(parent, 'Weapon')
                self.bolt = self.variant(*ranged.Boltgun)
                self.chain = self.variant(*melee.Chainsword)
                armory.add_special_weapons(self)
                armory.add_heavy_weapons(self)
                self.melee = [self.variant(*melee.PowerSword),
                              self.variant(*melee.PowerAxe),
                              self.variant(*melee.PowerMaul),
                              self.variant(*melee.PowerFist)]

        def __init__(self, parent):
            super(CrusaderSquad.Initiate, self).__init__(
                parent, points=armory.get_cost(units.CrusaderSquad),
                gear=armory.create_gears(ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol))
            self.weapon = self.CrusaderWeapon(self)

        @ListSubUnit.count_gear
        def count_heavy(self):
            return self.weapon.cur in (self.weapon.heavy + self.weapon.melee)

        @ListSubUnit.count_gear
        def count_special(self):
            return self.weapon.cur in self.weapon.spec

    class Neophyte(ListSubUnit):
        type_name = u'Neophyte'

        class NeophyteWeapon(OneOf):
            def __init__(self, parent):
                super(CrusaderSquad.Neophyte.NeophyteWeapon, self).__init__(parent=parent, name='Weapon')
                self.variant(*ranged.Boltgun)
                self.shotgun = self.variant(*ranged.AstartesShotgun)
                self.combatknife = self.variant(*melee.CombatKnife)

        def __init__(self, parent):
            super(CrusaderSquad.Neophyte, self).__init__(
                parent, points=armory.get_cost(units.Neophite),
                gear=armory.create_gears(ranged.FragGrenades,
                                         ranged.KrakGrenades, ranged.BoltPistol))
            self.weapon = self.NeophyteWeapon(self)

    class Sergeant(armory.ClawUser):
        type_name = u'Sword Brother'

        def __init__(self, parent):
            super(CrusaderSquad.Sergeant, self).__init__(
                parent, points=armory.get_cost(units.CrusaderSquad),
                gear=armory.create_gears(ranged.FragGrenades, ranged.KrakGrenades)
            )
            # self.weapon1 = CrusaderSquad.Initiate.CrusaderWeapon(self, 'Weapon')
            self.wep1 = armory.SergeantWeapon(self, double=True)
            self.wep2 = armory.SergeantWeapon(self, double=False)

        def check_rules(self):
            super(CrusaderSquad.Sergeant, self).check_rules()
            armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

    class Options(OptionsList):

        def __init__(self, parent):
            super(CrusaderSquad.Options, self).__init__(parent=parent, name='Options')
            self.sergeant = self.variant(CrusaderSquad.Sergeant.type_name, gear=[])
            self.neophytes = self.variant('Neophytes', gear=[])
            self.sergeant_unit = SubUnit(parent, CrusaderSquad.Sergeant(self))
            self.neophytes_unit = UnitList(parent, CrusaderSquad.Neophyte, 1, 10)

        def check_rules(self):
            super(CrusaderSquad.Options, self).check_rules()
            self.sergeant_unit.visible = self.sergeant_unit.used = self.sergeant.value
            self.neophytes_unit.visible = self.neophytes_unit.used = self.neophytes.value

    def __init__(self, parent):
        super(CrusaderSquad, self).__init__(parent)
        self.marines = UnitList(self, self.Initiate, 4, 10, start_value=5)
        self.opt = self.Options(self)

    def check_rules(self):
        super(CrusaderSquad, self).check_rules()
        self.marines.update_range(5 - self.opt.sergeant.value, 10 - self.opt.sergeant.value)
        # if self.marines.count < self.opt.neophytes_unit.count * self.opt.neophytes.value:
        #     self.error('You may not purchase more Neophytes than you have Initiates in the squad.')
        spec = sum(u.count_special() for u in self.marines.units)
        if spec > 1:
            self.error('Initiates can take only 1 special weapon (taken: {0})'.format(spec))
        heavy = sum(u.count_heavy() for u in self.marines.units)
        if heavy > 1:
            self.error('Initiates can take only 1 heavy or power weapon (taken: {0})'.format(heavy))

    def get_count(self):
        return self.marines.count + self.opt.sergeant_unit.used + \
            self.opt.neophytes_unit.used * self.opt.neophytes_unit.count

    def build_power(self):
        return 1 + 4 * (1 + (self.marines.count + self.opt.sergeant.used) > 5) +\
            3 * (((self.opt.neophytes_unit.used * self.opt.neophytes_unit.count) + 4) / 5)
