from builder.games.wh40k8ed.options import OneOf, Gear


class SergeantWeapon(OneOf):
    def add_single(self):
        self.single = [
            self.variant('Boltgun', 0),
            self.variant('Combi-flamer', 11),
            self.variant('Combi-grav', 17),
            self.variant('Combi-melta', 19),
            self.variant('Combi-plasma', 15),
            self.variant('Storm bolter', 2)
        ]

    def add_double(self, sword=True):
        self.double = [
            self.variant('Bolt pistol', 0),
            self.variant('Grav pistol', 8),
            self.variant('Plasma pistol', 7)] +\
            ([self.variant('Chainsword', 0)] if sword else []) +\
            [self.variant('Power sword', 4),
             self.variant('Power axe', 5),
             self.variant('Power mail', 4)]
        self.claw = self.variant('Ligntning claw', 9)
        self.double += [self.claw,
                        self.variant('Power fist', 20)]

    def __init__(self, parent, sword=True, double=False, stern=False):
        super(SergeantWeapon, self).__init__(parent, 'Sergeant Equipment')
        if double:
            self.add_double(sword)
            if not stern:
                self.double.append(self.variant('Thunder Hammer', 20))
        self.add_single()
        if not double:
            self.add_double(sword)
            if not stern:
                self.double.append(self.variant('Thunder Hammer', 20))

    @staticmethod
    def ensure_pairs(first, second):
        for opt in second.single:
            opt.active = not first.cur in first.single
        for opt in first.single:
            opt.active = not second.cur in second.single


def add_pistol_options(obj):
    obj.variant('Bolt pistol', 0)
    obj.variant('Plasma pistol', 7)
    obj.variant('Grav pistol', 8)


def add_combi_weapons(obj):
    obj.combi = [
        obj.variant('Combi-flamer', 11),
        obj.variant('Combi-grav', 17),
        obj.variant('Combi-melta', 19),
        obj.variant('Combi-plasma', 15),
        obj.variant('Storm bolter', 2)]


def add_melee_weapons(obj, axe=True):
    obj.variant('Chainsword', 0)
    obj.variant('Power sword', 4)
    if axe:
        obj.variant('Power axe', 5)
    obj.variant('Power maul', 4)
    obj.variant('Power lance', 4)
    obj.claw = obj.variant('Ligntning claw', 9)
    obj.variant('Power fist', 20)
    if 'CHARACTER' in obj.parent.keywords:
        obj.variant('Thunder Hammer', 25)
    else:
        obj.variant('Thunder Hammer', 20)


def add_special_weapons(obj):
    obj.spec = [
        obj.variant('Flamer', 9),
        obj.variant('Plasma gun', 13),
        obj.variant('Meltagun', 17),
        obj.variant('Grav-gun', 15)]


def add_heavy_weapons(obj):
    obj.heavy = [
        obj.variant('Missle launcher', 25),
        obj.variant('Heavy bolter', 10),
        obj.variant('Multi-melta', 27),
        obj.variant('Lascannon', 25),
        obj.variant('Grav-cannon and grav-amp', 28),
        obj.variant('Plasma cannon', 21)]


def add_term_melee_weapons(obj):
    obj.claw = obj.variant('Ligntning claw', 9)
    obj.fist = obj.variant('Power fist', 20)
    if 'CHARACTER' in obj.parent.keywords:
        obj.variant('Storm shield', 15)
        obj.variant('Thunder Hammer', 25)
    else:
        obj.variant('Storm shield', 5)
        obj.variant('Thunder Hammer', 20)


def add_term_combi_weapons(obj):
    obj.variant('Storm bolter', 2)
    obj.variant('Combi-plasma', 15)
    obj.variant('Combi-flamer', 11)
    obj.variant('Combi-melta', 19)


def add_term_heavy_weapons(obj):
    obj.heavy = [
        obj.variant('Heavy flamer', 17),
        obj.variant('Assault cannon', 21),
        obj.variant('Cyclone missile launcher and storm bolter', 52,
                    gear=[Gear('Cyclone missile launcher'), Gear('Storm bolter')])]


def add_dred_heavy_weapons(obj):
    obj.variant('Twin heavy flamer', 34)
    obj.variant('Twin autocannon', 33)
    obj.variant('Twin heavy bolter', 17)
    obj.variant('Twin lascannon', 50)
    obj.variant('Assault cannon', 21)
    obj.variant('Heavy plasma cannon', 30)
    obj.variant('Multi-melta', 27)
