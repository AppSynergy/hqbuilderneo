from builder.games.wh40k8ed.unit import FlierUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList
import armory, units, ranged


class Stormhawk(FlierUnit, armory.SMUnit):
    type_name = armory.get_name(units.StormhawkInterceptor)
    type_id = 'stormhawk_v1'

    keywords = ['Vehicle', 'Fly']
    power = 10

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Stormhawk.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.LasTalon)
            self.variant(*ranged.IcarusStormcannon)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Stormhawk.Weapon2, self).__init__(parent, '')
            self.variant('Two heavy bolters', armory.get_cost(ranged.HeavyBolter) * 2,
                         gear=armory.create_gears(ranged.HeavyBolter, ranged.HeavyBolter))
            self.variant(*ranged.SkyhammerMissileLauncher)
            self.variant(*ranged.TyphoonMissileLauncher)

    def __init__(self, parent):
        gear = [ranged.AssaultCannon, ranged.AssaultCannon]
        cost = armory.points_price(armory.get_cost(units.StormhawkInterceptor), *gear)
        super(Stormhawk, self).__init__(parent,
                                        gear=armory.create_gears(*gear),
                                        points=cost)
        self.Weapon1(self)
        self.Weapon2(self)


class Stormtalon(FlierUnit, armory.SMUnit):
    type_name = armory.get_name(units.StormtalonGunship)
    type_id = 'stormtalongunship_v1'

    keywords = ['Vehicle', 'Fly']
    power = 9

    def __init__(self, parent):
        gear = [ranged.TwinAssault]
        super(Stormtalon, self).__init__(parent=parent,
                                         points=armory.points_price(armory.get_cost(units.StormtalonGunship), *gear),
                                         gear=armory.create_gears(*gear))
        self.weapon = self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Stormtalon.Weapon, self).__init__(parent=parent, name='Weapon')

            self.twinlinkedheavybolter = self.variant('Two heavy bolters', 2 * armory.get_cost(ranged.HeavyBolter),
                                                      gear=[Gear('Heavy bolter', count=2)])
            self.twinlinkedlascannon = self.variant('Two lascannons', 2 * armory.get_cost(ranged.Lascannon),
                                                    gear=[Gear('Lascannon', count=2)])
            self.skyhammermissilelauncher = self.variant(*ranged.SkyhammerMissileLauncher)
            self.typhoonmissilelauncher = self.variant(*ranged.TyphoonMissileLauncher)


class StormravenGunship(FlierUnit, armory.CodexBAUnit):
    type_name = armory.get_name(units.StormravenGunship)
    type_id = 'stormraven_gunship_v1'

    keywords = ['Vehicle', 'Fly', 'Transport']
    power = 15

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(StormravenGunship.Weapon1, self).__init__(parent, 'Weapon')
            self.twinlinkedheavybolter = self.variant(*ranged.TwinHeavyBolter)
            self.twinlinkedmultimelta = self.variant(*ranged.TwinMultiMelta)
            self.typhoonmissilelauncher = self.variant(*ranged.TyphoonMissileLauncher)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(StormravenGunship.Weapon2, self).__init__(parent, '')
            self.twinlinkedassaultcannon = self.variant(*ranged.TwinAssault)
            self.twinlinkedlascannon = self.variant(*ranged.TwinLascannon)
            self.twinlinkedplasmacannon = self.variant(*ranged.TwinHeavyPlasmaCannon)

    class Options(OptionsList):
        def __init__(self, parent):
            super(StormravenGunship.Options, self).__init__(parent, 'Options')
            self.hurricanebolters = self.variant('Side sponsons with hurricane bolters',
                                                 armory.get_cost(ranged.HurricaneBolter) * 2,
                                                 gear=armory.create_gears(
                                                     ranged.HurricaneBolter,
                                                     ranged.HurricaneBolter
                                                 ))

    def __init__(self, parent):
        gear = [ranged.StormstrikeMissileLauncher] * 2
        super(StormravenGunship, self).__init__(
            parent=parent,
            gear=armory.create_gears(*gear),
            points=armory.points_price(armory.get_cost(units.StormravenGunship), *gear))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.opt = self.Options(self)
