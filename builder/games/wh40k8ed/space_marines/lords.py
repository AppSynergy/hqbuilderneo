from builder.games.wh40k8ed.unit import LordsUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList
from builder.games.wh40k8ed.utils import *
import armory, units, ranged


class Guilliman(LordsUnit, armory.AstartesUnit):
    type_id = 'guilliman_v1'
    type_name = get_name(units.RobouteGuilliman)
    faction = ['Ultramarines']
    power = 18
    keywords = ['Character', 'Monster', 'Primarch']

    def __init__(self, parent):
        super(Guilliman, self).__init__(parent, unique=True, points=get_cost(units.RobouteGuilliman),
                                        static=True, gear=[Gear("The Emperor's Sword"),
                                                           Gear('The Hand of Dominion')])

    def build_statistics(self):
        res = super(Guilliman, self).build_statistics()
        if self.star == 2:
            res['Command points'] = 3
        return res


class TerminusUltra(LordsUnit, armory.AstartesUnit):
    type_id = 'terminus_ultra_v1'
    type_name = get_name(units.TerminusUltra)

    faction = ['Ultramarines']
    keywords = ['Vehicle', 'Land Raider']
    power = 30

    class Options(OptionsList):
        def __init__(self, parent):
            super(TerminusUltra.Options, self).__init__(parent, 'Options')
            self.variant(*ragned.HunterKiller)
            self.variant(*ranged.StormBolter)
            self.variant(*ranged.MultiMelta)

    def __init__(self, parent):
        gear = [ranged.Lascannon] * 2 + [ranged.TwinLascannon] * 3
        cost = points_price(get_cost(units.TerminusUltra), *gear)
        super(TerminusUltra, self).__init__(parent=parent, points=cost,
                                            gear=create_gears(*gear))
        self.opt = self.Options(self)
