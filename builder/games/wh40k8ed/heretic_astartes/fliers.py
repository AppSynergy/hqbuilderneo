__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FlierUnit
from builder.games.wh40k8ed.options import Gear, OptionsList, OneOf
import armory
import wargear, ranged, melee, units
from builder.games.wh40k8ed.utils import *


class Heldrake(FlierUnit, armory.CultLegionUnit):
    type_name = get_name(units.Heldrake)
    type_id = 'heldrake_v1'

    keywords = ['Vehicle', 'Daemon', 'Daemon Engine', 'Fly']
    power = 10

    @classmethod
    def calc_faction(cls):
        return super(Heldrake, cls).calc_faction() + ['THOUSAND SONS']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Heldrake.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.HadesAutocannon)
            self.variant(*ranged.Baleflamer)

    def __init__(self, parent):
        super(Heldrake, self).__init__(parent, points=get_costs(units.Heldrake, melee.HeldrakeClaws),
                                       gear=create_gears(melee.HeldrakeClaws))
        self.Weapon(self)
