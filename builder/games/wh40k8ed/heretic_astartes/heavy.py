__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HeavyUnit, Unit
from builder.games.wh40k8ed.options import Gear, UnitDescription,\
    OptionsList, OneOf, ListSubUnit, SubUnit, UnitList
import armory
import wargear, ranged, melee, units
from builder.games.wh40k8ed.utils import *


class ChaosLandRaider(HeavyUnit, armory.CultLegionUnit):
    type_name = u'Chaos Land Raider'
    type_id = 'chaos_lr_v1'

    keywords = ['Vehicle', 'Transport']
    power = 19

    @classmethod
    def calc_faction(cls):
        return super(ChaosLandRaider, cls).calc_faction() + ['THOUSAND SONS', 'DEATH GUARD']

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(ChaosLandRaider.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.HavocLauncher)
            armory.add_combi_weapons(self)

        def check_rules(self):
            super(ChaosLandRaider.Weapon, self).check_rules()
            OptionsList.process_limit(self.combi, 1)

    def __init__(self, parent):
        gear = [ranged.TwinHeavyBolter, ranged.TwinLascannon, ranged.TwinLascannon]
        cost = points_price(get_cost(units.ChaosLandRaider), *gear)
        super(ChaosLandRaider, self).__init__(parent, points=cost,
                                              gear=create_gears(*gear))
        self.Weapon(self)


class ChaosPredator(HeavyUnit, armory.CultLegionUnit):
    type_name = u'Chaos Predator'
    type_id = 'chaos_pred_v1'

    keywords = ['Vehicle']
    power = 9

    @classmethod
    def calc_faction(cls):
        return super(ChaosPredator, cls).calc_faction() + ['THOUSAND SONS', 'DEATH GUARD']

    class Turret(OneOf):
        def __init__(self, parent):
            super(ChaosPredator.Turret, self).__init__(parent, 'Turret weapon')
            self.variant(*ranged.PredatorAutocannon)
            self.variant(*ranged.TwinLascannon)

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(ChaosPredator.Weapon, self).__init__(parent, 'Sponson weapon', limit=1)
            self.variant('Two heavy bolters', get_cost(ranged.HeavyBolter) * 2,
                         gear=create_gears(ranged.HeavyBolter, ranged.HeavyBolter))
            self.variant('Two lascannons', get_cost(ranged.Lascannon) * 2,
                         gear=create_gears(ranged.Lascannon, ranged.Lascannon))

    def __init__(self, parent):
        super(ChaosPredator, self).__init__(parent, points=get_cost(units.ChaosPredator))
        self.Turret(self)
        self.Weapon(self)
        ChaosLandRaider.Weapon(self)


class ChaosVindicator(HeavyUnit, armory.CultLegionUnit):
    type_name = get_name(units.ChaosVindicator)
    type_id = 'chaos_vindicator_v1'

    keywords = ['Vehicle']
    power = 8

    @classmethod
    def calc_faction(cls):
        return super(ChaosVindicator, cls).calc_faction() + ['THOUSAND SONS']

    def __init__(self, parent):
        super(ChaosVindicator, self).__init__(parent, points=get_costs(units.ChaosVindicator, ranged.DemolisherCannon),
                                              gear=create_gears(ranged.DemolisherCannon))
        ChaosLandRaider.Weapon(self)


class Obliterators(HeavyUnit, armory.CultLegionUnit):
    type_name = get_name(units.Obliterators)
    type_id = 'obliterators_v1'
    keywords = ['Infantry', 'Cult of Destruction', 'Daemon']
    power = 10

    def __init__(self, parent):
        super(Obliterators, self).__init__(parent, static=True, points=get_cost(units.Obliterators) * 3,
                                           gear=[UnitDescription('Obliterator', count=3,
                                                                 options=[Gear('Fleshmetal guns')])])

    def get_count(self):
        return 3


class Havocs(HeavyUnit, armory.CultLegionUnit):
    type_name = get_name(units.Havocs)
    type_id = 'havocs_v1'

    keywords = ['Infantry']
    power = 7

    class HavocsChampion(armory.ClawUser):
        type_name = u'Havocs Champion'

        def __init__(self, parent):
            super(Havocs.HavocsChampion, self).__init__(parent, gear=[Gear('Frag grenades'),
                                                                      Gear('Krak grenades')],
                                                        points=get_cost(units.Havocs))
            self.faction = parent.faction
            self.wep1 = armory.ChampionWeapon(self, True)
            self.wep2 = armory.ChampionWeapon(self)
            self.icon = armory.IconOfChaos(self)

        def check_rules(self):
            super(Havocs.HavocsChampion, self).check_rules()
            armory.ChampionWeapon.ensure_pairs(self.wep1, self.wep2)

    class SingleHavocs(ListSubUnit):
        type_name = u'Havoc'

        class Gun(OneOf):
            def __init__(self, parent):
                super(Havocs.SingleHavocs.Gun, self).__init__(parent, 'Weapon')
                self.default = [self.variant(*ranged.Boltgun),
                                self.variant(*melee.Chainsword)]
                armory.add_special_weapons(self)
                armory.add_heavy_weapons(self)

        def __init__(self, parent):
            super(Havocs.SingleHavocs, self).__init__(parent, gear=[Gear('Frag grenades'),
                                                                    Gear('Krak grenades')],
                                                      points=get_cost(units.Havocs))
            self.faction = self.root_unit.faction
            self.rng1 = self.Gun(self)
            self.icon = armory.IconOfChaos(self)

        @ListSubUnit.count_gear
        def has_icon(self):
            return self.icon.any

        @ListSubUnit.count_gear
        def has_other(self):
            return self.rng1.cur not in self.rng1.default

    def __init__(self, parent):
        super(Havocs, self).__init__(parent)
        self.boss = SubUnit(self, self.HavocsChampion(parent=self))
        self.models = UnitList(self, self.SingleHavocs, 4, 9)

    def get_count(self):
        return self.models.count + 1

    def build_power(self):
        return self.power + 3 * (self.get_count() > 5)

    def check_rules(self):
        super(Havocs, self).check_rules()
        if sum(u.has_icon() for u in self.models.units) + self.boss.unit.icon.any > 1:
            self.error('Only one model may carry an Icon of Chaos')
        if sum(u.has_other() for u in self.models.units) > 4:
            self.error('No more then 4 Heavy Weapon may be taken')


class Forgefiend(HeavyUnit, armory.CultLegionUnit):
    type_name = get_name(units.Forgefiend)
    type_id = 'forgefiend_v1'

    keywords = ['Vehicle', 'Daemon', 'Daemon Engine']
    power = 9

    @classmethod
    def calc_faction(cls):
        return super(Forgefiend, cls).calc_faction() + ['THOUSAND SONS']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Forgefiend.Weapon, self).__init__(parent, 'Arm weapons')
            self.variant('Two hades autocannons', get_cost(ranged.HadesAutocannon) * 2,
                         gear=create_gears(ranged.HadesAutocannon, ranged.HadesAutocannon))
            self.variant('Two ectoplasma cannons', get_cost(ranged.EctoplasmaCannon) * 2,
                         gear=create_gears(ranged.EctoplasmaCannon, ranged.EctoplasmaCannon))

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Forgefiend.Weapon2, self).__init__(parent, 'Head')
            self.variant(*melee.DaemonJaws)
            self.variant(*ranged.EctoplasmaCannon)

    def __init__(self, parent):
        super(Forgefiend, self).__init__(parent, points=get_cost(units.Forgefiend))
        self.Weapon(self)
        self.Weapon2(self)


class Maulerfiend(HeavyUnit, armory.CultLegionUnit):
    type_name = get_name(units.Maulerfiend)
    type_id = 'maulerfiend_v1'

    keywords = ['Vehicle', 'Daemon', 'Daemon Engine']
    power = 9

    @classmethod
    def calc_faction(cls):
        return super(Maulerfiend, cls).calc_faction() + ['THOUSAND SONS']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Maulerfiend.Weapon, self).__init__(parent, 'Arm weapons')
            self.variant('Two magna cutters', get_cost(ranged.MagmaCutter) * 2,
                         gear=create_gears(ranged.MagmaCutter, ranged.MagmaCutter))
            self.variant(*melee.LasherTendrils)

    def __init__(self, parent):
        super(Maulerfiend, self).__init__(parent, points=get_costs(units.Maulerfiend, melee.MaulerfiendFists),
                                          gear=create_gears(melee.MaulerfiendFists))
        self.Weapon(self)


class Defiler(HeavyUnit, armory.CultLegionUnit):
    type_name = get_name(units.Defiler)
    type_id = 'defiler_v1'

    keywords = ['Vehicle', 'Daemon', 'Daemon Engine']
    power = 11

    @classmethod
    def calc_faction(cls):
        return super(Defiler, cls).calc_faction() + ['THOUSAND SONS', 'DEATH GUARD']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Defiler.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.TwinHeavyFlamer)
            self.variant(*ranged.HavocLauncher)
            self.variant(*melee.DefilerScourge)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Defiler.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.ReaperAutocannon)
            self.variant(*ranged.TwinHeavyBolter)
            self.variant(*ranged.TwinLascannon)

    class Weapon3(OptionsList):
        def __init__(self, parent):
            super(Defiler.Weapon3, self).__init__(parent, 'Small weapon', limit=1)
            armory.add_combi_weapons(self)

    def __init__(self, parent):
        gear = [ranged.BattleCannon, melee.DefilerClaws]
        cost = points_price(get_cost(units.Defiler), *gear)
        super(Defiler, self).__init__(parent, points=cost, gear=create_gears(*gear))
        self.Weapon(self)
        self.Weapon2(self)
        self.Weapon3(self)
