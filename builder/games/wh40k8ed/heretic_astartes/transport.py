__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import TransportUnit
from builder.games.wh40k8ed.options import Gear, OptionsList
import armory
import wargear, ranged, melee, units
from builder.games.wh40k8ed.utils import *


class ChaosRhino(TransportUnit, armory.CultLegionUnit):
    type_name = get_name(units.ChaosRhino)
    type_id = 'chaos_rhino_v1'

    keywords = ['Vehicle', 'Transport']
    power = 4

    @classmethod
    def calc_faction(cls):
        return super(ChaosRhino, cls).calc_faction() + ['THOUSAND SONS', 'DEATH GUARD']

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(ChaosRhino.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.HavocLauncher)
            armory.add_combi_weapons(self)

        def check_rules(self):
            super(ChaosRhino.Weapon, self).check_rules()
            OptionsList.process_limit(self.combi, 1)

    def __init__(self, parent):
        super(ChaosRhino, self).__init__(parent, points=get_costs(units.ChaosRhino, ranged.CombiBolter),
                                         gear=create_gears(ranged.CombiBolter))
        self.Weapon(self)
