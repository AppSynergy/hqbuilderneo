__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit
import armory
from elites import Berzerkers, RubricMarines, PlagueMarines, NoiseMarines
import wargear, ranged, melee, units
from builder.games.wh40k8ed.utils import *


class ChaosSpaceMarines(TroopsUnit, armory.CultLegionUnit):
    type_name = get_name(units.ChaosSpaceMarines)
    type_id = 'csm_v1'

    keywords = ['Infantry']

    class AspiringChampion(armory.ClawUser):
        type_name = u'Aspiring Champion'

        def __init__(self, parent):
            super(ChaosSpaceMarines.AspiringChampion, self).__init__(parent, gear=[Gear('Frag grenades'),
                                                                                   Gear('Krak grenades')],
                                                                     points=get_cost(units.ChaosSpaceMarines))
            self.faction = parent.faction
            self.wep1 = armory.ChampionWeapon(self, True)
            self.wep2 = armory.ChampionWeapon(self)
            self.icon = armory.IconOfChaos(self)

        def check_rules(self):
            super(ChaosSpaceMarines.AspiringChampion, self).check_rules()
            armory.ChampionWeapon.ensure_pairs(self.wep1, self.wep2)

    class SingleChaosSpaceMarines(ListSubUnit):
        type_name = u'Chaos Space Marine'

        class Gun(OneOf):
            def __init__(self, parent):
                super(ChaosSpaceMarines.SingleChaosSpaceMarines.Gun, self).__init__(parent, 'Weapon')
                self.default = [self.variant(*ranged.Boltgun),
                                self.variant(*melee.Chainsword)]
                armory.add_special_weapons(self)
                armory.add_heavy_weapons(self)

        class Pistol(OneOf):
            def __init__(self, parent):
                super(ChaosSpaceMarines.SingleChaosSpaceMarines.Pistol, self).__init__(parent, 'Pistol')
                self.variant(*ranged.BoltPistol)
                self.ppist = self.variant(*ranged.PlasmaPistol)

        def __init__(self, parent):
            super(ChaosSpaceMarines.SingleChaosSpaceMarines, self).__init__(parent, points=13)
            self.faction = self.root_unit.faction
            self.rng1 = self.Gun(self)
            self.rng2 = self.Pistol(self)
            self.icon = armory.IconOfChaos(self)

        def check_rules(self):
            super(ChaosSpaceMarines.SingleChaosSpaceMarines, self).check_rules()
            self.rng2.ppist.active = (self.rng1.cur in self.rng1.default)

        @ListSubUnit.count_gear
        def has_gun(self):
            return self.rng1.cur in self.rng1.heavy + self.rng1.special

        @ListSubUnit.count_gear
        def has_pistol(self):
            return self.rng2.cur == self.rng2.ppist

        @ListSubUnit.count_gear
        def has_icon(self):
            return self.icon.any

    def __init__(self, parent):
        super(ChaosSpaceMarines, self).__init__(parent)
        self.boss = SubUnit(self, self.AspiringChampion(parent=self))
        self.models = UnitList(self, self.SingleChaosSpaceMarines, 4, 19)

    def get_count(self):
        return self.models.count + 1

    def build_power(self):
        return 5 + (10 if self.models.count > 10 else
                    (7 if self.models.count > 5 else
                     (4 if self.models.count > 4 else 0)))

    def check_rules(self):
        super(ChaosSpaceMarines, self).check_rules()
        # check mark
        mark_cnt = self.boss.unit.icon.any + sum(u.has_icon() for u in self.models.units)
        if mark_cnt > 1:
            self.error('Only one model may carry Icon of Chaos')
        # flag is either o or 1
        pist_cnt = sum(u.has_pistol() for u in self.models.units)
        if pist_cnt > 1:
            self.error('Only one Chaos Space Marine may take plasma pistol')
        gun_max = (1 - (pist_cnt > 0)) + (self.get_count() >= 10)
        gun_cnt = sum(u.has_gun() for u in self.models.units)
        if gun_cnt > gun_max:
            self.error('No more then {} Chaos Space Marines may exchange boltgun for heavy or special weapons; taken: {}'.format(gun_max, gun_cnt))


class ChaosCultists(TroopsUnit, armory.CultLegionUnit):
    type_name = get_name(units.ChaosCultists)
    type_id = "chaos_cultists_v1"

    keywords = ['Infantry']
    power = 3

    member = UnitDescription('Chaos Cultist')

    @classmethod
    def calc_faction(cls):
        return super(ChaosCultists, cls).calc_faction() + ['THOUSAND SONS', 'DEATH GUARD']

    class Champion(OneOf):
        def __init__(self, parent):
            super(ChaosCultists.Champion, self).__init__(parent=parent, name='Cultist Champion weapon')
            self.variant('Autogun')
            self.variant('Shotgun')
            self.variant('Brutal assault weapon and autopistol',
                         gear=[Gear('Brutal assault weapon'), Gear('Autopistol')])

        @property
        def description(self):
            res = UnitDescription('Cultist Champion')
            return [res.add(self.cur.gear)]

    class Cultist(Count):
        @property
        def description(self):
            return [ChaosCultists.member.clone().add(Gear('Autogun')).set_count(
                self.cur - sum(o.cur for o in self.parent.guns)
            )]

    def __init__(self, parent):
        super(ChaosCultists, self).__init__(parent, points=get_cost(units.ChaosCultists))
        self.champion = self.Champion(self)
        self.models = self.Cultist(self, 'Chaos Cultist', 9, 39, points=get_cost(units.ChaosCultists),
                                   per_model=True)
        self.pist = Count(
            self, 'Autopistol and brutal assault weapon', 0, 9,
            gear=ChaosCultists.member.clone().add([Gear('Brutal assault weapon'), Gear('Autopistol')]))
        self.stubb = Count(
            self, 'Heavy stubber', 0, 1, get_cost(ranged.HeavyStubber),
            gear=ChaosCultists.member.clone().add(Gear('Heavy stubber')))
        self.flame = Count(
            self, 'Flamer', 0, 1, get_cost(ranged.Flamer),
            gear=ChaosCultists.member.clone().add(Gear('Flamer')))
        self.guns = [self.pist, self.stubb, self.flame]

    def check_rules(self):
        super(ChaosCultists, self).check_rules()
        Count.norm_counts(0, self.get_count() / 10, [self.stubb, self.flame])
        self.pist.max = self.models.cur - sum(o.cur for o in [self.stubb, self.flame])

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return self.power * ((self.models.cur + 10) / 10)


class LegionBerzerkers(Berzerkers):
    wiki_faction = "World Eaters"
    faction_substitution = {'World Eaters': '<Legion>'}
    roles = TroopsUnit.roles

    @classmethod
    def calc_faction(cls):
        return []


# class LegionRubrics(RubricMarines):
#     wiki_faction = 'Thousand Sons'
#     faction_substitution = {'Thousand Sons': '<Legion>'}
#     roles = TroopsUnit.roles
#     sorc_points = 30

#     @classmethod
#     def calc_faction(cls):
#         return []


class LegionPlagues(PlagueMarines):
    faction = ['Death Guard']
    roles = TroopsUnit.roles
    obsolete = True

    @classmethod
    def calc_faction(cls):
        return []


class LegionNoises(NoiseMarines):
    wiki_faction = "Emperor's Children"
    faction_substitution = {"Emperor's Children": '<Legion>'}
    roles = TroopsUnit.roles

    @classmethod
    def calc_faction(cls):
        return []
