# -*- coding: utf-8 -*-
__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HqUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, OptionsList
import armory, units, wargear, melee, ranged
from builder.games.wh40k8ed.utils import *


class Abaddon(HqUnit, armory.AstartesUnit):
    type_name = get_name(units.Abaddon)
    type_id = 'abaddon_v1'
    faction = armory.gods + ['BLACK LEGION']
    keywords = ['Character', 'Infantry', 'Chaos Lord', 'Terminator']
    power = 12

    def __init__(self, parent):
        super(Abaddon, self).__init__(parent=parent, points=get_cost(units.Abaddon),
                                      unique=True, gear=[
                                          Gear("Drach'nyen"), Gear('Talon of Horus')
                                      ], static=True)

    def build_statistics(self):
        res = super(Abaddon, self).build_statistics()
        if self.star == 2:
            res['Command points'] = 2
        return res


class Haarken(HqUnit, armory.AstartesUnit):
    type_name = get_name(units.Haarken)
    type_id = 'haarken_v1'
    faction = ['BLACK LEGION']
    keywords = ['Character', 'Infantry', 'Chaos Lord', 'Raptor', 'Jump pack', 'Fly']
    power = 6

    def __init__(self, parent):
        super(Haarken, self).__init__(parent=parent, points=get_cost(units.Haarken),
                                      unique=True, gear=[
                                          Gear('Helspear'), Gear('Lightning claw')
                                      ], static=True)


class Huron(HqUnit, armory.AstartesUnit):
    type_name = get_name(units.Huron)
    type_id = 'huron_v1'
    faction = ['Red Corsairs']
    keywords = ['Character', 'Infantry', 'Chaos Lord', 'Psyker']
    power = 7

    def __init__(self, parent):
        super(Huron, self).__init__(parent=parent, points=get_cost(units.Huron),
                                    unique=True, gear=[
                                        Gear('Frag grenades'), Gear('Krak grenades'),
                                        Gear("Tyrant's claw"), Gear('Power axe')
                                    ], static=True)


class Fabius(HqUnit, armory.AstartesUnit):
    type_name = get_name(units.FabiusBile)
    type_id = 'fabius_v1'

    keywords = ['Character', 'Infantry']
    power = 6

    def __init__(self, parent):
        super(Fabius, self).__init__(parent=parent, points=get_cost(units.FabiusBile),
                                     unique=True, gear=[
                                         Gear('Frag grenades'), Gear('Krak grenades'),
                                         Gear('Xyclon Needler'), Gear('Rod of Torment')
                                     ], static=True)


class ChaosLord(HqUnit, armory.ClawUser, armory.CultLegionUnit):
    type_name = get_name(units.ChaosLord)
    type_id = 'clord_v1'
    keywords = ['Character', 'Infantry']
    power = 5

    class Pistol(OneOf):
        def __init__(self, parent):
            super(ChaosLord.Pistol, self).__init__(parent, 'Weapon')
            armory.add_pistols(self)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    class Sword(OneOf):
        def __init__(self, parent):
            super(ChaosLord.Sword, self).__init__(parent, '')
            armory.add_melee_weapons(self)
            armory.add_pistols(self)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(ChaosLord.Pack, self).__init__(parent, 'Options')
            self.variant('Jump pack', points=get_cost(units.JumpChaosLord) -
                         get_cost(units.ChaosLord))

    def __init__(self, parent):
        super(ChaosLord, self).__init__(parent, points=get_cost(units.ChaosLord),
                                        gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        self.wep1 = self.Pistol(self)
        self.wep2 = self.Sword(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + self.pack.any


class TermoLord(HqUnit, armory.ClawUser, armory.CultLegionUnit):
    type_name = get_name(units.TerminatorChaosLord)
    type_id = 'tlord_v1'
    keywords = ['Character', 'Infantry', 'Terminator']
    power = 6

    class Bolter(OneOf):
        def __init__(self, parent):
            super(TermoLord.Bolter, self).__init__(parent, 'Weapon')
            armory.add_combi_weapons(self)
            armory.add_terminator_melee(self)

    class Sword(OneOf):
        def __init__(self, parent):
            super(TermoLord.Sword, self).__init__(parent, '')
            self.variant(*melee.PowerSword)
            armory.add_terminator_melee(self, sword=False)

    def __init__(self, parent):
        super(TermoLord, self).__init__(parent, points=get_cost(units.TerminatorChaosLord))
        self.wep1 = self.Bolter(self)
        self.wep2 = self.Sword(self)


class BikeLord(HqUnit, armory.ClawUser, armory.CultLegionUnit):
    type_name = u'Chaos Lord on Bike'
    type_id = 'blord_v1'
    keywords = ['Character', 'Biker']
    power = 7

    def __init__(self, parent):
        super(BikeLord, self).__init__(parent, points=113 + get_cost(ranged.CombiBolter),
                                       gear=[Gear('Frag grenades'),
                                             Gear('Krak grenades'),
                                             Gear('Combi-bolter')])
        self.wep1 = ChaosLord.Pistol(self)
        self.wep2 = ChaosLord.Sword(self)


class JugLord(HqUnit, armory.ClawUser, armory.LegionUnit):
    type_name = u'Chaos Lord on Juggernaut of Khorne'
    type_id = 'jlord_v1'
    faction = ['Khorne']
    keywords = ['Character', 'Cavalry', 'Daemon']
    power = 7

    @classmethod
    def calc_faction(cls):
        return super(JugLord, cls).calc_faction() + ['WORLD EATERS']

    def __init__(self, parent):
        super(JugLord, self).__init__(parent, points=125 + 10, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'),
            UnitDescription('Juggernaut of Khorne', options=[Gear('Bladed horn')])])
        self.wep1 = ChaosLord.Pistol(self)
        self.wep2 = ChaosLord.Sword(self)


class DiscLord(HqUnit, armory.ClawUser, armory.LegionUnit):
    type_name = u'Chaos Lord on Disc of Tzeentch'
    type_id = 'dlord_v1'
    faction = ['Tzeentch']
    keywords = ['Character', 'Cavalry', 'Daemon', 'Fly']
    power = 6

    def __init__(self, parent):
        super(DiscLord, self).__init__(parent, points=100 + 2, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'),
            UnitDescription('Disc of Tzeentch', options=[Gear('Blades')])])
        self.wep1 = ChaosLord.Pistol(self)
        self.wep2 = ChaosLord.Sword(self)


class PalLord(HqUnit, armory.ClawUser, armory.LegionUnit):
    type_name = u'Chaos Lord on Palanquin of Nurgle'
    type_id = 'plord_v1'
    faction = ['Nurgle']
    keywords = ['Character', 'Cavalry', 'Daemon']
    power = 6

    @classmethod
    def calc_faction(cls):
        return super(PalLord, cls).calc_faction() + ['DEATH GUARD']

    def __init__(self, parent):
        super(PalLord, self).__init__(parent, points=99 + 6, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'),
            UnitDescription('Palanquin of Nurgle', options=[Gear("Nurglings' claws and teeth")])])
        self.wep1 = ChaosLord.Pistol(self)
        self.wep2 = ChaosLord.Sword(self)


class SteedLord(HqUnit, armory.ClawUser, armory.LegionUnit):
    type_name = u'Chaos Lord on Steed of Slaanesh'
    type_id = 'slord_v1'
    faction = ['Slaanesh']
    keywords = ['Character', 'Cavalry', 'Daemon']
    power = 6

    @classmethod
    def calc_faction(cls):
        return super(SteedLord, cls).calc_faction() + ["EMPEROR'S CHILDREN"]

    def __init__(self, parent):
        super(SteedLord, self).__init__(parent, points=94 + 4, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'),
            UnitDescription('Steed of Slaanesh', options=[Gear('Lashing tongue')])])
        self.wep1 = ChaosLord.Pistol(self)
        self.wep2 = ChaosLord.Sword(self)


class Sorcerer(HqUnit, armory.LegionUnit):
    type_name = get_name(units.Sorcerer)
    type_id = 'sorc_v1'
    keywords = ['Character', 'Infantry', 'Psyker']
    power = 6

    @classmethod
    def calc_faction(cls):
        return super(Sorcerer, cls).calc_faction() + ["EMPEROR'S CHILDREN", 'THOUSAND SONS']\
            + armory.gods[1:]

    class Pistol(OneOf):
        def __init__(self, parent):
            super(Sorcerer.Pistol, self).__init__(parent, 'Weapon')
            armory.add_pistols(self)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    class Sword(OneOf):
        def __init__(self, parent):
            super(Sorcerer.Sword, self).__init__(parent, '')
            self.variant(*melee.ForceSword)
            self.variant(*melee.ForceAxe)
            self.variant(*melee.ForceStave)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(Sorcerer.Pack, self).__init__(parent, 'Options')
            self.variant('Jump pack', get_cost(units.JumpSorcerer) -
                         get_cost(units.Sorcerer))

    def __init__(self, parent):
        super(Sorcerer, self).__init__(parent, points=get_cost(units.Sorcerer),
                                       gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        self.Pistol(self)
        self.Sword(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + self.pack.any


class TermoSorcerer(HqUnit, armory.LegionUnit):
    type_name = get_name(units.TerminatorSorcerer)
    type_id = 'tsorc_v1'
    keywords = ['Character', 'Infantry', 'Psyker', 'Terminator']
    power = 8

    @classmethod
    def calc_faction(cls):
        return super(TermoSorcerer, cls).calc_faction() + ["EMPEROR'S CHILDREN", 'THOUSAND SONS'] + armory.gods[1:]

    class Bolter(OneOf):
        def __init__(self, parent):
            super(TermoSorcerer.Bolter, self).__init__(parent, 'Weapon')
            armory.add_combi_weapons(self)
            armory.add_terminator_melee(self)

    class Sword(OneOf):
        def __init__(self, parent):
            super(TermoSorcerer.Sword, self).__init__(parent, '')
            self.variant(*melee.ForceStave)
            self.variant(*melee.ForceAxe)
            self.variant(*melee.ForceSword)

    def __init__(self, parent):
        super(TermoSorcerer, self).__init__(parent, points=get_cost(units.TerminatorSorcerer))
        self.Bolter(self)
        self.Sword(self)


class BikeSorcerer(HqUnit, armory.LegionUnit):
    type_name = u'Sorcerer on Bike'
    type_id = 'bsorc_v1'
    keywords = ['Character', 'Biker', 'Psyker']
    power = 8

    @classmethod
    def calc_faction(cls):
        return super(BikeSorcerer, cls).calc_faction() + ["EMPEROR'S CHILDREN"] + armory.gods[1:]

    def __init__(self, parent):
        super(BikeSorcerer, self).__init__(parent, points=130 + get_cost(ranged.CombiBolter),
                                           gear=[Gear('Frag grenades'), Gear('Krak grenades'), Gear('Combi-bolter')])
        Sorcerer.Pistol(self)
        Sorcerer.Sword(self)


class DiscSorcerer(HqUnit, armory.LegionUnit):
    type_name = u'Sorcerer on Disc of Tzeentch'
    type_id = 'dsorc_v1'
    faction = ['Tzeentch']
    keywords = ['Character', 'Cavalry', 'Psyker', 'Daemon', 'Fly']
    power = 8

    @classmethod
    def calc_faction(cls):
        return super(DiscSorcerer, cls).calc_faction() + ['THOUSAND SONS']

    def __init__(self, parent):
        super(DiscSorcerer, self).__init__(parent, points=125 + 2,
                                           gear=[Gear('Frag grenades'), Gear('Krak grenades'),
                                                 UnitDescription('Disc of tzeentch', options=[Gear('Blades')])])
        Sorcerer.Pistol(self)
        Sorcerer.Sword(self)


class PalSorcerer(HqUnit, armory.LegionUnit):
    type_name = u'Sorcerer on Palanqiun of Nurgle'
    type_id = 'psorc_v1'
    faction = ['Nurgle']
    keywords = ['Character', 'Cavalry', 'Psyker', 'Daemon']
    power = 8

    @classmethod
    def calc_faction(cls):
        return super(PalSorcerer, cls).calc_faction()

    def __init__(self, parent):
        super(PalSorcerer, self).__init__(parent, points=123 + 6,
                                          gear=[Gear('Frag grenades'), Gear('Krak grenades'),
                                                UnitDescription('Palanquin of Nurgle', options=[Gear("Nurglings' claws and teeth")])])
        Sorcerer.Pistol(self)
        Sorcerer.Sword(self)


class SteedSorcerer(HqUnit, armory.LegionUnit):
    type_name = u'Sorcerer on Steed of Slaanesh'
    type_id = 'ssorc_v1'
    faction = ['Slaanesh']
    keywords = ['Character', 'Cavalry', 'Psyker', 'Daemon']
    power = 7

    @classmethod
    def calc_faction(cls):
        return super(SteedSorcerer, cls).calc_faction() + ["EMPEROR'S CHILDREN"]

    def __init__(self, parent):
        super(SteedSorcerer, self).__init__(parent, points=115 + 4,
                                            gear=[Gear('Frag grenades'), Gear('Krak grenades'),
                                                  UnitDescription('Steed of Slaanesh', options=[Gear('Lashing tongue')])])
        Sorcerer.Pistol(self)
        Sorcerer.Sword(self)


class Warpsmith(HqUnit, armory.CultLegionUnit):
    type_name = get_name(units.Warpsmith)
    type_id = 'warpsmith_v1'
    keywords = ['Character', 'Infantry']
    power = 5

    class Pistol(OneOf):
        def __init__(self, parent):
            super(Warpsmith.Pistol, self).__init__(parent, 'Weapon')
            armory.add_pistols(self)
            armory.add_combi_weapons(self)

    def __init__(self, parent):
        gear = [wargear.FragGrenades, wargear.KrakGrenades, melee.PowerAxe,
                melee.Mechatendrils, ranged.Meltagun, ranged.Flamer]
        cost = points_price(get_cost(units.Warpsmith), *gear)
        super(Warpsmith, self).__init__(parent, points=cost,
                                        gear=create_gears(*gear))
        self.Pistol(self)


class DarkApostle(HqUnit, armory.CultLegionUnit):
    type_name = get_name(units.DarkApostle)
    type_id = 'dark_apostle_v1'
    keywords = ['Character', 'Infantry']
    power = 5

    class Pistol(OneOf):
        def __init__(self, parent):
            super(DarkApostle.Pistol, self).__init__(parent, 'Weapon')
            armory.add_pistols(self)
            armory.add_combi_weapons(self)

    def __init__(self, parent):
        gear = [wargear.FragGrenades, wargear.KrakGrenades, melee.PowerMaul]
        cost = points_price(get_cost(units.DarkApostle), *gear)
        super(DarkApostle, self).__init__(parent, points=cost,
                                          gear=create_gears(*gear))
        self.Pistol(self)


class ExaltedChampion(HqUnit, armory.ClawUser, armory.CultLegionUnit):
    type_name = get_name(units.ExaltedChampion)
    type_id = 'ex_champion_v1'
    keywords = ['Character', 'Infantry']
    power = 5

    def __init__(self, parent):
        super(ExaltedChampion, self).__init__(parent, points=get_cost(units.ExaltedChampion),
                                              gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        self.wep1 = ChaosLord.Pistol(self)
        self.wep2 = ChaosLord.Sword(self)


class DaemonPrince(HqUnit, armory.CultLegionUnit):
    type_name = get_name(units.DaemonPrince)
    type_id = 'dp_v1'

    keywords = ['Character', 'Monster', 'Daemon']

    power = 8

    @classmethod
    def calc_faction(cls):
        return super(DaemonPrince, cls).calc_faction() + ['THOUSAND SONS']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DaemonPrince.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*melee.HellforgedSword)
            self.variant(*melee.DaemonicAxe)
            self.variant('Malefic talons', 10)

    class Options(OptionsList):
        def __init__(self, parent):
            super(DaemonPrince.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.WarpBolter)
            self.wings = self.variant('Wings', get_cost(units.WingDaemonPrince) - get_cost(units.DaemonPrince))

    def __init__(self, parent):
        super(DaemonPrince, self).__init__(parent, gear=[Gear('Malefic talons')],
                                           points=get_cost(units.DaemonPrince))
        self.Weapon(self)
        self.opt = self.Options(self)

    def build_power(self):
        return self.power + self.opt.wings.value


class Kharn(HqUnit, armory.AstartesUnit):
    wiki_faction = "World Eaters"
    type_name = get_name(units.Kharn)
    type_id = 'kharn_v1'
    faction = ['Khorne', 'World Eaters']
    keywords = ['Character', 'Infantry', 'Chaos Lord']
    power = 8

    def __init__(self, parent):
        super(Kharn, self).__init__(parent=parent, points=get_cost(units.Kharn),
                                    unique=True, gear=[
                                        Gear('Frag grenades'), Gear('Krak grenades'),
                                        Gear("Gorechild"), Gear("Kharn's plasma pistol")
                                    ], static=True)


class Lucius(HqUnit, armory.AstartesUnit):
    wiki_faction = "Emperor's Children"
    type_name = get_name(units.Lucius)
    type_id = 'lucius_v1'
    faction = ['Slaanesh', "Emperor's Children"]
    keywords = ['Character', 'Infantry', 'Chaos Lord']
    power = 6

    def __init__(self, parent):
        super(Lucius, self).__init__(parent=parent, points=get_cost(units.Lucius),
                                     unique=True, gear=[
                                         Gear('Frag grenades'), Gear('Krak grenades'), Gear('Doom syren'),
                                         Gear('Master-crafted power sword'), Gear('Lash of Torment')
                                     ], static=True)


class Cypher(HqUnit, Unit):
    type_name = get_name(units.Cypher)
    type_id = 'cypher_v1'
    faction = ['Imperium', 'Fallen', 'Chaos']
    keywords = ['Character', 'Infantry']

    power = 6

    def __init__(self, parent):
        super(Cypher, self).__init__(parent=parent, points=get_cost(units.Cypher),
                                     unique=True, gear=[
                                         Gear('Frag grenades'), Gear('Krak grenades'),
                                         Gear("Cypher's bolt pistol"), Gear("Cypher's plasma pistol")
                                     ], static=True)
