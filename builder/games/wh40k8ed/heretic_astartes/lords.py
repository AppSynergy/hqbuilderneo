__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import LordsUnit, Unit
from builder.games.wh40k8ed.options import Gear, UnitDescription,\
    OptionsList, OneOf, ListSubUnit, SubUnit, UnitList
import armory
import wargear, ranged, melee, units
from builder.games.wh40k8ed.utils import *


class LordOfSkulls(LordsUnit, armory.LegionUnit):
    type_name = get_name(units.KhorneLordOfSkulls)
    type_id = 'lord_of_skulls_v1'

    faction = ['Khorne']
    keywords = ['Vehicle', 'Daemon', 'Daemon Engine']
    power = 29

    @classmethod
    def calc_faction(cls):
        return super(LordOfSkulls, cls).calc_faction() + ['WORLD EATERS']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(LordOfSkulls.Weapon, self).__init__(parent, 'Arm weapon')
            self.variant(*ranged.HadesGatlingCannon)
            self.variant(*ranged.Skullhurler)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(LordOfSkulls.Weapon2, self).__init__(parent, 'Torso weapon')
            self.variant(*ranged.GorestormCannon)
            self.variant(*ranged.IchorCannon)
            self.variant(*ranged.DaemongoreCannon)

    def __init__(self, parent):
        super(LordOfSkulls, self).__init__(parent, points=get_costs(units.KhorneLordOfSkulls, melee.GreatCleaverOfKhorne),
                                           gear=create_gears(melee.GreatCleaverOfKhorne))
        self.Weapon(self)
        self.Weapon2(self)
