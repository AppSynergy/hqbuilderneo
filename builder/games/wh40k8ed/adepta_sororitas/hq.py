__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import HqUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription
import armory, units, melee, ranged
from builder.games.wh40k8ed.utils import *


class Jacobus(HqUnit, armory.MinistorumUnit):
    type_name = get_name(units.UriahJacobus)
    type_id = 'jacobus_v1'
    keywords = ['Character', 'Infantry', 'Ministorum Priest', 'Missionary']

    power = 3

    def __init__(self, parent):
        super(Jacobus, self).__init__(parent, unique=True, static=True, points=get_cost(units.UriahJacobus), gear=[
            Gear('Bolt pistol'), Gear('The Redeemer'),
            Gear('Chainsword'), Gear('Frag grenades'),
            Gear('Krak grenades')
        ])


class Celestine(HqUnit, armory.MinistorumUnit):
    type_name = get_name(units.Celestine)
    type_id = 'celestine_v1'
    faction = ['Adepta Sororitas']
    keywords = ['Character', 'Infantry', 'Jump Pack', 'Fly']

    def __init__(self, parent):
        super(Celestine, self).__init__(parent, unique=True, static=True, points=get_cost(units.Celestine), gear=[
            UnitDescription('Celestine', options=[Gear('Ardent Blade')])
        ])


class Canoness(HqUnit, armory.SororitasUnit):
    type_name = get_name(units.Canoness)
    type_id = 'canoness_v1'

    keywords = ['Character', 'Infantry']
    power = 3

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Canoness.Weapon1, self).__init__(parent, 'Pistol')
            armory.add_pistol_options(self)
            armory.add_ranged_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Canoness.Weapon2, self).__init__(parent, 'Weapon')
            armory.add_melee_weapons(self)
            self.variant(*melee.Eviscerator)
            armory.add_pistol_options(self)
            armory.add_ranged_weapons(self)

    def __init__(self, parent):
        super(Canoness, self).__init__(parent, points=get_cost(units.Canoness), gear=[
            Gear('Frag grenades'), Gear('Krak grenades')
        ])
        self.Weapon1(self)
        self.Weapon2(self)


class Missionary(HqUnit, armory.MinistorumUnit):
    type_name = get_name(units.Missionary)
    type_id = 'missionary_v1'
    keywords = ['Character', 'Infantry', 'Ministorum Priest']

    power = 2

    def __init__(self, parent):
        gear = [ranged.Autogun, ranged.Laspistol, melee.Chainsword, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.Missionary), *gear)
        super(Missionary, self).__init__(parent, points=cost, gear=create_gears(*gear), static=True)
