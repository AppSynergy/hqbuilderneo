__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TransportUnit
from builder.games.wh40k8ed.options import OneOf, Gear, OptionsList
import armory, units, melee, ranged
from builder.games.wh40k8ed.utils import *


class ASRhino(TransportUnit, armory.SororitasUnit):
    type_name = get_name(units.SororitasRhino)

    type_id = 'rhino_v1'
    keywords = ['Vehicle', 'Transport', 'Rhino']

    power = 4

    class Options(OptionsList):
        def __init__(self, parent):
            super(ASRhino.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.HunterKillerMissile)
            self.variant(*ranged.StormBolter)

    def __init__(self, parent):
        super(ASRhino, self).__init__(parent, gear=create_gears(ranged.StormBolter),
                                      points=points_price(get_cost(units.SororitasRhino), ranged.StormBolter))
        self.opt = self.Options(self)


class Immolator(TransportUnit, armory.SororitasUnit):
    type_name = get_name(units.Immolator)
    type_id = 'immolator_v1'

    keywords = ['Vehicle', 'Transport']
    power = 5

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Immolator.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.ImmolationFlamer)
            self.variant(*ranged.TwinHeavyBolter)
            self.variant(*ranged.TwinMultiMelta)

    def __init__(self, parent):
        super(Immolator, self).__init__(parent, points=get_cost(units.Immolator), gear=[
            Gear('Smoke launchers')
        ])
        self.wep = self.Weapon(self)
        self.opt = ASRhino.Options(self)
