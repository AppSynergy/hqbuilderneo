from builder.games.wh40k8ed.space_marines.armory import AstartesUnit
import melee, wargear, ranged
from builder.games.wh40k8ed.options import Gear, OptionsList, OneOf


class GKUnit(AstartesUnit):
    faction = ['Grey Knights']
    wiki_faction = 'Grey Knights'


class DreadknightOptions(OptionsList):
    def __init__(self, parent):
        super(DreadknightOptions, self).__init__(parent, 'Options')
        self.variant(*wargear.DreadknightTeleporter)

class DreadknightRangedWeapons(OptionsList):
    def __init__(self, parent):
        super(DreadknightRangedWeapons, self).__init__(parent, 'Ranged weapons', limit=2)
        self.variant(*ranged.HeavyIncinerator)
        self.variant(*ranged.HeavyPsycannon)
        self.variant(*ranged.GatlingPsilencer)

class DreadknightMeleeWeapon(OneOf):
    def __init__(self, parent):
        super(DreadknightMeleeWeapon, self).__init__(parent, 'Melee weapon')
        self.variant(**melee.DoubleDreadfist)
        self.variant(**melee.DreadfistAndGreatHammer)
        self.variant(**melee.DreadfistAndGreatSword)

def add_ranged_weapons(obj):
    obj.sb = obj.variant(*ranged.StormBolter)
    obj.ranged = [
        obj.sb,
        obj.variant(*ranged.Incinerator),
        obj.variant(*ranged.Psycannon),
        obj.variant(*ranged.Psilencer)]

def add_term_ranged_weapons(obj):
    obj.sb = obj.variant(*ranged.StormBolter)
    obj.ranged = [
        obj.sb,
        obj.variant(*ranged.TerminatorIncinerator),
        obj.variant(*ranged.TerminatorPsycannon),
        obj.variant(*ranged.TerminatorPsilencer)]


def add_melee_weapons(obj):
    obj.melee = [
        obj.variant(*melee.NemesisSword),
        obj.variant(*melee.NemesisHalberd),
        obj.variant(*melee.NemesisStave),
        obj.variant(*melee.NemesisFalchion, gear=Gear(name=melee.NemesisFalchion[0], count=2)),
        obj.variant(*melee.NemesisDaemonHammer),
    ]


def add_combi_weapons(obj):
    obj.variant(*ranged.StormBolter)
    obj.variant(*ranged.CombiPlasma)
    obj.variant(*ranged.CombiFlamer)
    obj.variant(*ranged.CombiMelta)


def add_dred_heavy_weapons(obj):
    obj.variant(*ranged.HeavyPlasmaCannon)
    obj.variant(*ranged.MultiMelta)
    obj.variant(*ranged.TwinLascannon)
