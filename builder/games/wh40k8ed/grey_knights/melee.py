from builder.games.wh40k8ed.options import Gear

CroziusArcanum = ('Crozius Arcanum', 0)
DoubleDreadfist = {
    'name': 'Dreadfist',
    'points': 35,
    'gear': Gear('Dreadfist', count=2)
}
DreadfistAndGreatHammer = {
    'name': 'Dreadfist and Nemesis Daemon greathammer',
    'points': 25 + 15,
    'gear': [Gear('Dreadfist'), Gear('Nemesis Daemon greathammer')]
}
DreadfistAndGreatSword = {
    'name': 'Dreadfist and Nemesis greatsword',
    'points': 25 + 10,
    'gear': [Gear('Dreadfist'), Gear('Nemesis greatsword')]
}
DreadnoughtCombatWeapon = ('Dreadnought combat weapon', 30)
NemesisDaemonHammer = ('Nemesis Daemon hammer', 13)
NemesisFalchion = ('Nemesis falchion', 0)
NemesisHalberd = ('Nemesis force halberd', 0)
NemesisSword = ('Nemesis force sword', 0)
NemesisStave = ('Nemesis warding stave', 0)
PowerAxe = ('Power axe', 5)
ServoArm = ('Servo-arm', 0)
