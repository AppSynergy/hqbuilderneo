from heavy import Dreadknight, PurgationSquad, GKLandRaider, \
    GKLandRaiderCrusader, GKLandRaiderRedeemer
from fliers import GKStormhawk, GKStormtalon, GKStormravenGunship
from fast import InterceptorSquad
from troops import StrikeSquad, GKTerminatorSquad
from elites import PurifierSquad, PaladinSquad, GKApothecary, BrotherhoodAncient, PaladinAncient, GKDreadnought, \
    GKVenDreadnought, GKServitors
from hq import Draigo, Voldus, Stern, Crowe, GrandMaster, GMDreadknight, BrotherCaptain, GKTechmarine, \
    BrotherhoodChampion, GKChaplain, GKLibrarian
from transports import GKRhino, GKRazorback

unit_types = [Dreadknight, PurgationSquad, GKLandRaider,
              GKLandRaiderCrusader, GKLandRaiderRedeemer, GKStormhawk,
              GKStormtalon, GKStormravenGunship, InterceptorSquad,
              StrikeSquad, GKTerminatorSquad, PurifierSquad,
              PaladinSquad, GKApothecary, BrotherhoodAncient,
              PaladinAncient, GKDreadnought, GKVenDreadnought,
              GKServitors, Draigo, Voldus, Stern, Crowe, GrandMaster,
              GMDreadknight, BrotherCaptain, GKTechmarine,
              BrotherhoodChampion, GKChaplain, GKLibrarian,
              GKRhino, GKRazorback]
