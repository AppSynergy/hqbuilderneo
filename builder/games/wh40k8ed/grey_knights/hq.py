from builder.core2 import Gear
from builder.games.wh40k8ed.unit import HqUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList
from builder.games.wh40k8ed.utils import *
import armory
import ranged
import melee
import units


class Draigo(HqUnit, armory.GKUnit):
    type_id = 'draigo_v1'
    type_name = get_name(units.Draigo)
    power = 12

    keywords = ['Character', 'Infantry', 'Grand Master', 'Terminator', 'Psyker']

    def __init__(self, parent):
        super(Draigo, self).__init__(parent=parent, points=get_cost(units.Draigo), gear=[
            Gear('Titansword'),
            Gear('Storm shield'),
            Gear('Storm bolter'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Psyk-out grenades'),
        ], unique=True, static=True)


class Voldus(HqUnit, armory.GKUnit):
    type_id = 'voldus_v1'
    type_name = get_name(units.Voldus)
    power = 10

    keywords = ['Character', 'Infantry', 'Grand Master', 'Terminator', 'Psyker']

    def __init__(self, parent):
        super(Voldus, self).__init__(parent=parent, points=get_cost(units.Voldus), gear=[
            Gear('Malleus Argyrum'),
            Gear('Storm bolter'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Psyk-out grenades'),
        ], unique=True, static=True)


class GrandMaster(HqUnit, armory.GKUnit):
    type_name = get_name(units.GrandMaster)
    type_id = 'grand_master_v1'

    keywords = ['Character', 'Infantry', 'Terminator', 'Psyker']
    power = 10
    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.PsykOutGrenades]

    class Melee(OneOf):
        def __init__(self, parent):
            super(GrandMaster.Melee, self).__init__(parent, 'Weapon')
            self.variant(*melee.NemesisHalberd),
            self.variant(*melee.NemesisSword),
            self.variant(*melee.NemesisStave),
            self.variant(*melee.NemesisFalchion, gear=Gear(name=melee.NemesisFalchion[0], count=2)),
            self.variant(*melee.NemesisDaemonHammer),

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(GrandMaster.SpecialWeapon, self).__init__(parent=parent, name='')
            armory.add_term_ranged_weapons(self)

    def __init__(self, parent):
        super(GrandMaster, self).__init__(parent, 'Grand Master',
                                          points=get_cost(units.GrandMaster),
                                          gear=create_gears(
                                              *GrandMaster.model_gear
                                          ))
        self.Melee(self)
        self.SpecialWeapon(self)


class GMDreadknight(HqUnit, armory.GKUnit):
    type_id = 'GM_dreadknight_v1'
    type_name = get_name(units.GrandMasterInNemesisDreadknight)

    keywords = ['Character', 'Vehicle', 'Nemesis Dreadknight', 'Psyker']
    power = 14

    def __init__(self, parent):
        super(GMDreadknight, self).__init__(parent, points=get_cost(units.GrandMasterInNemesisDreadknight))
        armory.DreadknightMeleeWeapon(self)
        armory.DreadknightRangedWeapons(self)
        armory.DreadknightOptions(self)


class Crowe(HqUnit, armory.GKUnit):
    type_id = 'crowe_v1'
    type_name = get_name(units.Crowe)
    power = 7

    keywords = ['Character', 'Infantry', 'Brotherhood Champion', 'Psyker']

    def __init__(self, parent):
        super(Crowe, self).__init__(parent=parent, points=get_cost(units.Crowe), gear=[
            Gear('Black Blade of Antwyr'),
            Gear('Storm bolter'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Psyk-out grenades'),
        ], unique=True, static=True)


class Stern(HqUnit, armory.GKUnit):
    type_id = 'stern_v1'
    type_name = get_name(units.Stern)
    power = 8

    keywords = ['Character', 'Infantry', 'Brother-Captain', 'Terminator', 'Psyker']

    def __init__(self, parent):
        super(Stern, self).__init__(parent=parent, points=get_cost(units.Stern), gear=[
            Gear('Nemesis force sword'),
            Gear('Storm bolter'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Psyk-out grenades'),
        ], unique=True, static=True)


class BrotherCaptain(HqUnit, armory.GKUnit):
    type_name = get_name(units.BrotherCaptain)
    type_id = 'brother_captain_v1'

    keywords = ['Character', 'Infantry', 'Terminator', 'Psyker']
    power = 9
    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.PsykOutGrenades]

    class Melee(OneOf):
        def __init__(self, parent):
            super(BrotherCaptain.Melee, self).__init__(parent, 'Weapon')
            self.variant(*melee.NemesisHalberd),
            self.variant(*melee.NemesisSword),
            self.variant(*melee.NemesisStave),
            self.variant(*melee.NemesisFalchion, gear=Gear(name=melee.NemesisFalchion[0], count=2)),
            self.variant(*melee.NemesisDaemonHammer),

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(BrotherCaptain.SpecialWeapon, self).__init__(parent=parent, name='')
            armory.add_term_ranged_weapons(self)

    def __init__(self, parent):
        super(BrotherCaptain, self).__init__(parent, 'Brother Captain',
                                             points=get_cost(units.BrotherCaptain),
                                             gear=create_gears(
                                                 *BrotherCaptain.model_gear
                                             ))
        self.Melee(self)
        self.SpecialWeapon(self)


class GKLibrarian(HqUnit, armory.GKUnit):
    type_name = 'Grey Knights ' + get_name(units.Librarian)
    type_id = 'gk_librarian_v1'

    keywords = ['Character', 'Infantry', 'Terminator', 'Psyker']
    power = 9
    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.PsykOutGrenades]

    class Melee(OneOf):
        def __init__(self, parent):
            super(GKLibrarian.Melee, self).__init__(parent, 'Weapon')
            self.variant(*melee.NemesisStave),
            self.variant(*melee.NemesisSword),
            self.variant(*melee.NemesisHalberd),
            self.variant(*melee.NemesisFalchion, gear=Gear(name=melee.NemesisFalchion[0], count=2)),
            self.variant(*melee.NemesisDaemonHammer),

    class CombiWeapon(OneOf):
        def __init__(self, parent):
            super(GKLibrarian.CombiWeapon, self).__init__(parent=parent, name='')
            armory.add_combi_weapons(self)

    def __init__(self, parent):
        super(GKLibrarian, self).__init__(parent, 'Librarian',
                                          points=get_cost(units.Librarian),
                                          gear=create_gears(
                                              *GKLibrarian.model_gear
                                          ))
        self.Melee(self)
        self.CombiWeapon(self)


class GKTechmarine(HqUnit, armory.GKUnit):
    type_name = 'Grey Knights ' + get_name(units.Techmarine)
    type_id = 'gk_techmatine_v1'

    keywords = ['Character', 'Infantry', 'Psyker']
    power = 7

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(GKTechmarine.Weapon1, self).__init__(parent, 'Pistol')
            self.variant(*ranged.BoltPistol)
            self.variant(*ranged.Boltgun)

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades, melee.ServoArm, melee.ServoArm, ranged.PlasmaCutter,
                ranged.Flamer, ranged.PsykOutGrenades]
        cost = points_price(get_cost(units.Techmarine), *gear)
        super(GKTechmarine, self).__init__(parent, name=get_name(units.Techmarine),
                                           points=cost,
                                           gear=create_gears(*gear))
        self.Weapon1(self)


# TODO custom warger?
class GKChaplain(HqUnit, armory.GKUnit):
    type_id = 'gk_chaplain_v1'
    type_name = 'Grey Knights ' + get_name(units.Chaplain)
    power = 8

    keywords = ['Character', 'Infantry', 'Terminator', 'Psyker']

    def __init__(self, parent):
        super(GKChaplain, self).__init__(parent=parent, points=get_cost(units.Chaplain), gear=[
            Gear('Crozius arcanum'),
            Gear('Storm bolter'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Psyk-out grenades'),
        ], static=True)


class BrotherhoodChampion(HqUnit, armory.GKUnit):
    type_id = 'brotherhood_champion_v1'
    type_name = get_name(units.BrotherhoodChampion)
    power = 6

    keywords = ['Character', 'Infantry', 'Psyker']

    def __init__(self, parent):
        super(BrotherhoodChampion, self).__init__(parent=parent, points=get_cost(units.BrotherhoodChampion),
                                                  gear=[
                                                      Gear('Nemesis force sword'),
                                                      Gear('Storm bolter'),
                                                      Gear('Frag grenades'),
                                                      Gear('Krak grenades'),
                                                      Gear('Psyk-out grenades'),
                                                  ], static=True)
