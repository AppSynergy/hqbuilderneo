from builder.games.wh40k8ed.unit import TransportUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList
from builder.games.wh40k8ed.utils import *
import armory
import units
import ranged


class GKRhino(TransportUnit, armory.GKUnit):
    type_name = 'Grey Knights ' + get_name(units.Rhino)
    type_id = 'gk_rhino_aa_v1'

    keywords = ['Vehicle', 'Transport']
    power = 4

    class Options(OptionsList):
        def __init__(self, parent):
            super(GKRhino.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.HunterKillerMissile)
            self.variant(*ranged.StormBolter)

    def __init__(self, parent):
        gear = [ranged.StormBolter]
        super(GKRhino, self).__init__(
            parent, gear=create_gears(*gear),
            points=points_price(get_cost(units.Rhino), *gear))
        self.Options(self)


class GKRazorback(TransportUnit, armory.GKUnit):
    type_name = 'Grey Knights ' + get_name(units.Razorback)
    type_id = 'gk_razorback_v2'

    keywords = ['Vehicle', 'Transport']
    power = 5

    class Weapon(OneOf):
        def __init__(self, parent):
            super(GKRazorback.Weapon, self).__init__(parent=parent, name='Weapon')

            self.variant(*ranged.TwinHeavyBolter)
            self.variant(*ranged.TwinLascannon)
            self.variant(*ranged.TwinAssaultCannon)

    def __init__(self, parent):
        super(GKRazorback, self).__init__(parent=parent,
                                          points=get_cost(units.Razorback))
        self.Weapon(self)
        GKRhino.Options(self)
