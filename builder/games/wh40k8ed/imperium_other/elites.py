__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import ElitesUnit
from builder.games.wh40k8ed.options import Gear, UnitDescription, OneOf,\
    ListSubUnit, UnitList, Count
import armory
from builder.games.wh40k8ed.utils import *
import units, wargear


class VindicareAssasin(ElitesUnit, armory.AssassinUnit):
    type_name = get_name(units.Vindicare)
    type_id = "vindicare_assasin_v1"
    keywords = ['Infantry', 'Character']
    power = 5

    def __init__(self, parent):
        super(VindicareAssasin, self).__init__(parent, points=get_cost(units.Vindicare), gear=[
            Gear('Blind Grenades'),
            Gear('Exitus Rifle'), Gear('Exitus Pistol')])


class EversorAssasin(ElitesUnit, armory.AssassinUnit):
    type_name = "Eversor Assasin"
    type_id = "eversor_assasin_v1"
    keywords = ['Infantry', 'Character']
    power = 4

    def __init__(self, parent):
        super(EversorAssasin, self).__init__(parent, points=70,
                                             gear=[Gear('Melta Bombs'),
                                                   Gear('Neuro-gauntlet'),
                                                   Gear('Power Sword'),
                                                   Gear('Executioner Pistol')])


class CallidusAssasin(ElitesUnit, armory.AssassinUnit):
    type_name = get_name(units.Callidus)
    type_id = "callidus_assasin_v1"
    keywords = ['Infantry', 'Character']
    power = 5

    def __init__(self, parent):
        super(CallidusAssasin, self).__init__(parent, points=get_cost(units.Callidus),
                                              gear=[Gear('Neural Shredder'), Gear('Phase Sword'),
                                                    Gear('Poison Blades')])


class CulexusAssasin(ElitesUnit, armory.AssassinUnit):
    type_name = "Culexus Assasin"
    type_id = "culexus_assasin_v1"
    keywords = ['Infantry', 'Character']
    power = 5

    def __init__(self, parent):
        super(CulexusAssasin, self).__init__(parent, points=85,
                                             gear=[Gear('Animus Speculum'),
                                                   Gear('Psyk-out Grenades')])


class Acolytes(ElitesUnit, armory.OrdoUnit):
    type_name = u'Acolytes'
    type_id = 'inq_acolyte_v1'

    keywords = ['Infantry']

    class Acolyte(ListSubUnit):
        type_name = u'Acolyte'

        def __init__(self, parent):
            super(Acolytes.Acolyte, self).__init__(parent=parent, points=8)
            self.Weapon1(self)
            self.Weapon2(self)

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Acolytes.Acolyte.Weapon1, self).__init__(parent, 'Pistol')
                self.variant('Laspistol')
                armory.add_inq_pistol_weapons(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(Acolytes.Acolyte.Weapon2, self).__init__(parent, 'Weapon')
                self.variant('Chainsword')
                armory.add_inq_melee_weapons(self)
                armory.add_inq_ranged_weapons(self)

    def __init__(self, parent):
        super(Acolytes, self).__init__(parent)
        self.models = UnitList(self, self.Acolyte, 1, 6)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.get_count()


class Daemonhost(ElitesUnit, armory.InquisitionUnit):
    type_name = u'Daemonhost'
    type_id = 'dhost_v1'

    keywords = ['Daemon', 'Infantry']
    power = 1

    def __init__(self, parent):
        super(Daemonhost, self).__init__(parent, gear=[
            Gear('Unholy gaze'), Gear('Warp grasp')
        ], static=True, points=25)
    

class Jokaero(ElitesUnit, armory.OrdoUnit):
    type_name = u'Jokaero Waponsmith'
    type_id = 'monkey_v1'
    faction = ['Jokaero']
    keywords = ['Infantry']
    power = 2

    def __init__(self, parent):
        super(Jokaero, self).__init__(parent, gear=[
            Gear('Digital weapons')
        ], static=True, points=18)


class Prosecutors(ElitesUnit, armory.SSisterUnit):
    type_name = get_name(units.Prosecutors)
    type_id = 'prosecutors_v1'

    keywords = ['Infantry']
    power = 3

    model_cost = points_price(get_cost(units.Prosecutors), wargear.Boltgun)
    common_gear = [Gear('Boltgun'), Gear('Psyk-out grenades')]
    single_model = UnitDescription('Prosecutor').add(common_gear)

    def __init__(self, parent):
        super(Prosecutors, self).__init__(parent, points=self.model_cost,
                                          gear=UnitDescription('Sister Superior',
                                                               options=self.common_gear))
        self.models = Count(self, self.single_model.name, 4, 9, self.model_cost, True,
                            gear=self.single_model.clone())

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return self.power * (1 + (self.models.cur > 4))


class Vigilators(Prosecutors):
    type_name = get_name(units.Vigilators)
    type_id = 'vigilators_v1'

    power = 4

    model_cost = points_price(get_cost(units.Vigilators), wargear.Greatblade)
    common_gear = [Gear('Executioner greatblade'), Gear('Psyk-out grenades')]
    single_model = UnitDescription('Vigilator').add(common_gear)


class Witchseekers(Prosecutors):
    type_name = get_name(units.Witchseekers)
    type_id = 'witchseekers_v1'

    power = 5

    model_cost = points_price(get_cost(units.Witchseekers), wargear.Flamer)
    common_gear = [Gear('Flamer'), Gear('Psyk-out grenades')]
    single_model = UnitDescription('Witchseeker').add(common_gear)
