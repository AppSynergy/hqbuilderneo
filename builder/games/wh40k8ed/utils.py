from builder.core2 import Gear

def get_name(item):
    return item[0]


def get_cost(item):
    return item[1]


def get_costs(*items):
    return sum(i[1] for i in items)


def join_and(*items):
    items = [get_name(el) for el in items]
    if len(items) < 3:
        return u' and '.join(items)
    return u'{} and {}'.format(u', '.join(items[:-1]), items[-1])


def points_price(price, *gears):
    for el in gears:
        price += el[1]
    return price


def create_gears(*gears):
    result = []
    for el in gears:
        result.append(Gear(el[0]))
    return result
