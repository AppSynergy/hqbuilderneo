__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FastUnit
from builder.games.wh40k8ed.options import Gear, Count, UnitDescription,\
    OptionsList, ListSubUnit, UnitList, OneOf
import armory, units, wargear, ranged, melee
from builder.games.wh40k8ed.utils import *


class Wraiths(FastUnit, armory.DynastyUnit):
    type_name = get_name(units.CanoptekWraiths)
    type_id = 'wraiths_v1'

    faction = ['Canoptek']
    keywords = ['Beasts']

    model = UnitDescription('Canoptek Wraith', options=[Gear('Vicious Claws')])
    power = 9

    class Wraith(Count):
        @property
        def description(self):
            return Wraiths.model.clone().\
                set_count(self.cur - sum(c.cur for c in self.parent.weps))

    def __init__(self, parent):
        super(Wraiths, self).__init__(parent)
        self.models = self.Wraith(self, 'Canoptek Wraith', 3, 6, get_cost(units.CanoptekWraiths), per_model=True)
        self.weps = [
            Count(self, 'Particle caster', 0, 3, get_cost(ranged.ParticleCaster), gear=Wraiths.model.clone().add(Gear('Particle caster'))),
            Count(self, 'Transdimensional beamer', 0, 3, get_cost(ranged.TBeamer), gear=Wraiths.model.clone().add(Gear('Transdimensional beamer'))),
            Count(self, 'Whip coils', 0, 3, get_cost(melee.WhipCoils), gear=Wraiths.model.clone().add(Gear('Whip coils')))
        ]

    def get_count(self):
        return self.models.cur

    def check_rules(self):
        super(Wraiths, self).check_rules()
        Count.norm_counts(0, self.models.cur, self.weps)

    def build_power(self):
        return self.power * (1 + (self.models.cur > 3))


class Scarabs(FastUnit, armory.DynastyUnit):
    type_name = get_name(units.CanoptekScarabs)
    type_id = 'Scarabs_v1'
    faction = ['Canoptek']
    keywords = ['Swarm']
    power = 2

    def __init__(self, parent):
        super(Scarabs, self).__init__(parent)
        self.models = Count(self, 'Canoptek Scarabs', 3, 9, get_cost(units.CanoptekScarabs), True,
                            gear=UnitDescription('Canoptek Scarab',
                                                 options=[Gear('Feeder mandibles')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * ((self.models.cur + 2) / 3)


class TombBlades(FastUnit, armory.DynastyUnit):
    type_name = get_name(units.TombBlades)
    type_id = 'tomb_blades_v1'
    keywords = ['Biker', 'Fly']
    power = 5

    class TombBlade(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(TombBlades.TombBlade.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Two gauss blasters', 2 * get_cost(ranged.GaussBlaster),
                             gear=[Gear('Gauss blaster', count=2)])
                self.variant('Two tesla carbines', 2 * get_cost(ranged.TeslaCarbine),
                             gear=[Gear('Tesla carbine', count=2)])
                self.variant(*ranged.ParticleBeamer)

        class Option1(OptionsList):
            def __init__(self, parent):
                super(TombBlades.TombBlade.Option1, self).__init__(parent, 'Options')
                self.variant(*wargear.Shieldvanes)

        class Option2(OptionsList):
            def __init__(self, parent):
                super(TombBlades.TombBlade.Option2, self).__init__(parent, '', limit=1)
                self.variant(*wargear.Shadowloom)
                self.variant(*wargear.Nebuloscope)

        def __init__(self, parent):
            super(TombBlades.TombBlade, self).__init__(parent, 'Tomb Blade', get_cost(units.TombBlades))
            self.Weapon(self)
            self.Option1(self)
            self.Option2(self)

    def __init__(self, parent):
        super(TombBlades, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.TombBlade, 3, 9)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * ((self.models.count + 2) / 3)


class Destroyers(FastUnit, armory.DynastyUnit):
    type_name = get_name(units.Destroyers)
    type_id = 'destroyers_v1'
    keywords = ['Infantry', 'Fly']
    power = 3

    class Upgrade(OptionsList):
        def __init__(self, parent):
            super(Destroyers.Upgrade, self).__init__(parent, 'Upgrade')
            self.variant('Heavy Destroyer', points_price(get_cost(units.Destroyers), ranged.HeavyGaussCannon),
                         gear=[])

    def __init__(self, parent):
        super(Destroyers, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Destroyers', 1, 6,
                            points_price(get_cost(units.Destroyers), ranged.GaussCannon), True,
                            gear=UnitDescription('Destroyer',
                                                 options=[Gear('Gauss cannon')]))
        self.up = self.Upgrade(self)

    def check_rules(self):
        super(Destroyers, self).check_rules()
        self.models.min, self.models.max = (2 if self.up.any else 1, 5 if self.up.any else 6)

    def get_count(self):
        return self.models.cur + (1 if self.up.any else 0)

    def build_description(self):
        desc = UnitDescription(self.name, self.points, self.count)
        desc.add(self.models.description)
        if self.up.any:
            desc.add(UnitDescription('Heavy Destroyer',
                                     options=[Gear('Heavy gauss cannon')]))
        return desc

    def build_power(self):
        return self.power * self.models.cur
