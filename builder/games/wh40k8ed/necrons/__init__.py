__author__ = 'Ivan Truskov'

from hq import Anrakyr, CommandBarge, Cryptek, DLord,\
    Imotekh, Lord, Obyron, Orikan, Overlord, Szeras,\
    Trazyn, Zahndrekh
from troops import Warriors, Immortals
from elites import Deathmarks, Deciever,\
    FlayedOnes, Lychguard, Nightbringer,\
    Praetorians, TriarchStalker
from fast import Destroyers, FastUnit,\
    Scarabs, TombBlades, Wraiths
from heavy import AnnihilationBarge, DoomsdayArk, HeavyDestroyers, HeavyUnit, \
    Monolith, Spyders, TranscendentCtan
from transport import GhostArk
from fliers import NightScythe, DoomScythe
from lords import TesseractVault, Obelisk
from ia_hq import Kutlakh, Toholk
from ia_elites import TombStalker
from ia_fast import TombSentinel, Acanthrites
from ia_fliers import NightShroud
from ia_heavy import SentryPylon, TesseractArk
from ia_lords import GaussPylon

ia_types = [Kutlakh, Toholk, TombStalker, TombSentinel, Acanthrites,
            NightShroud, SentryPylon, TesseractArk, GaussPylon]

unit_types = [Anrakyr, CommandBarge, Cryptek, DLord, Imotekh, Lord,
              Obyron, Orikan, Overlord, Szeras, Trazyn, Zahndrekh,
              Warriors, Immortals, Deathmarks, Deciever, FlayedOnes,
              Lychguard, Nightbringer, Praetorians, TriarchStalker,
              Destroyers, Scarabs, TombBlades,
              Wraiths, AnnihilationBarge, DoomsdayArk, HeavyDestroyers,
              Monolith, Spyders, TranscendentCtan,
              GhostArk, NightScythe, DoomScythe, TesseractVault,
              Obelisk] + ia_types
