__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HqUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, OptionsList
import armory, units, wargear, melee, ranged
from builder.games.wh40k8ed.utils import *


class Ahriman(HqUnit, armory.SonsUnit):
    type_name = u'Ahriman'
    type_id = 'ahriman_v1'
    keywords = ['Character', 'Infantry', 'Sorcerer', 'Psyker']
    power = 7

    class Disc(OptionsList):
        def __init__(self, parent):
            super(Ahriman.Disc, self).__init__(parent, 'Ride')
            self.variant('Disc of Tzeentch', points=get_cost(units.DiscAhriman) - get_cost(units.Ahriman),
                         gear=[UnitDescription('Disc of Tzeentch', options=[Gear('Blades')])])

    def __init__(self, parent):
        super(Ahriman, self).__init__(parent=parent, points=get_cost(units.Ahriman),
                                      unique=True, gear=[
                                          Gear('Frag grenades'), Gear('Krak grenades'),
                                          Gear("Staff of Ahriman"), Gear('Inferno bolt pistol')])
        self.disc = self.Disc(self)

    def build_power(self):
        return self.power + 2 * self.disc.any


class TzeentchDaemonPrince(HqUnit, armory.SonsUnit):
    type_name = get_name(units.DaemonPrinceOfTzeentch)
    type_id = 'dp_tzeentch_v1'
    kwname = 'Daemon Prince'
    keywords = ['Character', 'Monster', 'Daemon', 'Psyker']

    power = 8

    class Weapon(OneOf):
        def __init__(self, parent):
            super(TzeentchDaemonPrince.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*melee.HellforgedSword)
            self.variant(*melee.DaemonicAxe)
            self.variant(*melee.MaleficTalonsPair)

    class Options(OptionsList):
        def __init__(self, parent):
            super(TzeentchDaemonPrince.Options, self).__init__(parent, 'Options')
            self.wings = self.variant('Wings', get_cost(units.WingDaemonPrinceOfTzeentch) -
                                      get_cost(units.DaemonPrinceOfTzeentch))

    def __init__(self, parent):
        super(TzeentchDaemonPrince, self).__init__(parent, gear=[Gear('Malefic talons')],
                                           points=get_cost(units.DaemonPrinceOfTzeentch))
        self.Weapon(self)
        self.opt = self.Options(self)

    def build_power(self):
        return self.power + self.opt.wings.value


class ExSorcerer(HqUnit, armory.SonsUnit):
    type_name = get_name(units.ExaltedSorcerer)
    type_id = 'ex_sorcerer_v1'
    keywords = ['Character', 'Infantry', 'Sorcerer', 'Psyker']
    power = 7

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ExSorcerer.Weapon, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.ForceStave)
            self.variant(join_and(melee.ForceStave, melee.PowerSword),
                         get_costs(melee.ForceStave, melee.PowerSword),
                         gear=create_gears((melee.ForceStave, melee.PowerSword)))
            self.variant('Two power swords', 2 * get_cost(melee.PowerSword),
                         gear=create_gears(melee.PowerSword, melee.PowerSword))

    class Pistol(OneOf):
        def __init__(self, parent):
            super(ExSorcerer.Pistol, self).__init__(parent, 'Pistol')
            self.variant(*ranged.InfernoBoltPistol)
            self.variant(*ranged.PlasmaPistol)
            self.variant(*ranged.WarpflamePistol)

    class Disc(OptionsList):
        def __init__(self, parent):
            super(ExSorcerer.Disc, self).__init__(parent, 'Ride')
            self.variant('Disc of Tzeentch', points=get_cost(units.DiscExaltedSorcerer) - get_cost(units.ExaltedSorcerer), gear=[UnitDescription('Disc of Tzeentch', options=[Gear('Blades')])])

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.ExaltedSorcerer), *gear)
        super(ExSorcerer, self).__init__(parent=parent, points=cost,
                                         gear=create_gears(*gear))
        self.Weapon(self)
        self.Pistol(self)
        self.disc = self.Disc(self)

    def build_power(self):
        return self.power + 2 * self.disc.any


class TSSorcerer(HqUnit, armory.SonsUnit):
    type_name = 'Thousand Sons ' + get_name(units.Sorcerer)
    type_id = 'ts_sorc_v1'
    keywords = ['Character', 'Infantry', 'Psyker']
    power = 6

    class Pistol(OneOf):
        def __init__(self, parent):
            super(TSSorcerer.Pistol, self).__init__(parent, 'Pistol')
            self.variant(*ranged.InfernoBoltPistol)
            self.variant(*ranged.PlasmaPistol)
            self.variant(*ranged.WarpflamePistol)

    class Sword(OneOf):
        def __init__(self, parent):
            super(TSSorcerer.Sword, self).__init__(parent, 'Weapon')
            self.variant(*melee.ForceSword)
            self.variant(*melee.ForceStave)

    def __init__(self, parent):
        super(TSSorcerer, self).__init__(parent, name=get_name(units.Sorcerer),
                                         points=get_cost(units.Sorcerer),
                                         gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        self.Pistol(self)
        self.Sword(self)


class TSTermoSorcerer(HqUnit, armory.SonsUnit):
    type_name = 'Thousand Sons ' + get_name(units.TermoSorcerer)
    type_id = 'ts_termo_sorc_v1'
    kwname = 'Sorcerer'
    keywords = ['Character', 'Infantry', 'Terminator', 'Psyker']
    power = 8

    class Weapon(OneOf):
        def __init__(self, parent):
            super(TSTermoSorcerer.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.InfernoCombiBolter)
            self.variant(*melee.PowerSword)

    class Options(OptionsList):
        def __init__(self, parent):
            super(TSTermoSorcerer.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.Familiar)

    def __init__(self, parent):
        super(TSTermoSorcerer, self).__init__(parent, get_name(units.TermoSorcerer),
                                              points_price(get_cost(units.TermoSorcerer), melee.ForceStave),
                                              create_gears(melee.ForceStave))
        self.Weapon(self)
        self.Options(self)
