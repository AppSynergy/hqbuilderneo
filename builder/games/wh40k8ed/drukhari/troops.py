__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, SubUnit, OptionsList, Count
from builder.games.wh40k8ed.utils import *
import armory, ranged, melee, wargear, units


class KabaliteWarriors(TroopsUnit, armory.KabalUnit):
    type_name = u'Kabalite Warriors'
    type_id = 'kabalwar_v1'

    keywords = ['Infantry']
    power = 2

    member = UnitDescription('Kabalite Warrior')

    class Sybarite(Unit):
        type_name = u'Sybarite'

        class Options(OptionsList):
            def __init__(self, parent):
                super(KabaliteWarriors.Sybarite.Options, self).\
                    __init__(parent, name='Options')
                self.variant(*ranged.PhantasmGrenadeLauncher)

        class Melee(OptionsList):
            def __init__(self, parent):
                super(KabaliteWarriors.Sybarite.Melee, self).__init__(parent, 'Melee weapon',
                                                                      limit=1)
                self.variant(*melee.PowerSword)
                self.variant(*melee.Agoniser)

        class Ranged(OneOf):
            def __init__(self, parent):
                super(KabaliteWarriors.Sybarite.Ranged, self).\
                    __init__(parent, name='Ranged weapon')
                self.variant(*ranged.SplinterRifle)
                self.variant(*ranged.SplinterPistol)
                self.variant(*ranged.BlastPistol)

        def __init__(self, parent):
            super(KabaliteWarriors.Sybarite, self).\
                __init__(parent, points=get_cost(units.KabaliteWarriors))
            self.rng = self.Ranged(self)
            self.mle = self.Melee(self)
            self.opt = self.Options(self)

    class Special(OneOf):
        def __init__(self, parent):
            super(KabaliteWarriors.Special, self).__init__(parent, 'Special weapon')
            self.variant(*ranged.SplinterRifle)
            self.variant(*ranged.Shredder)
            self.variant(*ranged.Blaster)

        @property
        def description(self):
            return [KabaliteWarriors.member.clone().add(self.cur.gear)]

    class Heavy(OneOf):
        def __init__(self, parent):
            super(KabaliteWarriors.Heavy, self).__init__(parent, 'Heavy weapon')
            self.variant(*ranged.SplinterRifle)
            self.variant(*ranged.SplinterCannon)
            self.variant(*ranged.DarkLance)

        @property
        def description(self):
            return [KabaliteWarriors.member.clone().add(self.cur.gear)]

    def init_weapon_list(self):
        self.sp1 = self.Special(self)
        self.sp2 = self.Special(self)
        self.sp3 = self.Special(self)
        self.sp4 = self.Special(self)
        self.hv1 = self.Heavy(self)
        self.hv2 = self.Heavy(self)

    def __init__(self, parent):
        super(KabaliteWarriors, self).__init__(parent, name=self.type_name)
        self.wars = Count(self, 'Kabalite Warriors', 4, 19, points=get_cost(units.KabaliteWarriors),
                          per_model=True)
        self.ldr = SubUnit(self, self.Sybarite(parent=self))
        self.init_weapon_list()

    def check_rules(self):
        super(KabaliteWarriors, self).check_rules()
        self.sp2.used = self.sp2.visible = self.get_count() >= 10
        self.sp3.used = self.sp3.visible = self.get_count() >= 15
        self.sp4.used = self.sp4.visible = self.get_count() == 20
        self.hv1.used = self.hv1.visible = self.get_count() >= 10
        self.hv2.used = self.hv2.visible = self.get_count() == 20

    def get_all_spec(self):
        return [self.hv1, self.hv2, self.sp1, self.sp2, self.sp3, self.sp4]

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.ldr.description)
        spec_count = sum([o.used for o in self.get_all_spec()])
        desc.add(self.member.clone().add(Gear('Splinter rifle')).set_count(self.wars.cur - spec_count))
        for g in self.get_all_spec():
            if g.used:
                desc.add_dup(g.description)
        return desc

    def get_count(self):
        return self.wars.cur + 1

    def build_power(self):
        return self.power * ((self.get_count() + 4) / 5)


class Wyches(TroopsUnit, armory.CultUnit):
    type_name = u'Wyches'
    type_id = 'wych'

    keywords = ['Infantry']
    power = 2
    member = UnitDescription('Wych', options=[Gear('Plasma grenades')])

    class Hekatrix(Unit):
        type_name = u'Hekatrix'

        class Options(OptionsList):
            def __init__(self, parent):
                super(Wyches.Hekatrix.Options, self).\
                    __init__(parent, name='Options')
                self.variant(*ranged.PhantasmGrenadeLauncher)

        class Pistol(OneOf):
            def __init__(self, parent):
                super(Wyches.Hekatrix.Pistol, self).\
                    __init__(parent, name='Ranged weapon')
                self.variant(*ranged.SplinterPistol)
                self.variant(*ranged.BlastPistol)

        class Melee(OneOf):
            def __init__(self, parent):
                super(Wyches.Hekatrix.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.HekatariiBlade)
                self.variant(*melee.PowerSword)
                self.variant(*melee.Agoniser)

        def __init__(self, parent):
            super(Wyches.Hekatrix, self).\
                __init__(parent, points=get_cost(units.Wyches), gear=[Gear('Darklight grenades')])
            self.mle = self.Melee(self)
            self.rng = self.Pistol(self)
            self.opt = self.Options(self)

    class WychWeapon(OneOf):
        def __init__(self, parent):
            super(Wyches.WychWeapon, self).__init__(parent, 'Wych weapons')
            self.variant(join_and(ranged.SplinterPistol, melee.HekatariiBlade),
                         gear=create_gears(ranged.SplinterPistol, melee.HekatariiBlade))
            armory.add_wych_weapons(self)

        @property
        def description(self):
            return [Wyches.member.clone().add(self.cur.gear)]

    def __init__(self, parent):
        super(Wyches, self).__init__(parent, self.type_name)
        self.ldr = SubUnit(self, self.Hekatrix(parent=self))
        self.wars = Count(self, 'Wyches', 4, 19, points=get_cost(units.Wyches), per_model=True)
        self.ww1 = self.WychWeapon(self)
        self.ww2 = self.WychWeapon(self)
        self.ww3 = self.WychWeapon(self)

    def get_count(self):
        return self.wars.cur + 1

    def check_rules(self):
        super(Wyches, self).check_rules()
        self.ww2.used = self.ww2.visible = self.get_count() >= 10
        self.ww3.used = self.ww3.visible = self.get_count() >= 10

    def get_all_spec(self):
        return [self.ww1, self.ww2, self.ww3]

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.ldr.description)
        spec_count = sum([(o.used) for o in self.get_all_spec()])
        desc.add(self.member.clone().add([Gear('Splinter pistol'), Gear('Hekatarii blade')]).set_count(self.wars.cur - spec_count))
        for g in self.get_all_spec():
            if g.used:
                desc.add_dup(g.description)
        return desc

    def build_power(self):
        return self.power * ((self.wars.cur + 5) / 5)


class Wracks(TroopsUnit, armory.CovenUnit):
    type_name = u'Wracks'
    type_id = 'wracks_v1'

    keywords = ['Infantry']
    power = 3

    member = UnitDescription('Wrack')

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Wracks.Weapon, self).__init__(parent, 'Weapon')
            self.tool = self.variant(*melee.HaemonculusTools)
            self.variant(*ranged.LiquifierGun)
            self.variant(*ranged.Ossefactor)

        @property
        def description(self):
            return Wracks.member.clone().add(self.cur.gear)

    class Acothyst(Unit):
        class WeaponMelee(OneOf):
            def __init__(self, parent):
                super(Wracks.Acothyst.WeaponMelee, self).__init__(parent, 'Weapons of Torture')
                self.variant(*melee.HaemonculusTools)
                armory.add_torture_weapons(self)

        class WeaponRanged(OptionsList):
            def __init__(self, parent):
                super(Wracks.Acothyst.WeaponRanged, self).__init__(parent, 'Ranged weapon', limit=1)
                armory.add_torment_tool(self)

        def __init__(self, parent):
            super(Wracks.Acothyst, self).\
                __init__(parent, 'Acothyst', points=get_cost(units.Wracks))
            self.WeaponMelee(self)
            self.WeaponRanged(self)

    def __init__(self, parent):
        super(Wracks, self).__init__(parent)
        self.ldr = SubUnit(self, self.Acothyst(parent=self))
        self.wars = Count(self, 'Wracks', 4, 9, points=get_cost(units.Wracks), per_model=True)
        self.wep1 = self.Weapon(self)
        self.wep2 = self.Weapon(self)

    def get_count(self):
        return self.wars.cur + 1

    def check_rules(self):
        super(Wracks, self).check_rules()
        self.wep2.used = self.wep2.visible = self.get_count() > 4

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.ldr.description)
        spec_count = sum([(o.used) for o in [self.wep1, self.wep2]])
        desc.add(self.member.clone().add([Gear('Haemonculus tools')]).set_count(self.wars.cur - spec_count))
        for g in [self.wep1, self.wep2]:
            if g.used:
                desc.add_dup(g.description)
        return desc

    def build_power(self):
        return self.power + 2 * (self.wars.cur > 4)
