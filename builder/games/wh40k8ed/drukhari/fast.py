__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, OptionsList,\
    Gear, SubUnit, UnitDescription, Count, ListSubUnit, UnitList
import armory, melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *


class ClawedFiends(FastUnit, armory.DEUnit):
    type_name = get_name(units.ClawedFiends)
    type_id = 'fiends_v1'

    keywords = ['Beast']

    @classmethod
    def calc_faction(cls):
        return ['YNNARI']

    def __init__(self, parent):
        super(ClawedFiends, self).__init__(parent)
        self.models = Count(self, 'Clawed Fiends', 1, 6, points=get_cost(units.ClawedFiends), per_model=True,
                            gear=UnitDescription('Clawed Fiend', options=[Gear('Bludgeoning fists')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return (10 if self.models.cur > 3 else
                (5 if self.models.cur > 1 else 2))


class Khymerae(FastUnit, armory.DEUnit):
    type_name = get_name(units.Khymerae)
    type_id = 'khymerae_v1'

    keywords = ['Beast']

    @classmethod
    def calc_faction(cls):
        return ['YNNARI']

    class KymeraPair(Count):
        @property
        def description(self):
            return UnitDescription('Khymera', options=[Gear('Claws and taloins')]).set_count(2 * self.cur)

    def __init__(self, parent):
        super(Khymerae, self).__init__(parent)
        self.models = self.KymeraPair(self, 'Pair of Khymerae', 1, 5, points=2 * get_cost(units.Khymerae))

    def get_count(self):
        return self.models.cur * 2

    def build_power(self):
        return self.models.cur


class RazorwingFlocks(FastUnit, armory.DEUnit):
    type_name = get_name(units.RazorwingFlocks)
    type_id = 'razorwings_v1'

    keywords = ['Beast', 'Swarm', 'Fly']

    @classmethod
    def calc_faction(cls):
        return ['YNNARI']

    def __init__(self, parent):
        super(RazorwingFlocks, self).__init__(parent)
        self.models = Count(self, 'Razorwing flocks', 3, 12, points=get_cost(units.RazorwingFlocks), per_model=True,
                            gear=UnitDescription('Razorwing flock', options=[Gear('Claws and talons')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return (7 if self.models.cur > 6 else
                (4 if self.models.cur > 3 else 2))


class Reavers(FastUnit, armory.CultUnit):
    type_name = get_name(units.Reavers)
    type_id = 'reavers_v1'

    keywords = ['Biker', 'Fly']
    power = 3

    class Champ(Unit):
        class Melee(OptionsList):
            def __init__(self, parent):
                super(Reavers.Champ.Melee, self).__init__(parent, 'Melee weapons', limit=1)
                self.variant(*melee.PowerSword)
                self.variant(*melee.Agoniser)

        def __init__(self, parent):
            super(Reavers.Champ, self).__init__(parent, 'Arena Champion', points=get_cost(units.Reavers), gear=[
                Gear('Splinter pistol'), Gear('Splinter rifle'), Gear('Bladevanes')])
            self.Melee(self)

    class Reaver(ListSubUnit):

        class Options(OptionsList):
            def __init__(self, parent):
                super(Reavers.Reaver.Options, self).__init__(parent, 'Options', limit=1)
                self.variant(*wargear.GravTalon)
                self.variant(*wargear.ClusterCaltrops)

        class Ranged(OneOf):
            def __init__(self, parent):
                super(Reavers.Reaver.Ranged, self).__init__(parent, 'Jetbike weapon')
                self.rifle = self.variant(*ranged.SplinterRifle)
                self.variant(*ranged.HeatLance)
                self.variant(*ranged.Blaster)

        def __init__(self, parent):
            super(Reavers.Reaver, self).__init__(parent, 'Reaver', points=get_cost(units.Reavers),
                                                 gear=[Gear('Splinter pistol'), Gear('Bladevanes')])
            self.bw = self.Ranged(self)
            self.opt = self.Options(self)

        @ListSubUnit.count_gear
        def count_bw(self):
            return self.bw.cur != self.bw.rifle

        @ListSubUnit.count_gear
        def count_opt(self):
            return self.opt.any

    def __init__(self, parent):
        super(Reavers, self).__init__(parent, 'Reavers')
        self.ldr = SubUnit(self, self.Champ(parent=self))
        self.warriors = UnitList(self, self.Reaver, 2, 11)

    def get_count(self):
        return self.warriors.count + 1

    def check_rules(self):
        super(Reavers, self).check_rules()

        bweps = sum(u.count_bw() for u in self.warriors.units)
        if bweps > (self.get_count() / 3):
            self.error('Only 1 special bike weapon for 3 models allowed; taken: {}'.format(bweps))

        opts = sum(u.count_opt() for u in self.warriors.units)
        if opts > (self.get_count() / 3):
            self.error('Only 1 piece of equipment per 3 models allowed; taken: {}'.format(opts))

    def build_power(self):
        return (13 if self.get_count() > 9 else
                (10 if self.get_count() > 6 else
                 (6 if self.get_count() > 3 else self.power)))


class Hellions(FastUnit, armory.CultUnit):
    type_name = get_name(units.Hellions)
    type_id = 'hellions_v1'

    keywords = ['Infantry', 'Fly']

    class Helliarch(Unit):

        class Options(OptionsList):
            def __init__(self, parent):
                super(Hellions.Helliarch.Options, self).__init__(parent, 'Options')
                self.variant(*ranged.PhantasmGrenadeLauncher)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Hellions.Helliarch.Weapon, self).__init__(parent, 'Weapon')
                self.variant(*melee.Hellglaive)
                self.variant('Splinter pistol and stunclaw', points=get_cost(melee.Stunclaw),
                             gear=[Gear('Splinter pistol'), Gear('Stunclaw')])
                self.variant('Splinter pistol and power sword', points=get_cost(melee.PowerSword),
                             gear=[Gear('Splinter pistol'), Gear('Power sword')])
                self.variant('Splinter pistol and agonizer', points=get_cost(melee.Agoniser),
                             gear=[Gear('Splinter pistol'), Gear('Agonizer')])

        def __init__(self, parent):
            super(Hellions.Helliarch, self).__init__(parent, 'Helliarch', points=get_cost(units.Hellions),
                                                     gear=[Gear('Splinter pods')])
            self.Weapon(self)
            self.Options(self)

    def __init__(self, parent):
        super(Hellions, self).__init__(parent, 'Hellions')
        self.leader = SubUnit(self, self.Helliarch(parent=self))
        self.warriors = Count(self, 'Hellions', 4, 19, points=get_cost(units.Hellions), per_model=True,
                              gear=UnitDescription('Hellion', options=[Gear('Hellglaive'), Gear('Splinter pods')]))

    def get_count(self):
        return self.warriors.cur + 1

    def build_power(self):
        return (14 if self.get_count() > 15 else
                (10 if self.get_count() > 10 else
                 (7 if self.get_count() > 5 else 3)))


class Scourges(FastUnit, armory.DEUnit):
    type_name = get_name(units.Scourges)
    type_id = 'scourges_v1'

    member = UnitDescription('Scourge', options=[Gear('Plasma grenades')])
    keywords = ['Infantry', 'Fly']
    power = 5

    @classmethod
    def calc_faction(cls):
        return ['YNNARI']

    class Solarite(Unit):
        class Ranged(OneOf):
            def __init__(self, parent):
                super(Scourges.Solarite.Ranged, self).__init__(parent, 'Ranged weapon')
                self.carbine = self.variant(*ranged.Shardcarbine)
                self.variant(*ranged.SplinterPistol)
                self.variant(*ranged.BlastPistol)

        class Melee(OptionsList):
            def __init__(self, parent):
                super(Scourges.Solarite.Melee, self).__init__(parent, 'Melee weapon', limit=1)
                self.variant(*melee.VenomBlade)
                self.variant(*melee.PowerLance)
                self.variant(*melee.Agoniser)

        def __init__(self, parent):
            super(Scourges.Solarite, self).__init__(parent, 'Solarite', points=get_cost(units.Scourges),
                                                    gear=[Gear('Plasma grenades')])
            self.rng = self.Ranged(self)
            self.mle = self.Melee(self)

        def check_rules(self):
            super(Scourges.Solarite, self).check_rules()
            self.mle.visible = self.mle.used = not self.rng.cur == self.rng.carbine

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Scourges.Weapon, self).__init__(parent, 'Ranged weapon')
            self.carbine = self.variant(*ranged.Shardcarbine)
            self.variant(*ranged.SplinterCannon)
            self.variant(*ranged.DarkLance)
            self.variant(*ranged.HeatLance)
            self.variant(*ranged.Shredder)
            self.variant(*ranged.HaywireBlaster)
            self.variant(*ranged.Blaster)

    def __init__(self, parent):
        super(Scourges, self).__init__(parent, 'Scourges')
        self.leader = SubUnit(self, self.Solarite(parent=self))
        self.wars = Count(self, 'Scourges', 4, 9, points=get_cost(units.Scourges), per_model=True)
        self.hweps = [self.Weapon(self) for i in range(0, 4)]

    def get_count(self):
        return self.wars.cur + 1

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.leader.description)
        spec_count = sum([(not o.cur == o.carbine) for o in self.hweps])
        desc.add(Scourges.member.clone().add([Gear('Shardcarbine')]).set_count(self.wars.cur - spec_count))
        for g in self.hweps:
            if not g.cur == g.carbine:
                desc.add_dup(Scourges.member.clone().add(g.description))
        return desc

    def build_power(self):
        return self.power + 3 * (self.wars.cur > 4)
