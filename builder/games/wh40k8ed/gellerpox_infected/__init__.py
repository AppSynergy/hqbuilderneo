from units import Vulgrar, VoxShamblers, Hullbreakers, Glitchlings,\
    Cursemites, EyestingerSwarms, SludgeGrubs
from builder.games.wh40k8ed.rosters import DetachPatrol, DetachOutrider, DetachAuxilary


class DetachPatrol_gellerpox_infected(DetachPatrol):
    army_name = u'Gellerpox infected (Patrol detachment)'
    faction_base = u'GELLERPOX INFECTED'
    army_id = u'patrol_gellerpox_infected'
    army_factions = [u'GELLERPOX INFECTED', u'CHAOS']

    def __init__(self, parent=None):
        super(DetachPatrol_gellerpox_infected, self).__init__(parent=parent, hq=True, elite=True, troops=True, fast=True)
        self.troops.add_classes([VoxShamblers, Glitchlings])
        self.elite.add_classes([Hullbreakers])
        self.fast.add_classes([Cursemites, EyestingerSwarms, SludgeGrubs])
        self.hq.add_classes([Vulgrar])


class DetachOutrider_gellerpox_infected(DetachOutrider):
    army_name = u'Gellerpox infected (Outrider detachment)'
    faction_base = u'GELLERPOX INFECTED'
    army_id = u'outrider_gellerpox_infected'
    army_factions = [u'GELLERPOX INFECTED', u'CHAOS']

    def __init__(self, parent=None):
        super(DetachOutrider_gellerpox_infected, self).__init__(parent=parent, hq=True, elite=True, troops=True, fast=True)
        self.troops.add_classes([VoxShamblers, Glitchlings])
        self.elite.add_classes([Hullbreakers])
        self.fast.add_classes([Cursemites, EyestingerSwarms, SludgeGrubs])
        self.hq.add_classes([Vulgrar])


class DetachAuxilary_gellerpox_infected(DetachAuxilary):
    army_name = u'Gellerpox infected (Auxilary Support detachment)'
    faction_base = u'GELLERPOX INFECTED'
    army_id = u'auxilary_gellerpox_infected'
    army_factions = [u'GELLERPOX INFECTED', u'CHAOS']

    def __init__(self, parent=None):
        super(DetachAuxilary_gellerpox_infected, self).__init__(parent=parent, hq=True, elite=True, troops=True, fast=True)
        self.troops.add_classes([VoxShamblers, Glitchlings])
        self.elite.add_classes([Hullbreakers])
        self.fast.add_classes([Cursemites, EyestingerSwarms, SludgeGrubs])
        self.hq.add_classes([Vulgrar])


unit_types = [Vulgrar, VoxShamblers, Hullbreakers, Glitchlings,
              Cursemites, EyestingerSwarms, SludgeGrubs]
detachments = [DetachPatrol_gellerpox_infected,
               DetachOutrider_gellerpox_infected,
               DetachAuxilary_gellerpox_infected]
