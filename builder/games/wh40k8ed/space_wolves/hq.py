from builder.games.wh40k8ed.space_marines.hq import Captain, TerminatorCaptain,\
    GravisCaptain, BikeCaptain
from builder.games.wh40k8ed.unit import HqUnit
from builder.games.wh40k8ed.options import OneOf, Gear, OptionsList, UnitDescription
import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class Logan(HqUnit, armory.SWUnit):
    type_name = u'Logan Grimnar'
    type_id = 'logan_v1'
    keywords = ['Character', 'Infantry', 'Chapter master', 'Terminator', 'Wolf lord']
    power = 9

    def __init__(self, parent):
        super(Logan, self).__init__(parent, points=get_cost(units.LoganGrimnar), static=True,
                                    gear=[Gear('Storm bolter'),
                                          Gear('The Axe Morkai')])

    def get_unique(self):
        return 'LOGAN GRIMNAR'


class PimpLogan(HqUnit, armory.SWUnit):
    type_name = u'Logan Grimnar on Stormrider'
    type_id = 'pimp_logan_v1'
    kwname = 'Logan Grimnar'
    keywords = ['Character', 'Infantry', 'Chapter master', 'Stormrider', 'Wolf lord']
    power = 10

    def __init__(self, parent):
        super(PimpLogan, self).__init__(parent, points=get_cost(units.LoganGrimnarOnStormrider), static=True,
                                        gear=[Gear('Storm bolter'),
                                              Gear('The Axe Morkai'),
                                              UnitDescription('Tyrnak & Fenrir', options=[
                                                  Gear('Flurry of teeth and claws')
                                              ])])

    def get_unique(self):
        return 'LOGAN GRIMNAR'


class Arjac(HqUnit, armory.SWUnit):
    type_name = u'Arjac Rockfist'
    type_id = 'arjac_v1'
    keywords = ['Character', 'Infantry', 'Terminator', 'Wolf Guard']
    power = 7

    def __init__(self, parent):
        super(Arjac, self).__init__(parent, gear=[Gear('Foehammer')],
                                    static=True, unique=True, points=get_cost(units.ArjacRockfist))


class Bjorn(HqUnit, armory.SWUnit):
    type_id = 'bjorn_v1'
    type_name = u'Bjorn the Fell-Handed'
    keywords = ['Character', 'Vehicle', 'Dreadnought']
    power = 12

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Bjorn.Weapon, self).__init__(parent, 'Ranged weapon')
            self.variant(*ranged.AssaultCannon)
            self.variant(*ranged.HelfrostCannon)
            self.variant(*ranged.HeavyPlasmaCannon)
            self.variant(*ranged.TwinLascannon)

    def __init__(self, parent):
        super(Bjorn, self).__init__(parent, unique=True, points=points_price(get_cost(units.BjornTheFellHanded), ranged.HeavyFlamer),
                                    gear=[Gear('Trueclaw'), Gear('Heavy flamer')])
        self.Weapon(self)

    def build_statistics(self):
        res = super(Bjorn, self).build_statistics()
        res['Command points'] = 1
        return res


class Njal(HqUnit, armory.SWUnit):
    type_id = 'njal_v1'
    type_name = u'Njal Stormcaller'
    keywords = ['Infantry', 'Character', 'Psyker', 'Rune Priest']
    power = 7

    def __init__(self, parent):
        super(Njal, self).__init__(parent,
                                   static=True, gear=[
                                       Gear('Staff of the Stormcaller'),
                                       Gear('Bolt pistol'),
                                       Gear('Frag grenades'),
                                       Gear('Krak grenades'),
                                       Gear('Nightwing'),
                                   ], points=get_cost(units.NjalStormcaller))

    def get_unique(self):
        return 'NJAL STORMCALLER'


class TermoNjal(HqUnit, armory.SWUnit):
    type_id = 'termo_njal_v1'
    type_name = u'Njal Stormcaller in Runic Terminator Armour'
    kwname = 'Njal Stormcaller'
    keywords = ['Infantry', 'Character', 'Psyker', 'Rune Priest', 'Terminator']
    power = 8

    def __init__(self, parent):
        super(TermoNjal, self).__init__(parent,
                                        static=True, gear=[
                                            Gear('Staff of the Stormcaller'),
                                            Gear('Bolt pistol'),
                                            Gear('Nightwing'),
                                        ], points=get_cost(units.NjalStormcallerInRunicTerminatorArmour))

    def get_unique(self):
        return 'NJAL STORMCALLER'


class TermRunePriest(HqUnit, armory.SWUnit):
    type_name = u'Rune Priest in Terminator Armour'
    type_id = 'term_rune_priest_v1'
    kwname = 'Rune Priest'
    keywords = ['Infantry', 'Character', 'Psyker', 'Terminator']
    power = 8

    class Weapon2(OptionsList):
        def __init__(self, parent):
            super(TermRunePriest.Weapon2, self).__init__(parent, 'Ranged weapon')
            armory.add_combi_weapons(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(TermRunePriest.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.PsychicHood)
            self.variant(*wargear.RunicTerminatorArmour)

    def __init__(self, parent):
        super(TermRunePriest, self).__init__(parent, points=get_cost(units.RunePriestInTerminatorArmour))
        RunePriest.Weapon1(self)
        self.Weapon2(self)
        self.opt = self.Options(self)


class PrimarisRunePriest(HqUnit, armory.SWUnit):
    type_name = get_name(units.PrimarisRunePriest)
    type_id = 'primaris_rune_priest_v1'
    kwname = 'Rune priest'
    keywords = ['Character', 'Infantry', 'Primaris', 'Psyker']
    power = 7

    def __init__(self, parent):
        gear = [melee.RunicSword, ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.PrimarisRunePriest), *gear)
        super(PrimarisRunePriest, self).__init__(parent, points=cost, gear=create_gears(*gear),
                                                static=True)


class RunePriest(HqUnit, armory.SWUnit):
    type_name = u'Rune Priest'
    type_id = 'rune_priest_v1'
    keywords = ['Infantry', 'Character', 'Psyker']
    power = 6

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(RunePriest.Weapon1, self).__init__(parent, 'Melee Weapon')
            self.variant(*melee.RunicAxe)
            self.variant(*melee.RunicStave)
            self.variant(*melee.RunicSword)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(RunePriest.Weapon2, self).__init__(parent, 'Ranged weapon')
            self.variant('Bolt pistol')
            self.variant(*ranged.PlasmaPistol)
            # armory.add_combi_weapons(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(RunePriest.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.PsychicHood)
            self.variant(*wargear.RunicArmour)
            self.pack = self.variant('Jump pack', get_cost(units.RunePriestWithJumpPack) - get_cost(units.RunePriest))

    def __init__(self, parent):
        super(RunePriest, self).__init__(parent, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'),
        ], points=get_cost(units.RunePriest))
        self.Weapon1(self)
        self.Weapon2(self)
        self.opt = self.Options(self)

    def build_power(self):
        return self.power + self.opt.pack.value


class BikeRunePriest(HqUnit, armory.SWUnit):
    type_name = u'Rune Priest on Bike (Index)'
    type_id = 'bike_rune_priest_v1'
    kwname = 'Rune Priest'
    keywords = ['Biker', 'Character', 'Psyker']
    power = 8

    class Options(OptionsList):
        def __init__(self, parent):
            super(BikeRunePriest.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.PsychicHood)
            self.variant(*wargear.RunicArmour)

    def __init__(self, parent):
        super(BikeRunePriest, self).__init__(parent, name='Rune Priest on Bike', gear=[
            Gear('Frag grenades'), Gear('Krak grenades'),
            Gear('Twin boltgun')
        ], points=points_price(get_cost(units.RunePriestOnBike), ranged.TwinBoltgun))
        RunePriest.Weapon1(self)
        RunePriest.Weapon2(self)
        self.opt = self.Options(self)




class Ulrik(HqUnit, armory.SWUnit):
    type_id = 'ulrik_v1'
    type_name = u'Ulrik the Slayer'
    keywords = ['Character', 'Infantry', 'Wolf Priest']
    power = 6

    def __init__(self, parent):
        super(Ulrik, self).__init__(parent,
                                    unique=True, static=True, gear=[
                                        Gear('Crozius arcanum'),
                                        Gear('Plasma pistol'),
                                        Gear('Frag grenades'),
                                        Gear('Krak grenades'),
                                    ], points=get_cost(units.UlrikTheSlayer))


    # def check_rules(self):
    #     super(WolfPriest, self).check_rules()
    #     if self.pack.pack.used:
    #         self.keywords.extend(['Fly', 'Jump Pack'])


class TerminatorWolfPriest(HqUnit, armory.SWUnit):
    type_name = u'Wolf Priest in Terminator Armour'
    type_id = 'termo_wolf_priest_v1'

    kwname = 'Wolf Priest'
    keywords = ['Character', 'Infantry', 'Terminator']
    power = 6

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(TerminatorWolfPriest.Weapon1, self).__init__(parent, 'Weapon')
            # self.variant(*ranged.StormBolter)
            armory.add_term_combi_weapons(self)

    def __init__(self, parent):
        super(TerminatorWolfPriest, self).__init__(parent, points=get_cost(units.WolfPriestInTerminatorArmour), gear=[Gear('Crozius Arcanum')])
        self.Weapon1(self)


class PrimarisWolfPriest(HqUnit, armory.SWUnit):
    type_name = get_name(units.PrimarisWolfPriest)
    type_id = 'primaris_wolf_priest_v1'
    kwname = 'Wolf Priest'
    keywords = ['Character', 'Infantry', 'Primaris']
    power = 6

    def __init__(self, parent):
        gear = [melee.Crozius, ranged.AbsolvorBoltPistol, ranged.FragGrenades, ranged.KrakGrenades]
        cost = points_price(get_cost(units.PrimarisWolfPriest), *gear)
        super(PrimarisWolfPriest, self).__init__(parent, points=cost, gear=create_gears(*gear),
                                                 static=True)


class WolfPriest(HqUnit, armory.SWUnit):
    type_name = u'Wolf Priest'
    type_id = 'wolf_priest_v1'

    keywords = ['Character', 'Infantry']
    power = 5

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(WolfPriest.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Bolt pistol')
            self.variant(*ranged.PlasmaPistol)
            # armory.add_combi_weapons(self)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(WolfPriest.Pack, self).__init__(parent, 'Options')
            self.variant(*melee.PowerFist)
            self.pack = self.variant('Jump pack', get_cost(units.WolfPriestWithJumpPack) - get_cost(units.WolfPriest))

    def __init__(self, parent):
        super(WolfPriest, self).__init__(parent, points=get_cost(units.WolfPriest), gear=[
            Gear('Frag grenades'), Gear('Krak grenades'), Gear('Crozius Arcanum')
        ])
        self.Weapon1(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + 1 * self.pack.pack.value


class BikeWolfPriest(HqUnit, armory.SWUnit):
    type_name = u'Wolf Priest on Bike (Index)'
    type_id = 'bike_wolf_priest_v1'

    kwname = 'Wolf Priest'
    keywords = ['Character', 'Biker']
    power = 7

    def __init__(self, parent):
        super(BikeWolfPriest, self).__init__(parent, name='Wolf Priest on Bike',
                                             points=points_price(get_cost(units.WolfPriestOnBike), ranged.TwinBoltgun), gear=[
                                                 Gear('Frag grenades'), Gear('Krak grenades'), Gear('Crozius Arcanum'), Gear('Twin boltgun'),
                                             ])
        WolfPriest.Weapon1(self)


class Ragnar(HqUnit, armory.SWUnit):
    type_id = 'ragnar_v1'
    type_name = u'Ragnar Blackmane'
    keywords = ['Character', 'Infantry', 'Wolf Lord']
    power = 7

    class Wolves(OptionsList):
        def __init__(self, parent):
            super(Ragnar.Wolves, self).__init__(parent, 'Options')
            self.variant('2 Fenrisian Wolves', get_cost(units.RagnarWolves) * 2, gear=[
                UnitDescription('Svangir & Ulfgir', options=[Gear('Teeth and claws')])])

    def __init__(self, parent):
        super(Ragnar, self).__init__(parent,
                                     points=get_cost(units.RagnarBlackmane), unique=True,
                                     gear=[
                                         Gear('Frostfang'),
                                         Gear('Bolt pistol'),
                                         Gear('Frag grenades'),
                                         Gear('Krak grenades'),
                                     ])
        self.wolves = self.Wolves(self)

    def build_power(self):
        return self.power + self.wolves.any


class Krom(HqUnit, armory.SWUnit):
    type_id = 'krom_v1'
    type_name = u'Krom Dragongaze'
    keywords = ['Character', 'Infantry', 'Wolf Lord']
    power = 5

    def __init__(self, parent):
        super(Krom, self).__init__(parent, unique=True,
                                   gear=[
                                       Gear('Wyrmclaw'),
                                       Gear('Bolt pistol'),
                                       Gear('Frag grenades'),
                                       Gear('Krak grenades'),
                                   ], static=True, points=get_cost(units.KromDragongaze))


class Harald(HqUnit, armory.SWUnit):
    type_id = 'harald_v1'
    type_name = u'Harald Deathwolf'
    keywords = ['Cavalry', 'Character', 'Thunderwolf', 'Wolf Lord']
    power = 10

    def __init__(self, parent):
        super(Harald, self).__init__(parent, unique=True,
                                     gear=[
                                         Gear('Glacius'),
                                         Gear('Bolt pistol'),
                                         Gear('Frag grenades'),
                                         Gear('Krak grenades'),
                                         UnitDescription('Icetooth', options=[
                                             Gear('Crushing teeth and claws')
                                         ])
                                     ], static=True, points=get_cost(units.HaraldDeathwolf))


class WolfLord(armory.WolfClawUser, HqUnit, armory.SWUnit):
    type_name = get_name(units.WolfLord)
    type_id = 'wolf_lord_v1'
    keywords = ['Character', 'Infantry']
    power = 5

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(WolfLord.Weapon1, self).__init__(parent, 'Weapon 1')
            self.variant(*ranged.MCBoltgun)
            self.variant(*ranged.BoltPistol)
            self.variant(*ranged.PlasmaPistol)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(WolfLord.Weapon2, self).__init__(parent, 'Weapon2')
            armory.add_melee_weapons(self)
            self.variant(*wargear.StormShieldCharacter)
            # self.variant('Relic blade', 21)

    class Pack(OptionsList):
        def __init__(self, parent):
            super(WolfLord.Pack, self).__init__(parent, 'Pack')
            self.pack = self.variant('Jump pack', get_cost(units.WolfLordJumpPack) - get_cost(units.WolfLord))

    def __init__(self, parent):
        super(WolfLord, self).__init__(parent, points=get_cost(units.WolfLord), gear=[
            Gear('Frag grenades'), Gear('Krak grenades')
        ])
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.pack = self.Pack(self)

    def build_power(self):
        return self.power + 1 * self.pack.any


class ThunderwolfLord(armory.WolfClawUser, HqUnit, armory.SWUnit):
    type_name = get_name(units.WolfLordOnThunderwolf)
    type_id = 'wolf_wolf_lord_v1'
    kwname = 'Wolf Lord'
    keywords = ['Cavalry', 'Character', 'Thunderwolf']
    power = 7

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(ThunderwolfLord.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*melee.Chainsword)
            self.variant(*ranged.PlasmaPistol)
            self.variant(*wargear.StormShieldCharacter)
            armory.add_melee_weapons(self, chain=False)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(ThunderwolfLord.Weapon2, self).__init__(parent, '')
            self.variant('Bolt pistol')
            self.variant('Boltgun')
            self.variant(*ranged.PlasmaPistol)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    def __init__(self, parent):
        super(ThunderwolfLord, self).__init__(parent, gear=[
            Gear('Frag grenades'), Gear('Krak grenades'),
            UnitDescription('Thunderwolf', options=[Gear('Crushing teeth and claws')])
        ], points=get_cost(units.WolfLordOnThunderwolf))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)


class TermWolfLord(armory.WolfClawUser, HqUnit, armory.SWUnit):
    type_name = get_name(units.WolfLordTerminator)
    type_id = 'term_wolf_lord_v1'
    power = 7
    kwname = 'Wolf Lord'
    keywords = ['Character', 'Infantry', 'Terminator']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(TermWolfLord.Weapon1, self).__init__(parent, 'Weapon')
            armory.add_term_combi_weapons(self)
            armory.add_term_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(TermWolfLord.Weapon2, self).__init__(parent, 'Weapon')
            self.variant(*melee.PowerSword)
            self.variant(*wargear.StormShieldCharacter)
            armory.add_term_melee_weapons(self, sword=False)

    class Launcher(OptionsList):
        def __init__(self, parent):
            super(TermWolfLord.Launcher, self).__init__(parent, 'Launcher')
            self.variant(*ranged.WristMountedGrenadeLauncher)

    def __init__(self, parent):
        super(TermWolfLord, self).__init__(parent,
                                           points=get_cost(units.WolfLordTerminator))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.launcher = self.Launcher(self)

    def check_rules(self):
        super(TermWolfLord, self).check_rules()
        fist = self.wep1.fist == self.wep1.cur or self.wep2.fist == self.wep2.cur
        self.launcher.visible = self.launcher.used = fist


class CataphractiiWolfLord(armory.WolfClawUser, HqUnit, armory.SWUnit):
    type_name = get_name(units.WolfLordCataphractii)
    type_id = 'cataphractii_wolf_lord_v1'
    kwname = 'Wolf Lord'
    keywords = ['Character', 'Infantry', 'Terminator']
    power = 8

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CataphractiiWolfLord.Weapon1, self).__init__(parent, 'Bolter')
            self.variant(*ranged.CombiBolter)
            armory.add_term_combi_weapons(self)
            armory.add_term_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CataphractiiWolfLord.Weapon2, self).__init__(parent, 'Weapon')
            armory.add_term_melee_weapons(self)

    def __init__(self, parent):
        super(CataphractiiWolfLord, self).__init__(parent,
                                                   points=get_cost(units.WolfLordCataphractii))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)


class PrimarisWolfLord(HqUnit, armory.SWUnit):
    type_name = get_name(units.PrimarisWolfLord)
    type_id = 'primaris_wolf_lord_v1'
    kwname = 'Wolf Lord'
    keywords = ['Character', 'Infantry', 'Primaris']
    power = 6

    @classmethod
    def calc_faction(cls):
        return super(PrimarisWolfLord, cls).calc_faction()

    class RangedWeapon(OneOf):
        def __init__(self, parent):
            super(PrimarisWolfLord.RangedWeapon, self).__init__(parent, 'Ranged weapon')
            self.variant(*ranged.MCAutoBoltRifle,
                         gear=create_gears(ranged.MCAutoBoltRifle, ranged.BoltPistol))
            self.variant(*ranged.MCStalkerBoltRifle,
                         gear=create_gears(ranged.MCAutoBoltRifle, ranged.BoltPistol))

    class Melee(OptionsList):
        def __init__(self, parent):
            super(PrimarisWolfLord.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.PowerSword)
            self.fist = self.variant(join_and(melee.PowerFist, ranged.PlasmaPistol),
                                     get_costs(melee.PowerFist, ranged.PlasmaPistol),
                                     gear=create_gears(melee.PowerFist, ranged.PlasmaPistol))

    def __init__(self, parent):
        gear = [ranged.FragGrenades, ranged.KrakGrenades]
        super(PrimarisWolfLord, self).__init__(parent,
                                              points=points_price(get_cost(units.PrimarisWolfLord), *gear),
                                              gear=create_gears(*gear))
        self.rng = self.RangedWeapon(self)
        self.mle = self.Melee(self)

    def check_rules(self):
        super(PrimarisWolfLord, self).check_rules()
        self.rng.used = self.rng.visible = not self.mle.fist.value


class GravisWolfLord(HqUnit, armory.SWUnit):
    type_name = get_name(units.WolfLordGravis)
    type_id = 'gravis_wolf_lord_v1'
    kwname = 'Wolf Lord'
    keywords = ['Character', 'Infantry', 'Mk X Gravis', 'Primaris']
    power = 7

    def __init__(self, parent):
        gear = [melee.MCPowerSword, ranged.BoltstormGauntlet]
        super(GravisWolfLord, self).__init__(parent,
                                             points=points_price(get_cost(units.GravisWolfLord), *gear),
                                             gear=create_gears(*gear), static=True)


class BikeWolfLord(armory.SWUnit, BikeCaptain):
    type_name = u'Wolf Lord on Bike (Index)'
    type_id = 'bike_wolf_lord_v1'
    unit_name = 'Wolf Lord on Bike'
    armory = armory


class Canis(HqUnit, armory.SWUnit):
    type_id = 'canis_v1'
    type_name = u'Canis Wolfborn'
    keywords = ['Cavalry', 'Character', 'Thunderwolf', 'Wolf Guard']
    power = 7

    def __init__(self, parent):
        super(Canis, self).__init__(parent, unique=True,
                                    gear=[
                                        Gear('Wolf claw', count=2),
                                        Gear('Bolt pistol'),
                                        Gear('Frag grenades'),
                                         UnitDescription('Fangir', options=[
                                             Gear('Crushing teeth and claws')
                                         ])
                                    ], static=True, points=get_cost(units.CanisWolfborn))


class WGBattleLeader(armory.WolfClawUser, HqUnit, armory.SWUnit):
    type_name = u'Wolf Guard Battle Leader'
    type_id = 'wgbl_v1'
    kwname = 'Battle Leader'
    keywords = ['Infantry', 'Character', 'Wolf Guard']
    power = 4

    class Options(OptionsList):
        def __init__(self, parent):
            super(WGBattleLeader.Options, self).__init__(parent, 'Options')
            self.variant('Jump pack', get_cost(units.WolfGuardBattleLeaderWithJumpPack) -
                         get_cost(units.WolfGuardBattleLeader))

    def __init__(self, parent):
        super(WGBattleLeader, self).__init__(parent, gear=[
            Gear('Frag grenades'), Gear('Krak grenades')
        ], points=get_cost(units.WolfGuardBattleLeader))
        self.wep1 = ThunderwolfLord.Weapon1(self)
        self.wep2 = ThunderwolfLord.Weapon2(self)
        self.pack = self.Options(self)

    def build_power(self):
        return self.power + self.pack.any


class BikeWGBattleLeader(armory.WolfClawUser, HqUnit, armory.SWUnit):
    type_name = u'Wolf Guard Battle Leader on Bike (Index)'
    type_id = 'bike_wgbl_v1'
    kwname = 'Battle Leader'
    keywords = ['Biker', 'Character', 'Wolf Guard']
    power = 6

    def __init__(self, parent):
        super(BikeWGBattleLeader, self).__init__(parent, name='Wolf Guard Battle Leader on Bike', gear=[
            Gear('Frag grenades'), Gear('Krak grenades'),
            Gear('Twin boltgun')
        ], points=points_price(get_cost(units.WolfGuardBattleLeaderOnBike), ranged.TwinBoltgun))
        self.wep1 = ThunderwolfLord.Weapon1(self)
        self.wep2 = ThunderwolfLord.Weapon2(self)


class PrimarisBattleLeader(HqUnit, armory.SWUnit):
    type_name = u'Primaris Battle Leader'
    type_id = 'primaris_wgbl_v1'
    kwname = 'Battle Leader'
    keywords = ['Infantry', 'Character', 'Primaris']
    power = 4

    class Weapons(OneOf):
        def __init__(self, parent):
            super(PrimarisBattleLeader.Weapons, self).__init__(parent, 'Weapons')
            self.variant(join_and(melee.PowerAxe, ranged.BoltCarbine),
                         get_costs(melee.PowerAxe, ranged.BoltCarbine),
                         gear=create_gears(melee.PowerAxe, ranged.BoltCarbine))
            self.variant(*melee.PowerSword)
            self.variant(*ranged.MCAutoBoltRifle)
            self.variant(*ranged.MCStalkerBoltRifle)

    def __init__(self, parent):
        super(PrimarisBattleLeader, self).__init__(parent, points=get_cost(units.PrimarisBattleLeader),
                                                   gear=create_gears(ranged.BoltPistol, ranged.FragGrenades,
                                                                     ranged.KrakGrenades))
        self.Weapons(self)


class TermWGBattleLeader(armory.WolfClawUser, HqUnit, armory.SWUnit):
    type_name = get_name(units.WolfGuardBattleLeaderInTerminatorArmour)
    type_id = 'termo_wgbl_v1'
    kwname = 'Battle Leader'
    keywords = ['Infantry', 'Character', 'Wolf Guard', 'Terminator']
    power = 6

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(TermWGBattleLeader.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*melee.PowerSword)
            self.variant(*wargear.StormShieldCharacter)
            armory.add_melee_weapons(self, sword=False)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(TermWGBattleLeader.Weapon2, self).__init__(parent, '')
            # self.variant(*ranged.StormBolter)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    def __init__(self, parent):
        super(TermWGBattleLeader, self).__init__(parent, points=get_cost(units.WolfGuardBattleLeaderInTerminatorArmour))
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)


class WolfWGBattleLeader(armory.WolfClawUser, HqUnit, armory.SWUnit):
    type_name = get_name(units.WolfGuardBattleLeaderOnThunderwolf)
    type_id = 'wolf_wgbl_v1'
    kwname = 'Battle Leader'
    keywords = ['Cavalry', 'Character', 'Wolf Guard', 'Thunderwolf']
    power = 6

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(WolfWGBattleLeader.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*melee.Chainsword)
            self.variant(*wargear.StormShieldCharacter)
            armory.add_melee_weapons(self, chain=False)

    def __init__(self, parent):
        super(WolfWGBattleLeader, self).__init__(parent, gear=[
            Gear('Frag grenades'), Gear('Krak grenades')
        ], points=get_cost(units.WolfGuardBattleLeaderOnThunderwolf))
        self.wep1 = self.Weapon1(self)
        self.wep2 = ThunderwolfLord.Weapon2(self)


class IronPriest(HqUnit, armory.SWUnit):
    type_name = u'Iron Priest (Index)'
    type_id = 'iron_priest_v1'

    keywords = ['Character', 'Infantry', 'Techmarine']
    power = 6

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(IronPriest.Weapon1, self).__init__(parent, 'Ranged Weapon')
            self.variant('Boltgun')
            self.variant('Bolt pistol')
            self.variant(*ranged.HelfrostPistol)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(IronPriest.Weapon2, self).__init__(parent, 'Melee Weapon')
            self.variant(*melee.ThunderHammerCharacter)
            self.variant(*melee.TempestHammer)

    def __init__(self, parent):

        super(IronPriest, self).__init__(parent, name='Iron Priest', gear=[
            Gear('Frag grenades'), Gear('Krak grenades'),
            Gear('Servo-arm')
        ], points=points_price(get_cost(units.IronPriest), melee.ServoArm))
        self.Weapon1(self)
        self.Weapon2(self)


class IronPriestV2(HqUnit, armory.SWUnit):
    type_name = get_name(units.IronPriest)
    type_id = 'iron_priest_v2'

    keywords = ['Character', 'Infantry', 'Techmarine']
    power = 5

    def __init__(self, parent):
        gear = [melee.TempestHammer, ranged.HelfrostPistol, melee.ServoArm,
                ranged.FragGrenades, ranged.KrakGrenades]
        super(IronPriestV2, self).__init__(parent, name='Iron Priest',
                                           gear=create_gears(*gear),
                                           points=points_price(get_cost(units.IronPriest), *gear),
                                           static=True)


class BikeIronPriest(HqUnit, armory.SWUnit):
    type_name = u'Iron Priest on Bike (Index)'
    type_id = 'bike_iron_priest_v1'
    kwname = 'Iron Priest'

    keywords = ['Character', 'Biker', 'Techmarine']
    power = 6

    def __init__(self, parent):
        super(BikeIronPriest, self).__init__(parent, points=points_price(get_cost(units.IronPriestOnBike), melee.ServoArm, ranged.TwinBoltgun), gear=[
            Gear('Frag grenades'), Gear('Krak grenades'),
            Gear('Servo-arm'), Gear('Twin boltgun')
        ], name='Iron Priest on Bike')
        IronPriest.Weapon1(self)
        IronPriest.Weapon2(self)


class WolfIronPriest(HqUnit, armory.SWUnit):
    type_name = u'Iron Priest on Thunderwolf (Index)'
    type_id = 'wolf_iron_priest_v1'
    kwname = 'Iron Priest'

    keywords = ['Character', 'Cavalry', 'Thunderwolf', 'Techmarine']
    power = 7

    def __init__(self, parent):
        super(WolfIronPriest, self).__init__(parent, points=points_price(get_cost(units.IronPriestOnThunderwolf), melee.ServoArm), gear=[
            Gear('Frag grenades'), Gear('Krak grenades'),
            Gear('Servo-arm'), UnitDescription('Thunderwolf', options=[
                Gear('Crushing teeth and claws')
            ])
        ], name='Iron Priest on Thunderwolf')
        IronPriest.Weapon1(self)
        IronPriest.Weapon2(self)


