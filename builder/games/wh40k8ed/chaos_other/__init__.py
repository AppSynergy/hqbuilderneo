from lords import RenegadeKnight, RenegadeDominus, RenegadeArmigers
from fort import ChaosBastion

unit_types = [RenegadeKnight, RenegadeDominus, RenegadeArmigers,
              ChaosBastion]
