__author__ = 'Ivan Truskov'

import melee, ranged, units
from builder.games.wh40k8ed.unit import LordsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, OptionsList, UnitList, ListSubUnit
from builder.games.wh40k8ed.utils import *


def add_knight_weapons(instance):
    instance.variant(join_and(ranged.AvengerGatlingCannon, ranged.HeavyFlamer),
                     get_costs(ranged.AvengerGatlingCannon, ranged.HeavyFlamer),
                     gear=create_gears(ranged.AvengerGatlingCannon, ranged.HeavyFlamer))
    instance.variant(join_and(ranged.RapidFireBattleCannon, ranged.HeavyStubber),
                     get_costs(ranged.RapidFireBattleCannon, ranged.HeavyStubber),
                     gear=create_gears(ranged.RapidFireBattleCannon, ranged.HeavyStubber))
    instance.variant(*ranged.ThermalCannon)


class RenegadeKnight(LordsUnit, Unit):
    type_name = get_name(units.RenegadeKnight)
    type_id = 'rknight_v1'
    faction = ['Questor Traitoris', 'Chaos']
    keywords = ['Titanic', 'Vehicle']

    power = 25

    class CaparaceWeapons(OptionsList):
        def __init__(self, parent):
            super(RenegadeKnight.CaparaceWeapons, self).__init__(parent, 'Caparace Weapon',
                                                                 limit=1)
            self.variant(*ranged.IronstormMissilePod)
            self.variant(*ranged.StormspearRocketPod)
            self.variant(*ranged.TwinIcarusAutocannon)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(RenegadeKnight.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*melee.ThunderstrikeGauntlet)
            add_knight_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(RenegadeKnight.Weapon2, self).__init__(parent, '')
            self.variant(*melee.ReaperChainsword)
            add_knight_weapons(self)

    class SecondaryWeapon(OneOf):
        def __init__(self, parent):
            super(RenegadeKnight.SecondaryWeapon, self).__init__(parent, 'Secondary weapon')
            self.variant(*ranged.HeavyStubber)
            self.variant(*ranged.Meltagun)

    def __init__(self, parent):
        super(RenegadeKnight, self).__init__(parent, points=get_cost(units.RenegadeKnight), gear=[Gear('Titanic feet')])
        self.Weapon1(self)
        self.Weapon2(self)
        self.SecondaryWeapon(self)
        self.CaparaceWeapons(self)


class RenegadeDominus(LordsUnit, Unit):
    type_name = get_name(units.RenegadeKnightDominus)
    type_id = 'rknight_dominus_v1'
    faction = ['Questor Traitoris', 'Chaos']
    keywords = ['Titanic', 'Vehicle']

    power = 30

    class WeaponCaparace(OneOf):
        def __init__(self, parent):
            super(RenegadeDominus.WeaponCaparace, self).__init__(parent, 'Caparace Weapon')
            self.variant(*ranged.TwinSiegebreakerCannon)
            self.variant('Two shieldbreaker missiles', 2 * get_cost(ranged.ShieldbreakerMissile),
                         gear=Gear('Shieldbreaker missile', count=2))

    class Weapons(OneOf):
        def __init__(self, parent):
            super(RenegadeDominus.Weapons, self).__init__(parent, 'Weapons')
            gear1 = [ranged.PlasmaDecimator, ranged.VolcanoLance]
            self.variant(join_and(*gear1), get_costs(*gear1),
                         gear=create_gears(*gear1))
            gear2 = [ranged.ConflagrationCannon, ranged.ThundercoilHarpoon]
            self.variant(join_and(*gear2), get_costs(*gear2),
                         gear=create_gears(*gear2))
    
    def __init__(self, parent):
        gear = [ranged.TwinSiegebreakerCannon,
                ranged.TwinMeltagun, ranged.TwinMeltagun,
                ranged.ShieldbreakerMissile, ranged.ShieldbreakerMissile,
                melee.TitanicFeet]
        cost = points_price(get_cost(units.RenegadeKnightDominus), *gear)
        super(RenegadeDominus, self).__init__(parent, get_name(units.RenegadeKnightDominus), points=cost,
                                            gear=create_gears(*gear))
        self.Weapons(self)
        self.WeaponCaparace(self)


class RenegadeArmigers(LordsUnit, Unit):
    type_name = get_name(units.RenegadeArmiger)
    type_id = 'rarmiger_v1'
    power = 9

    faction = ['Questor Traitoris', 'Chaos']
    keywords = ['Vehicle']

    class SingleArmiger(ListSubUnit):
        type_name = u'RenegadeArmiger'

        class Weapons(OneOf):
            def __init__(self, parent):
                super(RenegadeArmigers.SingleArmiger.Weapons, self).__init__(parent, 'Weapons')
                gear1 = [ranged.ArmigerAutocannon, ranged.ArmigerAutocannon]
                self.variant('Two armiger autocannons', get_costs(*gear1),
                             gear=create_gears(*gear1))
                gear2 = [ranged.ThermalSpear, melee.ReaperChainCleaver]
                self.variant(join_and(*gear2), get_costs(*gear2),
                             gear=create_gears(*gear2))

        class Caparace(OneOf):
            def __init__(self, parent):
                super(RenegadeArmigers.SingleArmiger.Caparace, self).__init__(parent, 'Caparace weapon')
                self.variant(*ranged.HeavyStubber)
                self.variant(*ranged.Meltagun)

        def __init__(self, parent):
            super(RenegadeArmigers.SingleArmiger, self).__init__(parent, points=get_cost(units.RenegadeArmiger))
            self.Weapons(self)
            self.Caparace(self)

    def __init__(self, parent):
        super(RenegadeArmigers, self).__init__(parent)
        self.models = UnitList(self, self.SingleArmiger, 1, 3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count
