__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FortUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, OptionsList


class ChaosBastion(FortUnit, Unit):
    type_name = u'Chaos Bastion'
    type_id = 'cbastion_v1'
    faction = ['Chaos']

    keywords = ['Building', 'Vehicle', 'Transport']

    power = 10

    class Emplacement(OptionsList):
        def __init__(self, parent):
            super(ChaosBastion.Emplacement, self).__init__(parent, 'Gun emplacement',
                                                           limit=1)
            self.variant('Icarus lascannon', 25)
            self.variant('Quad-gun', 30)

    def __init__(self, parent):
        super(ChaosBastion, self).__init__(parent, points=160 + 8 * 4,
                                           gear=Gear('Heavy bolter', count=4))
        self.Emplacement(self)
