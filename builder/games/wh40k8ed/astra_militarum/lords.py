__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import LordsUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription,\
    OptionsList, ListSubUnit
import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class Baneblade(LordsUnit, armory.RegimentUnit):
    type_id = 'baneblade_v1'
    type_name = get_name(units.Baneblade)
    keywords = ['Vehicle', 'Titanic']
    power = 28

    class Options(OptionsList):
        def __init__(self, parent):
            super(Baneblade.Options, self).__init__(parent, name='Options')
            self.variant(*ranged.HunterKillerMissile)
            self.sb = self.variant(*ranged.StormBolter)
            self.hs = self.variant(*ranged.HeavyStubber)

        def check_rules(self):
            super(Baneblade.Options, self).check_rules()
            self.process_limit([self.sb, self.hs], 1)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(Baneblade.Sponsons, self).__init__(parent, 'Sponsons', limit=1)
            self.variant('Two bolter sponsons', gear=[
                Gear('Lascannon', count=2),
                Gear('Twin heavy bolter', count=2)
            ], points=2 * get_costs(ranged.Lascannon, ranged.TwinHeavyBolter))
            self.variant('Two flamer sponsons', gear=[
                Gear('Lascannon', count=2),
                Gear('Twin heavy flamer', count=2)
            ], points=2 * get_costs(ranged.Lascannon, ranged.TwinHeavyFlamer))
            self.variant('Four bolter sponsons', gear=[
                Gear('Lascannon', count=4),
                Gear('Twin heavy bolter', count=4)
            ], points=4 * get_costs(ranged.Lascannon, ranged.TwinHeavyBolter))
            self.variant('Four flamer sponsons', gear=[
                Gear('Lascannon', count=4),
                Gear('Twin heavy flamer', count=4)
            ], points=4 * get_costs(ranged.Lascannon, ranged.TwinHeavyFlamer))

    def __init__(self, parent):
        gear = [ranged.Autocannon, ranged.BanebladeCannon,
                ranged.DemolisherCannon, ranged.TwinHeavyBolter,
                melee.AdamantiumTracks]
        cost = points_price(get_cost(units.Baneblade), *gear)
        super(Baneblade, self).__init__(parent=parent, points=cost,
                                        gear=create_gears(*gear))
        self.Options(self)
        self.Sponsons(self)


class Banehammer(LordsUnit, armory.RegimentUnit):
    type_id = 'banehammer_v1'
    type_name = get_name(units.Banehammer)
    keywords = ['Vehicle', 'Titanic']
    power = 26

    def __init__(self, parent):
        gear = [ranged.TremorCannon, ranged.TwinHeavyBolter,
                melee.AdamantiumTracks]
        cost = points_price(get_cost(units.Banehammer), *gear)
        super(Banehammer, self).__init__(parent=parent, points=cost,
                                         gear=create_gears(*gear))
        Baneblade.Options(self)
        Baneblade.Sponsons(self)


class Banesword(LordsUnit, armory.RegimentUnit):
    type_id = 'banesword_v1'
    type_name = get_name(units.Banesword)
    keywords = ['Vehicle', 'Titanic']
    power = 26

    def __init__(self, parent):
        gear = [ranged.QuakeCannon, ranged.TwinHeavyBolter,
                melee.AdamantiumTracks]
        cost = points_price(get_cost(units.Banesword), *gear)
        super(Banesword, self).__init__(parent=parent, points=cost,
                                        gear=create_gears(*gear))
        Baneblade.Options(self)
        Baneblade.Sponsons(self)


class Doomhammer(LordsUnit, armory.RegimentUnit):
    type_id = 'doomhammer_v1'
    type_name = get_name(units.Doomhammer)
    keywords = ['Vehicle', 'Titanic', 'Transport']
    power = 27

    def __init__(self, parent):
        gear = [ranged.MagmaCannon, ranged.TwinHeavyBolter,
                melee.AdamantiumTracks]
        cost = points_price(get_cost(units.Doomhammer), *gear)
        super(Doomhammer, self).__init__(parent=parent, points=cost,
                                         gear=create_gears(*gear))
        Baneblade.Options(self)
        Baneblade.Sponsons(self)


class Hellhammer(LordsUnit, armory.RegimentUnit):
    type_id = 'hellhammer_v1'
    type_name = get_name(units.Hellhammer)
    keywords = ['Vehicle', 'Titanic']
    power = 30

    def __init__(self, parent):
        gear = [ranged.Autocannon, ranged.HellhammerCannon,
                ranged.DemolisherCannon, ranged.TwinHeavyBolter,
                ranged.Lasgun, melee.AdamantiumTracks]
        cost = points_price(get_cost(units.Hellhammer), *gear)
        super(Hellhammer, self).__init__(parent=parent, points=cost,
                                         gear=create_gears(*gear))
        Baneblade.Options(self)
        Baneblade.Sponsons(self)


class Shadowsword(LordsUnit, armory.RegimentUnit):
    type_id = 'shadowsword_v1'
    type_name = get_name(units.Shadowsword)
    keywords = ['Vehicle', 'Titanic']
    power = 26

    def __init__(self, parent):
        gear = [ranged.VolcanoCannon, ranged.TwinHeavyBolter,
                melee.AdamantiumTracks]
        cost = points_price(get_cost(units.Shadowsword), *gear)
        super(Shadowsword, self).__init__(parent=parent, points=cost,
                                          gear=create_gears(*gear))
        Baneblade.Options(self)
        Baneblade.Sponsons(self)


class Stormlord(LordsUnit, armory.RegimentUnit):
    type_id = 'stormlord_v1'
    type_name = get_name(units.Stormlord)
    keywords = ['Vehicle', 'Titanic', 'Transport']
    power = 26

    def __init__(self, parent):
        gear = [ranged.VulcanMegaBolter, ranged.TwinHeavyBolter,
                ranged.HeavyStubber, ranged.HeavyStubber,
                melee.AdamantiumTracks]
        cost = points_price(get_cost(units.Stormlord), *gear)
        super(Stormlord, self).__init__(parent=parent, points=cost,
                                        gear=create_gears(*gear))
        Baneblade.Options(self)
        Baneblade.Sponsons(self)


class Stormsword(LordsUnit, armory.RegimentUnit):
    type_id = 'stormsword_v1'
    type_name = get_name(units.Stormsword)
    keywords = ['Vehicle', 'Titanic']
    power = 26

    def __init__(self, parent):
        gear = [ranged.StormswordSiegeCannon, ranged.TwinHeavyBolter,
                melee.AdamantiumTracks]
        cost = points_price(get_cost(units.Stormsword), *gear)

        super(Stormsword, self).__init__(parent=parent, points=cost,
                                         gear=create_gears(*gear))
        Baneblade.Options(self)
        Baneblade.Sponsons(self)
