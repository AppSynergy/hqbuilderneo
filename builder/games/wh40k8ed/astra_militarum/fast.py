__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription,\
    ListSubUnit, UnitList, OptionalSubUnit, SubUnit, OptionsList
import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class RoughRiders(FastUnit, armory.RegimentUnit):
    type_name = u'Rough Riders'
    type_id = 'riders_v1'

    keywords = ['Cavalry']

    model = UnitDescription('Rough Rider', options=[Gear('Frag grenades'), Gear('Laspistol'), Gear('Chainsword'),
                                                    UnitDescription('Purebreed Steed', options=[Gear('Trampling hooves')])])

    class Sergeant(Unit):
        type_name = 'Rough Rider Sergeant'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(RoughRiders.Sergeant.Weapon1, self).__init__(parent, 'Pistol')
                self.variant(*ranged.Laspistol)
                self.variant(*ranged.PlasmaPistol)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(RoughRiders.Sergeant.Weapon2, self).__init__(parent, 'Weapon')
                self.variant(*melee.Chainsword)
                self.variant('Power axe', 5)
                self.variant('Power lance', 4)
                self.variant('Power maul', 4)
                self.variant(*melee.PowerSword)

        def __init__(self, parent):
            super(RoughRiders.Sergeant, self).__init__(parent, points=8 + 2,
                                                       gear=[Gear('Frag grenades'), Gear('Hunting lance'),
                                                             UnitDescription('Purebreed Steed', options=[Gear('Trampling hooves')])])
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)

    class Rider(Count):
        @property
        def description(self):
            return RoughRiders.model.clone().add([Gear('Hunting lance')]).\
                set_count(self.cur - sum(c.cur != c.lance for c in [self.parent.wep1, self.parent.wep2]))

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(RoughRiders.SpecialWeapon, self).__init__(parent, 'Special weapon')
            self.lance = self.variant('Hunting lance', 2)
            armory.add_special_weapons(self, rider=True)

        @property
        def description(self):
            return []

    def __init__(self, parent):
        super(RoughRiders, self).__init__(parent)
        self.sup = SubUnit(self, self.Sergeant(parent=self))
        self.squad = self.Rider(self, 'Rough Riders', 4, 9, per_model=True, points=8)
        self.wep1 = self.SpecialWeapon(self)
        self.wep2 = self.SpecialWeapon(self)

    def build_points(self):
        return super(RoughRiders, self).build_points() + 2 * (self.squad.cur - 2)

    def build_description(self):
        res = super(RoughRiders, self).build_description()
        res.add_dup(RoughRiders.model.clone().add(self.wep1.cur.gear))
        res.add_dup(RoughRiders.model.clone().add(self.wep2.cur.gear))
        return res

    def get_count(self):
        return self.squad.cur + 1

    def build_power(self):
        return 3 + 2 * (self.squad.cur > 4)


class Squadron(Unit):
    unit_class = None
    unit_min = 1
    unit_max = 3

    def __init__(self, parent):
        super(Squadron, self).__init__(parent)
        self.tanks = UnitList(self, self.unit_class, self.unit_min, self.unit_max)

    def get_count(self):
        return self.tanks.count

    def build_power(self):
        return self.tanks.count * self.unit_class.power


class Hellhound(ListSubUnit):
    type_name = u'Hellhound'
    power = 6

    def __init__(self, parent):
        super(Hellhound, self).__init__(parent=parent, points=get_cost(units.Hellhounds))
        self.type = self.Type(self)
        self.Weapon(self)
        armory.VehicleEquipment(self)

    class Type(OneOf):
        def __init__(self, parent):
            super(Hellhound.Type, self).__init__(parent=parent, name='Type')
            self.hellhound = self.variant('Hellhound', get_cost(ranged.InfernoCannon),
                                          gear=[Gear('Inferno cannon')])
            self.devildog = self.variant('Devil dog', get_cost(ranged.MeltaCannon),
                                         gear=[Gear('Melta cannon')])
            self.banewolf = self.variant('Banewolf', get_cost(ranged.ChemCannon),
                                         gear=[Gear('Chem cannon')])

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Hellhound.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant(*ranged.HeavyBolter)
            self.heavyflamer = self.variant(*ranged.HeavyFlamer)
            self.multymelta = self.variant(*ranged.MultiMelta)

    def build_description(self):
        desc = super(Hellhound, self).build_description()
        desc.name = self.type.cur.title
        return desc


class ScoutSentinel(ListSubUnit):
    type_name = u'Scout Sentinel'
    power = 3
    base_points = get_cost(units.ScoutSentinels)
    use_plasma = False

    class Options(OptionsList):
        def __init__(self, parent):
            super(ScoutSentinel.Options, self).__init__(parent, 'Options')
            self.variant(*melee.SentinelChainsaw)
            self.variant(*ranged.HunterKillerMissile)
    
    def __init__(self, parent):
        super(ScoutSentinel, self).__init__(parent=parent, points=self.base_points)
        self.Weapon(self)
        self.Options(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ScoutSentinel.Weapon, self).__init__(parent=parent, name='Weapon')
            self.multilaser = self.variant(*ranged.MultiLaser)
            self.heavyflamer = self.variant(*ranged.HeavyFlamer)
            self.autocannon = self.variant(*ranged.Autocannon)
            self.missilelauncher = self.variant(*ranged.MissileLauncher)
            self.lascannon = self.variant(*ranged.Lascannon)
            if parent.use_plasma:
                self.variant(*ranged.PlasmaCannon)


class ArmouredSentinel(ScoutSentinel):
    type_name = u'Armoured Sentinel'
    base_points = get_cost(units.ArmouredSentinels)
    use_plasma = True


class ScoutSentinels(Squadron, FastUnit, armory.RegimentUnit):
    type_name = get_name(units.ScoutSentinels)
    type_id = 'scout_sentinel_squad_v1'
    unit_class = ScoutSentinel

    keywords = ['Vehicle']


class ArmouredSentinels(Squadron, FastUnit, armory.RegimentUnit):
    type_name = get_name(units.ArmouredSentinels)
    type_id = 'armour_sentinel_squad_v1'
    unit_class = ArmouredSentinel

    keywords = ['Vehicle']


class Hellhounds(Squadron, FastUnit, armory.RegimentUnit):
    type_name = get_name(units.Hellhounds)
    type_id = 'hellhound_squad_v1'
    unit_class = Hellhound

    keywords = ['Vehicle']
