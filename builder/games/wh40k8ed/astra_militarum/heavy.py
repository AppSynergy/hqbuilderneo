__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HeavyUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription,\
    OptionsList, ListSubUnit
import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *
from fast import Squadron


class LemanRuss(ListSubUnit):
    type_name = u'Leman Russ Battle Tank'
    type_id = 'lemanruss_v1'
    kwname = 'Leman Russ'

    keywords = ['Vehicle']
    power = 10

    def __init__(self, parent):
        super(LemanRuss, self).__init__(parent=parent, points=get_cost(units.LemanRussBattleTanks))
        self.MainGun(self)
        self.Weapon(self)
        self.Sponsons(self)
        armory.VehicleEquipment(self)

    class MainGun(OneOf):
        def __init__(self, parent):
            super(LemanRuss.MainGun, self).__init__(parent=parent, name='Turret cannon')
            self.variant(*ranged.BattleCannon)
            self.variant(*ranged.EradicatorNovaCannon)
            self.variant(*ranged.ExterminatorAutocannon)
            self.variant(*ranged.VanquisherBattleCannon)
            self.variant(*ranged.DemolisherCannon)
            self.variant(*ranged.ExecutionerPlasmaCannon)
            self.variant(*ranged.PunisherGatlingCannon)

    class Weapon(OneOf):
        def __init__(self, parent, las=True):
            super(LemanRuss.Weapon, self).__init__(parent=parent, name='Hull Weapon')
            self.heavybolter = self.variant(*ranged.HeavyBolter)
            self.heavyflamer = self.variant(*ranged.HeavyFlamer)
            if las:
                self.lascannon = self.variant(*ranged.Lascannon)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(LemanRuss.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)
            self.variant('Heavy flamers', 2 * get_cost(ranged.HeavyFlamer),
                         gear=[Gear('Heavy flamer', count=2)])
            self.variant('Heavy bolters', 2 * get_cost(ranged.HeavyBolter),
                         gear=[Gear('Heavy bolter', count=2)])
            self.variant('Multi-meltas', 2 * get_cost(ranged.MultiMelta),
                         gear=[Gear('Multimelta', count=2)])
            self.variant('Plasma cannons', 2 * get_cost(ranged.PlasmaCannon),
                         gear=[Gear('Plasma cannon', count=2)])


class HeavyWeaponSquad(HeavyUnit, armory.RegimentUnit):
    type_name = get_name(units.HeavyWeaponsSquad)
    type_id = 'hwep_squad_v1'

    keywords = ['Infantry']
    power = 3
    member = UnitDescription('Heavy Weapons Team', options=[Gear('Frag grenades'), Gear('Lasgun')])

    class Weapon(OneOf):
        def __init__(self, parent):
            super(HeavyWeaponSquad.Weapon, self).__init__(parent, 'Heavy Weapon')
            armory.add_heavy_weapons(self)

    def __init__(self, parent):
        super(HeavyWeaponSquad, self).__init__(
            parent, points=3 * get_cost(units.HeavyWeaponsSquad))
        self.weps = [self.Weapon(self) for i in range(0, 3)]

    def get_count(self):
        return 3
    
    def build_description(self):
        res = UnitDescription(self.name, self.points, 3)
        for wep in self.weps:
            res.add_dup(HeavyWeaponSquad.member.clone().add(wep.cur.gear))
        return res


class Basilisk(ListSubUnit):
    type_name = u'Basilisk'
    power = 6

    def __init__(self, parent):
        super(Basilisk, self).__init__(parent=parent, points=get_cost(units.Basilisks), gear=[
            Gear('Earthshaker cannon'),
        ])
        LemanRuss.Weapon(self, las=False)
        armory.VehicleEquipment(self)


class Hydra(ListSubUnit):
    type_name = u'Hydra'
    power = 6

    def __init__(self, parent):
        super(Hydra, self).__init__(parent=parent, points=get_cost(units.Hydras), gear=[
            Gear('Hydra quad autocannon'),
        ])
        LemanRuss.Weapon(self, las=False)
        armory.VehicleEquipment(self)


class Wyvern(ListSubUnit):
    type_name = u'Wyvern'
    power = 6

    def __init__(self, parent):
        super(Wyvern, self).__init__(parent=parent, points=get_cost(units.Wyverns), gear=[
            Gear('Wyvern quad stormshard mortar'),
        ])
        LemanRuss.Weapon(self, las=False)
        armory.VehicleEquipment(self)


class Manticore(HeavyUnit, armory.RegimentUnit):
    type_id = 'manticure_v1'
    type_name = get_name(units.Manticore)
    power = 8

    def __init__(self, parent):
        super(Manticore, self).__init__(parent=parent, points=get_cost(units.Manticore), gear=[
            Gear('Storm Eagle rocket', count=4),
        ])
        LemanRuss.Weapon(self, las=False)
        armory.VehicleEquipment(self)


class Deathstrike(HeavyUnit, armory.RegimentUnit):
    type_id = 'deathstrike_v1'
    type_name = get_name(units.Deathstrike)
    power = 9

    def __init__(self, parent):
        super(Deathstrike, self).__init__(parent=parent, points=get_cost(units.Deathstrike), gear=[
            Gear('Deathstrike missile')
        ])
        LemanRuss.Weapon(self, las=False)
        armory.VehicleEquipment(self)


class Basilisks(Squadron, HeavyUnit, armory.RegimentUnit):
    type_name = get_name(units.Basilisks)
    type_id = 'basilisk_squad_v1'
    unit_class = Basilisk

    keywords = ['Vehicle']

    def build_power(self):
        return 1 + super(Basilisks, self).build_power()
    


class Hydras(Squadron, HeavyUnit, armory.RegimentUnit):
    type_name = u'Hydras'
    type_id = 'hydra_squad_v1'
    unit_class = Hydra

    keywords = ['Vehicle']

    def build_power(self):
        return 1 + super(Hydras, self).build_power()


class Wyverns(Squadron, HeavyUnit, armory.RegimentUnit):
    type_name = u'Wyverns'
    type_id = 'wyvern_squad_v1'
    unit_class = Wyvern

    keywords = ['Vehicle']


class LemanRussSquadron(Squadron, HeavyUnit, armory.RegimentUnit):
    type_name = get_name(units.LemanRussBattleTanks)
    type_id = 'leman_russ_squadron_v1'
    kwname = 'Leman Russ Battle Tank'
    keywords = ['Vehicle', 'Leman Russ']

    unit_class = LemanRuss


class LemanRussDemolisher(ListSubUnit):
    type_name = u'Leman Russ Demolisher'
    type_id = 'lemanruss_dem_v1'

    power = 12

    def __init__(self, parent):
        super(LemanRussDemolisher, self).__init__(parent=parent, points=132)
        self.MainGun(self)
        LemanRuss.Weapon(self)
        LemanRuss.Sponsons(self)
        armory.VehicleEquipment(self)

    class MainGun(OneOf):
        def __init__(self, parent):
            super(LemanRussDemolisher.MainGun, self).__init__(parent=parent, name='Turret cannon')
            self.variant('Demolisher cannon', 40)
            self.variant('Executioner plasma cannon', 20)
            self.variant('Punisher gatling cannon', 20)


class LemanRussDemSquadron(Squadron, HeavyUnit, armory.RegimentUnit):
    type_name = u'Leman Russ Demolishers'
    type_id = 'leman_russ_dem_squadron_v1'
    kwname = 'Demolisher'
    keywords = ['Vehicle', 'Leman Russ']
    obsolete = True
    unit_class = LemanRussDemolisher
