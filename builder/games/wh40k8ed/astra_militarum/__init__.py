from hq import CCommander, Creed, LordCommissar, Pask, Primaris,\
    Straken, TankCommander, TempestorPrime, Yarrik, SlyMarbo
from elites import Bullgryns, CommandSquad, Commissar, Harker, Kell,\
    Ogryns, OrdnanceMaster, PCommander, Ratlings, SpecialWeaponSquad,\
    TempCommandSquad, VeteranSquad, Nork, FleetOfficer, Wyrdvanes,\
    Astropath, PriestV2, Crusaders, OgrynBodyguard, IGServitors
from troops import Conscripts, InfantrySquad, TempestusSquad
from fast import ArmouredSentinels, Hellhounds, RoughRiders,\
    ScoutSentinels
from transport import Chimera, Taurox, TauroxPrime
from fliers import Valkyries
from heavy import Basilisks, Deathstrike, HeavyWeaponSquad, Hydras,\
    LemanRussDemSquadron, LemanRussSquadron, Manticore, Wyverns
from lords import Baneblade, Banehammer, Banesword, Doomhammer, Hellhammer,\
    Shadowsword, Stormlord, Stormsword

from builder.games.wh40k8ed.sections import HQSection, ElitesSection, UnitType


class GuardHQ(HQSection):
    def __init__(self, *args, **kwargs):
        super(GuardHQ, self).__init__(*args, **kwargs)
        self.officer_types = []
        self.tempestor_types = []

    def unit_type_add(self, ut):
        if ut == TempestorPrime:
            self.tempestor_types += [super(GuardHQ, self).unit_type_add(ut)]
        elif 'OFFICER' in ut.keywords:
            self.officer_types += [super(GuardHQ, self).unit_type_add(ut)]
        else:
            super(GuardHQ, self).unit_type_add(ut)

    def count_officers(self):
        res = 0
        for ut in self.officer_types:
            res += ut.count
        return res

    def count_primes(self):
        res = 0
        for ut in self.tempestor_types:
            res += ut.count
        return res


class GuardElites(ElitesSection):
    def __init__(self, *args, **kwargs):
        super(GuardElites, self).__init__(*args, **kwargs)
        self.officer_types = []
        self.comsquad_types = []
        self.tcom_types = []

    def unit_type_add(self, ut):
        if ut == CommandSquad:
            self.comsquad_types += [super(GuardElites, self).unit_type_add(ut)]
        elif ut == TempCommandSquad:
            self.tcom_types += [super(GuardElites, self).unit_type_add(ut)]
        elif 'OFFICER' in ut.keywords:
            self.officer_types += [super(GuardElites, self).unit_type_add(ut)]
        else:
            super(GuardElites, self).unit_type_add(ut)

    def count_officers(self):
        res = 0
        for ut in self.officer_types:
            res += ut.count
        return res

    def check_rules(self):
        super(GuardElites, self).check_rules()
        # checkfor comsquads
        comcnt = sum(ut.count for ut in self.comsquad_types)
        if comcnt and comcnt > (self.count_officers() + self.roster.hq.count_officers()):
            self.error('Army may include a maximum of one <REGIMENT> Command Squad in a detachment for each <REGIMENT> OFFICER')
        tcnt = sum(ut.count for ut in self.tcom_types)
        if tcnt and tcnt > self.roster.hq.count_primes():
            self.error('Army may include a maximum of one Militarum Tempestus Command Squad in a detachment for each Tempestor Prime')


unit_types = [CCommander, Creed, LordCommissar, Pask, Primaris,
              Straken, TankCommander, TempestorPrime, Yarrik,
              Bullgryns, CommandSquad, Commissar, Harker, Kell,
              Ogryns, OrdnanceMaster, PCommander, Ratlings,
              SpecialWeaponSquad, TempCommandSquad, VeteranSquad,
              Conscripts, InfantrySquad, TempestusSquad,
              ArmouredSentinels, Hellhounds, RoughRiders,
              ScoutSentinels, Chimera, Taurox, TauroxPrime, Valkyries,
              Basilisks, Deathstrike, HeavyWeaponSquad, Hydras,
              LemanRussDemSquadron, LemanRussSquadron, Manticore,
              Wyverns, Baneblade, Banehammer, Banesword, Doomhammer,
              Hellhammer, Shadowsword, Stormlord, Stormsword, Nork,
              FleetOfficer, Wyrdvanes, Astropath, PriestV2,
              Crusaders, OgrynBodyguard, IGServitors, SlyMarbo]
