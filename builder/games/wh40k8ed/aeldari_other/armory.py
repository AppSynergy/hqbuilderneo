__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit
from builder.games.wh40k8ed.options import OptionsList, Count


class EldarUnit(Unit):
    faction = ['Aeldari']


class YnnariUnit(EldarUnit):
    faction = ['Ynnari']
    wiki_faction = 'Ynnari'
    always_common = True
    # think about a better way later
    @classmethod
    def calc_faction(cls):
        return ['DRUKHARI', 'ASURYANI', 'HARLEQUINS']


class HarlequinUnit(EldarUnit):
    faction = ['Harlequins', '<Mascue>']
    wiki_faction = 'Harlequins'

    @classmethod
    def calc_faction(cls):
        return ['YNNARI']
