__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import Unit, HqUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList,\
    Gear, UnitDescription, Count
import armory, melee, ranged, units
from builder.games.wh40k8ed.utils import *


class TroupeMaster(HqUnit, armory.HarlequinUnit):
    type_name = get_name(units.TroupeMaster)
    type_id = 'troupe_master_v1'

    keywords = ['Character', 'Infantry']

    power = 4

    class Ranged(OneOf):
        def __init__(self, parent):
            super(TroupeMaster.Ranged, self).__init__(parent, 'Ranged weapon')
            self.variant(*ranged.ShurikenPistol)
            self.variant(*ranged.NeuroDisruptor)
            self.variant(*ranged.FusionPistol)

    class Melee(OneOf):
        def __init__(self, parent):
            super(TroupeMaster.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.Blade)
            self.variant(*melee.PowerSword)
            self.variant(*melee.Embrace)
            self.variant(*melee.Kiss)
            self.variant(*melee.Caress)

    def __init__(self, parent):
        super(TroupeMaster, self).__init__(parent, points=get_cost(units.TroupeMaster),
                                           gear=[Gear('Prismatic grenades')])
        self.Ranged(self)
        self.Melee(self)


class Shadowseer(HqUnit, armory.HarlequinUnit):
    type_name = get_name(units.Shadowseer)
    type_id = 'shadowseer_v1'

    keywords = ['Character', 'Infantry', 'Psyker']

    power = 7

    class Ranged(OneOf):
        def __init__(self, parent):
            super(Shadowseer.Ranged, self).__init__(parent, 'Ranged weapon')
            self.variant(*ranged.ShurikenPistol)
            self.variant(*ranged.NeuroDisruptor)

    def __init__(self, parent):
        super(Shadowseer, self).__init__(parent, points=get_cost(units.Shadowseer), gear=[
            Gear('Hallucinogen grenade launcher'),
            Gear('Miststave')
        ])
        self.Ranged(self)


class Yvraine(HqUnit, armory.YnnariUnit):
    type_name = u'Yvraine'
    type_id = 'yvraine_v1'

    keywords = ['Character', 'Infantry', 'Psyker']

    power = 7

    def __init__(self, parent):
        super(Yvraine, self).__init__(parent, points=132, static=True, unique=True,
                                      gear=[Gear('Kha-vir, the Sword of Sorrows')])


class Visarch(HqUnit, armory.YnnariUnit):
    type_name = u'The Visarch'
    type_id = 'visarch_v1'

    keywords = ['Character', 'Infantry']

    power = 7

    def __init__(self, parent):
        super(Visarch, self).__init__(parent, points=141, static=True, unique=True,
                                      gear=[Gear('Asu-var, the Sword of Silent Screams')])


class Yncarne(HqUnit, armory.YnnariUnit):
    type_name = u'The Yncarne'
    type_id = 'Yncarne_v1'

    keywords = ['Character', 'Monster', 'Daemon', 'Fly', 'Psyker']

    power = 17

    def __init__(self, parent):
        super(Yncarne, self).__init__(parent, points=337, static=True, unique=True,
                                      gear=[Gear('Vilith-zhar, the Sword of Souls')])
