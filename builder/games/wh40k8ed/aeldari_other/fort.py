__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import Unit, FortUnit
import armory, units
from builder.games.wh40k8ed.utils import *


class WebwayGate(FortUnit, armory.EldarUnit):
    type_name = get_name(units.WebwayGate)
    type_id = 'webway_gate_v1'
    keywords = ['Vehicle', 'Building']
    power = 6

    def __init__(self, parent):
        super(WebwayGate, self).__init__(parent, points=get_cost(units.WebwayGate),
                                         static=True)
