__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit
from builder.games.wh40k8ed.options import OneOf, OptionsList, Gear


class DaemonUnit(Unit):
    faction = ['Daemon', 'Chaos']


class KhorneUnit(DaemonUnit):
    faction = ['Khorne']


class NurgleUnit(DaemonUnit):
    faction = ['Nurgle']


class SlaaneshUnit(DaemonUnit):
    faction = ['Slaanesh']


class TzeentchUnit(DaemonUnit):
    faction = ['Tzeentch']


class DevotedUnit(DaemonUnit):
    always_common = True

    @classmethod
    def calc_faction(cls):
        return ['KHORNE', 'TZEENTCH', 'NURGLE', 'SLAANESH']
