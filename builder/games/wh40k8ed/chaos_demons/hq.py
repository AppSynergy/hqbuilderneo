__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import HqUnit
from builder.games.wh40k8ed.options import OneOf, UnitDescription, OptionsList
import armory
import units
import wargear
import weapons
from builder.games.wh40k8ed.utils import *


class Belakor(HqUnit, armory.DaemonUnit):
    type_name = get_name(units.Belakor)
    type_id = 'belakor_v1'

    keywords = ['Character', 'Monster', 'Daemon Prince', 'Fly', 'Psyker']

    power = 14

    def __init__(self, parent):
        super(Belakor, self).__init__(parent=parent, points=get_cost(units.Belakor),
                                      unique=True, gear=[
                Gear('Blade of Shadows'),
                Gear('Malefic talons')
            ], static=True)


class ChaosDaemonPrince(HqUnit, armory.DevotedUnit):
    type_name = get_name(units.ChaosDaemonPrince)
    type_id = 'dp_chaos_v1'

    keywords = ['Character', 'Monster']

    power = 8

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ChaosDaemonPrince.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*weapons.HellforgedSword)
            self.variant(*weapons.DaemonicAxe)
            self.variant(*weapons.PairMaleficTalons)

    class Options(OptionsList):
        def __init__(self, parent):
            super(ChaosDaemonPrince.Options, self).__init__(parent, 'Options')
            # self.variant('Warp bolter', 9)
            self.variant(*wargear.Wings)

    def __init__(self, parent):
        super(ChaosDaemonPrince, self).__init__(parent, gear=[Gear('Malefic talons')],
                                                points=get_cost(units.ChaosDaemonPrince))
        self.Weapon(self)
        self.Options(self)


class Skulltaker(HqUnit, armory.KhorneUnit):
    type_name = get_name(units.Skulltaker)
    type_id = 'skulltaker_v1'

    keywords = ['Character', 'Infantry', 'Bloodletter', 'Herald of Khorne']
    power = 5

    def __init__(self, parent):
        super(Skulltaker, self).__init__(parent, gear=[
            Gear('The Slayer Sword')
        ], static=True, unique=True, points=get_cost(units.Skulltaker))


class Karanak(HqUnit, armory.KhorneUnit):
    type_name = get_name(units.Karanak)
    type_id = 'karanak_v1'

    keywords = ['Character', 'Beast', 'Flesh Hound']

    power = 4

    def __init__(self, parent):
        super(Karanak, self).__init__(parent, gear=[
            Gear('Soul-rending fangs'),
        ], unique=True, static=True, points=get_cost(units.Karanak))


class Scarbrand(HqUnit, armory.KhorneUnit):
    type_name = get_name(units.Skarbrand)
    type_id = 'skarbrand_v1'

    keywords = ['Character', 'Monster', 'Bloodthirster']
    power = 18

    def __init__(self, parent):
        super(Scarbrand, self).__init__(parent, 'Skarbrand', points=get_cost(units.Skarbrand), gear=[
            Gear('Bellow of endless fury'), Gear('Slaughter and Carnage')
        ], unique=True, static=True)


class FuryThirster(HqUnit, armory.KhorneUnit):
    type_name = get_name(units.FuryBloodthirster)
    type_id = 'bloodthirster_fury_v1'
    kwname = 'Bloodthirster'
    keywords = ['Character', 'Monster', 'Fly']

    power = 17

    def __init__(self, parent):
        super(FuryThirster, self).__init__(parent, points=get_cost(units.FuryBloodthirster), gear=[
            Gear('Axe of Khorne'),
            Gear('Lash of Khorne')
        ])


class RageThirster(HqUnit, armory.KhorneUnit):
    type_name = get_name(units.RageBloodthirster)
    type_id = 'bloodthirster_rage_v1'
    kwname = 'Bloodthirster'
    keywords = ['Character', 'Monster', 'Fly']

    power = 17

    def __init__(self, parent):
        super(RageThirster, self).__init__(parent, points=get_cost(units.RageBloodthirster), gear=[
            Gear('Great axe of Khorne')
        ], static=True)


class WrathThirster(HqUnit, armory.KhorneUnit):
    type_name = get_name(units.WrathBloodthirster)
    type_id = 'bloodthirster_wrath_v1'
    kwname = 'Bloodthirster'
    keywords = ['Character', 'Monster', 'Fly']

    power = 17

    def __init__(self, parent):
        super(WrathThirster, self).__init__(parent, points=get_cost(units.WrathBloodthirster), gear=[
            Gear('Axe of Khorne'),
            Gear('Hellfire'),
            Gear('Bloodlflail')
        ], static=True)


class HKhorne(HqUnit, armory.KhorneUnit):
    type_name = "Herald of Khorne"
    type_id = 'hok_v1'
    keywords = ['Character', 'Infantry', 'Bloodletter']
    power = 3
    obsolete = True

    def __init__(self, parent):
        super(HKhorne, self).__init__(parent, gear=[Gear('Hellblade')], points=56, static=True)


class BloodMaster(HqUnit, armory.KhorneUnit):
    type_name = get_name(units.Bloodmaster)
    type_id = 'bloodmaster_v1'
    kwname = 'Herald of Khorne'
    keywords = ['Character', 'Infantry', 'Bloodletter']
    power = 3

    def __init__(self, parent):
        super(BloodMaster, self).__init__(parent, gear=[Gear('Blade of Blood')], points=get_cost(units.Bloodmaster),
                                          static=True)


class ThroneHKhorne(HqUnit, armory.KhorneUnit):
    type_name = "Herald of Khorne on Blood Throne"
    kwname = 'Herald of Khorne'
    type_id = 'hok_throne_v1'
    keywords = ['Character', 'Chariot', 'Bloodletter']
    power = 6
    obsolete = True

    def __init__(self, parent):
        super(ThroneHKhorne, self).__init__(parent, gear=[Gear('Hellblade'),
                                                          UnitDescription('Bloodletter', count=2,
                                                                          options=[Gear('Hellblade')])],
                                            static=True, points=105)


class JugHKhorne(HqUnit, armory.KhorneUnit):
    type_name = u"Herald of Khorne on Juggernaut"
    type_id = 'hok_jug_v1'
    kwname = 'Herald of Khorne'
    keywords = ['Character', 'Cavalry', 'Bloodletter']
    power = 5
    obsolete = True

    def __init__(self, parent):
        super(JugHKhorne, self).__init__(parent, gear=[Gear('Hellblade'),
                                                       UnitDescription('Juggernaut', options=[Gear('Bladed horn')])],
                                         static=True, points=100)


class Skullmaster(HqUnit, armory.KhorneUnit):
    type_name = get_name(units.Skullmaster)
    type_id = 'skullmaster_v1'
    kwname = 'Herald of Khorne'
    keywords = ['Character', 'Cavalry', 'Bloodletter']
    power = 5

    def __init__(self, parent):
        super(Skullmaster, self).__init__(parent, gear=[Gear('Blade of Blood'),
                                                        UnitDescription('Juggernaut', options=[Gear('Bladed horn')])],
                                          static=True, points=get_cost(units.Skullmaster))


class BloodThrone(HqUnit, armory.KhorneUnit):
    type_name = get_name(units.BloodThrone)
    kwname = 'Herald of Khorne'
    type_id = 'hok_throne_v2'
    keywords = ['Character', 'Chariot', 'Bloodletter']
    power = 6

    def __init__(self, parent):
        super(BloodThrone, self).__init__(parent, gear=[UnitDescription('Rendmaster', options=[Gear('Blade of Blood')]),
                                                        UnitDescription('Bloodletter', count=2,
                                                                        options=[Gear('Hellblade')])],
                                          static=True, points=get_cost(units.BloodThrone))



class Kairos(HqUnit, armory.TzeentchUnit):
    type_name = get_name(units.KairosFateweaver)
    type_id = 'kairos_v1'

    keywords = ['Character', 'Monster', 'Psyker', 'Fly', 'Lord of Change']
    power = 19

    def __init__(self, parent):
        super(Kairos, self).__init__(parent, gear=[
            Gear('Staff of Tomorrow')
        ], unique=True, static=True, points=get_cost(units.KairosFateweaver))


class Changeling(HqUnit, armory.TzeentchUnit):
    type_name = get_name(units.TheChangeling)
    type_id = 'changeling_v1'

    keywords = ['Character', 'Infantry', 'Psyker', 'Herald of Tzeentch', 'Horror']
    power = 5

    def __init__(self, parent):
        super(Changeling, self).__init__(parent, gear=[
            Gear("The Trikster's Staff")
        ], unique=True, static=True, points=get_cost(units.TheChangeling))


class BlueScribes(HqUnit, armory.TzeentchUnit):
    type_name = get_name(units.TheBlueScribes)
    type_id = 'blue_scribes_v1'

    keywords = ['Character', 'Cavalry', 'Fly', 'Horror']
    power = 5

    def __init__(self, parent):
        super(BlueScribes, self).__init__(parent, gear=[
            Gear('Sharp quills'), UnitDescription('Disc of Tzeentch', options=[Gear('Blades')])
        ], unique=True, static=True, points=get_cost(units.TheBlueScribes))


class LordOfChange(HqUnit, armory.TzeentchUnit):
    type_name = get_name(units.LordOfChange)
    type_id = 'loch_v1'

    keywords = ['Character', 'Monster', 'Fly', 'Psyker']
    power = 17

    class Options(OptionsList):
        def __init__(self, parent):
            super(LordOfChange.Options, self).__init__(parent, 'Options', limit=1)
            self.variant(*weapons.BalefulSword)
            self.variant(*weapons.RodOfSorcery)

    def __init__(self, parent):
        super(LordOfChange, self).__init__(parent, gear=[
            Gear('Staff of tzeentch')], points=get_cost(units.LordOfChange))
        self.Options(self)


class HTzeentch(HqUnit, armory.TzeentchUnit):
    type_name = "Herald of Tzeentch"
    type_id = 'hot_v1'
    keywords = ['Character', 'Infantry', 'Horror', 'Psyker']
    power = 4
    obsolete = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(HTzeentch.Options, self).__init__(parent, 'Options')
            self.variant('Staff of change', 5)

    def __init__(self, parent):
        super(HTzeentch, self).__init__(parent, gear=[Gear('Ritual dagger')], points=78)
        self.Options(self)


class Changecaster(HqUnit, armory.TzeentchUnit):
    type_name = get_name(units.Changecaster)
    type_id = 'changecaster_v1'
    keywords = ['Character', 'Infantry', 'Horror', 'Psyker']
    kwname = 'Herald of Tzeentch'
    power = 4

    class Options(OptionsList):
        def __init__(self, parent):
            super(Changecaster.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.StaffOfChange)

    def __init__(self, parent):
        super(Changecaster, self).__init__(parent, gear=[Gear('Ritual dagger')], points=get_cost(units.Changecaster))
        self.Options(self)


class ChariotHTzeentch(HqUnit, armory.TzeentchUnit):
    type_name = "Herald of Tzeentch on Burning Chariot"
    type_id = 'hot_chariot_v1'
    keywords = ['Character', 'Chariot', 'Horror', 'Fly', 'Psyker']
    kwname = 'Herald of Tzeentch'
    power = 7
    obsolete = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(ChariotHTzeentch.Options, self).__init__(parent, 'Options')
            self.variant('Staff of change', 5)
            self.variant('Horrors', 5 * 3, gear=[UnitDescription('Blue Horror', count=3)])

    def __init__(self, parent):
        super(ChariotHTzeentch, self).__init__(parent, gear=[Gear('Ritual dagger'),
                                                             UnitDescription('Screamer', count=2,
                                                                             options=[Gear('Slashing talons'),
                                                                                      Gear('Lamprey bite')])],
                                               points=130)
        self.Options(self)


class Fateskimmer(HqUnit, armory.TzeentchUnit):
    type_name = get_name(units.Fateskimmer)
    type_id = 'fateskimmer_v1'
    keywords = ['Character', 'Chariot', 'Horror', 'Fly', 'Psyker']
    kwname = 'Herald of Tzeentch'
    power = 8

    class Options(OptionsList):
        def __init__(self, parent):
            super(Fateskimmer.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.StaffOfChange)
            self.variant('Horrors', get_cost(wargear.ChantingHorrors) * 3,
                         gear=[UnitDescription(get_name(wargear.ChantingHorrors), count=3)])

    def __init__(self, parent):
        super(Fateskimmer, self).__init__(parent, gear=[Gear('Ritual dagger'),
                                                        UnitDescription('Screamer', count=2,
                                                                        options=[Gear('Slashing talons'),
                                                                                 Gear('Lamprey bite')])],
                                          points=get_cost(units.Fateskimmer))
        self.Options(self)


class DiscHTzeentch(HqUnit, armory.TzeentchUnit):
    type_name = "Herald of Tzeentch on Disc"
    type_id = 'hot_disc_v1'
    keywords = ['Character', 'Cavalry', 'Horror', 'Fly', 'Psyker']
    kwname = 'Herald of Tzeentch'
    power = 5
    obsolete = True

    def __init__(self, parent):
        super(DiscHTzeentch, self).__init__(parent, gear=[Gear('Ritual dagger'),
                                                          UnitDescription('Disc', options=[Gear('Blades')])],
                                            points=99)
        HTzeentch.Options(self)


class Fluxmaster(HqUnit, armory.TzeentchUnit):
    type_name = get_name(units.Fluxmaster)
    type_id = 'fluxmaster_v1'
    keywords = ['Character', 'Cavalry', 'Horror', 'Fly', 'Psyker']
    kwname = 'Herald of Tzeentch'
    power = 5

    def __init__(self, parent):
        super(Fluxmaster, self).__init__(parent, gear=[Gear('Ritual dagger'),
                                                       UnitDescription('Disc', options=[Gear('Blades')])],
                                         points=get_cost(units.Fluxmaster))
        Changecaster.Options(self)


class Rotigus(HqUnit, armory.NurgleUnit):
    type_name = get_name(units.Rotigus)
    type_id = 'rotifus_v1'

    keywords = ['Character', 'Monster', 'Psyker', 'Fly', 'Great Unclean One']
    power = 17

    def __init__(self, parent):
        super(Rotigus, self).__init__(parent, gear=[
            Gear('Streams of brackish filth'),
            Gear('Gnarlrod'),
            Gear('Fanged maw'),
            Gear("Nurglings' claws and teeth")
        ], unique=True, static=True, points=get_cost(units.Rotigus))


class Epidemius(HqUnit, armory.NurgleUnit):
    type_name = get_name(units.Epidemius)
    type_id = 'epidemius_v1'
    keywords = ['Character', 'Cavalry', 'Plaguebearer', 'Herald of Nurgle']
    power = 5

    def __init__(self, parent):
        super(Epidemius, self).__init__(parent, gear=[Gear('Plaguesword'),
                                                      UnitDescription('Palanquin of Nurgle',
                                                                      options=[Gear("Nurglings' claws and teeth")])],
                                        static=True, unique=True, points=get_cost(units.Epidemius))


class Unclean(HqUnit, armory.NurgleUnit):
    type_name = get_name(units.GreatUncleanOne)
    type_id = 'guo_v1'
    keywords = ['Character', 'Monster', 'Psyker']

    power = 17

    class LeftWeapon(OneOf):
        def __init__(self, parent):
            super(Unclean.LeftWeapon, self).__init__(parent, 'Weapon')
            self.variant(*weapons.Bilesword)
            self.variant(*weapons.DoomsdayBell)

    class RightWeapon(OneOf):
        def __init__(self, parent):
            super(Unclean.RightWeapon, self).__init__(parent, 'Weapon')
            self.variant(*weapons.PlagueFlail)
            self.variant(*weapons.Bileblade)

    def __init__(self, parent):
        super(Unclean, self).__init__(parent, gear=[
            Gear("Nurglings' claws and teeth")
        ], points=get_cost(units.GreatUncleanOne))
        self.LeftWeapon(self)
        self.RightWeapon(self)


class HNurgle(HqUnit, armory.NurgleUnit):
    type_name = "Herald of Nurgle"
    type_id = 'hon_v1'
    keywords = ['Character', 'Infantry', 'Plaguebearer', 'Psyker']
    power = 4
    obsolete = True

    def __init__(self, parent):
        super(HNurgle, self).__init__(parent, gear=[Gear('Plaguesword')], static=True, points=70)


class Slimux(HqUnit, armory.NurgleUnit):
    type_name = get_name(units.HorticulousSlimux)
    type_id = 'slimux_v1'
    keywords = ['Character', 'Cavalry', 'Plaguebearer', 'Herald of Nurgle']
    power = 9

    def __init__(self, parent):
        super(Slimux, self).__init__(parent, gear=[Gear('Lopping shears'),
                                                   UnitDescription('Mulch',
                                                                   options=[Gear("Mulch' acidic maw")])],
                                     static=True, unique=True, points=get_cost(units.HorticulousSlimux))


class Poxbringer(HqUnit, armory.NurgleUnit):
    type_name = get_name(units.Poxbringer)
    type_id = 'poxbringer_v1'
    keywords = ['Character', 'Infantry', 'Plaguebearer', 'Psyker', 'Herald of Nurgle']
    power = 4

    def __init__(self, parent):
        super(Poxbringer, self).__init__(parent, gear=[Gear('Balesword')], static=True, points=get_cost(units.Poxbringer))


class SloppityBilepiper(HqUnit, armory.NurgleUnit):
    type_name = get_name(units.SloppityBilepiper)
    type_id = 'sloppity_bilepiper_v1'
    keywords = ['Character', 'Infantry', 'Plaguebearer', 'Psyker', 'Herald of Nurgle']
    power = 3

    def __init__(self, parent):
        super(SloppityBilepiper, self).__init__(parent, gear=[Gear('Marotter')], static=True,
                                                points=get_cost(units.SloppityBilepiper))


class SpoilpoxScrivener(HqUnit, armory.NurgleUnit):
    type_name = get_name(units.SpoilpoxScrivener)
    type_id = 'spoilpox_scrivener_v1'
    keywords = ['Character', 'Infantry', 'Plaguebearer', 'Psyker', 'Herald of Nurgle']
    power = 4

    def __init__(self, parent):
        super(SpoilpoxScrivener, self).__init__(parent, gear=[Gear('Plaguesword'), Gear('Distended maw'),
                                                              Gear('Disguisting sneezes'), ], static=True,
                                                points=get_cost(units.SpoilpoxScrivener))


class Masque(HqUnit, armory.SlaaneshUnit):
    type_name = get_name(units.TheMasqueofSlaanesh)
    type_id = 'masque_v1'
    keywords = ['Character', 'Infantry', 'Daemonette', 'Herald of Slaanesh']
    kwname = 'Herald of Slaanesh'
    power = 4

    def __init__(self, parent):
        super(Masque, self).__init__(parent, gear=[Gear('Piercing claws')],
                                     unique=True, static=True, points=get_cost(units.TheMasqueofSlaanesh))


class Keeper(HqUnit, armory.SlaaneshUnit):
    type_name = get_name(units.KeeperOfSecrets)
    type_id = 'keeper_v1'
    keywords = ['Character', 'Monster', 'Psyker']

    power = 12

    def __init__(self, parent):
        super(Keeper, self).__init__(parent, gear=[
            Gear('Witstealer sword'),
            Gear('Snapping claws')
        ], static=True, points=get_cost(units.KeeperOfSecrets))


class HSlaanesh(HqUnit, armory.SlaaneshUnit):
    type_name = get_name(units.HeraldofSlaanesh)
    type_id = 'hos_v1'
    keywords = ['Character', 'Infantry', 'Psyker']
    power = 4

    def __init__(self, parent):
        super(HSlaanesh, self).__init__(parent, gear=[Gear('Piercing claws')],
                                        static=True, points=get_cost(units.HeraldofSlaanesh))


class SteedHSlaanesh(HqUnit, armory.SlaaneshUnit):
    type_name = u"Herald of Slaanesh on Steed"
    type_id = 'hos_steed_v1'
    kwname = "Herald of Slaanesh"
    keywords = ['Character', 'Cavalry', 'Psyker']
    power = 4

    def __init__(self, parent):
        super(SteedHSlaanesh, self).__init__(
            parent, gear=[Gear('Piercing claws'),
                          UnitDescription('Steed of Slanesh', options=[Gear('Lashing tongue')])],
            static=True, points=82)


class ChariotHSlaanesh(HqUnit, armory.SlaaneshUnit):
    type_name = u"Herald of Slaanesh on Seeker Chariot"
    type_id = 'hos_chariot_v1'
    kwname = "Herald of Slaanesh"
    keywords = ['Character', 'Chariot', 'Psyker']
    power = 6

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(ChariotHSlaanesh.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Lashes of torment')

    def __init__(self, parent):
        super(ChariotHSlaanesh, self).__init__(
            parent, gear=[UnitDescription('Daemonette charioteer', options=[Gear('Piercing claws')]),
                          UnitDescription('Steed of Slanesh', count=2, options=[Gear('Lashing tongue')])],
            points=116)
        self.Weapon(self)


class ExChariotHSlaanesh(HqUnit, armory.SlaaneshUnit):
    type_name = u"Herald of Slaanesh on Exalted Seeker Chariot"
    type_id = 'hos_exchariot_v1'
    kwname = "Herald of Slaanesh"
    keywords = ['Character', 'Chariot', 'Psyker']
    power = 7

    def __init__(self, parent):
        super(ExChariotHSlaanesh, self).__init__(
            parent, gear=[UnitDescription('Daemonette charioteer', count=3, options=[Gear('Piercing claws')]),
                          UnitDescription('Steed of Slanesh', count=4, options=[Gear('Lashing tongue')])],
            points=140)
        ChariotHSlaanesh.Weapon(self)
