__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import Gear,\
    UnitDescription, Count, OptionsList
import armory, units, wargear
from builder.games.wh40k8ed.utils import *


class Furies(FastUnit, armory.DevotedUnit):
    type_name = get_name(units.Furies)
    type_id = 'furies_v1'

    keywords = ['Infantry', 'Fly']

    def __init__(self, parent):
        super(Furies, self).__init__(parent)
        self.models = Count(self, 'Chaos Furies', 5, 20, per_model=True, points=get_cost(units.Furies),
                            gear=UnitDescription('Chaos Fury', options=[Gear('Daemonic claws')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 3 if self.models.cur == 5 else 5 if  5 < self.models.cur < 10 else 7 if 10 <= self.models.cur < 20 else 10


class Hounds(FastUnit, armory.KhorneUnit):
    type_name = get_name(units.FleshHounds)
    type_id = 'hounds_v1'
    keywords = ['Beast']

    def __init__(self, parent):
        super(Hounds, self).__init__(parent)
        self.models = Count(self, self.type_name, 5, 20, points=get_cost(units.FleshHounds), per_model=True,
                            gear=UnitDescription('Flesh Hound', options=[Gear('Gore-drenched fangs')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 4 * ((self.models.cur + 4) / 5)


class Screamers(FastUnit, armory.TzeentchUnit):
    type_name = get_name(units.Screamers)
    type_id = 'screamers_v1'

    keywords = ['Cavalry', 'Fly']

    def __init__(self, parent):
        super(Screamers, self).__init__(parent)
        self.models = Count(self, 'Screamers', 3, 9, points=get_cost(units.Screamers), per_model=True,
                            gear=UnitDescription('Screamer', options=[Gear('Lamprey bite'), Gear('Slashing talons')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 4 * ((self.models.cur + 2) / 3)


class PlagueDrones(FastUnit, armory.NurgleUnit):
    type_name = get_name(units.PlagueDrones)
    type_id = 'p_drones_v1'

    keywords = ['Cavalry', 'Fly']

    member = UnitDescription('Plague Drone', options=[Gear('Plaguesword'), Gear("Death's heads"),
                                                      UnitDescription('Rot fly', options=[Gear('Prehensile proboscis')])])

    class Drones(Count):
        @property
        def description(self):
            return PlagueDrones.member.clone().set_count(self.cur - (self.parent.opt.icon.value + self.parent.opt.trumpet.value))

    class Options(OptionsList):
        def __init__(self, parent):
            super(PlagueDrones.Options, self).__init__(parent, 'Options')
            self.icon = self.variant(*wargear.DaemonicIcon,
                                     gear=[PlagueDrones.member.clone().add(Gear('Daemonic Icon'))])
            self.trumpet = self.variant(*wargear.InstrumentOfChaos,
                                        gear=[PlagueDrones.member.clone().add(Gear('Instrument of Chaos'))])

    def __init__(self, parent):
        super(PlagueDrones, self).__init__(parent, points=get_cost(units.PlagueDrones), gear=[
            UnitDescription('Plaguebringer',
                            options=[Gear('Plaguedsword'), Gear("Death's heads"),
                                     UnitDescription('Rot Fly', options=[Gear('Prehensile proboscis')])])])

        self.models = self.Drones(self, 'Plague Drones', 2, 8, points=get_cost(units.PlagueDrones), per_model=True)
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return 6 if self.models.cur == 3 else 11 if 3 < self.models.cur < 6 else 16


class Hellflayer(FastUnit, armory.SlaaneshUnit):
    type_name = get_name(units.Hellflayer)
    type_id = 'hellflayer_v1'
    keywords = ['Chariot']
    power = 5

    def __init__(self, parent):
        super(Hellflayer, self).__init__(
            parent, gear=[UnitDescription('Hellflayer chariot', options=[Gear('Bladed axle')]),
                          UnitDescription('Exalted Alluress', options=[Gear('Piercing claws')]),
                          UnitDescription('Seeker', count=2,
                                          options=[Gear('Piercing claws'), Gear('Lashes of torment')]),
                          UnitDescription('Steed of Slanesh', count=2, options=[Gear('Lashing tongue')])],
            static=True, points=get_cost(units.Hellflayer))


class Seekers(FastUnit, armory.SlaaneshUnit):
    type_name = get_name(units.Seekers)
    type_id = 'seekers_v1'

    keywords = ['Cavalry']

    member = UnitDescription('Seeker', options=[Gear('Piercing claws'),
                                                UnitDescription('Steed of Slaanesh', options=[Gear('Lashing tongue')])])

    class Riders(Count):
        @property
        def description(self):
            return Seekers.member.clone().set_count(self.cur - (self.parent.opt.icon.value + self.parent.opt.trumpet.value))

    class Options(OptionsList):
        def __init__(self, parent):
            super(Seekers.Options, self).__init__(parent, 'Options')
            self.icon = self.variant(*wargear.DaemonicIcon,
                                     gear=[Seekers.member.clone().add(Gear('Daemonic Icon'))])
            self.trumpet = self.variant(*wargear.InstrumentOfChaos,
                                        gear=[Seekers.member.clone().add(Gear('Instrument of Chaos'))])

    def __init__(self, parent):
        super(Seekers, self).__init__(parent, gear=[
            UnitDescription('Heartseeker',
                            options=[Gear('Piercing claws'),
                                     UnitDescription('Steed of Slaanesh', options=[Gear('lashing tongue')])])],
                                      points=get_cost(units.Seekers))

        self.models = self.Riders(self, 'Seekers', 4, 19, points=get_cost(units.Seekers), per_model=True)
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return 1 + 4 * ((self.models.cur + 5) / 5)
