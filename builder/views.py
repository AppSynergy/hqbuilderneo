__author__ = 'Denis Romanov'

import uuid
import json
import StringIO

from flask import Blueprint, render_template, jsonify, request, url_for, redirect, send_file
from flask.views import MethodView
from flask.ext.login import login_required, current_user

import tools


class BuilderBlueprint(Blueprint):
    def __init__(self, app):
        super(BuilderBlueprint, self).__init__('builder', __name__)
        self.builder = tools.Builder(app.debug, app.logger)

        self.add_app_template_filter(lambda data: self.builder.build_header(data), name='roster_header')
        self.add_app_template_filter(lambda data: self.builder.build_description(data), name='roster_description')

        self.app_context_processor(lambda: dict(games=self.builder.games.game_list))
        self.app_context_processor(lambda: dict(roster_count=self.builder.roster_count.count))

        self.add_url_rule('/_update_roster',
                          view_func=self.UpdateRoster.as_view('update_roster',
                                                              builder=self.builder))
        self.add_url_rule('/_delete_rosters',
                          view_func=self.DeleteRosters.as_view('delete_rosters',
                                                               builder=self.builder))
        self.add_url_rule('/_delete_roster',
                          view_func=self.DeleteRoster.as_view('delete_roster',
                                                              builder=self.builder))
        self.add_url_rule('/roster/<roster_id>',
                          view_func=self.EditRoster.as_view('edit_roster',
                                                            builder=self.builder))
        self.add_url_rule('/shared/<roster_id>',
                          view_func=self.SharedRoster.as_view('shared_roster',
                                                              builder=self.builder))
        self.add_url_rule('/print/<roster_id>',
                          view_func=self.PrintRoster.as_view('print_roster',
                                                             builder=self.builder))
        self.add_url_rule('/clone/<roster_id>',
                          view_func=self.CloneRoster.as_view('clone_roster',
                                                             builder=self.builder))
        self.add_url_rule('/', view_func=self.RosterList.as_view('main', builder=self.builder))
        self.add_url_rule('/export/<roster_id>/<format_id>',
                          view_func=self.ExportView.as_view('export_roster',
                                                            builder=self.builder))
        self.add_url_rule('/downloads/<roster_id>/<format_id>/<placeholder>',
                          view_func=self.Download.as_view('download_roster', builder=self.builder))
        self.add_url_rule('/error', view_func=self.ErrorView.as_view('error'))
        app.register_blueprint(self)

    class BuilderView(object):
        def __init__(self, builder):
            self.builder = builder
            super(BuilderBlueprint.BuilderView, self).__init__()

    class ErrorView(MethodView):
        def get(self):
            return render_template("builder/failure.html")

    class AuthPostView(BuilderView, MethodView):
        def post(self):
            if not current_user.is_authenticated:
                return jsonify(redirect=url_for('auth.signin_page'))
            data = json.loads(request.form.get('data'))
            return self.process_from(data)

        def process_from(self, data):
            raise NotImplementedError()

    class UpdateRoster(AuthPostView):
        def process_from(self, data):
            # import pdb; pdb.set_trace()
            try:
                return jsonify(self.builder.process_roster(data['roster']['id'], current_user.id, data['roster']))
            except tools.ReloadRosterError:
                return jsonify({'reload': url_for('builder.edit_roster', roster_id=data['roster']['id'])})

    class DeleteRosters(AuthPostView):
        def process_from(self, data):
            self.builder.delete_rosters(current_user.id, data['delete'])
            return jsonify(rosters=self.builder.get_user_rosters(current_user.id))

    class DeleteRoster(AuthPostView):
        def process_from(self, data):
            self.builder.delete_rosters(current_user.id, [data['delete']])
            return jsonify(redirect=url_for('builder.main'))

    class RosterView(BuilderView, MethodView):
        def get(self, roster_id):
            roster = self.builder.get_roster_dump(roster_id, current_user.get_id())
            return self.process_roster(roster)

        def process_roster(self, roster):
            raise NotImplementedError()

    class EditRoster(RosterView):
        def process_roster(self, roster):
            if roster['read_only']:
                return redirect(url_for('builder.shared_roster', roster_id=roster['id']))
            # import pdb; pdb.set_trace()
            return render_template("builder/roster.html", roster=roster)

    class SharedRoster(RosterView):
        def process_roster(self, roster):
            return render_template("builder/view.html", roster=roster)

    class PrintRoster(RosterView):
        def process_roster(self, roster):
            return render_template("builder/print.html", roster=roster)

    class CloneRoster(MethodView, BuilderView):
        decorators = [login_required]

        def get(self, roster_id):
            return redirect(url_for(
                'builder.edit_roster',
                roster_id=self.builder.clone_roster(roster_id, current_user.id)
            ))

    class RosterList(MethodView, BuilderView):
        decorators = [login_required]

        def get(self):
            return render_template("builder/roster_list.html", rosters=self.builder.get_user_rosters(current_user.id))

        def post(self):
            return redirect(url_for(
                'builder.edit_roster',
                roster_id=self.builder.create_roster(current_user.id, request.form.get('army'))
            ))

    class ExportView(MethodView, BuilderView):
        formats = dict(
            html="builder/export/html.html",
            bbcode="builder/export/bbcode.html",
            txt="builder/export/text.html",
            mdn="builder/export/mdn.html",
        )

        def get(self, roster_id, format_id, placeholder=None):
            del placeholder
            try:
                roster = self.builder.get_roster_dump(roster_id, None)
            except AttributeError:
                print("Roster {} is missing\n".format(roster_id))
                roster = None
            if not roster:
                return redirect(url_for('builder.main'))

            return self.process_export(roster, self.formats[format_id], format_id)

        @staticmethod
        def process_export(roster, export_template, format_id):
            return render_template(
                "builder/export.html",
                roster=render_template(export_template, roster=roster),
                format_id=format_id,
                roster_id=roster['id'],
                placeholder=uuid.uuid4()
            )

    class Download(ExportView):
        @staticmethod
        def process_export(roster, export_template, format_id):
            roster_name = u'{0}_{1}_{2}pt.{3}'.format(roster['name'], roster['points'], roster['limit'], format_id)
            try:
                roster_name = str(roster_name)
            except UnicodeEncodeError:
                print "Failed to print roster name; using roster id instead"
                roster_name = '{0}_{1}_{2}pt.{3}'.format(roster['id'], roster['points'], roster['limit'], format_id)
            str_io = StringIO.StringIO()
            str_io.write(render_template(export_template, roster=roster).encode('utf-8'))
            str_io.seek(0)
            return send_file(str_io, attachment_filename=roster_name, as_attachment=True)
