from builder.core import Node, build_id, node_load

__author__ = 'Denis Romanov'


class UnitType(Node):
    def __init__(self, unit_class, active=True, visible=True, type_id=None, deprecated=False):
        Node.__init__(self, id=type_id or build_id(unit_class.name), active=active, visible=visible)
        self.name = unit_class.name
        self.unit_class = unit_class
        self.deprecated = deprecated

    def dump(self):
        res = Node.dump(self)
        res['name'] = self.name
        return res

    def dump_save(self):
        res = Node.dump(self)
        res['name'] = self.unit_class.__name__
        return res

    @node_load
    def load(self, data):
        if self.deprecated:
            self.set_visible(False)
            self.set_active(False)
