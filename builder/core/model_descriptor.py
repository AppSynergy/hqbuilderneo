__author__ = 'dromanow'

from builder.core.options.options_list import OptionsList
from builder.core.options.one_of import OneOf


class ModelDescriptor():
    def __init__(self, name, count=0, points=0, gear=None, sub_unit=None):
        self.name = name
        self.points = points
        self.gear = gear[:] if gear else []
        self.count = count
        self.sub_unit = sub_unit if sub_unit else []

    def add_points(self, points):
        self.points += points
        return self

    def add_sub_unit(self, sub_unit):
        self.sub_unit.append(sub_unit)
        return self

    def add_gear(self, name, points=0, count=1):
        self.points += points
        self.gear += [name for _ in range(count)]
        return self

    def add_gear_list(self, gear, points=0, count=1):
        for _ in range(count):
            self.points += points
            self.gear += gear
        return self

    def add_gear_opt(self, opt, count=1):
        if isinstance(opt, OptionsList):
            if not opt.is_used():
                return self
            self.add_gear_list(gear=opt.get_selected(), points=opt.points(), count=count)
        if isinstance(opt, OneOf):
            if not opt.is_used():
                return self
            self.add_gear(name=opt.get_selected(), points=opt.points(), count=count)
        return self

    def set_count(self, count):
        self.count = count
        return self

    def set_name(self, name):
        self.name = name
        return self

    def clone(self):
        return ModelDescriptor(self.name, self.count, self.points, self.gear)

    def build_options(self):
        options = {}
        for g in self.gear:
            options[g] = options.get(g, 0) + 1
        return sorted([{'name': name, 'count': count} for name, count in options.iteritems()], key=lambda v: v['name'])

    def build(self, count=None):
        if count == 0:
            return
        return {
            'sub_unit': {
                'name': self.name,
                'points': self.points,
                'options': self.build_options(),
                'sub_units': self.sub_unit
            },
            'count': count if count is not None else self.count
        }
