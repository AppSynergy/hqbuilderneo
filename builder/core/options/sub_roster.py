__author__ = 'Denis Romanov'

from builder.core import id_filter
from builder.core.options import GenericOption
from builder.core import node_load


class SubRoster(GenericOption):

    def __init__(self, roster_class):
        GenericOption.__init__(self, roster_class.army_id, roster_class.name)
        self._roster = self.observable_object('roster', roster_class(secondary=True))

    def dump(self):
        res = GenericOption.dump(self)
        res.update({
            'type': 'sub_roster',
            'roster': self._roster.get().dump(),
        })
        return res

    def dump_save(self):
        res = GenericOption.dump(self)
        res.update({
            'roster': self._roster.get().save(),
        })
        return res

    @node_load
    def load(self, data):
        self._roster.get().load(data['roster'])

    @id_filter
    def update(self, data):
        if 'roster' in data.keys():
            self._roster.get().update(data['roster'])

    def get_roster(self):
        return self._roster.get()

    def points(self):
        if not self.is_used():
            return 0
        return self._roster.get().points

    def get_errors(self):
        return self.get_roster().errors

    @property
    def roster(self):
        return self.get_roster()
