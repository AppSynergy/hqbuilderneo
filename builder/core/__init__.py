__author__ = 'Denis Romanov'

import json
import re


def filter_items(items, limit):
    return [item for item in items if item[1] <= limit]


def get_ids(items):
    return [i[2] for i in items]


def build_points_string(points):
    if points is 0:
        return '(free)'
    else:
        return '(+{0}pt.)'.format(points)


def build_full_name(name, points):
    return name + ' ' + build_points_string(points)


pattern = re.compile('[\W_]+')


def build_id(name):
    name = name.lower()
    return pattern.sub('', name)


def id_filter(f):
    def check(self, data):
        if self.id == data['id']:
            return f(self, data)
    return check


def node_load(f):
    def load(self, data):
        Node.load(self, data)
        return f(self, data)
    return load


class Node(object):
    class Observable():
        def __init__(self, id, val):
            self._id = id
            self._value = val
            self._update_count = 0

        def set(self, val):
            if self._value == val:
                return
            self._update_count += 1
            self._value = val

        def get(self):
            return self._value

        @property
        def delta(self):
            if self._update_count is 0:
                return {}
            self._update_count = 0
            return {self._id: self._value}

        def flush(self):
            self._update_count = 0

    class ObservableArray():
        def __init__(self, id, array):
            self._id = id
            self._array = array

        def get(self):
            return self._array

        @property
        def delta(self):
            res = []
            for opt in self._array:
                delta = opt.delta
                if delta == {}:
                    continue
                res.append(delta)
            if not res:
                return {}
            return {self._id: res}

        def flush(self):
            for opt in self._array:
                opt.flush()

        def check_rules_chain(self):
            for opt in self._array:
                opt.check_rules_chain()

    class ObservableObject():
        def __init__(self, id, obj):
            self._id = id
            self._obj = obj

        def get(self):
            return self._obj

        @property
        def delta(self):
            delta = self._obj.delta
            if not delta:
                return {}
            return {self._id: delta}

        def flush(self):
            self._obj.flush()

        def check_rules_chain(self):
            self._obj.check_rules_chain()

    def __init__(self, id=None, active=True, visible=True):
        self._observables = []
        self._observable_arrays = []
        self.id = id
        self._active = self.observable('active', active)
        self._visible = self.observable('visible', visible)
        pass

    def observable(self, id, val):
        o = self.Observable(id, val)
        self._observables.append(o)
        return o

    def observable_array(self, id, array):
        o = self.ObservableArray(id, array)
        self._observable_arrays.append(o)
        return o

    def observable_object(self, id, obj):
        o = self.ObservableObject(id, obj)
        self._observable_arrays.append(o)
        return o

    def set_active(self, val):
        self._active.set(val)

    def enable(self):
        self.set_active(True)

    def disable(self):
        self.set_active(False)

    def is_active(self):
        return self._active.get()

    def set_visible(self, val):
        self._visible.set(val)

    def show(self):
        self.set_active(True)

    def hide(self):
        self.set_active(False)

    def is_visible(self):
        return self._visible.get()

    def is_used(self):
        return self.is_active() and self.is_visible()

    def update(self, data):  # Update current node attributes
        pass

    @property
    def delta(self):  # Get and clean current delta
        delta = {}
        for o in self._observables:
            delta.update(o.delta)
        for o in self._observable_arrays:
            delta.update(o.delta)
        if delta == {}:
            return {}
        delta['id'] = self.id
        return delta

    def flush(self):
        for o in self._observables:
            o.flush()
        for o in self._observable_arrays:
            o.flush()

    def dump(self):  # Dump currents attributes state
        return {
            'id': self.id,
            'active': self._active.get(),
            'visible': self._visible.get(),
        }

    def check_rules_chain(self):
        for o in self._observable_arrays:
            o.check_rules_chain()

    def load(self, data):
        self.id = data['id']
        self._active.set(data['active'])
        self._visible.set(data['visible'])

    def dump_json(self):
        return json.dumps(self.dump())
