import os

__author__ = 'dromanow'


class ReleaseConfig(object):
    ROOT = os.path.dirname(os.path.abspath(__file__))
    DEBUG = False
    ASSETS_DEBUG = DEBUG
    SECRET_KEY = "9d8e71cebd07430d95547372037f5e62"

    MAIL_SERVER = 'smtp.yandex.ru'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_DEBUG = False
    MAIL_BCC = 'bugs@hq-builder.com'
    MAIL_USERNAME = 'support@hq-builder.com'
    MAIL_PASSWORD = 'paradox2510808'
    MAIL_DEFAULT_SENDER = 'support@hq-builder.com'

    TORNADO_ADDRESS = '127.0.0.1'
    TORNADO_PORT = 5000

    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://hqbuilder:f73877da5c0a4992b7d4400dd924dfdb@localhost/bunker'

    SENTY_DSN = 'http://3575a075d0ca4de5a2f6c7559d8dc907:f1de2b422870413093e6259030f9cb5d@sentry.zaitzev.pro/2'


class DebugConfig(ReleaseConfig):
    DEBUG = True
    ASSETS_DEBUG = True
    MAIL_DEBUG = True

    TORNADO_PORT = 5001

    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://test_user:qwerty@localhost/test_database'
    SENTY_DSN = 'http://f835ca8c111f490fa3896fb1a7d43460:d736aa94b5a847b5ad229a54ac236d31@sentry.zaitzev.pro/3'


try:
    from config_local import *
except ImportError as e:
    pass
