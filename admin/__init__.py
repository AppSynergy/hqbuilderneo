__author__ = 'dromanow'


from datetime import datetime, timedelta
import subprocess
from flask.ext.admin import Admin, AdminIndexView, expose, BaseView
from flask.ext.login import current_user
from db.models import User, Roster, Post, Comment, Battle


class AdminBlueprint(Admin):

    def __init__(self, app):
        super(AdminBlueprint, self).__init__(app=app, index_view=self.Statistic(url='/4004f5a2aed84c2290d7bcced59eb7c0'))
        self.add_view(self.Disk(name='Disk'))

    class Statistic(AdminIndexView):
        def is_accessible(self):
            return current_user.is_authenticated and current_user.admin

        @expose('/')
        def index(self):
            return self.render(
                'admin/admin.html',
                stats=(
                    dict(
                        name=c.__name__,
                        count=c.query.count(),
                        grow1=c.query.filter(c.created > datetime.utcnow() - timedelta(days=1)).count(),
                        grow30=c.query.filter(c.created > datetime.utcnow() - timedelta(days=30)).count()
                    ) for c in [User, Roster, Post, Comment, Battle]
                ),
            )

    class Disk(BaseView):
        def is_accessible(self):
            return current_user.is_authenticated and current_user.admin

        @expose('/')
        def index(self):
            df = subprocess.Popen(["df", "-h"], stdout=subprocess.PIPE)
            output = df.communicate()[0]
            disk_info = [point.split() for point in output.split("\n")[1:]]

            return self.render(
                'admin/disk.html',
                disk_info=disk_info
            )
